﻿'==========================================
'Created Date   : 22 Sep 2021
'Created By     : NawaData
'Description    : Library for goAML Report
'                 
'==========================================

Imports System.Data.SqlClient
Imports NawaDevDAL

Public Class GoAML_Report_BLL

    'Enum Identification Person Type : fungsi utama untuk membedakan terutama Person Identification
    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        'Director = 4        'Untuk level transaksi gak dipakai. Yang dipakai untuk yg 6
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
    End Enum

    'Enum SenderInformation : fungsi utama untuk membedakan terutama untuk Entity Director (antara Account Entity Director dan Entity Director 1 table yang sama)
    Public Enum enumSenderInformation
        Account = 1
        Person = 2
        Entity = 3
    End Enum

    Shared Function getTRNorACT(strReportCode As String) As String
        Try
            Dim strResult As String = "TRN"
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("goAML_odm_ref_Report_TRNorACT", "ReportType", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("TRNorACT")) Then
                    strResult = drTemp("TRNorACT")
                End If
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return "TRN"
        End Try
    End Function

    Shared Function isUseIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                isUseIndicator = True
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function isRequireIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("ReportType")) Then
                    If drTemp("ReportType").ToString.Contains("STR") Then
                        bolResult = True
                    End If
                End If
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function getValidationFromType(transmodeCode As String, instrumenFrom As String, SenderInformation As Integer, myClient As Boolean) As String
        Dim keterangan As String = ""
        Try
            Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
                If SenderInformation = 1 Then ' Account
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 2 Then 'Person
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 3 Then 'Entity
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity NotMyClient"
                        End If
                    End If
                End If
            End Using

            Return keterangan
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getValidationToType(transmodeCode As String, instrumenFrom As String, instrumenTo As String, PenerimaInformation As Integer, TomyClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If PenerimaInformation = 1 Then ' Account
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 2 Then 'Person
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 3 Then 'Entity
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity NotMyClient"
                    End If
                End If

            End If
        End Using

        Return keterangan
    End Function

    Shared Function getValidationReportByReportID(strReportID As String)
        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@reportId"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_ResponseValidateReportByID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Shared Function GetGoAMLReportClassByID(ID As Long) As GoAML_Report_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLReportClass = New GoAML_Report_Class
            With objGoAMLReportClass
                'GoAML Report
                .obj_goAML_Report = objDb.goAML_Report.Where(Function(x) x.PK_Report_ID = ID).FirstOrDefault

                'Transaction
                .list_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = ID).ToList

                'Activity
                .list_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = ID).ToList

                'Indicator
                .list_goAML_Report_Indicator = objDb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = ID).ToList
            End With

            Return objGoAMLReportClass
        End Using
    End Function

    Shared Function GetGoAMLTransactionClassByID(ID As Long) As GoAML_Transaction_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLTransactionClass = New GoAML_Transaction_Class
            With objGoAMLTransactionClass
                'GoAML Transaction
                .obj_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = ID).FirstOrDefault

                If .obj_goAML_Transaction IsNot Nothing Then
                    If .obj_goAML_Transaction.FK_Transaction_Type IsNot Nothing Then
                        If .obj_goAML_Transaction.FK_Transaction_Type = 1 Then      'Transaction Bi-Party
                            'Account
                            Dim listAccount = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = ID).ToList
                            If listAccount IsNot Nothing Then .list_goAML_Transaction_Account.AddRange(listAccount)

                            'Signatory
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listSignatory = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = item.PK_Account_ID).ToList
                                    If listSignatory IsNot Nothing Then .list_goAML_Trn_acc_Signatory.AddRange(listSignatory)
                                Next
                            End If

                            'Signatory Address, Phone, Identification
                            If .list_goAML_Trn_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_acc_Signatory.Count > 0 Then
                                For Each item In .list_goAML_Trn_acc_Signatory
                                    Dim listAddress = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_sign_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_acc_sign_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Account Entity
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listAccountEntity = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = item.PK_Account_ID).ToList
                                    If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Entity_account.AddRange(listAccountEntity)
                                Next
                            End If

                            'Account Entity Address, Phone
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listAddress = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList
                                    Dim listPhone = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Account Entity Director
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = item.PK_goAML_Trn_Entity_account And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Account Entity Director Address, Phone, Identification
                            Dim listAccountEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                            If listAccountEntityDirector IsNot Nothing AndAlso listAccountEntityDirector.Count > 0 Then
                                For Each item In listAccountEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Person
                            Dim listPerson = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listPerson IsNot Nothing Then .list_goAML_Transaction_Person.AddRange(listPerson)

                            'Person Address, Phone, Identification
                            If .list_goAML_Transaction_Person IsNot Nothing AndAlso .list_goAML_Transaction_Person.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Person
                                    Dim listAddress = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Person_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Person_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Entity
                            Dim listEntity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listEntity IsNot Nothing Then .list_goAML_Transaction_Entity.AddRange(listEntity)

                            'Entity Address, Phone
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listAddress = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList
                                    Dim listPhone = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Entity Director
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Transaction_ID = ID And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                                    .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Entity Director Address, Phone, Identification
                            Dim listEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                            If listEntityDirector IsNot Nothing AndAlso listEntityDirector.Count > 0 Then
                                For Each item In listEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Conductor
                            Dim listConductor = objDb.goAML_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listConductor IsNot Nothing Then .list_goAML_Trn_Conductor.AddRange(listConductor)

                            'Conductor Address, Phone, Identification
                            If .list_goAML_Trn_Conductor IsNot Nothing AndAlso .list_goAML_Trn_Conductor.Count > 0 Then
                                For Each item In .list_goAML_Trn_Conductor
                                    Dim listAddress = objDb.goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Conductor).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Conductor_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Conductor_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                        Else    'Transaction Multi Party
                            Dim listParty = objDb.goAML_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listParty IsNot Nothing Then .list_goAML_Transaction_Party.AddRange(listParty)

                            If .list_goAML_Transaction_Party IsNot Nothing AndAlso .list_goAML_Transaction_Party.Count > 0 Then
                                For Each party In .list_goAML_Transaction_Party
                                    'Account
                                    Dim listAccount = objDb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listAccount IsNot Nothing Then .list_goAML_Trn_Party_Account.AddRange(listAccount)

                                    'Signatory
                                    If .list_goAML_Trn_Party_Account IsNot Nothing AndAlso .list_goAML_Trn_Party_Account.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Account
                                            Dim listSignatory = objDb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = item.PK_Trn_Party_Account_ID).ToList
                                            If listSignatory IsNot Nothing Then .list_goAML_Trn_par_acc_Signatory.AddRange(listSignatory)
                                        Next
                                    End If

                                    'Signatory Address, Phone, Identification
                                    If .list_goAML_Trn_par_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_par_acc_Signatory.Count > 0 Then
                                        For Each item In .list_goAML_Trn_par_acc_Signatory
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = item.PK_Trn_par_acc_Signatory_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = item.PK_Trn_par_acc_Signatory_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_par_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_sign_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_sign_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Account Entity
                                    If .list_goAML_Trn_Party_Account IsNot Nothing AndAlso .list_goAML_Trn_Party_Account.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Account
                                            Dim listAccountEntity = objDb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = item.PK_Trn_Party_Account_ID).ToList
                                            If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Par_Acc_Entity.AddRange(listAccountEntity)
                                        Next
                                    End If

                                    'Account Entity Address, Phone
                                    If .list_goAML_Trn_Par_Acc_Entity IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Entity
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)
                                        Next
                                    End If

                                    'Account Entity Director
                                    If .list_goAML_Trn_Par_Acc_Entity IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Entity
                                            Dim listDirector = objDb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList
                                            If listDirector IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director.AddRange(listDirector)
                                        Next
                                    End If

                                    'Account Entity Director Address, Phone, Identification
                                    If .list_goAML_Trn_Par_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Ent_Director.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Ent_Director
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Person
                                    Dim listPerson = objDb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listPerson IsNot Nothing Then .list_goAML_Trn_Party_Person.AddRange(listPerson)

                                    'Person Address, Phone, Identification
                                    If .list_goAML_Trn_Party_Person IsNot Nothing AndAlso .list_goAML_Trn_Party_Person.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Person
                                            Dim listAddress = objDb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = item.PK_Trn_Party_Person_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = item.PK_Trn_Party_Person_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Party_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Person_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Person_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Entity
                                    Dim listEntity = objDb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listEntity IsNot Nothing Then .list_goAML_Trn_Party_Entity.AddRange(listEntity)

                                    'Entity Address, Phone
                                    If .list_goAML_Trn_Party_Entity IsNot Nothing AndAlso .list_goAML_Trn_Party_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Entity
                                            Dim listAddress = objDb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Entity_Phone.AddRange(listPhone)
                                        Next
                                    End If

                                    'Entity Director
                                    If .list_goAML_Trn_Party_Entity IsNot Nothing AndAlso .list_goAML_Trn_Party_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Entity
                                            Dim listDirector = objDb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList
                                            .list_goAML_Trn_Par_Entity_Director.AddRange(listDirector)
                                        Next
                                    End If

                                    'Entity Director Address, Phone, Identification
                                    If .list_goAML_Trn_Par_Entity_Director IsNot Nothing AndAlso .list_goAML_Trn_Par_Entity_Director.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Entity_Director
                                            Dim listAddress = objDb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = item.PK_Trn_Par_Entity_Director_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = item.PK_Trn_Par_Entity_Director_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                Next
                            End If
                        End If
                    End If
                End If
            End With

            Return objGoAMLTransactionClass
        End Using
    End Function

    Shared Function GetGoAMLActivityClassByID(ID As Long) As GoAML_Activity_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLActivityClass = New GoAML_Activity_Class
            With objGoAMLActivityClass
                'GoAML Activity
                .obj_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault

                If .obj_goAML_Act_ReportPartyType IsNot Nothing Then
                    Dim objActivity = .obj_goAML_Act_ReportPartyType

                    'Account
                    Dim listAccount = objDb.goAML_Act_Account.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listAccount IsNot Nothing Then .list_goAML_Act_Account.AddRange(listAccount)

                    'Signatory
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listSignatory = objDb.goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listSignatory IsNot Nothing Then .list_goAML_Act_acc_Signatory.AddRange(listSignatory)
                        Next
                    End If

                    'Signatory Address, Phone, Identification
                    If .list_goAML_Act_acc_Signatory IsNot Nothing AndAlso .list_goAML_Act_acc_Signatory.Count > 0 Then
                        For Each item In .list_goAML_Act_acc_Signatory
                            Dim listAddress = objDb.goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_sign_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_sign_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Account Entity
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listAccountEntity = objDb.goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listAccountEntity IsNot Nothing Then .list_goAML_Act_Entity_Account.AddRange(listAccountEntity)
                        Next
                    End If

                    'Account Entity Address, Phone
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Account Entity Director
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            If listDirector IsNot Nothing Then .list_goAML_Act_Acc_Ent_Director.AddRange(listDirector)
                        Next
                    End If

                    'Account Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Acc_Ent_Director
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_Act_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Person
                    Dim listPerson = objDb.goAML_Act_Person.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listPerson IsNot Nothing Then .list_goAML_Act_Person.AddRange(listPerson)

                    'Person Address, Phone, Identification
                    If .list_goAML_Act_Person IsNot Nothing AndAlso .list_goAML_Act_Person.Count > 0 Then
                        For Each item In .list_goAML_Act_Person
                            Dim listAddress = objDb.goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Person_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Person_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Entity
                    Dim listEntity = objDb.goAML_Act_Entity.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listEntity IsNot Nothing Then .list_goAML_Act_Entity.AddRange(listEntity)

                    'Entity Address, Phone
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listAddress = objDb.goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Entity Director
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            .list_goAML_Act_Director.AddRange(listDirector)
                        Next
                    End If

                    'Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Director IsNot Nothing AndAlso .list_goAML_Act_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Director
                            Dim listAddress = objDb.goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Director_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If
                End If
            End With

            Return objGoAMLActivityClass
        End Using
    End Function

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As GoAML_Report_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_Report_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using
            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Transaction_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLTransactionClassByID(objData.obj_goAML_Transaction.PK_Transaction_ID)
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Transaction
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then       'Transaction Bi-Party
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            '1. Account
                            For Each objAccount In objData.list_goAML_Transaction_Account
                                OldPK = objAccount.PK_Account_ID
                                objAccount.FK_Report_ID = strReportID
                                objAccount.FK_Report_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objAccount.PK_Account_ID < 0 Then
                                    objDB.Entry(objAccount).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Account Signatory
                                Dim listSignatory = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = OldPK).ToList
                                If listSignatory IsNot Nothing Then
                                    For Each objSignatory In listSignatory
                                        OldPKSub = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                        objSignatory.FK_Report_ID = strReportID
                                        objSignatory.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objSignatory.FK_Transaction_Account_ID = objAccount.PK_Account_ID

                                        If objSignatory.PK_goAML_Trn_acc_Signatory_ID < 0 Then
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory Address
                                        Dim listAddress = objData.list_goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objAddress.PK_goAML_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Phone
                                        Dim listPhone = objData.list_goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objPhone.PK_goAML_trn_acc_sign_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If

                                'Account Entity
                                Dim listAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = OldPK).ToList
                                If listAccountEntity IsNot Nothing Then
                                    For Each objAccountEntity In listAccountEntity
                                        OldPKSub = objAccountEntity.PK_goAML_Trn_Entity_account
                                        objAccountEntity.FK_Report_ID = strReportID
                                        objAccountEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objAccountEntity.FK_Account_ID = objAccount.PK_Account_ID

                                        If objAccountEntity.PK_goAML_Trn_Entity_account < 0 Then
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Entity Address
                                        Dim listEntityAddress = objData.list_goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityAddress IsNot Nothing Then
                                            For Each objAddress In listEntityAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objAddress.PK_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Phone
                                        Dim listEntityPhone = objData.list_goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityPhone IsNot Nothing Then
                                            For Each objPhone In listEntityPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objPhone.PK_goAML_Trn_Acc_Entity_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Director
                                        Dim listAccountEntityDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Account And x.FK_Entity_ID = OldPKSub).ToList
                                        If listAccountEntityDirector IsNot Nothing Then
                                            For Each objDirector In listAccountEntityDirector
                                                OldPKSubSubID = objDirector.PK_goAML_Trn_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                                objDirector.FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Director Address
                                                Dim listAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Phone
                                                Dim listPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '2. Person
                            For Each objPerson In objData.list_goAML_Transaction_Person
                                OldPK = objPerson.PK_Person_ID
                                objPerson.FK_Report_ID = strReportID
                                objPerson.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objPerson.PK_Person_ID < 0 Then
                                    objDB.Entry(objPerson).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Person Address
                                Dim listAddress = objData.list_goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objAddress.PK_goAML_Trn_Person_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Phone
                                Dim listPhone = objData.list_goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objPhone.PK_goAML_trn_Person_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objPerson.PK_Person_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next

                            '3. Entity
                            For Each objEntity In objData.list_goAML_Transaction_Entity
                                OldPK = objEntity.PK_Entity_ID
                                objEntity.FK_Report_ID = strReportID
                                objEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objEntity.PK_Entity_ID < 0 Then
                                    objDB.Entry(objEntity).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Entity Address
                                Dim listAddress = objData.list_goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objAddress.PK_goAML_Trn_Entity_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Phone
                                Dim listPhone = objData.list_goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objPhone.PK_goAML_Trn_Entity_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Director
                                Dim listDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Entity And x.FK_Entity_ID = OldPK).ToList
                                If listDirector IsNot Nothing Then
                                    For Each objDirector In listDirector
                                        OldPKSub = objDirector.PK_goAML_Trn_Director_ID
                                        objDirector.FK_Report_ID = strReportID
                                        objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objDirector.FK_Entity_ID = objEntity.PK_Entity_ID

                                        If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'EntityDirector Address
                                        Dim listDirectorAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorAddress IsNot Nothing Then
                                            For Each objAddress In listDirectorAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Phone
                                        Dim listDirectorPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorPhone IsNot Nothing Then
                                            For Each objPhone In listDirectorPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '4. Conductor
                            For Each objConductor In objData.list_goAML_Trn_Conductor
                                objConductor.FK_REPORT_ID = strReportID
                                objConductor.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objConductor.PK_goAML_Trn_Conductor_ID < 0 Then
                                    objDB.Entry(objConductor).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objConductor).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Conductor Address
                                For Each objAddress In objData.list_goAML_Trn_Conductor_Address
                                    objAddress.FK_REPORT_ID = strReportID
                                    objAddress.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objAddress.PK_goAML_Trn_Conductor_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Phone
                                For Each objPhone In objData.list_goAML_trn_Conductor_Phone
                                    objPhone.FK_REPORT_ID = strReportID
                                    objPhone.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objPhone.PK_goAML_trn_Conductor_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Conductor).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next
                        Else    'Transaction Multi-Party
                            Dim OldPKTParty As Long = 0
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            For Each objTParty In objData.list_goAML_Transaction_Party
                                OldPKTParty = objTParty.PK_Trn_Party_ID
                                objTParty.FK_Report_ID = strReportID
                                objTParty.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objTParty.PK_Trn_Party_ID < 0 Then
                                    objDB.Entry(objTParty).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objTParty).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                '1. Account
                                Dim listAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = OldPKTParty).ToList
                                If listAccount IsNot Nothing Then
                                    For Each objAccount In listAccount
                                        OldPK = objAccount.PK_Trn_Party_Account_ID
                                        objAccount.FK_Report_ID = strReportID
                                        objAccount.FK_Trn_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objAccount.PK_Trn_Party_Account_ID < 0 Then
                                            objDB.Entry(objAccount).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory
                                        Dim listSignatory = objData.list_goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listSignatory IsNot Nothing Then
                                            For Each objSignatory In listSignatory
                                                OldPKSub = objSignatory.PK_Trn_par_acc_Signatory_ID
                                                objSignatory.FK_Report_ID = strReportID
                                                objSignatory.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objSignatory.PK_Trn_par_acc_Signatory_ID < 0 Then
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Signatory Address
                                                Dim listAddress = objData.list_goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = OldPKSub).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Signatory_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objAddress.PK_Trn_Par_Acc_sign_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Phone
                                                Dim listPhone = objData.list_goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = OldPKSub).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Par_Acc_Sign = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objPhone.PK_trn_Par_Acc_Sign_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If

                                        'Account Entity
                                        Dim listAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listAccountEntity IsNot Nothing Then
                                            For Each objAccountEntity In listAccountEntity
                                                OldPKSub = objAccountEntity.PK_Trn_Par_acc_Entity_ID
                                                objAccountEntity.FK_Report_ID = strReportID
                                                objAccountEntity.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objAccountEntity.PK_Trn_Par_acc_Entity_ID < 0 Then
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Address
                                                Dim listEntityAddress = objData.list_goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityAddress IsNot Nothing Then
                                                    For Each objAddress In listEntityAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Phone
                                                Dim listEntityPhone = objData.list_goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityPhone IsNot Nothing Then
                                                    For Each objPhone In listEntityPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objPhone.PK_trn_Par_Acc_Entity_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director
                                                Dim listAccountEntityDirector = objData.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = OldPKSub).ToList
                                                If listAccountEntityDirector IsNot Nothing Then
                                                    For Each objDirector In listAccountEntityDirector
                                                        OldPKSubSubID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID
                                                        objDirector.FK_Report_ID = strReportID
                                                        objDirector.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objDirector.PK_Trn_Par_Acc_Ent_Director_ID < 0 Then
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()

                                                        'Account Entity Director Address
                                                        Dim listAddress = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listAddress IsNot Nothing Then
                                                            For Each objAddress In listAddress
                                                                objAddress.FK_Report_ID = strReportID
                                                                objAddress.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Phone
                                                        Dim listPhone = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listPhone IsNot Nothing Then
                                                            For Each objPhone In listPhone
                                                                objPhone.FK_Report_ID = strReportID
                                                                objPhone.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objPhone.PK_Trn_Par_Acc_Ent_Director_Phone_ID < 0 Then
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Identification
                                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                        If listIdentification IsNot Nothing Then
                                                            For Each objIdentification In listIdentification
                                                                objIdentification.FK_Report_ID = strReportID
                                                                objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If

                                '2. Person
                                Dim listPerson = objData.list_goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listPerson IsNot Nothing Then
                                    For Each objPerson In listPerson
                                        OldPK = objPerson.PK_Trn_Party_Person_ID
                                        objPerson.FK_Report_ID = strReportID
                                        objPerson.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objPerson.PK_Trn_Party_Person_ID < 0 Then
                                            objDB.Entry(objPerson).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Person Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objAddress.PK_Trn_Party_Person_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objPhone.PK_Trn_Party_Person_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If

                                '3. Entity
                                Dim listEntity = objData.list_goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listEntity IsNot Nothing Then
                                    For Each objEntity In listEntity
                                        OldPK = objEntity.PK_Trn_Party_Entity_ID
                                        objEntity.FK_Report_ID = strReportID
                                        objEntity.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objEntity.PK_Trn_Party_Entity_ID < 0 Then
                                            objDB.Entry(objEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Entity Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objAddress.PK_Trn_Party_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objPhone.PK_Trn_Party_Entity_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Director
                                        Dim listDirector = objData.list_goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listDirector IsNot Nothing Then
                                            For Each objDirector In listDirector
                                                OldPKSub = objDirector.PK_Trn_Par_Entity_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objDirector.PK_Trn_Par_Entity_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'EntityDirector Address
                                                Dim listDirectorAddress = objData.list_goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorAddress IsNot Nothing Then
                                                    For Each objAddress In listDirectorAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objAddress.PK_Trn_Par_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Phone
                                                Dim listDirectorPhone = objData.list_goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorPhone IsNot Nothing Then
                                                    For Each objPhone In listDirectorPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objPhone.PK_Trn_Par_Entity_Director_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            'Create new object for deleted data
                            Dim objDeletedData As New GoAML_Transaction_Class

                            '======================== Bi-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Transaction_Account
                                Dim objCek = objData.list_goAML_Transaction_Account.Find(Function(x) x.PK_Account_ID = item_old.PK_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_acc_Signatory.Find(Function(x) x.PK_goAML_Trn_acc_Signatory_ID = item_old.PK_goAML_Trn_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_sign_Address.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = item_old.PK_goAML_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_acc_sign_Phone.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = item_old.PK_goAML_trn_acc_sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                                Dim objCek = objData.list_goAML_Trn_Entity_account.Find(Function(x) x.PK_goAML_Trn_Entity_account = item_old.PK_goAML_Trn_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Acc_Entity_Address_ID = item_old.PK_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone = item_old.PK_goAML_Trn_Acc_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Transaction_Person
                                Dim objCek = objData.list_goAML_Transaction_Person.Find(Function(x) x.PK_Person_ID = item_old.PK_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Person_Address.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = item_old.PK_goAML_Trn_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                                Dim objCek = objData.list_goAML_trn_Person_Phone.Find(Function(x) x.PK_goAML_trn_Person_Phone = item_old.PK_goAML_trn_Person_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Transaction_Entity
                                Dim objCek = objData.list_goAML_Transaction_Entity.Find(Function(x) x.PK_Entity_ID = item_old.PK_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Entity_Address.Find(Function(x) x.PK_goAML_Trn_Entity_Address_ID = item_old.PK_goAML_Trn_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Entity_Phone = item_old.PK_goAML_Trn_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Director
                                Dim objCek = objData.list_goAML_Trn_Director.Find(Function(x) x.PK_goAML_Trn_Director_ID = item_old.PK_goAML_Trn_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Director_Address.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = item_old.PK_goAML_Trn_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                                Dim objCek = objData.list_goAML_trn_Director_Phone.Find(Function(x) x.PK_goAML_trn_Director_Phone = item_old.PK_goAML_trn_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '4. Conductor
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor
                                Dim objCek = objData.list_goAML_Trn_Conductor.Find(Function(x) x.PK_goAML_Trn_Conductor_ID = item_old.PK_goAML_Trn_Conductor_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Address
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                                Dim objCek = objData.list_goAML_Trn_Conductor_Address.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = item_old.PK_goAML_Trn_Conductor_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Phone
                            For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                                Dim objCek = objData.list_goAML_trn_Conductor_Phone.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = item_old.PK_goAML_trn_Conductor_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Identification
                            For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                                Dim objCek = objData.list_goAML_Transaction_Person_Identification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = item_old.PK_goAML_Transaction_Person_Identification_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '============================ MULTI-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                                Dim objCek = objData.list_goAML_Trn_Party_Account.Find(Function(x) x.PK_Trn_Party_Account_ID = item_old.PK_Trn_Party_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_par_acc_Signatory.Find(Function(x) x.PK_Trn_par_acc_Signatory_ID = item_old.PK_Trn_par_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_sign_Address.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = item_old.PK_Trn_Par_Acc_sign_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_sign_Phone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = item_old.PK_trn_Par_Acc_Sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Entity.Find(Function(x) x.PK_Trn_Par_acc_Entity_ID = item_old.PK_Trn_Par_acc_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_Entity_Phone.Find(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID = item_old.PK_trn_Par_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID = item_old.PK_Trn_Par_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = item_old.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                                Dim objCek = objData.list_goAML_Trn_Party_Person.Find(Function(x) x.PK_Trn_Party_Person_ID = item_old.PK_Trn_Party_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Address.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = item_old.PK_Trn_Party_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Phone.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = item_old.PK_Trn_Party_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                                Dim objCek = objData.list_goAML_Trn_Party_Entity.Find(Function(x) x.PK_Trn_Party_Entity_ID = item_old.PK_Trn_Party_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Address.Find(Function(x) x.PK_Trn_Party_Entity_Address_ID = item_old.PK_Trn_Party_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Phone.Find(Function(x) x.PK_Trn_Party_Entity_Phone_ID = item_old.PK_Trn_Party_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director.Find(Function(x) x.PK_Trn_Par_Entity_Director_ID = item_old.PK_Trn_Par_Entity_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Address.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = item_old.PK_Trn_Par_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Phone.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = item_old.PK_Trn_Par_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveTransaction_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)

            End Using

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction_AuditTrail(objData As GoAML_Transaction_Class, objData_Old As GoAML_Transaction_Class, objDB As NawaDatadevEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert Then
                NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Transaction)
            ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Transaction, objData_Old.obj_goAML_Transaction)
            End If

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Account", objData.list_goAML_Transaction_Account, objData_Old.list_goAML_Transaction_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_acc_Signatory", objData.list_goAML_Trn_acc_Signatory, objData_Old.list_goAML_Trn_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_sign_Address", objData.list_goAML_Trn_Acc_sign_Address, objData_Old.list_goAML_Trn_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_acc_sign_Phone", objData.list_goAML_trn_acc_sign_Phone, objData_Old.list_goAML_trn_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_account", objData.list_goAML_Trn_Entity_account, objData_Old.list_goAML_Trn_Entity_account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Address", objData.list_goAML_Trn_Acc_Entity_Address, objData_Old.list_goAML_Trn_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Phone", objData.list_goAML_Trn_Acc_Entity_Phone, objData_Old.list_goAML_Trn_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person", objData.list_goAML_Transaction_Person, objData_Old.list_goAML_Transaction_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Person_Address", objData.list_goAML_Trn_Person_Address, objData_Old.list_goAML_Trn_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Person_Phone", objData.list_goAML_trn_Person_Phone, objData_Old.list_goAML_trn_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Entity", objData.list_goAML_Transaction_Entity, objData_Old.list_goAML_Transaction_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Address", objData.list_goAML_Trn_Entity_Address, objData_Old.list_goAML_Trn_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Phone", objData.list_goAML_Trn_Entity_Phone, objData_Old.list_goAML_Trn_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director", objData.list_goAML_Trn_Director, objData_Old.list_goAML_Trn_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director_Address", objData.list_goAML_Trn_Director_Address, objData_Old.list_goAML_Trn_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Director_Phone", objData.list_goAML_trn_Director_Phone, objData_Old.list_goAML_trn_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor", objData.list_goAML_Trn_Conductor, objData_Old.list_goAML_Trn_Conductor)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor_Address", objData.list_goAML_Trn_Conductor_Address, objData_Old.list_goAML_Trn_Conductor_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Conductor_Phone", objData.list_goAML_trn_Conductor_Phone, objData_Old.list_goAML_trn_Conductor_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person_Identification", objData.list_goAML_Transaction_Person_Identification, objData_Old.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party", objData.list_goAML_Transaction_Party, objData_Old.list_goAML_Transaction_Party)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Account", objData.list_goAML_Trn_Party_Account, objData_Old.list_goAML_Trn_Party_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_acc_Signatory", objData.list_goAML_Trn_par_acc_Signatory, objData_Old.list_goAML_Trn_par_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_sign_Address", objData.list_goAML_Trn_par_Acc_sign_Address, objData_Old.list_goAML_Trn_par_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_sign_Phone", objData.list_goAML_trn_par_acc_sign_Phone, objData_Old.list_goAML_trn_par_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Entity", objData.list_goAML_Trn_Par_Acc_Entity, objData_Old.list_goAML_Trn_Par_Acc_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_Entity_Address", objData.list_goAML_Trn_par_Acc_Entity_Address, objData_Old.list_goAML_Trn_par_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_Entity_Phone", objData.list_goAML_trn_par_acc_Entity_Phone, objData_Old.list_goAML_trn_par_acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director", objData.list_goAML_Trn_Par_Acc_Ent_Director, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Address", objData.list_goAML_Trn_Par_Acc_Ent_Director_Address, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Phone", objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person", objData.list_goAML_Trn_Party_Person, objData_Old.list_goAML_Trn_Party_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Address", objData.list_goAML_Trn_Party_Person_Address, objData_Old.list_goAML_Trn_Party_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Phone", objData.list_goAML_Trn_Party_Person_Phone, objData_Old.list_goAML_Trn_Party_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity", objData.list_goAML_Trn_Party_Entity, objData_Old.list_goAML_Trn_Party_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Address", objData.list_goAML_Trn_Party_Entity_Address, objData_Old.list_goAML_Trn_Party_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Phone", objData.list_goAML_Trn_Party_Entity_Phone, objData_Old.list_goAML_Trn_Party_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director", objData.list_goAML_Trn_Par_Entity_Director, objData_Old.list_goAML_Trn_Par_Entity_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Address", objData.list_goAML_Trn_Par_Entity_Director_Address, objData_Old.list_goAML_Trn_Par_Entity_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Phone", objData.list_goAML_Trn_Par_Entity_Director_Phone, objData_Old.list_goAML_Trn_Par_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party_Identification", objData.list_goAML_Transaction_Party_Identification, objData_Old.list_goAML_Transaction_Party_Identification)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity(objModule As NawaDAL.Module, objData As GoAML_Activity_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Activity_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLActivityClassByID(objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID)
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Act_ReportPartyType
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim OldPK As Long = 0
                        Dim OldPKSub As Long = 0
                        Dim OldPKSubSubID As Long = 0

                        '1. Account
                        For Each objAccount In objData.list_goAML_Act_Account
                            OldPK = objAccount.PK_goAML_Act_Account_ID
                            objAccount.FK_Report_ID = strReportID
                            objAccount.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objAccount.PK_goAML_Act_Account_ID < 0 Then
                                objDB.Entry(objAccount).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objAccount).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Account Signatory
                            Dim listSignatory = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = OldPK).ToList
                            If listSignatory IsNot Nothing Then
                                For Each objSignatory In listSignatory
                                    OldPKSub = objSignatory.PK_goAML_Act_acc_Signatory_ID
                                    objSignatory.FK_Report_ID = strReportID
                                    objSignatory.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objSignatory.FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objSignatory.PK_goAML_Act_acc_Signatory_ID < 0 Then
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Signatory Address
                                    Dim listAddress = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = OldPKSub).ToList
                                    If listAddress IsNot Nothing Then
                                        For Each objAddress In listAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objAddress.PK_goAML_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Phone
                                    Dim listPhone = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listPhone IsNot Nothing Then
                                        For Each objPhone In listPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objPhone.PK_goAML_act_acc_sign_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If

                            'Account Entity
                            Dim listAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = OldPK).ToList
                            If listAccountEntity IsNot Nothing Then
                                For Each objAccountEntity In listAccountEntity
                                    OldPKSub = objAccountEntity.PK_goAML_Act_Entity_account
                                    objAccountEntity.FK_Report_ID = strReportID
                                    objAccountEntity.FK_Activity_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objAccountEntity.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objAccountEntity.PK_goAML_Act_Entity_account < 0 Then
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Entity Address
                                    Dim listEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityAddress IsNot Nothing Then
                                        For Each objAddress In listEntityAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objAddress.PK_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Phone
                                    Dim listEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityPhone IsNot Nothing Then
                                        For Each objPhone In listEntityPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objPhone.PK_goAML_Act_Acc_Entity_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Director
                                    Dim listAccountEntityDirector = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listAccountEntityDirector IsNot Nothing Then
                                        For Each objDirector In listAccountEntityDirector
                                            OldPKSubSubID = objDirector.PK_Act_Acc_Ent_Director_ID
                                            objDirector.FK_Report_ID = strReportID
                                            objDirector.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objDirector.PK_Act_Acc_Ent_Director_ID < 0 Then
                                                objDB.Entry(objDirector).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()

                                            'Account Entity Director Address
                                            Dim listAddress = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listAddress IsNot Nothing Then
                                                For Each objAddress In listAddress
                                                    objAddress.FK_Report_ID = strReportID
                                                    objAddress.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objAddress.PK_goAML_Act_Acc_Entity_Director_address_ID < 0 Then
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Phone
                                            Dim listPhone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listPhone IsNot Nothing Then
                                                For Each objPhone In listPhone
                                                    objPhone.FK_Report_ID = strReportID
                                                    objPhone.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objPhone.PK_goAML_Act_Acc_Entity_Director_Phone_ID < 0 Then
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Identification
                                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = OldPKSubSubID).ToList
                                            If listIdentification IsNot Nothing Then
                                                For Each objIdentification In listIdentification
                                                    objIdentification.FK_Report_ID = strReportID
                                                    objIdentification.FK_Act_Person_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        '2. Person
                        For Each objPerson In objData.list_goAML_Act_Person
                            OldPK = objPerson.PK_goAML_Act_Person_ID
                            objPerson.FK_Report_ID = strReportID
                            objPerson.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objPerson.PK_goAML_Act_Person_ID < 0 Then
                                objDB.Entry(objPerson).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objPerson).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Person Address
                            Dim listAddress = objData.list_goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.FK_Report_ID = strReportID
                                    objAddress.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objAddress.PK_goAML_Act_Person_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Phone
                            Dim listPhone = objData.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objPhone.PK_goAML_Act_Person_Phone_ID < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Identification
                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Act_Person_ID = OldPK).ToList
                            If listIdentification IsNot Nothing Then
                                For Each objIdentification In listIdentification
                                    objIdentification.FK_Report_ID = strReportID
                                    objIdentification.FK_Act_Person_ID = objPerson.PK_goAML_Act_Person_ID

                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If
                        Next

                        '3. Entity
                        For Each objEntity In objData.list_goAML_Act_Entity
                            OldPK = objEntity.PK_goAML_Act_Entity_ID
                            objEntity.FK_Report_ID = strReportID
                            objEntity.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objEntity.PK_goAML_Act_Entity_ID < 0 Then
                                objDB.Entry(objEntity).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objEntity).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Entity Address
                            Dim listAddress = objData.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.FK_Report_ID = strReportID
                                    objAddress.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objAddress.PK_goAML_Act_Entity_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Phone
                            Dim listPhone = objData.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objPhone.PK_goAML_Act_Entity_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Director
                            Dim listDirector = objData.list_goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listDirector IsNot Nothing Then
                                For Each objDirector In listDirector
                                    OldPKSub = objDirector.PK_goAML_Act_Director_ID
                                    objDirector.FK_Report_ID = strReportID
                                    objDirector.FK_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objDirector.FK_Sender_Information = enumSenderInformation.Entity
                                    objDirector.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objDirector.PK_goAML_Act_Director_ID < 0 Then
                                        objDB.Entry(objDirector).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'EntityDirector Address
                                    Dim listDirectorAddress = objData.list_goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorAddress IsNot Nothing Then
                                        For Each objAddress In listDirectorAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objAddress.PK_goAML_Act_Director_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Phone
                                    Dim listDirectorPhone = objData.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorPhone IsNot Nothing Then
                                        For Each objPhone In listDirectorPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objPhone.PK_goAML_Act_Director_Phone < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            'Create new object for deleted data
                            Dim objDeletedData As New GoAML_Activity_Class

                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Act_Account
                                Dim objCek = objData.list_goAML_Act_Account.Find(Function(x) x.PK_goAML_Act_Account_ID = item_old.PK_goAML_Act_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                                Dim objCek = objData.list_goAML_Act_acc_Signatory.Find(Function(x) x.PK_goAML_Act_acc_Signatory_ID = item_old.PK_goAML_Act_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Address_ID = item_old.PK_goAML_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Phone.Find(Function(x) x.PK_goAML_act_acc_sign_Phone_ID = item_old.PK_goAML_act_acc_sign_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                                Dim objCek = objData.list_goAML_Act_Entity_Account.Find(Function(x) x.PK_goAML_Act_Entity_account = item_old.PK_goAML_Act_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Address.Find(Function(x) x.PK_Act_Acc_Entity_Address_ID = item_old.PK_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Act_Acc_Ent_Director.Find(Function(x) x.PK_Act_Acc_Ent_Director_ID = item_old.PK_Act_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID = item_old.PK_goAML_Act_Acc_Entity_Director_address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Act_Person
                                Dim objCek = objData.list_goAML_Act_Person.Find(Function(x) x.PK_goAML_Act_Person_ID = item_old.PK_goAML_Act_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Act_Person_Address
                                Dim objCek = objData.list_goAML_Act_Person_Address.Find(Function(x) x.PK_goAML_Act_Person_Address_ID = item_old.PK_goAML_Act_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                                Dim objCek = objData.list_goAML_Act_Person_Phone.Find(Function(x) x.PK_goAML_Act_Person_Phone_ID = item_old.PK_goAML_Act_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity
                                Dim objCek = objData.list_goAML_Act_Entity.Find(Function(x) x.PK_goAML_Act_Entity_ID = item_old.PK_goAML_Act_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Entity_Address.Find(Function(x) x.PK_goAML_Act_Entity_Address_ID = item_old.PK_goAML_Act_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Entity_Phone = item_old.PK_goAML_Act_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Director
                                Dim objCek = objData.list_goAML_Act_Director.Find(Function(x) x.PK_goAML_Act_Director_ID = item_old.PK_goAML_Act_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Director_Address
                                Dim objCek = objData.list_goAML_Act_Director_Address.Find(Function(x) x.PK_goAML_Act_Director_Address_ID = item_old.PK_goAML_Act_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Director_Phone.Find(Function(x) x.PK_goAML_Act_Director_Phone = item_old.PK_goAML_Act_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveActivity_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)
            End Using


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity_AuditTrail(objData As GoAML_Activity_Class, objData_Old As GoAML_Activity_Class, objDB As NawaDatadevEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert Then
                NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Act_ReportPartyType)
            ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Act_ReportPartyType, objData_Old.obj_goAML_Act_ReportPartyType)
            End If

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Account", objData.list_goAML_Act_Account, objData_Old.list_goAML_Act_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_acc_Signatory", objData.list_goAML_Act_acc_Signatory, objData_Old.list_goAML_Act_acc_Signatory)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Address", objData.list_goAML_Act_Acc_sign_Address, objData_Old.list_goAML_Act_Acc_sign_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Phone", objData.list_goAML_Act_Acc_sign_Phone, objData_Old.list_goAML_Act_Acc_sign_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Account", objData.list_goAML_Act_Entity_Account, objData_Old.list_goAML_Act_Entity_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Address", objData.list_goAML_Act_Acc_Entity_Address, objData_Old.list_goAML_Act_Acc_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Phone", objData.list_goAML_Act_Acc_Entity_Phone, objData_Old.list_goAML_Act_Acc_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Ent_Director", objData.list_goAML_Act_Acc_Ent_Director, objData_Old.list_goAML_Act_Acc_Ent_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_address", objData.list_goAML_Act_Acc_Entity_Director_address, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_Phone", objData.list_goAML_Act_Acc_Entity_Director_Phone, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person", objData.list_goAML_Act_Person, objData_Old.list_goAML_Act_Person)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Address", objData.list_goAML_Act_Person_Address, objData_Old.list_goAML_Act_Person_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Phone", objData.list_goAML_Act_Person_Phone, objData_Old.list_goAML_Act_Person_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity", objData.list_goAML_Act_Entity, objData_Old.list_goAML_Act_Entity)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Address", objData.list_goAML_Act_Entity_Address, objData_Old.list_goAML_Act_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Phone", objData.list_goAML_Act_Entity_Phone, objData_Old.list_goAML_Act_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director", objData.list_goAML_Act_Director, objData_Old.list_goAML_Act_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Address", objData.list_goAML_Act_Director_Address, objData_Old.list_goAML_Act_Director_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Phone", objData.list_goAML_Act_Director_Phone, objData_Old.list_goAML_Act_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Activity_Person_Identification", objData.list_goAML_Activity_Person_Identification, objData_Old.list_goAML_Activity_Person_Identification)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Function getParentID(dtPK As DataTable, currentPK As String) As Integer
        Try
            Dim dr As DataRow()
            Dim intParentID As Integer = 0

            If dtPK IsNot Nothing Then
                dr = dtPK.Select("pkCurrent=" & currentPK)
                If dr.Length > 0 Then
                    intParentID = dr(0)("pkInserted")
                End If
            End If

            Return intParentID
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
    End Function
End Class

Public Class GoAML_Report_Class

    'GoAML Report
    Public obj_goAML_Report As New NawaDevDAL.goAML_Report

    'Transaction
    Public list_goAML_Transaction As New List(Of NawaDevDAL.goAML_Transaction)

    'Activity
    Public list_goAML_Act_ReportPartyType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)

    'Indicator
    Public list_goAML_Report_Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator)

End Class


Public Class GoAML_Transaction_Class
    'Transaction
    Public obj_goAML_Transaction As New goAML_Transaction

    'Transaction Bi-Party
    Public list_goAML_Transaction_Account As New List(Of NawaDevDAL.goAML_Transaction_Account)
    Public list_goAML_Trn_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
    Public list_goAML_Trn_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
    Public list_goAML_trn_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)

    Public list_goAML_Trn_Entity_account As New List(Of NawaDevDAL.goAML_Trn_Entity_account)
    Public list_goAML_Trn_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
    Public list_goAML_Trn_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)

    Public list_goAML_Transaction_Person As New List(Of NawaDevDAL.goAML_Transaction_Person)
    Public list_goAML_Trn_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
    Public list_goAML_trn_Person_Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)

    Public list_goAML_Transaction_Entity As New List(Of NawaDevDAL.goAML_Transaction_Entity)
    Public list_goAML_Trn_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
    Public list_goAML_Trn_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)

    Public list_goAML_Trn_Director As New List(Of NawaDevDAL.goAML_Trn_Director)
    Public list_goAML_Trn_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
    Public list_goAML_trn_Director_Phone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)

    Public list_goAML_Trn_Conductor As New List(Of NawaDevDAL.goAML_Trn_Conductor)
    Public list_goAML_Trn_Conductor_Address As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
    Public list_goAML_trn_Conductor_Phone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)

    Public list_goAML_Transaction_Person_Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)

    'Transaction Multi Party
    Public list_goAML_Transaction_Party As New List(Of NawaDevDAL.goAML_Transaction_Party)

    Public list_goAML_Trn_Party_Account As New List(Of NawaDevDAL.goAML_Trn_Party_Account)
    Public list_goAML_Trn_par_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
    Public list_goAML_Trn_par_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
    Public list_goAML_trn_par_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)

    Public list_goAML_Trn_Par_Acc_Entity As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)
    Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
    Public list_goAML_trn_par_acc_Entity_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)

    Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

    Public list_goAML_Trn_Party_Person As New List(Of NawaDevDAL.goAML_Trn_Party_Person)
    Public list_goAML_Trn_Party_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
    Public list_goAML_Trn_Party_Person_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)

    Public list_goAML_Trn_Party_Entity As New List(Of NawaDevDAL.goAML_Trn_Party_Entity)
    Public list_goAML_Trn_Party_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
    Public list_goAML_Trn_Party_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)

    Public list_goAML_Trn_Par_Entity_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
    Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)

    Public list_goAML_Transaction_Party_Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
End Class

Public Class GoAML_Activity_Class
    Public obj_goAML_Act_ReportPartyType As New goAML_Act_ReportPartyType

    Public list_goAML_Act_Account As New List(Of NawaDevDAL.goAML_Act_Account)
    Public list_goAML_Act_acc_Signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
    Public list_goAML_Act_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
    Public list_goAML_Act_Acc_sign_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)

    Public list_goAML_Act_Entity_Account As New List(Of NawaDevDAL.goAML_Act_Entity_Account)
    Public list_goAML_Act_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
    Public list_goAML_Act_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)

    Public list_goAML_Act_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
    Public list_goAML_Act_Acc_Entity_Director_address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
    Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)

    Public list_goAML_Act_Person As New List(Of NawaDevDAL.goAML_Act_Person)
    Public list_goAML_Act_Person_Address As New List(Of NawaDevDAL.goAML_Act_Person_Address)
    Public list_goAML_Act_Person_Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)

    Public list_goAML_Act_Entity As New List(Of NawaDevDAL.goAML_Act_Entity)
    Public list_goAML_Act_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
    Public list_goAML_Act_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)

    Public list_goAML_Act_Director As New List(Of NawaDevDAL.goAML_Act_Director)
    Public list_goAML_Act_Director_Address As New List(Of NawaDevDAL.goAML_Act_Director_Address)
    Public list_goAML_Act_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)

    Public list_goAML_Activity_Person_Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
End Class

