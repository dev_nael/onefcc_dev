﻿Imports System.Collections.Generic
Imports GoAMLDAL

Public Class ReportingPersonData
    Public objPerson As New goAML_ODM_Reporting_Person
    Public ObjIdent As New List(Of goAML_Person_Identification)
    Public ObjAddress As New List(Of goAML_Ref_Address)
    Public ObjPhone As New List(Of goAML_Ref_Phone)
    Public ObjEmpAddress As New List(Of goAML_Ref_Address)
    Public ObjEmpPhone As New List(Of goAML_Ref_Phone)
End Class

