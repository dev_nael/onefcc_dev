﻿Public Class GoAML_Grid_Session_Constant
    Public Const SESSION_UNIQUE_NAME As String = "UniqueName"
    Public Const SESSION_FK_REF_DETAIL_OF As String = "FK_REF_DETAIL_OF-"
    Public Const SESSION_CIF As String = "CIF-"
    Public Const SESSION_OBJTEMP_EDIT As String = "objTemp_Edit-"
    Public Const SESSION_OBJTEMP_GOAML_REF As String = "objTemp_goAML_Ref-"
    Public Const SESSION_OBJLIST_GOAML_Ref As String = "objListgoAML_Ref-"
    Public Const SESSION_OBJLIST_GOAML_VW As String = "objListgoAML_vw-"
    Public Const SESSION_IS_VIEW_MODE = "IsViewMode-"
    Public Const SESSION_IS_CUSTOMER = "IsCustomer-"
    Public Const SESSION_IS_DATA_CHANGE = "IsDataChange-"
End Class
