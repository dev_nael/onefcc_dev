﻿Namespace Base.Constant
    Public Class RefDetail
        Shared Property CUSTOMER_DIRECTOR As Integer = 6
        Shared Property WIC_DIRECTOR As Integer = 8
        Shared Property CUSTOMER_RELATED_PERSON As Integer = 31
        Shared Property WIC_RELATED_PERSON As Integer = 31
        Shared Property CUSTOMER_RELATED_PERSON_WORK As Integer = 32
        Shared Property WIC_RELATED_PERSON_WORK As Integer = 32
        Shared Property CUSTOMER_RELATED_ENTITY As Integer = 36
        Shared Property WIC_RELATED_ENTITY As Integer = 36
    End Class
End Namespace

