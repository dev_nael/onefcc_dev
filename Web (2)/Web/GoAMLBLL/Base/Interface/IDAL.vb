﻿Imports GoAMLBLL.goAML_Customer.DataModel
Imports GoAMLBLL.WICDataBLL

Public Interface IDAL(Of T)
    Function Save(objData As T) As Integer
    Function GetData(Where As String) As List(Of T)
    Sub Update(objData As T, Where As String)
    Sub Delete(Where As String)
End Interface
