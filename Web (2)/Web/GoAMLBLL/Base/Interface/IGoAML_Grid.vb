﻿Public Interface IGoAML_Grid(Of T)
    Property UniqueName As String
    Property FK_REF_DETAIL_OF As Integer
    Property CIF As String
    Property objTemp_Edit As T
    Property objTemp_goAML_Ref As T
    Property objListgoAML_Ref As List(Of T)
    Property objListgoAML_vw As List(Of T)
    Property Title As String
    ReadOnly Property SessionPrefix As String
    Property IsViewMode As Boolean
    Property IsDataChange As Boolean

    'Shadows Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Sub btnAdd_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
    Sub btnSave_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
    Sub btnBack_DirectEvent(sender As Object, e As Ext.Net.DirectEventArgs)
    Sub GrdCmd(sender As Object, e As Ext.Net.DirectEventArgs)
    Sub LoadData(id As Long, isEdit As Boolean)
    Sub DeleteRecord(id As Long)
    Sub Clearinput()
    Function IsData_Valid() As Boolean
    Sub Save(Optional isNew As Boolean = True)
    Sub LoadData(data As List(Of T))
End Interface
