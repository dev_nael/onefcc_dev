﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class goAML_WIC_Service

        Private Shared _context As DataContext = New DataContext(AmlModule.WIC)
        Private Shared ModuleID As Integer = AmlModule.WIC

        Public Shared AddressService As AddressService = New AddressService(ModuleID)
        Public Shared DirectorService As DirectorService = New DirectorService(ModuleID)
        Public Shared EmailService As WICEmailService = New WICEmailService(ModuleID)
        Public Shared PEPService As WICPEPService = New WICPEPService(ModuleID)
        Public Shared PersonIdentificationService As PersonIdentificationService = New PersonIdentificationService(ModuleID)
        Public Shared PhoneService As PhoneService = New PhoneService(ModuleID)
        Public Shared SanctionService As WICSanctionService = New WICSanctionService(ModuleID)
        Public Shared EntityIdentificationService As WICEntityIdentificationService = New WICEntityIdentificationService(ModuleID)
        Public Shared UrlService As WICUrlService = New WICUrlService(ModuleID)
        Public Shared RelatedPersonService As WICRelatedPersonService = New WICRelatedPersonService(ModuleID)
        Public Shared RelatedEntityService As WICRelatedEntityService = New WICRelatedEntityService(ModuleID)

        Shared Function GetObjEmailRef(PK As String, objRefList As List(Of DataModel.goAML_Ref_WIC_Email))
            Dim objRetList = New List(Of DataModel.goAML_Ref_WIC_Email)

            ' Save Email
            For Each element In objRefList
                Dim obj_goAML_Ref_WIC_Email = New DataModel.goAML_Ref_WIC_Email
                obj_goAML_Ref_WIC_Email.PK_goAML_Ref_WIC_Email_ID = element.PK_goAML_Ref_WIC_Email_ID
                obj_goAML_Ref_WIC_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref_WIC_Email.WIC_No = PK
                obj_goAML_Ref_WIC_Email.email_address = element.email_address

                objRetList.Add(obj_goAML_Ref_WIC_Email)
            Next

            Return objRetList
        End Function

        Shared Function GetEmailByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Email)
            Dim Where = $"(WIC_No = '{pk}' OR FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Email.GetData(Where)
            Return ret
        End Function

        Shared Function GetPEPByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Person_PEP)
            ' 2023-10-03, Nael: Where clause jadi menggunakan FK_FOR_TABLE_ID bukan WIC_No
            Dim Where = $"(FK_FOR_TABLE_ID = {pk} OR WIC_No = '{pk}') AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Person_PEP.GetData(Where)

            Return ret
        End Function


        Shared Function GetSanctionByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Sanction)
            ' 2023-10-03, Nael: Where clause jadi menggunakan FK_FOR_TABLE_ID bukan WIC_No
            Dim Where = $"(FK_FOR_TABLE_ID = {pk} OR WIC_No = '{pk}') AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Sanction.GetData(Where)

            Return ret
        End Function

        Shared Function GetEntityIdentificationByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Entity_Identification)
            Dim Where = $"WIC_No = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Entity_Identification.GetData(Where)

            Return ret
        End Function

        Shared Function GetUrlByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_URL)
            Dim Where = $"WIC_No = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_URL.GetData(Where)

            Return ret
        End Function

        Shared Function GetAddressByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsWIC As Boolean = False) As List(Of DataModel.goAML_Ref_Address)

            Dim Where = $"FK_To_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsWIC Then
                Where = Where & "AND IsCustomer = 0 "
            End If

            Dim ret = _context.goAML_Ref_Address.GetData(Where)

            Return ret
        End Function

        Shared Function GetPhoneByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsWIC As Boolean = False) As List(Of DataModel.goAML_Ref_Phone)

            Dim Where = $"FK_for_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsWIC Then
                Where = Where & "AND IsCustomer = 0 "
            End If

            Dim ret = _context.goAML_Ref_Phone.GetData(Where)

            Return ret
        End Function

        Shared Function GetIdentificationPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsWIC As Boolean = False) As List(Of DataModel.goAML_Person_Identification)
            'Dim Where = $"FK_Person_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} " '' Edit 21-Sep-2023, Felix.
            Dim Where = $"FK_Person_ID = {pk_parent} AND FK_Person_Type = {fk_ref} "
            If IsWIC Then
                Where = Where & "AND IsCustomer = 0 "
            End If

            Dim ret = _context.goAML_Person_Identification.GetData(Where)

            Return ret
        End Function

        'Shared Function Get()
#Region "Add 21-Sep-2023, Felix. Bikin versi Related Person"
        '' Add 21-Sep-2023, Felix. Untuk Related Person, pakai FK_FOR_TABLE_ID
        Shared Function GetRPEmailByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Email)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Email.GetData(Where)
            Return ret
        End Function

        Shared Function GetRPSanctionByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Sanction)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Sanction.GetData(Where)

            Return ret
        End Function

        Shared Function GetRPPEPByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_WIC_Person_PEP)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Person_PEP.GetData(Where)

            Return ret
        End Function
        '' End 21-Sep-2023
#End Region
    End Class
End Namespace