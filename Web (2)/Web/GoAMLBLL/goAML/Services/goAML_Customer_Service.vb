﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class goAML_Customer_Service

        Private Shared _context As DataContext = New DataContext(AmlModule.Customer)
        Private Shared ModuleID As Integer = AmlModule.Customer

        Public Shared AddressService As AddressService = New AddressService(ModuleID)
        Public Shared DirectorService As DirectorService = New DirectorService(ModuleID)
        Public Shared EmailService As EmailService = New EmailService(ModuleID)
        Public Shared PEPService As PEPService = New PEPService(ModuleID)
        Public Shared PersonIdentificationService As PersonIdentificationService = New PersonIdentificationService(ModuleID)
        Public Shared PhoneService As PhoneService = New PhoneService(ModuleID)
        Public Shared SanctionService As SanctionService = New SanctionService(ModuleID)
        Public Shared EntityIdentificationService As EntityIdentificationService = New EntityIdentificationService(ModuleID)
        Public Shared UrlService As UrlService = New UrlService(ModuleID)
        Public Shared RelatedPersonService As RelatedPersonService = New RelatedPersonService(ModuleID)
        Public Shared RelatedEntityService As RelatedEntityService = New RelatedEntityService(ModuleID)

        ' [START] 2023-11-09, Nael: Add
        Public Shared WICRelatedPersonService As RelatedPersonService = New RelatedPersonService(AmlModule.WIC) ' 2023-11-09, Nael: Menambahkan WIC Related Person Service
        Public Shared WICRelatedEntityService As RelatedEntityService = New RelatedEntityService(AmlModule.WIC) ' 2023-11-14, Nael: Menambahkan WIC Related Entity Service
        Public Shared WICEmailService As WICEmailService = New WICEmailService(AmlModule.WIC)
        Public Shared WICPersonPEPService As WICPersonPEPService = New WICPersonPEPService(AmlModule.WIC)
        Public Shared WICSanctionService As WICSanctionService = New WICSanctionService(AmlModule.WIC)
        Public Shared WICUrlService As WICUrlService = New WICUrlService(AmlModule.WIC)
        ' [END] 2023-11-09, Nael: Add

        ' [START] 2023-11-14, Nael: Add
        Public Shared WICEntityIdentificationService As WICEntityIdentificationService = New WICEntityIdentificationService(AmlModule.WIC)
        ' [END] 2023-11-14, Nael: Add


        Shared Function GetObjEmailRef(PK As String, objRefList As List(Of DataModel.goAML_Ref_Customer_Email))
            Dim objRetList = New List(Of DataModel.goAML_Ref_Customer_Email)

            ' Save Email
            For Each element In objRefList
                Dim obj_goAML_Ref_Customer_Email = New DataModel.goAML_Ref_Customer_Email
                obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref_Customer_Email.CIF = PK
                obj_goAML_Ref_Customer_Email.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                objRetList.Add(obj_goAML_Ref_Customer_Email)
            Next

            Return objRetList
        End Function

        Shared Function GetEmailByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Email)
            Dim Where = $"(CIF = '{pk}' OR FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Email.GetData(Where)
            Return ret
        End Function

        Shared Function GetPEPByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_PEP)
            ' 2023-10-03, Nael: Where clause jadi menggunakan FK_FOR_TABLE_ID bukan CIF
            Dim Where = $"(FK_FOR_TABLE_ID = {pk} OR CIF = '{pk}') AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_PEP.GetData(Where)

            Return ret
        End Function


        Shared Function GetSanctionByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Sanction)
            ' 2023-10-03, Nael: Where clause jadi menggunakan FK_FOR_TABLE_ID bukan CIF
            Dim Where = $"(FK_FOR_TABLE_ID = {pk} OR CIF = '{pk}') AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Sanction.GetData(Where)

            Return ret
        End Function

        Shared Function GetEntityIdentificationByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
            Dim Where = $"(CIF = '{pk}' OR FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Entity_Identification.GetData(Where)

            Return ret
        End Function

        Shared Function GetUrlByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_URL)
            Dim Where = $"(CIF = '{pk}' OR FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_URL.GetData(Where)

            Return ret
        End Function

        Shared Function GetAddressByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Ref_Address)

            Dim Where = $"FK_To_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Ref_Address.GetData(Where)

            Return ret
        End Function

        Shared Function GetPhoneByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Ref_Phone)

            Dim Where = $"FK_for_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Ref_Phone.GetData(Where)

            Return ret
        End Function

        Shared Function GetIdentificationPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Person_Identification)
            'Dim Where = $"FK_Person_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} " '' Edit 21-Sep-2023, Felix.
            Dim Where = $"FK_Person_ID = {pk_parent} AND FK_Person_Type = {fk_ref} "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Person_Identification.GetData(Where)

            Return ret
        End Function

        'Shared Function Get()
#Region "Add 21-Sep-2023, Felix. Bikin versi Related Person"
        '' Add 21-Sep-2023, Felix. Untuk Related Person, pakai FK_FOR_TABLE_ID
        Shared Function GetRPEmailByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Email)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Email.GetData(Where)
            Return ret
        End Function

        Shared Function GetRPSanctionByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Sanction)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Sanction.GetData(Where)

            Return ret
        End Function

        Shared Function GetRPPEPByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_PEP)
            Dim Where = $"FK_FOR_TABLE_ID = '{pk}' AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_PEP.GetData(Where)

            Return ret
        End Function
        '' End 21-Sep-2023
#End Region
    End Class
End Namespace