﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class SanctionService
        Private _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Sanction)
            ' 2023-10-25, Nael: where clause hanya menggunakan FK_For_Table
            'Dim Where = $"CIF = '{pk}' AND FK_REF_DETAIL_OF = {fk} "
            Dim Where = $"(FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_Sanction.GetData(Where)

            Return ret
        End Function
    End Class
End Namespace