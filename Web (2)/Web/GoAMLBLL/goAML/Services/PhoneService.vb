﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class PhoneService
        Private _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Ref_Phone)

            Dim Where = $"FK_for_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Ref_Phone.GetData(Where)

            Return ret
        End Function

        Function GenerateDataTable(listPhone As List(Of GoAMLDAL.goAML_Ref_Phone)) As DataTable
            Dim dtPhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listPhone)
            If listPhone IsNot Nothing OrElse listPhone.Count > 0 Then
                Using objDB As New GoAMLDAL.GoAMLEntities
                    Dim listTipeKontak = objDB.goAML_Ref_Kategori_Kontak.ToList()
                    For Each row In dtPhone.Rows
                        If Not IsDBNull(row("Tph_Contact_Type")) Then
                            Dim objContactType = listTipeKontak.Find(Function(x) x.Kode.Equals(row("Tph_Contact_Type")))
                            If objContactType IsNot Nothing Then
                                row("Tph_Contact_Type") = objContactType.Kode & " - " & objContactType.Keterangan
                            End If
                        End If
                    Next
                End Using
            End If
            Return dtPhone
        End Function

    End Class
End Namespace

