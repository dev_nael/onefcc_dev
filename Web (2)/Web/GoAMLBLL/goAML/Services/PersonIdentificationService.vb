﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class PersonIdentificationService
        Private _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Person_Identification)
            '2023-10-11, Nael: ganti jadi pake FK_Person_Type
            Dim Where = $"FK_Person_ID = {pk_parent} AND (FK_Person_Type = {fk_ref}) "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Person_Identification.GetData(Where)

            Return ret
        End Function

        ' 2023-10-05, Nael: Menambahkan fungsi Generate data table dari model goAMl_Person_Identification
        ' Kepake buat ngisi grid identification
        Function GenerateDataTable(listId As List(Of GoAMLDAL.goAML_Person_Identification)) As DataTable
            Dim dtId As DataTable = NawaBLL.Common.CopyGenericToDataTable(listId)
            If listId IsNot Nothing And listId.Count > 0 Then
                Using objDB As New GoAMLDAL.GoAMLEntities
                    Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                    For Each row In dtId.Rows
                        If Not IsDBNull(row("Issued_Country")) Then
                            Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("Issued_Country")))
                            If objCountry IsNot Nothing Then
                                row("Issued_Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
            End If
            Return dtId
        End Function

    End Class
End Namespace

