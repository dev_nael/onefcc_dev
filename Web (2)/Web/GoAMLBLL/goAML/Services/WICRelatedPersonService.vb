﻿
Imports System.Runtime.Remoting.Contexts
Imports GoAMLBLL.Base.Constant
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.Helper.CommonHelper

Namespace goAML.Services
    Public Class WICRelatedPersonService
        Private _context As DataContext
        Private _module As AmlModule

        Public Sub New(amlModule As AmlModule)
            _module = amlModule
            _context = New DataContext(amlModule)
        End Sub
        Function GetByWIC_No(WIC_No As String) As List(Of DataModel.goAML_Ref_WIC_Related_Person)

            Dim Where = $"WIC_No = {WrapSqlVariable(WIC_No)} "

            Dim ret = _context.goAML_Ref_WIC_Related_Person.GetData(Where)

            Return ret
        End Function

        Function GetCompleteByWIC_No(WIC_No As String) As List(Of DataModel.goAML_Ref_WIC_Related_Person)

            Dim fk = 0
            Dim fk_work = 0
            Dim is_customer = False

            If _module = AmlModule.Customer Then
                fk = RefDetail.CUSTOMER_RELATED_PERSON
                fk_work = RefDetail.CUSTOMER_RELATED_PERSON_WORK
                is_customer = True
            End If

            If _module = AmlModule.WIC Then
                fk = RefDetail.WIC_RELATED_PERSON
                fk_work = RefDetail.WIC_RELATED_PERSON_WORK
                is_customer = False
            End If

            Dim Where = $"WIC_No = { WrapSqlVariable(WIC_No) } "

            Dim item_list = _context.goAML_Ref_WIC_Related_Person.GetData(Where)

            For Each item As DataModel.goAML_Ref_WIC_Related_Person In item_list
                item.ObjList_GoAML_Person_Identification = goAML_WIC_Service.PersonIdentificationService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk, is_customer)
                item.ObjList_GoAML_Ref_Phone = goAML_WIC_Service.PhoneService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk, is_customer)
                item.ObjList_GoAML_Ref_Address = goAML_WIC_Service.AddressService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk, is_customer)
                item.ObjList_GoAML_Ref_Phone_Work = goAML_WIC_Service.PhoneService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk_work, is_customer)
                item.ObjList_GoAML_Ref_Address_Work = goAML_WIC_Service.AddressService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk_work, is_customer)
                item.ObjList_GoAML_Ref_WIC_Email = goAML_WIC_Service.EmailService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk)
                item.ObjList_GoAML_Ref_WIC_Sanction = goAML_WIC_Service.SanctionService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk)
                item.ObjList_GoAML_Ref_WIC_Person_PEP = goAML_WIC_Service.PEPService.GetByPkFk(item.PK_goAML_Ref_WIC_Related_Person_ID, fk)
            Next

            Return item_list
        End Function

        Function SaveChild(pk As Integer, objData As WICDataBLL.goAML_Ref_WIC_Related_Person) As Boolean
            Try
                Dim pk_column = "WIC_No"

                If _module = AmlModule.WIC Then
                    pk_column = "WIC_No"
                End If

                If objData.ObjList_GoAML_Person_Identification IsNot Nothing Then
                    _context.goAML_Person_Identification.Delete($"FK_Person_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Person_Identification
                        'ObjGrid.FK
                        ObjGrid.FK_Person_ID = pk
                        ObjGrid.FK_Person_Type = RefDetail.WIC_RELATED_PERSON ' 2023-10-12, Nael
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Person_Identification.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Address IsNot Nothing Then
                    _context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Address
                        ObjGrid.FK_To_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Ref_Address.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Phone IsNot Nothing Then
                    _context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Phone
                        ObjGrid.FK_for_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Ref_Phone.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Address_Work IsNot Nothing Then
                    _context.goAML_Ref_Address.Delete($"FK_To_Table_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON_WORK}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Address_Work
                        ObjGrid.FK_To_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_PERSON_WORK
                        _context.goAML_Ref_Address.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Phone_Work IsNot Nothing Then
                    _context.goAML_Ref_Phone.Delete($"FK_for_Table_ID = {pk} AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON_WORK}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Phone_Work
                        ObjGrid.FK_for_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_PERSON_WORK
                        _context.goAML_Ref_Phone.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_WIC_Email IsNot Nothing Then
                    _context.goAML_Ref_WIC_Email.Delete($"{pk_column} = '{pk}' AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_WIC_Email
                        ObjGrid.WIC_No = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Ref_WIC_Email.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_WIC_Sanction IsNot Nothing Then
                    _context.goAML_Ref_WIC_Sanction.Delete($"{pk_column} = '{pk}' AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_WIC_Sanction
                        ObjGrid.WIC_No = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Ref_WIC_Sanction.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_WIC_PEP IsNot Nothing Then
                    _context.goAML_Ref_WIC_PEP.Delete($"{pk_column} = '{pk}' AND FK_REF_DETAIL_OF = {RefDetail.WIC_RELATED_PERSON}")
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_WIC_PEP
                        ObjGrid.WIC_No = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_PERSON
                        _context.goAML_Ref_WIC_Person_PEP.Save(ObjGrid)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
            Return True
        End Function
    End Class

End Namespace
