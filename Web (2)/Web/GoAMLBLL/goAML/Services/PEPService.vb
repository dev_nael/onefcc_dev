﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class PEPService
        Private Shared _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_PEP)
            ' 2023-10-25, Nael: where clause hanya menggunakan FK_For_Table
            'Dim Where = $"CIF = '{pk}' AND FK_REF_DETAIL_OF = {fk} "
            Dim Where = $"(FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_Customer_PEP.GetData(Where)
            Return ret
        End Function

        ' 2023-10-05, Nael: Menambahkan fungsi ini untuk generate datatable dari model goAMl_Ref_Customer_PEP
        ' Kepake buat ngisi grid Identity
        Function GenerateDataTable(pepList As List(Of GoAMLDAL.goAML_Ref_Customer_PEP)) As DataTable
            Dim dtPEP As DataTable = NawaBLL.Common.CopyGenericToDataTable(pepList)
            If pepList IsNot Nothing Then
                ' 2023-10-04, Nael: Menambahkan detail untuk kolom PEP_COUNTRY
                Using objDB As New GoAMLDAL.GoAMLEntities
                    Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                    For Each row In dtPEP.Rows
                        If Not IsDBNull(row("PEP_COUNTRY")) Then
                            Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("PEP_COUNTRY")))
                            If objCountry IsNot Nothing Then
                                row("PEP_COUNTRY") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                End Using
            End If
            Return dtPEP
        End Function

    End Class

End Namespace
