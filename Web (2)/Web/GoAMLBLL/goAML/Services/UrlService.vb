﻿Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Enum

Public Class UrlService
    Private Shared _context As DataContext
    Private _module As AmlModule

    Public Sub New(amlModule As AmlModule)
        _module = amlModule
        _context = New DataContext(amlModule)
    End Sub

    Function GetByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_URL)
        Dim Where = $"(CIF = '{pk}' OR FK_FOR_TABLE_ID = {pk}) AND FK_REF_DETAIL_OF = {fk} "

        Dim ret = _context.goAML_Ref_Customer_URL.GetData(Where)

        Return ret
    End Function
End Class
