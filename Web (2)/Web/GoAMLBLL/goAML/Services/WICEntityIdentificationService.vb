﻿Imports GoAMLBLL.goAML
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.Helper.CommonHelper
Imports System.Data

Public Class WICEntityIdentificationService
    Private _context As DataContext
    Private _module As AmlModule

    Public Sub New(amlModule As AmlModule)
        _module = amlModule
        _context = New DataContext(amlModule)
    End Sub

    Function Save(objData As DataModel.goAML_Ref_Customer_Entity_Identification) As Integer
        Try


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Dim Where = $"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {fk}"

        Dim ret = _context.goAML_Ref_WIC_Entity_Identification.GetData(Where)

        Return ret
    End Function

    Function GenerateDataTable(listId As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Identification)) As DataTable
        Dim dtId As DataTable = NawaBLL.Common.CopyGenericToDataTable(listId)
        If listId IsNot Nothing AndAlso listId.Count > 0 Then
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                For Each row In dtId.Rows
                    If Not IsDBNull(row("ISSUE_COUNTRY")) Then
                        Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("ISSUE_COUNTRY")))
                        If objCountry IsNot Nothing Then
                            row("ISSUE_COUNTRY") = objCountry.Kode & " - " & objCountry.Keterangan
                        End If
                    End If
                Next
            End Using
        End If
        Return dtId
    End Function

    Function GenerateDataTable(listId As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Entity_Identification)) As DataTable
        Dim dtId As DataTable = NawaBLL.Common.CopyGenericToDataTable(listId)
        If listId IsNot Nothing AndAlso listId.Count > 0 Then
            Using objDB As New GoAMLDAL.GoAMLEntities
                Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                For Each row In dtId.Rows
                    If Not IsDBNull(row("ISSUE_COUNTRY")) Then
                        Dim objCountry = listCountry.Find(Function(x) x.Kode.Equals(row("ISSUE_COUNTRY")))
                        If objCountry IsNot Nothing Then
                            row("ISSUE_COUNTRY") = objCountry.Kode & " - " & objCountry.Keterangan
                        End If
                    End If
                Next
            End Using
        End If
        Return dtId
    End Function

End Class
