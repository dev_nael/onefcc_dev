﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class AddressService
        Private Shared _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk_parent As Integer, fk_ref As Integer, Optional IsCustomer As Boolean = False) As List(Of DataModel.goAML_Ref_Address)

            Dim Where = $"FK_To_Table_ID = {pk_parent} AND FK_REF_DETAIL_OF = {fk_ref} "
            If IsCustomer Then
                Where = Where & "AND IsCustomer = 1 "
            End If

            Dim ret = _context.goAML_Ref_Address.GetData(Where)

            Return ret
        End Function

        ' 2023-10-05, Nael: Menambahkan fungsi helper untuk generate datatable dari model goAML_Ref_Address
        ' Kepake buat ngisi grid address
        Function GenerateDataTable(addressList As List(Of GoAMLDAL.goAML_Ref_Address)) As DataTable
            Dim dtAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(addressList)
            If addressList IsNot Nothing AndAlso addressList.Count > 0 Then
                ' 2023-10-04, Nael: Menambahkan detail untuk kolom Country dan kolom Address Type
                Using objDB As New GoAMLDAL.GoAMLEntities
                    dtAddress.Columns.Add(New DataColumn("Country", GetType(String)))
                    dtAddress.Columns.Add(New DataColumn("AddressType", GetType(String)))
                    Dim listAddressType = objDB.goAML_Ref_Kategori_Kontak.ToList()
                    Dim listCountry = objDB.goAML_Ref_Nama_Negara.ToList()
                    For Each row In dtAddress.Rows
                        If Not IsDBNull(row("Address_Type")) Then
                            Dim objAddressType = listAddressType.FirstOrDefault(Function(x) x.Kode = row("Address_Type"))
                            If objAddressType IsNot Nothing Then
                                row("Address_Type") = objAddressType.Kode & " - " & objAddressType.Keterangan
                            End If
                        End If

                        If Not IsDBNull(row("Country_Code")) Then
                            Dim objCountry = listCountry.FirstOrDefault(Function(x) x.Kode.Equals(row("Country_Code")))
                            If objCountry IsNot Nothing Then
                                row("Country") = objCountry.Kode & " - " & objCountry.Keterangan
                            End If
                        End If
                    Next
                    '=====================================================================================
                End Using
            End If
            Return dtAddress
        End Function

    End Class
End Namespace

