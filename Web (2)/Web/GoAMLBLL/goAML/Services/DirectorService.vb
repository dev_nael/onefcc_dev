﻿Imports GoAMLBLL.Base.Constant
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.Helper.CommonHelper
Imports goAML_Customer_Service
Imports GoAMLBLL.goAML.DataModel

Namespace goAML.Services
    Public Class DirectorService
        Private Shared _context As DataContext
        Private _module As AmlModule

        Public Sub New(amlModule As AmlModule)
            _module = amlModule
            _context = New DataContext(amlModule)
        End Sub

        Function GetByCIF(CIF As String) As List(Of DataModel.goAML_Ref_Entity_Director)

            Dim Where = $"FK_Entity_ID = {WrapSqlVariable(CIF)} "

            Dim ret = _context.goAML_Ref_Entity_Director.GetData(Where)

            Return ret
        End Function

        Function GetCompleteByCIF(CIF As String) As List(Of DataModel.goAML_Ref_Entity_Director)

            Dim fk = 0

            If _module = AmlModule.Customer Then
                fk = RefDetail.CUSTOMER_DIRECTOR
            End If

            Dim Where = $"FK_Entity_ID = {WrapSqlVariable(CIF)} "

            Dim dir_list = _context.goAML_Ref_Entity_Director.GetData(Where)

            For Each dir As DataModel.goAML_Ref_Entity_Director In dir_list
                dir.ObjList_GoAML_Person_Identification = goAML_Customer_Service.PersonIdentificationService.GetByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                dir.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.PhoneService.GetByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                '        Public Property ObjList_GoAML_Ref_Address As List(Of goAML_Ref_Address)
                'Public Property ObjList_GoAML_Ref_Phone_Work As List(Of goAML_Ref_Phone)
                'Public Property ObjList_GoAML_Ref_Address_Work As List(Of goAML_Ref_Address)
                'Public Property ObjList_GoAML_Ref_Customer_Email As List(Of goAML_Ref_Customer_Email)
                'Public Property ObjList_GoAML_Ref_Customer_Sanction As List(Of goAML_Ref_Customer_Sanction)
                'Public Property ObjList_GoAML_Ref_Customer_PEP As List(Of goAML_Ref_Customer_PEP)
                'dir.ObjList_GoAML_Ref_Phone = goAML_Customer_Service.PhoneService.GetPhoneByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                'dir.ObjList_GoAML_Ref_Address = goAML_Customer_Service.AddressService.GetAddressByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                'dir.ObjList_GoAML_Ref_Phone_Work
                dir.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.EmailService.GetByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                dir.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.SanctionService.GetByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
                dir.ObjList_GoAML_Ref_Customer_PEP = goAML_Customer_Service.PEPService.GetByPkFk(dir.PK_goAML_Ref_Customer_Entity_Director_ID, fk)
            Next


            Return dir_list
        End Function

    End Class
End Namespace


