﻿Imports GoAMLBLL.goAML.Enum

Namespace goAML.Services
    Public Class WICEmailService
        Private Shared _context As DataContext

        Public Sub New(amlModule As AmlModule)
            _context = New DataContext(amlModule)
        End Sub

        Function GetByPkFk(pk As Integer, fk As Integer) As List(Of DataModel.goAML_Ref_Customer_Email)
            ' 2023-10-10, Nael: Menambahkan kondisi untuk ngecek juga ke FK_FOR_TABLE_ID
            Dim Where = $"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = {fk} "

            Dim ret = _context.goAML_Ref_WIC_Email.GetData(Where)
            Return ret
        End Function

    End Class
End Namespace

