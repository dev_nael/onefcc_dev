﻿
Imports System.Runtime.Remoting.Contexts
Imports GoAMLBLL.Base.Constant
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.goAML.Enum
Imports GoAMLBLL.Helper.CommonHelper

Namespace goAML.Services
    Public Class RelatedEntityService
        Private Shared _context As DataContext
        Private _module As AmlModule

        Public Sub New(amlModule As AmlModule)
            _module = amlModule
            _context = New DataContext(amlModule)
        End Sub
        Function GetByCIF(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Related_Entities)

            Dim Where = $"CIF = {WrapSqlVariable(CIF)} "

            Dim ret = _context.goAML_Ref_Customer_Related_Entities.GetData(Where)

            Return ret
        End Function

        Function GetCompleteByCIF(CIF As String) As List(Of DataModel.goAML_Ref_Customer_Related_Entities)

            Dim fk = 0

            If _module = AmlModule.Customer Then
                fk = RefDetail.CUSTOMER_RELATED_ENTITY
            End If

            Dim Where = $"CIF = {WrapSqlVariable(CIF)} "

            Dim item_list = _context.goAML_Ref_Customer_Related_Entities.GetData(Where)

            For Each item As DataModel.goAML_Ref_Customer_Related_Entities In item_list
                item.ObjList_GoAML_Ref_Customer_Phone = goAML_Customer_Service.PhoneService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
                item.ObjList_GoAML_Ref_Customer_Address = goAML_Customer_Service.AddressService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
                item.ObjList_GoAML_Ref_Customer_Email = goAML_Customer_Service.EmailService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
                item.ObjList_GoAML_Ref_Customer_Entity_Identification = goAML_Customer_Service.EntityIdentificationService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
                item.ObjList_GoAML_Ref_Customer_URL = goAML_Customer_Service.UrlService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
                item.ObjList_GoAML_Ref_Customer_Sanction = goAML_Customer_Service.SanctionService.GetByPkFk(item.PK_goAML_Ref_Customer_Related_Entities_ID, fk)
            Next


            Return item_list
        End Function

        Function SaveChild(pk As Integer, objData As WICDataBLL.goAML_Ref_WIC_Related_Entity) As Boolean
            Try
                Dim where_re = $"CIF = '{pk}' AND FK_REF_DETAIL_OF = 36"

                If _module = AmlModule.WIC Then
                    where_re = $"WIC_No = '{pk}' AND FK_REF_DETAIL_OF = 36"
                End If

                If objData.ObjList_GoAML_Ref_Customer_Email IsNot Nothing Then

                    If _module = AmlModule.WIC Then
                        _context.goAML_Ref_WIC_Email.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    Else
                        _context.goAML_Ref_Customer_Email.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    End If

                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Email
                        ObjGrid.CIF = Nothing
                        ObjGrid.FK_FOR_TABLE_ID = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_ENTITY

                        If _module = AmlModule.WIC Then
                            _context.goAML_Ref_WIC_Email.Save(ObjGrid)
                        Else
                            _context.goAML_Ref_Customer_Email.Save(ObjGrid)
                        End If

                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_Sanction IsNot Nothing Then
                    If _module = AmlModule.WIC Then
                        _context.goAML_Ref_WIC_Sanction.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    Else
                        _context.goAML_Ref_Customer_Sanction.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    End If

                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Sanction
                        ObjGrid.CIF = Nothing
                        ObjGrid.FK_FOR_TABLE_ID = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_ENTITY
                        If _module = AmlModule.WIC Then
                            _context.goAML_Ref_WIC_Sanction.Save(ObjGrid)
                        Else
                            _context.goAML_Ref_Customer_Sanction.Save(ObjGrid)
                        End If
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_Entity_Identification IsNot Nothing Then
                    If _module = AmlModule.WIC Then
                        _context.goAML_REf_WIC_Entity_Identification.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    Else
                        _context.goAML_Ref_Customer_Entity_Identification.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    End If

                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Entity_Identification
                        ObjGrid.CIF = Nothing
                        ObjGrid.FK_FOR_TABLE_ID = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_ENTITY
                        If _module = AmlModule.WIC Then
                            _context.goAML_REf_WIC_Entity_Identification.Save(ObjGrid)
                        Else
                            _context.goAML_Ref_Customer_Entity_Identification.Save(ObjGrid)
                        End If
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_URL IsNot Nothing Then
                    If _module = AmlModule.WIC Then
                        _context.goAML_Ref_WIC_URL.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    Else
                        _context.goAML_Ref_Customer_URL.Delete($"FK_FOR_TABLE_ID = {pk} AND FK_REF_DETAIL_OF = 36")
                    End If
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_URL
                        ObjGrid.CIF = Nothing
                        ObjGrid.FK_FOR_TABLE_ID = pk
                        ObjGrid.FK_REF_DETAIL_OF = RefDetail.WIC_RELATED_ENTITY
                        If _module = AmlModule.WIC Then
                            _context.goAML_Ref_WIC_URL.Save(ObjGrid)
                        Else
                            _context.goAML_Ref_Customer_URL.Save(ObjGrid)
                        End If
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_Phone IsNot Nothing Then
                    where_re = $"FK_for_Table_ID = '{pk}' AND FK_REF_DETAIL_OF = 36"
                    _context.goAML_Ref_Phone.Delete(where_re)
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Phone
                        ObjGrid.FK_for_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_ENTITY
                        _context.goAML_Ref_Phone.Save(ObjGrid)
                    Next
                End If

                If objData.ObjList_GoAML_Ref_Customer_Address IsNot Nothing Then
                    where_re = $"FK_To_Table_ID = '{pk}' AND FK_REF_DETAIL_OF = 36"
                    _context.goAML_Ref_Address.Delete(where_re)
                    For Each ObjGrid In objData.ObjList_GoAML_Ref_Customer_Address
                        ObjGrid.FK_To_Table_ID = pk
                        ObjGrid.FK_Ref_Detail_Of = RefDetail.WIC_RELATED_ENTITY
                        _context.goAML_Ref_Address.Save(ObjGrid)
                    Next
                End If

            Catch ex As Exception
                Throw ex
            End Try
            Return True
        End Function
    End Class
End Namespace

