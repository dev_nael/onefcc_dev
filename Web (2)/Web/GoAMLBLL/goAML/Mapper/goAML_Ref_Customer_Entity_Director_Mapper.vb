﻿Imports Microsoft.VisualBasic

Public NotInheritable Class goAML_Ref_Customer_Entity_Director_Mapper
    Shared Function Serialize(data As GoAMLDAL.goAML_Ref_Customer_Entity_Director) As GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director
        Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director

        objReturn.PK_goAML_Ref_Customer_Entity_Director_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID
        objReturn.Role = data.Role
        objReturn.FK_Entity_ID = data.FK_Entity_ID
        objReturn.Title = data.Title
        objReturn.First_name = data.First_name
        objReturn.[Alias] = data.[Alias]
        objReturn.Birth_Place = data.Birth_Place
        objReturn.BirthDate = data.BirthDate
        objReturn.Comments = data.Comments
        objReturn.Deceased = data.Deceased
        objReturn.Deceased_Date = data.Deceased_Date
        objReturn.Email = data.Email
        objReturn.Employer_Name = data.Employer_Name
        objReturn.Middle_Name = data.Middle_Name
        objReturn.Prefix = data.Prefix
        objReturn.Last_Name = data.Last_Name
        objReturn.Mothers_Name = data.Mothers_Name
        objReturn.SSN = data.SSN
        objReturn.Gender = data.Gender
        objReturn.ID_Number = data.ID_Number
        objReturn.Nationality1 = data.Nationality1
        objReturn.Nationality2 = data.Nationality3
        objReturn.Nationality3 = data.Nationality3
        objReturn.Occupation = data.Occupation
        objReturn.Passport_Country = data.Passport_Country
        objReturn.Passport_Number = data.Passport_Number
        objReturn.Residence = data.Residence
        objReturn.Source_of_Wealth = data.Source_of_Wealth
        objReturn.Tax_Number = data.Tax_Number
        objReturn.Tax_Reg_Number = data.Tax_Reg_Number
        objReturn.Email2 = data.Email2
        objReturn.Email3 = data.Email3
        objReturn.Email4 = data.Email4
        objReturn.Email5 = data.Email5

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director)

        If list_data IsNot Nothing Then
            For Each data As GoAMLDAL.goAML_Ref_Customer_Entity_Director In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(data As GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director) As GoAMLDAL.goAML_Ref_Customer_Entity_Director
        Dim objReturn = New GoAMLDAL.goAML_Ref_Customer_Entity_Director

        objReturn.PK_goAML_Ref_Customer_Entity_Director_ID = data.PK_goAML_Ref_Customer_Entity_Director_ID
        objReturn.Role = data.Role
        objReturn.FK_Entity_ID = data.FK_Entity_ID
        objReturn.Title = data.Title
        objReturn.First_name = data.First_name
        objReturn.[Alias] = data.[Alias]
        objReturn.Birth_Place = data.Birth_Place
        objReturn.BirthDate = data.BirthDate
        objReturn.Comments = data.Comments
        objReturn.Deceased = data.Deceased
        objReturn.Deceased_Date = data.Deceased_Date
        objReturn.Email = data.Email
        objReturn.Employer_Name = data.Employer_Name
        objReturn.Middle_Name = data.Middle_Name
        objReturn.Prefix = data.Prefix
        objReturn.Last_Name = data.Last_Name
        objReturn.Mothers_Name = data.Mothers_Name
        objReturn.SSN = data.SSN
        objReturn.Gender = data.Gender
        objReturn.ID_Number = data.ID_Number
        objReturn.Nationality1 = data.Nationality1
        objReturn.Nationality2 = data.Nationality3
        objReturn.Nationality3 = data.Nationality3
        objReturn.Occupation = data.Occupation
        objReturn.Passport_Country = data.Passport_Country
        objReturn.Passport_Number = data.Passport_Number
        objReturn.Residence = data.Residence
        objReturn.Source_of_Wealth = data.Source_of_Wealth
        objReturn.Tax_Number = data.Tax_Number
        objReturn.Tax_Reg_Number = data.Tax_Reg_Number
        objReturn.Email2 = data.Email2
        objReturn.Email3 = data.Email3
        objReturn.Email4 = data.Email4
        objReturn.Email5 = data.Email5

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director)) As List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)
        Dim objReturnList = New List(Of GoAMLDAL.goAML_Ref_Customer_Entity_Director)

        If list_data IsNot Nothing Then

            For Each data As GoAMLBLL.goAML.DataModel.goAML_Ref_Entity_Director In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function
End Class
