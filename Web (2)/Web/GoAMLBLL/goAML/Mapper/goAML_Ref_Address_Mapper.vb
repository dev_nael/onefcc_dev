﻿Imports Microsoft.VisualBasic

Public Class goAML_Ref_Address_Mapper
    Shared Function Serialize(data As GoAMLDAL.goAML_Ref_Address) As GoAMLBLL.goAML.DataModel.goAML_Ref_Address
        Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Ref_Address

        objReturn.PK_Customer_Address_ID = data.PK_Customer_Address_ID
        objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
        objReturn.FK_To_Table_ID = data.FK_To_Table_ID
        objReturn.Address_Type = data.Address_Type
        objReturn.Address = data.Address
        objReturn.Town = data.Town
        objReturn.City = data.City
        objReturn.Zip = data.Zip
        objReturn.Country_Code = data.Country_Code
        objReturn.State = data.State
        objReturn.Comments = data.Comments
        objReturn.IsCustomer = False
        objReturn.Active = data.Active
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLDAL.goAML_Ref_Address)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)

        If list_data IsNot Nothing Then
            For Each data As GoAMLDAL.goAML_Ref_Address In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(data As GoAMLBLL.goAML.DataModel.goAML_Ref_Address) As GoAMLDAL.goAML_Ref_Address
        Dim objReturn = New GoAMLDAL.goAML_Ref_Address

        objReturn.PK_Customer_Address_ID = data.PK_Customer_Address_ID
        objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
        objReturn.FK_To_Table_ID = data.FK_To_Table_ID
        objReturn.Address_Type = data.Address_Type
        objReturn.Address = data.Address
        objReturn.Town = data.Town
        objReturn.City = data.City
        objReturn.Zip = data.Zip
        objReturn.Country_Code = data.Country_Code
        objReturn.State = data.State
        objReturn.Comments = data.Comments
        'objReturn.IsCustomer = False
        objReturn.Active = data.Active
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)) As List(Of GoAMLDAL.goAML_Ref_Address)
        Dim objReturnList = New List(Of GoAMLDAL.goAML_Ref_Address)

        If list_data IsNot Nothing Then
            For Each data As GoAMLBLL.goAML.DataModel.goAML_Ref_Address In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(pk As String, list_data As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Address)

        If list_data IsNot Nothing Then
            For Each data As GoAMLBLL.goAML.DataModel.goAML_Ref_Address In list_data
                Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Ref_Address

                objReturn.PK_Customer_Address_ID = data.PK_Customer_Address_ID
                objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
                objReturn.FK_To_Table_ID = pk
                objReturn.Address_Type = data.Address_Type
                objReturn.Address = data.Address
                objReturn.Town = data.Town
                objReturn.City = data.City
                objReturn.Zip = data.Zip
                objReturn.Country_Code = data.Country_Code
                objReturn.State = data.State
                objReturn.Comments = data.Comments
                objReturn.IsCustomer = data.IsCustomer
                objReturn.Active = data.Active
                objReturn.CreatedBy = data.CreatedBy
                objReturn.LastUpdateBy = data.LastUpdateBy
                objReturn.ApprovedBy = data.ApprovedBy
                objReturn.CreatedDate = data.CreatedDate
                objReturn.LastUpdateDate = data.LastUpdateDate
                objReturn.ApprovedDate = data.ApprovedDate
                objReturn.Alternateby = data.Alternateby

                objReturnList.Add(objReturn)
            Next
        End If

        Return objReturnList
    End Function
End Class
