﻿Imports Microsoft.VisualBasic

Public Class goAML_Ref_Customer_URL_Mapper
    Shared Function Serialize(fk As String, listData As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_URL)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_URL)
        Dim objListRet = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_URL)

        If listData IsNot Nothing Then
            For Each element In listData
                Dim obj_goAML_Ref = New GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_URL
                obj_goAML_Ref.PK_goAML_Ref_Customer_URL_ID = element.PK_goAML_Ref_Customer_URL_ID
                obj_goAML_Ref.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref.CIF = element.CIF
                obj_goAML_Ref.URL = element.URL

                'Additional
                obj_goAML_Ref.Active = element.Active
                obj_goAML_Ref.CreatedBy = element.CreatedBy
                obj_goAML_Ref.LastUpdateBy = element.LastUpdateBy
                obj_goAML_Ref.ApprovedBy = element.ApprovedBy
                obj_goAML_Ref.CreatedDate = element.CreatedDate
                obj_goAML_Ref.LastUpdateDate = element.LastUpdateDate
                obj_goAML_Ref.ApprovedDate = element.ApprovedDate
                obj_goAML_Ref.Alternateby = element.Alternateby

                objListRet.Add(obj_goAML_Ref)
            Next
        End If

        Return objListRet
    End Function
End Class
