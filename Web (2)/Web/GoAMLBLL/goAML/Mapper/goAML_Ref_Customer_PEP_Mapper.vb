﻿Public Class goAML_Ref_Customer_PEP_Mapper
    Shared Function Serialize(fk As String, listData As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_PEP)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_PEP)
        Dim objListRet = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_PEP)

        If listData IsNot Nothing Then
            For Each element In listData
                Dim obj_goAML_Ref_Customer_Email = New GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_PEP
                obj_goAML_Ref_Customer_Email.PK_goAML_Ref_Customer_PEP_ID = element.PK_goAML_Ref_Customer_PEP_ID
                obj_goAML_Ref_Customer_Email.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref_Customer_Email.CIF = element.CIF ' 2023-10-25, Nael
                obj_goAML_Ref_Customer_Email.PEP_COUNTRY = element.PEP_COUNTRY
                obj_goAML_Ref_Customer_Email.FUNCTION_NAME = element.FUNCTION_NAME
                obj_goAML_Ref_Customer_Email.FUNCTION_DESCRIPTION = element.FUNCTION_DESCRIPTION
                obj_goAML_Ref_Customer_Email.PEP_DATE_RANGE_VALID_FROM = element.PEP_DATE_RANGE_VALID_FROM
                obj_goAML_Ref_Customer_Email.PEP_DATE_RANGE_IS_APPROX_FROM_DATE = element.PEP_DATE_RANGE_IS_APPROX_FROM_DATE
                obj_goAML_Ref_Customer_Email.PEP_DATE_RANGE_VALID_TO = element.PEP_DATE_RANGE_VALID_TO
                obj_goAML_Ref_Customer_Email.PEP_DATE_RANGE_IS_APPROX_TO_DATE = element.PEP_DATE_RANGE_IS_APPROX_TO_DATE
                obj_goAML_Ref_Customer_Email.COMMENTS = element.COMMENTS

                'Additional
                obj_goAML_Ref_Customer_Email.Active = element.Active
                obj_goAML_Ref_Customer_Email.CreatedBy = element.CreatedBy
                obj_goAML_Ref_Customer_Email.LastUpdateBy = element.LastUpdateBy
                obj_goAML_Ref_Customer_Email.ApprovedBy = element.ApprovedBy
                obj_goAML_Ref_Customer_Email.CreatedDate = element.CreatedDate
                obj_goAML_Ref_Customer_Email.LastUpdateDate = element.LastUpdateDate
                obj_goAML_Ref_Customer_Email.ApprovedDate = element.ApprovedDate
                obj_goAML_Ref_Customer_Email.Alternateby = element.Alternateby

                objListRet.Add(obj_goAML_Ref_Customer_Email)
            Next
        End If

        Return objListRet
    End Function
End Class
