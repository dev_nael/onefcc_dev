﻿Imports Microsoft.VisualBasic

Public Class goAML_Ref_Customer_Sanction_Mapper
    Shared Function Serialize(fk As String, listData As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Sanction)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Sanction)
        Dim objListRet = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Sanction)

        If listData IsNot Nothing Then
            For Each element In listData
                Dim obj_goAML_Ref = New GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Sanction
                obj_goAML_Ref.PK_goAML_Ref_Customer_Sanction_ID = element.PK_goAML_Ref_Customer_Sanction_ID
                obj_goAML_Ref.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref.CIF = element.CIF ' 2023-10-25, Nael
                obj_goAML_Ref.PROVIDER = element.PROVIDER
                obj_goAML_Ref.SANCTION_LIST_NAME = element.SANCTION_LIST_NAME
                obj_goAML_Ref.MATCH_CRITERIA = element.MATCH_CRITERIA
                obj_goAML_Ref.LINK_TO_SOURCE = element.LINK_TO_SOURCE
                obj_goAML_Ref.SANCTION_LIST_ATTRIBUTES = element.SANCTION_LIST_ATTRIBUTES
                obj_goAML_Ref.SANCTION_LIST_DATE_RANGE_VALID_FROM = element.SANCTION_LIST_DATE_RANGE_VALID_FROM
                obj_goAML_Ref.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = element.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE
                obj_goAML_Ref.SANCTION_LIST_DATE_RANGE_VALID_TO = element.SANCTION_LIST_DATE_RANGE_VALID_TO
                obj_goAML_Ref.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = element.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE
                obj_goAML_Ref.COMMENTS = element.COMMENTS

                'Additional
                obj_goAML_Ref.Active = element.Active
                obj_goAML_Ref.CreatedBy = element.CreatedBy
                obj_goAML_Ref.LastUpdateBy = element.LastUpdateBy
                obj_goAML_Ref.ApprovedBy = element.ApprovedBy
                obj_goAML_Ref.CreatedDate = element.CreatedDate
                obj_goAML_Ref.LastUpdateDate = element.LastUpdateDate
                obj_goAML_Ref.ApprovedDate = element.ApprovedDate
                obj_goAML_Ref.Alternateby = element.Alternateby

                objListRet.Add(obj_goAML_Ref)
            Next
        End If

        Return objListRet
    End Function
End Class
