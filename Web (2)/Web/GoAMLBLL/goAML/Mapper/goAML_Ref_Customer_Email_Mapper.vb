﻿Imports Microsoft.VisualBasic

Public Class goAML_Ref_Customer_Email_Mapper
    Shared Function Serialize(pk As String, listData As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Email)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Email)
        Dim objListRet = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Email)

        If listData IsNot Nothing Then
            For Each element In listData
                Dim obj_goAML_Ref = New GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Email
                obj_goAML_Ref.PK_goAML_Ref_Customer_Email_ID = element.PK_goAML_Ref_Customer_Email_ID
                obj_goAML_Ref.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref.CIF = element.CIF ' 2023-10-25, Nael
                obj_goAML_Ref.EMAIL_ADDRESS = element.EMAIL_ADDRESS

                objListRet.Add(obj_goAML_Ref)
            Next
        End If

        Return objListRet
    End Function

End Class
