﻿Imports Microsoft.VisualBasic
Imports GoAMLBLL.goAML

Public NotInheritable Class goAML_Ref_Phone_Mapper
    Shared Function Serialize(data As GoAMLDAL.goAML_Ref_Phone) As GoAMLBLL.goAML.DataModel.goAML_Ref_Phone
        Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Ref_Phone

        objReturn.PK_goAML_Ref_Phone = data.PK_goAML_Ref_Phone
        objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
        objReturn.FK_for_Table_ID = data.FK_for_Table_ID
        objReturn.Tph_Contact_Type = data.Tph_Contact_Type
        objReturn.Tph_Communication_Type = data.Tph_Communication_Type
        objReturn.tph_country_prefix = data.tph_country_prefix
        objReturn.tph_number = data.tph_number
        objReturn.tph_extension = data.tph_extension
        objReturn.comments = data.comments
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby
        objReturn.IsCustomer = False

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLDAL.goAML_Ref_Phone)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Phone)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Phone)

        If list_data IsNot Nothing Then
            For Each data As GoAMLDAL.goAML_Ref_Phone In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(data As DataModel.goAML_Ref_Phone) As GoAMLDAL.goAML_Ref_Phone
        Dim objReturn = New GoAMLDAL.goAML_Ref_Phone

        objReturn.PK_goAML_Ref_Phone = data.PK_goAML_Ref_Phone
        objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
        objReturn.FK_for_Table_ID = data.FK_for_Table_ID
        objReturn.Tph_Contact_Type = data.Tph_Contact_Type
        objReturn.Tph_Communication_Type = data.Tph_Communication_Type
        objReturn.tph_country_prefix = data.tph_country_prefix
        objReturn.tph_number = data.tph_number
        objReturn.tph_extension = data.tph_extension
        objReturn.comments = data.comments
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby
        'objReturn.IsCustomer = False

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of DataModel.goAML_Ref_Phone)) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Dim objReturnList = New List(Of GoAMLDAL.goAML_Ref_Phone)

        If list_data IsNot Nothing Then

            For Each data As DataModel.goAML_Ref_Phone In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(pk As String, list_data As List(Of DataModel.goAML_Ref_Phone)) As List(Of DataModel.goAML_Ref_Phone)
        Dim objReturnList = New List(Of DataModel.goAML_Ref_Phone)

        If list_data IsNot Nothing Then

            For Each data As DataModel.goAML_Ref_Phone In list_data
                Dim objReturn = New DataModel.goAML_Ref_Phone

                objReturn.PK_goAML_Ref_Phone = data.PK_goAML_Ref_Phone
                objReturn.FK_Ref_Detail_Of = data.FK_Ref_Detail_Of
                objReturn.FK_for_Table_ID = pk
                objReturn.Tph_Contact_Type = data.Tph_Contact_Type
                objReturn.Tph_Communication_Type = data.Tph_Communication_Type
                objReturn.tph_country_prefix = data.tph_country_prefix
                objReturn.tph_number = data.tph_number
                objReturn.tph_extension = data.tph_extension
                objReturn.comments = data.comments
                objReturn.CreatedBy = data.CreatedBy
                objReturn.LastUpdateBy = data.LastUpdateBy
                objReturn.ApprovedBy = data.ApprovedBy
                objReturn.CreatedDate = data.CreatedDate
                objReturn.LastUpdateDate = data.LastUpdateDate
                objReturn.ApprovedDate = data.ApprovedDate
                objReturn.Alternateby = data.Alternateby
                objReturn.IsCustomer = data.IsCustomer

                objReturnList.Add(objReturn)
            Next
        End If

        Return objReturnList
    End Function
End Class
