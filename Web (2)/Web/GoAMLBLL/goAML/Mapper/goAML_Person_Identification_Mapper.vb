﻿Imports Microsoft.VisualBasic

Public NotInheritable Class goAML_Person_Identification_Mapper
    Shared Function Serialize(data As GoAMLDAL.goAML_Person_Identification) As GoAMLBLL.goAML.DataModel.goAML_Person_Identification
        Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Person_Identification

        objReturn.PK_Person_Identification_ID = data.PK_Person_Identification_ID
        objReturn.isReportingPerson = data.isReportingPerson
        objReturn.FK_Person_ID = data.FK_Person_ID
        objReturn.Active = data.Active
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby
        objReturn.Type = data.Type
        objReturn.Number = data.Number
        objReturn.Issue_Date = data.Issue_Date
        objReturn.Expiry_Date = data.Expiry_Date
        objReturn.Issued_By = data.Issued_By
        objReturn.Issued_Country = data.Issued_Country
        objReturn.Identification_Comment = data.Identification_Comment
        objReturn.FK_Person_Type = data.FK_Person_Type
        objReturn.IsCustomer = False

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLDAL.goAML_Person_Identification)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)

        If list_data IsNot Nothing Then

            For Each data As GoAMLDAL.goAML_Person_Identification In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    Shared Function Serialize(data As GoAMLBLL.goAML.DataModel.goAML_Person_Identification) As GoAMLDAL.goAML_Person_Identification
        Dim objReturn = New GoAMLDAL.goAML_Person_Identification

        objReturn.PK_Person_Identification_ID = data.PK_Person_Identification_ID
        objReturn.isReportingPerson = data.isReportingPerson
        objReturn.FK_Person_ID = data.FK_Person_ID
        objReturn.Active = data.Active
        objReturn.CreatedBy = data.CreatedBy
        objReturn.LastUpdateBy = data.LastUpdateBy
        objReturn.ApprovedBy = data.ApprovedBy
        objReturn.CreatedDate = data.CreatedDate
        objReturn.LastUpdateDate = data.LastUpdateDate
        objReturn.ApprovedDate = data.ApprovedDate
        objReturn.Alternateby = data.Alternateby
        objReturn.Type = data.Type
        objReturn.Number = data.Number
        objReturn.Issue_Date = data.Issue_Date
        objReturn.Expiry_Date = data.Expiry_Date
        objReturn.Issued_By = data.Issued_By
        objReturn.Issued_Country = data.Issued_Country
        objReturn.Identification_Comment = data.Identification_Comment
        objReturn.FK_Person_Type = data.FK_Person_Type
        'objReturn.IsCustomer = False

        Return objReturn
    End Function

    Shared Function Serialize(list_data As List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)) As List(Of GoAMLDAL.goAML_Person_Identification)
        Dim objReturnList = New List(Of GoAMLDAL.goAML_Person_Identification)

        If list_data IsNot Nothing Then

            For Each data As GoAMLBLL.goAML.DataModel.goAML_Person_Identification In list_data
                Dim data_mapped = Serialize(data)

                objReturnList.Add(data_mapped)
            Next
        End If

        Return objReturnList
    End Function

    'Shared Function Serialize(data As GoAMLBLL.goAML_Customer.DataModel.goAML_Person_Identification) As GoAMLBLL.goAML_Customer.DataModel.goAML_Person_Identification
    '    Dim objReturn = New GoAMLBLL.goAML_Customer.DataModel.goAML_Person_Identification

    '    objReturn.PK_Person_Identification_ID = data.PK_Person_Identification_ID
    '    objReturn.isReportingPerson = data.isReportingPerson
    '    objReturn.FK_Person_ID = data.FK_Person_ID
    '    objReturn.Active = data.Active
    '    objReturn.CreatedBy = data.CreatedBy
    '    objReturn.LastUpdateBy = data.LastUpdateBy
    '    objReturn.ApprovedBy = data.ApprovedBy
    '    objReturn.CreatedDate = data.CreatedDate
    '    objReturn.LastUpdateDate = data.LastUpdateDate
    '    objReturn.ApprovedDate = data.ApprovedDate
    '    objReturn.Alternateby = data.Alternateby
    '    objReturn.Type = data.Type
    '    objReturn.Number = data.Number
    '    objReturn.Issue_Date = data.Issue_Date
    '    objReturn.Expiry_Date = data.Expiry_Date
    '    objReturn.Issued_By = data.Issued_By
    '    objReturn.Issued_Country = data.Issued_Country
    '    objReturn.Identification_Comment = data.Identification_Comment
    '    objReturn.FK_Person_Type = data.FK_Person_Type
    '    objReturn.IsCustomer = False

    '    Return objReturn
    'End Function

    Shared Function Serialize(pk As String, list_data As List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)
        Dim objReturnList = New List(Of GoAMLBLL.goAML.DataModel.goAML_Person_Identification)

        If list_data IsNot Nothing Then
            For Each data As GoAMLBLL.goAML.DataModel.goAML_Person_Identification In list_data
                Dim objReturn = New GoAMLBLL.goAML.DataModel.goAML_Person_Identification

                objReturn.PK_Person_Identification_ID = data.PK_Person_Identification_ID
                objReturn.isReportingPerson = data.isReportingPerson
                objReturn.FK_Person_ID = pk
                objReturn.Active = data.Active
                objReturn.CreatedBy = data.CreatedBy
                objReturn.LastUpdateBy = data.LastUpdateBy
                objReturn.ApprovedBy = data.ApprovedBy
                objReturn.CreatedDate = data.CreatedDate
                objReturn.LastUpdateDate = data.LastUpdateDate
                objReturn.ApprovedDate = data.ApprovedDate
                objReturn.Alternateby = data.Alternateby
                objReturn.Type = data.Type
                objReturn.Number = data.Number
                objReturn.Issue_Date = data.Issue_Date
                objReturn.Expiry_Date = data.Expiry_Date
                objReturn.Issued_By = data.Issued_By
                objReturn.Issued_Country = data.Issued_Country
                objReturn.Identification_Comment = data.Identification_Comment
                objReturn.FK_Person_Type = data.FK_Person_Type
                objReturn.FK_REF_DETAIL_OF = data.FK_REF_DETAIL_OF
                objReturn.IsCustomer = data.IsCustomer

                objReturnList.Add(objReturn)
            Next
        End If

        Return objReturnList
    End Function

    ' 2023-10-07, Nael: Buat mapping ke Vw_Person_Identification
    Shared Function CreateView(obj As GoAMLDAL.goAML_Person_Identification) As GoAMLDAL.Vw_Person_Identification
        Dim vwIdentification As New GoAMLDAL.Vw_Person_Identification()
        Using objDb As New GoAMLDAL.GoAMLEntities
            vwIdentification.PK_Person_Identification_ID = obj.PK_Person_Identification_ID
            vwIdentification.FK_Person_ID = obj.FK_Person_ID
            vwIdentification.Type = obj.Type
            vwIdentification.isReportingPerson = obj.isReportingPerson
            vwIdentification.FK_Person_ID = obj.FK_Person_ID
            vwIdentification.Type_Document = objDb.goAML_Ref_Jenis_Dokumen_Identitas.FirstOrDefault(Function(x) x.Kode.Equals(obj.Type)).Keterangan
            vwIdentification.Number = obj.Number
            vwIdentification.Issue_Date = obj.Issue_Date
            vwIdentification.Issued_By = obj.Issued_By
            vwIdentification.Expiry_Date = obj.Expiry_Date
            vwIdentification.Country = obj.Issued_Country
            vwIdentification.Identification_Comment = obj.Identification_Comment
        End Using
        Return vwIdentification
    End Function

End Class
