﻿Imports Microsoft.VisualBasic

Public Class goAML_Ref_Customer_Entity_Identification_Mapper
    Shared Function Serialize(fk As String, listData As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Entity_Identification)) As List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Entity_Identification)
        Dim objListRet = New List(Of GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Entity_Identification)

        If listData IsNot Nothing Then
            For Each element In listData
                Dim obj_goAML_Ref = New GoAMLBLL.goAML.DataModel.goAML_Ref_Customer_Entity_Identification
                obj_goAML_Ref.PK_goAML_Ref_Customer_Entity_Identification_ID = element.PK_goAML_Ref_Customer_Entity_Identification_ID
                obj_goAML_Ref.FK_REF_DETAIL_OF = element.FK_REF_DETAIL_OF
                obj_goAML_Ref.CIF = fk
                obj_goAML_Ref.TYPE = element.TYPE
                obj_goAML_Ref.NUMBER = element.NUMBER
                obj_goAML_Ref.ISSUE_DATE = element.ISSUE_DATE
                obj_goAML_Ref.EXPIRY_DATE = element.EXPIRY_DATE
                obj_goAML_Ref.ISSUED_BY = element.ISSUED_BY
                obj_goAML_Ref.ISSUE_COUNTRY = element.ISSUE_COUNTRY
                obj_goAML_Ref.COMMENTS = element.COMMENTS

                'Additional
                obj_goAML_Ref.Active = element.Active
                obj_goAML_Ref.CreatedBy = element.CreatedBy
                obj_goAML_Ref.LastUpdateBy = element.LastUpdateBy
                obj_goAML_Ref.ApprovedBy = element.ApprovedBy
                obj_goAML_Ref.CreatedDate = element.CreatedDate
                obj_goAML_Ref.LastUpdateDate = element.LastUpdateDate
                obj_goAML_Ref.ApprovedDate = element.ApprovedDate
                obj_goAML_Ref.Alternateby = element.Alternateby

                objListRet.Add(obj_goAML_Ref)
            Next
        End If

        Return objListRet
    End Function

End Class
