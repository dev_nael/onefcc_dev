﻿Imports GoAMLBLL.goAML.DataAccess
Imports GoAMLBLL.goAML.Enum

Public Class DataContext
    Public Property goAML_Ref_Customer_Email As goAML_Ref_Customer_Email_DAL
    Public Property goAML_Ref_Customer_Sanction As goAML_Ref_Customer_Sanction_DAL
    Public Property goAML_Ref_Customer_PEP As goAML_Ref_Customer_PEP_DAL
    Public Property goAML_Person_Identification As goAML_Person_Identification_DAL
    Public Property goAML_Ref_Address As goAML_Ref_Address_DAL
    Public Property goAML_Ref_Phone As goAML_Ref_Phone_DAL
    Public Property goAML_Ref_Customer_Entity_Identification As goAML_Ref_Customer_Entity_Identification_DAL
    Public Property goAML_Ref_Customer_URL As goAML_Ref_Customer_URL_DAL
    Public Property goAML_Ref_Customer_Related_Person As goAML_Ref_Customer_Related_Person_DAL
    Public Property goAML_Ref_Customer_Related_Entities As goAML_Ref_Customer_Related_Entities_DAL
    Public Property goAML_Ref_Entity_Director As goAML_Ref_Customer_Entity_Director_DAL

    ' [START] 2023-11-09, Nael: Menambahkan data access khusus untuk WIC karena tabel email, pep, sanction dan url untuk customer dan WIC itu berbeda
    Public Property goAML_Ref_WIC_Email As goAML_Ref_WIC_Email_DAL
    Public Property goAML_Ref_WIC_Sanction As goAML_Ref_WIC_Sanction_DAL
    Public Property goAML_Ref_WIC_Person_PEP As goAML_Ref_WIC_Person_PEP_DAL

    Public Property goAML_Ref_WIC_URL As goAML_Ref_WIC_URL_DAL
    ' [END] 2023-11-09, Nael: Menambahkan data access khusus untuk WIC karena tabel email, pep, sanction dan url untuk customer dan WIC itu berbeda

    ' [START] 2023-11-14, Nael: Add
    Public Property goAML_Ref_WIC_Entity_Identification As goAML_Ref_WIC_Entity_Identification_DAL
    ' [END] 2023-11-14, Nael: Add

    ' [START] 2023-11-15, Nael: Add
    Public Property goAML_Ref_WIC_Related_Entity As goAML_Ref_WIC_Related_Entities_DAL
    Public Property goAML_Ref_WIC_Related_Person As goAML_Ref_WIC_Related_Person_DAL
    ' [END] 2023-11-15, Nael: Add


    Public Sub New(AmlModule As AmlModule)
        goAML_Ref_Customer_Email = New goAML_Ref_Customer_Email_DAL
        goAML_Ref_Customer_Sanction = New goAML_Ref_Customer_Sanction_DAL
        goAML_Ref_Customer_PEP = New goAML_Ref_Customer_PEP_DAL
        goAML_Person_Identification = New goAML_Person_Identification_DAL
        goAML_Ref_Address = New goAML_Ref_Address_DAL
        goAML_Ref_Phone = New goAML_Ref_Phone_DAL
        goAML_Ref_Customer_Entity_Identification = New goAML_Ref_Customer_Entity_Identification_DAL
        goAML_Ref_Customer_URL = New goAML_Ref_Customer_URL_DAL
        goAML_Ref_Customer_Related_Person = New goAML_Ref_Customer_Related_Person_DAL
        goAML_Ref_Customer_Related_Entities = New goAML_Ref_Customer_Related_Entities_DAL
        goAML_Ref_Entity_Director = New goAML_Ref_Customer_Entity_Director_DAL
        goAML_Ref_WIC_Email = New goAML_Ref_WIC_Email_DAL
        goAML_Ref_WIC_Sanction = New goAML_Ref_WIC_Sanction_DAL
        goAML_Ref_WIC_Person_PEP = New goAML_Ref_WIC_Person_PEP_DAL
        goAML_Ref_WIC_URL = New goAML_Ref_WIC_URL_DAL
        goAML_Ref_WIC_Entity_Identification = New goAML_Ref_WIC_Entity_Identification_DAL
        goAML_Ref_WIC_Related_Entity = New goAML_Ref_WIC_Related_Entities_DAL
        goAML_Ref_WIC_Related_Person = New goAML_Ref_WIC_Related_Person_DAL
    End Sub
End Class
