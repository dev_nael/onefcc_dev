﻿Imports GoAMLBLL.goAML.DataModel

Namespace goAML.DataAccess
    Public Class goAML_Ref_WIC_Person_PEP_DAL
        Implements IDAL(Of goAML_Ref_Customer_PEP)

        Public Sub Update(objData As goAML_Ref_Customer_PEP, Where As String) Implements IDAL(Of goAML_Ref_Customer_PEP).Update
            Try
                Dim strQuery As String = ""

                strQuery = $"update goAML_Ref_WIC_Person_PEP"
                strQuery += $" set"
                strQuery += $" WIC_No =  {Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $" ,PEP_COUNTRY = {Helper.CommonHelper.WrapSqlVariable(objData.PEP_COUNTRY)}"
                strQuery += $" ,FUNCTION_NAME = {Helper.CommonHelper.WrapSqlVariable(objData.FUNCTION_NAME)}"
                strQuery += $" ,FUNCTION_DESCRIPTION = {Helper.CommonHelper.WrapSqlVariable(objData.FUNCTION_DESCRIPTION)}"
                'strQuery += $" ,PEP_DATE_RANGE_VALID_FROM = '{objData.PEP_DATE_RANGE_VALID_FROM}'"
                'strQuery += $" ,PEP_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.PEP_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $" ,PEP_DATE_RANGE_VALID_TO = '{objData.PEP_DATE_RANGE_VALID_TO}'"
                'strQuery += $" ,PEP_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.PEP_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $" ,COMMENTS = {Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)}"
                strQuery += $" where {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_PEP).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_WIC_Person_PEP WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Customer_PEP) As Integer Implements IDAL(Of goAML_Ref_Customer_PEP).Save
            Try
                Dim strQuery As String = ""

                strQuery = $"INSERT INTO [dbo].[goAML_Ref_WIC_Person_PEP]"
                strQuery += $"           ([WIC_No]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[PEP_COUNTRY]"
                strQuery += $"           ,[FUNCTION_NAME]"
                strQuery += $"           ,[FUNCTION_DESCRIPTION]"
                'strQuery += $"           ,[PEP_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[PEP_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[PEP_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[PEP_DATE_RANGE_IS_APPROX_TO_DATE]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ({IIf(objData.CIF Is Nothing, "NULL", $"'{objData.CIF}'")}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.PEP_COUNTRY)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FUNCTION_NAME)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FUNCTION_DESCRIPTION)}"
                'strQuery += $"           ,'{objData.PEP_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.PEP_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.PEP_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.PEP_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_FOR_TABLE_ID)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_PEP) Implements IDAL(Of goAML_Ref_Customer_PEP).GetData
            Try
                'Dim cekQuery As String = $"SELECT * FROM goAML_Ref_Customer_PEP WHERE {Where}"
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_WIC_Person_PEP WHERE {Where}")
                Dim listReturn As New List(Of goAML_Ref_Customer_PEP)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Ref_Customer_PEP
                    If Not IsDBNull(data("PK_goAML_Ref_WIC_PEP_ID")) Then
                        objReturn.PK_goAML_Ref_Customer_PEP_ID = data("PK_goAML_Ref_WIC_PEP_ID")
                    End If
                    '' Edit 21-Sep-2023, Felix.
                    'objReturn.WIC_No = data("WIC_No")
                    If Not IsDBNull(data("WIC_No")) Then
                            objReturn.CIF = data("WIC_No")
                        End If
                        If Not IsDBNull(data("FK_FOR_TABLE_ID")) Then
                        objReturn.FK_FOR_TABLE_ID = data("FK_FOR_TABLE_ID")
                    End If
                    '' End 21-Sep-2023
                    If Not data.IsNull("PEP_COUNTRY") Then
                        objReturn.PEP_COUNTRY = data("PEP_COUNTRY")
                    End If
                    If Not data.IsNull("FUNCTION_NAME") Then
                        objReturn.FUNCTION_NAME = data("FUNCTION_NAME")
                    End If
                    If Not data.IsNull("FUNCTION_DESCRIPTION") Then
                        objReturn.FUNCTION_DESCRIPTION = data("FUNCTION_DESCRIPTION")
                    End If
                    'objReturn.PEP_DATE_RANGE_VALID_FROM = data("PEP_DATE_RANGE_VALID_FROM")
                    'objReturn.PEP_DATE_RANGE_IS_APPROX_FROM_DATE = data("PEP_DATE_RANGE_IS_APPROX_FROM_DATE")
                    'objReturn.PEP_DATE_RANGE_VALID_TO = data("PEP_DATE_RANGE_VALID_TO")
                    'objReturn.PEP_DATE_RANGE_IS_APPROX_TO_DATE = data("PEP_DATE_RANGE_IS_APPROX_TO_DATE")
                    If Not data.IsNull("COMMENTS") Then
                        objReturn.COMMENTS = data("COMMENTS")
                    End If

                    listReturn.Add(objReturn)
                Next

                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace

