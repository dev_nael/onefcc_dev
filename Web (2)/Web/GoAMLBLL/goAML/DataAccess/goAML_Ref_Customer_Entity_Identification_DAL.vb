﻿Imports GoAMLBLL.goAML.DataModel

Namespace goAML.DataAccess
    Public Class goAML_Ref_Customer_Entity_Identification_DAL
        Implements IDAL(Of goAML_Ref_Customer_Entity_Identification)

        Private Property _moduleId As String
        Public Property ModuleID As String
            Get
                Return _moduleId
            End Get
            Set(value As String)
                _moduleId = value
            End Set
        End Property

        Public ReadOnly Property Table As String
            Get
                Return "[dbo].goAML_Ref_Customer_Entity_Identification"
            End Get
        End Property

        Public Sub Update(objData As goAML_Ref_Customer_Entity_Identification, Where As String) Implements IDAL(Of goAML_Ref_Customer_Entity_Identification).Update
            Try
                Dim strQuery As String = ""

                strQuery += $"update {Table}"
                strQuery += $" set"
                strQuery += $" CIF =  '{Helper.CommonHelper.WrapSqlVariable(objData.CIF)}'"
                strQuery += $" ,TYPE = '{Helper.CommonHelper.WrapSqlVariable(objData.TYPE)}'"
                strQuery += $" ,NUMBER = {Helper.CommonHelper.WrapSqlVariable(objData.NUMBER)}"
                strQuery += $" ,ISSUE_DATE = {Helper.CommonHelper.WrapSqlVariable(objData.ISSUE_DATE)}"
                strQuery += $" ,EXPIRY_DATE = {Helper.CommonHelper.WrapSqlVariable(objData.EXPIRY_DATE)}"
                strQuery += $" ,ISSUED_BY = {Helper.CommonHelper.WrapSqlVariable(objData.ISSUED_BY)}'"
                strQuery += $" ,ISSUE_COUNTRY = {Helper.CommonHelper.WrapSqlVariable(objData.ISSUE_COUNTRY)}"
                strQuery += $" ,COMMENTS = {Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)}"
                strQuery += $" where {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_Entity_Identification).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM {Table} WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Customer_Entity_Identification) As Integer Implements IDAL(Of goAML_Ref_Customer_Entity_Identification).Save
            Try
                Dim strQuery As String = ""

                strQuery += $"INSERT INTO {Table}"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[TYPE]"
                strQuery += $"           ,[NUMBER]"
                strQuery += $"           ,[ISSUE_DATE]"
                strQuery += $"           ,[EXPIRY_DATE]"
                strQuery += $"           ,[ISSUED_BY]"
                strQuery += $"           ,[ISSUE_COUNTRY]"
                strQuery += $"           ,[COMMENTS]"
                strQuery += $"           ,[FK_FOR_TABLE_ID])"
                strQuery += $"     VALUES"
                strQuery += $"           ({IIf(objData.CIF Is Nothing, "NULL", Helper.CommonHelper.WrapSqlVariable(objData.CIF))}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.TYPE)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.NUMBER)}"
                ' 2023-10-07, Nael: NULL jika issue date atau expiry date = Nothing
                strQuery += $"           ,{IIf(objData.ISSUE_DATE IsNot Nothing, Helper.CommonHelper.WrapSqlVariable(objData.ISSUE_DATE), "NULL")}"
                strQuery += $"           ,{IIf(objData.EXPIRY_DATE IsNot Nothing, Helper.CommonHelper.WrapSqlVariable(objData.EXPIRY_DATE), "NULL")}"

                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.ISSUED_BY)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.ISSUE_COUNTRY)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_FOR_TABLE_ID)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_Entity_Identification) Implements IDAL(Of goAML_Ref_Customer_Entity_Identification).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM {Table} WHERE {Where}")
                Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Entity_Identification)

                For Each Data As DataRow In dataDT.Rows
                    Dim objReturn = New DataModel.goAML_Ref_Customer_Entity_Identification
                    If Not Data.IsNull("PK_goAML_Ref_Customer_Entity_Identification_ID") Then
                        objReturn.PK_goAML_Ref_Customer_Entity_Identification_ID = Data("PK_goAML_Ref_Customer_Entity_Identification_ID")
                    End If
                    If Not Data.IsNull("CIF") Then
                        objReturn.CIF = Data("CIF")
                    End If
                    If Not Data.IsNull("FK_REF_DETAIL_OF") Then
                        objReturn.FK_REF_DETAIL_OF = Data("FK_REF_DETAIL_OF")
                    End If
                    If Not Data.IsNull("TYPE") Then
                        objReturn.TYPE = Data("TYPE")
                    End If
                    If Not Data.IsNull("NUMBER") Then
                        objReturn.NUMBER = Data("NUMBER")
                    End If
                    If Not Data.IsNull("ISSUE_DATE") Then
                        objReturn.ISSUE_DATE = Data("ISSUE_DATE")
                    End If
                    If Not Data.IsNull("EXPIRY_DATE") Then
                        objReturn.EXPIRY_DATE = Data("EXPIRY_DATE")
                    End If
                    If Not Data.IsNull("ISSUED_BY") Then
                        objReturn.ISSUED_BY = Data("ISSUED_BY")
                    End If
                    If Not Data.IsNull("ISSUE_COUNTRY") Then
                        objReturn.ISSUE_COUNTRY = Data("ISSUE_COUNTRY")
                    End If
                    If Not Data.IsNull("COMMENTS") Then
                        objReturn.COMMENTS = Data("COMMENTS")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

