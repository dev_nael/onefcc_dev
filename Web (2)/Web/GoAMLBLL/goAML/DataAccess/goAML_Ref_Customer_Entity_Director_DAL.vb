﻿Imports NawaBLL.CodeEffects
Imports System.Security.Policy
Imports GoAMLBLL.goAML.DataModel
Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Information
Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Math
Imports GoAMLBLL.Helper.CommonHelper
Imports GoAMLBLL.goAML

Public Class goAML_Ref_Customer_Entity_Director_DAL
    Implements IDAL(Of goAML_Ref_Entity_Director)
    Private Property _moduleId As String
    Public Property ModuleID As String
        Get
            Return _moduleId
        End Get
        Set(value As String)
            _moduleId = value
        End Set
    End Property

    Public ReadOnly Property Table As String
        Get
            Return "[dbo].goAML_Ref_Customer_Entity_Director"
        End Get
    End Property

    Public Sub Update(objData As goAML_Ref_Entity_Director, Where As String) Implements IDAL(Of goAML_Ref_Entity_Director).Update
        Try
            Dim strQuery As String = ""

            strQuery = $"update {Table}"
            strQuery += $" set"
            strQuery += $" PK_goAML_Ref_Customer_Entity_Director_ID = {WrapSqlVariable(objData.PK_goAML_Ref_Customer_Entity_Director_ID)}"
            strQuery += $" ,Role = {WrapSqlVariable(objData.Role)}"
            strQuery += $" ,FK_Entity_ID = {WrapSqlVariable(objData.FK_Entity_ID)}"
            strQuery += $" ,Title = {WrapSqlVariable(objData.Title)}"
            strQuery += $" ,First_name = {WrapSqlVariable(objData.First_name)}"
            strQuery += $" ,Alias = {WrapSqlVariable(objData.Alias)}"
            strQuery += $" ,Birth_Place = {WrapSqlVariable(objData.Birth_Place)}"
            strQuery += $" ,BirthDate = {WrapSqlVariable(objData.BirthDate)}"
            strQuery += $" ,Comments = {WrapSqlVariable(objData.Comments)}"
            strQuery += $" ,Deceased = {WrapSqlVariable(objData.Deceased)}"
            strQuery += $" ,Deceased_Date = {WrapSqlVariable(objData.Deceased_Date)}"
            strQuery += $" ,Email = {WrapSqlVariable(objData.Email)}"
            strQuery += $" ,Employer_Name = {WrapSqlVariable(objData.Employer_Name)}"
            strQuery += $" ,Middle_Name = {WrapSqlVariable(objData.Middle_Name)}"
            strQuery += $" ,Prefix = {WrapSqlVariable(objData.Prefix)}"
            strQuery += $" ,Last_Name = {WrapSqlVariable(objData.Last_Name)}"
            strQuery += $" ,Mothers_Name = {WrapSqlVariable(objData.Mothers_Name)}"
            strQuery += $" ,SSN = {WrapSqlVariable(objData.SSN)}"
            strQuery += $" ,Gender = {WrapSqlVariable(objData.Gender)}"
            strQuery += $" ,ID_Number = {WrapSqlVariable(objData.ID_Number)}"
            strQuery += $" ,Nationality1 = {WrapSqlVariable(objData.Nationality1)}"
            strQuery += $" ,Nationality2 = {WrapSqlVariable(objData.Nationality2)}"
            strQuery += $" ,Nationality3 = {WrapSqlVariable(objData.Nationality3)}"
            strQuery += $" ,Occupation = {WrapSqlVariable(objData.Occupation)}"
            strQuery += $" ,Passport_Country = {WrapSqlVariable(objData.Passport_Country)}"
            strQuery += $" ,Passport_Number = {WrapSqlVariable(objData.Passport_Number)}"
            strQuery += $" ,Residence = {WrapSqlVariable(objData.Residence)}"
            strQuery += $" ,Source_of_Wealth = {WrapSqlVariable(objData.Source_of_Wealth)}"
            strQuery += $" ,Tax_Number = {WrapSqlVariable(objData.Tax_Number)}"
            strQuery += $" ,Tax_Reg_Number = {WrapSqlVariable(objData.Tax_Reg_Number)}"
            strQuery += $" where {Where};"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Entity_Director).Delete
        Try
            Dim strQuery As String = ""
            strQuery += $"DELETE FROM {Table} WHERE {Where};"

            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function Save(objData As goAML_Ref_Entity_Director) As Integer Implements IDAL(Of goAML_Ref_Entity_Director).Save
        Try
            Dim strQuery As String = ""

            strQuery += $"INSERT INTO {Table}"
            strQuery += $"           ([Role]"
            strQuery += $"           ,[FK_Entity_ID]"
            strQuery += $"           ,[Title]"
            strQuery += $"           ,[First_name]"
            strQuery += $"           ,[Alias]"
            strQuery += $"           ,[Birth_Place]"
            strQuery += $"           ,[BirthDate]"
            strQuery += $"           ,[Comments]"
            strQuery += $"           ,[Deceased]"
            strQuery += $"           ,[Deceased_Date]"
            strQuery += $"           ,[Email]"
            strQuery += $"           ,[Employer_Name]"
            strQuery += $"           ,[Middle_Name]"
            strQuery += $"           ,[Prefix]"
            strQuery += $"           ,[Last_Name]"
            strQuery += $"           ,[Mothers_Name]"
            strQuery += $"           ,[SSN]"
            strQuery += $"           ,[Gender]"
            strQuery += $"           ,[ID_Number]"
            strQuery += $"           ,[Nationality1]"
            strQuery += $"           ,[Nationality2]"
            strQuery += $"           ,[Nationality3]"
            strQuery += $"           ,[Occupation]"
            strQuery += $"           ,[Passport_Country]"
            strQuery += $"           ,[Passport_Number]"
            strQuery += $"           ,[Residence]"
            strQuery += $"           ,[Source_of_Wealth]"
            strQuery += $"           ,[Tax_Number]"
            strQuery += $"           ,[Tax_Reg_Number])"
            strQuery += $"     VALUES"
            strQuery += $"           ({WrapSqlVariable(objData.Role)}"
            strQuery += $"           ,{WrapSqlVariable(objData.FK_Entity_ID)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Title)}"
            strQuery += $"           ,{WrapSqlVariable(objData.First_name)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Alias)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Birth_Place)}"
            ' 2023-10-07, Nael: Jika BirthDate Nothing, maka akan di set NULL pada raw sqlnya
            strQuery += $"           ,{IIf(objData.BirthDate IsNot Nothing, WrapSqlVariable(objData.BirthDate), "NULL")}"
            strQuery += $"           ,{WrapSqlVariable(objData.Comments)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Deceased)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Deceased_Date)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Email)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Employer_Name)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Middle_Name)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Prefix)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Last_Name)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Mothers_Name)}"
            strQuery += $"           ,{WrapSqlVariable(objData.SSN)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Gender)}"
            strQuery += $"           ,{WrapSqlVariable(objData.ID_Number)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Nationality1)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Nationality2)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Nationality3)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Occupation)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Passport_Country)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Passport_Number)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Residence)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Source_of_Wealth)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Tax_Number)}"
            strQuery += $"           ,{WrapSqlVariable(objData.Tax_Reg_Number)});"


            strQuery += " SELECT SCOPE_IDENTITY() AS NewID;"
            Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Dim pk As Integer = row("NewID")

            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Return pk
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetData(Where As String) As List(Of goAML_Ref_Entity_Director) Implements IDAL(Of goAML_Ref_Entity_Director).GetData
        Try
            Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Entity_Director WHERE {Where}")
            Dim listReturn As New List(Of DataModel.goAML_Ref_Entity_Director)

            For Each data As DataRow In dataDT.Rows
                Dim objReturn = New DataModel.goAML_Ref_Entity_Director

                If Not data.IsNull("PK_goAML_Ref_Customer_Entity_Director_ID") Then
                    objReturn.PK_goAML_Ref_Customer_Entity_Director_ID = data("PK_goAML_Ref_Customer_Entity_Director_ID")
                End If
                If Not data.IsNull("Role") Then
                    objReturn.Role = data("Role")
                End If
                If Not data.IsNull("FK_Entity_ID") Then
                    objReturn.FK_Entity_ID = data("FK_Entity_ID")
                End If
                If Not data.IsNull("Title") Then
                    objReturn.Title = data("Title")
                End If
                'objReturn.First_name = data("First_name")
                If Not data.IsNull("Alias") Then
                    objReturn.Alias = data("Alias")
                End If
                If Not data.IsNull("Birth_Place") Then
                    objReturn.Birth_Place = data("Birth_Place")
                End If
                If Not data.IsNull("BirthDate") Then
                    objReturn.BirthDate = data("BirthDate")
                Else
                    objReturn.BirthDate = Nothing
                End If
                If Not data.IsNull("Comments") Then
                    objReturn.Comments = data("Comments")
                End If
                If Not data.IsNull("Deceased") Then
                    objReturn.Deceased = data("Deceased")
                End If
                If Not data.IsNull("Deceased_Date") Then
                    objReturn.Deceased_Date = data("Deceased_Date")
                End If
                If Not data.IsNull("Email") Then

                    objReturn.Email = data("Email")
                End If
                If Not data.IsNull("Employer_Name") Then

                    objReturn.Employer_Name = data("Employer_Name")
                End If
                'objReturn.Middle_Name = data("Middle_Name")
                'objReturn.Prefix = data("Prefix")
                objReturn.Last_Name = data("Last_Name")
                If Not data.IsNull("Mothers_Name") Then

                    objReturn.Mothers_Name = data("Mothers_Name")
                End If
                If Not data.IsNull("SSN") Then

                    objReturn.SSN = data("SSN")
                End If
                If Not data.IsNull("Gender") Then

                    objReturn.Gender = data("Gender")
                End If
                If Not data.IsNull("ID_Number") Then

                    objReturn.ID_Number = data("ID_Number")
                End If
                If Not data.IsNull("Nationality1") Then

                    objReturn.Nationality1 = data("Nationality1")
                End If
                If Not data.IsNull("Nationality2") Then

                    objReturn.Nationality2 = data("Nationality2")
                End If
                If Not data.IsNull("Nationality3") Then

                    objReturn.Nationality3 = data("Nationality3")
                End If
                If Not data.IsNull("Occupation") Then
                    objReturn.Occupation = data("Occupation")
                End If
                If Not data.IsNull("Passport_Country") Then
                    objReturn.Passport_Country = data("Passport_Country")
                End If
                If Not data.IsNull("Passport_Number") Then
                    objReturn.Passport_Number = data("Passport_Number")
                End If
                If Not data.IsNull("Residence") Then
                    objReturn.Residence = data("Residence")
                End If
                If Not data.IsNull("Source_of_Wealth") Then
                    objReturn.Source_of_Wealth = data("Source_of_Wealth")
                End If
                If Not data.IsNull("Tax_Number") Then
                    objReturn.Tax_Number = data("Tax_Number")
                End If
                If Not data.IsNull("Tax_Reg_Number") Then
                    objReturn.Tax_Reg_Number = data("Tax_Reg_Number")
                End If

                listReturn.Add(objReturn)
            Next
            Return listReturn
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
