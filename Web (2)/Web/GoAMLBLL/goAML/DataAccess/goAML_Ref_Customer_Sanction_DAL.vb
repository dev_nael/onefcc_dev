﻿Imports GoAMLBLL.goAML.DataModel
Imports NawaDAL

Namespace goAML.DataAccess
    Public Class goAML_Ref_Customer_Sanction_DAL
        Implements IDAL(Of goAML_Ref_Customer_Sanction)

        Public Sub Update(objData As goAML_Ref_Customer_Sanction, Where As String) Implements IDAL(Of goAML_Ref_Customer_Sanction).Update
            Try
                Dim strQuery As String = ""

                strQuery = $"update goAML_Ref_Customer_Sanction"
                strQuery += $" set"
                strQuery += $" CIF =  {Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $" ,PROVIDER = {Helper.CommonHelper.WrapSqlVariable(objData.PROVIDER)}"
                strQuery += $" ,SANCTION_LIST_NAME = {Helper.CommonHelper.WrapSqlVariable(objData.SANCTION_LIST_NAME)}"
                strQuery += $" ,MATCH_CRITERIA = {Helper.CommonHelper.WrapSqlVariable(objData.MATCH_CRITERIA)}"
                strQuery += $" ,LINK_TO_SOURCE = {Helper.CommonHelper.WrapSqlVariable(objData.LINK_TO_SOURCE)}"
                strQuery += $" ,SANCTION_LIST_ATTRIBUTES = {Helper.CommonHelper.WrapSqlVariable(objData.SANCTION_LIST_ATTRIBUTES)}"
                'strQuery += $" ,SANCTION_LIST_DATE_RANGE_VALID_FROM = '{objData.SANCTION_LIST_DATE_RANGE_VALID_FROM}'"
                'strQuery += $" ,SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = {If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $" ,SANCTION_LIST_DATE_RANGE_VALID_TO = '{objData.SANCTION_LIST_DATE_RANGE_VALID_TO}'"
                'strQuery += $" ,SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = {If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $" ,COMMENTS = {Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)}"
                strQuery += $" where {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_Sanction).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_Customer_Sanction WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Customer_Sanction) As Integer Implements IDAL(Of goAML_Ref_Customer_Sanction).Save
            Try
                Dim strQuery As String = ""

                strQuery = $"INSERT INTO [dbo].[goAML_Ref_Customer_Sanction]"
                strQuery += $"           ([CIF]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[PROVIDER]"
                strQuery += $"           ,[SANCTION_LIST_NAME]"
                strQuery += $"           ,[MATCH_CRITERIA]"
                strQuery += $"           ,[LINK_TO_SOURCE]"
                strQuery += $"           ,[SANCTION_LIST_ATTRIBUTES]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_VALID_FROM]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_VALID_TO]"
                'strQuery += $"           ,[SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[COMMENTS])"
                strQuery += $"     VALUES"
                strQuery += $"           ({IIf(objData.CIF Is Nothing, "NULL", $"'{objData.CIF}'")}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.PROVIDER)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.SANCTION_LIST_NAME)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.MATCH_CRITERIA)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.LINK_TO_SOURCE)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.SANCTION_LIST_ATTRIBUTES)}"
                'strQuery += $"           ,'{objData.SANCTION_LIST_DATE_RANGE_VALID_FROM}'"
                'strQuery += $"           ,{If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE, 1, 0)}"
                'strQuery += $"           ,'{objData.SANCTION_LIST_DATE_RANGE_VALID_TO}'"
                'strQuery += $"           ,{If(objData.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE, 1, 0)}"
                strQuery += $"           ,{IIf(objData.FK_FOR_TABLE_ID Is Nothing, "NULL", objData.FK_FOR_TABLE_ID)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.COMMENTS)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_Sanction) Implements IDAL(Of goAML_Ref_Customer_Sanction).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Sanction WHERE {Where}")
                Dim listReturn As New List(Of goAML_Ref_Customer_Sanction)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Ref_Customer_Sanction
                    objReturn.PK_goAML_Ref_Customer_Sanction_ID = data("PK_goAML_Ref_Customer_Sanction_ID")
                    '' Edit 21-Sep-2023, Felix.
                    'objReturn.CIF = data("CIF")
                    If Not IsDBNull(data("CIF")) Then
                        objReturn.CIF = data("CIF")
                    End If

                    If Not IsDBNull(data("FK_FOR_TABLE_ID")) Then
                        objReturn.FK_FOR_TABLE_ID = Convert.ToInt32(data("FK_FOR_TABLE_ID"))
                    End If
                    '' End 21-Sep-2023
                    objReturn.PK_goAML_Ref_Customer_Sanction_ID = data("PK_goAML_Ref_Customer_Sanction_ID")
                    objReturn.PROVIDER = data("PROVIDER")
                    objReturn.SANCTION_LIST_NAME = data("SANCTION_LIST_NAME")
                    If Not data.IsNull("MATCH_CRITERIA") Then
                        objReturn.MATCH_CRITERIA = data("MATCH_CRITERIA")
                    End If
                    If Not data.IsNull("LINK_TO_SOURCE") Then
                        objReturn.LINK_TO_SOURCE = data("LINK_TO_SOURCE")
                    End If
                    If Not data.IsNull("SANCTION_LIST_ATTRIBUTES") Then
                        objReturn.SANCTION_LIST_ATTRIBUTES = data("SANCTION_LIST_ATTRIBUTES")
                    End If
                    'objReturn.SANCTION_LIST_DATE_RANGE_VALID_FROM = data("SANCTION_LIST_DATE_RANGE_VALID_FROM")
                    'objReturn.SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE = data("SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE")
                    'objReturn.SANCTION_LIST_DATE_RANGE_VALID_TO = data("SANCTION_LIST_DATE_RANGE_VALID_TO")
                    'objReturn.SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE = data("SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE")
                    If Not data.IsNull("COMMENTS") Then
                        objReturn.COMMENTS = data("COMMENTS")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace

