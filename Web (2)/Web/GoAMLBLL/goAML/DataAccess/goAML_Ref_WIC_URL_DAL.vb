﻿Imports GoAMLBLL.goAML.DataModel

Namespace goAML.DataAccess
    Public Class goAML_Ref_WIC_URL_DAL
        Implements IDAL(Of goAML_Ref_Customer_URL)

        Public Function Save(objData As goAML_Ref_Customer_URL) As Integer Implements IDAL(Of goAML_Ref_Customer_URL).Save
            Try
                Dim strQuery As String = ""

                strQuery = $"INSERT INTO [dbo].[goAML_Ref_WIC_URL]"
                strQuery += $"           ([WIC_No]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]" ' 2023-10-12, Nael
                strQuery += $"           ,[URL])"
                strQuery += $"     VALUES"
                strQuery += $"           ({Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_FOR_TABLE_ID)}" ' 2023-10-12, Nael
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.URL)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub Update(objData As goAML_Ref_Customer_URL, Where As String) Implements IDAL(Of goAML_Ref_Customer_URL).Update
            Try
                Dim strQuery As String = ""

                strQuery = $"update goAML_Ref_WIC_URL"
                strQuery += $" set"
                strQuery += $" WIC_No =  {Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $" ,URL = {Helper.CommonHelper.WrapSqlVariable(objData.URL)}"
                strQuery += $" where {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_URL).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_WIC_URL WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_URL) Implements IDAL(Of goAML_Ref_Customer_URL).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_WIC_URL WHERE {Where}")
                Dim listReturn As New List(Of goAML_Ref_Customer_URL)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Ref_Customer_URL
                    If Not data.IsNull("PK_goAML_Ref_WIC_URL_ID") Then
                        objReturn.PK_goAML_Ref_Customer_URL_ID = data("PK_goAML_Ref_WIC_URL_ID")
                    End If
                    If Not data.IsNull("FK_REF_DETAIL_OF") Then
                        objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
                    End If
                    If Not data.IsNull("WIC_No") Then
                        objReturn.CIF = data("WIC_No")
                    End If
                    If Not data.IsNull("URL") Then
                        objReturn.URL = data("URL")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class

End Namespace
