﻿

Namespace goAML.DataAccess
    Public Class goAML_Ref_WIC_Email_DAL
        Implements IDAL(Of DataModel.goAML_Ref_Customer_Email)
        Public Function Save(objData As DataModel.goAML_Ref_Customer_Email) As Integer Implements IDAL(Of DataModel.goAML_Ref_Customer_Email).Save
            Try
                Dim strQuery As String = ""

                strQuery = $"INSERT INTO [dbo].[goAML_Ref_WIC_Email]"
                strQuery += $"           ([WIC_No]"
                strQuery += $"           ,[FK_REF_DETAIL_OF]"
                strQuery += $"           ,[FK_FOR_TABLE_ID]"
                strQuery += $"           ,[EMAIL_ADDRESS])"
                strQuery += $"     VALUES"
                strQuery += $"           ({Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_FOR_TABLE_ID)}"
                strQuery += $"           ,{Helper.CommonHelper.WrapSqlVariable(objData.EMAIL_ADDRESS)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of DataModel.goAML_Ref_Customer_Email) Implements IDAL(Of DataModel.goAML_Ref_Customer_Email).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_WIC_Email WHERE {Where}")
                Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Email)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New DataModel.goAML_Ref_Customer_Email
                    If Not IsDBNull(data("PK_goAML_Ref_WIC_Email_ID")) Then
                        objReturn.PK_goAML_Ref_Customer_Email_ID = data("PK_goAML_Ref_WIC_Email_ID")
                    End If
                    If Not IsDBNull(data("FK_REF_DETAIL_OF")) Then
                        objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
                    End If

                    '' Edit 21-Sep-2023, Felix
                    'objReturn.WIC_No = data("WIC_No")
                    If Not IsDBNull(data("WIC_No")) Then
                        objReturn.CIF = data("WIC_No")
                    End If

                    If Not IsDBNull(data("FK_FOR_TABLE_ID")) Then
                        objReturn.FK_FOR_TABLE_ID = Convert.ToInt32(data("FK_FOR_TABLE_ID")) ' 2023-10-16, Nael type jadi integer
                    End If
                    '' End 21-Sep-2023

                    If Not IsDBNull(data("EMAIL_ADDRESS")) Then
                        objReturn.EMAIL_ADDRESS = data("EMAIL_ADDRESS")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function

        Public Sub Update(objData As DataModel.goAML_Ref_Customer_Email, Where As String) Implements IDAL(Of DataModel.goAML_Ref_Customer_Email).Update
            Try
                Dim strQuery As String = ""

                strQuery = $"update goAML_Ref_WIC_Email"
                strQuery += $" set"
                strQuery += $" WIC_No =  {Helper.CommonHelper.WrapSqlVariable(objData.CIF)}"
                strQuery += $" ,EMAIL_ADDRESS = {Helper.CommonHelper.WrapSqlVariable(objData.EMAIL_ADDRESS)}"
                strQuery += $" where {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of DataModel.goAML_Ref_Customer_Email).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_WIC_Email WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace

