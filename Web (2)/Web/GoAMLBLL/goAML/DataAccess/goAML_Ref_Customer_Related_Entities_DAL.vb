﻿Imports GoAMLBLL.goAML.DataModel
Imports System.Text
Imports GoAMLBLL.Helper.CommonHelper

Namespace goAML.DataAccess
    Public Class goAML_Ref_Customer_Related_Entities_DAL
        Implements IDAL(Of DataModel.goAML_Ref_Customer_Related_Entities)

        Public Sub Update(objData As goAML_Ref_Customer_Related_Entities, Where As String) Implements IDAL(Of goAML_Ref_Customer_Related_Entities).Update
            Try
                '2023-10-17, Nael
                Dim queryBuilder As New StringBuilder()
                queryBuilder.Append("UPDATE goAML_Ref_Customer_Related_Entities")
                queryBuilder.Append($"SET [CIF] = {WrapSqlVariable(objData.CIF)}")
                queryBuilder.Append($",[ENTITY_ENTITY_RELATION] = {WrapSqlVariable(objData.ENTITY_ENTITY_RELATION)}")

                If objData.RELATION_DATE_RANGE_VALID_FROM Is Nothing Then
                    queryBuilder.Append($", [RELATION_DATE_RANGE_VALID_FROM] = NULL")
                Else
                    queryBuilder.Append($", [RELATION_DATE_RANGE_VALID_FROM] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_FROM)}")
                End If


                queryBuilder.Append($",[RELATION_DATE_RANGE_IS_APPROX_FROM_DATE] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE)}")

                If objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE Then
                    queryBuilder.Append($",[RELATION_DATE_RANGE_VALID_TO] = NULL")
                Else
                    queryBuilder.Append($",[RELATION_DATE_RANGE_VALID_TO] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_TO)}")
                End If

                queryBuilder.Append($",[RELATION_DATE_RANGE_IS_APPROX_TO_DATE] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE)}")
                queryBuilder.Append($",[SHARE_PERCENTAGE] = {WrapSqlVariable(objData.SHARE_PERCENTAGE)}")
                queryBuilder.Append($",[COMMENTS] = {WrapSqlVariable(objData.COMMENTS)}")
                queryBuilder.Append($",[NAME] = {WrapSqlVariable(objData.NAME)}")
                queryBuilder.Append($",[COMMERCIAL_NAME] = {WrapSqlVariable(objData.COMMERCIAL_NAME)}")
                queryBuilder.Append($",[INCORPORATION_LEGAL_FORM] = {WrapSqlVariable(objData.INCORPORATION_LEGAL_FORM)}")
                queryBuilder.Append($",[INCORPORATION_NUMBER] = {WrapSqlVariable(objData.INCORPORATION_NUMBER)}")
                queryBuilder.Append($",[BUSINESS] = {WrapSqlVariable(objData.BUSINESS)}")


                queryBuilder.Append($",[ENTITY_STATUS] = {WrapSqlVariable(objData.ENTITY_STATUS)}")

                If objData.ENTITY_STATUS_DATE Is Nothing Then
                    queryBuilder.Append($",[ENTITY_STATUS_DATE] = NULL")
                Else
                    queryBuilder.Append($",[ENTITY_STATUS_DATE] = {WrapSqlVariable(objData.ENTITY_STATUS_DATE)}")
                End If

                queryBuilder.Append($",[INCORPORATION_STATE] = {WrapSqlVariable(objData.INCORPORATION_STATE)}")
                queryBuilder.Append($",[INCORPORATION_COUNTRY_CODE] = {WrapSqlVariable(objData.INCORPORATION_COUNTRY_CODE)}")

                If objData.INCORPORATION_DATE Is Nothing Then
                    queryBuilder.Append($",[INCORPORATION_DATE] = NULL")
                Else
                    queryBuilder.Append($",[INCORPORATION_DATE] = {WrapSqlVariable(objData.INCORPORATION_DATE)}")
                End If

                queryBuilder.Append($",[BUSINESS_CLOSED] = {WrapSqlVariable(objData.BUSINESS_CLOSED)}")

                If objData.DATE_BUSINESS_CLOSED Is Nothing Then
                    queryBuilder.Append($",[DATE_BUSINESS_CLOSED] = NULL")
                Else
                    queryBuilder.Append($",[DATE_BUSINESS_CLOSED] = {WrapSqlVariable(objData.DATE_BUSINESS_CLOSED)}")
                End If

                queryBuilder.Append($",[TAX_NUMBER] = {WrapSqlVariable(objData.TAX_NUMBER)}")
                queryBuilder.Append($",[TAX_REG_NUMBER] = {WrapSqlVariable(objData.TAX_REG_NUMBER)}")
                queryBuilder.Append($",[relation_comments] = {WrapSqlVariable(objData.RELATION_COMMENTS)}")
                queryBuilder.Append($" WHERE PK_goAML_Ref_Customer_Related_Entities_ID = {objData.PK_goAML_Ref_Customer_Related_Entities_ID}")

                Dim stringQuery As String = queryBuilder.ToString()

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_Related_Entities).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_Customer_Related_Entities WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Customer_Related_Entities) As Integer Implements IDAL(Of goAML_Ref_Customer_Related_Entities).Save
            Try
                ' 2023-10-17, Nael
                Dim queryBuilder As New StringBuilder()
                queryBuilder.Append("INSERT INTO goAML_Ref_Customer_Related_Entities")
                queryBuilder.Append("([CIF]")
                queryBuilder.Append(",[ENTITY_ENTITY_RELATION]")
                queryBuilder.Append(",[RELATION_DATE_RANGE_VALID_FROM]")
                queryBuilder.Append(",[RELATION_DATE_RANGE_IS_APPROX_FROM_DATE]")
                queryBuilder.Append(",[RELATION_DATE_RANGE_VALID_TO]")
                queryBuilder.Append(",[RELATION_DATE_RANGE_IS_APPROX_TO_DATE]")
                queryBuilder.Append(",[SHARE_PERCENTAGE]")
                queryBuilder.Append(",[COMMENTS]")
                queryBuilder.Append(",[NAME]")
                queryBuilder.Append(",[COMMERCIAL_NAME]")
                queryBuilder.Append(",[INCORPORATION_LEGAL_FORM]")
                queryBuilder.Append(",[INCORPORATION_NUMBER]")
                queryBuilder.Append(",[BUSINESS]")
                queryBuilder.Append(",[ENTITY_STATUS]")
                queryBuilder.Append(",[ENTITY_STATUS_DATE]")
                queryBuilder.Append(",[INCORPORATION_STATE]")
                queryBuilder.Append(",[INCORPORATION_COUNTRY_CODE]")
                queryBuilder.Append(",[INCORPORATION_DATE]")
                queryBuilder.Append(",[BUSINESS_CLOSED]")
                queryBuilder.Append(",[DATE_BUSINESS_CLOSED]")
                queryBuilder.Append(",[TAX_NUMBER]")
                queryBuilder.Append(",[TAX_REG_NUMBER]")
                queryBuilder.Append(",[relation_comments]")
                queryBuilder.Append(") VALUES (")
                queryBuilder.Append($"{WrapSqlVariable(objData.CIF)}")
                queryBuilder.Append($",{WrapSqlVariable(objData.ENTITY_ENTITY_RELATION)}")

                If objData.RELATION_DATE_RANGE_VALID_FROM Is Nothing Then
                    queryBuilder.Append($",NULL")
                Else
                    queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_FROM)}")
                End If

                queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE)}")
                queryBuilder.Append($",{IIf(objData.RELATION_DATE_RANGE_VALID_TO Is Nothing, "NULL", objData.RELATION_DATE_RANGE_VALID_TO)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.SHARE_PERCENTAGE)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.COMMENTS)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.NAME)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.COMMERCIAL_NAME)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.INCORPORATION_LEGAL_FORM)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.INCORPORATION_NUMBER)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.BUSINESS)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.ENTITY_STATUS)}")

                If objData.ENTITY_STATUS_DATE Is Nothing Then
                    queryBuilder.Append($", NULL")
                Else
                    queryBuilder.Append($", {WrapSqlVariable(objData.ENTITY_STATUS_DATE)}")
                End If

                queryBuilder.Append($", {WrapSqlVariable(objData.INCORPORATION_STATE)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.INCORPORATION_COUNTRY_CODE)}")

                If objData.INCORPORATION_DATE Is Nothing Then
                    queryBuilder.Append($", NULL")
                Else
                    queryBuilder.Append($", {WrapSqlVariable(objData.INCORPORATION_DATE)}")
                End If

                queryBuilder.Append($", {WrapSqlVariable(objData.BUSINESS_CLOSED)}")

                If objData.DATE_BUSINESS_CLOSED Is Nothing Then
                    queryBuilder.Append($", NULL")
                Else
                    queryBuilder.Append($", {WrapSqlVariable(objData.DATE_BUSINESS_CLOSED)}")
                End If

                queryBuilder.Append($", {WrapSqlVariable(objData.TAX_NUMBER)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.TAX_REG_NUMBER)}")
                queryBuilder.Append($", {WrapSqlVariable(objData.RELATION_COMMENTS)}")
                queryBuilder.Append(");")

                queryBuilder.Append("SELECT SCOPE_IDENTITY() AS NewID;")
                Dim stringQuery As String = queryBuilder.ToString()
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringQuery, Nothing)
                Dim pk As Integer = row("NewID")
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_Related_Entities) Implements IDAL(Of goAML_Ref_Customer_Related_Entities).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Customer_Related_Entities WHERE {Where}")
                Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Related_Entities)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New DataModel.goAML_Ref_Customer_Related_Entities

                    If Not data.IsNull("PK_goAML_Ref_Customer_Related_Entities_ID") Then
                        objReturn.PK_goAML_Ref_Customer_Related_Entities_ID = data("PK_goAML_Ref_Customer_Related_Entities_ID")
                    End If
                    'objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")

                    If Not data.IsNull("CIF") Then
                        objReturn.CIF = data("CIF")
                    End If
                    If Not data.IsNull("ENTITY_ENTITY_RELATION") Then
                        objReturn.ENTITY_ENTITY_RELATION = data("ENTITY_ENTITY_RELATION")
                    End If
                    If Not data.IsNull("COMMENTS") Then
                        objReturn.COMMENTS = data("COMMENTS")
                    End If
                    objReturn.NAME = data("NAME")
                    If Not data.IsNull("COMMERCIAL_NAME") Then
                        objReturn.COMMERCIAL_NAME = data("COMMERCIAL_NAME")
                    End If
                    If Not data.IsNull("INCORPORATION_LEGAL_FORM") Then
                        objReturn.INCORPORATION_LEGAL_FORM = data("INCORPORATION_LEGAL_FORM")
                    End If
                    If Not data.IsNull("INCORPORATION_NUMBER") Then
                        objReturn.INCORPORATION_NUMBER = data("INCORPORATION_NUMBER")
                    End If
                    If Not data.IsNull("BUSINESS") Then
                        objReturn.BUSINESS = data("BUSINESS")
                    End If
                    If Not data.IsNull("INCORPORATION_STATE") Then
                        objReturn.INCORPORATION_STATE = data("INCORPORATION_STATE")
                    End If
                    If Not data.IsNull("INCORPORATION_COUNTRY_CODE") Then
                        objReturn.INCORPORATION_COUNTRY_CODE = data("INCORPORATION_COUNTRY_CODE")
                    End If
                    If Not data.IsNull("INCORPORATION_DATE") Then
                        objReturn.INCORPORATION_DATE = data("INCORPORATION_DATE")
                    End If
                    If Not data.IsNull("BUSINESS_CLOSED") Then
                        objReturn.BUSINESS_CLOSED = data("BUSINESS_CLOSED")
                    End If
                    If Not data.IsNull("DATE_BUSINESS_CLOSED") Then
                        objReturn.DATE_BUSINESS_CLOSED = data("DATE_BUSINESS_CLOSED")
                    End If
                    If Not data.IsNull("TAX_NUMBER") Then
                        objReturn.TAX_NUMBER = data("TAX_NUMBER")
                    End If
                    If Not data.IsNull("TAX_REG_NUMBER") Then
                        objReturn.TAX_REG_NUMBER = data("TAX_REG_NUMBER")
                    End If
                    If Not data.IsNull("relation_comments") Then
                        objReturn.RELATION_COMMENTS = data("relation_comments")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace

