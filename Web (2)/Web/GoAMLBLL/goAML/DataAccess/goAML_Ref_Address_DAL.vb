﻿Imports Ext.Net
Imports GoAMLBLL.goAML.DataModel
Imports OfficeOpenXml.ExcelErrorValue

Namespace goAML.DataAccess
    Public Class goAML_Ref_Address_DAL
        Implements IDAL(Of goAML_Ref_Address)

        Public Sub Update(objData As goAML_Ref_Address, Where As String) Implements IDAL(Of goAML_Ref_Address).Update
            Try
                Dim strQuery As String = ""

                strQuery += $"Update [dbo].[goAML_Ref_Address]"
                strQuery += $"   SET [FK_Ref_Detail_Of] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_Ref_Detail_Of)}"
                strQuery += $"      ,[FK_To_Table_ID] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_To_Table_ID)}"
                strQuery += $"      ,[Address_Type] = {Helper.CommonHelper.WrapSqlVariable(objData.Address_Type)}"
                strQuery += $"      ,[Address] = {Helper.CommonHelper.WrapSqlVariable(objData.Address)}"
                strQuery += $"      ,[Town] = {Helper.CommonHelper.WrapSqlVariable(objData.Town)}"
                strQuery += $"      ,[City] = {Helper.CommonHelper.WrapSqlVariable(objData.City)}"
                strQuery += $"      ,[Zip] = {Helper.CommonHelper.WrapSqlVariable(objData.Zip)}"
                strQuery += $"      ,[Country_Code] = {Helper.CommonHelper.WrapSqlVariable(objData.Country_Code)}"
                strQuery += $"      ,[State] = {Helper.CommonHelper.WrapSqlVariable(objData.State)}"
                strQuery += $"      ,[Comments] = {Helper.CommonHelper.WrapSqlVariable(objData.Comments)}"
                strQuery += $"      ,[IsCustomer] = {Helper.CommonHelper.WrapSqlVariable(objData.IsCustomer)}"
                strQuery += $" WHERE {Where}"



                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Address).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_Address WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Address) As Integer Implements IDAL(Of goAML_Ref_Address).Save
            Try
                Dim strQuery As String = ""

                strQuery += $"INSERT INTO [dbo].[goAML_Ref_Address]"
                strQuery += $"       ([FK_Ref_Detail_Of]"
                strQuery += $"       ,[FK_To_Table_ID]"
                strQuery += $"       ,[Address_Type]"
                strQuery += $"       ,[Address]"
                strQuery += $"       ,[Town]"
                strQuery += $"       ,[City]"
                strQuery += $"       ,[Zip]"
                strQuery += $"       ,[Country_Code]"
                strQuery += $"       ,[State]"
                strQuery += $"       ,[Comments]"
                strQuery += $"       ,[IsCustomer])"
                strQuery += $" Values"
                strQuery += $"       ({Helper.CommonHelper.WrapSqlVariable(objData.FK_Ref_Detail_Of)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_To_Table_ID)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Address_Type)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Address)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Town)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.City)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Zip)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Country_Code)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.State)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Comments)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.IsCustomer)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Address) Implements IDAL(Of goAML_Ref_Address).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Address WHERE {Where}")
                Dim listReturn As New List(Of goAML_Ref_Address)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Ref_Address
                    If Not data.IsNull("FK_To_Table_ID") Then
                        objReturn.FK_To_Table_ID = data("FK_To_Table_ID")
                    End If
                    If Not data.IsNull("Address_Type") Then
                        objReturn.Address_Type = data("Address_Type")
                    End If
                    If Not data.IsNull("Address") Then
                        objReturn.Address = data("Address")
                    End If
                    If Not data.IsNull("Town") Then
                        objReturn.Town = data("Town")
                    End If
                    objReturn.City = data("City")
                    If Not data.IsNull("Zip") Then
                        objReturn.Zip = data("Zip")
                    End If
                    objReturn.Country_Code = data("Country_Code")
                    If Not data.IsNull("State") Then
                        objReturn.State = data("State")
                    End If
                    If Not data.IsNull("Comments") Then
                        objReturn.Comments = data("Comments")
                    End If
                    If data("IsCustomer") Then
                        objReturn.IsCustomer = data("IsCustomer")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace
