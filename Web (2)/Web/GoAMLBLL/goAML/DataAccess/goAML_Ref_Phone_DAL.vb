﻿Imports Ext.Net
Imports GoAMLBLL.goAML.DataModel
Imports OfficeOpenXml.ExcelErrorValue

Namespace goAML.DataAccess
    Public Class goAML_Ref_Phone_DAL
        Implements IDAL(Of goAML_Ref_Phone)

        Public Sub Update(objData As goAML_Ref_Phone, Where As String) Implements IDAL(Of goAML_Ref_Phone).Update
            Try
                Dim strQuery As String = ""

                strQuery += $"Update [dbo].[goAML_Ref_Phone]"
                strQuery += $"   SET [FK_Ref_Detail_Of] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_Ref_Detail_Of)}"
                strQuery += $"      ,[FK_for_Table_ID] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_for_Table_ID)}"
                strQuery += $"      ,[Tph_Contact_Type] = {Helper.CommonHelper.WrapSqlVariable(objData.Tph_Contact_Type)}"
                strQuery += $"      ,[Tph_Communication_Type] = {Helper.CommonHelper.WrapSqlVariable(objData.Tph_Communication_Type)}"
                strQuery += $"      ,[tph_country_prefix] = {Helper.CommonHelper.WrapSqlVariable(objData.tph_country_prefix)}"
                strQuery += $"      ,[tph_number] = {Helper.CommonHelper.WrapSqlVariable(objData.tph_number)}"
                strQuery += $"      ,[tph_extension] = {Helper.CommonHelper.WrapSqlVariable(objData.tph_extension)}"
                strQuery += $"      ,[comments] = {Helper.CommonHelper.WrapSqlVariable(objData.comments)}"
                strQuery += $"      ,[IsCustomer] = {Helper.CommonHelper.WrapSqlVariable(objData.IsCustomer)}"
                strQuery += $" Where {Where}"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Phone).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_Phone WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Phone) As Integer Implements IDAL(Of goAML_Ref_Phone).Save
            Try
                Dim strQuery As String = ""

                strQuery += $"INSERT INTO [dbo].[goAML_Ref_Phone]"
                strQuery += $"       ([FK_Ref_Detail_Of]"
                strQuery += $"       ,[FK_for_Table_ID]"
                strQuery += $"       ,[Tph_Contact_Type]"
                strQuery += $"       ,[Tph_Communication_Type]"
                strQuery += $"       ,[tph_country_prefix]"
                strQuery += $"       ,[tph_number]"
                strQuery += $"       ,[tph_extension]"
                strQuery += $"       ,[comments]"
                strQuery += $"       ,[IsCustomer])"
                strQuery += $" Values"
                strQuery += $"       ({Helper.CommonHelper.WrapSqlVariable(objData.FK_Ref_Detail_Of)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.FK_for_Table_ID)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Tph_Contact_Type)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.Tph_Communication_Type)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.tph_country_prefix)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.tph_number)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.tph_extension)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.comments)}"
                strQuery += $"       ,{Helper.CommonHelper.WrapSqlVariable(objData.IsCustomer)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Phone) Implements IDAL(Of goAML_Ref_Phone).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_Phone WHERE {Where}")
                Dim listReturn As New List(Of goAML_Ref_Phone)

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Ref_Phone
                    If Not data.IsNull("PK_goAML_Ref_Phone") Then
                        objReturn.PK_goAML_Ref_Phone = data("PK_goAML_Ref_Phone")
                    End If
                    If Not data.IsNull("FK_Ref_Detail_Of") Then
                        objReturn.FK_Ref_Detail_Of = data("FK_Ref_Detail_Of")
                    End If
                    If Not data.IsNull("FK_for_Table_ID") Then
                        objReturn.FK_for_Table_ID = data("FK_for_Table_ID")
                    End If
                    If Not data.IsNull("Tph_Contact_Type") Then
                        objReturn.Tph_Contact_Type = data("Tph_Contact_Type")
                    End If
                    If Not data.IsNull("Tph_Communication_Type") Then
                        objReturn.Tph_Communication_Type = data("Tph_Communication_Type")
                    End If
                    If Not data.IsNull("tph_country_prefix") Then
                        objReturn.tph_country_prefix = data("tph_country_prefix")
                    End If
                    objReturn.tph_number = data("tph_number")
                    If Not data.IsNull("tph_extension") Then
                        objReturn.tph_extension = data("tph_extension")
                    End If
                    If Not data.IsNull("comments") Then
                        objReturn.comments = data("comments")
                    End If
                    If Not data.IsNull("IsCustomer") Then
                        objReturn.IsCustomer = data("IsCustomer")

                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace
