﻿Imports Ext.Net
Imports GoAMLBLL.goAML.DataModel
Imports GoAMLBLL.Helper
Imports OfficeOpenXml.ExcelErrorValue

Namespace goAML.DataAccess
    Public Class goAML_Person_Identification_DAL
        Implements IDAL(Of goAML_Person_Identification)

        Public Sub Update(objData As goAML_Person_Identification, Where As String) Implements IDAL(Of goAML_Person_Identification).Update
            Try
                Dim strQuery As String = ""

                strQuery += $"Update [dbo].[goAML_Person_Identification]"
                strQuery += $"   SET [isReportingPerson] = {Helper.CommonHelper.WrapSqlVariable(objData.isReportingPerson)}"
                strQuery += $"      ,[FK_Person_ID] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_Person_ID)}"
                strQuery += $"      ,[Type] = {Helper.CommonHelper.WrapSqlVariable(objData.Type)}"
                strQuery += $"      ,[Number] = {Helper.CommonHelper.WrapSqlVariable(objData.Number)}"
                strQuery += $"      ,[Issue_Date] = {Helper.CommonHelper.WrapSqlVariable(objData.Issue_Date)}"
                strQuery += $"      ,[Expiry_Date] = {Helper.CommonHelper.WrapSqlVariable(objData.Expiry_Date)}"
                strQuery += $"      ,[Issued_By] = {Helper.CommonHelper.WrapSqlVariable(objData.Issued_By)}"
                strQuery += $"      ,[Issued_Country] = {Helper.CommonHelper.WrapSqlVariable(objData.Issued_Country)}"
                strQuery += $"      ,[Identification_Comment] = {Helper.CommonHelper.WrapSqlVariable(objData.Identification_Comment)}"
                strQuery += $"      ,[FK_Person_Type] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_Person_Type)}"
                strQuery += $"      ,[FK_REF_DETAIL_OF] = {Helper.CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"      ,[IsCustomer] = {Helper.CommonHelper.WrapSqlVariable(objData.IsCustomer)}"
                strQuery += $" Where {Where}"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Person_Identification).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Person_Identification WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Person_Identification) As Integer Implements IDAL(Of goAML_Person_Identification).Save
            Try
                Dim strQuery As String = ""

                strQuery += $"INSERT INTO [dbo].[goAML_Person_Identification]"
                strQuery += $"       ([isReportingPerson]"
                strQuery += $"       ,[FK_Person_ID]"
                strQuery += $"       ,[Type]"
                strQuery += $"       ,[Number]"
                strQuery += $"       ,[Issue_Date]"
                strQuery += $"       ,[Expiry_Date]"
                strQuery += $"       ,[Issued_By]"
                strQuery += $"       ,[Issued_Country]"
                strQuery += $"       ,[Identification_Comment]"
                strQuery += $"       ,[FK_Person_Type]"
                'strQuery += $"       ,[FK_REF_DETAIL_OF]" ' 2023-11-09, Nael: field ini sudah tidak digunakan untuk identification
                strQuery += $"       ,[IsCustomer])"
                strQuery += $" Values"
                strQuery += $"       ({CommonHelper.WrapSqlVariable(objData.isReportingPerson)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.FK_Person_ID)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.Type)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.Number)}"

                ' 2023-10-02, Nael: Jika Issue_Date atau Expiry_Date nothing SQL akan menganggap itu string kosong, jadi harus dipasang logic
                strQuery += $"       ,{IIf(objData.Issue_Date Is Nothing, "NULL", CommonHelper.WrapSqlVariable(objData.Issue_Date))}"
                strQuery += $"       ,{IIf(objData.Expiry_Date Is Nothing, "NULL", CommonHelper.WrapSqlVariable(objData.Expiry_Date))}"
                ' ============================================================================================

                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.Issued_By)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.Issued_Country)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.Identification_Comment)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.FK_Person_Type)}"
                'strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.FK_REF_DETAIL_OF)}"
                strQuery += $"       ,{CommonHelper.WrapSqlVariable(objData.IsCustomer)})"

                strQuery += " SELECT SCOPE_IDENTITY() AS NewID"
                Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Dim pk As Integer = row("NewID")

                'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                Return pk
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Person_Identification) Implements IDAL(Of goAML_Person_Identification).GetData
            Try
                ' FIXME: Ini ada yg salah di WHERE Clausenya
                ' Example SELECT * FROM goAML_Person_Identification WHERE FK_Person_ID = 70060 AND FK_Person_Type = 31
                Dim queryDataDT As String = $"SELECT * FROM goAML_Person_Identification WHERE {Where}"
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, queryDataDT)
                Dim listReturn As New List(Of goAML_Person_Identification)

                Dim countRows As Integer = dataDT.Rows.Count

                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New goAML_Person_Identification
                    If Not data.IsNull("PK_Person_Identification_ID") Then
                        objReturn.PK_Person_Identification_ID = data("PK_Person_Identification_ID")
                    End If
                    If Not data.IsNull("isReportingPerson") Then
                        objReturn.isReportingPerson = data("isReportingPerson")
                    End If
                    If Not data.IsNull("FK_Person_ID") Then
                        objReturn.FK_Person_ID = data("FK_Person_ID")
                    End If
                    If Not data.IsNull("Type") Then
                        objReturn.Type = data("Type")
                    End If
                    If Not data.IsNull("Number") Then
                        objReturn.Number = data("Number")
                    End If
                    If Not data.IsNull("Issue_Date") Then
                        objReturn.Issue_Date = data("Issue_Date")
                    End If
                    If Not data.IsNull("Expiry_Date") Then
                        objReturn.Expiry_Date = data("Expiry_Date")
                    End If
                    If Not data.IsNull("Issued_By") Then
                        objReturn.Issued_By = data("Issued_By")
                    End If
                    If Not data.IsNull("Issued_Country") Then
                        objReturn.Issued_Country = data("Issued_Country")
                    End If
                    If Not data.IsNull("Identification_Comment") Then
                        objReturn.Identification_Comment = data("Identification_Comment")
                    End If

                    '' Edit 21-Sep-2023, Felix.
                    'objReturn.FK_Person_Type = data("FK_Person_Type")
                    'objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
                    If Not data.IsNull("FK_Person_Type") Then
                        objReturn.FK_Person_Type = data("FK_Person_Type")
                    End If
                    If Not data.IsNull("FK_REF_DETAIL_OF") Then
                        objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
                    End If
                    '' End 21-Sep-2023

                    If Not data.IsNull("IsCustomer") Then
                        objReturn.IsCustomer = data("IsCustomer")
                    End If
                    If Not data.IsNull("CreatedBy") Then
                        objReturn.CreatedBy = data("CreatedBy")
                    End If
                    If Not data.IsNull("CreatedDate") Then
                        objReturn.CreatedDate = data("CreatedDate")
                    End If
                    If Not data.IsNull("LastUpdateBy") Then
                        objReturn.LastUpdateBy = data("LastUpdateBy")
                    End If
                    If Not data.IsNull("LastUpdateDate") Then
                        objReturn.LastUpdateDate = data("LastUpdateDate")
                    End If
                    If Not data.IsNull("ApprovedBy") Then
                        objReturn.ApprovedBy = data("ApprovedBy")
                    End If
                    If Not data.IsNull("ApprovedDate") Then
                        objReturn.ApprovedDate = data("ApprovedDate")
                    End If
                    If Not data.IsNull("Active") Then
                        objReturn.Active = data("Active")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace