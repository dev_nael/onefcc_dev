﻿Imports GoAMLBLL.goAML.DataModel
Imports System.Text
Imports GoAMLBLL.Helper.CommonHelper

Namespace goAML.DataAccess
    Public Class goAML_Ref_WIC_Related_Person_DAL
        Implements IDAL(Of DataModel.goAML_Ref_Customer_Related_Person)

        Public Sub Update(objData As goAML_Ref_Customer_Related_Person, Where As String) Implements IDAL(Of goAML_Ref_Customer_Related_Person).Update
            Dim queryBuilder As New StringBuilder()
            queryBuilder.Append("UPDATE goAML_Ref_WIC_Related_Person ")
            queryBuilder.Append($"SET [WIC_No] = {WrapSqlVariable(objData.CIF)}")
            queryBuilder.Append($",[person_person_relation] = {WrapSqlVariable(objData.PERSON_PERSON_RELATION)}")

            If objData.RELATION_DATE_RANGE_VALID_FROM Is Nothing Then
                queryBuilder.Append($",[relation_date_range_valid_from] = NULL")
            Else
                queryBuilder.Append($",[relation_date_range_valid_from] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_FROM)}")
            End If

            queryBuilder.Append($",[relation_date_range_is_approx_from_date] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE)}")

            If objData.RELATION_DATE_RANGE_VALID_TO Is Nothing Then
                queryBuilder.Append($",[relation_date_range_valid_to] = NULL")
            Else
                queryBuilder.Append($",[relation_date_range_valid_to] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_TO)}")
            End If

            queryBuilder.Append($",[relation_date_range_is_approx_to_date] = {WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE)}")
            queryBuilder.Append($",[comments] = {WrapSqlVariable(objData.COMMENTS)}")
            queryBuilder.Append($",[gender] = {WrapSqlVariable(objData.GENDER)}")
            queryBuilder.Append($",[title] = {WrapSqlVariable(objData.TITLE)}")
            queryBuilder.Append($",[first_name] = {WrapSqlVariable(objData.FIRST_NAME)}")
            queryBuilder.Append($",[middle_name] = {WrapSqlVariable(objData.MIDDLE_NAME)}")
            queryBuilder.Append($",[prefix] = {WrapSqlVariable(objData.PREFIX)}")
            queryBuilder.Append($",[last_name] = {WrapSqlVariable(objData.LAST_NAME)}")

            If objData.BIRTHDATE Is Nothing Then
                queryBuilder.Append($",[birthdate] = NULL")
            Else
                queryBuilder.Append($",[birthdate] = {WrapSqlVariable(objData.BIRTHDATE)}")
            End If

            queryBuilder.Append($",[birth_place] = {WrapSqlVariable(objData.BIRTH_PLACE)}")
            queryBuilder.Append($",[country_of_birth] = {WrapSqlVariable(objData.COUNTRY_OF_BIRTH)}")
            queryBuilder.Append($",[mothers_name] = {WrapSqlVariable(objData.MOTHERS_NAME)}")
            queryBuilder.Append($",[alias] = {WrapSqlVariable(objData.ALIAS)}")
            queryBuilder.Append($",[full_name_frn] = {WrapSqlVariable(objData.FULL_NAME_FRN)}")
            queryBuilder.Append($",[ssn] = {WrapSqlVariable(objData.SSN)}")
            queryBuilder.Append($",[passport_number] = {WrapSqlVariable(objData.PASSPORT_NUMBER)}")
            queryBuilder.Append($",[passport_country] = {WrapSqlVariable(objData.PASSPORT_COUNTRY)}")
            queryBuilder.Append($",[id_number] = {WrapSqlVariable(objData.ID_NUMBER)}")
            queryBuilder.Append($",[nationality1] = {WrapSqlVariable(objData.NATIONALITY1)}")
            queryBuilder.Append($",[nationality2] = {WrapSqlVariable(objData.NATIONALITY2)}")
            queryBuilder.Append($",[nationality3] = {WrapSqlVariable(objData.NATIONALITY3)}")
            queryBuilder.Append($",[residence] = {WrapSqlVariable(objData.RESIDENCE)}")

            If objData.RESIDENCE_SINCE Is Nothing Then
                queryBuilder.Append($",[residence_since] = NULL")
            Else
                queryBuilder.Append($",[residence_since] = {WrapSqlVariable(objData.RESIDENCE_SINCE)}")
            End If

            queryBuilder.Append($",[occupation] = {WrapSqlVariable(objData.OCCUPATION)}")
            queryBuilder.Append($",[deceased] = {WrapSqlVariable(objData.DECEASED)}")

            If objData.DATE_DECEASED Is Nothing Then
                queryBuilder.Append($",[date_deceased] = NULL")
            Else
                queryBuilder.Append($",[date_deceased] = {WrapSqlVariable(objData.DATE_DECEASED)}")
            End If

            queryBuilder.Append($",[tax_number] = {WrapSqlVariable(objData.TAX_NUMBER)}")
            queryBuilder.Append($",[tax_reg_number] = {WrapSqlVariable(objData.TAX_REG_NUMBER)}")
            queryBuilder.Append($",[source_of_wealth] = {WrapSqlVariable(objData.SOURCE_OF_WEALTH)}")
            queryBuilder.Append($",[is_protected] = {WrapSqlVariable(objData.IS_PROTECTED)}")
            queryBuilder.Append($",[EMPLOYER_NAME] = {WrapSqlVariable(objData.EMPLOYER_NAME)}")
            queryBuilder.Append($",[RELATION_COMMENTS] = {WrapSqlVariable(objData.RELATION_COMMENTS)}")
            queryBuilder.Append($" WHERE PK_goAML_Ref_WIC_Related_Person_ID = {objData.PK_goAML_Ref_Customer_Related_Person_ID}")
            Dim stringQuery As String = queryBuilder.ToString()
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringQuery, Nothing)
        End Sub

        Public Sub Delete(Where As String) Implements IDAL(Of goAML_Ref_Customer_Related_Person).Delete
            Try
                Dim strQuery As String = ""
                strQuery += $"DELETE FROM goAML_Ref_WIC_Related_Person WHERE {Where};"

                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function Save(objData As goAML_Ref_Customer_Related_Person) As Integer Implements IDAL(Of goAML_Ref_Customer_Related_Person).Save
            ' 2023-10-17, Nael
            Dim queryBuilder As New StringBuilder()
            queryBuilder.Append("INSERT INTO goAML_Ref_WIC_Related_Person ")
            queryBuilder.Append("([WIC_No]")
            queryBuilder.Append(",[person_person_relation]")
            queryBuilder.Append(",[relation_date_range_valid_from]")
            queryBuilder.Append(",[relation_date_range_is_approx_from_date]")
            queryBuilder.Append(",[relation_date_range_valid_to]")
            queryBuilder.Append(",[relation_date_range_is_approx_to_date]")
            queryBuilder.Append(",[comments]")
            queryBuilder.Append(",[gender]")
            queryBuilder.Append(",[title]")
            queryBuilder.Append(",[first_name]")
            queryBuilder.Append(",[middle_name]")
            queryBuilder.Append(",[prefix]")
            queryBuilder.Append(",[last_name]")
            queryBuilder.Append(",[Birth_Date]")
            queryBuilder.Append(",[birth_place]")
            queryBuilder.Append(",[country_of_birth]")
            queryBuilder.Append(",[Mother_Name]")
            queryBuilder.Append(",[alias]")
            queryBuilder.Append(",[full_name_frn]")
            queryBuilder.Append(",[ssn]")
            queryBuilder.Append(",[passport_number]")
            queryBuilder.Append(",[passport_country]")
            queryBuilder.Append(",[id_number]")
            queryBuilder.Append(",[nationality1]")
            queryBuilder.Append(",[nationality2]")
            queryBuilder.Append(",[nationality3]")
            queryBuilder.Append(",[residence]")
            queryBuilder.Append(",[residence_since]")
            queryBuilder.Append(",[occupation]")
            queryBuilder.Append(",[deceased]")
            queryBuilder.Append(",[date_deceased]")
            queryBuilder.Append(",[tax_number]")
            queryBuilder.Append(",[Is_PEP]")
            queryBuilder.Append(",[source_of_wealth]")
            queryBuilder.Append(",[is_protected]")
            queryBuilder.Append(",[EMPLOYER_NAME]")
            queryBuilder.Append(",[RELATION_COMMENTS]")
            queryBuilder.Append(") VALUES (")
            queryBuilder.Append($"{WrapSqlVariable(objData.CIF)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.PERSON_PERSON_RELATION)}")

            If objData.RELATION_DATE_RANGE_VALID_FROM Is Nothing Then
                queryBuilder.Append($",NULL")
            Else
                queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_FROM)}")
            End If

            queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_FROM_DATE)}")

            If objData.RELATION_DATE_RANGE_VALID_TO Is Nothing Then
                queryBuilder.Append($",NULL")
            Else
                queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_VALID_TO)}")
            End If

            queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_DATE_RANGE_IS_APPROX_TO_DATE)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.COMMENTS)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.GENDER)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.TITLE)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.FIRST_NAME)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.MIDDLE_NAME)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.PREFIX)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.LAST_NAME)}")

            If objData.BIRTHDATE Is Nothing Then
                queryBuilder.Append($",NULL")
            Else
                queryBuilder.Append($",{WrapSqlVariable(objData.BIRTHDATE)}")
            End If

            queryBuilder.Append($",{WrapSqlVariable(objData.BIRTH_PLACE)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.COUNTRY_OF_BIRTH)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.MOTHERS_NAME)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.ALIAS)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.FULL_NAME_FRN)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.SSN)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.PASSPORT_NUMBER)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.PASSPORT_COUNTRY)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.ID_NUMBER)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.NATIONALITY1)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.NATIONALITY2)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.NATIONALITY3)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.RESIDENCE)}")

            If objData.RESIDENCE_SINCE Is Nothing Then
                queryBuilder.Append($",NULL")
            Else
                queryBuilder.Append($",{WrapSqlVariable(objData.RESIDENCE_SINCE)}")
            End If

            queryBuilder.Append($",{WrapSqlVariable(objData.OCCUPATION)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.DECEASED)}")

            If objData.DATE_DECEASED Is Nothing Then
                queryBuilder.Append($",NULL")
            Else
                queryBuilder.Append($",{WrapSqlVariable(objData.DATE_DECEASED)}")
            End If

            queryBuilder.Append($",{WrapSqlVariable(objData.TAX_NUMBER)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.TAX_REG_NUMBER)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.SOURCE_OF_WEALTH)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.IS_PROTECTED)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.EMPLOYER_NAME)}")
            queryBuilder.Append($",{WrapSqlVariable(objData.RELATION_COMMENTS)}")
            queryBuilder.Append($");")
            queryBuilder.Append("SELECT SCOPE_IDENTITY() AS NewID;")
            Dim stringQuery As String = queryBuilder.ToString()
            Dim row = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, stringQuery, Nothing)
            Dim pk As Integer = row("NewID")
            Return pk
        End Function

        Public Function GetData(Where As String) As List(Of goAML_Ref_Customer_Related_Person) Implements IDAL(Of goAML_Ref_Customer_Related_Person).GetData
            Try
                Dim dataDT As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, $"SELECT * FROM goAML_Ref_WIC_Related_Person WHERE {Where}")
                Dim listReturn As New List(Of DataModel.goAML_Ref_Customer_Related_Person)

                Dim countRows As Integer = dataDT.Rows.Count


                For Each data As DataRow In dataDT.Rows
                    Dim objReturn = New DataModel.goAML_Ref_Customer_Related_Person

                    objReturn.PK_goAML_Ref_Customer_Related_Person_ID = data("PK_goAML_Ref_WIC_Related_Person_ID")
                    If Not data.IsNull("WIC_No") Then
                        objReturn.CIF = data("WIC_No")
                    End If
                    'objReturn.FK_REF_DETAIL_OF = data("FK_REF_DETAIL_OF")
                    If Not data.IsNull("PERSON_PERSON_RELATION") Then
                        objReturn.PERSON_PERSON_RELATION = data("PERSON_PERSON_RELATION")
                    End If
                    If Not data.IsNull("COMMENTS") Then
                        objReturn.COMMENTS = data("COMMENTS")
                    End If
                    If Not data.IsNull("GENDER") Then
                        objReturn.GENDER = data("GENDER")
                    End If
                    If Not data.IsNull("TITLE") Then
                        objReturn.TITLE = data("TITLE")
                    End If
                    'objReturn.FIRST_NAME = data("FIRST_NAME")
                    'objReturn.MIDDLE_NAME = data("MIDDLE_NAME")
                    'objReturn.PREFIX = data("PREFIX")
                    If Not data.IsNull("LAST_NAME") Then
                        objReturn.LAST_NAME = data("LAST_NAME")
                    End If
                    If Not data.IsNull("Birth_Date") Then
                        objReturn.BIRTHDATE = data("Birth_Date")
                    End If
                    If Not data.IsNull("BIRTH_PLACE") Then
                        objReturn.BIRTH_PLACE = data("BIRTH_PLACE")
                    End If
                    'objReturn.COUNTRY_OF_BIRTH = data("COUNTRY_OF_BIRTH")
                    If Not data.IsNull("Mother_Name") Then
                        objReturn.MOTHERS_NAME = data("Mother_Name")
                    End If
                    If Not data.IsNull("ALIAS") Then
                        objReturn.ALIAS = data("ALIAS")
                    End If
                    If Not data.IsNull("SSN") Then
                        objReturn.SSN = data("SSN")
                    End If
                    If Not data.IsNull("PASSPORT_NUMBER") Then
                        objReturn.PASSPORT_NUMBER = data("PASSPORT_NUMBER")
                    End If
                    If Not data.IsNull("PASSPORT_COUNTRY") Then
                        objReturn.PASSPORT_COUNTRY = data("PASSPORT_COUNTRY")
                    End If
                    If Not data.IsNull("ID_NUMBER") Then
                        objReturn.ID_NUMBER = data("ID_NUMBER")
                    End If
                    If Not data.IsNull("NATIONALITY1") Then
                        objReturn.NATIONALITY1 = data("NATIONALITY1")
                    End If
                    If Not data.IsNull("NATIONALITY2") Then
                        objReturn.NATIONALITY1 = data("NATIONALITY2")
                    End If
                    If Not data.IsNull("NATIONALITY3") Then
                        objReturn.NATIONALITY1 = data("NATIONALITY3")
                    End If
                    If Not data.IsNull("RESIDENCE") Then
                        objReturn.RESIDENCE = data("RESIDENCE")
                    End If
                    If Not data.IsNull("OCCUPATION") Then
                        objReturn.OCCUPATION = data("OCCUPATION")
                    End If
                    If Not data.IsNull("DECEASED") Then
                        objReturn.DECEASED = data("DECEASED")
                    End If
                    If Not data.IsNull("DATE_DECEASED") Then
                        objReturn.DATE_DECEASED = data("DATE_DECEASED")
                    End If
                    If Not data.IsNull("TAX_NUMBER") Then
                        objReturn.TAX_NUMBER = data("TAX_NUMBER")
                    End If
                    If Not data.IsNull("Is_PEP") Then
                        objReturn.TAX_REG_NUMBER = data("Is_PEP")
                    End If
                    If Not data.IsNull("EMPLOYER_NAME") Then
                        objReturn.EMPLOYER_NAME = data("EMPLOYER_NAME") ' 2023-10-18, Nael
                    End If
                    If Not data.IsNull("SOURCE_OF_WEALTH") Then
                        objReturn.SOURCE_OF_WEALTH = data("SOURCE_OF_WEALTH")
                    End If
                    If Not data.IsNull("IS_PROTECTED") Then
                        objReturn.IS_PROTECTED = data("IS_PROTECTED")
                    End If
                    If Not data.IsNull("relation_comments") Then
                        objReturn.RELATION_COMMENTS = data("relation_comments")
                    End If

                    listReturn.Add(objReturn)
                Next
                Return listReturn
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Function
    End Class
End Namespace

