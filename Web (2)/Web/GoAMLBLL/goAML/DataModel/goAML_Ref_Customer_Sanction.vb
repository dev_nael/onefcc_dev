﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_Sanction
        'Core
        Public Property PK_goAML_Ref_Customer_Sanction_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer) '' Add 21-Sep-2023, Felix. '' Edit 16-Okt-2023, Nael: Jadi integer asalnya long
        Public Property CIF As String
        Public Property PROVIDER As String
        Public Property SANCTION_LIST_NAME As String
        Public Property MATCH_CRITERIA As String
        Public Property LINK_TO_SOURCE As String
        Public Property SANCTION_LIST_ATTRIBUTES As String
        Public Property SANCTION_LIST_DATE_RANGE_VALID_FROM As Nullable(Of Date)
        Public Property SANCTION_LIST_DATE_RANGE_IS_APPROX_FROM_DATE As Boolean
        Public Property SANCTION_LIST_DATE_RANGE_VALID_TO As Nullable(Of Date)
        Public Property SANCTION_LIST_DATE_RANGE_IS_APPROX_TO_DATE As Boolean
        Public Property COMMENTS As String

        Public Property IsCustomer As Nullable(Of Boolean) ' 2023-11-12, Nael: Menambahkan field IsCustomer sesuai dengan Module


        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class
End Namespace

