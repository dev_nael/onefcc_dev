﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_Related_Entities
        'Core
        Public Property PK_goAML_Ref_Customer_Related_Entities_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property ENTITY_ENTITY_RELATION As String
        Public Property RELATION_DATE_RANGE_VALID_FROM As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_FROM_DATE As Boolean
        Public Property RELATION_DATE_RANGE_VALID_TO As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_TO_DATE As Boolean
        Public Property SHARE_PERCENTAGE As Decimal
        Public Property COMMENTS As String
        Public Property NAME As String
        Public Property COMMERCIAL_NAME As String
        Public Property INCORPORATION_LEGAL_FORM As String
        Public Property INCORPORATION_NUMBER As String
        Public Property BUSINESS As String
        Public Property ENTITY_STATUS As String
        Public Property ENTITY_STATUS_DATE As Nullable(Of Date)
        Public Property INCORPORATION_STATE As String
        Public Property INCORPORATION_COUNTRY_CODE As String
        Public Property INCORPORATION_DATE As Nullable(Of Date)
        Public Property BUSINESS_CLOSED As Boolean
        Public Property DATE_BUSINESS_CLOSED As Nullable(Of Date)
        Public Property TAX_NUMBER As String
        Public Property TAX_REG_NUMBER As String
        Public Property RELATION_COMMENTS As String ' 2023-10-18, Nael


        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        'Grid
        Public Property ObjList_GoAML_Ref_Customer_Phone As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Customer_Address As List(Of goAML_Ref_Address)
        'email
        Public Property ObjList_GoAML_Ref_Customer_Email As List(Of goAML_Ref_Customer_Email)
        'entity identification
        Public Property ObjList_GoAML_Ref_Customer_Entity_Identification As List(Of goAML_Ref_Customer_Entity_Identification)
        'url
        Public Property ObjList_GoAML_Ref_Customer_URL As List(Of goAML_Ref_Customer_URL)
        'sanction
        Public Property ObjList_GoAML_Ref_Customer_Sanction As List(Of goAML_Ref_Customer_Sanction)

    End Class
End Namespace

