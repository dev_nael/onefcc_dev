﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_Entity_Identification
        'Core
        Public Property PK_goAML_Ref_Customer_Entity_Identification_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property TYPE As String
        Public Property NUMBER As String
        Public Property ISSUE_DATE As Nullable(Of Date)
        Public Property EXPIRY_DATE As Nullable(Of Date)
        Public Property ISSUED_BY As String
        Public Property ISSUE_COUNTRY As String
        Public Property COMMENTS As String
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer) ' 2023-10-12, Nael

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class
End Namespace

