﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Phone
        Public Property PK_goAML_Ref_Phone As Integer
        Public Property FK_Ref_Detail_Of As Nullable(Of Integer)
        Public Property FK_for_Table_ID As Integer
        Public Property Tph_Contact_Type As String
        Public Property Tph_Communication_Type As String
        Public Property tph_country_prefix As String
        Public Property tph_number As String
        Public Property tph_extension As String
        Public Property comments As String
        Public Property IsCustomer As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String


    End Class
End Namespace
