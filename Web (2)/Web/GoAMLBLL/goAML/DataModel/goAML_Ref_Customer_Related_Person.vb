﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_Related_Person
        'Core
        Public Property PK_goAML_Ref_Customer_Related_Person_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property PERSON_PERSON_RELATION As String
        Public Property RELATION_DATE_RANGE_VALID_FROM As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_FROM_DATE As Boolean
        Public Property RELATION_DATE_RANGE_VALID_TO As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_TO_DATE As Boolean
        Public Property COMMENTS As String
        Public Property GENDER As String
        Public Property TITLE As String
        Public Property FIRST_NAME As String
        Public Property MIDDLE_NAME As String
        Public Property PREFIX As String
        Public Property LAST_NAME As String
        Public Property BIRTHDATE As String
        Public Property BIRTH_PLACE As String
        Public Property COUNTRY_OF_BIRTH As String
        Public Property MOTHERS_NAME As String
        Public Property [ALIAS] As String
        Public Property FULL_NAME_FRN As String
        Public Property SSN As String
        Public Property PASSPORT_NUMBER As String
        Public Property PASSPORT_COUNTRY As String
        Public Property ID_NUMBER As String
        Public Property NATIONALITY1 As String
        Public Property NATIONALITY2 As String
        Public Property NATIONALITY3 As String
        Public Property RESIDENCE As String
        Public Property RESIDENCE_SINCE As Nullable(Of Date)
        Public Property OCCUPATION As String
        Public Property EMPLOYER_NAME As String
        Public Property DECEASED As Boolean
        Public Property DATE_DECEASED As Nullable(Of Date)
        Public Property TAX_NUMBER As String
        Public Property TAX_REG_NUMBER As Boolean
        Public Property SOURCE_OF_WEALTH As String
        Public Property IS_PROTECTED As Nullable(Of Boolean)
        Public Property RELATION_COMMENTS As String ' 2023-10-18, Nael

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        'Grid
        Public Property ObjList_GoAML_Person_Identification As List(Of goAML_Person_Identification)
        Public Property ObjList_GoAML_Ref_Phone As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address As List(Of goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Phone_Work As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address_Work As List(Of goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Customer_Email As List(Of goAML_Ref_Customer_Email)
        Public Property ObjList_GoAML_Ref_Customer_Sanction As List(Of goAML_Ref_Customer_Sanction)
        Public Property ObjList_GoAML_Ref_Customer_PEP As List(Of goAML_Ref_Customer_PEP)

    End Class
End Namespace

