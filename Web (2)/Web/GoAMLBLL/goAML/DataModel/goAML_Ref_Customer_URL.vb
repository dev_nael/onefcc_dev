﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_URL
        'Core
        Public Property PK_goAML_Ref_Customer_URL_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property URL As String
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer) ' 2023-10-12, Nael

        Public Property IsCustomer As Nullable(Of Boolean) ' 2023-11-12, Nael: Menambahkan field IsCustomer sesuai dengan Module

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class
End Namespace

