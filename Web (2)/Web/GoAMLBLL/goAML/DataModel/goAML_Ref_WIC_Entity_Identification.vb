﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Entity_Identification
        'Core
        Public Property PK_goAML_Ref_WIC_Entity_Identification_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property WIC_No As String
        Public Property Type As String
        Public Property Number As String
        Public Property issue_Date As Nullable(Of Date)
        Public Property expiry_date As Nullable(Of Date)
        Public Property issued_by As String
        Public Property issue_country As String
        Public Property comments As String
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer) ' 2023-10-12, Nael

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class
End Namespace

