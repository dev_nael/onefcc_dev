﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Email
        'Core
        Public Property PK_goAML_Ref_WIC_Email_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property WIC_No As String
        Public Property email_address As String
        Public Property FK_FOR_TABLE_ID As Long '' Add 21-Sep-2023, Felix.

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class
End Namespace

