﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Customer_Email
        'Core
        Public Property PK_goAML_Ref_Customer_Email_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property CIF As String
        Public Property EMAIL_ADDRESS As String
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer) '' Add 21-Sep-2023, Felix. '' Edit 16-Okt-2023, Nael: jadi integer asalnya long

        Public Property IsCustomer As Nullable(Of Boolean) ' 2023-11-12, Nael: Menambahkan field IsCustomer sesuai dengan Module

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class
End Namespace

