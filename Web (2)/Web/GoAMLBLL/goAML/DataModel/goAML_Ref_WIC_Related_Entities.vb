﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Related_Entities
        'Core
        Public Property PK_goAML_Ref_WIC_Related_Entities_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property WIC_No As String
        Public Property Entity_Entity_Relation As String
        Public Property Relation_Date_Range_Valid_From As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_From_Date As Boolean
        Public Property Relation_Date_Range_Valid_To As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_To_Date As Boolean
        Public Property Share_Percentage As Decimal
        Public Property Comments As String
        Public Property Name As String
        Public Property Commercial_Name As String
        Public Property Incorporation_Legal_Form As String
        Public Property Incorporation_Number As String
        Public Property Business As String
        Public Property Entity_Status As String
        Public Property Entity_Status_Date As Nullable(Of Date)
        Public Property Incorporation_State As String
        Public Property Incorporation_Country_Code As String
        Public Property Incorporation_Date As Nullable(Of Date)
        Public Property Business_Closed As Boolean
        Public Property Date_Business_Closed As Nullable(Of Date)
        Public Property Tax_Number As String
        Public Property Tax_Reg_Number As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        'Grid
        Public Property ObjList_GoAML_Ref_WIC_Phone As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_WIC_Address As List(Of goAML_Ref_Address)
        'email
        Public Property ObjList_GoAML_Ref_WIC_Email As List(Of goAML_Ref_WIC_Email)
        'entity identification
        Public Property ObjList_GoAML_Ref_WIC_Entity_Identification As List(Of goAML_Ref_WIC_Entity_Identification)
        'url
        Public Property ObjList_GoAML_Ref_WIC_URL As List(Of goAML_Ref_WIC_URL)
        'sanction
        Public Property ObjList_GoAML_Ref_WIC_Sanction As List(Of goAML_Ref_WIC_Sanction)

    End Class
End Namespace

