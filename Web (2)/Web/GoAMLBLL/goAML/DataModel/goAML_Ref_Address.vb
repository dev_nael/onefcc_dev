﻿Namespace goAML.DataModel
    Public Class goAML_Ref_Address
        Public Property PK_Customer_Address_ID As Integer
        Public Property FK_Ref_Detail_Of As Nullable(Of Integer)
        Public Property FK_To_Table_ID As Nullable(Of Integer)
        Public Property Address_Type As String
        Public Property Address As String
        Public Property Town As String
        Public Property City As String
        Public Property Zip As String
        Public Property Country_Code As String
        Public Property State As String
        Public Property Comments As String
        Public Property IsCustomer As Nullable(Of Boolean)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class
End Namespace