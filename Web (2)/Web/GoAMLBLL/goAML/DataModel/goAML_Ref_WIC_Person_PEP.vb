﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Person_PEP
        'Core
        Public Property PK_goAML_Ref_WIC_PEP_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property FK_FOR_TABLE_ID As Long '' Add 21-Sep-2023, Felix.
        Public Property WIC_No As String
        Public Property pep_country As String
        Public Property function_name As String
        Public Property function_description As String
        Public Property pep_date_range_valid_from As Nullable(Of Date)
        Public Property pep_date_range_is_approx_from_date As Boolean
        Public Property pep_date_range_valid_to As Nullable(Of Date)
        Public Property pep_date_range_is_approx_to_date As Boolean
        Public Property comments As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class
End Namespace
