﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Sanction
        'Core
        Public Property PK_goAML_Ref_WIC_Sanction_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property FK_FOR_TABLE_ID As Long '' Add 21-Sep-2023, Felix.
        Public Property WIC_No As String
        Public Property Provider As String
        Public Property Sanction_List_Name As String
        Public Property Match_Criteria As String
        Public Property Link_To_Source As String
        Public Property Sanction_List_Attributes As String
        Public Property Sanction_List_Date_Range_Valid_From As Nullable(Of Date)
        Public Property Sanction_List_Date_Range_Is_Approx_From_Date As Boolean
        Public Property Sanction_List_Date_Range_Valid_To As Nullable(Of Date)
        Public Property Sanction_List_Date_Range_Is_Approx_To_Date As Boolean
        Public Property Comments As String


        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class
End Namespace

