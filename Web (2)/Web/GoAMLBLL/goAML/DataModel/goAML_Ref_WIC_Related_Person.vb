﻿Namespace goAML.DataModel
    Public Class goAML_Ref_WIC_Related_Person
        'Core
        Public Property PK_goAML_Ref_WIC_Related_Person_ID As Long
        Public Property FK_REF_DETAIL_OF As Integer
        Public Property WIC_No As String
        Public Property Person_Person_Relation As String
        Public Property Relation_Date_Range_Valid_From As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_From_Date As Boolean
        Public Property Relation_Date_Range_Valid_To As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_To_Date As Boolean
        Public Property Comments As String
        Public Property Gender As String
        Public Property Title As String
        Public Property First_Name As String
        Public Property Middle_Name As String
        Public Property Prefix As String
        Public Property Last_Name As String
        Public Property Birth_Date As String
        Public Property Birth_Place As String
        Public Property Country_Of_Birth As String
        Public Property Mother_Name As String
        Public Property [Alias] As String
        Public Property Full_Name_Frn As String
        Public Property SSN As String
        Public Property Passport_Number As String
        Public Property Passport_Country As String
        Public Property ID_Number As String
        Public Property Nationality1 As String
        Public Property Nationality2 As String
        Public Property Nationality3 As String
        Public Property Residence As String
        Public Property Residence_Since As Nullable(Of Date)
        Public Property Occupation As String
        Public Property Deceased As Boolean
        Public Property Date_Deceased As Nullable(Of Date)
        Public Property Tax_Number As String
        Public Property Tax_Reg_Number As Boolean
        Public Property Source_Of_Wealth As String
        Public Property Is_Protected As Nullable(Of Boolean)
        Public Property relation_comments As String
        Public Property EMPLOYER_NAME As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        'Grid
        Public Property ObjList_GoAML_Person_Identification As List(Of goAML_Person_Identification)
        Public Property ObjList_GoAML_Ref_Phone As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address As List(Of goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Phone_Work As List(Of goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address_Work As List(Of goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_WIC_Email As List(Of goAML_Ref_WIC_Email)
        Public Property ObjList_GoAML_Ref_WIC_Sanction As List(Of goAML_Ref_WIC_Sanction)
        Public Property ObjList_GoAML_Ref_WIC_Person_PEP As List(Of goAML_Ref_WIC_Person_PEP)

    End Class
End Namespace

