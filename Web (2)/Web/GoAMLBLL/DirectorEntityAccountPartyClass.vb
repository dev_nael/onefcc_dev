﻿Public Class DirectorEntityAccountPartyClass
    Public objDirectorEntityAccountParty As New GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director
    Public listAddress As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public listPhone As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
    Public listAddressEmployer As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public listPhoneEmployer As New List(Of GoAMLDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
    Public listIdentification As New List(Of GoAMLDAL.goAML_Transaction_Party_Identification)

End Class
