﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Ext.Net
Imports NawaDAL
Imports GoAMLDAL

Public Class goAML_AccountBLL
    Implements IDisposable
    '' edited on 04 Dec 2020
    Shared Function getDataApproval(id As String, moduleName As String) As GoAMLDAL.ModuleApproval
        Dim objModuleApproval As New GoAMLDAL.ModuleApproval
        Using objdb As New GoAMLEntities
            Dim Approval As GoAMLDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function

    Shared Function getPKAccount(id As String) As String
        Dim PKAccount = ""
        Using objdb As New GoAMLEntities
            Dim PK As String = objdb.goAML_Ref_Account.Where(Function(x) x.Account_No = id).FirstOrDefault.PK_Account_ID
            If PK IsNot Nothing Then
                PKAccount = PK
            End If
        End Using
        Return PKAccount
    End Function
    '' end of edit on 04 Dec 2020
    Shared Function GetVw_Account_Signatory(ID As String) As List(Of GoAMLDAL.Vw_Account_Signatory)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Account_Signatory Where x.AccountNo = ID Select x).ToList()
        End Using
    End Function
    Shared Function GetAccount_Signatory() As List(Of GoAMLDAL.goAML_Ref_Account_Signatory)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Account_Signatory Select x).ToList()
        End Using
    End Function


    Shared Function GetVw_Customer_Phones(ID As String) As List(Of GoAMLDAL.Vw_Customer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_Addresses(ID As String) As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function
    'Update: Zikri_12102020
    Shared Function GetVw_WIC_Employer_Phones(id As String) As List(Of GoAMLDAL.Vw_WIC_Employer_Phones)
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As List(Of GoAMLDAL.Vw_WIC_Employer_Phones) = ObjDb.Vw_WIC_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function GetVw_WIC_Phones(id As String) As List(Of GoAMLDAL.Vw_WIC_Phones)
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As List(Of GoAMLDAL.Vw_WIC_Phones) = ObjDb.Vw_WIC_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function Getvw_wic_addresses(id As String) As List(Of GoAMLDAL.vw_wic_addresses)
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As List(Of GoAMLDAL.vw_wic_addresses) = ObjDb.vw_wic_addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function Getvw_wic_employer_addresses(id As String) As List(Of GoAMLDAL.vw_wic_employer_addresses)
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As List(Of GoAMLDAL.vw_wic_employer_addresses) = ObjDb.vw_wic_employer_addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update
    Shared Function GetVw_Customer_AddressesCustomer(ID As String) As List(Of GoAMLDAL.Vw_Customer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_PhonesCustomer(ID As String) As List(Of GoAMLDAL.Vw_Customer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_EmployerPhonesCustomer(ID As String) As List(Of GoAMLDAL.Vw_Employer_Phones)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_EmployerAddressCustomer(ID As String) As List(Of GoAMLDAL.Vw_Employer_Addresses)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_AddressesbyPK(ID As String) As GoAMLDAL.Vw_Customer_Addresses
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.PK_Customer_Address_ID = ID Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetVw_Customer_PhonebyPK(ID As String) As GoAMLDAL.Vw_Customer_Phones
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.PK_goAML_Ref_Phone = ID Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetAccountSignatory(ID As String) As List(Of GoAMLDAL.goAML_Ref_Account_Signatory)
        Using objDb As New GoAMLDAL.GoAMLEntities
            Return (From x In objDb.goAML_Ref_Account_Signatory Where x.FK_Account_No = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomer(ID As Integer) As GoAMLDAL.goAML_Ref_Customer
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
        End Using
    End Function

    'Dedy Added
    Shared Function GetWicByCIF(cif As String) As GoAMLDAL.goAML_Ref_WIC
        Using objDB As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = cif).FirstOrDefault
        End Using
    End Function


    Shared Function GetPhonebyPK(ID As Integer) As GoAMLDAL.goAML_Ref_Phone
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressbyPK(ID As Integer) As GoAMLDAL.goAML_Ref_Address
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetListgoAML_Ref_PhoneByPKID(ID As Integer) As List(Of GoAMLDAL.goAML_Ref_Phone)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function

    Shared Function GetListgoAML_Ref_AddressByPKID(ID As Integer) As List(Of GoAMLDAL.goAML_Ref_Address)
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function
    Shared Function GetTabelTypeForByID(id As Integer) As GoAMLDAL.goAML_For_Table
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_For_Table.Where(Function(x) x.PK_For_Table_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByID(id As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByKeterangan(id As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByKeterangan(id As String) As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByID(id As String) As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByID(id As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByID(id As String) As GoAMLDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerByCIF(cif As String) As GoAMLDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As GoAMLDAL.goAML_Ref_Customer = objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
            'Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
        End Using
    End Function

    Shared Function GetAccountSignatorybyID(id As String) As GoAMLDAL.goAML_Ref_Account_Signatory
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As GoAMLDAL.goAML_Ref_Account_Signatory = objDb.goAML_Ref_Account_Signatory.Where(Function(x) x.PK_Signatory_ID = id).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
            'Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerByCIFandCustomerType(cif As String) As GoAMLDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif And x.FK_Customer_Type_ID = 2).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerEmployerAddressByPK(pkCustomer As String) As GoAMLDAL.goAML_Ref_Address
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = pkCustomer And x.FK_Ref_Detail_Of = 5).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerEmployerPhoneByPK(pkCustomer As String) As GoAMLDAL.goAML_Ref_Phone
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = pkCustomer And x.FK_Ref_Detail_Of = 5).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeDescByCode(kode As String) As GoAMLDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeDescByCode(kode As String) As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetNameSignatoryByID(id As String) As GoAMLDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
        End Using
    End Function

    'Update: Zikri_24092020 Change dropdown to NDFSDropdown
    Shared Function GetCBSignatoryNotPerson(id As String) As GoAMLDAL.goAML_Ref_WIC
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As GoAMLDAL.goAML_Ref_WIC = ObjDb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update

    'Update: Zikri_24092020 Change dropdown to NDFSDropdown
    Shared Function GetMappingAccountTypebyCode(kode As String) As GoAMLDAL.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP
        Using ObjDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Dim result As GoAMLDAL.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP = ObjDb.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP.Where(Function(x) x.Kode = kode).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update

    Shared Function GetRoleSignatoryByID(id As String) As GoAMLDAL.goAML_Ref_Peran_orang_dalam_rekening
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_rekening.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListPeranOrangDalamRekening() As List(Of GoAMLDAL.goAML_Ref_Peran_orang_dalam_rekening)
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_rekening.ToList()
        End Using
    End Function

    Shared Function GetJenisRekeningByID(id As String) As GoAMLDAL.goAML_Ref_Jenis_Rekening
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Rekening.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAccountByAccountNo(id As String) As GoAMLDAL.goAML_Ref_Account
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Account.Where(Function(x) x.Account_No = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListJenisRekening() As List(Of GoAMLDAL.goAML_Ref_Jenis_Rekening)
        'done:code  GetListEmailTableTypes
        Using objDb As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            Return objDb.goAML_Ref_Jenis_Rekening.ToList()
        End Using
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    Function SaveEditApproval(objData As GoAMLDAL.goAML_Ref_Account, objModule As NawaDAL.Module, objListgoAML_Ref_Account_Signatory As List(Of GoAMLDAL.goAML_Ref_Account_Signatory), objListgoAML_Related_Entitis As List(Of goAML_Ref_Account_Related_Entity), objListgoAML_Related_Person As List(Of goAML_Ref_Account_Related_Person), objListgoAML_Related_Account As List(Of goAML_Ref_Account_Related_Account), objListgoAML_Sanctions As List(Of goAML_Ref_Account_Sanction)) As Boolean
        'done:code  SaveAddApproval
        Using objdb As New NawaDAL.NawaDataEntities
            Using objdbDev As New GoAMLDAL.GoAMLEntities
                Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                    Try
                        Dim objxData As New GoAMLBLL.goAML_AccountDataBLL
                        objxData.objgoAML_Ref_Account = objData
                        objxData.objListgoAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory
                        objxData.objListgoAML_RelatedEntities = objListgoAML_Related_Entitis
                        objxData.objListgoAML_RelatedPerson = objListgoAML_Related_Person
                        objxData.objListgoAML_RelatedAccount = objListgoAML_Related_Account
                        objxData.objListgoAML_Sanction = objListgoAML_Sanctions
                        Dim xmldata As String = NawaBLL.Common.Serialize(objxData)

                        Dim objgoAML_AccountBefore As GoAMLDAL.goAML_Ref_Account = objdbDev.goAML_Ref_Account.Where(Function(x) x.PK_Account_ID = objxData.objgoAML_Ref_Account.PK_Account_ID).FirstOrDefault
                        Dim objListgoAML_Ref_Account_SignatoryBefore As List(Of GoAMLDAL.goAML_Ref_Account_Signatory) = objdbDev.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objxData.objgoAML_Ref_Account.Account_No).ToList
                        Dim objListgoAML_RelatedEntitiesBefore As List(Of goAML_Ref_Account_Related_Entity) = getRelatedEntitiesByID(objxData.objgoAML_Ref_Account.Account_No)
                        Dim objListgoAML_RelatedAccountBefore As List(Of goAML_Ref_Account_Related_Account) = getRelatedAccountByID(objxData.objgoAML_Ref_Account.Account_No)
                        Dim objListgoAML_RelatedPersonBefore As List(Of goAML_Ref_Account_Related_Person) = getRelatedPersonByID(objxData.objgoAML_Ref_Account.Account_No)
                        Dim objListgoAML_SanctionBefore As List(Of goAML_Ref_Account_Sanction) = getSanctionByID(objxData.objgoAML_Ref_Account.Account_No)

                        Dim objxDatabefore As New GoAMLBLL.goAML_AccountDataBLL
                        objxDatabefore.objgoAML_Ref_Account = objgoAML_AccountBefore
                        objxDatabefore.objListgoAML_RelatedAccount = objListgoAML_RelatedAccountBefore
                        objxDatabefore.objListgoAML_RelatedPerson = objListgoAML_RelatedPersonBefore
                        objxDatabefore.objListgoAML_RelatedEntities = objListgoAML_RelatedEntitiesBefore
                        objxDatabefore.objListgoAML_Sanction = objListgoAML_SanctionBefore
                        objxDatabefore.objListgoAML_Ref_Account_Signatory = objListgoAML_Ref_Account_SignatoryBefore

                        Dim xmlbefore As String = NawaBLL.Common.Serialize(objxDatabefore)

                        Dim objModuleApproval As New NawaDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = objxData.objgoAML_Ref_Account.PK_Account_ID
                            .ModuleField = xmldata
                            .ModuleFieldBefore = xmlbefore
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                        'Using objDba As New GoAMLEntities
                        '    Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                        '    'AuditTrailDetail
                        '    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objData)
                        '    If objListgoAML_Ref_Account_Signatory.Count > 0 Then
                        '        For Each signatory As GoAMLDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                        '            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                        '        Next
                        '    End If
                        'End Using
                        Dim strQuery = ""
                        ' Audit Trail
                        strQuery = "INSERT INTO AuditTrailHeader ( "
                        strQuery += "ApproveBy,"
                        strQuery += "CreatedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "FK_AuditTrailStatus_ID,"
                        strQuery += "FK_ModuleAction_ID,"
                        strQuery += "ModuleLabel"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                        strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Update & "'"
                        strQuery += " , '" & objModule.ModuleLabel & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)
                        If objListgoAML_Related_Entitis Is Nothing Then
                            objListgoAML_Related_Entitis = New List(Of goAML_Ref_Account_Related_Entity)
                        End If
                        ' Audit Related Entities
                        strQuery = ""
                        If objListgoAML_Related_Entitis IsNot Nothing AndAlso objListgoAML_Related_Entitis.Count > 0 Then
                            For Each item In objListgoAML_Related_Entitis
                                strQuery = ""
                                Dim objectdata As Object
                                objectdata = item
                                Dim objtype As Type = objectdata.GetType
                                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                                For Each itemEntities As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    If Not itemEntities.GetValue(objectdata, Nothing) Is Nothing Then
                                        If itemEntities.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = itemEntities.GetValue(objectdata, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    strQuery = "INSERT INTO AuditTrailDetail(  "
                                    strQuery += "FK_AuditTrailHeader_ID,"
                                    strQuery += "FieldName,"
                                    strQuery += "OldValue,"
                                    strQuery += "NewValue"
                                    strQuery += " ) VALUES ( "
                                    strQuery += " " & PKAudit & ""
                                    strQuery += " ,'" & itemEntities.Name & "'"
                                    strQuery += " , ''"
                                    strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                                Next

                            Next
                        End If

                        strQuery = "INSERT INTO AuditTrailHeader ( "
                        strQuery += "ApproveBy,"
                        strQuery += "CreatedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "FK_AuditTrailStatus_ID,"
                        strQuery += "FK_ModuleAction_ID,"
                        strQuery += "ModuleLabel"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                        strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Update & "'"
                        strQuery += " , '" & objModule.ModuleLabel & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim PKAuditPerson As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)
                        If objListgoAML_Related_Person Is Nothing Then
                            objListgoAML_Related_Person = New List(Of goAML_Ref_Account_Related_Person)
                        End If
                        ' Audit Related Person
                        strQuery = ""
                        If objListgoAML_Related_Person IsNot Nothing AndAlso objListgoAML_Related_Person.Count > 0 Then
                            For Each item In objListgoAML_Related_Person
                                strQuery = ""
                                Dim objectdata As Object
                                objectdata = item
                                Dim objtype As Type = objectdata.GetType
                                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                                For Each itemPerson As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    If Not itemPerson.GetValue(objectdata, Nothing) Is Nothing Then
                                        If itemPerson.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = itemPerson.GetValue(objectdata, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    strQuery = "INSERT INTO AuditTrailDetail(  "
                                    strQuery += "FK_AuditTrailHeader_ID,"
                                    strQuery += "FieldName,"
                                    strQuery += "OldValue,"
                                    strQuery += "NewValue"
                                    strQuery += " ) VALUES ( "
                                    strQuery += " " & PKAudit & ""
                                    strQuery += " ,'" & itemPerson.Name & "'"
                                    strQuery += " , ''"
                                    strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                                Next

                            Next
                        End If

                        strQuery = ""
                        strQuery = "INSERT INTO AuditTrailHeader ( "
                        strQuery += "ApproveBy,"
                        strQuery += "CreatedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "FK_AuditTrailStatus_ID,"
                        strQuery += "FK_ModuleAction_ID,"
                        strQuery += "ModuleLabel"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                        strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Update & "'"
                        strQuery += " , '" & objModule.ModuleLabel & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim PKAuditAccount As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                        If objListgoAML_Related_Account Is Nothing Then
                            objListgoAML_Related_Account = New List(Of goAML_Ref_Account_Related_Account)
                        End If
                        ' Audit Related Account
                        strQuery = ""
                        If objListgoAML_Related_Account IsNot Nothing Then
                            For Each item In objListgoAML_Related_Account
                                strQuery = ""
                                Dim objectdata As Object
                                objectdata = item
                                Dim objtype As Type = objectdata.GetType
                                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                                For Each itemAccount As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    If Not itemAccount.GetValue(objectdata, Nothing) Is Nothing Then
                                        If itemAccount.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = itemAccount.GetValue(objectdata, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    strQuery = "INSERT INTO AuditTrailDetail(  "
                                    strQuery += "FK_AuditTrailHeader_ID,"
                                    strQuery += "FieldName,"
                                    strQuery += "OldValue,"
                                    strQuery += "NewValue"
                                    strQuery += " ) VALUES ( "
                                    strQuery += " " & PKAudit & ""
                                    strQuery += " ,'" & itemAccount.Name & "'"
                                    strQuery += " , ''"
                                    strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                                Next

                            Next
                        End If


                        If objListgoAML_Sanctions Is Nothing Then
                            objListgoAML_Sanctions = New List(Of goAML_Ref_Account_Sanction)
                        End If

                        strQuery = ""
                        ' Audit Trail
                        strQuery = "INSERT INTO AuditTrailHeader ( "
                        strQuery += "ApproveBy,"
                        strQuery += "CreatedBy,"
                        strQuery += "CreatedDate,"
                        strQuery += "FK_AuditTrailStatus_ID,"
                        strQuery += "FK_ModuleAction_ID,"
                        strQuery += "ModuleLabel"
                        strQuery += " ) VALUES ( "
                        strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                        strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                        strQuery += " , '" & NawaBLL.Common.ModuleActionEnum.Update & "'"
                        strQuery += " , '" & objModule.ModuleLabel & "')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim PKAuditSanction As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                        ' Audit Related Sanction
                        strQuery = ""
                        If objListgoAML_Sanctions IsNot Nothing AndAlso objListgoAML_Sanctions.Count > 0 Then
                            For Each item In objListgoAML_Sanctions
                                strQuery = ""
                                Dim objectdata As Object
                                objectdata = item
                                Dim objtype As Type = objectdata.GetType
                                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                                For Each itemSanction As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    If Not itemSanction.GetValue(objectdata, Nothing) Is Nothing Then
                                        If itemSanction.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = itemSanction.GetValue(objectdata, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    strQuery = "INSERT INTO AuditTrailDetail(  "
                                    strQuery += "FK_AuditTrailHeader_ID,"
                                    strQuery += "FieldName,"
                                    strQuery += "OldValue,"
                                    strQuery += "NewValue"
                                    strQuery += " ) VALUES ( "
                                    strQuery += " " & PKAudit & ""
                                    strQuery += " ,'" & itemSanction.Name & "'"
                                    strQuery += " , ''"
                                    strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                                Next

                            Next
                        End If


                        objdb.SaveChanges()
                        objtrans.Commit()
                        'NawaBLL.Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)

                    Catch ex As Exception
                        objtrans.Rollback()
                        Throw
                    End Try
                End Using
            End Using
        End Using
    End Function

    Sub SaveEditTanpaApproval(objData As GoAMLDAL.goAML_Ref_Account, objModule As NawaDAL.Module, objListgoAML_Ref_Account_Signatory As List(Of GoAMLDAL.goAML_Ref_Account_Signatory))
        Using objDB As New GoAMLDAL.GoAMLEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Modified

                    For Each itemx As GoAMLDAL.goAML_Ref_Account_Signatory In objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objData.Account_No).ToList()
                        Dim objcek As GoAMLDAL.goAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = itemx.PK_Signatory_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next
                    For Each item As GoAMLDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                        Dim obcek As GoAMLDAL.goAML_Ref_Account_Signatory = (From x In objDB.goAML_Ref_Account_Signatory Where x.PK_Signatory_ID = item.PK_Signatory_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.Active = 1
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.CreatedDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    Using objDba As New GoAMLEntities
                        Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                        'AuditTrailDetail
                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objData)
                        If objListgoAML_Ref_Account_Signatory.Count > 0 Then
                            For Each signatory As GoAMLDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                            Next
                        End If
                    End Using

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub


    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub


    Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)

        'done: loadPanel

        Dim objgoAML_AccountDataBLL As GoAMLBLL.goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(GoAMLBLL.goAML_AccountDataBLL))
        Dim objAccount As GoAMLDAL.goAML_Ref_Account = objgoAML_AccountDataBLL.objgoAML_Ref_Account
        Dim objRelatedEntities As List(Of goAML_Ref_Account_Related_Entity) = objgoAML_AccountDataBLL.objListgoAML_RelatedEntities
        Dim objRelatedAccount As List(Of goAML_Ref_Account_Related_Account) = objgoAML_AccountDataBLL.objListgoAML_RelatedAccount
        Dim objRelatedPerson As List(Of goAML_Ref_Account_Related_Person) = objgoAML_AccountDataBLL.objListgoAML_RelatedPerson
        Dim objSanction As List(Of goAML_Ref_Account_Sanction) = objgoAML_AccountDataBLL.objListgoAML_Sanction
        'Dim objListEmailTemplatedetail As List(Of NawaDAL.EmailTemplateDetail) = GetListEmailTemplateDetailByPKID(unikkey)
        'Dim objListEmailTemplateAction As List(Of NawaDAL.EmailTemplateAction) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateActionByPKID(unikkey)
        'Dim objListEmailTemplateAttachment As List(Of NawaDAL.EmailTemplateAttachment) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateAttachmentByPKID(unikkey)

        Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
        Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
        Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
        Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

        Using db As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
            If Not objAccount Is Nothing Then
                Dim strunik As String = unikkey
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_Account_ID" & strunik, objAccount.PK_Account_ID)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account Name", "Account_Name" & strunik, objAccount.Account_Name)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objAccount.Account_No)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Branch", "Branch" & strunik, objAccount.Branch)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "IBAN", "IBAN" & strunik, objAccount.IBAN)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Clint Number", "client_number" & strunik, objAccount.client_number)
                ' 6 Okt 2022 : Ari penambahan kondisi jika personal account type kosong
                If objAccount.personal_account_type IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Personal Account Type", "personal_account_type" & strunik, GetJenisRekeningByID(objAccount.personal_account_type).Keterangan)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Personal Account Type", "personal_account_type" & strunik, "")
                End If
                'Update: Zikri_16102020
                If Not objAccount.balance Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Balance", "balance" & strunik, objAccount.balance)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Balance", "balance" & strunik, "")
                End If
                If Not objAccount.date_balance Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Balance", "date_balance" & strunik, CDate(objAccount.date_balance).ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Balance", "date_balance" & strunik, "")
                End If
                'End Update
                If goAML_CustomerBLL.GetStatusRekeningbyID(objAccount.status_code) IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Code", "status_code" & strunik, goAML_CustomerBLL.GetStatusRekeningbyID(objAccount.status_code).Keterangan)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Code", "status_code" & strunik, "")
                End If
                If objAccount.isUpdateFromDataSource = True Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source", "isUpdateFromDataSource" & strunik, "Ya")
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source", "isUpdateFromDataSource" & strunik, "Tidak")
                End If
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Beneficiary", "beneficiary" & strunik, objAccount.beneficiary)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Beneficiary Comment", "beneficiary_comment" & strunik, objAccount.beneficiary_comment)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "comments" & strunik, objAccount.comments)
                'Update BSIM - 07Oct2020
                If Not objAccount.opened Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Opened Date", "Opened" & strunik, CDate(objAccount.opened).ToString("dd-MMM-yyyy"))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Opened Date", "Opened" & strunik, "")
                End If
                If Not objAccount.closed Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed Date", "Closed" & strunik, CDate(objAccount.closed).ToString("dd-MMM-yyyy"))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed Date", "Closed" & strunik, "")
                End If
                '*****end update***

                If Not String.IsNullOrEmpty(objAccount.FK_CIF_Entity_ID) Then
                    Dim objentitiy As GoAMLDAL.goAML_Ref_Customer = db.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.FK_CIF_Entity_ID).FirstOrDefault()
                    If objentitiy IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corporate Account", "Rekening_Corp" & strunik, "Yes")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objentitiy.CIF)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Name", "Corp_Name" & strunik, objentitiy.Corp_Name)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Commercial Name", "Corp_Commercial_Name" & strunik, objentitiy.Corp_Commercial_Name)
                        If Not goAML_CustomerBLL.GetBentukBadanUsahaByID(objentitiy.Corp_Incorporation_Legal_Form) Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Legal Form", "Corp_Incorporation_Legal_Form" & strunik, goAML_CustomerBLL.GetBentukBadanUsahaByID(objentitiy.Corp_Incorporation_Legal_Form).Keterangan)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Legal Form", "Corp_Incorporation_Legal_Form" & strunik, "")
                        End If
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corporate  Incorporation Number", "Corp_Incorporation_Number" & strunik, objentitiy.Corp_Incorporation_Number)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Business", "Corp_Business" & strunik, objentitiy.Corp_Business)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "Corp_Email" & strunik, objentitiy.Corp_Email)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Url", "Corp_Url" & strunik, objentitiy.Corp_Url)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "State", "Corp_Incorporation_State" & strunik, objentitiy.Corp_Incorporation_State)
                        If Not goAML_CustomerBLL.GetNamaNegaraByID(objentitiy.Corp_Incorporation_Country_Code) Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country", "Corp_Incorporation_Country_Code" & strunik, goAML_CustomerBLL.GetNamaNegaraByID(objentitiy.Corp_Incorporation_Country_Code).Keterangan)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country", "Corp_Incorporation_Country_Code" & strunik, "")
                        End If
                        'update BSIM - 07Oct2020
                        If Not objentitiy.Corp_Incorporation_Date Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date", "Corp_Incorporation_Date" & strunik, objentitiy.Corp_Incorporation_Date)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date", "Corp_Incorporation_Date" & strunik, "")
                        End If
                        If Not objentitiy.Corp_Business_Closed Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed?", "Corp_Business_Closed" & strunik, objentitiy.Corp_Business_Closed)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed?", "Corp_Business_Closed" & strunik, False)
                        End If
                        '*****end update***
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tax Number", "Corp_Tax_Number" & strunik, objentitiy.Corp_Tax_Number)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "Corp_Comments" & strunik, objentitiy.Corp_Comments)

                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corporate Account", "Rekening_Corp" & strunik, "No")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Name", "Corp_Name" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Commercial Name", "Corp_Commercial_Name" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Legal Form", "Corp_Incorporation_Legal_Form" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Number", "Corp_Incorporation_Number" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Business", "Corp_Business" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "Corp_Email" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Url", "Corp_Url" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "State", "Corp_Incorporation_State" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country", "Corp_Incorporation_Country_Code" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date", "Corp_Incorporation_Date" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed?", "Corp_Business_Closed" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tax Number", "Corp_Tax_Number" & strunik, "")
                        'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "Corp_Comments" & strunik, "")
                    End If
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corporate Account", "Rekening_Corp" & strunik, "No")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Name", "Corp_Name" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Commercial Name", "Corp_Commercial_Name" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Legal Form", "Corp_Incorporation_Legal_Form" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Number", "Corp_Incorporation_Number" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Business", "Corp_Business" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "Corp_Email" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Url", "Corp_Url" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "State", "Corp_Incorporation_State" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country", "Corp_Incorporation_Country_Code" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Pendirian", "Corp_Incorporation_Date" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed?", "Corp_Business_Closed" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tax Number", "Corp_Tax_Number" & strunik, "")
                    '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "Corp_Comments" & strunik, "")
                End If
                'INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objCustomer.Gender).FirstOrDefault
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objCustomer.Balance)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objCustomer.Date_Balance).ToString("dd-MMM-yyyy"))

                'INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objCustomer.Status_Code).FirstOrDefault
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objCustomer.Beneficiary)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objCustomer.Beneficiary_Comment)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, IIf(String.IsNullOrEmpty(objCustomer.Comments), "", objCustomer.Comments))

                'If objEmailTemplate.FK_Monitoringduration_ID <> "1" Then
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime)
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

                'Else
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

                'End If

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objEmailTemplate.Active)

                '' Signatory
                Dim objStore As New Ext.Net.Store
                objStore.ID = strunik & "StoreGrid"
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModel As New Ext.Net.Model
                Dim objField As Ext.Net.ModelField

                objField = New Ext.Net.ModelField
                objField.Name = "PK_Signatory_ID"
                objField.Type = ModelFieldType.Auto
                objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_Ref_Detail_Of"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_for_Table_ID"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_Account_No"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Role"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "isPrimary"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)


                objField = New Ext.Net.ModelField
                objField.Name = "FK_CIF_Person_ID"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "WIC_No"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)
                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_country_prefix"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_country_number"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_extension"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "comments"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)


                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_EmailTemplate_ID"
                'objField.Type = ModelFieldType.Auto
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_EmailTableType_ID"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "EmailTableType"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)

                objStore.Model.Add(objModel)

                Dim objListcolumn As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumn.Add(objcolumnNo)
                End Using

                Dim objColum As Ext.Net.Column
                'Dim objColumwic As Ext.Net.Column

                Dim objColumname As Ext.Net.Column

                objColumname = New Ext.Net.Column
                objColumname.Text = "Name"
                objColumname.DataIndex = "FK_CIF_Person_ID"
                objColumname.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumname.Flex = 1
                objListcolumn.Add(objColumname)

                'objColumwic = New Ext.Net.Column
                'objColumwic.Text = "Name"
                'objColumwic.DataIndex = "WIC_No"
                'objColumwic.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumwic.Flex = 1
                'objListcolumn.Add(objColumwic)

                objColum = New Ext.Net.Column
                objColum.Text = "Role"
                objColum.DataIndex = "Role"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "isPrimary"
                objColum.DataIndex = "isPrimary"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)


                Dim objdtSignatory As Data.DataTable = CopyGenericToDataTable(objgoAML_AccountDataBLL.objListgoAML_Ref_Account_Signatory)


                For Each item As DataRow In objdtSignatory.Rows
                    If Not item.IsNull("FK_CIF_Person_ID") Then
                        item("FK_CIF_Person_ID") = GetNameSignatoryByID(item("FK_CIF_Person_ID")).INDV_Last_Name
                        'objColumname.Hidden = False
                        'objColumwic.Hidden = True
                    Else
                        item("FK_CIF_Person_ID") = GetCBSignatoryNotPerson(item("WIC_No")).INDV_Last_Name
                        'item("WIC_No") = GetCBSignatoryNotPerson(item("WIC_No")).INDV_Last_Name
                        'objColumname.Hidden = True
                        'objColumwic.Hidden = False
                    End If
                Next

                For Each item As DataRow In objdtSignatory.Rows
                    item("Role") = GetRoleSignatoryByID(item("Role")).Keterangan
                Next

                For Each item As DataRow In objdtSignatory.Rows
                    item("isPrimary") = Convert.ToBoolean(IIf(item("isPrimary") = True, 1, 0))
                Next

                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Signatory", objStore, objListcolumn, objdtSignatory)

                ' 20 Feb 2023 Ari : Penambahan Data Related dan Sanction
                'Related Entities
                Dim objStoreRelatedEntities As New Ext.Net.Store
                objStoreRelatedEntities.ID = strunik & "StoreGridRelatedEntities"
                objStoreRelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelRelatedEntities As New Ext.Net.Model
                Dim objFieldRelatedEntities As Ext.Net.ModelField

                objFieldRelatedEntities = New Ext.Net.ModelField
                objFieldRelatedEntities.Name = "PK_goAML_Ref_Account_Related_Entity_ID"
                objFieldRelatedEntities.Type = ModelFieldType.Auto
                objModelRelatedEntities.Fields.Add(objFieldRelatedEntities)

                objFieldRelatedEntities = New Ext.Net.ModelField
                objFieldRelatedEntities.Name = "ACCOUNT_ENTITY_RELATION"
                objFieldRelatedEntities.Type = ModelFieldType.Auto
                objModelRelatedEntities.Fields.Add(objFieldRelatedEntities)

                objFieldRelatedEntities = New Ext.Net.ModelField
                objFieldRelatedEntities.Name = "name"
                objFieldRelatedEntities.Type = ModelFieldType.Auto
                objModelRelatedEntities.Fields.Add(objFieldRelatedEntities)



                objStoreRelatedEntities.Model.Add(objModelRelatedEntities)


                Dim objListcolumnRelatedEntities As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnRelatedEntities.Add(objcolumnNo)
                End Using


                Dim objColumRelatedEntities As Ext.Net.Column

                objColumRelatedEntities = New Ext.Net.Column
                objColumRelatedEntities.Text = "Relation"
                objColumRelatedEntities.DataIndex = "ACCOUNT_ENTITY_RELATION"
                objColumRelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumRelatedEntities.Flex = 1
                objListcolumnRelatedEntities.Add(objColumRelatedEntities)

                objColumRelatedEntities = New Ext.Net.Column
                objColumRelatedEntities.Text = "Nama"
                objColumRelatedEntities.DataIndex = "name"
                objColumRelatedEntities.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumRelatedEntities.Flex = 1
                objListcolumnRelatedEntities.Add(objColumRelatedEntities)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "RelatedEntities", objStoreRelatedEntities, objListcolumnRelatedEntities, objdtRelatedEntities)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Entities", objStoreRelatedEntities, objListcolumnRelatedEntities, objRelatedEntities)

                ''Related Person
                'Dim objStoreRelatedPerson As New Ext.Net.Store
                'objStoreRelatedPerson.ID = strunik & "StoreGridRelatedPerson"
                'objStoreRelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'Dim objModelRelatedPerson As New Ext.Net.Model
                'Dim objFieldRelatedPerson As Ext.Net.ModelField

                'objFieldRelatedPerson = New Ext.Net.ModelField
                'objFieldRelatedPerson.Name = "PK_goAML_Ref_Account_Related_Person_ID"
                'objFieldRelatedPerson.Type = ModelFieldType.Auto
                'objModelRelatedPerson.Fields.Add(objFieldRelatedPerson)

                'objFieldRelatedPerson = New Ext.Net.ModelField
                'objFieldRelatedPerson.Name = "ROLE"
                'objFieldRelatedPerson.Type = ModelFieldType.Auto
                'objModelRelatedPerson.Fields.Add(objFieldRelatedPerson)

                'objFieldRelatedPerson = New Ext.Net.ModelField
                'objFieldRelatedPerson.Name = "last_name"
                'objFieldRelatedPerson.Type = ModelFieldType.Auto
                'objModelRelatedPerson.Fields.Add(objFieldRelatedPerson)

                'objFieldRelatedPerson = New Ext.Net.ModelField
                'objFieldRelatedPerson.Name = "IS_PRIMARY"
                'objFieldRelatedPerson.Type = ModelFieldType.Auto
                'objModelRelatedPerson.Fields.Add(objFieldRelatedPerson)



                'objStoreRelatedPerson.Model.Add(objModelRelatedPerson)


                'Dim objListcolumnRelatedPerson As New List(Of ColumnBase)
                'Using objcolumnNo As New Ext.Net.RowNumbererColumn
                '    objcolumnNo.Text = "No."
                '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                '    objListcolumnRelatedPerson.Add(objcolumnNo)
                'End Using


                'Dim objColumRelatedPerson As Ext.Net.Column

                'objColumRelatedPerson = New Ext.Net.Column
                'objColumRelatedPerson.Text = "Role"
                'objColumRelatedPerson.DataIndex = "ROLE"
                'objColumRelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumRelatedPerson.Flex = 1
                'objListcolumnRelatedPerson.Add(objColumRelatedPerson)

                'objColumRelatedPerson = New Ext.Net.Column
                'objColumRelatedPerson.Text = "Nama"
                'objColumRelatedPerson.DataIndex = "last_name"
                'objColumRelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumRelatedPerson.Flex = 1
                'objListcolumnRelatedPerson.Add(objColumRelatedPerson)

                'objColumRelatedPerson = New Ext.Net.Column
                'objColumRelatedPerson.Text = "Is Primary"
                'objColumRelatedPerson.DataIndex = "IS_PRIMARY"
                'objColumRelatedPerson.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumRelatedPerson.Flex = 1
                'objListcolumnRelatedPerson.Add(objColumRelatedPerson)

                ''Dim listObjRelatedPerson As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Account_Related_Person where ACCOUNT_NO = '" & objAccount.Account_No & "'", Nothing)


                ''NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "RelatedPerson", objStoreRelatedPerson, objListcolumnRelatedPerson, objdtRelatedPerson)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Person", objStoreRelatedPerson, objListcolumnRelatedPerson, objListcolumnRelatedPerson)

                'Related Account
                Dim objStoreRelatedAccount As New Ext.Net.Store
                objStoreRelatedAccount.ID = strunik & "StoreGridRelatedAccount"
                objStoreRelatedAccount.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelRelatedAccount As New Ext.Net.Model
                Dim objFieldRelatedAccount As Ext.Net.ModelField

                objFieldRelatedAccount = New Ext.Net.ModelField
                objFieldRelatedAccount.Name = "PK_goAML_Ref_Account_Rel_Account_ID"
                objFieldRelatedAccount.Type = ModelFieldType.Auto
                objModelRelatedAccount.Fields.Add(objFieldRelatedAccount)

                objFieldRelatedAccount = New Ext.Net.ModelField
                objFieldRelatedAccount.Name = "ACCOUNT_ACCOUNT_RELATIONSHIP"
                objFieldRelatedAccount.Type = ModelFieldType.Auto
                objModelRelatedAccount.Fields.Add(objFieldRelatedAccount)

                objFieldRelatedAccount = New Ext.Net.ModelField
                objFieldRelatedAccount.Name = "account_name"
                objFieldRelatedAccount.Type = ModelFieldType.Auto
                objModelRelatedAccount.Fields.Add(objFieldRelatedAccount)



                objStoreRelatedAccount.Model.Add(objModelRelatedAccount)


                Dim objListcolumnRelatedAccount As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnRelatedAccount.Add(objcolumnNo)
                End Using


                Dim objColumRelatedAccount As Ext.Net.Column

                objColumRelatedAccount = New Ext.Net.Column
                objColumRelatedAccount.Text = "Relation"
                objColumRelatedAccount.DataIndex = "ACCOUNT_ACCOUNT_RELATIONSHIP"
                objColumRelatedAccount.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumRelatedAccount.Flex = 1
                objListcolumnRelatedAccount.Add(objColumRelatedAccount)

                objColumRelatedAccount = New Ext.Net.Column
                objColumRelatedAccount.Text = "Nama"
                objColumRelatedAccount.DataIndex = "account_name"
                objColumRelatedAccount.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumRelatedAccount.Flex = 1
                objListcolumnRelatedAccount.Add(objColumRelatedAccount)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "RelatedAccount", objStoreRelatedAccount, objListcolumnRelatedAccount, objdtRelatedAccount)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Related Account", objStoreRelatedAccount, objListcolumnRelatedAccount, objRelatedAccount)

                'Sanction
                Dim objStoreSanction As New Ext.Net.Store
                objStoreSanction.ID = strunik & "StoreGridSanction"
                objStoreSanction.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelSanction As New Ext.Net.Model
                Dim objFieldSanction As Ext.Net.ModelField

                objFieldSanction = New Ext.Net.ModelField
                objFieldSanction.Name = "PK_goAML_Ref_Account_Sanction_ID"
                objFieldSanction.Type = ModelFieldType.Auto
                objModelSanction.Fields.Add(objFieldSanction)

                objFieldSanction = New Ext.Net.ModelField
                objFieldSanction.Name = "sanction_list_name"
                objFieldSanction.Type = ModelFieldType.Auto
                objModelSanction.Fields.Add(objFieldSanction)

                objFieldSanction = New Ext.Net.ModelField
                objFieldSanction.Name = "provider"
                objFieldSanction.Type = ModelFieldType.Auto
                objModelSanction.Fields.Add(objFieldSanction)

                'objFieldSanction = New Ext.Net.ModelField
                'objFieldSanction.Name = "comments"
                'objFieldSanction.Type = ModelFieldType.Auto
                'objModelSanction.Fields.Add(objFieldSanction)



                objStoreSanction.Model.Add(objModelSanction)


                Dim objListcolumnSanction As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnSanction.Add(objcolumnNo)
                End Using


                Dim objColumSanction As Ext.Net.Column

                objColumSanction = New Ext.Net.Column
                objColumSanction.Text = "Name"
                objColumSanction.DataIndex = "sanction_list_name"
                objColumSanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumSanction.Flex = 1
                objListcolumnSanction.Add(objColumSanction)

                objColumSanction = New Ext.Net.Column
                objColumSanction.Text = "Provider"
                objColumSanction.DataIndex = "provider"
                objColumSanction.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumSanction.Flex = 1
                objListcolumnSanction.Add(objColumSanction)

                'objColumSanction = New Ext.Net.Column
                'objColumSanction.Text = "Comments"
                'objColumSanction.DataIndex = "comments"
                'objColumSanction.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumSanction.Flex = 1
                'objListcolumnSanction.Add(objColumSanction)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Sanction", objStoreSanction, objListcolumnSanction, objdtSanction)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Sanction", objStoreSanction, objListcolumnSanction, objSanction)



                'objColum = New Ext.Net.Column
                'objColum.Text = "Country Prefix"
                'objColum.DataIndex = "tph_country_prefix"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Number"
                'objColum.DataIndex = "tph_number"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Extension"
                'objColum.DataIndex = "tph_extension"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Comments"
                'objColum.DataIndex = "comments"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                ''' Phone -> tbl Reference Contact Type
                'Dim objdt As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)
                'Dim objcol As New Data.DataColumn
                'objcol.ColumnName = "ContactType"
                'objcol.DataType = GetType(String)
                'objdt.Columns.Add(objcol)

                'For Each item As DataRow In objdt.Rows
                '    Dim objtask As GoAMLDAL.goAML_Ref_Kategori_Kontak = GetContactTypeByID(item("Tph_Contact_Type"))
                '    If Not objtask Is Nothing Then
                '        item("ContactType") = objtask.Keterangan
                '    End If
                'Next

                ''' Phone -> tbl Reference Communication Type
                'Dim objdtCommunicationType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)
                'Dim objcolCommunicationType As New Data.DataColumn
                'objcolCommunicationType.ColumnName = "CommunicationType"
                'objcolCommunicationType.DataType = GetType(String)
                'objdtCommunicationType.Columns.Add(objcolCommunicationType)

                'For Each item As DataRow In objdtCommunicationType.Rows
                '    Dim objtask As GoAMLDAL.goAML_Ref_Jenis_Alat_Komunikasi = GetCommunicationTypeByID(item("Tph_Communication_Type"))
                '    If Not objtask Is Nothing Then
                '        item("CommunicationType") = objtask.Keterangan
                '    End If
                'Next


                '' Address
                Dim objStoreAddress As New Ext.Net.Store
                objStoreAddress.ID = strunik & "StoreGridAddress"
                objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelAddress As New Ext.Net.Model
                Dim objFieldAddress As Ext.Net.ModelField

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "PK_Customer_Address_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_Ref_Detail_Of"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_To_Table_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address_Type"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Town"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "City"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Zip"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Country_Code"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "State"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Comments"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objStoreAddress.Model.Add(objModelAddress)


                Dim objListcolumnAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnAddress.Add(objcolumnNo)
                End Using


                Dim objColumAddress As Ext.Net.Column

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address Type"
                objColumAddress.DataIndex = "Address_Type"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address"
                objColumAddress.DataIndex = "Address"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Town"
                objColumAddress.DataIndex = "Town"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "City"
                objColumAddress.DataIndex = "City"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Zip"
                objColumAddress.DataIndex = "Zip"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "CountryCode"
                objColumAddress.DataIndex = "Country_Code"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "State"
                objColumAddress.DataIndex = "State"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Comment"
                objColumAddress.DataIndex = "Comments"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                ''' Address -> tbl Reference Address Type
                'Dim objdtAddressType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)
                'Dim objcolAddressType As New Data.DataColumn
                'objcolAddressType.ColumnName = "AddressType"
                'objcolAddressType.DataType = GetType(String)
                'objdtAddressType.Columns.Add(objcolAddressType)

                'For Each item As DataRow In objdtAddressType.Rows
                '    Dim objtaskAddressType As GoAMLDAL.goAML_Ref_Kategori_Kontak = GetAddressTypeByID(item("Address_type"))
                '    If Not objtaskAddressType Is Nothing Then
                '        item("AddressType") = objtaskAddressType.Keterangan
                '    End If
                'Next

                ''' Address -> tbl Reference Nama Negara
                'Dim objdtNamaNegaraType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)
                'Dim objcolNamaNegaraType As New Data.DataColumn
                'objcolNamaNegaraType.ColumnName = "NamaNegara"
                'objcolNamaNegaraType.DataType = GetType(String)
                'objdtNamaNegaraType.Columns.Add(objcolNamaNegaraType)

                'For Each item As DataRow In objdtNamaNegaraType.Rows
                '    Dim objtaskNamaNegaraType As GoAMLDAL.goAML_Ref_Nama_Negara = GetNamaNegaraByID(item("country_code"))
                '    If Not objtaskNamaNegaraType Is Nothing Then
                '        item("NamaNegara") = objtaskNamaNegaraType.Keterangan
                '    End If
                'Next


                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Contact Type", objStore, objListcolumn, objdt)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Communication Type", objStore, objListcolumn, objdtCommunicationType)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Tipe Alamat", objStoreAddress, objListcolumnAddress, objdtAddressType)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Negara", objStoreAddress, objListcolumnAddress, objdtNamaNegaraType)

            End If
        End Using

    End Sub
    Shared Sub SettingColor(objPanelOld As FormPanel, objPanelNew As FormPanel, objdata As String, objdatabefore As String, unikkeyold As String, unikkeynew As String)

        If objdata.Length > 0 And objdatabefore.Length > 0 Then
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("PK_Customer_ID", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("CIF", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("AccountNo", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Opened", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Closed", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Gender", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("date_balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("status_code", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary_comment", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("comments", objPanelOld, objPanelNew, unikkeyold, unikkeynew)

        End If
    End Sub

    Shared Function Accept(ID As String) As Boolean
        'done: accept


        Using objdb As New GoAMLDAL.GoAMLEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As GoAMLDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As GoAMLDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objCustomer As GoAMLDAL.goAML_Ref_Customer = objModuledata.objgoAML_Ref_Customer
                            Dim objPhone As List(Of GoAMLDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Phone
                            Dim objAddress As List(Of GoAMLDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Address
                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now

                            objdb.Entry(objCustomer).State = Entity.EntityState.Added

                            'audittrail
                            Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objPhone
                                item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objAddress
                                item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next

                            Dim objtype As Type = objCustomer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objCustomer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objCustomer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_AccountDataBLL))
                            Dim objModuledataOld As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_AccountDataBLL))

                            Dim objAccount As GoAMLDAL.goAML_Ref_Account = objModuledata.objgoAML_Ref_Account

                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objAccount.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objAccount.Alternateby = ""
                            End If
                            objAccount.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objAccount.ApprovedDate = Now
                            objAccount.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objAccount.LastUpdateDate = Now
                            objdb.goAML_Ref_Account.Attach(objAccount)
                            objdb.Entry(objAccount).State = Entity.EntityState.Modified


                            For Each itemx As GoAMLDAL.goAML_Ref_Account_Signatory In (From x In objdb.goAML_Ref_Account_Signatory Where x.FK_Account_No = objModuledata.objgoAML_Ref_Account.Account_No Select x).ToList
                                Dim objcek As GoAMLDAL.goAML_Ref_Account_Signatory = objModuledata.objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = itemx.PK_Signatory_ID)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As GoAMLDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                Dim obcek As GoAMLDAL.goAML_Ref_Account_Signatory = (From x In objdb.goAML_Ref_Account_Signatory Where x.PK_Signatory_ID = item.PK_Signatory_ID Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.LastUpdateDate = Now
                                    item.Active = 1
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            Using objDba As New GoAMLEntities
                                Dim header As GoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                                'AuditTrailDetail
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objAccount)
                                If objModuledata.objListgoAML_Ref_Account_Signatory.Count > 0 Then
                                    For Each signatory As GoAMLDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                                    Next
                                End If
                            End Using


                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Deleted

                            For Each item As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            For Each item As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            'For Each item As EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'For Each item As EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'audittrail
                            Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next
                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next


                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next



                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Modified

                            'audittrail
                            Dim objaudittrailheader As New GoAMLDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    Dim objModuledata_2 As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_AccountDataBLL))
                    Dim objModuledataOld_2 As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_AccountDataBLL))
                    Dim AccountNo As String = objModuledata_2.objgoAML_Ref_Account.Account_No
                    Dim objListRelatedEntitiesNew As List(Of goAML_Ref_Account_Related_Entity) = objModuledata_2.objListgoAML_RelatedEntities
                    Dim objListRelatedEntitiesOld As List(Of goAML_Ref_Account_Related_Entity) = objModuledataOld_2.objListgoAML_RelatedEntities
                    Dim objListRelatedPersonNew As List(Of goAML_Ref_Account_Related_Person) = objModuledata_2.objListgoAML_RelatedPerson
                    Dim objListRelatedPersonOld As List(Of goAML_Ref_Account_Related_Person) = objModuledataOld_2.objListgoAML_RelatedPerson
                    Dim objListRelatedAccountNew As List(Of goAML_Ref_Account_Related_Account) = objModuledata_2.objListgoAML_RelatedAccount
                    Dim objListRelatedAccountOld As List(Of goAML_Ref_Account_Related_Account) = objModuledataOld_2.objListgoAML_RelatedAccount
                    Dim objListSanctionNew As List(Of goAML_Ref_Account_Sanction) = objModuledata_2.objListgoAML_Sanction
                    Dim objListSanctionOld As List(Of goAML_Ref_Account_Sanction) = objModuledataOld_2.objListgoAML_Sanction
                    Dim StrAction As String = ""
                    If objApproval.PK_ModuleAction_ID = 1 Then
                        StrAction = "Insert"
                    ElseIf objApproval.PK_ModuleAction_ID = 2 Then
                        StrAction = "Update"
                    ElseIf objApproval.PK_ModuleAction_ID = 3 Then
                        StrAction = "Delete"
                    End If
                    ' Entities
                    Dim strQuery As String
                    Dim ListOldRelatedEntities As New List(Of goAML_Ref_Account_Related_Entity)
                    ListOldRelatedEntities = getRelatedEntitiesByID(ID)
                    If objListRelatedEntitiesNew IsNot Nothing Then
                        For Each item In objListRelatedEntitiesNew
                            If item.PK_goAML_Ref_Account_Related_Entity_ID > 0 Then
                                strQuery = "UPDATE goAML_Ref_Account_Related_Entity  "
                                strQuery += "SET                                   "
                                strQuery += " ACCOUNT_NO									 ='" & item.ACCOUNT_NO & "'"
                                strQuery += " ,ACCOUNT_ENTITY_RELATION                       ='" & item.ACCOUNT_ENTITY_RELATION & "'"
                                strQuery += " ,relation_date_range_valid_from                ='" & item.relation_date_range_valid_from & "'"
                                strQuery += " ,relation_date_range_is_approx_from_date       ='" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += " ,relation_date_range_valid_to                  ='" & item.relation_date_range_valid_to & "'"
                                strQuery += " ,relation_date_range_is_approx_to_date         ='" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += " ,comments                                      ='" & item.comments & "'"
                                strQuery += " ,name                                          ='" & item.name & "'"
                                strQuery += " ,commercial_name                               ='" & item.commercial_name & "'"
                                strQuery += " ,incorporation_legal_form                      ='" & item.incorporation_legal_form & "'"
                                strQuery += " ,incorporation_number                          ='" & item.incorporation_number & "'"
                                strQuery += " ,business                                      ='" & item.business & "'"
                                strQuery += " ,entity_status                                 ='" & item.entity_status & "'"
                                strQuery += " ,entity_status_date                            ='" & item.entity_status_date & "'"
                                strQuery += " ,incorporation_state                           ='" & item.incorporation_state & "'"
                                strQuery += " ,incorporation_country_code                    ='" & item.incorporation_country_code & "'"
                                strQuery += " ,incorporation_date                            ='" & item.incorporation_date & "'"
                                strQuery += " ,business_closed                               ='" & item.business_closed & "'"
                                strQuery += " ,date_business_closed                          ='" & item.date_business_closed & "'"
                                strQuery += " ,tax_number                                    ='" & item.tax_number & "'"
                                strQuery += " ,tax_reg_number                                ='" & item.tax_reg_number & "'"
                                strQuery += " ,Active                                        ='" & item.Active & "'"
                                strQuery += " ,CreatedBy                                     ='" & item.CreatedBy & "'"
                                strQuery += " ,LastUpdateBy                                  ='" & item.LastUpdateBy & "'"
                                strQuery += " ,ApprovedBy                                    ='" & item.ApprovedBy & "'"
                                strQuery += " ,CreatedDate                                   ='" & item.CreatedDate & "'"
                                strQuery += " ,LastUpdateDate                                ='" & item.LastUpdateDate & "'"
                                strQuery += " ,ApprovedDate                                  ='" & item.ApprovedDate & "'"
                                strQuery += " ,Alternateby                                   ='" & item.Alternateby & "'"
                                strQuery += " ,IS_SEARCH                                     ='" & item.IS_SEARCH & "'"
                                strQuery += " ,CIF_NO_REF                                    ='" & item.CIF_NO_REF & "'"
                                strQuery += " ,WIC_NO_REF                                    ='" & item.WIC_NO_REF & "'"
                                strQuery += "from goAML_Ref_Account_Related_Entity  where PK_goAML_Ref_Account_Related_Entity_ID = " & item.PK_goAML_Ref_Account_Related_Entity_ID
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                            Else
                                strQuery = "INSERT INTO goAML_Ref_Account_Related_Entity ( "
                                strQuery += " ACCOUNT_NO "
                                strQuery += " ,ACCOUNT_ENTITY_RELATION "
                                strQuery += " ,relation_date_range_valid_from"
                                strQuery += " ,relation_date_range_is_approx_from_date"
                                strQuery += " ,relation_date_range_valid_to"
                                strQuery += " ,relation_date_range_is_approx_to_date"
                                strQuery += " ,comments"
                                strQuery += " ,name"
                                strQuery += " ,commercial_name"
                                strQuery += " ,incorporation_legal_form"
                                strQuery += " ,incorporation_number"
                                strQuery += " ,business"
                                strQuery += " ,entity_status"
                                strQuery += " ,entity_status_date"
                                strQuery += " ,incorporation_state"
                                strQuery += " ,incorporation_country_code"
                                strQuery += " ,incorporation_date"
                                strQuery += " ,business_closed"
                                strQuery += " ,date_business_closed"
                                strQuery += " ,tax_number"
                                strQuery += " ,tax_reg_number"
                                strQuery += " ,Active"
                                strQuery += " ,CreatedBy"
                                strQuery += " ,LastUpdateBy"
                                strQuery += " ,ApprovedBy"
                                strQuery += " ,CreatedDate"
                                strQuery += " ,LastUpdateDate"
                                strQuery += " ,ApprovedDate"
                                strQuery += " ,Alternateby"
                                strQuery += " ,IS_SEARCH"
                                strQuery += " ,CIF_NO_REF"
                                strQuery += " ,WIC_NO_REF"
                                strQuery += " ) VALUES ( "
                                strQuery += "  '" & item.ACCOUNT_NO & "'"
                                strQuery += "  ,'" & item.ACCOUNT_ENTITY_RELATION & "'"
                                strQuery += "  ,'" & item.relation_date_range_valid_from & "'"
                                strQuery += "  ,'" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += "  ,'" & item.relation_date_range_valid_to & "'"
                                strQuery += "  ,'" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += "  ,'" & item.comments & "'"
                                strQuery += "  ,'" & item.name & "'"
                                strQuery += "  ,'" & item.commercial_name & "'"
                                strQuery += "  ,'" & item.incorporation_legal_form & "'"
                                strQuery += "  ,'" & item.incorporation_number & "'"
                                strQuery += "  ,'" & item.business & "'"
                                strQuery += "  ,'" & item.entity_status & "'"
                                strQuery += "  ,'" & item.entity_status_date & "'"
                                strQuery += "  ,'" & item.incorporation_state & "'"
                                strQuery += "  ,'" & item.incorporation_country_code & "'"
                                strQuery += "  ,'" & item.incorporation_date & "'"
                                strQuery += "  ,'" & item.business_closed & "'"
                                strQuery += "  ,'" & item.date_business_closed & "'"
                                strQuery += "  ,'" & item.tax_number & "'"
                                strQuery += "  ,'" & item.tax_reg_number & "'"
                                strQuery += "  ,'" & item.Active & "'"
                                strQuery += "  ,'" & item.CreatedBy & "'"
                                strQuery += "  ,'" & item.LastUpdateBy & "'"
                                strQuery += "  ,'" & item.ApprovedBy & "'"
                                strQuery += "  ,'" & item.CreatedDate & "'"
                                strQuery += "  ,'" & item.LastUpdateDate & "'"
                                strQuery += "  ,'" & item.ApprovedDate & "'"
                                strQuery += "  ,'" & item.Alternateby & "'"
                                strQuery += "  ,'" & item.IS_SEARCH & "'"
                                strQuery += "  ,'" & item.CIF_NO_REF & "'"
                                strQuery += "  ,'" & item.WIC_NO_REF & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            End If

                        Next
                    End If
                    If objListRelatedEntitiesNew Is Nothing Then
                        objListRelatedEntitiesNew = New List(Of goAML_Ref_Account_Related_Entity)
                    End If
                    Dim strQuery2 As String = ""
                    For Each itemx As goAML_Ref_Account_Related_Entity In (From x In objListRelatedEntitiesOld Where x.ACCOUNT_NO = AccountNo Select x).ToList
                        Dim objcek As goAML_Ref_Account_Related_Entity = objListRelatedEntitiesNew.Find(Function(x) x.PK_goAML_Ref_Account_Related_Entity_ID = itemx.PK_goAML_Ref_Account_Related_Entity_ID)
                        If objcek Is Nothing Then
                            strQuery2 += "DELETE FROM goAML_Ref_Account_Related_Entity		"
                            strQuery2 += "where PK_goAML_Ref_Account_Related_Entity_ID =  " & itemx.PK_goAML_Ref_Account_Related_Entity_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)


                        End If
                    Next


                    strQuery = ""
                    ' Audit Trail
                    strQuery = "INSERT INTO AuditTrailHeader ( "
                    strQuery += "ApproveBy,"
                    strQuery += "CreatedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "FK_AuditTrailStatus_ID,"
                    strQuery += "FK_ModuleAction_ID,"
                    strQuery += "ModuleLabel"
                    strQuery += " ) VALUES ( "
                    strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                    strQuery += " , '" & objApproval.PK_ModuleAction_ID & "'"
                    strQuery += " , '" & objModule.ModuleLabel & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)
                    If objListRelatedEntitiesNew Is Nothing Then
                        objListRelatedEntitiesNew = New List(Of goAML_Ref_Account_Related_Entity)
                    End If
                    ' Audit Related Entities
                    strQuery = ""
                    If objListRelatedEntitiesNew IsNot Nothing AndAlso objListRelatedEntitiesNew.Count > 0 Then
                        For Each item In objListRelatedEntitiesNew
                            strQuery = ""
                            Dim objectdata As Object
                            objectdata = item
                            Dim objtype As Type = objectdata.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each itemEntities As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                If Not itemEntities.GetValue(objectdata, Nothing) Is Nothing Then
                                    If itemEntities.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                        objaudittraildetail.NewValue = itemEntities.GetValue(objectdata, Nothing)
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                strQuery = "INSERT INTO AuditTrailDetail(  "
                                strQuery += "FK_AuditTrailHeader_ID,"
                                strQuery += "FieldName,"
                                strQuery += "OldValue,"
                                strQuery += "NewValue"
                                strQuery += " ) VALUES ( "
                                strQuery += " " & PKAudit & ""
                                strQuery += " ,'" & itemEntities.Name & "'"
                                strQuery += " , ''"
                                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            Next

                        Next
                    End If


                    If objListRelatedPersonNew IsNot Nothing Then
                        For Each item In objListRelatedPersonNew
                            If item.PK_goAML_Ref_Account_Related_Person_ID > 0 Then
                                strQuery = "UPDATE goAML_Ref_Account_Related_Person  "
                                strQuery += "SET                                   "
                                strQuery += " ACCOUNT_NO								='" & item.ACCOUNT_NO & "'"
                                strQuery += " ,IS_PRIMARY                               ='" & item.IS_PRIMARY & "'"
                                strQuery += " ,ROLE                                     ='" & item.ROLE & "'"
                                strQuery += " ,relation_date_range_valid_from           ='" & item.relation_date_range_valid_from & "'"
                                strQuery += " ,relation_date_range_is_approx_from_date  ='" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += " ,relation_date_range_valid_to             ='" & item.relation_date_range_valid_to & "'"
                                strQuery += " ,relation_date_range_is_approx_to_date    ='" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += " ,comments                                 ='" & item.comments & "'"
                                strQuery += " ,gender                                   ='" & item.gender & "'"
                                strQuery += " ,title                                    ='" & item.title & "'"
                                strQuery += " ,first_name                               ='" & item.first_name & "'"
                                strQuery += " ,middle_name                              ='" & item.middle_name & "'"
                                strQuery += " ,prefix                                   ='" & item.prefix & "'"
                                strQuery += " ,last_name                                ='" & item.last_name & "'"
                                strQuery += " ,birthdate                                ='" & item.birthdate & "'"
                                strQuery += " ,birth_place                              ='" & item.birth_place & "'"
                                strQuery += " ,country_of_birth                         ='" & item.country_of_birth & "'"
                                strQuery += " ,mothers_name                             ='" & item.mothers_name & "'"
                                strQuery += " ,alias                                    ='" & item.alias & "'"
                                strQuery += " ,full_name_frn                            ='" & item.full_name_frn & "'"
                                strQuery += " ,ssn                                      ='" & item.ssn & "'"
                                strQuery += " ,passport_number                          ='" & item.passport_number & "'"
                                strQuery += " ,passport_country                         ='" & item.passport_country & "'"
                                strQuery += " ,id_number                                ='" & item.id_number & "'"
                                strQuery += " ,nationality1                             ='" & item.nationality1 & "'"
                                strQuery += " ,nationality2                             ='" & item.nationality2 & "'"
                                strQuery += " ,nationality3                             ='" & item.nationality3 & "'"
                                strQuery += " ,residence                                ='" & item.residence & "'"
                                strQuery += " ,residence_since                          ='" & item.residence_since & "'"
                                strQuery += " ,occupation                               ='" & item.occupation & "'"
                                strQuery += " ,deceased                                 ='" & item.deceased & "'"
                                strQuery += " ,date_deceased                            ='" & item.date_deceased & "'"
                                strQuery += " ,tax_number                               ='" & item.tax_number & "'"
                                strQuery += " ,tax_reg_number                           ='" & item.tax_reg_number & "'"
                                strQuery += " ,source_of_wealth                         ='" & item.source_of_wealth & "'"
                                strQuery += " ,is_protected                             ='" & item.is_protected & "'"
                                strQuery += " ,Active                                   ='" & item.Active & "'"
                                strQuery += " ,CreatedBy                                ='" & item.CreatedBy & "'"
                                strQuery += " ,LastUpdateBy                             ='" & item.LastUpdateBy & "'"
                                strQuery += " ,ApprovedBy                               ='" & item.ApprovedBy & "'"
                                strQuery += " ,CreatedDate                              ='" & item.CreatedDate & "'"
                                strQuery += " ,LastUpdateDate                           ='" & item.LastUpdateDate & "'"
                                strQuery += " ,ApprovedDate                             ='" & item.ApprovedDate & "'"
                                strQuery += " ,Alternateby                              ='" & item.Alternateby & "'"
                                strQuery += " ,IS_SEARCH                                ='" & item.IS_SEARCH & "'"
                                strQuery += " ,CIF_NO_REF                               ='" & item.CIF_NO_REF & "'"
                                strQuery += " ,WIC_NO_REF                               ='" & item.WIC_NO_REF & "'"
                                strQuery += "from goAML_Ref_Account_Related_Person  where PK_goAML_Ref_Account_Related_Person_ID = " & item.PK_goAML_Ref_Account_Related_Person_ID
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                            Else
                                strQuery = "INSERT INTO goAML_Ref_Account_Related_Person ( "
                                strQuery += " ACCOUNT_NO "
                                strQuery += " ,IS_PRIMARY"
                                strQuery += " ,ROLE"
                                strQuery += " ,relation_date_range_valid_from"
                                strQuery += " ,relation_date_range_is_approx_from_date"
                                strQuery += " ,relation_date_range_valid_to"
                                strQuery += " ,relation_date_range_is_approx_to_date"
                                strQuery += " ,comments"
                                strQuery += " ,gender"
                                strQuery += " ,title"
                                strQuery += " ,first_name"
                                strQuery += " ,middle_name"
                                strQuery += " ,prefix"
                                strQuery += " ,last_name "
                                strQuery += " ,birthdate"
                                strQuery += " ,birth_place"
                                strQuery += " ,country_of_birth"
                                strQuery += " ,mothers_name"
                                strQuery += " ,alias"
                                strQuery += " ,full_name_frn"
                                strQuery += " ,ssn"
                                strQuery += " ,passport_number"
                                strQuery += " ,passport_country"
                                strQuery += " ,id_number"
                                strQuery += " ,nationality1"
                                strQuery += " ,nationality2"
                                strQuery += " ,nationality3"
                                strQuery += " ,residence"
                                strQuery += " ,residence_since"
                                strQuery += " ,occupation"
                                strQuery += " ,deceased"
                                strQuery += " ,date_deceased"
                                strQuery += " ,tax_number"
                                strQuery += " ,tax_reg_number"
                                strQuery += " ,source_of_wealth"
                                strQuery += " ,is_protected"
                                strQuery += " ,Active"
                                strQuery += " ,CreatedBy"
                                strQuery += " ,LastUpdateBy"
                                strQuery += " ,ApprovedBy"
                                strQuery += " ,CreatedDate"
                                strQuery += " ,LastUpdateDate"
                                strQuery += " ,ApprovedDate"
                                strQuery += " ,Alternateby"
                                strQuery += " ,IS_SEARCH"
                                strQuery += " ,CIF_NO_REF"
                                strQuery += " ,WIC_NO_REF"
                                strQuery += " ) VALUES ( "
                                strQuery += " '" & item.ACCOUNT_NO & "'"
                                strQuery += " ,'" & item.IS_PRIMARY & "'"
                                strQuery += " ,'" & item.ROLE & "'"
                                strQuery += " ,'" & item.relation_date_range_valid_from & "'"
                                strQuery += " ,'" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += " ,'" & item.relation_date_range_valid_to & "'"
                                strQuery += " ,'" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += " ,'" & item.comments & "'"
                                strQuery += " ,'" & item.gender & "'"
                                strQuery += " ,'" & item.title & "'"
                                strQuery += " ,'" & item.first_name & "'"
                                strQuery += " ,'" & item.middle_name & "'"
                                strQuery += " ,'" & item.prefix & "'"
                                strQuery += " ,'" & item.last_name & "'"
                                strQuery += " ,'" & item.birthdate & "'"
                                strQuery += " ,'" & item.birth_place & "'"
                                strQuery += " ,'" & item.country_of_birth & "'"
                                strQuery += " ,'" & item.mothers_name & "'"
                                strQuery += " ,'" & item.alias & "'"
                                strQuery += " ,'" & item.full_name_frn & "'"
                                strQuery += " ,'" & item.ssn & "'"
                                strQuery += " ,'" & item.passport_number & "'"
                                strQuery += " ,'" & item.passport_country & "'"
                                strQuery += " ,'" & item.id_number & "'"
                                strQuery += " ,'" & item.nationality1 & "'"
                                strQuery += " ,'" & item.nationality2 & "'"
                                strQuery += " ,'" & item.nationality3 & "'"
                                strQuery += " ,'" & item.residence & "'"
                                strQuery += " ,'" & item.residence_since & "'"
                                strQuery += " ,'" & item.occupation & "'"
                                strQuery += " ,'" & item.deceased & "'"
                                strQuery += " ,'" & item.date_deceased & "'"
                                strQuery += " ,'" & item.tax_number & "'"
                                strQuery += " ,'" & item.tax_reg_number & "'"
                                strQuery += " ,'" & item.source_of_wealth & "'"
                                strQuery += " ,'" & item.is_protected & "'"
                                strQuery += " ,'" & item.Active & "'"
                                strQuery += " ,'" & DateTime.Now & "'"
                                strQuery += " ,'" & item.LastUpdateBy & "'"
                                strQuery += " ,'" & item.ApprovedBy & "'"
                                strQuery += " ,'" & item.CreatedDate & "'"
                                strQuery += " ,'" & item.LastUpdateDate & "'"
                                strQuery += " ,'" & item.ApprovedDate & "'"
                                strQuery += " ,'" & item.Alternateby & "'"
                                strQuery += " ,'" & item.IS_SEARCH & "'"
                                strQuery += " ,'" & item.CIF_NO_REF & "'"
                                strQuery += " ,'" & item.WIC_NO_REF & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            End If

                        Next


                    End If
                    If objListRelatedPersonNew Is Nothing Then
                        objListRelatedPersonNew = New List(Of goAML_Ref_Account_Related_Person)
                    End If
                    Dim strQuery3 As String = ""
                    For Each itemx As goAML_Ref_Account_Related_Person In (From x In objListRelatedPersonOld Where x.ACCOUNT_NO = AccountNo Select x).ToList
                        Dim objcek As goAML_Ref_Account_Related_Person = objListRelatedPersonNew.Find(Function(x) x.PK_goAML_Ref_Account_Related_Person_ID = itemx.PK_goAML_Ref_Account_Related_Person_ID)
                        If objcek Is Nothing Then
                            strQuery3 += "DELETE FROM goAML_Ref_Account_Related_Person		"
                            strQuery3 += "where PK_goAML_Ref_Account_Related_Person_ID =  " & itemx.PK_goAML_Ref_Account_Related_Person_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery3, Nothing)


                        End If
                    Next
                    If objListRelatedAccountNew IsNot Nothing Then
                        For Each item In objListRelatedAccountNew
                            If item.PK_goAML_Ref_Account_Rel_Account_ID > 0 Then
                                strQuery = "UPDATE goAML_Ref_Account_Related_Account  "
                                strQuery += "SET                                   "
                                strQuery += "  ACCOUNT_NO='" & item.ACCOUNT_NO & "'"
                                strQuery += " ,ACCOUNT_ACCOUNT_RELATIONSHIP='" & item.ACCOUNT_ACCOUNT_RELATIONSHIP & "'"
                                strQuery += " ,relation_date_range_valid_from='" & item.relation_date_range_valid_from & "'"
                                strQuery += " ,relation_date_range_is_approx_from_date='" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += " ,relation_date_range_valid_to='" & item.relation_date_range_valid_to & "'"
                                strQuery += " ,relation_date_range_is_approx_to_date='" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += " ,comments='" & item.comments & "'"
                                strQuery += " ,institution_name='" & item.institution_name & "'"
                                strQuery += " ,institution_code='" & item.institution_code & "'"
                                strQuery += " ,swift='" & item.swift & "'"
                                strQuery += " ,institution_country='" & item.institution_country & "'"
                                strQuery += " ,non_bank_institution='" & item.non_bank_institution & "'"
                                strQuery += " ,collection_account='" & item.collection_account & "'"
                                strQuery += " ,branch='" & item.branch & "'"
                                strQuery += " ,account_category='" & item.account_category & "'"
                                strQuery += " ,account='" & item.account & "'"
                                strQuery += " ,currency_code='" & item.currency_code & "'"
                                strQuery += " ,account_name='" & item.account_name & "'"
                                strQuery += " ,iban='" & item.iban & "'"
                                strQuery += " ,client_number='" & item.client_number & "'"
                                strQuery += " ,account_type='" & item.account_type & "'"
                                strQuery += " ,opened_date='" & item.opened_date & "'"
                                strQuery += " ,closed_date='" & item.closed_date & "'"
                                strQuery += " ,balance='" & item.balance & "'"
                                strQuery += " ,date_balance='" & item.date_balance & "'"
                                strQuery += " ,status_code='" & item.status_code & "'"
                                strQuery += " ,status_date='" & item.status_date & "'"
                                strQuery += " ,beneficiary='" & item.beneficiary & "'"
                                strQuery += " ,beneficiary_comment='" & item.beneficiary_comment & "'"
                                strQuery += " ,Active='" & item.Active & "'"
                                strQuery += " ,CreatedBy='" & item.CreatedBy & "'"
                                strQuery += " ,LastUpdateBy='" & item.LastUpdateBy & "'"
                                strQuery += " ,ApprovedBy='" & item.ApprovedBy & "'"
                                strQuery += " ,CreatedDate='" & item.CreatedDate & "'"
                                strQuery += " ,LastUpdateDate='" & item.LastUpdateDate & "'"
                                strQuery += " ,ApprovedDate='" & item.ApprovedDate & "'"
                                strQuery += " ,Alternateby='" & item.Alternateby & "'"
                                strQuery += " ,IS_SEARCH='" & item.IS_SEARCH & "'"
                                strQuery += " ,ACCOUNT_NO_REF='" & item.ACCOUNT_NO_REF & "'"
                                strQuery += "from goAML_Ref_Account_Related_Account  where PK_goAML_Ref_Account_Rel_Account_ID = " & item.PK_goAML_Ref_Account_Rel_Account_ID
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                            Else
                                strQuery = "INSERT INTO goAML_Ref_Account_Related_Account ( "
                                strQuery += "  ACCOUNT_NO"
                                strQuery += " ,ACCOUNT_ACCOUNT_RELATIONSHIP"
                                strQuery += " ,relation_date_range_valid_from"
                                strQuery += " ,relation_date_range_is_approx_from_date"
                                strQuery += " ,relation_date_range_valid_to"
                                strQuery += " ,relation_date_range_is_approx_to_date"
                                strQuery += " ,comments"
                                strQuery += " ,institution_name"
                                strQuery += " ,institution_code"
                                strQuery += " ,swift"
                                strQuery += " ,institution_country"
                                strQuery += " ,non_bank_institution"
                                strQuery += " ,collection_account"
                                strQuery += " ,branch"
                                strQuery += " ,account_category"
                                strQuery += " ,account"
                                strQuery += " ,currency_code"
                                strQuery += " ,account_name"
                                strQuery += " ,iban"
                                strQuery += " ,client_number"
                                strQuery += " ,account_type"
                                strQuery += " ,opened_date"
                                strQuery += " ,closed_date"
                                strQuery += " ,balance"
                                strQuery += " ,date_balance"
                                strQuery += " ,status_code"
                                strQuery += " ,status_date"
                                strQuery += " ,beneficiary"
                                strQuery += " ,beneficiary_comment"
                                strQuery += " ,Active"
                                strQuery += " ,CreatedBy"
                                strQuery += " ,LastUpdateBy"
                                strQuery += " ,ApprovedBy"
                                strQuery += " ,CreatedDate"
                                strQuery += " ,LastUpdateDate"
                                strQuery += " ,ApprovedDate"
                                strQuery += " ,Alternateby"
                                strQuery += " ,IS_SEARCH"
                                strQuery += " ,ACCOUNT_NO_REF"
                                strQuery += " ) VALUES ( "
                                strQuery += " '" & item.ACCOUNT_NO & "'"
                                strQuery += " ,'" & item.ACCOUNT_ACCOUNT_RELATIONSHIP & "'"
                                strQuery += " ,'" & item.relation_date_range_valid_from & "'"
                                strQuery += " ,'" & item.relation_date_range_is_approx_from_date & "'"
                                strQuery += " ,'" & item.relation_date_range_valid_to & "'"
                                strQuery += " ,'" & item.relation_date_range_is_approx_to_date & "'"
                                strQuery += " ,'" & item.comments & "'"
                                strQuery += " ,'" & item.institution_name & "'"
                                strQuery += " ,'" & item.institution_code & "'"
                                strQuery += " ,'" & item.swift & "'"
                                strQuery += " ,'" & item.institution_country & "'"
                                strQuery += " ,'" & item.non_bank_institution & "'"
                                strQuery += " ,'" & item.collection_account & "'"
                                strQuery += " ,'" & item.branch & "'"
                                strQuery += " ,'" & item.account_category & "'"
                                strQuery += " ,'" & item.account & "'"
                                strQuery += " ,'" & item.currency_code & "'"
                                strQuery += " ,'" & item.account_name & "'"
                                strQuery += " ,'" & item.iban & "'"
                                strQuery += " ,'" & item.client_number & "'"
                                strQuery += " ,'" & item.account_type & "'"
                                strQuery += " ,'" & item.opened_date & "'"
                                strQuery += " ,'" & item.closed_date & "'"
                                strQuery += " ,'" & item.balance & "'"
                                strQuery += " ,'" & item.date_balance & "'"
                                strQuery += " ,'" & item.status_code & "'"
                                strQuery += " ,'" & item.status_date & "'"
                                strQuery += " ,'" & item.beneficiary & "'"
                                strQuery += " ,'" & item.beneficiary_comment & "'"
                                strQuery += " ,'" & item.Active & "'"
                                strQuery += " ,'" & item.CreatedBy & "'"
                                strQuery += " ,'" & item.LastUpdateBy & "'"
                                strQuery += " ,'" & item.ApprovedBy & "'"
                                strQuery += " ,'" & item.CreatedDate & "'"
                                strQuery += " ,'" & item.LastUpdateDate & "'"
                                strQuery += " ,'" & item.ApprovedDate & "'"
                                strQuery += " ,'" & item.Alternateby & "'"
                                strQuery += " ,'" & item.IS_SEARCH & "'"
                                strQuery += " ,'" & item.ACCOUNT_NO_REF & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            End If

                        Next

                    End If
                    If objListRelatedAccountNew Is Nothing Then
                        objListRelatedAccountNew = New List(Of goAML_Ref_Account_Related_Account)
                    End If
                    Dim strQuery4 As String = ""
                    For Each itemx As goAML_Ref_Account_Related_Account In (From x In objListRelatedAccountOld Where x.ACCOUNT_NO = AccountNo Select x).ToList
                        Dim objcek As goAML_Ref_Account_Related_Account = objListRelatedAccountNew.Find(Function(x) x.PK_goAML_Ref_Account_Rel_Account_ID = itemx.PK_goAML_Ref_Account_Rel_Account_ID)
                        If objcek Is Nothing Then
                            strQuery4 += "DELETE FROM goAML_Ref_Account_Related_Account		"
                            strQuery4 += "where PK_goAML_Ref_Account_Rel_Account_ID =  " & itemx.PK_goAML_Ref_Account_Rel_Account_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery4, Nothing)
                        End If
                    Next

                    strQuery = ""
                    strQuery = "INSERT INTO AuditTrailHeader ( "
                    strQuery += "ApproveBy,"
                    strQuery += "CreatedBy,"
                    strQuery += "CreatedDate,"
                    strQuery += "FK_AuditTrailStatus_ID,"
                    strQuery += "FK_ModuleAction_ID,"
                    strQuery += "ModuleLabel"
                    strQuery += " ) VALUES ( "
                    strQuery += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    strQuery += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                    strQuery += " , '" & objApproval.PK_ModuleAction_ID & "'"
                    strQuery += " , '" & objModule.ModuleLabel & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                    Dim PKAuditAccount As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                    If objListRelatedAccountNew Is Nothing Then
                        objListRelatedAccountNew = New List(Of goAML_Ref_Account_Related_Account)
                    End If
                    ' Audit Related Account
                    strQuery = ""
                    If objListRelatedAccountNew IsNot Nothing Then
                        For Each item In objListRelatedAccountNew
                            strQuery = ""
                            Dim objectdata As Object
                            objectdata = item
                            Dim objtype As Type = objectdata.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each itemAccount As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New GoAMLDAL.AuditTrailDetail
                                If Not itemAccount.GetValue(objectdata, Nothing) Is Nothing Then
                                    If itemAccount.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                        objaudittraildetail.NewValue = itemAccount.GetValue(objectdata, Nothing)
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                strQuery = "INSERT INTO AuditTrailDetail(  "
                                strQuery += "FK_AuditTrailHeader_ID,"
                                strQuery += "FieldName,"
                                strQuery += "OldValue,"
                                strQuery += "NewValue"
                                strQuery += " ) VALUES ( "
                                strQuery += " " & PKAudit & ""
                                strQuery += " ,'" & itemAccount.Name & "'"
                                strQuery += " , ''"
                                strQuery += " , '" & objaudittraildetail.NewValue & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            Next

                        Next
                    End If


                    If objListSanctionNew IsNot Nothing Then
                        For Each item In objListSanctionNew
                            If item.PK_goAML_Ref_Account_Sanction_ID > 0 Then
                                strQuery = "UPDATE goAML_Ref_Account_Sanction  "
                                strQuery += "SET                                   "
                                strQuery += "  ACCOUNT_NO='" & item.ACCOUNT_NO & "'"
                                strQuery += " ,provider='" & item.provider & "'"
                                strQuery += " ,sanction_list_name='" & item.sanction_list_name & "'"
                                strQuery += " ,match_criteria='" & item.match_criteria & "'"
                                strQuery += " ,link_to_source='" & item.link_to_source & "'"
                                strQuery += " ,sanction_list_attributes='" & item.sanction_list_attributes & "'"
                                strQuery += " ,sanction_list_date_range_valid_from='" & item.sanction_list_date_range_valid_from & "'"
                                strQuery += " ,sanction_list_date_range_is_approx_from_date='" & item.sanction_list_date_range_is_approx_from_date & "'"
                                strQuery += " ,sanction_list_date_range_valid_to='" & item.sanction_list_date_range_valid_to & "'"
                                strQuery += " ,sanction_list_date_range_is_approx_to_date='" & item.sanction_list_date_range_is_approx_to_date & "'"
                                strQuery += " ,comments='" & item.comments & "'"
                                strQuery += " ,Active='" & item.Active & "'"
                                strQuery += " ,CreatedBy='" & item.CreatedBy & "'"
                                strQuery += " ,LastUpdateBy='" & item.LastUpdateBy & "'"
                                strQuery += " ,ApprovedBy='" & item.ApprovedBy & "'"
                                strQuery += " ,CreatedDate='" & item.CreatedDate & "'"
                                strQuery += " ,LastUpdateDate='" & item.LastUpdateDate & "'"
                                strQuery += " ,ApprovedDate='" & item.ApprovedDate & "'"
                                strQuery += " ,Alternateby='" & item.Alternateby & "'"
                                strQuery += "from goAML_Ref_Account_Sanction  where PK_goAML_Ref_Account_Sanction_ID = " & item.PK_goAML_Ref_Account_Sanction_ID
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


                            Else
                                strQuery = "INSERT INTO goAML_Ref_Account_Sanction ( "
                                strQuery += "  ACCOUNT_NO"
                                strQuery += " ,provider"
                                strQuery += " ,sanction_list_name"
                                strQuery += " ,match_criteria"
                                strQuery += " ,link_to_source"
                                strQuery += " ,sanction_list_attributes"
                                strQuery += " ,sanction_list_date_range_valid_from"
                                strQuery += " ,sanction_list_date_range_is_approx_from_date"
                                strQuery += " ,sanction_list_date_range_valid_to"
                                strQuery += " ,sanction_list_date_range_is_approx_to_date"
                                strQuery += " ,comments"
                                strQuery += " ,Active"
                                strQuery += " ,CreatedBy"
                                strQuery += " ,LastUpdateBy"
                                strQuery += " ,ApprovedBy"
                                strQuery += " ,CreatedDate"
                                strQuery += " ,LastUpdateDate"
                                strQuery += " ,ApprovedDate"
                                strQuery += " ,Alternateby"
                                strQuery += " ) VALUES ( "
                                strQuery += " '" & item.ACCOUNT_NO & "'"
                                strQuery += " ,'" & item.provider & "'"
                                strQuery += " ,'" & item.sanction_list_name & "'"
                                strQuery += " ,'" & item.match_criteria & "'"
                                strQuery += " ,'" & item.link_to_source & "'"
                                strQuery += " ,'" & item.sanction_list_attributes & "'"
                                strQuery += " ,'" & item.sanction_list_date_range_valid_from & "'"
                                strQuery += " ,'" & item.sanction_list_date_range_is_approx_from_date & "'"
                                strQuery += " ,'" & item.sanction_list_date_range_valid_to & "'"
                                strQuery += " ,'" & item.sanction_list_date_range_is_approx_to_date & "'"
                                strQuery += " ,'" & item.comments & "'"
                                strQuery += " ,'" & item.Active & "'"
                                strQuery += " ,'" & item.CreatedBy & "'"
                                strQuery += " ,'" & item.LastUpdateBy & "'"
                                strQuery += " ,'" & item.ApprovedBy & "'"
                                strQuery += " ,'" & item.CreatedDate & "'"
                                strQuery += " ,'" & item.LastUpdateDate & "'"
                                strQuery += " ,'" & item.ApprovedDate & "'"
                                strQuery += " ,'" & item.Alternateby & "')"
                                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            End If

                        Next
                    End If
                    If objListSanctionNew Is Nothing Then
                        objListSanctionNew = New List(Of goAML_Ref_Account_Sanction)
                    End If
                    Dim strquery5 As String = ""
                    For Each itemx As goAML_Ref_Account_Sanction In (From x In objListSanctionOld Where x.ACCOUNT_NO = AccountNo Select x).ToList
                        Dim objcek As goAML_Ref_Account_Sanction = objListSanctionNew.Find(Function(x) x.PK_goAML_Ref_Account_Sanction_ID = itemx.PK_goAML_Ref_Account_Sanction_ID)
                        If objcek Is Nothing Then
                            strquery5 += "DELETE FROM obj_ListSanction_Edit		"
                            strquery5 += "where PK_goAML_Ref_Account_Sanction_ID =  " & itemx.PK_goAML_Ref_Account_Sanction_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery5, Nothing)


                        End If
                    Next

                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    'Shared Sub LoadPanelActivation(objPanel As FormPanel, objmodulename As String, unikkey As String)
    '    'done: code anelActivation


    '    'Dim objEmailTemplateDataBLL As NawaBLL.EmailTemplateDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(NawaBLL.EmailTemplateDataBLL))
    '    Dim objgoAML_Customer As GoAMLDAL.goAML_Ref_Customer = GetCustomer(unikkey)
    '    Dim objListgoAML_Ref_Phone As List(Of GoAMLDAL.goAML_Ref_Phone) = GetListgoAML_Ref_PhoneByPKID(unikkey)
    '    Dim objListgoAML_Ref_Address As List(Of GoAMLDAL.goAML_Ref_Address) = GetListgoAML_Ref_AddressByPKID(unikkey)

    '    Dim INDV_gender As GoAMLDAL.goAML_Ref_Jenis_Kelamin = Nothing
    '    Dim INDV_statusCode As GoAMLDAL.goAML_Ref_Status_Rekening = Nothing
    '    Dim INDV_nationality1 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality2 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality3 As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_residence As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Incorporation_legal_form As GoAMLDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
    '    Dim CORP_incorporation_country_code As GoAMLDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Role As GoAMLDAL.goAML_Ref_Party_Role = Nothing

    '    Using db As GoAMLDAL.GoAMLEntities = New GoAMLDAL.GoAMLEntities
    '        If Not objgoAML_Customer Is Nothing Then
    '            Dim strunik As String = unikkey
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objgoAML_Customer.CIF)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objgoAML_Customer.AccountNo)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, CDate(objgoAML_Customer.Opened).ToString("dd-MMM-yyyy"))
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, CDate(objgoAML_Customer.Closed).ToString("dd-MMM-yyyy"))

    '            INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objgoAML_Customer.Gender).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objgoAML_Customer.Balance)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objgoAML_Customer.Date_Balance).ToString("dd-MMM-yyyy"))

    '            INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objgoAML_Customer.Status_Code).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objgoAML_Customer.Beneficiary)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, objgoAML_Customer.Comments)

    '            If objgoAML_Customer.FK_Customer_Type_ID = "1" Then
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "INDV_last_name" & strunik, objgoAML_Customer.INDV_Last_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "INDV_Birthdate" & strunik, CDate(objgoAML_Customer.INDV_BirthDate).ToString("dd-MMM-yyyy"))
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "INDV_Birth_place" & strunik, objgoAML_Customer.INDV_Birth_Place)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "INDV_Mothers_name" & strunik, objgoAML_Customer.INDV_Mothers_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "INDV_Alias" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "INDV_SSN" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Passport", "INDV_Passport_number" & strunik, objgoAML_Customer.INDV_Passport_Number)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Passport", "INDV_Passport_country" & strunik, objgoAML_Customer.INDV_Passport_Country)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lain", "INDV_ID_Number" & strunik, objgoAML_Customer.INDV_ID_Number)

    '                INDV_nationality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality1).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "INDV_Nationality1" & strunik, INDV_nationality1.Keterangan)

    '                INDV_nationality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality2).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "INDV_Nationality2" & strunik, INDV_nationality2.Keterangan)

    '                INDV_nationality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality3).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "INDV_Nationality3" & strunik, INDV_nationality3.Keterangan)

    '                INDV_residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Residence).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "INDV_residence" & strunik, INDV_residence.Keterangan)

    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objgoAML_Customer.INDV_Email)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "INDV_Occupation" & strunik, objgoAML_Customer.INDV_Occupation)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "INDV_employer_name" & strunik, objgoAML_Customer.INDV_Occupation)

    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

    '            Else
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

    '            End If

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objgoAML_Customer.Active.ToString & " -->" & (Not objgoAML_Customer.Active).ToString)

    '            Dim objStore As New Ext.Net.Store
    '            objStore.ID = strunik & "StoreGrid"
    '            objStore.ClientIDMode = Web.UI.ClientIDMode.Static

    '            Dim objModel As New Ext.Net.Model
    '            Dim objField As Ext.Net.ModelField

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "PK_goAML_ref_phone"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_Ref_detail_of"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_for_Table_ID"
    '            objField.Type = ModelFieldType.Int
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_contact_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_communication_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_country_prefix"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_number"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_extension"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "comments"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objStore.Model.Add(objModel)



    '            Dim objListcolumn As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumn.Add(objcolumnNo)
    '            End Using


    '            Dim objColum As Ext.Net.Column


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Detail Dari"
    '            objColum.DataIndex = "FK_Ref_detail_of"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)



    '            objColum = New Ext.Net.Column
    '            objColum.Text = "PK"
    '            objColum.DataIndex = "FK_for_Table_ID"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kategori Kontak"
    '            objColum.DataIndex = "tph_contact_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Jenis Alat Komunikasi"
    '            objColum.DataIndex = "tph_communication_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kode Area Telp"
    '            objColum.DataIndex = "tph_country_prefix"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Telepon"
    '            objColum.DataIndex = "tph_number"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Extensi"
    '            objColum.DataIndex = "tph_extension"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Catatan"
    '            objColum.DataIndex = "comments"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            Dim objdt As Data.DataTable = CopyGenericToDataTable(objListgoAML_Ref_Phone)
    '            Dim objcol As New Data.DataColumn
    '            objcol.ColumnName = "Detail of"
    '            objcol.DataType = GetType(String)
    '            objdt.Columns.Add(objcol)

    '            For Each item As DataRow In objdt.Rows
    '                Dim objtask As GoAMLDAL.goAML_For_Table = GetTabelTypeForByID(item("FK_Ref_Detail_Of"))
    '                If Not objtask Is Nothing Then
    '                    item("FK_Ref_Detail_Of") = objtask.Description
    '                End If
    '            Next

    '            '' ini
    '            Dim objStoreAddress As New Ext.Net.Store
    '            objStoreAddress.ID = strunik & "StoreGridReplacer"
    '            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            Dim objModelAddress As New Ext.Net.Model
    '            Dim objFieldAddress As Ext.Net.ModelField




    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "PK_Customer_Address_ID"
    '            objFieldAddress.Type = ModelFieldType.Auto
    '            objModelAddress.Fields.Add(objFieldAddress)


    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_Ref_Detail_Of"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_To_Table_ID"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address_Type"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Town"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "City"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Zip"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Country_Code"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "State"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Comments"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objStoreAddress.Model.Add(objModelAddress)


    '            Dim objListcolumnAddress As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumnAddress.Add(objcolumnNo)
    '            End Using

    '            '' Address
    '            Dim objColumAddress As Ext.Net.Column

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Detail dari"
    '            objColumAddress.DataIndex = "FK_Ref_Detail_Of"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "PK"
    '            objColumAddress.DataIndex = "FK_To_Table_ID"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address Type"
    '            objColumAddress.DataIndex = "Address_Type"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address"
    '            objColumAddress.DataIndex = "Address"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Town"
    '            objColumAddress.DataIndex = "Town"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "City"
    '            objColumAddress.DataIndex = "City"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Zip"
    '            objColumAddress.DataIndex = "Zip"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Country Code"
    '            objColumAddress.DataIndex = "Country_Code"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "State"
    '            objColumAddress.DataIndex = "State"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Comments"
    '            objColumAddress.DataIndex = "Comments"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)
    '            '' Address


    '            'Dim objStoreAction As New Store
    '            'objStoreAction.ID = strunik & "StoreGridAction"
    '            'objStoreAction.ClientIDMode = Web.UI.ClientIDMode.Static
    '            'Dim ObjModelAction As New Ext.Net.Model

    '            'ObjModelAction.Fields.Add(New ModelField("EmailActionType", ModelFieldType.String))
    '            'ObjModelAction.Fields.Add(New ModelField("TSQLtoExecute", ModelFieldType.String))

    '            'objStoreAction.Model.Add(ObjModelAction)

    '            'Dim objListcolumnAction As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAction.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "EmailActionType", .DataIndex = "EmailActionType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "TSQLtoExecute", .DataIndex = "TSQLtoExecute", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})


    '            'Dim objdtaction As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAction)
    '            'Dim objcolaction As New Data.DataColumn
    '            'objcolaction.ColumnName = "EmailActionType"
    '            'objcolaction.DataType = GetType(String)
    '            'objdtaction.Columns.Add(objcolaction)


    '            'For Each item As DataRow In objdtaction.Rows
    '            '    Dim objtask As NawaDAL.EmailActionType = NawaBLL.EmailTemplateBLL.GetEmailActionTypeByID(item("FK_EmailActionType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailActionType") = objtask.EmailActionTypeName
    '            '    End If

    '            'Next

    '            'Dim objStoreAttachment As New Store With {.ID = "StoreGridAttachment", .ClientIDMode = Web.UI.ClientIDMode.Static}
    '            'Dim ObjModelAttachment As New Ext.Net.Model

    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailAttachmentType", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("ParameterReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaFile", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailRenderAsName", ModelFieldType.String))
    '            'objStoreAttachment.Model.Add(ObjModelAttachment)



    '            'Dim objListcolumnAttachment As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAttachment.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Email Attachment Type", .DataIndex = "EmailAttachmentType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Name", .DataIndex = "NamaReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Parameter", .DataIndex = "ParameterReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report File Name", .DataIndex = "NamaFile", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Render As", .DataIndex = "EmailRenderAsName", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})



    '            'Dim objdtattachment As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAttachment)

    '            'Dim objcolAttachment As New Data.DataColumn
    '            'objcolAttachment.ColumnName = "EmailAttachmentType"
    '            'objcolAttachment.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment)

    '            'Dim objcolAttachment1 As New Data.DataColumn
    '            'objcolAttachment1.ColumnName = "EmailRenderAsName"
    '            'objcolAttachment1.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment1)

    '            'For Each item As DataRow In objdtattachment.Rows
    '            '    Dim objtask As NawaDAL.EmailAttachmentType = NawaBLL.EmailTemplateBLL.GetEMailAttachmentTypebypk(item("FK_EmailAttachmentType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailAttachmentType") = objtask.EmailAttachmentType1
    '            '    End If
    '            '    If item("FK_EmailRenderAs_Id").ToString <> "" Then
    '            '        Dim objtask1 As NawaDAL.EmailRenderA = NawaBLL.EmailTemplateBLL.GetEmailRenderAsbypk(item("FK_EmailRenderAs_Id"))
    '            '        If Not objtask1 Is Nothing Then
    '            '            item("EmailRenderAsName") = objtask1.EmailRenderAsName
    '            '        End If
    '            '    End If

    '            'Next


    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objListgoAML_Ref_Address)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Action", objStoreAction, objListcolumnAction, objdtaction)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Attachment", objStoreAttachment, objListcolumnAttachment, objdtattachment)

    '        End If
    '    End Using

    'End Sub

    Shared Function Reject(ID As String) As Boolean
        'done:reject
        Using objdb As New NawaDAL.NawaDataEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next




                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Update

                            Dim objModuledata As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_AccountDataBLL))
                            Dim objModuledataOld As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_AccountDataBLL))

                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledataOld.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New GoAMLDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledataOld, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledataOld, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objdb.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As GoAMLDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                        'For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_ref_Address
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next
                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))


                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As GoAMLDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As GoAMLDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function
#End Region

#Region "GetData Related Data and Sanction"
    Shared Function getRelatedEntitiesByID(id As String) As List(Of goAML_Ref_Account_Related_Entity)
        Dim listRelatedEntities As New List(Of goAML_Ref_Account_Related_Entity)
        Dim listObjRelatedEntities As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Account_Related_Entity where ACCOUNT_NO = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedEntities.Rows
            Dim Entity As New goAML_Ref_Account_Related_Entity
            Entity.PK_goAML_Ref_Account_Related_Entity_ID = item("PK_goAML_Ref_Account_Related_Entity_ID")
            Entity.ACCOUNT_NO = item("ACCOUNT_NO")
            If Not IsDBNull(item("business")) Then
                Entity.business = item("business")
            End If
            If Not IsDBNull(item("business_closed")) Then
                Entity.business_closed = item("business_closed")
            End If
            If Not IsDBNull(item("comments")) Then
                Entity.comments = item("comments")
            End If
            If Not IsDBNull(item("commercial_name")) Then
                Entity.commercial_name = item("commercial_name")
            End If
            If Not IsDBNull(item("date_business_closed")) Then
                Entity.date_business_closed = item("date_business_closed")
            End If
            If Not IsDBNull(item("entity_status")) Then
                Entity.entity_status = item("entity_status")
            End If
            If Not IsDBNull(item("entity_status_date")) Then
                Entity.entity_status_date = item("entity_status_date")
            End If
            If Not IsDBNull(item("incorporation_country_code")) Then
                Entity.incorporation_country_code = item("incorporation_country_code")
            End If
            If Not IsDBNull(item("incorporation_date")) Then
                Entity.incorporation_date = item("incorporation_date")
            End If
            If Not IsDBNull(item("incorporation_legal_form")) Then
                Entity.incorporation_legal_form = item("incorporation_legal_form")
            End If
            If Not IsDBNull(item("incorporation_number")) Then
                Entity.incorporation_number = item("incorporation_number")
            End If
            If Not IsDBNull(item("incorporation_state")) Then
                Entity.incorporation_state = item("incorporation_state")
            End If
            If Not IsDBNull(item("name")) Then
                Entity.name = item("name")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                Entity.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                Entity.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                Entity.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                Entity.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("tax_number")) Then
                Entity.tax_number = item("tax_number")
            End If
            If Not IsDBNull(item("tax_reg_number")) Then
                Entity.tax_reg_number = item("tax_reg_number")
            End If
            If Not IsDBNull(item("Active")) Then
                Entity.Active = item("Active")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Entity.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Entity.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("ApprovedBy")) Then
                Entity.ApprovedBy = item("ApprovedBy")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Entity.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Entity.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("ApprovedDate")) Then
                Entity.ApprovedDate = item("ApprovedDate")
            End If
            If Not IsDBNull(item("Alternateby")) Then
                Entity.Alternateby = item("Alternateby")
            End If
            If Not IsDBNull(item("CIF_NO_REF")) Then
                Entity.CIF_NO_REF = item("CIF_NO_REF")
            End If
            If Not IsDBNull(item("WIC_NO_REF")) Then
                Entity.WIC_NO_REF = item("WIC_NO_REF")
            End If
            If Not IsDBNull(item("ACCOUNT_ENTITY_RELATION")) Then
                Entity.ACCOUNT_ENTITY_RELATION = item("ACCOUNT_ENTITY_RELATION")
            End If
            listRelatedEntities.Add(Entity)
        Next
        Return listRelatedEntities
    End Function

    Shared Function getRelatedPersonByID(id As String) As List(Of goAML_Ref_Account_Related_Person)
        Dim listRelatedPerson As New List(Of goAML_Ref_Account_Related_Person)
        Dim listObjRelatedPerson As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Account_Related_Person where ACCOUNT_NO = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedPerson.Rows
            Dim Person As New goAML_Ref_Account_Related_Person
            Person.PK_goAML_Ref_Account_Related_Person_ID = item("PK_goAML_Ref_Account_Related_Person_ID")
            Person.ACCOUNT_NO = item("ACCOUNT_NO")
            If Not IsDBNull(item("alias")) Then
                Person.alias = item("alias")
            End If
            If Not IsDBNull(item("birthdate")) Then
                Person.birthdate = item("birthdate")
            End If
            If Not IsDBNull(item("comments")) Then
                Person.comments = item("comments")
            End If
            If Not IsDBNull(item("birth_place")) Then
                Person.birth_place = item("birth_place")
            End If
            If Not IsDBNull(item("country_of_birth")) Then
                Person.country_of_birth = item("country_of_birth")
            End If
            If Not IsDBNull(item("date_deceased")) Then
                Person.date_deceased = item("date_deceased")
            End If
            If Not IsDBNull(item("deceased")) Then
                Person.deceased = item("deceased")
            End If
            If Not IsDBNull(item("first_name")) Then
                Person.first_name = item("first_name")
            End If
            If Not IsDBNull(item("full_name_frn")) Then
                Person.full_name_frn = item("full_name_frn")
            End If
            If Not IsDBNull(item("gender")) Then
                Person.gender = item("gender")
            End If
            If Not IsDBNull(item("id_number")) Then
                Person.id_number = item("id_number")
            End If
            If Not IsDBNull(item("IS_PRIMARY")) Then
                Person.IS_PRIMARY = item("IS_PRIMARY")
            End If
            If Not IsDBNull(item("is_protected")) Then
                Person.is_protected = item("is_protected")
            End If
            If Not IsDBNull(item("last_name")) Then
                Person.last_name = item("last_name")
            End If
            If Not IsDBNull(item("middle_name")) Then
                Person.middle_name = item("middle_name")
            End If
            If Not IsDBNull(item("mothers_name")) Then
                Person.mothers_name = item("mothers_name")
            End If
            If Not IsDBNull(item("nationality1")) Then
                Person.nationality1 = item("nationality1")
            End If
            If Not IsDBNull(item("nationality2")) Then
                Person.nationality2 = item("nationality2")
            End If
            If Not IsDBNull(item("nationality3")) Then
                Person.nationality3 = item("nationality3")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                Person.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                Person.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                Person.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                Person.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("occupation")) Then
                Person.occupation = item("occupation")
            End If
            If Not IsDBNull(item("passport_country")) Then
                Person.passport_country = item("passport_country")
            End If
            If Not IsDBNull(item("passport_number")) Then
                Person.passport_number = item("passport_number")
            End If
            If Not IsDBNull(item("prefix")) Then
                Person.prefix = item("prefix")
            End If
            If Not IsDBNull(item("residence")) Then
                Person.residence = item("residence")
            End If
            If Not IsDBNull(item("residence_since")) Then
                Person.residence_since = item("residence_since")
            End If
            If Not IsDBNull(item("ROLE")) Then
                Person.ROLE = item("ROLE")
            End If
            If Not IsDBNull(item("source_of_wealth")) Then
                Person.source_of_wealth = item("source_of_wealth")
            End If
            If Not IsDBNull(item("ssn")) Then
                Person.ssn = item("ssn")
            End If
            If Not IsDBNull(item("title")) Then
                Person.title = item("title")
            End If
            If Not IsDBNull(item("tax_number")) Then
                Person.tax_number = item("tax_number")
            End If
            If Not IsDBNull(item("tax_reg_number")) Then
                Person.tax_reg_number = item("tax_reg_number")
            End If
            If Not IsDBNull(item("Active")) Then
                Person.Active = item("Active")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Person.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Person.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("ApprovedBy")) Then
                Person.ApprovedBy = item("ApprovedBy")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Person.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Person.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("ApprovedDate")) Then
                Person.ApprovedDate = item("ApprovedDate")
            End If
            If Not IsDBNull(item("Alternateby")) Then
                Person.Alternateby = item("Alternateby")
            End If
            If Not IsDBNull(item("CIF_NO_REF")) Then
                Person.CIF_NO_REF = item("CIF_NO_REF")
            End If
            If Not IsDBNull(item("WIC_NO_REF")) Then
                Person.WIC_NO_REF = item("WIC_NO_REF")
            End If
            listRelatedPerson.Add(Person)
        Next
        Return listRelatedPerson
    End Function

    Shared Function getRelatedAccountByID(id As String) As List(Of goAML_Ref_Account_Related_Account)
        Dim listRelatedAccount As New List(Of goAML_Ref_Account_Related_Account)
        Dim listObjRelatedAccount As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Account_Related_Account where ACCOUNT_NO = '" & id & "'", Nothing)
        For Each item As DataRow In listObjRelatedAccount.Rows
            Dim Account As New goAML_Ref_Account_Related_Account
            Account.PK_goAML_Ref_Account_Rel_Account_ID = item("PK_goAML_Ref_Account_Rel_Account_ID")
            Account.ACCOUNT_NO = item("ACCOUNT_NO")
            If Not IsDBNull(item("account")) Then
                Account.account = item("account")
            End If
            If Not IsDBNull(item("ACCOUNT_ACCOUNT_RELATIONSHIP")) Then
                Account.ACCOUNT_ACCOUNT_RELATIONSHIP = item("ACCOUNT_ACCOUNT_RELATIONSHIP")
            End If
            If Not IsDBNull(item("comments")) Then
                Account.comments = item("comments")
            End If
            If Not IsDBNull(item("account_category")) Then
                Account.account_category = item("account_category")
            End If
            If Not IsDBNull(item("account_name")) Then
                Account.account_name = item("account_name")
            End If
            If Not IsDBNull(item("account_type")) Then
                Account.account_type = item("account_type")
            End If
            If Not IsDBNull(item("balance")) Then
                Account.balance = item("balance")
            End If
            If Not IsDBNull(item("beneficiary")) Then
                Account.beneficiary = item("beneficiary")
            End If
            If Not IsDBNull(item("beneficiary_comment")) Then
                Account.beneficiary_comment = item("beneficiary_comment")
            End If
            If Not IsDBNull(item("branch")) Then
                Account.branch = item("branch")
            End If
            If Not IsDBNull(item("client_number")) Then
                Account.client_number = item("client_number")
            End If
            If Not IsDBNull(item("closed_date")) Then
                Account.closed_date = item("closed_date")
            End If
            If Not IsDBNull(item("currency_code")) Then
                Account.currency_code = item("currency_code")
            End If
            If Not IsDBNull(item("date_balance")) Then
                Account.date_balance = item("date_balance")
            End If
            If Not IsDBNull(item("iban")) Then
                Account.iban = item("iban")
            End If
            If Not IsDBNull(item("institution_code")) Then
                Account.institution_code = item("institution_code")
            End If
            If Not IsDBNull(item("institution_country")) Then
                Account.institution_country = item("institution_country")
            End If
            If Not IsDBNull(item("institution_name")) Then
                Account.institution_name = item("institution_name")
            End If
            If Not IsDBNull(item("non_bank_institution")) Then
                Account.non_bank_institution = item("non_bank_institution")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_from_date")) Then
                Account.relation_date_range_is_approx_from_date = item("relation_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("relation_date_range_is_approx_to_date")) Then
                Account.relation_date_range_is_approx_to_date = item("relation_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("relation_date_range_valid_from")) Then
                Account.relation_date_range_valid_from = item("relation_date_range_valid_from")
            End If
            If Not IsDBNull(item("relation_date_range_valid_to")) Then
                Account.relation_date_range_valid_to = item("relation_date_range_valid_to")
            End If
            If Not IsDBNull(item("opened_date")) Then
                Account.opened_date = item("opened_date")
            End If
            If Not IsDBNull(item("status_code")) Then
                Account.status_code = item("status_code")
            End If
            If Not IsDBNull(item("status_date")) Then
                Account.status_date = item("status_date")
            End If
            If Not IsDBNull(item("swift")) Then
                Account.swift = item("swift")
            End If
            If Not IsDBNull(item("Active")) Then
                Account.Active = item("Active")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Account.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Account.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("ApprovedBy")) Then
                Account.ApprovedBy = item("ApprovedBy")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Account.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Account.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("ApprovedDate")) Then
                Account.ApprovedDate = item("ApprovedDate")
            End If
            If Not IsDBNull(item("Alternateby")) Then
                Account.Alternateby = item("Alternateby")
            End If
            If Not IsDBNull(item("Alternateby")) Then
                Account.Alternateby = item("Alternateby")
            End If
            If Not IsDBNull(item("ACCOUNT_NO_REF")) Then
                Account.ACCOUNT_NO_REF = item("ACCOUNT_NO_REF")
            End If
            listRelatedAccount.Add(Account)
        Next
        Return listRelatedAccount
    End Function

    Shared Function getSanctionByID(id As String) As List(Of goAML_Ref_Account_Sanction)
        Dim listSanction As New List(Of goAML_Ref_Account_Sanction)
        Dim listObjSanction As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM goAML_Ref_Account_Sanction where ACCOUNT_NO = '" & id & "'", Nothing)
        For Each item As DataRow In listObjSanction.Rows
            Dim Sanction As New goAML_Ref_Account_Sanction
            Sanction.PK_goAML_Ref_Account_Sanction_ID = item("PK_goAML_Ref_Account_Sanction_ID")
            Sanction.ACCOUNT_NO = item("ACCOUNT_NO")
            If Not IsDBNull(item("link_to_source")) Then
                Sanction.link_to_source = item("link_to_source")
            End If
            If Not IsDBNull(item("match_criteria")) Then
                Sanction.match_criteria = item("match_criteria")
            End If
            If Not IsDBNull(item("comments")) Then
                Sanction.comments = item("comments")
            End If
            If Not IsDBNull(item("provider")) Then
                Sanction.provider = item("provider")
            End If
            If Not IsDBNull(item("sanction_list_attributes")) Then
                Sanction.sanction_list_attributes = item("sanction_list_attributes")
            End If
            If Not IsDBNull(item("sanction_list_date_range_is_approx_from_date")) Then
                Sanction.sanction_list_date_range_is_approx_from_date = item("sanction_list_date_range_is_approx_from_date")
            End If
            If Not IsDBNull(item("sanction_list_date_range_is_approx_to_date")) Then
                Sanction.sanction_list_date_range_is_approx_to_date = item("sanction_list_date_range_is_approx_to_date")
            End If
            If Not IsDBNull(item("sanction_list_date_range_valid_from")) Then
                Sanction.sanction_list_date_range_valid_from = item("sanction_list_date_range_valid_from")
            End If
            If Not IsDBNull(item("sanction_list_date_range_valid_to")) Then
                Sanction.sanction_list_date_range_valid_to = item("sanction_list_date_range_valid_to")
            End If
            If Not IsDBNull(item("sanction_list_name")) Then
                Sanction.sanction_list_name = item("sanction_list_name")
            End If
            If Not IsDBNull(item("Active")) Then
                Sanction.Active = item("Active")
            End If
            If Not IsDBNull(item("CreatedBy")) Then
                Sanction.CreatedBy = item("CreatedBy")
            End If
            If Not IsDBNull(item("LastUpdateBy")) Then
                Sanction.LastUpdateBy = item("LastUpdateBy")
            End If
            If Not IsDBNull(item("ApprovedBy")) Then
                Sanction.ApprovedBy = item("ApprovedBy")
            End If
            If Not IsDBNull(item("CreatedDate")) Then
                Sanction.CreatedDate = item("CreatedDate")
            End If
            If Not IsDBNull(item("LastUpdateDate")) Then
                Sanction.LastUpdateDate = item("LastUpdateDate")
            End If
            If Not IsDBNull(item("ApprovedDate")) Then
                Sanction.ApprovedDate = item("ApprovedDate")
            End If
            If Not IsDBNull(item("Alternateby")) Then
                Sanction.Alternateby = item("Alternateby")
            End If
            listSanction.Add(Sanction)
        Next
        Return listSanction
    End Function

#End Region

#Region "Class"
    Public Class goAML_Ref_Account
        Public Property PK_Account_ID As Integer
        Public Property Branch As String
        Public Property Account_No As String
        Public Property Currency_Code As String
        Public Property Account_Name As String
        Public Property IBAN As String
        Public Property client_number As String
        Public Property personal_account_type As String
        Public Property opened As Nullable(Of Date)
        Public Property closed As Nullable(Of Date)
        Public Property balance As Nullable(Of Decimal)
        Public Property date_balance As Nullable(Of Date)
        Public Property status_code As String
        Public Property beneficiary As String
        Public Property beneficiary_comment As String
        Public Property comments As String
        Public Property isUpdateFromDataSource As Nullable(Of Boolean)
        Public Property FK_CIF_Entity_ID As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
        Public Property INSTITUTION_COUNTRY As String
        Public Property COLLECTION_ACCOUNT As Nullable(Of Boolean)
        Public Property ACCOUNT_CATEGORY As String
        Public Property ACCOUNT_TYPE As String
        Public Property STATUS_DATE As Nullable(Of Date)

    End Class

    Public Class goAML_Ref_Account_Account_Relationship
        Public Property PK_goAML_Ref_Account_Account_Relationship_ID As Long
        Public Property Kode As String
        Public Property Keterangan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Additional_Information
        Public Property PK_goAML_Ref_Account_Additional_Information_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property info_type As String
        Public Property info_subject As String
        Public Property info_text As String
        Public Property info_numeric As Nullable(Of Double)
        Public Property info_date As Nullable(Of Date)
        Public Property info_boolean As Nullable(Of Boolean)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Category
        Public Property PK_goAML_Ref_Account_Category_ID As Long
        Public Property Kode As String
        Public Property Keterangan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Entity_Relationship
        Public Property PK_goAML_Ref_Account_Entity_Relationship_ID As Long
        Public Property Kode As String
        Public Property Keterangan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Funds
        Public Property PK_goAML_Ref_Account_Funds_ID As Long
        Public Property Account_No As String
        Public Property currency_code As String
        Public Property currency_balance As Nullable(Of Double)
        Public Property currency_balance_date As Nullable(Of Date)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Network_Device
        Public Property PK_goAML_Ref_Account_Network_Device_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property device_number As String
        Public Property operating_system As String
        Public Property service_provider As String
        Public Property ipv6 As String
        Public Property ipv4 As String
        Public Property cgn_port As Nullable(Of Integer)
        Public Property ipv4_ipv6 As String
        Public Property nat As String
        Public Property first_seen_date As Nullable(Of Date)
        Public Property last_seen_date As Nullable(Of Date)
        Public Property using_proxy As Nullable(Of Boolean)
        Public Property city As String
        Public Property country As String
        Public Property comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Person_Role
        Public Property PK_goAML_Ref_Account_Person_Role_Relationship_ID As Long
        Public Property Kode As String
        Public Property Keterangan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class


    Public Class goAML_Ref_Account_Related_Account
        Public Property PK_goAML_Ref_Account_Rel_Account_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property ACCOUNT_ACCOUNT_RELATIONSHIP As String
        Public Property relation_date_range_valid_from As Nullable(Of Date)
        Public Property relation_date_range_is_approx_from_date As Nullable(Of Boolean)
        Public Property relation_date_range_valid_to As Nullable(Of Date)
        Public Property relation_date_range_is_approx_to_date As Nullable(Of Boolean)
        Public Property comments As String
        Public Property institution_name As String
        Public Property institution_code As String
        Public Property swift As String
        Public Property institution_country As String
        Public Property non_bank_institution As Nullable(Of Boolean)
        Public Property collection_account As Nullable(Of Boolean)
        Public Property branch As String
        Public Property account_category As String
        Public Property account As String
        Public Property currency_code As String
        Public Property account_name As String
        Public Property iban As String
        Public Property client_number As String
        Public Property account_type As String
        Public Property opened_date As Nullable(Of Date)
        Public Property closed_date As Nullable(Of Date)
        Public Property balance As Nullable(Of Double)
        Public Property date_balance As Nullable(Of Date)
        Public Property status_code As String
        Public Property status_date As Nullable(Of Date)
        Public Property beneficiary As String
        Public Property beneficiary_comment As String
        Public Property IS_SEARCH As Nullable(Of Boolean)
        Public Property ACCOUNT_NO_REF As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Related_Entity
        Public Property PK_goAML_Ref_Account_Related_Entity_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property ACCOUNT_ENTITY_RELATION As String
        Public Property relation_date_range_valid_from As Nullable(Of Date)
        Public Property relation_date_range_is_approx_from_date As Nullable(Of Boolean)
        Public Property relation_date_range_valid_to As Nullable(Of Date)
        Public Property relation_date_range_is_approx_to_date As Nullable(Of Boolean)
        Public Property comments As String
        Public Property name As String
        Public Property commercial_name As String
        Public Property incorporation_legal_form As String
        Public Property incorporation_number As String
        Public Property business As String
        Public Property entity_status As String
        Public Property entity_status_date As Nullable(Of Date)
        Public Property incorporation_state As String
        Public Property incorporation_country_code As String
        Public Property incorporation_date As Nullable(Of Date)
        Public Property business_closed As Nullable(Of Boolean)
        Public Property date_business_closed As Nullable(Of Date)
        Public Property IS_SEARCH As Nullable(Of Boolean)
        Public Property CIF_NO_REF As String
        Public Property WIC_NO_REF As String
        Public Property tax_number As String
        Public Property tax_reg_number As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Related_Person
        Public Property PK_goAML_Ref_Account_Related_Person_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property IS_PRIMARY As Nullable(Of Boolean)
        Public Property ROLE As String
        Public Property relation_date_range_valid_from As Nullable(Of Date)
        Public Property relation_date_range_is_approx_from_date As Nullable(Of Boolean)
        Public Property relation_date_range_valid_to As Nullable(Of Date)
        Public Property relation_date_range_is_approx_to_date As Nullable(Of Boolean)
        Public Property comments As String
        Public Property gender As String
        Public Property title As String
        Public Property first_name As String
        Public Property middle_name As String
        Public Property prefix As String
        Public Property last_name As String
        Public Property birthdate As Nullable(Of Date)
        Public Property birth_place As String
        Public Property country_of_birth As String
        Public Property mothers_name As String
        Public Property [alias] As String
        Public Property full_name_frn As String
        Public Property ssn As String
        Public Property passport_number As String
        Public Property passport_country As String
        Public Property id_number As String
        Public Property nationality1 As String
        Public Property nationality2 As String
        Public Property nationality3 As String
        Public Property residence As String
        Public Property residence_since As Nullable(Of Date)
        Public Property occupation As String
        Public Property deceased As Nullable(Of Boolean)
        Public Property date_deceased As Nullable(Of Date)
        Public Property tax_number As String
        Public Property tax_reg_number As String
        Public Property source_of_wealth As String
        Public Property is_protected As Nullable(Of Boolean)
        Public Property IS_SEARCH As Nullable(Of Boolean)
        Public Property CIF_NO_REF As String
        Public Property WIC_NO_REF As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Sanction
        Public Property PK_goAML_Ref_Account_Sanction_ID As Long
        Public Property ACCOUNT_NO As String
        Public Property provider As String
        Public Property sanction_list_name As String
        Public Property match_criteria As String
        Public Property link_to_source As String
        Public Property sanction_list_attributes As String
        Public Property sanction_list_date_range_valid_from As Nullable(Of Date)
        Public Property sanction_list_date_range_is_approx_from_date As Nullable(Of Boolean)
        Public Property sanction_list_date_range_valid_to As Nullable(Of Date)
        Public Property sanction_list_date_range_is_approx_to_date As Nullable(Of Boolean)
        Public Property comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    Public Class goAML_Ref_Account_Type
        Public Property PK_goAML_Ref_Account_Type_ID As Long
        Public Property Kode As String
        Public Property Keterangan As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

    End Class

    'Public Class goAML_NewAccountDataBLL
    '    Public objgoAML_Ref_Account As New GoAMLDAL.goAML_Ref_Account
    '    Public objListgoAML_Ref_Account_Signatory As New List(Of GoAMLDAL.goAML_Ref_Account_Signatory)
    '    Public objListgoAML_RelatedEntities As New List(Of goAML_Ref_Account_Related_Entity)
    '    Public objListgoAML_RelatedPerson As New List(Of goAML_Ref_Account_Related_Person)
    '    Public objListgoAML_RelatedAccount As New List(Of goAML_Ref_Account_Related_Account)
    '    Public objListgoAML_Sanction As New List(Of goAML_Ref_Account_Sanction)

    'End Class



#End Region

End Class
