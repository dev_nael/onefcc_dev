﻿Imports GoAMLBLL.goAML
Imports GoAMLDAL

Public Class WICDataBLL
    Public ObjWIC As goAML_Ref_WIC
    Public ObjWICAddress As List(Of goAML_Ref_Address)
    Public ObjWICPhone As List(Of goAML_Ref_Phone)
    Public ObjWICAddressEmployer As List(Of goAML_Ref_Address)
    Public OBjWICPhoneEmployer As List(Of goAML_Ref_Phone)
    Public ObjWICIdentification As List(Of goAML_Person_Identification)
    Public ObjDirector As List(Of WICDirectorDataBLL)
    
    ''goAML 5.0.1, add by Septian, 2023-03-06
    Public ObjWIC2 As goAML_Ref_WIC2
    Public ObjGrid As goAML_Grid_WIC
    ''End goAML 5.0.1
    
    Public Sub New()

    End Sub

    'add by Septian, goAML WIC 5.0.1, 2023-02-21
    Public Class goAML_Ref_WIC2
        Public Property PK_Customer_ID As Integer
        Public Property WIC_No As String
        Public Property IS_PROTECTED As Boolean
        Public Property IS_REAL_WIC As Boolean
    End Class
    
    Public Class goAML_Ref_WIC_Social_Media
        Public Property PK_goAML_Ref_WIC_Social_Media_ID As Integer
        Public Property WIC_No As String
        Public Property Platform As String
        Public Property User_Name As String
        Public Property Comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Ref_WIC_Previous_Name
        Public Property PK_goAML_Ref_WIC_Previous_Name_ID As Integer
        Public Property WIC_No As String
        Public Property First_Name As String
        Public Property Last_Name As String
        Public Property Comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Ref_WIC_Email
        Public Property PK_goAML_Ref_WIC_Email_ID As Integer
        Public Property WIC_No As String
        Public Property email_address As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        ' [START] 2023-11-14, Nael: Add
        Public Property FK_REF_DETAIL_OF As Nullable(Of Integer)
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer)
        ' [END] 2023-11-14, Nael: Add

    End Class

    Public Class goAML_Ref_WIC_Employment_History
        Public Property PK_goAML_Ref_WIC_Employment_History_ID As Integer
        Public Property WIC_No As String
        Public Property employer_name As String
        Public Property employer_business As String
        Public Property employer_identifier As String
        Public Property employment_period_valid_from As Nullable(Of Date)
        Public Property employment_period_is_approx_from_date As Nullable(Of Boolean)
        Public Property employment_period_valid_to As Nullable(Of Date)
        Public Property employment_period_is_approx_to_date As Nullable(Of Boolean)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Ref_WIC_Person_PEP
        Public Property PK_goAML_Ref_WIC_PEP_ID As Integer
        Public Property WIC_No As String
        Public Property pep_country As String
        Public Property function_name As String
        Public Property function_description As String
        Public Property pep_date_range_valid_from As Nullable(Of Date)
        Public Property pep_date_range_is_approx_from_date As Nullable(Of Boolean)
        Public Property pep_date_range_valid_to As Nullable(Of Date)
        Public Property pep_date_range_is_approx_to_date As Nullable(Of Boolean)
        Public Property comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        ' [START] 2023-11-14, Nael: Add
        Public Property FK_REF_DETAIL_OF As Nullable(Of Integer)
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer)
        ' [END] 2023-11-14, Nael: Add

    End Class

    Public Class goAML_Ref_WIC_Network_Devices
        Public Property PK_goAML_Ref_WIC_Network_Device_ID As Integer
        Public Property WIC_No As String
        Public Property Device_Number As String
        Public Property Operating_System As String
        Public Property Service_Provider As String
        Public Property IPv6 As String
        Public Property IPv4 As String
        Public Property CGN_port As String
        Public Property IPv4_IPv6 As String
        Public Property NAT As String
        Public Property First_Seen_Date As Nullable(Of Date)
        Public Property Last_Seen_Date As Nullable(Of Date)
        Public Property Using_Proxy As Nullable(Of Boolean)
        Public Property City As String
        Public Property Country As String
        Public Property Comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
    End Class

    Public Class goAML_Ref_WIC_Sanction
        Public Property PK_goAML_Ref_WIC_Sanction_ID As Integer
        Public Property WIC_No As String
        Public Property Provider As String
        Public Property Sanction_List_Name As String
        Public Property Match_Criteria As String
        Public Property Link_To_Source As String
        Public Property Sanction_List_Attributes As String
        Public Property Sanction_List_Date_Range_Valid_From As Nullable(Of Date)
        Public Property Sanction_List_Date_Range_Is_Approx_From_Date As Nullable(Of Boolean)
        Public Property Sanction_List_Date_Range_Valid_To As Nullable(Of Date)
        Public Property Sanction_List_Date_Range_Is_Approx_To_Date As Nullable(Of Boolean)
        Public Property Comments As String
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
        Public Property FK_REF_DETAIL_OF As Integer ' 2023-11-09, Nael
        Public Property FK_FOR_TABLE_ID As Integer ' 2023-11-09, Nael
    End Class

    Public Class goAML_Ref_WIC_Related_Person
        Public Property PK_goAML_Ref_WIC_Related_Person_ID As Integer
        Public Property WIC_No As String
        Public Property Person_Person_Relation As String
        Public Property Relation_Date_Range_Valid_From As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_From_Date As Nullable(Of Boolean)
        Public Property Relation_Date_Range_Valid_To As Nullable(Of Date)
        Public Property Relation_Date_Range_Is_Approx_To_Date As Nullable(Of Boolean)
        Public Property Comments As String
        Public Property Gender As String
        Public Property Title As String
        Public Property First_Name As String
        Public Property Middle_Name As String
        Public Property Prefix As String
        Public Property Last_Name As String
        Public Property Birth_Date As Nullable(Of Date)
        Public Property Birth_Place As String
        Public Property Country_Of_Birth As String
        Public Property Mother_Name As String
        Public Property _Alias As String
        Public Property Full_Name_Frn As String
        Public Property SSN As String
        Public Property Passport_Number As String
        Public Property Passport_Country As String
        Public Property ID_Number As String
        Public Property Nationality1 As String
        Public Property Nationality2 As String
        Public Property Nationality3 As String
        Public Property Residence As String
        Public Property Residence_Since As Nullable(Of Date)
        Public Property Occupation As String
        Public Property Deceased As Nullable(Of Boolean)
        Public Property Date_Deceased As Nullable(Of Date)
        Public Property Tax_Number As String
        Public Property Is_PEP As Nullable(Of Boolean)
        Public Property Source_Of_Wealth As String
        Public Property Is_Protected As Nullable(Of Boolean)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String
        Public Property EMPLOYER_NAME As String ' 2023-10-24, Nael: menyesuaikan dengan tabel goAML_Ref_WIC_Related_Person di database
        Public Property relation_comments As String ' 2023-10-24, Nael: menyesuaikan dengan tabel goAML_Ref_WIC_Related_Person di database

        'Grid
        Public Property ObjList_GoAML_Person_Identification As List(Of DataModel.goAML_Person_Identification)
        Public Property ObjList_GoAML_Ref_Phone As List(Of DataModel.goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address As List(Of DataModel.goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Phone_Work As List(Of DataModel.goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Address_Work As List(Of DataModel.goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Customer_Email As List(Of DataModel.goAML_Ref_Customer_Email)
        Public Property ObjList_GoAML_Ref_Customer_Sanction As List(Of DataModel.goAML_Ref_Customer_Sanction)
        Public Property ObjList_GoAML_Ref_Customer_PEP As List(Of DataModel.goAML_Ref_Customer_PEP)
    End Class

    Public Class goAML_Ref_WIC_Additional_Information
        Public Property PK_goAML_Report_Additional_Information_ID As Integer
        Public Property WIC_No As String
        Public Property info_type As String
        Public Property info_subject As String
        Public Property info_text As String
        Public Property info_numeric As Integer
        Public Property info_date As Nullable(Of Date)
        Public Property info_boolean As Nullable(Of Boolean)
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String


    End Class
    Public Class goAML_Ref_WIC_URL
        'Core
        Public Property PK_goAML_Ref_WIC_URL_ID As Long
        Public Property WIC_NO As String
        Public Property URL As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        ' [START] 2023-11-14, Nael: Add
        Public Property FK_REF_DETAIL_OF As Nullable(Of Integer)
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer)
        ' [END] 2023-11-14, Nael: Add
    End Class

    Public Class goAML_Ref_WIC_Entity_Identification
        'Core
        Public Property PK_goAML_Ref_WIC_Entity_Identifications_ID As Long
        Public Property WIC_NO As String
        Public Property TYPE As String
        Public Property NUMBER As String
        Public Property ISSUE_DATE As Nullable(Of Date)
        Public Property EXPIRY_DATE As Nullable(Of Date)
        Public Property ISSUED_BY As String
        Public Property ISSUE_COUNTRY As String
        Public Property COMMENTS As String

        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        ' [START] 2023-11-14, Nael: Add
        Public Property FK_REF_DETAIL_OF As Nullable(Of Integer)
        Public Property FK_FOR_TABLE_ID As Nullable(Of Integer)
        ' [END] 2023-11-14, Nael: Add

    End Class

    Public Class goAML_Ref_WIC_Related_Entity
        'Core
        Public Property PK_goAML_Ref_WIC_Related_Entities_ID As Long
        Public Property WIC_NO As String
        Public Property ENTITY_ENTITY_RELATION As String
        Public Property RELATION_DATE_RANGE_VALID_FROM As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_FROM_DATE As Boolean
        Public Property RELATION_DATE_RANGE_VALID_TO As Nullable(Of Date)
        Public Property RELATION_DATE_RANGE_IS_APPROX_TO_DATE As Boolean
        Public Property SHARE_PERCENTAGE As Decimal
        Public Property COMMENTS As String
        Public Property NAME As String
        Public Property COMMERCIAL_NAME As String
        Public Property INCORPORATION_LEGAL_FORM As String
        Public Property INCORPORATION_NUMBER As String
        Public Property BUSINESS As String
        Public Property ENTITY_STATUS As String
        Public Property ENTITY_STATUS_DATE As Nullable(Of Date)
        Public Property INCORPORATION_STATE As String
        Public Property INCORPORATION_COUNTRY_CODE As String
        Public Property INCORPORATION_DATE As Nullable(Of Date)
        Public Property BUSINESS_CLOSED As Boolean
        Public Property DATE_BUSINESS_CLOSED As Nullable(Of Date)
        Public Property TAX_NUMBER As String
        Public Property TAX_REG_NUMBER As String
        Public Property RELATION_COMMENTS As String ' 2023-10-24, Nael


        'Additional
        Public Property Active As Nullable(Of Boolean)
        Public Property CreatedBy As String
        Public Property LastUpdateBy As String
        Public Property ApprovedBy As String
        Public Property CreatedDate As Nullable(Of Date)
        Public Property LastUpdateDate As Nullable(Of Date)
        Public Property ApprovedDate As Nullable(Of Date)
        Public Property Alternateby As String

        'Grid
        Public Property ObjList_GoAML_Ref_Customer_Phone As List(Of DataModel.goAML_Ref_Phone)
        Public Property ObjList_GoAML_Ref_Customer_Address As List(Of DataModel.goAML_Ref_Address)
        Public Property ObjList_GoAML_Ref_Customer_Email As List(Of DataModel.goAML_Ref_Customer_Email)
        Public Property ObjList_GoAML_Ref_Customer_Entity_Identification As List(Of DataModel.goAML_Ref_Customer_Entity_Identification)
        Public Property ObjList_GoAML_Ref_Customer_URL As List(Of DataModel.goAML_Ref_Customer_URL)
        Public Property ObjList_GoAML_Ref_Customer_Sanction As List(Of DataModel.goAML_Ref_Customer_Sanction)
    End Class

    Public Class goAML_Grid_WIC
        Public Property ObjList_GoAML_SocialMedia As List(Of WICDataBLL.goAML_Ref_WIC_Social_Media)
        Public Property ObjList_GoAML_PreviousName As List(Of WICDataBLL.goAML_Ref_WIC_Previous_Name)
        Public Property ObjList_GoAML_Email As List(Of WICDataBLL.goAML_Ref_WIC_Email)
        Public Property ObjList_GoAML_EmploymentHistory As List(Of WICDataBLL.goAML_Ref_WIC_Employment_History)
        Public Property ObjList_GoAML_PersonPEP As List(Of WICDataBLL.goAML_Ref_WIC_Person_PEP)
        Public Property ObjList_GoAML_NetworkDevice As List(Of WICDataBLL.goAML_Ref_WIC_Network_Devices)
        Public Property ObjList_GoAML_Sanction As List(Of WICDataBLL.goAML_Ref_WIC_Sanction)
        Public Property ObjList_GoAML_RelatedPerson As List(Of WICDataBLL.goAML_Ref_WIC_Related_Person)
        Public Property ObjList_GoAML_AdditionalInformation As List(Of WICDataBLL.goAML_Ref_WIC_Additional_Information)
        Public Property ObjList_GoAML_URL As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_URL)
        Public Property ObjList_GoAML_EntityIdentification As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Entity_Identification)
        Public Property ObjList_GoAML_RelatedEntity As List(Of GoAMLBLL.WICDataBLL.goAML_Ref_WIC_Related_Entity)
        Public Property ObjList_GoAML_Director As List(Of DataModel.goAML_Ref_Entity_Director)
    End Class

    Public Class General_Reference
        Public Property Kode As String
        Public Property Keterangan As String
    End Class

    'end add
End Class
