﻿'==========================================
'Created Date   : 22 Sep 2021
'Created By     : NawaData
'Description    : Library for goAML Report
'                 
'==========================================

Imports System.Data.SqlClient
Imports NawaDevDAL

Public Class GoAML_Report_BLL

    'Enum Identification Person Type : fungsi utama untuk membedakan terutama Person Identification
    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        'Director = 4        'Untuk level transaksi gak dipakai. Yang dipakai untuk yg 6
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
    End Enum

    'Enum SenderInformation : fungsi utama untuk membedakan terutama untuk Entity Director (antara Account Entity Director dan Entity Director 1 table yang sama)
    Public Enum enumSenderInformation
        Account = 1
        Person = 2
        Entity = 3
    End Enum

    Shared Function getTRNorACT(strReportCode As String) As String
        Try
            Dim strResult As String = "TRN"
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("goAML_odm_ref_Report_TRNorACT", "ReportType", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("TRNorACT")) Then
                    strResult = drTemp("TRNorACT")
                End If
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return "TRN"
        End Try
    End Function

    Shared Function isUseIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                bolResult = True
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function isRequireIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("ReportType")) Then
                    If drTemp("ReportType").ToString.Contains("STR") Then
                        bolResult = True
                    End If
                End If
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function getValidationFromType(transmodeCode As String, instrumenFrom As String, SenderInformation As Integer, myClient As Boolean) As String
        Dim keterangan As String = ""
        Try
            Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
                If SenderInformation = 1 Then ' Account
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 2 Then 'Person
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 3 Then 'Entity
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity NotMyClient"
                        End If
                    End If
                End If
            End Using

            Return keterangan
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getValidationToType(transmodeCode As String, instrumenFrom As String, instrumenTo As String, PenerimaInformation As Integer, TomyClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If PenerimaInformation = 1 Then ' Account
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 2 Then 'Person
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 3 Then 'Entity
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity NotMyClient"
                    End If
                End If

            End If
        End Using

        Return keterangan
    End Function

    Shared Function getValidationReportByReportID(strReportID As String)
        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@reportId"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_ResponseValidateReportByID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function GetGoAMLReportClassByID(ID As Long) As GoAML_Report_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLReportClass = New GoAML_Report_Class
            With objGoAMLReportClass
                'GoAML Report
                .obj_goAML_Report = objDb.goAML_Report.Where(Function(x) x.PK_Report_ID = ID).FirstOrDefault

                'Transaction
                Dim listTransaction = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = ID).ToList
                If listTransaction IsNot Nothing Then
                    .list_goAML_Transaction = listTransaction
                End If

                'Activity
                Dim listActivity = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = ID).ToList
                If listActivity IsNot Nothing Then
                    .list_goAML_Act_ReportPartyType = listActivity
                End If

                'Indicator
                Dim listIndicator = objDb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = ID).ToList
                If listIndicator IsNot Nothing Then
                    .list_goAML_Report_Indicator = listIndicator
                End If
            End With

            Return objGoAMLReportClass
        End Using
    End Function

    Shared Function GetGoAMLTransactionClassByID(ID As Long) As GoAML_Transaction_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLTransactionClass = New GoAML_Transaction_Class
            With objGoAMLTransactionClass
                'GoAML Transaction
                .obj_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = ID).FirstOrDefault

                If .obj_goAML_Transaction IsNot Nothing Then
                    If .obj_goAML_Transaction.FK_Transaction_Type IsNot Nothing Then
                        If .obj_goAML_Transaction.FK_Transaction_Type = 1 Then      'Transaction Bi-Party
                            'Account
                            Dim listAccount = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = ID).ToList
                            If listAccount IsNot Nothing Then .list_goAML_Transaction_Account.AddRange(listAccount)

                            'Signatory
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listSignatory = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = item.PK_Account_ID).ToList
                                    If listSignatory IsNot Nothing Then .list_goAML_Trn_acc_Signatory.AddRange(listSignatory)
                                Next
                            End If

                            'Signatory Address, Phone, Identification
                            If .list_goAML_Trn_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_acc_Signatory.Count > 0 Then
                                For Each item In .list_goAML_Trn_acc_Signatory
                                    Dim listAddress = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_sign_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_acc_sign_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Account Entity
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listAccountEntity = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = item.PK_Account_ID).ToList
                                    If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Entity_account.AddRange(listAccountEntity)
                                Next
                            End If

                            'Account Entity Address, Phone
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listAddress = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList
                                    Dim listPhone = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Account Entity Director
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = item.PK_goAML_Trn_Entity_account And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Account Entity Director Address, Phone, Identification
                            Dim listAccountEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                            If listAccountEntityDirector IsNot Nothing AndAlso listAccountEntityDirector.Count > 0 Then
                                For Each item In listAccountEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Person
                            Dim listPerson = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listPerson IsNot Nothing Then .list_goAML_Transaction_Person.AddRange(listPerson)

                            'Person Address, Phone, Identification
                            If .list_goAML_Transaction_Person IsNot Nothing AndAlso .list_goAML_Transaction_Person.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Person
                                    Dim listAddress = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Person_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Person_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Entity
                            Dim listEntity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listEntity IsNot Nothing Then .list_goAML_Transaction_Entity.AddRange(listEntity)

                            'Entity Address, Phone
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listAddress = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList
                                    Dim listPhone = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Entity Director
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Transaction_ID = ID And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Entity Director Address, Phone, Identification
                            Dim listEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                            If listEntityDirector IsNot Nothing AndAlso listEntityDirector.Count > 0 Then
                                For Each item In listEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Conductor
                            Dim listConductor = objDb.goAML_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listConductor IsNot Nothing Then .list_goAML_Trn_Conductor.AddRange(listConductor)

                            'Conductor Address, Phone, Identification
                            If .list_goAML_Trn_Conductor IsNot Nothing AndAlso .list_goAML_Trn_Conductor.Count > 0 Then
                                For Each item In .list_goAML_Trn_Conductor
                                    Dim listAddress = objDb.goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Conductor).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Conductor_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Conductor_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                        Else    'Transaction Multi Party
                            Dim listParty = objDb.goAML_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listParty IsNot Nothing Then
                                .list_goAML_Transaction_Party.AddRange(listParty)

                                For Each party In listParty
                                    'Account
                                    Dim listAccount = objDb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Signatory
                                    If listAccount IsNot Nothing Then
                                        .list_goAML_Trn_Party_Account.AddRange(listAccount)

                                        For Each objAccount In listAccount
                                            Dim listSignatory = objDb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList

                                            'Signatory Address, Phone, Identification
                                            If listSignatory IsNot Nothing Then
                                                .list_goAML_Trn_par_acc_Signatory.AddRange(listSignatory)

                                                For Each objSignatory In listSignatory
                                                    Dim listAddress = objDb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = objSignatory.PK_Trn_par_acc_Signatory_ID).ToList
                                                    Dim listPhone = objDb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = objSignatory.PK_Trn_par_acc_Signatory_ID).ToList
                                                    Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objSignatory.PK_Trn_par_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                                    If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_sign_Address.AddRange(listAddress)
                                                    If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_sign_Phone.AddRange(listPhone)
                                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                Next
                                            End If

                                            'Account Entity
                                            Dim listAccountEntity = objDb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList

                                            'Account Entity Address, Phone
                                            If listAccountEntity IsNot Nothing Then
                                                .list_goAML_Trn_Par_Acc_Entity.AddRange(listAccountEntity)

                                                For Each objAccountEntity In listAccountEntity
                                                    Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                                                    Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList

                                                    If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                                    If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)

                                                    'Account Entity Director
                                                    Dim listDirector = objDb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList

                                                    'Account Entity Director Address, Phone, Identification
                                                    If listDirector IsNot Nothing Then
                                                        .list_goAML_Trn_Par_Acc_Ent_Director.AddRange(listDirector)

                                                        For Each item In listDirector
                                                            Dim listDirectorAddress = objDb.goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                            Dim listDirectorPhone = objDb.goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                                            If listDirectorAddress IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director_Address.AddRange(listDirectorAddress)
                                                            If listDirectorPhone IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director_Phone.AddRange(listDirectorPhone)
                                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If

                                    'Person
                                    Dim listPerson = objDb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Person Address, Phone, Identification
                                    If listPerson IsNot Nothing Then
                                        .list_goAML_Trn_Party_Person.AddRange(listPerson)

                                        For Each objPerson In listPerson
                                            Dim listAddress = objDb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objPerson.PK_Trn_Party_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Person_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Person_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Entity
                                    Dim listEntity = objDb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList

                                    'Entity Address, Phone, Director
                                    If listEntity IsNot Nothing Then
                                        .list_goAML_Trn_Party_Entity.AddRange(listEntity)

                                        For Each objEntity In listEntity
                                            Dim listAddress = objDb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Entity_Phone.AddRange(listPhone)

                                            'Entity Director
                                            Dim listDirector = objDb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID).ToList
                                            If listDirector IsNot Nothing Then
                                                .list_goAML_Trn_Par_Entity_Director.AddRange(listDirector)

                                                'Entity Director Address, Phone, Identification
                                                For Each objDirector In listDirector
                                                    Dim listDirectorAddress = objDb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                    Dim listDirectorPhone = objDb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                    Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                                    If listDirectorAddress IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Address.AddRange(listDirectorAddress)
                                                    If listDirectorPhone IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Phone.AddRange(listDirectorPhone)
                                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            End With

            Return objGoAMLTransactionClass
        End Using
    End Function

    Shared Function GetGoAMLActivityClassByID(ID As Long) As GoAML_Activity_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLActivityClass = New GoAML_Activity_Class
            With objGoAMLActivityClass
                'GoAML Activity
                .obj_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault

                If .obj_goAML_Act_ReportPartyType IsNot Nothing Then
                    Dim objActivity = .obj_goAML_Act_ReportPartyType

                    'Account
                    Dim listAccount = objDb.goAML_Act_Account.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listAccount IsNot Nothing Then .list_goAML_Act_Account.AddRange(listAccount)

                    'Signatory
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listSignatory = objDb.goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listSignatory IsNot Nothing Then .list_goAML_Act_acc_Signatory.AddRange(listSignatory)
                        Next
                    End If

                    'Signatory Address, Phone, Identification
                    If .list_goAML_Act_acc_Signatory IsNot Nothing AndAlso .list_goAML_Act_acc_Signatory.Count > 0 Then
                        For Each item In .list_goAML_Act_acc_Signatory
                            Dim listAddress = objDb.goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_sign_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_sign_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Account Entity
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listAccountEntity = objDb.goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listAccountEntity IsNot Nothing Then .list_goAML_Act_Entity_Account.AddRange(listAccountEntity)
                        Next
                    End If

                    'Account Entity Address, Phone
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Account Entity Director
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            If listDirector IsNot Nothing Then .list_goAML_Act_Acc_Ent_Director.AddRange(listDirector)
                        Next
                    End If

                    'Account Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Acc_Ent_Director
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_Act_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Person
                    Dim listPerson = objDb.goAML_Act_Person.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listPerson IsNot Nothing Then .list_goAML_Act_Person.AddRange(listPerson)

                    'Person Address, Phone, Identification
                    If .list_goAML_Act_Person IsNot Nothing AndAlso .list_goAML_Act_Person.Count > 0 Then
                        For Each item In .list_goAML_Act_Person
                            Dim listAddress = objDb.goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Person_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Person_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Entity
                    Dim listEntity = objDb.goAML_Act_Entity.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listEntity IsNot Nothing Then .list_goAML_Act_Entity.AddRange(listEntity)

                    'Entity Address, Phone
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listAddress = objDb.goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Entity Director
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listDirector = objDb.goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            .list_goAML_Act_Director.AddRange(listDirector)
                        Next
                    End If

                    'Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Director IsNot Nothing AndAlso .list_goAML_Act_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Director
                            Dim listAddress = objDb.goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Director_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If
                End If
            End With

            Return objGoAMLActivityClass
        End Using
    End Function

    Shared Function GetGoAMLReportIndicatorByID(ID As Long) As goAML_Report_Indicator
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Dim objResult = objDb.goAML_Report_Indicator.Where(Function(x) x.PK_Report_Indicator = ID).FirstOrDefault
            Return objResult
        End Using
    End Function

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As GoAML_Report_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_Report_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listReport As New List(Of goAML_Report)
                Dim listReport_Old As New List(Of goAML_Report)

                listReport.Add(objData.obj_goAML_Report)
                listReport_Old.Add(objData_Old.obj_goAML_Report)
                NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "goAML_Report", listReport, listReport_Old)

            End Using

            Dim strTRN_OR_ACT As String = getTRNorACT(objData.obj_goAML_Report.Report_Code)
            Dim strTRN_OR_ACT_Old As String = getTRNorACT(objData_Old.obj_goAML_Report.Report_Code)
            If strTRN_OR_ACT <> strTRN_OR_ACT_Old Then
                If strTRN_OR_ACT = "TRN" Then
                    DeleteActivityByReportID(objData.obj_goAML_Report.PK_Report_ID)
                ElseIf strTRN_OR_ACT = "ACT" Then
                    DeleteTransactionByReportID(objData.obj_goAML_Report.PK_Report_ID)
                End If
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Transaction_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLTransactionClassByID(objData.obj_goAML_Transaction.PK_Transaction_ID)
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Transaction
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then       'Transaction Bi-Party
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            '1. Account
                            For Each objAccount In objData.list_goAML_Transaction_Account
                                OldPK = objAccount.PK_Account_ID
                                objAccount.FK_Report_ID = strReportID
                                objAccount.FK_Report_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objAccount.PK_Account_ID < 0 Then
                                    objDB.Entry(objAccount).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Account Signatory
                                Dim listSignatory = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = OldPK).ToList
                                If listSignatory IsNot Nothing Then
                                    For Each objSignatory In listSignatory
                                        OldPKSub = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                        objSignatory.FK_Report_ID = strReportID
                                        objSignatory.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objSignatory.FK_Transaction_Account_ID = objAccount.PK_Account_ID

                                        If objSignatory.PK_goAML_Trn_acc_Signatory_ID < 0 Then
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory Address
                                        Dim listAddress = objData.list_goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objAddress.PK_goAML_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Phone
                                        Dim listPhone = objData.list_goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objPhone.PK_goAML_trn_acc_sign_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Signatory Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If

                                'Account Entity
                                Dim listAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = OldPK).ToList
                                If listAccountEntity IsNot Nothing Then
                                    For Each objAccountEntity In listAccountEntity
                                        OldPKSub = objAccountEntity.PK_goAML_Trn_Entity_account
                                        objAccountEntity.FK_Report_ID = strReportID
                                        objAccountEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objAccountEntity.FK_Account_ID = objAccount.PK_Account_ID

                                        If objAccountEntity.PK_goAML_Trn_Entity_account < 0 Then
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Entity Address
                                        Dim listEntityAddress = objData.list_goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityAddress IsNot Nothing Then
                                            For Each objAddress In listEntityAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objAddress.PK_Trn_Acc_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Phone
                                        Dim listEntityPhone = objData.list_goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = OldPKSub).ToList
                                        If listEntityPhone IsNot Nothing Then
                                            For Each objPhone In listEntityPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objPhone.PK_goAML_Trn_Acc_Entity_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Account Entity Director
                                        Dim listAccountEntityDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Account And x.FK_Entity_ID = OldPKSub).ToList
                                        If listAccountEntityDirector IsNot Nothing Then
                                            For Each objDirector In listAccountEntityDirector
                                                OldPKSubSubID = objDirector.PK_goAML_Trn_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                                objDirector.FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account

                                                If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Director Address
                                                Dim listAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Phone
                                                Dim listPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSubSubID).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '2. Person
                            For Each objPerson In objData.list_goAML_Transaction_Person
                                OldPK = objPerson.PK_Person_ID
                                objPerson.FK_Report_ID = strReportID
                                objPerson.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objPerson.PK_Person_ID < 0 Then
                                    objDB.Entry(objPerson).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Person Address
                                Dim listAddress = objData.list_goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objAddress.PK_goAML_Trn_Person_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Phone
                                Dim listPhone = objData.list_goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Person = objPerson.PK_Person_ID

                                        If objPhone.PK_goAML_trn_Person_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Person Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objPerson.PK_Person_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next

                            '3. Entity
                            For Each objEntity In objData.list_goAML_Transaction_Entity
                                OldPK = objEntity.PK_Entity_ID
                                objEntity.FK_Report_ID = strReportID
                                objEntity.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objEntity.PK_Entity_ID < 0 Then
                                    objDB.Entry(objEntity).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Entity Address
                                Dim listAddress = objData.list_goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listAddress IsNot Nothing Then
                                    For Each objAddress In listAddress
                                        objAddress.FK_Report_ID = strReportID
                                        objAddress.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objAddress.PK_goAML_Trn_Entity_Address_ID < 0 Then
                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Phone
                                Dim listPhone = objData.list_goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = OldPK).ToList
                                If listPhone IsNot Nothing Then
                                    For Each objPhone In listPhone
                                        objPhone.FK_Report_ID = strReportID
                                        objPhone.FK_Trn_Entity = objEntity.PK_Entity_ID

                                        If objPhone.PK_goAML_Trn_Entity_Phone < 0 Then
                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If

                                'Entity Director
                                Dim listDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Entity And x.FK_Entity_ID = OldPK).ToList
                                If listDirector IsNot Nothing Then
                                    For Each objDirector In listDirector
                                        OldPKSub = objDirector.PK_goAML_Trn_Director_ID
                                        objDirector.FK_Report_ID = strReportID
                                        objDirector.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID
                                        objDirector.FK_Entity_ID = objEntity.PK_Entity_ID

                                        If objDirector.PK_goAML_Trn_Director_ID < 0 Then
                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'EntityDirector Address
                                        Dim listDirectorAddress = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorAddress IsNot Nothing Then
                                            For Each objAddress In listDirectorAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objAddress.PK_goAML_Trn_Director_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Phone
                                        Dim listDirectorPhone = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = OldPKSub).ToList
                                        If listDirectorPhone IsNot Nothing Then
                                            For Each objPhone In listDirectorPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objPhone.PK_goAML_trn_Director_Phone < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'EntityDirector Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID

                                                If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If
                            Next

                            '4. Conductor
                            For Each objConductor In objData.list_goAML_Trn_Conductor
                                objConductor.FK_REPORT_ID = strReportID
                                objConductor.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objConductor.PK_goAML_Trn_Conductor_ID < 0 Then
                                    objDB.Entry(objConductor).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objConductor).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                'Conductor Address
                                For Each objAddress In objData.list_goAML_Trn_Conductor_Address
                                    objAddress.FK_REPORT_ID = strReportID
                                    objAddress.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objAddress.PK_goAML_Trn_Conductor_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Phone
                                For Each objPhone In objData.list_goAML_trn_Conductor_Phone
                                    objPhone.FK_REPORT_ID = strReportID
                                    objPhone.FK_Trn_Conductor_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                    If objPhone.PK_goAML_trn_Conductor_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next

                                'Conductor Identification
                                Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Conductor).ToList
                                If listIdentification IsNot Nothing Then
                                    For Each objIdentification In listIdentification
                                        objIdentification.FK_Report_ID = strReportID
                                        objIdentification.FK_Person_ID = objConductor.PK_goAML_Trn_Conductor_ID

                                        If objIdentification.PK_goAML_Transaction_Person_Identification_ID < 0 Then
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()
                                    Next
                                End If
                            Next
                        Else    'Transaction Multi-Party
                            Dim OldPKTParty As Long = 0
                            Dim OldPK As Long = 0
                            Dim OldPKSub As Long = 0
                            Dim OldPKSubSubID As Long = 0

                            For Each objTParty In objData.list_goAML_Transaction_Party
                                OldPKTParty = objTParty.PK_Trn_Party_ID
                                objTParty.FK_Report_ID = strReportID
                                objTParty.FK_Transaction_ID = objData.obj_goAML_Transaction.PK_Transaction_ID

                                If objTParty.PK_Trn_Party_ID < 0 Then
                                    objDB.Entry(objTParty).State = Entity.EntityState.Added
                                Else
                                    objDB.Entry(objTParty).State = Entity.EntityState.Modified
                                End If
                                objDB.SaveChanges()

                                '1. Account
                                Dim listAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = OldPKTParty).ToList
                                If listAccount IsNot Nothing Then
                                    For Each objAccount In listAccount
                                        OldPK = objAccount.PK_Trn_Party_Account_ID
                                        objAccount.FK_Report_ID = strReportID
                                        objAccount.FK_Trn_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objAccount.PK_Trn_Party_Account_ID < 0 Then
                                            objDB.Entry(objAccount).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objAccount).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Account Signatory
                                        Dim listSignatory = objData.list_goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listSignatory IsNot Nothing Then
                                            For Each objSignatory In listSignatory
                                                OldPKSub = objSignatory.PK_Trn_par_acc_Signatory_ID
                                                objSignatory.FK_Report_ID = strReportID
                                                objSignatory.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objSignatory.PK_Trn_par_acc_Signatory_ID < 0 Then
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Signatory Address
                                                Dim listAddress = objData.list_goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = OldPKSub).ToList
                                                If listAddress IsNot Nothing Then
                                                    For Each objAddress In listAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Signatory_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objAddress.PK_Trn_Par_Acc_sign_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Phone
                                                Dim listPhone = objData.list_goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = OldPKSub).ToList
                                                If listPhone IsNot Nothing Then
                                                    For Each objPhone In listPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_Par_Acc_Sign = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objPhone.PK_trn_Par_Acc_Sign_Phone < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Signatory Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objSignatory.PK_Trn_par_acc_Signatory_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If

                                        'Account Entity
                                        Dim listAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = OldPK).ToList
                                        If listAccountEntity IsNot Nothing Then
                                            For Each objAccountEntity In listAccountEntity
                                                OldPKSub = objAccountEntity.PK_Trn_Par_acc_Entity_ID
                                                objAccountEntity.FK_Report_ID = strReportID
                                                objAccountEntity.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                                If objAccountEntity.PK_Trn_Par_acc_Entity_ID < 0 Then
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'Account Entity Address
                                                Dim listEntityAddress = objData.list_goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityAddress IsNot Nothing Then
                                                    For Each objAddress In listEntityAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Phone
                                                Dim listEntityPhone = objData.list_goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = OldPKSub).ToList
                                                If listEntityPhone IsNot Nothing Then
                                                    For Each objPhone In listEntityPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objPhone.PK_trn_Par_Acc_Entity_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'Account Entity Director
                                                Dim listAccountEntityDirector = objData.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = OldPKSub).ToList
                                                If listAccountEntityDirector IsNot Nothing Then
                                                    For Each objDirector In listAccountEntityDirector
                                                        OldPKSubSubID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID
                                                        objDirector.FK_Report_ID = strReportID
                                                        objDirector.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                                        If objDirector.PK_Trn_Par_Acc_Ent_Director_ID < 0 Then
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()

                                                        'Account Entity Director Address
                                                        Dim listAddress = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listAddress IsNot Nothing Then
                                                            For Each objAddress In listAddress
                                                                objAddress.FK_Report_ID = strReportID
                                                                objAddress.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objAddress.PK_Trn_Par_Acc_Entity_Address_ID < 0 Then
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Phone
                                                        Dim listPhone = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = OldPKSubSubID).ToList
                                                        If listPhone IsNot Nothing Then
                                                            For Each objPhone In listPhone
                                                                objPhone.FK_Report_ID = strReportID
                                                                objPhone.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objPhone.PK_Trn_Par_Acc_Ent_Director_Phone_ID < 0 Then
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If

                                                        'Account Entity Director Identification
                                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = OldPKSubSubID).ToList
                                                        If listIdentification IsNot Nothing Then
                                                            For Each objIdentification In listIdentification
                                                                objIdentification.FK_Report_ID = strReportID
                                                                objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID

                                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                                Else
                                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                                End If
                                                                objDB.SaveChanges()
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If

                                '2. Person
                                Dim listPerson = objData.list_goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listPerson IsNot Nothing Then
                                    For Each objPerson In listPerson
                                        OldPK = objPerson.PK_Trn_Party_Person_ID
                                        objPerson.FK_Report_ID = strReportID
                                        objPerson.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objPerson.PK_Trn_Party_Person_ID < 0 Then
                                            objDB.Entry(objPerson).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objPerson).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Person Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objAddress.PK_Trn_Party_Person_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objPhone.PK_Trn_Party_Person_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Person Identification
                                        Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Person_ID = OldPK).ToList
                                        If listIdentification IsNot Nothing Then
                                            For Each objIdentification In listIdentification
                                                objIdentification.FK_Report_ID = strReportID
                                                objIdentification.FK_Person_ID = objPerson.PK_Trn_Party_Person_ID

                                                If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If
                                    Next
                                End If

                                '3. Entity
                                Dim listEntity = objData.list_goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = OldPKTParty).ToList
                                If listEntity IsNot Nothing Then
                                    For Each objEntity In listEntity
                                        OldPK = objEntity.PK_Trn_Party_Entity_ID
                                        objEntity.FK_Report_ID = strReportID
                                        objEntity.FK_Transaction_Party_ID = objTParty.PK_Trn_Party_ID

                                        If objEntity.PK_Trn_Party_Entity_ID < 0 Then
                                            objDB.Entry(objEntity).State = Entity.EntityState.Added
                                        Else
                                            objDB.Entry(objEntity).State = Entity.EntityState.Modified
                                        End If
                                        objDB.SaveChanges()

                                        'Entity Address
                                        Dim listAddress = objData.list_goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listAddress IsNot Nothing Then
                                            For Each objAddress In listAddress
                                                objAddress.FK_Report_ID = strReportID
                                                objAddress.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objAddress.PK_Trn_Party_Entity_Address_ID < 0 Then
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Phone
                                        Dim listPhone = objData.list_goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listPhone IsNot Nothing Then
                                            For Each objPhone In listPhone
                                                objPhone.FK_Report_ID = strReportID
                                                objPhone.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objPhone.PK_Trn_Party_Entity_Phone_ID < 0 Then
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()
                                            Next
                                        End If

                                        'Entity Director
                                        Dim listDirector = objData.list_goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = OldPK).ToList
                                        If listDirector IsNot Nothing Then
                                            For Each objDirector In listDirector
                                                OldPKSub = objDirector.PK_Trn_Par_Entity_Director_ID
                                                objDirector.FK_Report_ID = strReportID
                                                objDirector.FK_Trn_Party_Entity_ID = objEntity.PK_Trn_Party_Entity_ID

                                                If objDirector.PK_Trn_Par_Entity_Director_ID < 0 Then
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Added
                                                Else
                                                    objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                                End If
                                                objDB.SaveChanges()

                                                'EntityDirector Address
                                                Dim listDirectorAddress = objData.list_goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorAddress IsNot Nothing Then
                                                    For Each objAddress In listDirectorAddress
                                                        objAddress.FK_Report_ID = strReportID
                                                        objAddress.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objAddress.PK_Trn_Par_Entity_Address_ID < 0 Then
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Phone
                                                Dim listDirectorPhone = objData.list_goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = OldPKSub).ToList
                                                If listDirectorPhone IsNot Nothing Then
                                                    For Each objPhone In listDirectorPhone
                                                        objPhone.FK_Report_ID = strReportID
                                                        objPhone.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objPhone.PK_Trn_Par_Entity_Director_Phone_ID < 0 Then
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If

                                                'EntityDirector Identification
                                                Dim listIdentification = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Person_ID = OldPKSub).ToList
                                                If listIdentification IsNot Nothing Then
                                                    For Each objIdentification In listIdentification
                                                        objIdentification.FK_Report_ID = strReportID
                                                        objIdentification.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID

                                                        If objIdentification.PK_Transaction_Party_Identification_ID < 0 Then
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                        Else
                                                            objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                        End If
                                                        objDB.SaveChanges()
                                                    Next
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            '======================== Bi-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Transaction_Account
                                Dim objCek = objData.list_goAML_Transaction_Account.Find(Function(x) x.PK_Account_ID = item_old.PK_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_acc_Signatory.Find(Function(x) x.PK_goAML_Trn_acc_Signatory_ID = item_old.PK_goAML_Trn_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_sign_Address.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = item_old.PK_goAML_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_acc_sign_Phone.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = item_old.PK_goAML_trn_acc_sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                                Dim objCek = objData.list_goAML_Trn_Entity_account.Find(Function(x) x.PK_goAML_Trn_Entity_account = item_old.PK_goAML_Trn_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Acc_Entity_Address_ID = item_old.PK_Trn_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone = item_old.PK_goAML_Trn_Acc_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Transaction_Person
                                Dim objCek = objData.list_goAML_Transaction_Person.Find(Function(x) x.PK_Person_ID = item_old.PK_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Person_Address.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = item_old.PK_goAML_Trn_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                                Dim objCek = objData.list_goAML_trn_Person_Phone.Find(Function(x) x.PK_goAML_trn_Person_Phone = item_old.PK_goAML_trn_Person_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Transaction_Entity
                                Dim objCek = objData.list_goAML_Transaction_Entity.Find(Function(x) x.PK_Entity_ID = item_old.PK_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Entity_Address.Find(Function(x) x.PK_goAML_Trn_Entity_Address_ID = item_old.PK_goAML_Trn_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Entity_Phone = item_old.PK_goAML_Trn_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Director
                                Dim objCek = objData.list_goAML_Trn_Director.Find(Function(x) x.PK_goAML_Trn_Director_ID = item_old.PK_goAML_Trn_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Director_Address.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = item_old.PK_goAML_Trn_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account/Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                                Dim objCek = objData.list_goAML_trn_Director_Phone.Find(Function(x) x.PK_goAML_trn_Director_Phone = item_old.PK_goAML_trn_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '4. Conductor
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor
                                Dim objCek = objData.list_goAML_Trn_Conductor.Find(Function(x) x.PK_goAML_Trn_Conductor_ID = item_old.PK_goAML_Trn_Conductor_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Address
                            For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                                Dim objCek = objData.list_goAML_Trn_Conductor_Address.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = item_old.PK_goAML_Trn_Conductor_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Phone
                            For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                                Dim objCek = objData.list_goAML_trn_Conductor_Phone.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = item_old.PK_goAML_trn_Conductor_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Conductor Identification
                            For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                                Dim objCek = objData.list_goAML_Transaction_Person_Identification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = item_old.PK_goAML_Transaction_Person_Identification_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '============================ MULTI-PARTY
                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                                Dim objCek = objData.list_goAML_Trn_Party_Account.Find(Function(x) x.PK_Trn_Party_Account_ID = item_old.PK_Trn_Party_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                                Dim objCek = objData.list_goAML_Trn_par_acc_Signatory.Find(Function(x) x.PK_Trn_par_acc_Signatory_ID = item_old.PK_Trn_par_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_sign_Address.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = item_old.PK_Trn_Par_Acc_sign_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_sign_Phone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = item_old.PK_trn_Par_Acc_Sign_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Entity.Find(Function(x) x.PK_Trn_Par_acc_Entity_ID = item_old.PK_Trn_Par_acc_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_par_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                                Dim objCek = objData.list_goAML_trn_par_acc_Entity_Phone.Find(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID = item_old.PK_trn_Par_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID = item_old.PK_Trn_Par_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = item_old.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                                Dim objCek = objData.list_goAML_Trn_Party_Person.Find(Function(x) x.PK_Trn_Party_Person_ID = item_old.PK_Trn_Party_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Address.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = item_old.PK_Trn_Party_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Person_Phone.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = item_old.PK_Trn_Party_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                                Dim objCek = objData.list_goAML_Trn_Party_Entity.Find(Function(x) x.PK_Trn_Party_Entity_ID = item_old.PK_Trn_Party_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Address.Find(Function(x) x.PK_Trn_Party_Entity_Address_ID = item_old.PK_Trn_Party_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                                Dim objCek = objData.list_goAML_Trn_Party_Entity_Phone.Find(Function(x) x.PK_Trn_Party_Entity_Phone_ID = item_old.PK_Trn_Party_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director.Find(Function(x) x.PK_Trn_Par_Entity_Director_ID = item_old.PK_Trn_Par_Entity_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Address.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = item_old.PK_Trn_Par_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Phone.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = item_old.PK_Trn_Par_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveTransaction_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)

            End Using

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction_AuditTrail(objData As GoAML_Transaction_Class, objData_Old As GoAML_Transaction_Class, objDB As NawaDatadevEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            'If intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert Then
            '    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Transaction)
            'ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
            '    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Transaction, objData_Old.obj_goAML_Transaction)
            'End If

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listTransaction As New List(Of goAML_Transaction)
            Dim listTransaction_Old As New List(Of goAML_Transaction)

            listTransaction.Add(objData.obj_goAML_Transaction)
            listTransaction_Old.Add(objData_Old.obj_goAML_Transaction)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "goAML_Transaction", listTransaction, listTransaction_Old)


            If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Account", objData.list_goAML_Transaction_Account, objData_Old.list_goAML_Transaction_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_acc_Signatory", objData.list_goAML_Trn_acc_Signatory, objData_Old.list_goAML_Trn_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_sign_Address", objData.list_goAML_Trn_Acc_sign_Address, objData_Old.list_goAML_Trn_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_acc_sign_Phone", objData.list_goAML_trn_acc_sign_Phone, objData_Old.list_goAML_trn_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_account", objData.list_goAML_Trn_Entity_account, objData_Old.list_goAML_Trn_Entity_account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Address", objData.list_goAML_Trn_Acc_Entity_Address, objData_Old.list_goAML_Trn_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Phone", objData.list_goAML_Trn_Acc_Entity_Phone, objData_Old.list_goAML_Trn_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person", objData.list_goAML_Transaction_Person, objData_Old.list_goAML_Transaction_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Person_Address", objData.list_goAML_Trn_Person_Address, objData_Old.list_goAML_Trn_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Person_Phone", objData.list_goAML_trn_Person_Phone, objData_Old.list_goAML_trn_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Entity", objData.list_goAML_Transaction_Entity, objData_Old.list_goAML_Transaction_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Address", objData.list_goAML_Trn_Entity_Address, objData_Old.list_goAML_Trn_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Phone", objData.list_goAML_Trn_Entity_Phone, objData_Old.list_goAML_Trn_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director", objData.list_goAML_Trn_Director, objData_Old.list_goAML_Trn_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director_Address", objData.list_goAML_Trn_Director_Address, objData_Old.list_goAML_Trn_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Director_Phone", objData.list_goAML_trn_Director_Phone, objData_Old.list_goAML_trn_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor", objData.list_goAML_Trn_Conductor, objData_Old.list_goAML_Trn_Conductor)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor_Address", objData.list_goAML_Trn_Conductor_Address, objData_Old.list_goAML_Trn_Conductor_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Conductor_Phone", objData.list_goAML_trn_Conductor_Phone, objData_Old.list_goAML_trn_Conductor_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person_Identification", objData.list_goAML_Transaction_Person_Identification, objData_Old.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party", objData.list_goAML_Transaction_Party, objData_Old.list_goAML_Transaction_Party)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Account", objData.list_goAML_Trn_Party_Account, objData_Old.list_goAML_Trn_Party_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_acc_Signatory", objData.list_goAML_Trn_par_acc_Signatory, objData_Old.list_goAML_Trn_par_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_sign_Address", objData.list_goAML_Trn_par_Acc_sign_Address, objData_Old.list_goAML_Trn_par_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_sign_Phone", objData.list_goAML_trn_par_acc_sign_Phone, objData_Old.list_goAML_trn_par_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Entity", objData.list_goAML_Trn_Par_Acc_Entity, objData_Old.list_goAML_Trn_Par_Acc_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_Entity_Address", objData.list_goAML_Trn_par_Acc_Entity_Address, objData_Old.list_goAML_Trn_par_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_Entity_Phone", objData.list_goAML_trn_par_acc_Entity_Phone, objData_Old.list_goAML_trn_par_acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director", objData.list_goAML_Trn_Par_Acc_Ent_Director, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Address", objData.list_goAML_Trn_Par_Acc_Ent_Director_Address, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Phone", objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person", objData.list_goAML_Trn_Party_Person, objData_Old.list_goAML_Trn_Party_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Address", objData.list_goAML_Trn_Party_Person_Address, objData_Old.list_goAML_Trn_Party_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Phone", objData.list_goAML_Trn_Party_Person_Phone, objData_Old.list_goAML_Trn_Party_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity", objData.list_goAML_Trn_Party_Entity, objData_Old.list_goAML_Trn_Party_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Address", objData.list_goAML_Trn_Party_Entity_Address, objData_Old.list_goAML_Trn_Party_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Phone", objData.list_goAML_Trn_Party_Entity_Phone, objData_Old.list_goAML_Trn_Party_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director", objData.list_goAML_Trn_Par_Entity_Director, objData_Old.list_goAML_Trn_Par_Entity_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Address", objData.list_goAML_Trn_Par_Entity_Director_Address, objData_Old.list_goAML_Trn_Par_Entity_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Phone", objData.list_goAML_Trn_Par_Entity_Director_Phone, objData_Old.list_goAML_Trn_Par_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party_Identification", objData.list_goAML_Transaction_Party_Identification, objData_Old.list_goAML_Transaction_Party_Identification)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByTransactionID(strTransactionID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Transaction")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetGoAMLTransactionClassByID(strTransactionID)
            If objData_Old Is Nothing Then
                objData_Old = New GoAML_Transaction_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction started (Transaction ID = " & strTransactionID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Transaction
                        objDB.Entry(objData_Old.obj_goAML_Transaction).State = Entity.EntityState.Deleted

                        '======================== Bi-PARTY
                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Transaction_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Transaction_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Transaction_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account/Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '4. Conductor
                        For Each item_old In objData_Old.list_goAML_Trn_Conductor
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Address
                        For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Phone
                        For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Conductor Identification
                        For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '============================ MULTI-PARTY
                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction finished (Transaction ID = " & strTransactionID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Save Audit Trail
                DeleteTransactionByTransactionID_AuditTrail(objData_Old, objDB, objModule)

            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByTransactionID_AuditTrail(objData_Old As GoAML_Transaction_Class, objDB As NawaDatadevEntities, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.obj_goAML_Transaction)

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listTransactionToDelete As New List(Of goAML_Transaction)
            listTransactionToDelete.Add(objData_Old.obj_goAML_Transaction)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction", Nothing, listTransactionToDelete)

            If objData_Old.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Account", Nothing, objData_Old.list_goAML_Transaction_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_acc_Signatory", Nothing, objData_Old.list_goAML_Trn_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_sign_Address", Nothing, objData_Old.list_goAML_Trn_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_acc_sign_Phone", Nothing, objData_Old.list_goAML_trn_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_account", Nothing, objData_Old.list_goAML_Trn_Entity_account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Address", Nothing, objData_Old.list_goAML_Trn_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Acc_Entity_Phone", Nothing, objData_Old.list_goAML_Trn_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person", Nothing, objData_Old.list_goAML_Transaction_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Person_Address", Nothing, objData_Old.list_goAML_Trn_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Person_Phone", Nothing, objData_Old.list_goAML_trn_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Entity", Nothing, objData_Old.list_goAML_Transaction_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Address", Nothing, objData_Old.list_goAML_Trn_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Entity_Phone", Nothing, objData_Old.list_goAML_Trn_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director", Nothing, objData_Old.list_goAML_Trn_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Director_Address", Nothing, objData_Old.list_goAML_Trn_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Director_Phone", Nothing, objData_Old.list_goAML_trn_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor", Nothing, objData_Old.list_goAML_Trn_Conductor)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Conductor_Address", Nothing, objData_Old.list_goAML_Trn_Conductor_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_Conductor_Phone", Nothing, objData_Old.list_goAML_trn_Conductor_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Person_Identification", Nothing, objData_Old.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party", Nothing, objData_Old.list_goAML_Transaction_Party)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Account", Nothing, objData_Old.list_goAML_Trn_Party_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_acc_Signatory", Nothing, objData_Old.list_goAML_Trn_par_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_sign_Address", Nothing, objData_Old.list_goAML_Trn_par_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_sign_Phone", Nothing, objData_Old.list_goAML_trn_par_acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Entity", Nothing, objData_Old.list_goAML_Trn_Par_Acc_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_par_Acc_Entity_Address", Nothing, objData_Old.list_goAML_Trn_par_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_trn_par_acc_Entity_Phone", Nothing, objData_Old.list_goAML_trn_par_acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director", Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Address", Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Acc_Ent_Director_Phone", Nothing, objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person", Nothing, objData_Old.list_goAML_Trn_Party_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Address", Nothing, objData_Old.list_goAML_Trn_Party_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Person_Phone", Nothing, objData_Old.list_goAML_Trn_Party_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity", Nothing, objData_Old.list_goAML_Trn_Party_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Address", Nothing, objData_Old.list_goAML_Trn_Party_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Party_Entity_Phone", Nothing, objData_Old.list_goAML_Trn_Party_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director", Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Address", Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Trn_Par_Entity_Director_Phone", Nothing, objData_Old.list_goAML_Trn_Par_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Transaction_Party_Identification", Nothing, objData_Old.list_goAML_Transaction_Party_Identification)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteTransactionByReportID(strReportID As String)
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_DeleteTransaction_ByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Transaction finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity(objModule As NawaDAL.Module, objData As GoAML_Activity_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New GoAML_Activity_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLActivityClassByID(objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID)
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Act_ReportPartyType
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        '===================== NEW ADDED/EDITED DATA
                        'Insert ke EODLogSP untuk ukur performance
                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data started')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Dim OldPK As Long = 0
                        Dim OldPKSub As Long = 0
                        Dim OldPKSubSubID As Long = 0

                        '1. Account
                        For Each objAccount In objData.list_goAML_Act_Account
                            OldPK = objAccount.PK_goAML_Act_Account_ID
                            objAccount.FK_Report_ID = strReportID
                            objAccount.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objAccount.PK_goAML_Act_Account_ID < 0 Then
                                objDB.Entry(objAccount).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objAccount).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Account Signatory
                            Dim listSignatory = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = OldPK).ToList
                            If listSignatory IsNot Nothing Then
                                For Each objSignatory In listSignatory
                                    OldPKSub = objSignatory.PK_goAML_Act_acc_Signatory_ID
                                    objSignatory.FK_Report_ID = strReportID
                                    objSignatory.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objSignatory.FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objSignatory.PK_goAML_Act_acc_Signatory_ID < 0 Then
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objSignatory).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Signatory Address
                                    Dim listAddress = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = OldPKSub).ToList
                                    If listAddress IsNot Nothing Then
                                        For Each objAddress In listAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objAddress.PK_goAML_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Phone
                                    Dim listPhone = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listPhone IsNot Nothing Then
                                        For Each objPhone In listPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objPhone.PK_goAML_act_acc_sign_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Signatory Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objSignatory.PK_goAML_Act_acc_Signatory_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If

                            'Account Entity
                            Dim listAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = OldPK).ToList
                            If listAccountEntity IsNot Nothing Then
                                For Each objAccountEntity In listAccountEntity
                                    OldPKSub = objAccountEntity.PK_goAML_Act_Entity_account
                                    objAccountEntity.FK_Report_ID = strReportID
                                    objAccountEntity.FK_Activity_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objAccountEntity.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                    If objAccountEntity.PK_goAML_Act_Entity_account < 0 Then
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAccountEntity).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'Account Entity Address
                                    Dim listEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityAddress IsNot Nothing Then
                                        For Each objAddress In listEntityAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objAddress.PK_Act_Acc_Entity_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Phone
                                    Dim listEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listEntityPhone IsNot Nothing Then
                                        For Each objPhone In listEntityPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objPhone.PK_goAML_Act_Acc_Entity_Phone_ID < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'Account Entity Director
                                    Dim listAccountEntityDirector = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = OldPKSub).ToList
                                    If listAccountEntityDirector IsNot Nothing Then
                                        For Each objDirector In listAccountEntityDirector
                                            OldPKSubSubID = objDirector.PK_Act_Acc_Ent_Director_ID
                                            objDirector.FK_Report_ID = strReportID
                                            objDirector.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                            If objDirector.PK_Act_Acc_Ent_Director_ID < 0 Then
                                                objDB.Entry(objDirector).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()

                                            'Account Entity Director Address
                                            Dim listAddress = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listAddress IsNot Nothing Then
                                                For Each objAddress In listAddress
                                                    objAddress.FK_Report_ID = strReportID
                                                    objAddress.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objAddress.PK_goAML_Act_Acc_Entity_Director_address_ID < 0 Then
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Phone
                                            Dim listPhone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = OldPKSubSubID).ToList
                                            If listPhone IsNot Nothing Then
                                                For Each objPhone In listPhone
                                                    objPhone.FK_Report_ID = strReportID
                                                    objPhone.FK_Act_Entity_Director_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objPhone.PK_goAML_Act_Acc_Entity_Director_Phone_ID < 0 Then
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If

                                            'Account Entity Director Identification
                                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = OldPKSubSubID).ToList
                                            If listIdentification IsNot Nothing Then
                                                For Each objIdentification In listIdentification
                                                    objIdentification.FK_Report_ID = strReportID
                                                    objIdentification.FK_Act_Person_ID = objDirector.PK_Act_Acc_Ent_Director_ID

                                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                                    Else
                                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                                    End If
                                                    objDB.SaveChanges()
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        '2. Person
                        For Each objPerson In objData.list_goAML_Act_Person
                            OldPK = objPerson.PK_goAML_Act_Person_ID
                            objPerson.FK_Report_ID = strReportID
                            objPerson.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objPerson.PK_goAML_Act_Person_ID < 0 Then
                                objDB.Entry(objPerson).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objPerson).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Person Address
                            Dim listAddress = objData.list_goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.FK_Report_ID = strReportID
                                    objAddress.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objAddress.PK_goAML_Act_Person_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Phone
                            Dim listPhone = objData.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Person = objPerson.PK_goAML_Act_Person_ID

                                    If objPhone.PK_goAML_Act_Person_Phone_ID < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Person Identification
                            Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person And x.FK_Act_Person_ID = OldPK).ToList
                            If listIdentification IsNot Nothing Then
                                For Each objIdentification In listIdentification
                                    objIdentification.FK_Report_ID = strReportID
                                    objIdentification.FK_Act_Person_ID = objPerson.PK_goAML_Act_Person_ID

                                    If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If
                        Next

                        '3. Entity
                        For Each objEntity In objData.list_goAML_Act_Entity
                            OldPK = objEntity.PK_goAML_Act_Entity_ID
                            objEntity.FK_Report_ID = strReportID
                            objEntity.FK_Act_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID

                            If objEntity.PK_goAML_Act_Entity_ID < 0 Then
                                objDB.Entry(objEntity).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objEntity).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            'Entity Address
                            Dim listAddress = objData.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listAddress IsNot Nothing Then
                                For Each objAddress In listAddress
                                    objAddress.FK_Report_ID = strReportID
                                    objAddress.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objAddress.PK_goAML_Act_Entity_Address_ID < 0 Then
                                        objDB.Entry(objAddress).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Phone
                            Dim listPhone = objData.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listPhone IsNot Nothing Then
                                For Each objPhone In listPhone
                                    objPhone.FK_Report_ID = strReportID
                                    objPhone.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objPhone.PK_goAML_Act_Entity_Phone < 0 Then
                                        objDB.Entry(objPhone).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()
                                Next
                            End If

                            'Entity Director
                            Dim listDirector = objData.list_goAML_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = OldPK).ToList
                            If listDirector IsNot Nothing Then
                                For Each objDirector In listDirector
                                    OldPKSub = objDirector.PK_goAML_Act_Director_ID
                                    objDirector.FK_Report_ID = strReportID
                                    objDirector.FK_ReportParty_ID = objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID
                                    objDirector.FK_Sender_Information = enumSenderInformation.Entity
                                    objDirector.FK_Act_Entity_ID = objEntity.PK_goAML_Act_Entity_ID

                                    If objDirector.PK_goAML_Act_Director_ID < 0 Then
                                        objDB.Entry(objDirector).State = Entity.EntityState.Added
                                    Else
                                        objDB.Entry(objDirector).State = Entity.EntityState.Modified
                                    End If
                                    objDB.SaveChanges()

                                    'EntityDirector Address
                                    Dim listDirectorAddress = objData.list_goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorAddress IsNot Nothing Then
                                        For Each objAddress In listDirectorAddress
                                            objAddress.FK_Report_ID = strReportID
                                            objAddress.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objAddress.PK_goAML_Act_Director_Address_ID < 0 Then
                                                objDB.Entry(objAddress).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objAddress).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Phone
                                    Dim listDirectorPhone = objData.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = OldPKSub).ToList
                                    If listDirectorPhone IsNot Nothing Then
                                        For Each objPhone In listDirectorPhone
                                            objPhone.FK_Report_ID = strReportID
                                            objPhone.FK_Act_Director_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objPhone.PK_goAML_Act_Director_Phone < 0 Then
                                                objDB.Entry(objPhone).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objPhone).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If

                                    'EntityDirector Identification
                                    Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity And x.FK_Act_Person_ID = OldPKSub).ToList
                                    If listIdentification IsNot Nothing Then
                                        For Each objIdentification In listIdentification
                                            objIdentification.FK_Report_ID = strReportID
                                            objIdentification.FK_Act_Person_ID = objDirector.PK_goAML_Act_Director_ID

                                            If objIdentification.PK_goAML_Activity_Person_Identification_ID < 0 Then
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Added
                                            Else
                                                objDB.Entry(objIdentification).State = Entity.EntityState.Modified
                                            End If
                                            objDB.SaveChanges()
                                        Next
                                    End If
                                Next
                            End If
                        Next

                        strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Add/Edit Data finished')"
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '===================== END OF NEW ADDED/EDITED DATA

                        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                        If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data started')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            '1. Account
                            For Each item_old In objData_Old.list_goAML_Act_Account
                                Dim objCek = objData.list_goAML_Act_Account.Find(Function(x) x.PK_goAML_Act_Account_ID = item_old.PK_goAML_Act_Account_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory
                            For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                                Dim objCek = objData.list_goAML_Act_acc_Signatory.Find(Function(x) x.PK_goAML_Act_acc_Signatory_ID = item_old.PK_goAML_Act_acc_Signatory_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Address_ID = item_old.PK_goAML_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Signatory Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_sign_Phone.Find(Function(x) x.PK_goAML_act_acc_sign_Phone_ID = item_old.PK_goAML_act_acc_sign_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                                Dim objCek = objData.list_goAML_Act_Entity_Account.Find(Function(x) x.PK_goAML_Act_Entity_account = item_old.PK_goAML_Act_Entity_account)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Address.Find(Function(x) x.PK_Act_Acc_Entity_Address_ID = item_old.PK_Act_Acc_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                                Dim objCek = objData.list_goAML_Act_Acc_Ent_Director.Find(Function(x) x.PK_Act_Acc_Ent_Director_ID = item_old.PK_Act_Acc_Ent_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_address.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID = item_old.PK_goAML_Act_Acc_Entity_Director_address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Account Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Acc_Entity_Director_Phone.Find(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID = item_old.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '2. Person
                            For Each item_old In objData_Old.list_goAML_Act_Person
                                Dim objCek = objData.list_goAML_Act_Person.Find(Function(x) x.PK_goAML_Act_Person_ID = item_old.PK_goAML_Act_Person_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Address
                            For Each item_old In objData_Old.list_goAML_Act_Person_Address
                                Dim objCek = objData.list_goAML_Act_Person_Address.Find(Function(x) x.PK_goAML_Act_Person_Address_ID = item_old.PK_goAML_Act_Person_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Person Phone
                            For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                                Dim objCek = objData.list_goAML_Act_Person_Phone.Find(Function(x) x.PK_goAML_Act_Person_Phone_ID = item_old.PK_goAML_Act_Person_Phone_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            '3. Entity
                            For Each item_old In objData_Old.list_goAML_Act_Entity
                                Dim objCek = objData.list_goAML_Act_Entity.Find(Function(x) x.PK_goAML_Act_Entity_ID = item_old.PK_goAML_Act_Entity_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Address
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                                Dim objCek = objData.list_goAML_Act_Entity_Address.Find(Function(x) x.PK_goAML_Act_Entity_Address_ID = item_old.PK_goAML_Act_Entity_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Phone
                            For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                                Dim objCek = objData.list_goAML_Act_Entity_Phone.Find(Function(x) x.PK_goAML_Act_Entity_Phone = item_old.PK_goAML_Act_Entity_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director
                            For Each item_old In objData_Old.list_goAML_Act_Director
                                Dim objCek = objData.list_goAML_Act_Director.Find(Function(x) x.PK_goAML_Act_Director_ID = item_old.PK_goAML_Act_Director_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Address
                            For Each item_old In objData_Old.list_goAML_Act_Director_Address
                                Dim objCek = objData.list_goAML_Act_Director_Address.Find(Function(x) x.PK_goAML_Act_Director_Address_ID = item_old.PK_goAML_Act_Director_Address_ID)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            'Entity Director Phone
                            For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                                Dim objCek = objData.list_goAML_Act_Director_Phone.Find(Function(x) x.PK_goAML_Act_Director_Phone = item_old.PK_goAML_Act_Director_Phone)
                                If objCek Is Nothing Then
                                    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                                End If
                            Next

                            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Deleted Data finished')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '===================== END OF _DeleteD DATA
                        End If

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                SaveActivity_AuditTrail(objData, objData_Old, objDB, objModule, intModuleAction)
            End Using

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_ValidasiSatuan", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity_AuditTrail(objData As GoAML_Activity_Class, objData_Old As GoAML_Activity_Class, objDB As NawaDatadevEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            'If intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert Then
            '    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Act_ReportPartyType)
            'ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
            '    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Act_ReportPartyType, objData_Old.obj_goAML_Act_ReportPartyType)
            'End If

            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

            Dim listActivity As New List(Of goAML_Act_ReportPartyType)
            Dim listActivity_Old As New List(Of goAML_Act_ReportPartyType)

            listActivity.Add(objData.obj_goAML_Act_ReportPartyType)
            listActivity_Old.Add(objData_Old.obj_goAML_Act_ReportPartyType)
            NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "goAML_Act_ReportPartyType", listActivity, listActivity_Old)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Account", objData.list_goAML_Act_Account, objData_Old.list_goAML_Act_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_acc_Signatory", objData.list_goAML_Act_acc_Signatory, objData_Old.list_goAML_Act_acc_Signatory)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Address", objData.list_goAML_Act_Acc_sign_Address, objData_Old.list_goAML_Act_Acc_sign_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Phone", objData.list_goAML_Act_Acc_sign_Phone, objData_Old.list_goAML_Act_Acc_sign_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Account", objData.list_goAML_Act_Entity_Account, objData_Old.list_goAML_Act_Entity_Account)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Address", objData.list_goAML_Act_Acc_Entity_Address, objData_Old.list_goAML_Act_Acc_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Phone", objData.list_goAML_Act_Acc_Entity_Phone, objData_Old.list_goAML_Act_Acc_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Ent_Director", objData.list_goAML_Act_Acc_Ent_Director, objData_Old.list_goAML_Act_Acc_Ent_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_address", objData.list_goAML_Act_Acc_Entity_Director_address, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_Phone", objData.list_goAML_Act_Acc_Entity_Director_Phone, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person", objData.list_goAML_Act_Person, objData_Old.list_goAML_Act_Person)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Address", objData.list_goAML_Act_Person_Address, objData_Old.list_goAML_Act_Person_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Phone", objData.list_goAML_Act_Person_Phone, objData_Old.list_goAML_Act_Person_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity", objData.list_goAML_Act_Entity, objData_Old.list_goAML_Act_Entity)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Address", objData.list_goAML_Act_Entity_Address, objData_Old.list_goAML_Act_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Phone", objData.list_goAML_Act_Entity_Phone, objData_Old.list_goAML_Act_Entity_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director", objData.list_goAML_Act_Director, objData_Old.list_goAML_Act_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Address", objData.list_goAML_Act_Director_Address, objData_Old.list_goAML_Act_Director_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Phone", objData.list_goAML_Act_Director_Phone, objData_Old.list_goAML_Act_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Activity_Person_Identification", objData.list_goAML_Activity_Person_Identification, objData_Old.list_goAML_Activity_Person_Identification)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID(strActivityID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Act_ReportPartyType")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetGoAMLActivityClassByID(strActivityID)
            If objData_Old Is Nothing Then
                objData_Old = New GoAML_Activity_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity started (Activity ID = " & strActivityID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Activity
                        objDB.Entry(objData_Old.obj_goAML_Act_ReportPartyType).State = Entity.EntityState.Deleted

                        '1. Account
                        For Each item_old In objData_Old.list_goAML_Act_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory
                        For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Signatory Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Account Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '2. Person
                        For Each item_old In objData_Old.list_goAML_Act_Person
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Act_Person_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        '3. Entity
                        For Each item_old In objData_Old.list_goAML_Act_Entity
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director
                        For Each item_old In objData_Old.list_goAML_Act_Director
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Address
                        For Each item_old In objData_Old.list_goAML_Act_Director_Address
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        'Entity Director Phone
                        For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        Next

                        objDB.SaveChanges()
                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity finished (Activity ID = " & strActivityID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End Using

            'Save Audit Trail
            DeleteActivityByActivityID_AuditTrail(objData_Old, objModule)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID_AuditTrail(objData_Old As GoAML_Activity_Class, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New NawaDatadevEntities
                Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.obj_goAML_Act_ReportPartyType)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = objAuditTrailheader.PK_AuditTrail_ID

                Dim listActivityToDelete As New List(Of goAML_Act_ReportPartyType)
                listActivityToDelete.Add(objData_Old.obj_goAML_Act_ReportPartyType)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_ReportPartyType", Nothing, listActivityToDelete)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Account", Nothing, objData_Old.list_goAML_Act_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_acc_Signatory", Nothing, objData_Old.list_goAML_Act_acc_Signatory)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Address", Nothing, objData_Old.list_goAML_Act_Acc_sign_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_sign_Phone", Nothing, objData_Old.list_goAML_Act_Acc_sign_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Account", Nothing, objData_Old.list_goAML_Act_Entity_Account)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Address", Nothing, objData_Old.list_goAML_Act_Acc_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Phone", Nothing, objData_Old.list_goAML_Act_Acc_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Ent_Director", Nothing, objData_Old.list_goAML_Act_Acc_Ent_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_address", Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Acc_Entity_Director_Phone", Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person", Nothing, objData_Old.list_goAML_Act_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Address", Nothing, objData_Old.list_goAML_Act_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Person_Phone", Nothing, objData_Old.list_goAML_Act_Person_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity", Nothing, objData_Old.list_goAML_Act_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Address", Nothing, objData_Old.list_goAML_Act_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Entity_Phone", Nothing, objData_Old.list_goAML_Act_Entity_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director", Nothing, objData_Old.list_goAML_Act_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Address", Nothing, objData_Old.list_goAML_Act_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Act_Director_Phone", Nothing, objData_Old.list_goAML_Act_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, "goAML_Activity_Person_Identification", Nothing, objData_Old.list_goAML_Activity_Person_Identification)
            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByReportID(strReportID As String)
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_DeleteActivity_ByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Activity', 'Delete Activity finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveIndicator(objModule As NawaDAL.Module, objData As goAML_Report_Indicator, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.PK_Report_Indicator < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New goAML_Report_Indicator
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLReportIndicatorByID(objData.PK_Report_Indicator)
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData.PK_Report_Indicator < 0 Then
                            objDB.Entry(objData).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(objData).State = Entity.EntityState.Modified
                        End If
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    'If intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert Then
                    '    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData)
                    'ElseIf intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                    '    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData, objData_Old)
                    'End If
                    Dim listIndicator As New List(Of goAML_Report_Indicator)
                    Dim listIndicator_Old As New List(Of goAML_Report_Indicator)

                    listIndicator.Add(objData)
                    listIndicator_Old.Add(objData_Old)

                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "goAML_Report_Indicator", listIndicator, listIndicator_Old)

                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteIndicatorByIndicatorID(strIndicatorID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("goAML_Report_Indicator")
            Dim strModuleName As String = objModule.ModuleLabel

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = New goAML_Report_Indicator
            objData_Old = GetGoAMLReportIndicatorByID(strIndicatorID)

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Data
                        objDB.Entry(objData_Old).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old)

                    Dim listIndicatorToDelete As New List(Of goAML_Report_Indicator)
                    listIndicatorToDelete.Add(objData_Old)
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "goAML_Transaction", Nothing, listIndicatorToDelete)

                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Import Data from GoAML Ref"
    Shared Function GetBiPartyAccountSignatoryClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Transaction_Account.Where(Function(x) x.PK_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = objAccount.PK_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Trn_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Trn_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_trn_acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = objSignatory_Old.PK_goAML_Trn_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Transaction_Person_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Trn_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Trn_acc_Signatory.Min(Function(x) x.PK_goAML_Trn_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Trn_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Trn_Acc_sign_Address.Min(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_trn_acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_trn_acc_sign_Phone.Min(Function(x) x.PK_goAML_trn_acc_sign_Phone)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Transaction_Person_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Transaction_Person_Identification.Min(Function(x) x.PK_goAML_Transaction_Person_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Trn_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Trn_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                        .FK_From_Or_To = objAccount.FK_From_Or_To

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Trn_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_trn_acc_sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Transaction_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Transaction_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .from_or_To_Type = objAccount.FK_From_Or_To

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Trn_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Trn_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                        .FK_From_Or_To = objAccount.FK_From_Or_To

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Trn_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_trn_acc_sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Acc_Entity = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Transaction_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Transaction_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_goAML_Trn_acc_Signatory_ID
                                                    .from_or_To_Type = objAccount.FK_From_Or_To

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetBiPartyAccountEntityClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Transaction_Account.Where(Function(x) x.PK_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = objAccount.PK_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Trn_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_Trn_Acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Account And x.FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Trn_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Trn_Director_Address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_trn_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector_Old.PK_goAML_Trn_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Transaction_Person_Identification.Remove(objIdentification_Old)
                                Next
                            End If
                        Next
                    End If
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = objAccount.PK_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0

                        'PK Entity Address
                        If objData.list_goAML_Trn_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_Acc_Entity_Address.Min(Function(x) x.PK_Trn_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_Trn_Acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Trn_Acc_Entity_Phone.Min(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director
                        If objData.list_goAML_Trn_Director.Count > 0 Then
                            lngPK_Director = objData.list_goAML_Trn_Director.Min(Function(x) x.PK_goAML_Trn_Director_ID)
                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Trn_Director_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_Director_Address.Min(Function(x) x.PK_goAML_Trn_Director_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_trn_Director_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_trn_Director_Phone.Min(Function(x) x.PK_goAML_trn_Director_Phone)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Transaction_Person_Identification.Count > 0 Then
                            lngPK_Director_Identification = objData.list_goAML_Transaction_Person_Identification.Min(Function(x) x.PK_goAML_Transaction_Person_Identification_ID)
                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Trn_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Trn_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_Trn_Acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_goAML_Trn_Acc_Entity_Phone = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Acc_Entity = objAccountEntity.PK_goAML_Trn_Entity_account

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_Acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Trn_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_goAML_Trn_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Transaction_ID = objAccount.FK_Report_Transaction_ID
                                    .FK_From_Or_To = objAccount.FK_From_Or_To
                                    .FK_Sender_Information = enumSenderInformation.Account
                                    .FK_Entity_ID = objAccountEntity.PK_goAML_Trn_Entity_account

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Trn_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Trn_Director_Address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_goAML_Trn_Director_Address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_Director_ID = objDirector_New.PK_goAML_Trn_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Director_Address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        Dim objNewDirectorPhone As New goAML_trn_acc_sign_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_goAML_trn_acc_sign_Phone = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_Acc_Entity = objDirector_New.PK_goAML_Trn_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_trn_acc_sign_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 4 And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification In objListIdentification
                                        Dim objNewDirectorIdentification As New goAML_Transaction_Person_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_goAML_Transaction_Person_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = enumIdentificationPersonType.DirectorAccount
                                            .FK_Person_ID = objDirector_New.PK_goAML_Trn_Director_ID
                                            .from_or_To_Type = objAccount.FK_From_Or_To

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Person_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetMultiPartyAccountSignatoryClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.PK_Trn_Party_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Trn_par_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Trn_par_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_trn_par_acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Person_ID = objSignatory_Old.PK_Trn_par_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Transaction_Party_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Trn_par_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Trn_par_acc_Signatory.Min(Function(x) x.PK_Trn_par_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Trn_par_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Trn_par_Acc_sign_Address.Min(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_trn_par_acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_trn_par_acc_sign_Phone.Min(Function(x) x.PK_trn_Par_Acc_Sign_Phone)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Transaction_Party_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Transaction_Party_Identification.Min(Function(x) x.PK_Transaction_Party_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Trn_par_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_Trn_par_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_par_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_par_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_Trn_Par_Acc_sign_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_par_acc_Signatory_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_par_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_par_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_trn_Par_Acc_Sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Par_Acc_Sign = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_par_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Transaction_Party_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_Transaction_Party_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Party_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Trn_par_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_Trn_par_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Trn_par_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Trn_par_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_Trn_Par_Acc_sign_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_par_acc_Signatory_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Trn_par_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_trn_par_acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_trn_Par_Acc_Sign_Phone = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Trn_Par_Acc_Sign = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_trn_par_acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Transaction_Party_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_Transaction_Party_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Person_ID = objSignatory_New.PK_Trn_par_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Transaction_Party_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetMultiPartyAccountEntityClassByAccountID(objData As GoAML_Transaction_Class, strPK_Account_ID As String) As GoAML_Transaction_Class
        Try
            Dim objAccount = objData.list_goAML_Trn_Party_Account.Where(Function(x) x.PK_Trn_Party_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Trn_par_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_trn_par_acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Trn_Par_Acc_Ent_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Person_ID = objDirector_Old.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Transaction_Party_Identification.Remove(objIdentification_Old)
                                Next
                            End If
                        Next
                    End If
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = objAccount.PK_Trn_Party_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0

                        'PK Entity Address
                        If objData.list_goAML_Trn_par_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_par_Acc_Entity_Address.Min(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_trn_par_acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_trn_par_acc_Entity_Phone.Min(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director.Count > 0 Then
                            lngPK_Director = objData.list_goAML_Trn_Par_Acc_Ent_Director.Min(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID)
                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Min(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Min(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Transaction_Party_Identification.Count > 0 Then
                            lngPK_Director_Identification = objData.list_goAML_Transaction_Party_Identification.Min(Function(x) x.PK_Transaction_Party_Identification_ID)
                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Trn_par_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Trn_Par_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Trn_par_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_trn_par_acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_trn_Par_Acc_Entity_Phone_ID = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Trn_par_acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_trn_par_acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Trn_Par_Acc_Ent_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_Trn_Par_Acc_Ent_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Trn_Par_Acc_Entity_ID = objAccountEntity.PK_Trn_Par_acc_Entity_ID

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Trn_Par_Acc_Ent_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Trn_Par_Acc_Ent_Director_Address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_Trn_Par_Acc_Entity_Address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_par_acc_Entity_Director_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        Dim objNewDirectorPhone As New goAML_Trn_Par_Acc_Ent_Director_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_Trn_Par_Acc_Ent_Director_Phone_ID = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Trn_par_acc_Entity_Director_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isEmployer = False
                                            Else
                                                .isEmployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 4 And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification In objListIdentification
                                        Dim objNewDirectorIdentification As New goAML_Transaction_Party_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_Transaction_Party_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = enumIdentificationPersonType.DirectorAccount
                                            .FK_Person_ID = objDirector_New.PK_Trn_Par_Acc_Ent_Director_ID

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Transaction_Party_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetActivityAccountSignatoryClassByAccountID(objData As GoAML_Activity_Class, strPK_Account_ID As String) As GoAML_Activity_Class
        Try
            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_goAML_Act_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 Signatory yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim listSignatory_Old = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID).ToList
                If listSignatory_Old IsNot Nothing Then
                    For Each objSignatory_Old In listSignatory_Old
                        objData.list_goAML_Act_acc_Signatory.Remove(objSignatory_Old)

                        'Signatory Address
                        Dim listAddress_Old = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listAddress_Old IsNot Nothing Then
                            For Each objAddress_Old In listAddress_Old
                                objData.list_goAML_Act_Acc_sign_Address.Remove(objAddress_Old)
                            Next
                        End If

                        'Signatory Phone
                        Dim listPhone_Old = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listPhone_Old IsNot Nothing Then
                            For Each objPhone_Old In listPhone_Old
                                objData.list_goAML_Act_Acc_sign_Phone.Remove(objPhone_Old)
                            Next
                        End If

                        'Signatory Identification
                        Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory And x.FK_Act_Person_ID = objSignatory_Old.PK_goAML_Act_acc_Signatory_ID).ToList
                        If listIdentification_Old IsNot Nothing Then
                            For Each objIdentification_Old In listIdentification_Old
                                objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                            Next
                        End If
                    Next
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    'Get Last PK
                    Dim lngPK_Signatory As Long = 0
                    Dim lngPK_Address As Long = 0
                    Dim lngPK_Phone As Long = 0
                    Dim lngPK_Identification As Long = 0

                    'PK Signatory
                    If objData.list_goAML_Act_acc_Signatory.Count > 0 Then
                        lngPK_Signatory = objData.list_goAML_Act_acc_Signatory.Min(Function(x) x.PK_goAML_Act_acc_Signatory_ID)
                        If lngPK_Signatory > 0 Then
                            lngPK_Signatory = 0
                        End If
                    End If

                    'PK Signatory Address
                    If objData.list_goAML_Act_Acc_sign_Address.Count > 0 Then
                        lngPK_Address = objData.list_goAML_Act_Acc_sign_Address.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Address_ID)
                        If lngPK_Address > 0 Then
                            lngPK_Address = 0
                        End If
                    End If

                    'PK Signatory Phone
                    If objData.list_goAML_Act_Acc_sign_Phone.Count > 0 Then
                        lngPK_Phone = objData.list_goAML_Act_Acc_sign_Phone.Min(Function(x) x.PK_goAML_act_acc_sign_Phone_ID)
                        If lngPK_Phone > 0 Then
                            lngPK_Phone = 0
                        End If
                    End If

                    'PK Signatory Identification
                    If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
                        lngPK_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_goAML_Activity_Person_Identification_ID)
                        If lngPK_Identification > 0 Then
                            lngPK_Identification = 0
                        End If
                    End If

                    'Masukkand data2 Signatory dari goAML Ref
                    Dim objListgoAML_Ref_Account_Signatory As List(Of goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.Account).ToList
                    If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                        For Each objSignatory In objListgoAML_Ref_Account_Signatory

                            If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
                                Dim objSignatory_New As New goAML_Act_acc_Signatory

                                'Get Signatory Nasabah
                                Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
                                If objCIFPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Act_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                        .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                        'Load Person CIF Detail
                                        If objCIFPerson IsNot Nothing Then
                                            .Gender = objCIFPerson.INDV_Gender
                                            .Title = objCIFPerson.INDV_Title
                                            .First_Name = objCIFPerson.INDV_First_name
                                            .Middle_Name = objCIFPerson.INDV_Middle_name
                                            .Prefix = objCIFPerson.INDV_Prefix
                                            .Last_Name = objCIFPerson.INDV_Last_Name
                                            .BirthDate = objCIFPerson.INDV_BirthDate
                                            .Birth_Place = objCIFPerson.INDV_Birth_Place
                                            .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                            .Alias = objCIFPerson.INDV_Alias
                                            .SSN = objCIFPerson.INDV_SSN
                                            .Passport_Number = objCIFPerson.INDV_Passport_Number
                                            .Passport_Country = objCIFPerson.INDV_Passport_Country
                                            .Id_Number = objCIFPerson.INDV_ID_Number
                                            .Nationality1 = objCIFPerson.INDV_Nationality1
                                            .Nationality2 = objCIFPerson.INDV_Nationality2
                                            .Nationality3 = objCIFPerson.INDV_Nationality3
                                            .Residence = objCIFPerson.INDV_Residence
                                            .Email = objCIFPerson.INDV_Email
                                            .email2 = objCIFPerson.INDV_Email2
                                            .email3 = objCIFPerson.INDV_Email3
                                            .email4 = objCIFPerson.INDV_Email4
                                            .email5 = objCIFPerson.INDV_Email5
                                            .Occupation = objCIFPerson.INDV_Occupation
                                            .Employer_Name = objCIFPerson.INDV_Employer_Name
                                            .Deceased = objCIFPerson.INDV_Deceased
                                            .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                            .Tax_Number = objCIFPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                            .Comment = objCIFPerson.INDV_Comments

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (1=Address, 5=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Act_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_act_acc_sign_Phone_ID = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 1 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Activity_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Activity_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            Else        '====== SIGNATORY NON NASABAH ======
                                Dim objSignatory_New As New goAML_Act_acc_Signatory

                                'Get Signatory Non Nasabah
                                Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
                                If objWICPerson IsNot Nothing Then
                                    With objSignatory_New
                                        lngPK_Signatory = lngPK_Signatory - 1

                                        .PK_goAML_Act_acc_Signatory_ID = lngPK_Signatory
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_ReportParty_ID = objAccount.FK_Act_ReportParty_ID
                                        .FK_Activity_Account_ID = objAccount.PK_goAML_Act_Account_ID

                                        'Load Person CIF Detail
                                        If objWICPerson IsNot Nothing Then
                                            .Gender = objWICPerson.INDV_Gender
                                            .Title = objWICPerson.INDV_Title
                                            .First_Name = objWICPerson.INDV_First_Name
                                            .Middle_Name = objWICPerson.INDV_Middle_Name
                                            .Prefix = objWICPerson.INDV_Prefix
                                            .Last_Name = objWICPerson.INDV_Last_Name
                                            .BirthDate = objWICPerson.INDV_BirthDate
                                            .Birth_Place = objWICPerson.INDV_Birth_Place
                                            .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                            .Alias = objWICPerson.INDV_Alias
                                            .SSN = objWICPerson.INDV_SSN
                                            .Passport_Number = objWICPerson.INDV_Passport_Number
                                            .Passport_Country = objWICPerson.INDV_Passport_Country
                                            .Id_Number = objWICPerson.INDV_ID_Number
                                            .Nationality1 = objWICPerson.INDV_Nationality1
                                            .Nationality2 = objWICPerson.INDV_Nationality2
                                            .Nationality3 = objWICPerson.INDV_Nationality3
                                            .Residence = objWICPerson.INDV_Residence
                                            .Email = objWICPerson.INDV_Email
                                            .email2 = objWICPerson.INDV_Email2
                                            .email3 = objWICPerson.INDV_Email3
                                            .email4 = objWICPerson.INDV_Email4
                                            .email5 = objWICPerson.INDV_Email5
                                            .Occupation = objWICPerson.INDV_Occupation
                                            .Employer_Name = objWICPerson.INDV_Employer_Name
                                            .Deceased = objWICPerson.INDV_Deceased
                                            .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                            .Tax_Number = objWICPerson.INDV_Tax_Number
                                            .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                            .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                            .Comment = objWICPerson.INDV_Comment

                                            .isPrimary = objSignatory.isPrimary
                                            .role = objSignatory.Role
                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End If
                                        objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

                                        'Signatory Address(es)  (3=Address, 10=Employer Address)
                                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                            For Each objAddress In objListAddress
                                                Dim objNewSignatoryAddress As New goAML_Act_Acc_sign_Address
                                                With objNewSignatoryAddress
                                                    lngPK_Address = lngPK_Address - 1

                                                    .PK_goAML_Act_Acc_Entity_Address_ID = lngPK_Address
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Address_Type = objAddress.Address_Type
                                                    .Address = objAddress.Address
                                                    .Town = objAddress.Town
                                                    .City = objAddress.City
                                                    .Zip = objAddress.Zip
                                                    .Country_Code = objAddress.Country_Code
                                                    .State = objAddress.State
                                                    .Comments = objAddress.Comments

                                                    If objAddress.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
                                            Next
                                        End If

                                        'Signatory Phone(s) (3=Phone, 10=Employer Phone)
                                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                            For Each objPhone In objListPhone
                                                Dim objNewSignatoryPhone As New goAML_Act_Acc_sign_Phone
                                                With objNewSignatoryPhone
                                                    lngPK_Phone = lngPK_Phone - 1

                                                    .PK_goAML_act_acc_sign_Phone_ID = lngPK_Phone
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Act_Acc_Entity_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                    .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                    .tph_country_prefix = objPhone.tph_country_prefix
                                                    .tph_number = objPhone.tph_number
                                                    .tph_extension = objPhone.tph_extension
                                                    .comments = objPhone.comments

                                                    If objPhone.FK_Ref_Detail_Of = 3 Then
                                                        .isEmployer = False
                                                    Else
                                                        .isEmployer = True
                                                    End If

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
                                            Next
                                        End If

                                        'Signatory Identification(s)
                                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
                                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                            For Each objIdentification In objListIdentification
                                                Dim objNewSignatoryIdentification As New goAML_Activity_Person_Identification
                                                With objNewSignatoryIdentification
                                                    lngPK_Identification = lngPK_Identification - 1

                                                    .PK_goAML_Activity_Person_Identification_ID = lngPK_Identification
                                                    .FK_Report_ID = objAccount.FK_Report_ID
                                                    .FK_Person_Type = enumIdentificationPersonType.Signatory
                                                    .FK_Act_Person_ID = objSignatory_New.PK_goAML_Act_acc_Signatory_ID

                                                    .Type = objIdentification.Type
                                                    .Number = objIdentification.Number
                                                    .Issue_Date = objIdentification.Issue_Date
                                                    .Expiry_Date = objIdentification.Expiry_Date
                                                    .Issued_By = objIdentification.Issued_By
                                                    .Issued_Country = objIdentification.Issued_Country
                                                    .Identification_Comment = objIdentification.Identification_Comment

                                                    .Active = 1
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                End With
                                                objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
                                            Next
                                        End If
                                    End With
                                End If
                            End If
                        Next
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetActivityAccountEntityClassByAccountID(objData As GoAML_Activity_Class, strPK_Account_ID As String) As GoAML_Activity_Class
        Try
            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_goAML_Act_Account_ID = strPK_Account_ID).FirstOrDefault

            'Hapus data2 yang lama (jika ada)
            If objAccount IsNot Nothing Then
                Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID).FirstOrDefault
                If objAccountEntity IsNot Nothing Then
                    'Account Entity Address
                    Dim listAccountEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listAccountEntityAddress IsNot Nothing Then
                        For Each objAddress_Old In listAccountEntityAddress
                            objData.list_goAML_Act_Acc_Entity_Address.Remove(objAddress_Old)
                        Next
                    End If

                    'Account Entity Phone
                    Dim listAccountEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listAccountEntityPhone IsNot Nothing Then
                        For Each objPhone_Old In listAccountEntityPhone
                            objData.list_goAML_Act_Acc_Entity_Phone.Remove(objPhone_Old)
                        Next
                    End If

                    'Director
                    Dim listDirector_Old = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
                    If listDirector_Old IsNot Nothing Then
                        For Each objDirector_Old In listDirector_Old
                            objData.list_goAML_Act_Acc_Ent_Director.Remove(objDirector_Old)

                            'Director Address
                            Dim listAddress_Old = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listAddress_Old IsNot Nothing Then
                                For Each objAddress_Old In listAddress_Old
                                    objData.list_goAML_Act_Acc_Entity_Director_address.Remove(objAddress_Old)
                                Next
                            End If

                            'Director Phone
                            Dim listPhone_Old = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listPhone_Old IsNot Nothing Then
                                For Each objPhone_Old In listPhone_Old
                                    objData.list_goAML_Act_Acc_Entity_Director_Phone.Remove(objPhone_Old)
                                Next
                            End If

                            'Director Identification
                            Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
                            If listIdentification_Old IsNot Nothing Then
                                For Each objIdentification_Old In listIdentification_Old
                                    objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
                                Next
                            End If
                        Next
                    End If
                End If
            End If

            'Masukkan data2 baru ke dalam Class
            Using objDB As New NawaDatadevEntities
                If objAccount IsNot Nothing Then
                    Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = objAccount.PK_goAML_Act_Account_ID).FirstOrDefault
                    If objAccount IsNot Nothing Then
                        'Get Last PK
                        Dim lngPK_Entity_Address As Long = 0
                        Dim lngPK_Entity_Phone As Long = 0

                        Dim lngPK_Director As Long = 0
                        Dim lngPK_Director_Address As Long = 0
                        Dim lngPK_Director_Phone As Long = 0
                        Dim lngPK_Director_Identification As Long = 0

                        'PK Entity Address
                        If objData.list_goAML_Act_Acc_Entity_Address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Act_Acc_Entity_Address.Min(Function(x) x.PK_Act_Acc_Entity_Address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Entity Phone
                        If objData.list_goAML_Act_Acc_Entity_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Act_Acc_Entity_Phone.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director
                        If objData.list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                            lngPK_Director = objData.list_goAML_Act_Acc_Ent_Director.Min(Function(x) x.PK_Act_Acc_Ent_Director_ID)
                            If lngPK_Director > 0 Then
                                lngPK_Director = 0
                            End If
                        End If

                        'PK Director Address
                        If objData.list_goAML_Act_Acc_Entity_Director_address.Count > 0 Then
                            lngPK_Director_Address = objData.list_goAML_Act_Acc_Entity_Director_address.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID)
                            If lngPK_Director_Address > 0 Then
                                lngPK_Director_Address = 0
                            End If
                        End If

                        'PK Director Phone
                        If objData.list_goAML_Act_Acc_Entity_Director_Phone.Count > 0 Then
                            lngPK_Director_Phone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
                            If lngPK_Director_Phone > 0 Then
                                lngPK_Director_Phone = 0
                            End If
                        End If

                        'PK Director Identification
                        If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
                            lngPK_Director_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_goAML_Activity_Person_Identification_ID)
                            If lngPK_Director_Identification > 0 Then
                                lngPK_Director_Identification = 0
                            End If
                        End If

                        'Masukkan data2 Entity Address & Phone dari goAML Ref
                        'Account Entity Address(es)
                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.Client_Number).FirstOrDefault
                        If objCustomer IsNot Nothing Then
                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
                                For Each objAddress In objListEntityAddress
                                    Dim objNewEntityAddress As New goAML_Act_Acc_Entity_Address
                                    With objNewEntityAddress
                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

                                        .PK_Act_Acc_Entity_Address_ID = lngPK_Entity_Address
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                        .Address_Type = objAddress.Address_Type
                                        .Address = objAddress.Address
                                        .Town = objAddress.Town
                                        .City = objAddress.City
                                        .Zip = objAddress.Zip
                                        .Country_Code = objAddress.Country_Code
                                        .State = objAddress.State
                                        .Comments = objAddress.Comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Act_Acc_Entity_Address.Add(objNewEntityAddress)
                                Next
                            End If

                            'Entity Phone(s)
                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
                                For Each objPhone In objListEntityPhone
                                    Dim objNewEntityPhone As New goAML_Act_Acc_Entity_Phone
                                    With objNewEntityPhone
                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

                                        .PK_goAML_Act_Acc_Entity_Phone_ID = lngPK_Entity_Phone
                                        .FK_Report_ID = objAccount.FK_Report_ID
                                        .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                        .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                        .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                        .tph_country_prefix = objPhone.tph_country_prefix
                                        .tph_number = objPhone.tph_number
                                        .tph_extension = objPhone.tph_extension
                                        .comments = objPhone.comments

                                        .Active = 1
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objData.list_goAML_Act_Acc_Entity_Phone.Add(objNewEntityPhone)
                                Next
                            End If
                        End If

                        'Masukkand data2 Director dari goAML Ref
                        Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.Client_Number).ToList
                        If objListgoAML_Ref_Account_Director IsNot Nothing Then
                            For Each objDirector In objListgoAML_Ref_Account_Director
                                Dim objDirector_New As New goAML_Act_Acc_Ent_Director
                                With objDirector_New
                                    lngPK_Director = lngPK_Director - 1

                                    .PK_Act_Acc_Ent_Director_ID = lngPK_Director
                                    .FK_Report_ID = objAccount.FK_Report_ID
                                    .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

                                    .Gender = objDirector.Gender
                                    .Title = objDirector.Title
                                    .First_Name = objDirector.First_name
                                    .Middle_Name = objDirector.Middle_Name
                                    .Prefix = objDirector.Prefix
                                    .Last_Name = objDirector.Last_Name
                                    .BirthDate = objDirector.BirthDate
                                    .Birth_Place = objDirector.Birth_Place
                                    .Mothers_Name = objDirector.Mothers_Name
                                    .Alias = objDirector.Alias
                                    .SSN = objDirector.SSN
                                    .Passport_Number = objDirector.Passport_Number
                                    .Passport_Country = objDirector.Passport_Country
                                    .Id_Number = objDirector.ID_Number
                                    .Nationality1 = objDirector.Nationality1
                                    .Nationality2 = objDirector.Nationality2
                                    .Nationality3 = objDirector.Nationality3
                                    .Residence = objDirector.Residence
                                    .Email = objDirector.Email
                                    .email2 = objDirector.Email2
                                    .email3 = objDirector.Email3
                                    .email4 = objDirector.Email4
                                    .email5 = objDirector.Email5
                                    .Occupation = objDirector.Occupation
                                    .Employer_Name = objDirector.Employer_Name
                                    .Deceased = objDirector.Deceased
                                    .Deceased_Date = objDirector.Deceased_Date
                                    .Tax_Number = objDirector.Tax_Number
                                    .Tax_Reg_Number = objDirector.Tax_Reg_Number
                                    .Source_Of_Wealth = objDirector.Source_of_Wealth
                                    .Comment = objDirector.Comments

                                    .role = objDirector.Role
                                    .Active = 1
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                End With
                                objData.list_goAML_Act_Acc_Ent_Director.Add(objDirector_New)

                                'Director Address(es)  (6=Address, 7=Employer Address)
                                Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        Dim objNewDirectorAddress As New goAML_Act_Acc_Entity_Director_address
                                        With objNewDirectorAddress
                                            lngPK_Director_Address = lngPK_Director_Address - 1

                                            .PK_goAML_Act_Acc_Entity_Director_address_ID = lngPK_Director_Address
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments

                                            If objAddress.FK_Ref_Detail_Of = 6 Then
                                                .isemployer = False
                                            Else
                                                .isemployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Act_Acc_Entity_Director_address.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s) (6=Phone, 7=Employer Phone)
                                Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        Dim objNewDirectorPhone As New goAML_Act_Acc_Entity_Director_Phone
                                        With objNewDirectorPhone
                                            lngPK_Director_Phone = lngPK_Director_Phone - 1

                                            .PK_goAML_Act_Acc_Entity_Director_Phone_ID = lngPK_Director_Phone
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments

                                            If objPhone.FK_Ref_Detail_Of = 6 Then
                                                .isemployer = False
                                            Else
                                                .isemployer = True
                                            End If

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Act_Acc_Entity_Director_Phone.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 4 And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification In objListIdentification
                                        Dim objNewDirectorIdentification As New goAML_Activity_Person_Identification
                                        With objNewDirectorIdentification
                                            lngPK_Director_Identification = lngPK_Director_Identification - 1

                                            .PK_goAML_Activity_Person_Identification_ID = lngPK_Director_Identification
                                            .FK_Report_ID = objAccount.FK_Report_ID
                                            .FK_Person_Type = enumIdentificationPersonType.DirectorAccount
                                            .FK_Act_Person_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment

                                            .Active = 1
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                        End With
                                        objData.list_goAML_Activity_Person_Identification.Add(objNewDirectorIdentification)
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End Using

            'Balikin lagi datanya ke Front End
            Return objData
        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

#End Region

#Region "22-Nov-2021 : Save Data To Validate by XML"
    Shared Sub validateTransaction(objReport As GoAML_Report_Class, objTransaction As GoAML_Transaction_Class)
        Try
            Dim strQuery As String

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of NawaDevDAL.goAML_Report)
            Dim listGoAML_Transaction As New List(Of NawaDevDAL.goAML_Transaction)

            listGoAML_Report.Add(objReport.obj_goAML_Report)
            listGoAML_Transaction.Add(objTransaction.obj_goAML_Transaction)

            SaveDataToValidateWithXML("goAML_Report", listGoAML_Report)
            SaveDataToValidateWithXML("goAML_Transaction", listGoAML_Transaction)

            If objTransaction.obj_goAML_Transaction.FK_Transaction_Type = 1 Then   'Transaction Bi-Party
                SaveDataToValidateWithXML("goAML_Transaction_Account", objTransaction.list_goAML_Transaction_Account)
                SaveDataToValidateWithXML("goAML_Trn_acc_Signatory", objTransaction.list_goAML_Trn_acc_Signatory)
                SaveDataToValidateWithXML("goAML_Trn_Acc_sign_Address", objTransaction.list_goAML_Trn_Acc_sign_Address)
                SaveDataToValidateWithXML("goAML_trn_acc_sign_Phone", objTransaction.list_goAML_trn_acc_sign_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Entity_account", objTransaction.list_goAML_Trn_Entity_account)
                SaveDataToValidateWithXML("goAML_Trn_Acc_Entity_Address", objTransaction.list_goAML_Trn_Acc_Entity_Address)
                SaveDataToValidateWithXML("goAML_Trn_Acc_Entity_Phone", objTransaction.list_goAML_Trn_Acc_Entity_Phone)

                SaveDataToValidateWithXML("goAML_Transaction_Person", objTransaction.list_goAML_Transaction_Person)
                SaveDataToValidateWithXML("goAML_Trn_Person_Address", objTransaction.list_goAML_Trn_Person_Address)
                SaveDataToValidateWithXML("goAML_trn_Person_Phone", objTransaction.list_goAML_trn_Person_Phone)

                SaveDataToValidateWithXML("goAML_Transaction_Entity", objTransaction.list_goAML_Transaction_Entity)
                SaveDataToValidateWithXML("goAML_Trn_Entity_Address", objTransaction.list_goAML_Trn_Entity_Address)
                SaveDataToValidateWithXML("goAML_Trn_Entity_Phone", objTransaction.list_goAML_Trn_Entity_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Director", objTransaction.list_goAML_Trn_Director)
                SaveDataToValidateWithXML("goAML_Trn_Director_Address", objTransaction.list_goAML_Trn_Director_Address)
                SaveDataToValidateWithXML("goAML_trn_Director_Phone", objTransaction.list_goAML_trn_Director_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Conductor", objTransaction.list_goAML_Trn_Conductor)
                SaveDataToValidateWithXML("goAML_Trn_Conductor_Address", objTransaction.list_goAML_Trn_Conductor_Address)
                SaveDataToValidateWithXML("goAML_trn_Conductor_Phone", objTransaction.list_goAML_trn_Conductor_Phone)

                SaveDataToValidateWithXML("goAML_Transaction_Person_Identification", objTransaction.list_goAML_Transaction_Person_Identification)
            Else    'Transaction Multi-Party
                SaveDataToValidateWithXML("goAML_Transaction_Party", objTransaction.list_goAML_Transaction_Party)

                SaveDataToValidateWithXML("goAML_Trn_Party_Account", objTransaction.list_goAML_Trn_Party_Account)
                SaveDataToValidateWithXML("goAML_Trn_par_acc_Signatory", objTransaction.list_goAML_Trn_par_acc_Signatory)
                SaveDataToValidateWithXML("goAML_Trn_par_Acc_sign_Address", objTransaction.list_goAML_Trn_par_Acc_sign_Address)
                SaveDataToValidateWithXML("goAML_trn_par_acc_sign_Phone", objTransaction.list_goAML_trn_par_acc_sign_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Par_Acc_Entity", objTransaction.list_goAML_Trn_Par_Acc_Entity)
                SaveDataToValidateWithXML("goAML_Trn_par_Acc_Entity_Address", objTransaction.list_goAML_Trn_par_Acc_Entity_Address)
                SaveDataToValidateWithXML("goAML_trn_par_acc_Entity_Phone", objTransaction.list_goAML_trn_par_acc_Entity_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Par_Acc_Ent_Director", objTransaction.list_goAML_Trn_Par_Acc_Ent_Director)
                SaveDataToValidateWithXML("goAML_Trn_Par_Acc_Ent_Director_Address", objTransaction.list_goAML_Trn_Par_Acc_Ent_Director_Address)
                SaveDataToValidateWithXML("goAML_Trn_Par_Acc_Ent_Director_Phone", objTransaction.list_goAML_Trn_Par_Acc_Ent_Director_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Party_Person", objTransaction.list_goAML_Trn_Party_Person)
                SaveDataToValidateWithXML("goAML_Trn_Party_Person_Address", objTransaction.list_goAML_Trn_Party_Person_Address)
                SaveDataToValidateWithXML("goAML_Trn_Party_Person_Phone", objTransaction.list_goAML_Trn_Party_Person_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Party_Entity", objTransaction.list_goAML_Trn_Party_Entity)
                SaveDataToValidateWithXML("goAML_Trn_Party_Entity_Address", objTransaction.list_goAML_Trn_Party_Entity_Address)
                SaveDataToValidateWithXML("goAML_Trn_Party_Entity_Phone", objTransaction.list_goAML_Trn_Party_Entity_Phone)

                SaveDataToValidateWithXML("goAML_Trn_Par_Entity_Director", objTransaction.list_goAML_Trn_Par_Entity_Director)
                SaveDataToValidateWithXML("goAML_Trn_Par_Entity_Director_Address", objTransaction.list_goAML_Trn_Par_Entity_Director_Address)
                SaveDataToValidateWithXML("goAML_Trn_Par_Entity_Director_Phone", objTransaction.list_goAML_Trn_Par_Entity_Director_Phone)

                SaveDataToValidateWithXML("goAML_Transaction_Party_Identification", objTransaction.list_goAML_Transaction_Party_Identification)
            End If

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Report', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveDataToValidateWithXML(tablename As String, objData As Object)
        Try
            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            If objData IsNot Nothing Then
                objdt = NawaBLL.Common.CopyGenericToDataTable(objData)
                objdataset.Tables.Add(objdt)
                objXMLData = objdataset.GetXml()
                byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            End If

            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@TableName"
            param(0).Value = tablename
            param(0).SqlDbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@XmlDocument"
            param(1).Value = byteData
            param(1).SqlDbType = SqlDbType.VarBinary

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).SqlDbType = SqlDbType.VarChar

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_Report_XML_ToValidate_Save", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

Public Class GoAML_Report_Class

    'GoAML Report
    Public obj_goAML_Report As New NawaDevDAL.goAML_Report

    'Transaction
    Public list_goAML_Transaction As New List(Of NawaDevDAL.goAML_Transaction)

    'Activity
    Public list_goAML_Act_ReportPartyType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)

    'Indicator
    Public list_goAML_Report_Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator)

End Class


Public Class GoAML_Transaction_Class
    'Transaction
    Public obj_goAML_Transaction As New goAML_Transaction

    'Transaction Bi-Party
    Public list_goAML_Transaction_Account As New List(Of NawaDevDAL.goAML_Transaction_Account)
    Public list_goAML_Trn_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
    Public list_goAML_Trn_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
    Public list_goAML_trn_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)

    Public list_goAML_Trn_Entity_account As New List(Of NawaDevDAL.goAML_Trn_Entity_account)
    Public list_goAML_Trn_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
    Public list_goAML_Trn_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)

    Public list_goAML_Transaction_Person As New List(Of NawaDevDAL.goAML_Transaction_Person)
    Public list_goAML_Trn_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
    Public list_goAML_trn_Person_Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)

    Public list_goAML_Transaction_Entity As New List(Of NawaDevDAL.goAML_Transaction_Entity)
    Public list_goAML_Trn_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
    Public list_goAML_Trn_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)

    Public list_goAML_Trn_Director As New List(Of NawaDevDAL.goAML_Trn_Director)
    Public list_goAML_Trn_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
    Public list_goAML_trn_Director_Phone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)

    Public list_goAML_Trn_Conductor As New List(Of NawaDevDAL.goAML_Trn_Conductor)
    Public list_goAML_Trn_Conductor_Address As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
    Public list_goAML_trn_Conductor_Phone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)

    Public list_goAML_Transaction_Person_Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)

    'Transaction Multi Party
    Public list_goAML_Transaction_Party As New List(Of NawaDevDAL.goAML_Transaction_Party)

    Public list_goAML_Trn_Party_Account As New List(Of NawaDevDAL.goAML_Trn_Party_Account)
    Public list_goAML_Trn_par_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
    Public list_goAML_Trn_par_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
    Public list_goAML_trn_par_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)

    Public list_goAML_Trn_Par_Acc_Entity As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)
    Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
    Public list_goAML_trn_par_acc_Entity_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)

    Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

    Public list_goAML_Trn_Party_Person As New List(Of NawaDevDAL.goAML_Trn_Party_Person)
    Public list_goAML_Trn_Party_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
    Public list_goAML_Trn_Party_Person_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)

    Public list_goAML_Trn_Party_Entity As New List(Of NawaDevDAL.goAML_Trn_Party_Entity)
    Public list_goAML_Trn_Party_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
    Public list_goAML_Trn_Party_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)

    Public list_goAML_Trn_Par_Entity_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
    Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)

    Public list_goAML_Transaction_Party_Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
End Class

Public Class GoAML_Activity_Class
    Public obj_goAML_Act_ReportPartyType As New goAML_Act_ReportPartyType

    Public list_goAML_Act_Account As New List(Of NawaDevDAL.goAML_Act_Account)
    Public list_goAML_Act_acc_Signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
    Public list_goAML_Act_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
    Public list_goAML_Act_Acc_sign_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)

    Public list_goAML_Act_Entity_Account As New List(Of NawaDevDAL.goAML_Act_Entity_Account)
    Public list_goAML_Act_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
    Public list_goAML_Act_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)

    Public list_goAML_Act_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
    Public list_goAML_Act_Acc_Entity_Director_address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
    Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)

    Public list_goAML_Act_Person As New List(Of NawaDevDAL.goAML_Act_Person)
    Public list_goAML_Act_Person_Address As New List(Of NawaDevDAL.goAML_Act_Person_Address)
    Public list_goAML_Act_Person_Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)

    Public list_goAML_Act_Entity As New List(Of NawaDevDAL.goAML_Act_Entity)
    Public list_goAML_Act_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
    Public list_goAML_Act_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)

    Public list_goAML_Act_Director As New List(Of NawaDevDAL.goAML_Act_Director)
    Public list_goAML_Act_Director_Address As New List(Of NawaDevDAL.goAML_Act_Director_Address)
    Public list_goAML_Act_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)

    Public list_goAML_Activity_Person_Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
End Class