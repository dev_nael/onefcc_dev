﻿'==========================================
'Created Date   : 22 Sep 2021
'Created By     : NawaData
'Description    : Library for goAML Report
'                 
'==========================================

Imports System.Data.SqlClient
Imports NawaDevDAL

Public Class GoAML_Report_BLL

    'Enum Identification Person Type : fungsi utama untuk membedakan terutama Person Identification
    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        'Director = 4        'Untuk level transaksi gak dipakai. Yang dipakai untuk yg 6
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
    End Enum

    'Enum SenderInformation : fungsi utama untuk membedakan terutama untuk Entity Director (antara Account Entity Director dan Entity Director 1 table yang sama)
    Public Enum enumSenderInformation
        Account = 1
        Person = 2
        Entity = 3
    End Enum

    Shared Function getTRNorACT(strReportCode As String) As String
        Try
            Dim strResult As String = "TRN"
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("goAML_odm_ref_Report_TRNorACT", "ReportType", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("TRNorACT")) Then
                    strResult = drTemp("TRNorACT")
                End If
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return "TRN"
        End Try
    End Function

    Shared Function isUseIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                isUseIndicator = True
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function isRequireIndicator(strReportCode As String) As Boolean
        Try
            Dim bolResult As Boolean = False
            Dim drTemp As DataRow = GoAML_Global_BLL.getDataRowByID("GoAML_Laporan_With_Indicator", "Kode", strReportCode)
            If drTemp IsNot Nothing Then
                If Not IsDBNull(drTemp("ReportType")) Then
                    If drTemp("ReportType").ToString.Contains("STR") Then
                        bolResult = True
                    End If
                End If
            End If

            Return bolResult
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Shared Function getValidationFromType(transmodeCode As String, instrumenFrom As String, SenderInformation As Integer, myClient As Boolean) As String
        Dim keterangan As String = ""
        Try
            Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
                If SenderInformation = 1 Then ' Account
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Account = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Account NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 2 Then 'Person
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Person = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Person NotMyClient"
                        End If
                    End If

                ElseIf SenderInformation = 3 Then 'Entity
                    If myClient Then
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity MyClient"
                        End If
                    Else
                        Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Entity = True).FirstOrDefault
                        If obj Is Nothing Then
                            keterangan = "Can not choose sender information Entity NotMyClient"
                        End If
                    End If
                End If
            End Using

            Return keterangan
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getValidationToType(transmodeCode As String, instrumenFrom As String, instrumenTo As String, PenerimaInformation As Integer, TomyClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If PenerimaInformation = 1 Then ' Account
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Account NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 2 Then 'Person
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Person NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 3 Then 'Entity
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = "Can not choose recipient information Entity NotMyClient"
                    End If
                End If

            End If
        End Using

        Return keterangan
    End Function

    Shared Function GetGoAMLReportClassByID(ID As Long) As GoAML_Report_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLReportClass = New GoAML_Report_Class
            With objGoAMLReportClass
                'GoAML Report
                .obj_goAML_Report = objDb.goAML_Report.Where(Function(x) x.PK_Report_ID = ID).FirstOrDefault

                'Transaction
                .list_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = ID).ToList

                'Activity
                .list_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = ID).ToList

                'Indicator
                .list_goAML_Report_Indicator = objDb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = ID).ToList
            End With

            Return objGoAMLReportClass
        End Using
    End Function

    Shared Function GetGoAMLTransactionClassByID(ID As Long) As GoAML_Transaction_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLTransactionClass = New GoAML_Transaction_Class
            With objGoAMLTransactionClass
                'GoAML Transaction
                .obj_goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = ID).FirstOrDefault

                If .obj_goAML_Transaction IsNot Nothing Then
                    If .obj_goAML_Transaction.FK_Transaction_Type IsNot Nothing Then
                        If .obj_goAML_Transaction.FK_Transaction_Type = 1 Then      'Transaction Bi-Party
                            'Account
                            Dim listAccount = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = ID).ToList
                            If listAccount IsNot Nothing Then .list_goAML_Transaction_Account.AddRange(listAccount)

                            'Signatory
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listSignatory = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = item.PK_Account_ID).ToList
                                    If listSignatory IsNot Nothing Then .list_goAML_Trn_acc_Signatory.AddRange(listSignatory)
                                Next
                            End If

                            'Signatory Address, Phone, Identification
                            If .list_goAML_Trn_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_acc_Signatory.Count > 0 Then
                                For Each item In .list_goAML_Trn_acc_Signatory
                                    Dim listAddress = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_acc_Signatory_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_sign_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_acc_sign_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Account Entity
                            If .list_goAML_Transaction_Account IsNot Nothing AndAlso .list_goAML_Transaction_Account.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Account
                                    Dim listAccountEntity = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = item.PK_Account_ID).ToList
                                    If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Entity_account.AddRange(listAccountEntity)
                                Next
                            End If

                            'Account Entity Address, Phone
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listAddress = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList
                                    Dim listPhone = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = item.PK_goAML_Trn_Entity_account).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Acc_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Account Entity Director
                            If .list_goAML_Trn_Entity_account IsNot Nothing AndAlso .list_goAML_Trn_Entity_account.Count > 0 Then
                                For Each item In .list_goAML_Trn_Entity_account
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = item.PK_goAML_Trn_Entity_account And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                                    If listDirector IsNot Nothing Then .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Account Entity Director Address, Phone, Identification
                            Dim listAccountEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Account).ToList
                            If listAccountEntityDirector IsNot Nothing AndAlso listAccountEntityDirector.Count > 0 Then
                                For Each item In listAccountEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Person
                            Dim listPerson = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listPerson IsNot Nothing Then .list_goAML_Transaction_Person.AddRange(listPerson)

                            'Person Address, Phone, Identification
                            If .list_goAML_Transaction_Person IsNot Nothing AndAlso .list_goAML_Transaction_Person.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Person
                                    Dim listAddress = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = item.PK_Person_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Person_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Person_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Entity
                            Dim listEntity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listEntity IsNot Nothing Then .list_goAML_Transaction_Entity.AddRange(listEntity)

                            'Entity Address, Phone
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listAddress = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList
                                    Dim listPhone = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = item.PK_Entity_ID).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Entity_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_Trn_Entity_Phone.AddRange(listPhone)
                                Next
                            End If

                            'Entity Director
                            If .list_goAML_Transaction_Entity IsNot Nothing AndAlso .list_goAML_Transaction_Entity.Count > 0 Then
                                For Each item In .list_goAML_Transaction_Entity
                                    Dim listDirector = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Transaction_ID = ID And x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                                    .list_goAML_Trn_Director.AddRange(listDirector)
                                Next
                            End If

                            'Entity Director Address, Phone, Identification
                            Dim listEntityDirector = .list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = GoAML_Report_BLL.enumSenderInformation.Entity).ToList
                            If listEntityDirector IsNot Nothing AndAlso listEntityDirector.Count > 0 Then
                                For Each item In listEntityDirector
                                    Dim listAddress = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Director_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Director_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                            'Conductor
                            Dim listConductor = objDb.goAML_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listConductor IsNot Nothing Then .list_goAML_Trn_Conductor.AddRange(listConductor)

                            'Conductor Address, Phone, Identification
                            If .list_goAML_Trn_Conductor IsNot Nothing AndAlso .list_goAML_Trn_Conductor.Count > 0 Then
                                For Each item In .list_goAML_Trn_Conductor
                                    Dim listAddress = objDb.goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listPhone = objDb.goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = item.PK_goAML_Trn_Conductor_ID).ToList
                                    Dim listIdentification = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Conductor).ToList

                                    If listAddress IsNot Nothing Then .list_goAML_Trn_Conductor_Address.AddRange(listAddress)
                                    If listPhone IsNot Nothing Then .list_goAML_trn_Conductor_Phone.AddRange(listPhone)
                                    If listIdentification IsNot Nothing Then .list_goAML_Transaction_Person_Identification.AddRange(listIdentification)
                                Next
                            End If

                        Else    'Transaction Multi Party
                            Dim listParty = objDb.goAML_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = ID).ToList
                            If listParty IsNot Nothing Then .list_goAML_Transaction_Party.AddRange(listParty)

                            If .list_goAML_Transaction_Party IsNot Nothing AndAlso .list_goAML_Transaction_Party.Count > 0 Then
                                For Each party In .list_goAML_Transaction_Party
                                    'Account
                                    Dim listAccount = objDb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listAccount IsNot Nothing Then .list_goAML_Trn_Party_Account.AddRange(listAccount)

                                    'Signatory
                                    If .list_goAML_Trn_Party_Account IsNot Nothing AndAlso .list_goAML_Trn_Party_Account.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Account
                                            Dim listSignatory = objDb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = item.PK_Trn_Party_Account_ID).ToList
                                            If listSignatory IsNot Nothing Then .list_goAML_Trn_par_acc_Signatory.AddRange(listSignatory)
                                        Next
                                    End If

                                    'Signatory Address, Phone, Identification
                                    If .list_goAML_Trn_par_acc_Signatory IsNot Nothing AndAlso .list_goAML_Trn_par_acc_Signatory.Count > 0 Then
                                        For Each item In .list_goAML_Trn_par_acc_Signatory
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = item.PK_Trn_par_acc_Signatory_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = item.PK_Trn_par_acc_Signatory_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_par_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_sign_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_sign_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Account Entity
                                    If .list_goAML_Trn_Party_Account IsNot Nothing AndAlso .list_goAML_Trn_Party_Account.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Account
                                            Dim listAccountEntity = objDb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = item.PK_Trn_Party_Account_ID).ToList
                                            If listAccountEntity IsNot Nothing Then .list_goAML_Trn_Par_Acc_Entity.AddRange(listAccountEntity)
                                        Next
                                    End If

                                    'Account Entity Address, Phone
                                    If .list_goAML_Trn_Par_Acc_Entity IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Entity
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)
                                        Next
                                    End If

                                    'Account Entity Director
                                    If .list_goAML_Trn_Par_Acc_Entity IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Entity
                                            Dim listDirector = objDb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = item.PK_Trn_Par_acc_Entity_ID).ToList
                                            If listDirector IsNot Nothing Then .list_goAML_Trn_Par_Acc_Ent_Director.AddRange(listDirector)
                                        Next
                                    End If

                                    'Account Entity Director Address, Phone, Identification
                                    If .list_goAML_Trn_Par_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Trn_Par_Acc_Ent_Director.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Acc_Ent_Director
                                            Dim listAddress = objDb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                            Dim listPhone = objDb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = item.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_par_Acc_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_trn_par_acc_Entity_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Person
                                    Dim listPerson = objDb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listPerson IsNot Nothing Then .list_goAML_Trn_Party_Person.AddRange(listPerson)

                                    'Person Address, Phone, Identification
                                    If .list_goAML_Trn_Party_Person IsNot Nothing AndAlso .list_goAML_Trn_Party_Person.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Person
                                            Dim listAddress = objDb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = item.PK_Trn_Party_Person_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = item.PK_Trn_Party_Person_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Party_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Person_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Person_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                    'Entity
                                    Dim listEntity = objDb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = party.PK_Trn_Party_ID).ToList
                                    If listEntity IsNot Nothing Then .list_goAML_Trn_Party_Entity.AddRange(listEntity)

                                    'Entity Address, Phone
                                    If .list_goAML_Trn_Party_Entity IsNot Nothing AndAlso .list_goAML_Trn_Party_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Entity
                                            Dim listAddress = objDb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Party_Entity_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Party_Entity_Phone.AddRange(listPhone)
                                        Next
                                    End If

                                    'Entity Director
                                    If .list_goAML_Trn_Party_Entity IsNot Nothing AndAlso .list_goAML_Trn_Party_Entity.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Party_Entity
                                            Dim listDirector = objDb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = item.PK_Trn_Party_Entity_ID).ToList
                                            .list_goAML_Trn_Par_Entity_Director.AddRange(listDirector)
                                        Next
                                    End If

                                    'Entity Director Address, Phone, Identification
                                    If .list_goAML_Trn_Par_Entity_Director IsNot Nothing AndAlso .list_goAML_Trn_Par_Entity_Director.Count > 0 Then
                                        For Each item In .list_goAML_Trn_Par_Entity_Director
                                            Dim listAddress = objDb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = item.PK_Trn_Par_Entity_Director_ID).ToList
                                            Dim listPhone = objDb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = item.PK_Trn_Par_Entity_Director_ID).ToList
                                            Dim listIdentification = objDb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = item.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                                            If listAddress IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Address.AddRange(listAddress)
                                            If listPhone IsNot Nothing Then .list_goAML_Trn_Par_Entity_Director_Phone.AddRange(listPhone)
                                            If listIdentification IsNot Nothing Then .list_goAML_Transaction_Party_Identification.AddRange(listIdentification)
                                        Next
                                    End If

                                Next
                            End If
                        End If
                    End If
                End If
            End With

            Return objGoAMLTransactionClass
        End Using
    End Function

    Shared Function GetGoAMLActivityClassByID(ID As Long) As GoAML_Activity_Class
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objGoAMLActivityClass = New GoAML_Activity_Class
            With objGoAMLActivityClass
                'GoAML Activity
                .obj_goAML_Act_ReportPartyType = objDb.goAML_Act_ReportPartyType.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = ID).FirstOrDefault

                If .obj_goAML_Act_ReportPartyType IsNot Nothing Then
                    Dim objActivity = .obj_goAML_Act_ReportPartyType

                    'Account
                    Dim listAccount = objDb.goAML_Act_Account.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listAccount IsNot Nothing Then .list_goAML_Act_Account.AddRange(listAccount)

                    'Signatory
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listSignatory = objDb.goAML_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listSignatory IsNot Nothing Then .list_goAML_Act_acc_Signatory.AddRange(listSignatory)
                        Next
                    End If

                    'Signatory Address, Phone, Identification
                    If .list_goAML_Act_acc_Signatory IsNot Nothing AndAlso .list_goAML_Act_acc_Signatory.Count > 0 Then
                        For Each item In .list_goAML_Act_acc_Signatory
                            Dim listAddress = objDb.goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_acc_Signatory_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_acc_Signatory_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Signatory).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_sign_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_sign_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Account Entity
                    If .list_goAML_Act_Account IsNot Nothing AndAlso .list_goAML_Act_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Account
                            Dim listAccountEntity = objDb.goAML_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = item.PK_goAML_Act_Account_ID).ToList
                            If listAccountEntity IsNot Nothing Then .list_goAML_Act_Entity_Account.AddRange(listAccountEntity)
                        Next
                    End If

                    'Account Entity Address, Phone
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Account Entity Director
                    If .list_goAML_Act_Entity_Account IsNot Nothing AndAlso .list_goAML_Act_Entity_Account.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity_Account
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_account).ToList
                            If listDirector IsNot Nothing Then .list_goAML_Act_Acc_Ent_Director.AddRange(listDirector)
                        Next
                    End If

                    'Account Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Acc_Ent_Director IsNot Nothing AndAlso .list_goAML_Act_Acc_Ent_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Acc_Ent_Director
                            Dim listAddress = objDb.goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = item.PK_Act_Acc_Ent_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_Act_Acc_Ent_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorAccount).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Acc_Entity_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Person
                    Dim listPerson = objDb.goAML_Act_Person.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listPerson IsNot Nothing Then .list_goAML_Act_Person.AddRange(listPerson)

                    'Person Address, Phone, Identification
                    If .list_goAML_Act_Person IsNot Nothing AndAlso .list_goAML_Act_Person.Count > 0 Then
                        For Each item In .list_goAML_Act_Person
                            Dim listAddress = objDb.goAML_Act_Person_Address.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = item.PK_goAML_Act_Person_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Person_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.Person).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Person_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Person_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If

                    'Entity
                    Dim listEntity = objDb.goAML_Act_Entity.Where(Function(x) x.FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID).ToList
                    If listEntity IsNot Nothing Then .list_goAML_Act_Entity.AddRange(listEntity)

                    'Entity Address, Phone
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listAddress = objDb.goAML_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Entity_Phone.AddRange(listPhone)
                        Next
                    End If

                    'Entity Director
                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            Dim listDirector = objDb.goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = item.PK_goAML_Act_Entity_ID).ToList
                            .list_goAML_Act_Director.AddRange(listDirector)
                        Next
                    End If

                    'Entity Director Address, Phone, Identification
                    If .list_goAML_Act_Director IsNot Nothing AndAlso .list_goAML_Act_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Director
                            Dim listAddress = objDb.goAML_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listPhone = objDb.goAML_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = item.PK_goAML_Act_Director_ID).ToList
                            Dim listIdentification = objDb.goAML_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = item.PK_goAML_Act_Director_ID And x.FK_Person_Type = GoAML_Report_BLL.enumIdentificationPersonType.DirectorEntity).ToList

                            If listAddress IsNot Nothing Then .list_goAML_Act_Director_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                        Next
                    End If
                End If
            End With

            Return objGoAMLActivityClass
        End Using
    End Function

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As GoAML_Report_Class)
        Try
            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_Report_ID)

            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using
            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransaction(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            If objData.obj_goAML_Transaction.PK_Transaction_ID < 0 Then
                SaveTransactionAdd(objModule, objData, strReportID)
            Else
                SaveTransactionEdit(objModule, objData, strReportID)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransactionAdd(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            ''Get Old Data for Audit Trail
            'Dim objData_Old = GetGoAMLTransactionClassByID(objData.obj_goAML_Transaction.PK_Transaction_ID)

            ''Define local variables
            'Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            'Dim strAlternateID As String = Nothing
            'Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            'Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            'Dim strModuleName As String = objModule.ModuleLabel

            'If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
            '    strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            'End If

            'Using objDB As New NawaDevDAL.NawaDatadevEntities
            '    Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
            '        Try
            '            'Additional Information for Header
            '            With objData.obj_goAML_Transaction
            '                .Alternateby = strAlternateID
            '            End With

            '            'Save objData
            '            objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Modified
            '            objDB.SaveChanges()

            '            objTrans.Commit()
            '        Catch ex As Exception
            '            objTrans.Rollback()
            '            Throw ex
            '        End Try
            '    End Using

            '    '09-Sep-2021 : Save data by XML
            '    'Declare datatable untuk menampung PK inserted (jika dibutuhkan)
            '    Dim dtPK As DataTable = Nothing
            '    Dim dr() As DataRow
            '    Dim intParentPK As Long = 0

            '    '===================== NEW ADDED/EDITED DATA
            '    'Insert ke EODLogSP untuk ukur performance
            '    Dim strQuery As String
            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data started')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '    'Save ACCOUNT
            '    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
            '        With item
            '            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            '        End With
            '    Next
            '    dtPK = SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ACCOUNT", objData.objList_SIPENDAR_PROFILE_ACCOUNT, 1)

            '    'Save ACCOUNT ATM
            '    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
            '        If dtPK IsNot Nothing Then
            '            intParentPK = item.FK_SIPENDAR_PROFILE_ACCOUNT_ID
            '            dr = dtPK.Select("pkCurrent=" & item.FK_SIPENDAR_PROFILE_ACCOUNT_ID)
            '            If dr.Length > 0 Then
            '                intParentPK = dr(0)("pkInserted")
            '            End If
            '        End If

            '        With item
            '            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            '            .FK_SIPENDAR_PROFILE_ACCOUNT_ID = intParentPK
            '        End With
            '    Next
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ACCOUNT_ATM", objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM, 1)

            '    'Save Address
            '    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
            '        With item
            '            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            '        End With
            '    Next
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ADDRESS", objData.objList_SIPENDAR_PROFILE_ADDRESS, 1)

            '    'Save PHONE
            '    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
            '        With item
            '            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            '        End With
            '    Next
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_PHONE", objData.objList_SIPENDAR_PROFILE_PHONE, 1)

            '    'Save Identity
            '    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
            '        With item
            '            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            '        End With
            '    Next
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_IDENTIFICATION", objData.objList_SIPENDAR_PROFILE_IDENTIFICATION, 1)

            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Add/Edit Data finished')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            '    '===================== END OF NEW ADDED/EDITED DATA


            '    '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Deleted Data started')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '    Dim listObjSIPENDAR_PROFILE_ACCOUNT_Delete As New List(Of SIPENDAR_PROFILE_ACCOUNT)
            '    Dim listObjSIPENDAR_PROFILE_ACCOUNT_ATM_Delete As New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
            '    Dim listObjSIPENDAR_PROFILE_ADDRESS_Delete As New List(Of SIPENDAR_PROFILE_ADDRESS)
            '    Dim listObjSIPENDAR_PROFILE_PHONE_Delete As New List(Of SIPENDAR_PROFILE_PHONE)
            '    Dim listObjSIPENDAR_PROFILE_IDENTIFICATION_Delete As New List(Of SIPENDAR_PROFILE_IDENTIFICATION)

            '    '_Deleted ACCOUNT
            '    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
            '        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
            '        If objCek Is Nothing Then
            '            listObjSIPENDAR_PROFILE_ACCOUNT_Delete.Add(item_old)
            '        End If
            '    Next

            '    '_Deleted ACCOUNT ATM
            '    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
            '        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
            '        If objCek Is Nothing Then
            '            listObjSIPENDAR_PROFILE_ACCOUNT_ATM_Delete.Add(item_old)
            '        End If
            '    Next

            '    '_Deleted Address
            '    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
            '        Dim objCek = objData.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item_old.PK_SIPENDAR_PROFILE_ADDRESS_ID)
            '        If objCek Is Nothing Then
            '            listObjSIPENDAR_PROFILE_ADDRESS_Delete.Add(item_old)
            '        End If
            '    Next

            '    '_Deleted PHONE
            '    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_PHONE
            '        Dim objCek = objData.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item_old.PK_SIPENDAR_PROFILE_PHONE_ID)
            '        If objCek Is Nothing Then
            '            listObjSIPENDAR_PROFILE_PHONE_Delete.Add(item_old)
            '        End If
            '    Next

            '    '_Deleted Identity
            '    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
            '        Dim objCek = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item_old.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
            '        If objCek Is Nothing Then
            '            listObjSIPENDAR_PROFILE_IDENTIFICATION_Delete.Add(item_old)
            '        End If
            '    Next

            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ACCOUNT", listObjSIPENDAR_PROFILE_ACCOUNT_Delete, 2)
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ACCOUNT_ATM", listObjSIPENDAR_PROFILE_ACCOUNT_ATM_Delete, 2)
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_ADDRESS", listObjSIPENDAR_PROFILE_ADDRESS_Delete, 2)
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_PHONE", listObjSIPENDAR_PROFILE_PHONE_Delete, 2)
            '    SiPendarBLL.NawaFramework.SaveDataWithXML("SIPENDAR_PROFILE_IDENTIFICATION", listObjSIPENDAR_PROFILE_IDENTIFICATION_Delete, 2)

            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Deleted Data finished')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            '    '===================== END OF _DeleteD DATA


            '    'Audit Trail Header
            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Audit Trail started')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            '    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            '    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE, objData_Old.objSIPENDAR_PROFILE)

            '    '01-Sep-2021 Adi : Save Audit Trail Detail by XML
            '    'Dim listObjSIPENDAR_PROFILE_ACCOUNT As List(Of SIPENDAR_PROFILE_ACCOUNT) = objData.objList_SIPENDAR_PROFILE_ACCOUNT
            '    'Dim listObjSIPENDAR_PROFILE_ACCOUNT_ATM As List(Of SIPENDAR_PROFILE_ACCOUNT_ATM) = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
            '    'Dim listObjSIPENDAR_PROFILE_ADDRESS As List(Of SIPENDAR_PROFILE_ADDRESS) = objData.objList_SIPENDAR_PROFILE_ADDRESS
            '    'Dim listObjSIPENDAR_PROFILE_PHONE As List(Of SIPENDAR_PROFILE_PHONE) = objData.objList_SIPENDAR_PROFILE_PHONE
            '    'Dim listObjSIPENDAR_PROFILE_IDENTIFICATION As List(Of SIPENDAR_PROFILE_IDENTIFICATION) = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION

            '    'Dim listObjSIPENDAR_PROFILE_ACCOUNT_Old As List(Of SIPENDAR_PROFILE_ACCOUNT) = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
            '    'Dim listObjSIPENDAR_PROFILE_ACCOUNT_ATM_Old As List(Of SIPENDAR_PROFILE_ACCOUNT_ATM) = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
            '    'Dim listObjSIPENDAR_PROFILE_ADDRESS_Old As List(Of SIPENDAR_PROFILE_ADDRESS) = objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
            '    'Dim listObjSIPENDAR_PROFILE_PHONE_Old As List(Of SIPENDAR_PROFILE_PHONE) = objData_Old.objList_SIPENDAR_PROFILE_PHONE
            '    'Dim listObjSIPENDAR_PROFILE_IDENTIFICATION_Old As List(Of SIPENDAR_PROFILE_IDENTIFICATION) = objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION

            '    SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT", objData.objList_SIPENDAR_PROFILE_ACCOUNT, objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT)
            '    SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT_ATM", objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM, objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM)
            '    SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ADDRESS", objData.objList_SIPENDAR_PROFILE_ADDRESS, objData_Old.objList_SIPENDAR_PROFILE_ADDRESS)
            '    SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_PHONE", objData.objList_SIPENDAR_PROFILE_PHONE, objData_Old.objList_SIPENDAR_PROFILE_PHONE)
            '    SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_IDENTIFICATION", objData.objList_SIPENDAR_PROFILE_IDENTIFICATION, objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION)

            '    'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT", listObjSIPENDAR_PROFILE_ACCOUNT, listObjSIPENDAR_PROFILE_ACCOUNT_Old)
            '    'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT_ATM", listObjSIPENDAR_PROFILE_ACCOUNT_ATM, listObjSIPENDAR_PROFILE_ACCOUNT_ATM_Old)
            '    'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ADDRESS", listObjSIPENDAR_PROFILE_ADDRESS, listObjSIPENDAR_PROFILE_ADDRESS_Old)
            '    'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_PHONE", listObjSIPENDAR_PROFILE_PHONE, listObjSIPENDAR_PROFILE_PHONE_Old)
            '    'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_IDENTIFICATION", listObjSIPENDAR_PROFILE_IDENTIFICATION, listObjSIPENDAR_PROFILE_IDENTIFICATION_Old)

            '    strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Audit Trail finished')"
            '    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            '    'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
            'End Using

            ''Jalankan validasi satuan untuk replace IsValid dan Error Message
            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
            'param(0).Value = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            'param(0).DbType = SqlDbType.BigInt
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveTransactionEdit(objModule As NawaDAL.Module, objData As GoAML_Transaction_Class, strReportID As String)
        Try
            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLTransactionClassByID(objData.obj_goAML_Transaction.PK_Transaction_ID)

            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            Using objDB As New NawaDevDAL.NawaDatadevEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Transaction
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Transaction).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                '09-Sep-2021 : Save data by XML
                'Declare datatable untuk menampung PK inserted (jika dibutuhkan)
                Dim dtPK As DataTable = Nothing
                Dim dr() As DataRow
                Dim intParentPK As Long = 0

                '===================== NEW ADDED/EDITED DATA
                'Insert ke EODLogSP untuk ukur performance
                Dim strQuery As String
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                If objData.obj_goAML_Transaction.FK_Transaction_Type = 1 Then       'Transaction Bi-Party
                    '1. Account
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Transaction_Account", objData.list_goAML_Transaction_Account, 1)

                    'Account Signatory
                    Dim listAccountSignatoryIdentification As New List(Of goAML_Transaction_Person_Identification)
                    For Each item In objData.list_goAML_Trn_acc_Signatory
                        item.FK_Transaction_Account_ID = getParentID(dtPK, item.FK_Transaction_Account_ID)

                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Signatory).ToList
                        If listIdentification IsNot Nothing Then
                            listAccountSignatoryIdentification.AddRange(listIdentification)
                        End If
                    Next
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Trn_acc_Signatory", objData.list_goAML_Trn_acc_Signatory, 1)

                    'Account Signatory Address
                    For Each item In objData.list_goAML_Trn_Acc_sign_Address
                        item.FK_Trn_Acc_Entity = getParentID(dtPK, item.FK_Trn_Acc_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Acc_sign_Address", objData.list_goAML_Trn_Acc_sign_Address, 1)

                    'Account Signatory Phone
                    For Each item In objData.list_goAML_trn_acc_sign_Phone
                        item.FK_Trn_Acc_Entity = getParentID(dtPK, item.FK_Trn_Acc_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_trn_acc_sign_Phone", objData.list_goAML_trn_acc_sign_Phone, 1)

                    'Account Signatory Identification
                    For Each item In listAccountSignatoryIdentification
                        item.FK_Person_ID = getParentID(dtPK, item.FK_Person_ID)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", listAccountSignatoryIdentification, 1)

                    'Account Entity
                    For Each item In objData.list_goAML_Trn_Entity_account
                        item.FK_Account_ID = getParentID(dtPK, item.FK_Account_ID)
                    Next
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Trn_Entity_account", objData.list_goAML_Trn_Entity_account, 1)

                    'Account Entity Address
                    For Each item In objData.list_goAML_Trn_Acc_Entity_Address
                        item.FK_Trn_Acc_Entity = getParentID(dtPK, item.FK_Trn_Acc_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Acc_Entity_Address", objData.list_goAML_Trn_Acc_Entity_Address, 1)

                    'Account Entity Phone
                    For Each item In objData.list_goAML_Trn_Acc_Entity_Phone
                        item.FK_Trn_Acc_Entity = getParentID(dtPK, item.FK_Trn_Acc_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Acc_Entity_Phone", objData.list_goAML_Trn_Acc_Entity_Phone, 1)

                    'Account Entity Director (Hati2 karena objectnya sama dengan Entity Director biasa)
                    Dim listAccountEntityDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Account).ToList
                    Dim listAccountEntityDirectorAddress = New List(Of goAML_Trn_Director_Address)
                    Dim listAccountEntityDirectorPhone = New List(Of goAML_trn_Director_Phone)
                    Dim listAccountEntityDirectorIdentification As New List(Of goAML_Transaction_Person_Identification)
                    If listAccountEntityDirector IsNot Nothing Then
                        For Each item In listAccountEntityDirector
                            item.FK_Entity_ID = getParentID(dtPK, item.FK_Entity_ID)

                            Dim listAddressCheck = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                            If listAddressCheck IsNot Nothing Then
                                listAccountEntityDirectorAddress.AddRange(listAddressCheck)
                            End If

                            Dim listPhoneCheck = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                            If listPhoneCheck IsNot Nothing Then
                                listAccountEntityDirectorPhone.AddRange(listPhoneCheck)
                            End If

                            Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount).ToList
                            If listIdentification IsNot Nothing Then
                                listAccountEntityDirectorIdentification.AddRange(listIdentification)
                            End If
                        Next
                        dtPK = NawaFramework.SaveDataWithXML("goAML_Trn_Director", listAccountEntityDirector, 1)

                        'Account Entity Director Address
                        For Each item In listAccountEntityDirectorAddress
                            item.FK_Trn_Director_ID = getParentID(dtPK, item.FK_Trn_Director_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Trn_Director_Address", listAccountEntityDirectorAddress, 1)

                        'Account Entity Director Phone
                        For Each item In listAccountEntityDirectorPhone
                            item.FK_Trn_Director_ID = getParentID(dtPK, item.FK_Trn_Director_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Trn_Director_Phone", listAccountEntityDirectorPhone, 1)

                        'Account Entity Director Identification
                        For Each item In listAccountEntityDirectorIdentification
                            item.FK_Person_ID = getParentID(dtPK, item.FK_Person_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", listAccountEntityDirectorIdentification, 1)
                    End If

                    '2. Person
                    Dim listPersonIdentification As New List(Of goAML_Transaction_Person_Identification)
                    For Each item In objData.list_goAML_Trn_Person_Address
                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Person).ToList
                        If listIdentification IsNot Nothing Then
                            listPersonIdentification.AddRange(listIdentification)
                        End If
                    Next
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Transaction_Person", objData.list_goAML_Transaction_Person, 1)

                    'Person Address
                    For Each item In objData.list_goAML_Trn_Person_Address
                        item.FK_Trn_Person = getParentID(dtPK, item.FK_Trn_Person)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Person_Address", objData.list_goAML_Trn_Person_Address, 1)

                    'Person Phone
                    For Each item In objData.list_goAML_trn_Person_Phone
                        item.FK_Trn_Person = getParentID(dtPK, item.FK_Trn_Person)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Person_Phone", objData.list_goAML_trn_Person_Phone, 1)

                    'Person Identification
                    For Each item In listPersonIdentification
                        item.FK_Person_ID = getParentID(dtPK, item.FK_Person_ID)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", listPersonIdentification, 1)

                    '3. Entity
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Transaction_Entity", objData.list_goAML_Transaction_Entity, 1)

                    'Entity Address
                    For Each item In objData.list_goAML_Trn_Entity_Address
                        item.FK_Trn_Entity = getParentID(dtPK, item.FK_Trn_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Entity_Address", objData.list_goAML_Trn_Entity_Address, 1)

                    'Entity Phone
                    For Each item In objData.list_goAML_Trn_Entity_Phone
                        item.FK_Trn_Entity = getParentID(dtPK, item.FK_Trn_Entity)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Entity_Phone", objData.list_goAML_Trn_Entity_Phone, 1)

                    'Account Entity Director (Hati2 karena objectnya sama dengan Entity Director biasa)
                    Dim listEntityDirector = objData.list_goAML_Trn_Director.Where(Function(x) x.FK_Sender_Information = enumSenderInformation.Entity).ToList
                    Dim listEntityDirectorAddress = New List(Of goAML_Trn_Director_Address)
                    Dim listEntityDirectorPhone = New List(Of goAML_trn_Director_Phone)
                    Dim listEntityDirectorIdentification As New List(Of goAML_Transaction_Person_Identification)
                    If listEntityDirector IsNot Nothing Then
                        For Each item In listEntityDirector
                            item.FK_Entity_ID = getParentID(dtPK, item.FK_Entity_ID)

                            Dim listAddressCheck = objData.list_goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                            If listAddressCheck IsNot Nothing Then
                                listEntityDirectorAddress.AddRange(listAddressCheck)
                            End If

                            Dim listPhoneCheck = objData.list_goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = item.PK_goAML_Trn_Director_ID).ToList
                            If listPhoneCheck IsNot Nothing Then
                                listEntityDirectorPhone.AddRange(listPhoneCheck)
                            End If

                            Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorEntity).ToList
                            If listIdentification IsNot Nothing Then
                                listEntityDirectorIdentification.AddRange(listIdentification)
                            End If
                        Next
                        dtPK = NawaFramework.SaveDataWithXML("goAML_Trn_Director", listEntityDirector, 1)

                        'Account Entity Director Address
                        For Each item In listEntityDirectorAddress
                            item.FK_Trn_Director_ID = getParentID(dtPK, item.FK_Trn_Director_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Trn_Director_Address", listEntityDirectorAddress, 1)

                        'Account Entity Director Phone
                        For Each item In listEntityDirectorPhone
                            item.FK_Trn_Director_ID = getParentID(dtPK, item.FK_Trn_Director_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Trn_Director_Phone", listEntityDirectorPhone, 1)

                        'Account Entity Director Identification
                        For Each item In listEntityDirectorIdentification
                            item.FK_Person_ID = getParentID(dtPK, item.FK_Person_ID)
                        Next
                        NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", listEntityDirectorIdentification, 1)
                    End If

                    '4. Conductor
                    Dim listConductorIdentification As New List(Of goAML_Transaction_Person_Identification)
                    For Each item In objData.list_goAML_Trn_Conductor_Address
                        Dim listIdentification = objData.list_goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.Conductor).ToList
                        If listIdentification IsNot Nothing Then
                            listConductorIdentification.AddRange(listIdentification)
                        End If
                    Next
                    dtPK = NawaFramework.SaveDataWithXML("goAML_Trn_Conductor", objData.list_goAML_Trn_Conductor, 1)

                    'Conductor Address
                    For Each item In objData.list_goAML_Trn_Conductor_Address
                        item.FK_Trn_Conductor_ID = getParentID(dtPK, item.FK_Trn_Conductor_ID)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Conductor_Address", objData.list_goAML_Trn_Conductor_Address, 1)

                    'Conductor Phone
                    For Each item In objData.list_goAML_trn_Conductor_Phone
                        item.FK_Trn_Conductor_ID = getParentID(dtPK, item.FK_Trn_Conductor_ID)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Trn_Conductor_Phone", objData.list_goAML_trn_Conductor_Phone, 1)

                    'Conductor Identification
                    For Each item In listConductorIdentification
                        item.FK_Person_ID = getParentID(dtPK, item.FK_Person_ID)
                    Next
                    NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", listConductorIdentification, 1)
                Else    'Transaction Multi-Party

                    ''Transaction Multi Party
                    'Public list_goAML_Transaction_Party As New List(Of NawaDevDAL.goAML_Transaction_Party)

                    'Public list_goAML_Trn_Party_Account As New List(Of NawaDevDAL.goAML_Trn_Party_Account)
                    'Public list_goAML_Trn_par_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
                    'Public list_goAML_Trn_par_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
                    'Public list_goAML_trn_par_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)

                    'Public list_goAML_Trn_Par_Acc_Entity As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)
                    'Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
                    'Public list_goAML_trn_par_acc_Entity_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)

                    'Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
                    'Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
                    'Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

                    'Public list_goAML_Trn_Party_Person As New List(Of NawaDevDAL.goAML_Trn_Party_Person)
                    'Public list_goAML_Trn_Party_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
                    'Public list_goAML_Trn_Party_Person_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)

                    'Public list_goAML_Trn_Party_Entity As New List(Of NawaDevDAL.goAML_Trn_Party_Entity)
                    'Public list_goAML_Trn_Party_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
                    'Public list_goAML_Trn_Party_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)

                    'Public list_goAML_Trn_Par_Entity_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
                    'Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
                    'Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)

                    'Public list_goAML_Transaction_Party_Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)

                End If

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Add/Edit Data finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                '===================== END OF NEW ADDED/EDITED DATA

                '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                'Create new object for deleted data
                Dim objDeletedData As New GoAML_Transaction_Class

                '1. Account
                For Each item_old In objData_Old.list_goAML_Transaction_Account
                    Dim objCek = objData.list_goAML_Transaction_Account.Find(Function(x) x.PK_Account_ID = item_old.PK_Account_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Account.Add(item_old)
                    End If
                Next

                'Account Signatory
                For Each item_old In objData_Old.list_goAML_Trn_acc_Signatory
                    Dim objCek = objData.list_goAML_Trn_acc_Signatory.Find(Function(x) x.PK_goAML_Trn_acc_Signatory_ID = item_old.PK_goAML_Trn_acc_Signatory_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_acc_Signatory.Add(item_old)
                    End If
                Next

                'Account Signatory Address
                For Each item_old In objData_Old.list_goAML_Trn_Acc_sign_Address
                    Dim objCek = objData.list_goAML_Trn_Acc_sign_Address.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = item_old.PK_goAML_Trn_Acc_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Acc_sign_Address.Add(item_old)
                    End If
                Next

                'Account Signatory Phone
                For Each item_old In objData_Old.list_goAML_trn_acc_sign_Phone
                    Dim objCek = objData.list_goAML_trn_acc_sign_Phone.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = item_old.PK_goAML_trn_acc_sign_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_acc_sign_Phone.Add(item_old)
                    End If
                Next

                'Account Entity
                For Each item_old In objData_Old.list_goAML_Trn_Entity_account
                    Dim objCek = objData.list_goAML_Trn_Entity_account.Find(Function(x) x.PK_goAML_Trn_Entity_account = item_old.PK_goAML_Trn_Entity_account)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Entity_account.Add(item_old)
                    End If
                Next

                'Account Entity Address
                For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Address
                    Dim objCek = objData.list_goAML_Trn_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Acc_Entity_Address_ID = item_old.PK_Trn_Acc_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Acc_Entity_Address.Add(item_old)
                    End If
                Next

                'Account Entity Phone
                For Each item_old In objData_Old.list_goAML_Trn_Acc_Entity_Phone
                    Dim objCek = objData.list_goAML_Trn_Acc_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone = item_old.PK_goAML_Trn_Acc_Entity_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Acc_Entity_Phone.Add(item_old)
                    End If
                Next

                '2. Person
                For Each item_old In objData_Old.list_goAML_Transaction_Person
                    Dim objCek = objData.list_goAML_Transaction_Person.Find(Function(x) x.PK_Person_ID = item_old.PK_Person_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Person.Add(item_old)
                    End If
                Next

                'Person Address
                For Each item_old In objData_Old.list_goAML_Trn_Person_Address
                    Dim objCek = objData.list_goAML_Trn_Person_Address.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = item_old.PK_goAML_Trn_Person_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Person_Address.Add(item_old)
                    End If
                Next

                'Person Phone
                For Each item_old In objData_Old.list_goAML_trn_Person_Phone
                    Dim objCek = objData.list_goAML_trn_Person_Phone.Find(Function(x) x.PK_goAML_trn_Person_Phone = item_old.PK_goAML_trn_Person_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_Person_Phone.Add(item_old)
                    End If
                Next

                '3. Entity
                For Each item_old In objData_Old.list_goAML_Transaction_Entity
                    Dim objCek = objData.list_goAML_Transaction_Entity.Find(Function(x) x.PK_Entity_ID = item_old.PK_Entity_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Entity.Add(item_old)
                    End If
                Next

                'Entity Address
                For Each item_old In objData_Old.list_goAML_Trn_Entity_Address
                    Dim objCek = objData.list_goAML_Trn_Entity_Address.Find(Function(x) x.PK_goAML_Trn_Entity_Address_ID = item_old.PK_goAML_Trn_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Entity_Address.Add(item_old)
                    End If
                Next

                'Entity Phone
                For Each item_old In objData_Old.list_goAML_Trn_Entity_Phone
                    Dim objCek = objData.list_goAML_Trn_Entity_Phone.Find(Function(x) x.PK_goAML_Trn_Entity_Phone = item_old.PK_goAML_Trn_Entity_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Entity_Phone.Add(item_old)
                    End If
                Next

                'Account/Entity Director
                For Each item_old In objData_Old.list_goAML_Trn_Director
                    Dim objCek = objData.list_goAML_Trn_Director.Find(Function(x) x.PK_goAML_Trn_Director_ID = item_old.PK_goAML_Trn_Director_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Director.Add(item_old)
                    End If
                Next

                'Account/Entity Director Address
                For Each item_old In objData_Old.list_goAML_Trn_Director_Address
                    Dim objCek = objData.list_goAML_Trn_Director_Address.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = item_old.PK_goAML_Trn_Director_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Director_Address.Add(item_old)
                    End If
                Next

                'Account/Entity Director Phone
                For Each item_old In objData_Old.list_goAML_trn_Director_Phone
                    Dim objCek = objData.list_goAML_trn_Director_Phone.Find(Function(x) x.PK_goAML_trn_Director_Phone = item_old.PK_goAML_trn_Director_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_Director_Phone.Add(item_old)
                    End If
                Next

                '4. Conductor
                For Each item_old In objData_Old.list_goAML_Trn_Conductor
                    Dim objCek = objData.list_goAML_Trn_Conductor.Find(Function(x) x.PK_goAML_Trn_Conductor_ID = item_old.PK_goAML_Trn_Conductor_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Conductor.Add(item_old)
                    End If
                Next

                'Conductor Address
                For Each item_old In objData_Old.list_goAML_Trn_Conductor_Address
                    Dim objCek = objData.list_goAML_Trn_Conductor_Address.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = item_old.PK_goAML_Trn_Conductor_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Conductor_Address.Add(item_old)
                    End If
                Next

                'Conductor Phone
                For Each item_old In objData_Old.list_goAML_trn_Conductor_Phone
                    Dim objCek = objData.list_goAML_trn_Conductor_Phone.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = item_old.PK_goAML_trn_Conductor_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_Conductor_Phone.Add(item_old)
                    End If
                Next

                'Identification
                For Each item_old In objData_Old.list_goAML_Transaction_Person_Identification
                    Dim objCek = objData.list_goAML_Transaction_Person_Identification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = item_old.PK_goAML_Transaction_Person_Identification_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Person_Identification.Add(item_old)
                    End If
                Next

                'Transaction Multi Party
                '0. List TParty
                For Each item_old In objData_Old.list_goAML_Transaction_Party
                    Dim objCek = objData.list_goAML_Transaction_Party.Find(Function(x) x.PK_Trn_Party_ID = item_old.PK_Trn_Party_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Party.Add(item_old)
                    End If
                Next

                '1. Account
                For Each item_old In objData_Old.list_goAML_Trn_Party_Account
                    Dim objCek = objData.list_goAML_Trn_Party_Account.Find(Function(x) x.PK_Trn_Party_Account_ID = item_old.PK_Trn_Party_Account_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Account.Add(item_old)
                    End If
                Next

                'Account Signatory
                For Each item_old In objData_Old.list_goAML_Trn_par_acc_Signatory
                    Dim objCek = objData.list_goAML_Trn_par_acc_Signatory.Find(Function(x) x.PK_Trn_par_acc_Signatory_ID = item_old.PK_Trn_par_acc_Signatory_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_par_acc_Signatory.Add(item_old)
                    End If
                Next

                'Account Signatory Address
                For Each item_old In objData_Old.list_goAML_Trn_par_Acc_sign_Address
                    Dim objCek = objData.list_goAML_Trn_par_Acc_sign_Address.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = item_old.PK_Trn_Par_Acc_sign_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_par_Acc_sign_Address.Add(item_old)
                    End If
                Next

                'Account Signatory Phone
                For Each item_old In objData_Old.list_goAML_trn_par_acc_sign_Phone
                    Dim objCek = objData.list_goAML_trn_par_acc_sign_Phone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = item_old.PK_trn_Par_Acc_Sign_Phone)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_par_acc_sign_Phone.Add(item_old)
                    End If
                Next

                'Account Entity
                For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Entity
                    Dim objCek = objData.list_goAML_Trn_Par_Acc_Entity.Find(Function(x) x.PK_Trn_Par_acc_Entity_ID = item_old.PK_Trn_Par_acc_Entity_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Acc_Entity.Add(item_old)
                    End If
                Next

                'Account Entity Address
                For Each item_old In objData_Old.list_goAML_Trn_par_Acc_Entity_Address
                    Dim objCek = objData.list_goAML_Trn_par_Acc_Entity_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_par_Acc_Entity_Address.Add(item_old)
                    End If
                Next

                'Account Entity Phone
                For Each item_old In objData_Old.list_goAML_trn_par_acc_Entity_Phone
                    Dim objCek = objData.list_goAML_trn_par_acc_Entity_Phone.Find(Function(x) x.PK_trn_Par_Acc_Entity_Phone_ID = item_old.PK_trn_Par_Acc_Entity_Phone_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_trn_par_acc_Entity_Phone.Add(item_old)
                    End If
                Next

                'Account Entity Director
                For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director
                    Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_ID = item_old.PK_Trn_Par_Acc_Ent_Director_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director.Add(item_old)
                    End If
                Next

                'Account Entity Director Address
                For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Address
                    Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Find(Function(x) x.PK_Trn_Par_Acc_Entity_Address_ID = item_old.PK_Trn_Par_Acc_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director_Address.Add(item_old)
                    End If
                Next

                'Account Entity Director Phone
                For Each item_old In objData_Old.list_goAML_Trn_Par_Acc_Ent_Director_Phone
                    Dim objCek = objData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Find(Function(x) x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = item_old.PK_Trn_Par_Acc_Ent_Director_Phone_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director_Phone.Add(item_old)
                    End If
                Next

                '2. Person
                For Each item_old In objData_Old.list_goAML_Trn_Party_Person
                    Dim objCek = objData.list_goAML_Trn_Party_Person.Find(Function(x) x.PK_Trn_Party_Person_ID = item_old.PK_Trn_Party_Person_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Person.Add(item_old)
                    End If
                Next

                'Person Address
                For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Address
                    Dim objCek = objData.list_goAML_Trn_Party_Person_Address.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = item_old.PK_Trn_Party_Person_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Person_Address.Add(item_old)
                    End If
                Next

                'Person Phone
                For Each item_old In objData_Old.list_goAML_Trn_Party_Person_Phone
                    Dim objCek = objData.list_goAML_Trn_Party_Person_Phone.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = item_old.PK_Trn_Party_Person_Phone_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Person_Phone.Add(item_old)
                    End If
                Next

                '3. Entity
                For Each item_old In objData_Old.list_goAML_Trn_Party_Entity
                    Dim objCek = objData.list_goAML_Trn_Party_Entity.Find(Function(x) x.PK_Trn_Party_Entity_ID = item_old.PK_Trn_Party_Entity_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Entity.Add(item_old)
                    End If
                Next

                'Entity Address
                For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Address
                    Dim objCek = objData.list_goAML_Trn_Party_Entity_Address.Find(Function(x) x.PK_Trn_Party_Entity_Address_ID = item_old.PK_Trn_Party_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Entity_Address.Add(item_old)
                    End If
                Next

                'Entity Phone
                For Each item_old In objData_Old.list_goAML_Trn_Party_Entity_Phone
                    Dim objCek = objData.list_goAML_Trn_Party_Entity_Phone.Find(Function(x) x.PK_Trn_Party_Entity_Phone_ID = item_old.PK_Trn_Party_Entity_Phone_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Party_Entity_Phone.Add(item_old)
                    End If
                Next

                'Entity Director
                For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director
                    Dim objCek = objData.list_goAML_Trn_Par_Entity_Director.Find(Function(x) x.PK_Trn_Par_Entity_Director_ID = item_old.PK_Trn_Par_Entity_Director_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Entity_Director.Add(item_old)
                    End If
                Next

                'Entity Director Address
                For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Address
                    Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Address.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = item_old.PK_Trn_Par_Entity_Address_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Entity_Director_Address.Add(item_old)
                    End If
                Next

                'Entity Director Phone
                For Each item_old In objData_Old.list_goAML_Trn_Par_Entity_Director_Phone
                    Dim objCek = objData.list_goAML_Trn_Par_Entity_Director_Phone.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = item_old.PK_Trn_Par_Entity_Director_Phone_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Trn_Par_Entity_Director_Phone.Add(item_old)
                    End If
                Next

                'Identification
                For Each item_old In objData_Old.list_goAML_Transaction_Party_Identification
                    Dim objCek = objData.list_goAML_Transaction_Party_Identification.Find(Function(x) x.PK_Transaction_Party_Identification_ID = item_old.PK_Transaction_Party_Identification_ID)
                    If objCek Is Nothing Then
                        objDeletedData.list_goAML_Transaction_Party_Identification.Add(item_old)
                    End If
                Next

                '=================== Delete data using SP
                'Transaction Bi-Party
                NawaFramework.SaveDataWithXML("goAML_Transaction_Account", objDeletedData.list_goAML_Transaction_Account, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_acc_Signatory", objDeletedData.list_goAML_Trn_acc_Signatory, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Acc_sign_Address", objDeletedData.list_goAML_Trn_Acc_sign_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_acc_sign_Phone", objDeletedData.list_goAML_trn_acc_sign_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Entity_account", objDeletedData.list_goAML_Trn_Entity_account, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Acc_Entity_Address", objDeletedData.list_goAML_Trn_Acc_Entity_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Acc_Entity_Phone", objDeletedData.list_goAML_Trn_Acc_Entity_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Transaction_Person", objDeletedData.list_goAML_Transaction_Person, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Person_Address", objDeletedData.list_goAML_Trn_Person_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_Person_Phone", objDeletedData.list_goAML_trn_Person_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Transaction_Entity", objDeletedData.list_goAML_Transaction_Entity, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Entity_Address", objDeletedData.list_goAML_Trn_Entity_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Entity_Phone", objDeletedData.list_goAML_Trn_Entity_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Director", objDeletedData.list_goAML_Trn_Director, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Director_Address", objDeletedData.list_goAML_Trn_Director_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_Director_Phone", objDeletedData.list_goAML_trn_Director_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Conductor", objDeletedData.list_goAML_Trn_Conductor, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Conductor_Address", objDeletedData.list_goAML_Trn_Conductor_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_Conductor_Phone", objDeletedData.list_goAML_trn_Conductor_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Transaction_Person_Identification", objDeletedData.list_goAML_Transaction_Person_Identification, 2)

                'Transaction Multi Party
                NawaFramework.SaveDataWithXML("goAML_Transaction_Party", objDeletedData.list_goAML_Transaction_Party, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Account", objDeletedData.list_goAML_Trn_Party_Account, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_par_acc_Signatory", objDeletedData.list_goAML_Trn_par_acc_Signatory, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_par_Acc_sign_Address", objDeletedData.list_goAML_Trn_par_Acc_sign_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_par_acc_sign_Phone", objDeletedData.list_goAML_trn_par_acc_sign_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Acc_Entity", objDeletedData.list_goAML_Trn_Par_Acc_Entity, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_par_Acc_Entity_Address", objDeletedData.list_goAML_Trn_par_Acc_Entity_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_trn_par_acc_Entity_Phone", objDeletedData.list_goAML_trn_par_acc_Entity_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Acc_Ent_Director", objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Acc_Ent_Director_Address", objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Acc_Ent_Director_Phone", objDeletedData.list_goAML_Trn_Par_Acc_Ent_Director_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Person", objDeletedData.list_goAML_Trn_Party_Person, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Person_Address", objDeletedData.list_goAML_Trn_Party_Person_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Person_Phone", objDeletedData.list_goAML_Trn_Party_Person_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Entity", objDeletedData.list_goAML_Trn_Party_Entity, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Entity_Address", objDeletedData.list_goAML_Trn_Party_Entity_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Party_Entity_Phone", objDeletedData.list_goAML_Trn_Party_Entity_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Entity_Director", objDeletedData.list_goAML_Trn_Par_Entity_Director, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Entity_Director_Address", objDeletedData.list_goAML_Trn_Par_Entity_Director_Address, 2)
                NawaFramework.SaveDataWithXML("goAML_Trn_Par_Entity_Director_Phone", objDeletedData.list_goAML_Trn_Par_Entity_Director_Phone, 2)

                NawaFramework.SaveDataWithXML("goAML_Transaction_Party_Identification", objDeletedData.list_goAML_Transaction_Party_Identification, 2)

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Deleted Data finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                '===================== END OF _DeleteD DATA


                'Audit Trail Header
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'GoAML Transaction', 'Save Audit Trail started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.obj_goAML_Transaction, objData_Old.obj_goAML_Transaction)

                'Save Audit Trail Detail by XML
                'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT", objData.objList_SIPENDAR_PROFILE_ACCOUNT, objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT)
                'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ACCOUNT_ATM", objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM, objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM)
                'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_ADDRESS", objData.objList_SIPENDAR_PROFILE_ADDRESS, objData_Old.objList_SIPENDAR_PROFILE_ADDRESS)
                'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_PHONE", objData.objList_SIPENDAR_PROFILE_PHONE, objData_Old.objList_SIPENDAR_PROFILE_PHONE)
                'SiPendarBLL.NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, "SIPENDAR_PROFILE_IDENTIFICATION", objData.objList_SIPENDAR_PROFILE_IDENTIFICATION, objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION)

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'Sipendar Profile', 'Save Audit Trail finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
            End Using

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            'Dim param(0) As SqlParameter
            'param(0) = New SqlParameter
            'param(0).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
            'param(0).Value = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
            'param(0).DbType = SqlDbType.BigInt
            'NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity(objModule As NawaDAL.Module, objData As GoAML_Activity_Class, strReportID As String)
        Try
            'If objData.obj_goAML_Act_ReportPartyType.PK_goAML_Act_ReportPartyType_ID < 0 Then
            '    SaveActivityAdd(objModule, objData, strReportID)
            'Else
            '    SaveActivityEdit(objModule, objData, strReportID)
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Shared Function getParentID(dtPK As DataTable, currentPK As String) As Integer
        Try
            Dim dr As DataRow()
            Dim intParentID As Integer = 0

            If dtPK IsNot Nothing Then
                dr = dtPK.Select("pkCurrent=" & currentPK)
                If dr.Length > 0 Then
                    intParentID = dr(0)("pkInserted")
                End If
            End If

            Return intParentID
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
    End Function
End Class

Public Class GoAML_Report_Class

    'GoAML Report
    Public obj_goAML_Report As New NawaDevDAL.goAML_Report

    'Transaction
    Public list_goAML_Transaction As New List(Of NawaDevDAL.goAML_Transaction)

    'Activity
    Public list_goAML_Act_ReportPartyType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)

    'Indicator
    Public list_goAML_Report_Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator)

End Class


Public Class GoAML_Transaction_Class
    'Transaction
    Public obj_goAML_Transaction As New goAML_Transaction

    'Transaction Bi-Party
    Public list_goAML_Transaction_Account As New List(Of NawaDevDAL.goAML_Transaction_Account)
    Public list_goAML_Trn_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
    Public list_goAML_Trn_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
    Public list_goAML_trn_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)

    Public list_goAML_Trn_Entity_account As New List(Of NawaDevDAL.goAML_Trn_Entity_account)
    Public list_goAML_Trn_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
    Public list_goAML_Trn_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)

    Public list_goAML_Transaction_Person As New List(Of NawaDevDAL.goAML_Transaction_Person)
    Public list_goAML_Trn_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
    Public list_goAML_trn_Person_Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)

    Public list_goAML_Transaction_Entity As New List(Of NawaDevDAL.goAML_Transaction_Entity)
    Public list_goAML_Trn_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
    Public list_goAML_Trn_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)

    Public list_goAML_Trn_Director As New List(Of NawaDevDAL.goAML_Trn_Director)
    Public list_goAML_Trn_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
    Public list_goAML_trn_Director_Phone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)

    Public list_goAML_Trn_Conductor As New List(Of NawaDevDAL.goAML_Trn_Conductor)
    Public list_goAML_Trn_Conductor_Address As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
    Public list_goAML_trn_Conductor_Phone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)

    Public list_goAML_Transaction_Person_Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)

    'Transaction Multi Party
    Public list_goAML_Transaction_Party As New List(Of NawaDevDAL.goAML_Transaction_Party)

    Public list_goAML_Trn_Party_Account As New List(Of NawaDevDAL.goAML_Trn_Party_Account)
    Public list_goAML_Trn_par_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
    Public list_goAML_Trn_par_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
    Public list_goAML_trn_par_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)

    Public list_goAML_Trn_Par_Acc_Entity As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)
    Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
    Public list_goAML_trn_par_acc_Entity_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)

    Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

    Public list_goAML_Trn_Party_Person As New List(Of NawaDevDAL.goAML_Trn_Party_Person)
    Public list_goAML_Trn_Party_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
    Public list_goAML_Trn_Party_Person_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)

    Public list_goAML_Trn_Party_Entity As New List(Of NawaDevDAL.goAML_Trn_Party_Entity)
    Public list_goAML_Trn_Party_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
    Public list_goAML_Trn_Party_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)

    Public list_goAML_Trn_Par_Entity_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
    Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)

    Public list_goAML_Transaction_Party_Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
End Class

Public Class GoAML_Activity_Class
    Public obj_goAML_Act_ReportPartyType As New goAML_Act_ReportPartyType

    Public list_goAML_Act_Account As New List(Of NawaDevDAL.goAML_Act_Account)
    Public list_goAML_Act_acc_Signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
    Public list_goAML_Act_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
    Public list_goAML_Act_Acc_sign_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)

    Public list_goAML_Act_Entity_Account As New List(Of NawaDevDAL.goAML_Act_Entity_Account)
    Public list_goAML_Act_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
    Public list_goAML_Act_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)

    Public list_goAML_Act_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
    Public list_goAML_Act_Acc_Entity_Director_address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
    Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)

    Public list_goAML_Act_Person As New List(Of NawaDevDAL.goAML_Act_Person)
    Public list_goAML_Act_Person_Address As New List(Of NawaDevDAL.goAML_Act_Person_Address)
    Public list_goAML_Act_Person_Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)

    Public list_goAML_Act_Entity As New List(Of NawaDevDAL.goAML_Act_Entity)
    Public list_goAML_Act_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
    Public list_goAML_Act_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)

    Public list_goAML_Act_Director As New List(Of NawaDevDAL.goAML_Act_Director)
    Public list_goAML_Act_Director_Address As New List(Of NawaDevDAL.goAML_Act_Director_Address)
    Public list_goAML_Act_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)

    Public list_goAML_Activity_Person_Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
End Class

