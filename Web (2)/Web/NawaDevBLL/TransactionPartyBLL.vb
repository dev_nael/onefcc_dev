﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionPartyBLL
    Public Shared Sub InsertTransactionParty(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionParty As New goAML_Transaction_Party
                    itemObjTransactionParty = Transactions.objTransactionParty

                    With itemObjTransactionParty
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionParty).State = EntityState.Added

                    Dim objtypeReport As Type = itemObjTransactionParty.GetType
                    Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    For Each item As PropertyInfo In propertiesReport
                        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        {
                            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            .FieldName = item.Name,
                            .OldValue = "",
                            .NewValue = If(Not item.GetValue(itemObjTransactionParty, Nothing) Is Nothing, item.GetValue(itemObjTransactionParty, Nothing), "")
                        }
                        objdb.Entry(objATDetail).State = EntityState.Added
                    Next
                    objdb.SaveChanges()

                    If Transactions.objTransactionParty.fk_ref_detail_of = 1 Then
                        TransactionPartyAccountBLL.InsertTransactionPartyAccount(Transactions, Audit)
                    End If

                    If Transactions.objTransactionParty.fk_ref_detail_of = 2 Then
                        TransactionPartyPersonBLL.InsertTransactionPartyPerson(Transactions, Audit)
                    End If

                    If Transactions.objTransactionParty.fk_ref_detail_of = 3 Then
                        TransactionPartyEntityBLL.InsertTransactionPartyEntity(Transactions, Audit)
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionParty(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionParty As New goAML_Transaction_Party
                    itemObjTransactionParty = Transactions.objTransactionParty

                    objdb.Entry(itemObjTransactionParty).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Transactions.objTransactionParty.fk_ref_detail_of = 1 Then
                        TransactionPartyAccountBLL.DeleteTransactionPartyAccount(Transactions, Audit)
                    End If

                    If Transactions.objTransactionParty.fk_ref_detail_of = 2 Then
                        TransactionPartyPersonBLL.DeleteTransactionPartyPerson(Transactions, Audit)
                    End If

                    If Transactions.objTransactionParty.fk_ref_detail_of = 3 Then
                        TransactionPartyEntityBLL.DeleteTransactionPartyEntity(Transactions, Audit)
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionParty(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim TransactionParty As goAML_Transaction_Party = Transactions.objTransactionParty
                    Dim TransactionPartyCek As goAML_Transaction_Party = (From x In objdb.goAML_Transaction_Party Where x.PK_Trn_Party_ID = TransactionParty.PK_Trn_Party_ID Select x).FirstOrDefault

                    If TransactionPartyCek Is Nothing Then
                        InsertTransactionParty(Transactions, Audit)
                    Else

                        objdb.Entry(TransactionPartyCek).CurrentValues.SetValues(TransactionParty)
                        objdb.Entry(TransactionPartyCek).State = EntityState.Modified

                        Dim objtypeReport As Type = TransactionParty.GetType
                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        For Each item As PropertyInfo In propertiesReport
                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            {
                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                .FieldName = item.Name,
                                .OldValue = "",
                                .NewValue = If(Not item.GetValue(TransactionParty, Nothing) Is Nothing, item.GetValue(TransactionParty, Nothing), "")
                            }
                            objdb.Entry(objATDetail).State = EntityState.Added
                        Next
                        objdb.SaveChanges()

                        If Transactions.objTransactionParty.fk_ref_detail_of = 1 Then
                            Dim TransactionPartyAccountx As goAML_Trn_Party_Account = (From x In objdb.goAML_Trn_Party_Account Where x.FK_Trn_Party_ID = Transactions.objTransactionParty.PK_Trn_Party_ID Select x).FirstOrDefault
                            If Transactions.objAccountParty.PK_Trn_Party_Account_ID <> TransactionPartyAccountx.PK_Trn_Party_Account_ID Then
                                TransactionPartyAccountBLL.DeleteTransactionPartyAccount(Transactions, Audit)
                            Else
                                TransactionPartyAccountBLL.UpdateTransactionPartyAccount(Transactions, Audit)
                            End If
                        End If
                        If Transactions.objTransactionParty.fk_ref_detail_of = 2 Then
                            Dim TransactionPartyPersonx As goAML_Trn_Party_Person = (From x In objdb.goAML_Trn_Party_Person Where x.FK_Transaction_Party_ID = Transactions.objTransactionParty.PK_Trn_Party_ID Select x).FirstOrDefault
                            If Transactions.objPersonParty.PK_Trn_Party_Person_ID <> TransactionPartyPersonx.PK_Trn_Party_Person_ID Then
                                TransactionPartyPersonBLL.DeleteTransactionPartyPerson(Transactions, Audit)
                            Else
                                TransactionPartyPersonBLL.UpdateTransactionPartyPerson(Transactions, Audit)
                            End If
                        End If

                        If Transactions.objTransactionParty.fk_ref_detail_of = 3 Then
                            Dim TransactionPartyEntityx As goAML_Trn_Party_Entity = (From x In objdb.goAML_Trn_Party_Entity Where x.FK_Transaction_Party_ID = Transactions.objTransactionParty.PK_Trn_Party_ID Select x).FirstOrDefault
                            If Transactions.objentityParty.PK_Trn_Party_Entity_ID <> TransactionPartyEntityx.PK_Trn_Party_Entity_ID Then
                                TransactionPartyEntityBLL.UpdateTransactionPartyEntity(Transactions, Audit)
                            End If
                        End If
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
