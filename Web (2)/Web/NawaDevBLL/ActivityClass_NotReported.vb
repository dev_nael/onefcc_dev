﻿Public Class ActivityClass_NotReported
    'Activity
    Public objActivity As New NawaDevDAL.goAML_Act_ReportPartyType_NotReported
    Public listDirectorEntytAccountActivity As New List(Of DirectorEntityAccountActivityClass_NotReported)
    Public listDirectorEntityActivity As New List(Of DirectorEntityActivityClass_NotReported)
    Public listSignatoryAccountActivity As New List(Of SignatoryAccountActivityClass_NotReported)

    ' Activity account
    Public objAccountActivity As New NawaDevDAL.goAML_Act_Account_NotReported
    Public objAccountEntityActivity As New NawaDevDAL.goAML_Act_Entity_Account_NotReported
    Public listAddresAccountEntityActivity As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
    Public listPhoneAccountEntityActivity As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)

    ' Activity person
    Public objPersonActivity As New NawaDevDAL.goAML_Act_Person_NotReported
    Public listAddressPersonActivity As New List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
    Public listPhonePersonActivity As New List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
    Public listAddressEmployerPersonActivity As New List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
    Public listPhoneEmployerPersonActivity As New List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
    Public listIdentificationPersonActivity As New List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)

    ' Activity entity
    Public objentityActivity As New NawaDevDAL.goAML_Act_Entity_NotReported
    Public listAddressEntityActivity As New List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)
    Public listPhoneEntityActivity As New List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)
End Class

