﻿'--------------------------------------------
'Classname      : AML_Watchlist
'Description    : Maintain AML Watchlist Data
'Created By     : Adi Darmawan
'Created Date   : 24-Feb-2021
'--------------------------------------------

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports NawaDevDAL

Public Class AML_WATCHLIST_BLL

    Shared Function GetWatchlistClassByID(ID As Long) As NawaDevBLL.AML_WATCHLIST_CLASS
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim objWatchlist = objDb.AML_WATCHLIST.Where(Function(x) x.PK_AML_WATCHLIST_ID = ID).FirstOrDefault
            Dim objWatchlistAlias = objDb.AML_WATCHLIST_ALIAS.Where(Function(x) x.FK_AML_WATCHLIST_ID = ID).ToList
            Dim objWatchlistAddress = objDb.AML_WATCHLIST_ADDRESS.Where(Function(x) x.FK_AML_WATCHLIST_ID = ID).ToList
            Dim objWatchlistIdentity = objDb.AML_WATCHLIST_IDENTITY.Where(Function(x) x.FK_AML_WATCHLIST_ID = ID).ToList

            Dim objWatchlistClass = New AML_WATCHLIST_CLASS
            With objWatchlistClass
                .objAML_WATCHLIST = objWatchlist

                If objWatchlistAlias IsNot Nothing Then
                    .objList_AML_WATCHLIST_ALIAS = objWatchlistAlias
                End If
                If objWatchlistAddress IsNot Nothing Then
                    .objList_AML_WATCHLIST_ADDRESS = objWatchlistAddress
                End If
                If objWatchlistIdentity IsNot Nothing Then
                    .objList_AML_WATCHLIST_IDENTITY = objWatchlistIdentity
                End If
            End With

            Return objWatchlistClass
        End Using
    End Function

    Shared Function GetWatchlistTypeByID(ID As String) As NawaDevDAL.AML_WATCHLIST_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_WATCHLIST_TYPE.Where(Function(x) x.PK_AML_WATCHLIST_TYPE_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetWatchlistCategoryByID(ID As String) As NawaDevDAL.AML_WATCHLIST_CATEGORY
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_WATCHLIST_CATEGORY.Where(Function(x) x.PK_AML_WATCHLIST_CATEGORY_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetCountryByCountryCode(Kode As String) As NawaDevDAL.AML_COUNTRY
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByCode(Kode As String) As NawaDevDAL.AML_ADDRESS_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_ADDRESS_TYPE.Where(Function(x) x.FK_AML_ADDRES_TYPE_CODE = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetIdentityTypeByCode(Kode As String) As NawaDevDAL.AML_IDENTITY_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_IDENTITY_TYPE.Where(Function(x) x.FK_AML_IDENTITY_TYPE_CODE = Kode).FirstOrDefault
        End Using
    End Function

    Shared Sub SaveEditWithApproval(objModule As NawaDAL.Module, objData As NawaDevBLL.AML_WATCHLIST_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetWatchlistClassByID(objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID)

        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objAML_WATCHLIST
                        .Active = True
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objData_Old)

                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveEditWithoutApproval(objModule As NawaDAL.Module, objData As AML_WATCHLIST_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetWatchlistClassByID(objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID)

        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objAML_WATCHLIST
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objAML_WATCHLIST).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objAML_WATCHLIST, objData_Old.objAML_WATCHLIST)


                    '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                    'Save Alias
                    For Each item In objData.objList_AML_WATCHLIST_ALIAS.Where(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID < 0).ToList
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Address
                    For Each item In objData.objList_AML_WATCHLIST_ADDRESS.Where(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID < 0).ToList
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_AML_WATCHLIST_IDENTITY.Where(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID < 0).ToList
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    '===================== END OF NEW ADDED DATA


                    '===================== UPDATED DATA (PK untuk masing2 data > 0)
                    'Save Alias
                    For Each item In objData.objList_AML_WATCHLIST_ALIAS.Where(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID > 0).ToList
                        Dim item_old = objData_Old.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = item.PK_AML_WATCHLIST_ALIAS_ID)

                        With item
                            .LastUpdateBy = strUserID
                            .LastUpdateDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save Address
                    For Each item In objData.objList_AML_WATCHLIST_ADDRESS.Where(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID > 0).ToList
                        Dim item_old = objData_Old.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = item.PK_AML_WATCHLIST_ADDRESS_ID)

                        With item
                            .LastUpdateBy = strUserID
                            .LastUpdateDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save Identity
                    For Each item In objData.objList_AML_WATCHLIST_IDENTITY.Where(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID > 0).ToList
                        Dim item_old = objData_Old.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = item.PK_AML_WATCHLIST_IDENTITY_ID)

                        With item
                            .LastUpdateBy = strUserID
                            .LastUpdateDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next
                    '===================== END OF UPDATED DATA


                    '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                    'Deleted Alias
                    For Each item_old In objData_Old.objList_AML_WATCHLIST_ALIAS
                        Dim objCek = objData.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = item_old.PK_AML_WATCHLIST_ALIAS_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Address
                    For Each item_old In objData_Old.objList_AML_WATCHLIST_ADDRESS
                        Dim objCek = objData.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = item_old.PK_AML_WATCHLIST_ADDRESS_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Identity
                    For Each item_old In objData_Old.objList_AML_WATCHLIST_IDENTITY
                        Dim objCek = objData.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = item_old.PK_AML_WATCHLIST_IDENTITY_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next
                    '===================== END OF DELETED DATA

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveAddWithApproval(objModule As NawaDAL.Module, objData As AML_WATCHLIST_CLASS)

        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objAML_WATCHLIST
                        .Active = True
                        .CreatedBy = strUserID
                        .CreatedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleKey = Nothing
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = Nothing
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function SaveAddWithoutApproval(objModule As NawaDAL.Module, objData As AML_WATCHLIST_CLASS)
        Dim IDUnik As Long

        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objAML_WATCHLIST
                        .Active = True
                        .CreatedBy = strUserID
                        .CreatedDate = Now
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objAML_WATCHLIST).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    '25-May-2021
                    IDUnik = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                    '-----------

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objAML_WATCHLIST)

                    'Save Alias
                    For Each item In objData.objList_AML_WATCHLIST_ALIAS
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Address
                    For Each item In objData.objList_AML_WATCHLIST_ADDRESS
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_AML_WATCHLIST_IDENTITY
                        With item
                            .FK_AML_WATCHLIST_ID = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                            .Active = True
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                            .ApprovedBy = strUserID
                            .ApprovedDate = Now
                            .Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()

                    Return IDUnik
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex

                    Return Nothing
                End Try
            End Using
        End Using
    End Function

    Shared Sub SaveDeleteWithApproval(objModule As NawaDAL.Module, objData As AML_WATCHLIST_CLASS)

        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = Nothing
                        .ModuleFieldBefore = objXMLData
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveDeleteWithoutApproval(objModule As NawaDAL.Module, objData As AML_WATCHLIST_CLASS)
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Delete objData
                    objDB.Entry(objData.objAML_WATCHLIST).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objAML_WATCHLIST)

                    'Save Alias
                    For Each item In objData.objList_AML_WATCHLIST_ALIAS
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Address
                    For Each item In objData.objList_AML_WATCHLIST_ADDRESS
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_AML_WATCHLIST_IDENTITY
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Accept(ID As Long)

        Dim IDUnik As Long

        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_WATCHLIST_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            '25-May-2021 ganti jadi function agar bisa mengembalikan IDUnik
                            'SaveAddWithoutApproval(objModule, objData)
                            IDUnik = SaveAddWithoutApproval(objModule, objData)

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_WATCHLIST_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveEditWithoutApproval(objModule, objData)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData.objAML_WATCHLIST.PK_AML_WATCHLIST_ID

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AML_WATCHLIST_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveDeleteWithoutApproval(objModule, objData_Old)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData_Old.objAML_WATCHLIST.PK_AML_WATCHLIST_ID

                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                    '25-May-2021 Adi : Update AML_WATCHLIST_SCREENING and History Perubahan Watchlist
                    Dim param(0) As SqlParameter
                    param(0) = New SqlParameter
                    param(0).ParameterName = "@PK_AML_WATCHLIST_ID"
                    param(0).Value = IDUnik
                    param(0).DbType = SqlDbType.BigInt

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_Generate_WatchlistScreening_ByPK", param)

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Reject(ID As Long)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_WATCHLIST_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objAML_WATCHLIST)

                            'Save Alias
                            For Each item In objData.objList_AML_WATCHLIST_ALIAS
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData.objList_AML_WATCHLIST_ADDRESS
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData.objList_AML_WATCHLIST_IDENTITY
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AML_WATCHLIST_CLASS))
                            Dim objData_Old As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AML_WATCHLIST_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objAML_WATCHLIST, objData_Old.objAML_WATCHLIST)

                            '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                            'Save Alias
                            For Each item In objData.objList_AML_WATCHLIST_ALIAS.Where(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData.objList_AML_WATCHLIST_ADDRESS.Where(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData.objList_AML_WATCHLIST_IDENTITY.Where(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next
                            '===================== END OF NEW ADDED DATA


                            '===================== UPDATED DATA (PK untuk masing2 data > 0)
                            'Save Alias
                            For Each item In objData.objList_AML_WATCHLIST_ALIAS.Where(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID > 0).ToList
                                Dim item_old = objData_Old.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = item.PK_AML_WATCHLIST_ALIAS_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next

                            'Save Address
                            For Each item In objData.objList_AML_WATCHLIST_ADDRESS.Where(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID > 0).ToList
                                Dim item_old = objData_Old.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = item.PK_AML_WATCHLIST_ADDRESS_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next

                            'Save Identity
                            For Each item In objData.objList_AML_WATCHLIST_IDENTITY.Where(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID > 0).ToList
                                Dim item_old = objData_Old.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = item.PK_AML_WATCHLIST_IDENTITY_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next
                            '===================== END OF UPDATED DATA


                            '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                            'Deleted Alias
                            For Each item_old In objData_Old.objList_AML_WATCHLIST_ALIAS
                                Dim objCek = objData.objList_AML_WATCHLIST_ALIAS.Find(Function(x) x.PK_AML_WATCHLIST_ALIAS_ID = item_old.PK_AML_WATCHLIST_ALIAS_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next

                            'Deleted Address
                            For Each item_old In objData_Old.objList_AML_WATCHLIST_ADDRESS
                                Dim objCek = objData.objList_AML_WATCHLIST_ADDRESS.Find(Function(x) x.PK_AML_WATCHLIST_ADDRESS_ID = item_old.PK_AML_WATCHLIST_ADDRESS_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next

                            'Deleted Identity
                            For Each item_old In objData_Old.objList_AML_WATCHLIST_IDENTITY
                                Dim objCek = objData.objList_AML_WATCHLIST_IDENTITY.Find(Function(x) x.PK_AML_WATCHLIST_IDENTITY_ID = item_old.PK_AML_WATCHLIST_IDENTITY_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next
                        '===================== END OF DELETED DATA

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As AML_WATCHLIST_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AML_WATCHLIST_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.objAML_WATCHLIST)

                            'Save Alias
                            For Each item In objData_Old.objList_AML_WATCHLIST_ALIAS
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData_Old.objList_AML_WATCHLIST_ADDRESS
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData_Old.objList_AML_WATCHLIST_IDENTITY
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next
                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New NawaDatadevEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function

End Class

Public Class AML_WATCHLIST_CLASS

    Public objAML_WATCHLIST As New NawaDevDAL.AML_WATCHLIST
    Public objList_AML_WATCHLIST_ALIAS As New List(Of NawaDevDAL.AML_WATCHLIST_ALIAS)
    Public objList_AML_WATCHLIST_ADDRESS As New List(Of NawaDevDAL.AML_WATCHLIST_ADDRESS)
    Public objList_AML_WATCHLIST_IDENTITY As New List(Of NawaDevDAL.AML_WATCHLIST_IDENTITY)

End Class
