﻿Imports NawaDevDAL

Public Class EntityDataBLL
    Public objEntityAccountFrom As goAML_Trn_Entity_account
    Public listObjPhoneAccountEntityFrom As List(Of goAML_Trn_Acc_Entity_Phone)
    Public listObjAddressAccountEntityFrom As List(Of goAML_Trn_Acc_Entity_Address)

    Public objEntityFrom As goAML_Transaction_Entity
    Public listObjPhoneEntityFrom As List(Of goAML_Trn_Entity_Phone)
    Public listObjAddressEntityFrom As List(Of goAML_Trn_Entity_Address)

    Public listObjDirectorEntityFrom As List(Of goAML_Trn_Director)


    Public objEntityAccountTo As goAML_Trn_Entity_account
    Public listObjPhoneAccountEntityTo As List(Of goAML_Trn_Acc_Entity_Phone)
    Public listObjAddressAccountEntityTo As List(Of goAML_Trn_Acc_Entity_Address)

    Public objEntityTo As goAML_Transaction_Entity
    Public listObjPhoneEntityTo As List(Of goAML_Trn_Entity_Phone)
    Public listObjAddressEntityTo As List(Of goAML_Trn_Entity_Address)

    Public listObjDirectorEntityTo As List(Of goAML_Trn_Director)
End Class
