﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports NawaDevDAL
<Serializable()>
Public Class WICBLL
    Public oPanelinput As FormPanel
    Sub New(ObjPanel As FormPanel)
        oPanelinput = ObjPanel
    End Sub
    Sub New()

    End Sub
    'saad 17112020
    Shared Function getDataApproval(id As String, moduleName As String) As NawaDevDAL.ModuleApproval
        Dim objModuleApproval As New NawaDevDAL.ModuleApproval
        Using objdb As New NawaDatadevEntities
            Dim Approval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function
    Sub SaveAddTanpaApproval(objData As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module)
        Using objdb As New NawaDatadevEntities
            Using objtrans As DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    With objData
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = DateTime.Now
                    End With
                    objdb.Entry(objData).State = EntityState.Added

                    objdb.SaveChanges()
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objData)

                    If listDirector.Count > 0 Then

                        For Each DirectorBLL As WICDirectorDataBLL In listDirector
                            Dim objDirector As goAML_Ref_Walk_In_Customer_Director = DirectorBLL.ObjDirector
                            objDirector.FK_Entity_ID = objData.PK_Customer_ID
                            objdb.Entry(objDirector).State = EntityState.Added

                            objdb.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, objDirector)
                            If DirectorBLL.ListDirectorAddress.Count > 0 Then

                                For Each itemAddress As goAML_Ref_Address In DirectorBLL.ListDirectorAddress
                                    With itemAddress
                                        .FK_To_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With

                                    objdb.Entry(itemAddress).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemAddress)
                                Next
                            End If

                            If DirectorBLL.ListDirectorAddressEmployer.Count > 0 Then

                                For Each itemAddress As goAML_Ref_Address In DirectorBLL.ListDirectorAddressEmployer
                                    With itemAddress
                                        .FK_To_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With

                                    objdb.Entry(itemAddress).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemAddress)
                                Next

                            End If

                            If DirectorBLL.ListDirectorPhone.Count > 0 Then

                                For Each itemPhone As goAML_Ref_Phone In DirectorBLL.ListDirectorPhone
                                    With itemPhone
                                        .FK_for_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemPhone).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemPhone)
                                Next

                            End If

                            If DirectorBLL.ListDirectorPhoneEmployer.Count > 0 Then

                                For Each itemPhone As goAML_Ref_Phone In DirectorBLL.ListDirectorPhoneEmployer
                                    With itemPhone
                                        .FK_for_Table_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemPhone).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemPhone)
                                Next

                            End If

                            If DirectorBLL.ListDirectorIdentification.Count > 0 Then

                                For Each itemIdentification As goAML_Person_Identification In DirectorBLL.ListDirectorIdentification
                                    With itemIdentification
                                        .FK_Person_ID = objDirector.NO_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objdb.Entry(itemIdentification).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, itemIdentification)
                                Next

                            End If
                        Next
                    End If

                    If listAddressDetail.Count > 0 Then

                        For Each item As goAML_Ref_Address In listAddressDetail
                            With item
                                .FK_To_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listPhoneDetail.Count > 0 Then

                        For Each item As goAML_Ref_Phone In listPhoneDetail
                            With item
                                .FK_for_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listAddressEmployerDetail.Count > 0 Then

                        For Each item As goAML_Ref_Address In listAddressEmployerDetail
                            With item
                                .FK_To_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listPhoneEmployerDetail.Count > 0 Then

                        For Each item As goAML_Ref_Phone In listPhoneEmployerDetail
                            With item
                                .FK_for_Table_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    If listIdentificationDetail.Count > 0 Then

                        For Each item As goAML_Person_Identification In listIdentificationDetail
                            With item
                                .FK_Person_ID = objData.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .CreatedDate = DateTime.Now
                            End With
                            objdb.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next

                    End If

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Sub SaveAddApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New WICDataBLL With
                    {
                        .ObjWIC = objSave,
                        .ObjWICAddress = listAddressDetail,
                        .ObjWICPhone = listPhoneDetail,
                        .ObjWICAddressEmployer = listAddressEmployerDetail,
                        .OBjWICPhoneEmployer = listPhoneEmployerDetail,
                        .ObjWICIdentification = listIdentificationDetail,
                        .ObjDirector = listDirector
                    }
                    Dim dataXML As String = Common.Serialize(objData)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval With
                    {
                        .ModuleName = objModule.ModuleName,
                        .ModuleKey = 0,
                        .ModuleField = dataXML,
                        .ModuleFieldBefore = "",
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Insert,
                        .CreatedBy = Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now
                    }

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = objModule.ModuleLabel
                    Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objData)

                    objDB.SaveChanges()

                    If listDirector.Count > 0 Then
                        For Each itemDetail As WICDirectorDataBLL In listDirector
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail.ObjDirector)
                            If itemDetail.ListDirectorAddress.Count > 0 Then
                                For Each item In itemDetail.ListDirectorAddress
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If
                            If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                For Each item In itemDetail.ListDirectorAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorPhone.Count > 0 Then
                                For Each item In itemDetail.ListDirectorPhone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each item In itemDetail.ListDirectorPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If itemDetail.ListDirectorIdentification.Count > 0 Then
                                For Each item In itemDetail.ListDirectorIdentification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If
                        Next
                    End If

                    If listAddressDetail.Count > 0 Then
                        For Each item In listAddressDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listAddressEmployerDetail.Count > 0 Then
                        For Each item In listAddressEmployerDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listPhoneDetail.Count > 0 Then
                        For Each item In listPhoneDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listPhoneEmployerDetail.Count > 0 Then
                        For Each item In listPhoneEmployerDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listIdentificationDetail.Count > 0 Then
                        For Each item In listIdentificationDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Sub Accept(pK_ModuleApproval_ID As String)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert

                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            With objModuledata.ObjWIC
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Added

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each objDirector As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = objDirector.ObjDirector
                                    With item
                                        .FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                    objDB.SaveChanges()

                                    If objDirector.ListDirectorAddress.Count > 0 Then

                                        For Each itemAddress As goAML_Ref_Address In objDirector.ListDirectorAddress
                                            With itemAddress
                                                .FK_To_Table_ID = item.NO_ID
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemAddress).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemAddress)
                                        Next

                                    End If

                                    If objDirector.ListDirectorAddressEmployer.Count > 0 Then

                                        For Each itemAddress As goAML_Ref_Address In objDirector.ListDirectorAddressEmployer
                                            With itemAddress
                                                .FK_To_Table_ID = item.NO_ID
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemAddress).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemAddress)
                                        Next

                                    End If

                                    If objDirector.ListDirectorPhone.Count > 0 Then

                                        For Each itemPhone As goAML_Ref_Phone In objDirector.ListDirectorPhone
                                            With itemPhone
                                                .FK_for_Table_ID = item.NO_ID
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemPhone).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemPhone)
                                        Next

                                    End If

                                    If objDirector.ListDirectorPhoneEmployer.Count > 0 Then

                                        For Each itemPhone As goAML_Ref_Phone In objDirector.ListDirectorPhoneEmployer
                                            With itemPhone
                                                .FK_for_Table_ID = item.NO_ID
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemPhone).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemPhone)
                                        Next

                                    End If

                                    If objDirector.ListDirectorIdentification.Count > 0 Then

                                        For Each itemIdentification As goAML_Person_Identification In objDirector.ListDirectorIdentification
                                            With itemIdentification
                                                .FK_Person_ID = item.NO_ID
                                                .CreatedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .CreatedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemIdentification).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemIdentification)
                                        Next

                                    End If
                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    With item
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    With item
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    With item
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then

                                For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    With item
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then

                                For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    With item
                                        .FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .CreatedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                        Case Common.ModuleActionEnum.Update

                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            Dim objModuledataOld As WICDataBLL = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(WICDataBLL))

                            objModuledata.ObjWIC.ApprovedBy = Common.SessionCurrentUser.UserID
                            objModuledata.ObjWIC.ApprovedDate = Now
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Modified

                            Dim user As String = Common.SessionCurrentUser.UserID
                            Dim act As Integer = Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objModuledata, objModuledataOld)
                            objDB.SaveChanges()

                            For Each objDirectors As WICDirectorDataBLL In objModuledata.ObjDirector
                                Dim itemx As goAML_Ref_Walk_In_Customer_Director = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID Select x).FirstOrDefault
                                If itemx IsNot Nothing Then
                                    If objDirectors.ObjDirector.NO_ID <> itemx.NO_ID Then
                                        objDB.Entry(itemx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objDirectors.ObjDirector, itemx)
                                    End If

                                    For Each itemAddressx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList()
                                        Dim objAddressCek As goAML_Ref_Address = objDirectors.ListDirectorAddress.Find(Function(x) x.PK_Customer_Address_ID = itemAddressx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8)
                                        If objAddressCek Is Nothing Then
                                            objDB.Entry(itemAddressx).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objAddressCek, itemAddressx)
                                        End If
                                    Next

                                    For Each itemPhonex As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList()
                                        Dim objPhoneCek As goAML_Ref_Phone = objDirectors.ListDirectorPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemPhonex.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8)
                                        If objPhoneCek Is Nothing Then
                                            objDB.Entry(itemPhonex).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objPhoneCek, itemPhonex)
                                        End If
                                    Next

                                    For Each itemAddressx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList()
                                        Dim objAddressCek As goAML_Ref_Address = objDirectors.ListDirectorAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = itemAddressx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4)
                                        If objAddressCek Is Nothing Then
                                            objDB.Entry(itemAddressx).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objAddressCek, itemAddressx)
                                        End If
                                    Next

                                    For Each itemPhonex As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList()
                                        Dim objPhoneCek As goAML_Ref_Phone = objDirectors.ListDirectorPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = itemPhonex.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4)
                                        If objPhoneCek Is Nothing Then
                                            objDB.Entry(itemPhonex).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objPhoneCek, itemPhonex)
                                        End If
                                    Next

                                    For Each itemIdentificationx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = itemx.NO_ID And x.FK_Person_Type = 7 Select x).ToList()
                                        Dim objIdentificationCek As goAML_Person_Identification = objDirectors.ListDirectorIdentification.Find(Function(x) x.PK_Person_Identification_ID = itemIdentificationx.PK_Person_Identification_ID And x.FK_Person_Type = 7)
                                        If objIdentificationCek Is Nothing Then
                                            objDB.Entry(itemIdentificationx).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objIdentificationCek, itemIdentificationx)
                                        End If
                                    Next
                                End If

                                Dim item As goAML_Ref_Walk_In_Customer_Director = objDirectors.ObjDirector
                                Dim obcek As goAML_Ref_Walk_In_Customer_Director = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.NO_ID = item.NO_ID Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_Entity_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If

                                If objDirectors.ListDirectorPhone.Count > 0 Then
                                    For Each itemP As goAML_Ref_Phone In objDirectors.ListDirectorPhone
                                        Dim obcekP As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemP.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_for_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                    Next
                                End If

                                If objDirectors.ListDirectorAddress.Count > 0 Then
                                    For Each itemP As goAML_Ref_Address In objDirectors.ListDirectorAddress
                                        Dim obcekP As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemP.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_To_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                    Next
                                End If

                                If objDirectors.ListDirectorPhoneEmployer.Count > 0 Then
                                    For Each itemP As goAML_Ref_Phone In objDirectors.ListDirectorPhoneEmployer
                                        Dim obcekP As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemP.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_for_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                    Next
                                End If

                                If objDirectors.ListDirectorAddressEmployer.Count > 0 Then
                                    For Each itemP As goAML_Ref_Address In objDirectors.ListDirectorAddressEmployer
                                        Dim obcekP As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemP.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_To_Table_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                    Next
                                End If

                                If objDirectors.ListDirectorIdentification.Count > 0 Then
                                    For Each itemP As goAML_Person_Identification In objDirectors.ListDirectorIdentification
                                        Dim obcekP As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemP.PK_Person_Identification_ID And x.FK_Person_Type = 7 Select x).FirstOrDefault
                                        If obcekP Is Nothing Then
                                            With itemP
                                                'agam 23102020
                                                .FK_Person_ID = item.NO_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                            End With
                                            objDB.Entry(itemP).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        Else
                                            With itemP
                                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                            End With
                                            objDB.Entry(obcekP).CurrentValues.SetValues(itemP)
                                            objDB.Entry(obcekP).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemP, obcekP)
                                        End If
                                    Next
                                End If

                            Next

                            For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList()
                                Dim objCek As goAML_Ref_Address = objModuledata.ObjWICAddress.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                            Next

                            For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                            Next

                            For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList()
                                Dim objCek As goAML_Ref_Phone = objModuledata.ObjWICPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                            Next

                            For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                            Next

                            For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList()
                                Dim objCek As goAML_Ref_Address = objModuledata.ObjWICAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                            Next

                            For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                            Next

                            For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList()
                                Dim objCek As goAML_Ref_Phone = objModuledata.OBjWICPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                            Next

                            For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                            Next

                            For Each itemx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Person_Type = 8 Select x).ToList()
                                Dim objCek As goAML_Person_Identification = objModuledata.ObjWICIdentification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID And x.FK_Person_Type = 8)
                                If objCek Is Nothing Then
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemx, objCek)
                                End If
                            Next

                            For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                Dim obcek As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 8 Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    With item
                                        'agam 23102020
                                        .FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID
                                        .ApprovedBy = Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                    End With
                                    objDB.Entry(item).State = EntityState.Added
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                Else
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, obcek)
                                End If
                            Next

                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            objDB.Entry(objModuledata.ObjWIC).State = EntityState.Deleted

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each item As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim items As goAML_Ref_Walk_In_Customer_Director = item.ObjDirector
                                    objDB.Entry(items).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If item.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In item.ListDirectorAddress
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In item.ListDirectorAddressEmployer
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In item.ListDirectorPhone
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In item.ListDirectorPhoneEmployer
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If item.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In item.ListDirectorIdentification
                                            objDB.Entry(itemD).State = EntityState.Deleted
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then
                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then
                                For Each item As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then
                                For Each item As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then
                                For Each item As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then
                                For Each item As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    objDB.Entry(item).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)
                                Next
                            End If

                            objDB.SaveChanges()

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Sub Reject(pK_ModuleApproval_ID As Long)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If
                                Next

                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then
                                For Each itemDetail As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then
                                For Each itemDetail As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                        Case Common.ModuleActionEnum.Update
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))
                            Dim objModuledataOld As WICDataBLL = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, objModuledata, objModuledataOld)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, item, objModuledataOld.ObjDirector)

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 8)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 4)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 8)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.NO_ID And x.FK_Ref_Detail_Of = 4)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            Dim itemm As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.NO_ID And x.FK_Person_Type = 7)).FirstOrDefault
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                        Next
                                    End If
                                Next

                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each itemD As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each itemD As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 3)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each itemD As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    Dim itemm As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then

                                For Each itemD As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    Dim itemm As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Ref_Detail_Of = 10)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then

                                For Each itemD As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    Dim itemm As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objModuledata.ObjWIC.PK_Customer_ID And x.FK_Person_Type = 8)).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeader.PK_AuditTrail_ID, itemD, itemm)
                                Next

                            End If

                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledata As WICDataBLL = Common.Deserialize(objApproval.ModuleField, GetType(WICDataBLL))

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, objModuledata)
                            objDB.SaveChanges()

                            If objModuledata.ObjDirector.Count > 0 Then

                                For Each itemDetail As WICDirectorDataBLL In objModuledata.ObjDirector
                                    Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, item)

                                    If itemDetail.ListDirectorAddress.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhone.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                        For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                    If itemDetail.ListDirectorIdentification.Count > 0 Then
                                        For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemD)
                                        Next
                                    End If

                                Next
                            End If

                            If objModuledata.ObjWICAddress.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddress
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.ObjWICPhone.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Phone In objModuledata.ObjWICPhone
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICAddressEmployer.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Address In objModuledata.ObjWICAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next
                            End If

                            If objModuledata.OBjWICPhoneEmployer.Count > 0 Then

                                For Each itemDetail As goAML_Ref_Phone In objModuledata.OBjWICPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                            If objModuledata.ObjWICIdentification.Count > 0 Then

                                For Each itemDetail As goAML_Person_Identification In objModuledata.ObjWICIdentification
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeader.PK_AuditTrail_ID, itemDetail)
                                Next

                            End If

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Sub LoadPanel(formPanelNew As FormPanel, moduleField As String, unikID As String)
        Dim objWICBLL As WICDataBLL = Common.Deserialize(moduleField, GetType(WICDataBLL))
        If Not objWICBLL Is Nothing Then
            If objWICBLL.ObjWIC.FK_Customer_Type_ID = 1 Then
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "ID", "PK_Customer_ID" & unikID, objWICBLL.ObjWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Gelar", "INDV_Title" & unikID, objWICBLL.ObjWIC.INDV_Title)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Lengkap", "INDV_last_name" & unikID, objWICBLL.ObjWIC.INDV_Last_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Lahir", "INDV_Birthdate" & unikID, objWICBLL.ObjWIC.INDV_BirthDate)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Lahir", "INDV_Birth_place" & unikID, objWICBLL.ObjWIC.INDV_Birth_Place)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Ibu Kandung", "INDV_Mothers_name" & unikID, objWICBLL.ObjWIC.INDV_Mothers_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Alias", "INDV_Alias" & unikID, objWICBLL.ObjWIC.INDV_Alias)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NIK", "INDV_SSN" & unikID, objWICBLL.ObjWIC.INDV_SSN)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "No. Passport", "INDV_Passport_number" & unikID, objWICBLL.ObjWIC.INDV_Passport_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara Penerbit Passport", "INDV_Passport_country" & unikID, objWICBLL.ObjWIC.INDV_Passport_Country)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "No. Identitas Lain", "INDV_ID_Number" & unikID, objWICBLL.ObjWIC.INDV_ID_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 1", "INDV_Nationality1 " & unikID, objWICBLL.ObjWIC.INDV_Nationality1)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 2", "INDV_Nationality2" & unikID, objWICBLL.ObjWIC.INDV_Nationality2)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kewarganegaraan 3", "INDV_Nationality3" & unikID, objWICBLL.ObjWIC.INDV_Nationality3)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara Domisili", "INDV_residence" & unikID, objWICBLL.ObjWIC.INDV_Residence)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Email", "INDV_Email" & unikID, objWICBLL.ObjWIC.INDV_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Pekerjaan", "INDV_Occupation" & unikID, objWICBLL.ObjWIC.INDV_Occupation)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tempat Bekerja", "INDV_employer_name" & unikID, objWICBLL.ObjWIC.INDV_Employer_Name)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "ID", "PK_Customer_ID" & unikID, objWICBLL.ObjWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Korporasi", "Corp_Name" & unikID, objWICBLL.ObjWIC.Corp_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nama Komersial", "Corp_Commercial_name" & unikID, objWICBLL.ObjWIC.Corp_Commercial_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Bentuk Korporasi", "Corp_Incorporation_legal_form" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Legal_Form)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Nomor Induk Berusaha", "Corp_Incorporation_number" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Bidang Usaha", "Corp_Business" & unikID, objWICBLL.ObjWIC.Corp_Business)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Email Korporasi", "Corp_Email" & unikID, objWICBLL.ObjWIC.Corp_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Website Korporasi", "Corp_url" & unikID, objWICBLL.ObjWIC.Corp_Url)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Provinsi", "Corp_incorporation_state" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_State)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara", "Corp_incorporation_country_code" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Country_Code)
                'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Pemilik/Pengurus/Orang yang diberikan Otorisasi Melakukan Transaksi", "Corp_director_id" & unikID, objWICBLL.ObjWIC.Corp_Director_ID)
                'BLL.NawaFramework.ExtDisplayField(formPanelNew, "Peran", "Corp_Role" & unikID, objWICBLL.ObjWIC.Corp_Role)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Pendirian", "Corp_incorporation_date" & unikID, objWICBLL.ObjWIC.Corp_Incorporation_Date)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tutup?", "Corp_business_closed" & unikID, objWICBLL.ObjWIC.Corp_Business_Closed)
                If objWICBLL.ObjWIC.Corp_Business_Closed Then
                    BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tanggal Tutup", "Corp_date_business_closed" & unikID, objWICBLL.ObjWIC.Corp_Date_Business_Closed)
                End If
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "NPWP", "Corp_tax_number" & unikID, objWICBLL.ObjWIC.Corp_Tax_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelNew, "Catatan", "Corp_Comments" & unikID, objWICBLL.ObjWIC.Corp_Comments)
            End If



            '--------------------- Phone -------------------
            Dim objStorePhone As New Store
            objStorePhone.ID = unikID & "StoreGridPhone"
            objStorePhone.ClientIDMode = ClientIDMode.Static

            Dim objModelPhone = New Model
            Dim objFieldPhone = New ModelField

            objFieldPhone = New ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_Ref_Detail_Of"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_for_Table_ID"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_country_prefix"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_extension"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "comments"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListColumnPhone As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnPhone.Add(objColumnNo)
            End Using

            Dim objColumnPhone As Column

            objColumnPhone = New Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Kode Area Telp"
            objColumnPhone.DataIndex = "tph_country_prefix"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Extensi"
            objColumnPhone.DataIndex = "tph_extension"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            Dim objParamPhone(0) As SqlParameter
            objParamPhone(0) = New SqlParameter
            objParamPhone(0).ParameterName = "@PK_Customer_ID"
            objParamPhone(0).SqlDbType = SqlDbType.BigInt
            objParamPhone(0).Value = objWICBLL.ObjWIC.PK_Customer_ID

            Dim objdtPhone As DataTable = Common.CopyGenericToDataTable(objWICBLL.ObjWICPhone)
            BLL.NawaFramework.ExtGridPanel(formPanelNew, "Phone", objStorePhone, objListColumnPhone, objdtPhone)




            '---------------- Address ----------------
            Dim objStoreAddress As New Store
            objStoreAddress.ID = unikID & "StoreGridAddress"
            objStoreAddress.ClientIDMode = ClientIDMode.Static

            Dim objModelAddress As New Model
            Dim objFieldAddress As ModelField

            objFieldAddress = New ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_Ref_detail_of"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_to_Table_ID"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Town"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Zip"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "State"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "comments"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListColumnAddress As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnAddress.Add(objColumnNo)
            End Using

            Dim objColumnAddress As Column

            objColumnAddress = New Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kecamatan"
            objColumnAddress.DataIndex = "Town"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kode Pos"
            objColumnAddress.DataIndex = "Zip"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Negara"
            objColumnAddress.DataIndex = "Country_Code"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Provinsi"
            objColumnAddress.DataIndex = "State"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            Dim objParamAddress(0) As SqlParameter
            objParamAddress(0) = New SqlParameter
            objParamAddress(0).ParameterName = "@PK_Customer_ID"
            objParamAddress(0).SqlDbType = SqlDbType.BigInt
            objParamAddress(0).Value = objWICBLL.ObjWIC.PK_Customer_ID

            Dim objdtAddress As DataTable = Common.CopyGenericToDataTable(objWICBLL.ObjWICAddress)
            BLL.NawaFramework.ExtGridPanel(formPanelNew, "Address", objStoreAddress, objListColumnAddress, objdtAddress)

        End If
    End Sub

    Sub LoadPanelDelete(formPanelWICDelete As FormPanel, moduleName As String, strUnikKey As String)
        Dim objWIC As goAML_Ref_WIC = GetWICByID(strUnikKey)
        Dim negara, direktur, badan, role As String
        If Not objWIC Is Nothing Then
            Dim strUnik As String = Guid.NewGuid.ToString()
            If objWIC.FK_Customer_Type_ID = 1 Then
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "ID", "PK_Customer_ID" & strUnik, objWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Gelar", "INDV_Title" & strUnik, objWIC.INDV_Title)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Lengkap", "INDV_last_name" & strUnik, objWIC.INDV_Last_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Lahir", "INDV_Birthdate" & strUnik, objWIC.INDV_BirthDate.Value.ToString("dd-MMM-yy"))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tempat Lahir", "INDV_Birth_place" & strUnik, objWIC.INDV_Birth_Place)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Ibu Kandung", "INDV_Mothers_name" & strUnik, objWIC.INDV_Mothers_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Alias", "INDV_Alias" & strUnik, objWIC.INDV_Alias)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "NIK", "INDV_SSN" & strUnik, objWIC.INDV_SSN)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "No. Passport", "INDV_Passport_number" & strUnik, objWIC.INDV_Passport_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara Penerbit Passport", "INDV_Passport_country" & strUnik, objWIC.INDV_Passport_Country)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "No. Identitas Lain", "INDV_ID_Number" & strUnik, objWIC.INDV_ID_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 1", "INDV_Nationality1" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality1))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 2", "INDV_Nationality2" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality2))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Kewarganegaraan 3", "INDV_Nationality3" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Nationality3))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara Domisili", "INDV_residence" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.INDV_Residence))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Email", "INDV_Email" & strUnik, objWIC.INDV_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Pekerjaan", "INDV_Occupation" & strUnik, objWIC.INDV_Occupation)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tempat Bekerja", "INDV_employer_name" & strUnik, objWIC.INDV_Employer_Name)
            Else
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "ID", "PK_Customer_ID" & strUnik, objWIC.PK_Customer_ID)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Korporasi", "Corp_Name" & strUnik, objWIC.Corp_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nama Komersial", "Corp_Commercial_name" & strUnik, objWIC.Corp_Commercial_Name)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Bentuk Korporasi", "Corp_Incorporation_legal_form" & strUnik, GlobalReportFunctionBLL.getBentukKorporasiByKode(objWIC.Corp_Incorporation_Legal_Form))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Nomor Induk Berusaha", "Corp_Incorporation_number" & strUnik, objWIC.Corp_Incorporation_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Bidang Usaha", "Corp_Business" & strUnik, objWIC.Corp_Business)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Email Korporasi", "Corp_Email" & strUnik, objWIC.Corp_Email)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Website Korporasi", "Corp_url" & strUnik, objWIC.Corp_Url)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Provinsi", "Corp_incorporation_state" & strUnik, objWIC.Corp_Incorporation_State)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Negara", "Corp_incorporation_country_code" & strUnik, GlobalReportFunctionBLL.getCountryByCode(objWIC.Corp_Incorporation_Country_Code))
                'direktur = GetBentukBadanUsaha(objWIC.Corp_Director_ID)
                'BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Pemilik/Pengurus/Orang yang diberikan Otorisasi Melakukan Transaksi", "Corp_director_id" & strUnik, direktur)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Peran", "Corp_Role" & strUnik, GlobalReportFunctionBLL.getRoleKorporasiTypebyCode(objWIC.Corp_Role))
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Pendirian", "Corp_incorporation_date" & strUnik, objWIC.Corp_Incorporation_Date)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tutup?", "Corp_business_closed" & strUnik, objWIC.Corp_Business_Closed)
                If objWIC.Corp_Business_Closed Then
                    BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Tanggal Tutup", "Corp_date_business_closed" & strUnik, objWIC.Corp_Date_Business_Closed)
                End If
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "NPWP", "Corp_tax_number" & strUnik, objWIC.Corp_Tax_Number)
                BLL.NawaFramework.ExtDisplayField(formPanelWICDelete, "Catatan", "Corp_Comments" & strUnik, objWIC.Corp_Comments)
            End If


            '--------------------- Director -------------------
            Dim objStoreDirector As New Store
            objStoreDirector.ID = strUnik & "StoreGridDirector"
            objStoreDirector.ClientIDMode = ClientIDMode.Static

            Dim objModelDirector = New Model
            Dim objFieldDirector = New ModelField

            objFieldDirector = New ModelField
            objFieldDirector.Name = "NO_ID"
            objFieldDirector.Type = ModelFieldType.Auto
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Role"
            objFieldDirector.Type = ModelFieldType.Int
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "FK_Entity_ID"
            objFieldDirector.Type = ModelFieldType.Int
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Tph_Contact_Type"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "Tph_Communication_Type"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_country_prefix"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_number"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "tph_extension"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objFieldDirector = New ModelField
            objFieldDirector.Name = "comments"
            objFieldDirector.Type = ModelFieldType.String
            objModelDirector.Fields.Add(objFieldDirector)

            objStoreDirector.Model.Add(objModelDirector)

            Dim objListColumnDirector As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnDirector.Add(objColumnNo)
            End Using

            Dim objColumnDirector As Column

            objColumnDirector = New Column
            objColumnDirector.Text = "Kategori Kontak"
            objColumnDirector.DataIndex = "Tph_Contact_Type"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            objColumnDirector = New Column
            objColumnDirector.Text = "Jenis Alat Komunikasi"
            objColumnDirector.DataIndex = "Tph_Communication_Type"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            objColumnDirector = New Column
            objColumnDirector.Text = "Nomor Telepon"
            objColumnDirector.DataIndex = "tph_number"
            objColumnDirector.ClientIDMode = ClientIDMode.Static
            objColumnDirector.Flex = 1
            objListColumnDirector.Add(objColumnDirector)

            Dim objParamDirector(0) As SqlParameter
            objParamDirector(0) = New SqlParameter
            objParamDirector(0).ParameterName = "@PK_Customer_ID"
            objParamDirector(0).SqlDbType = SqlDbType.BigInt
            objParamDirector(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneDetailByWIC", objParamDirector)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Director", objStoreDirector, objListColumnDirector, objData)
            End Using


            '--------------------- Phone -------------------
            Dim objStorePhone As New Store
            objStorePhone.ID = strUnik & "StoreGridPhone"
            objStorePhone.ClientIDMode = ClientIDMode.Static

            Dim objModelPhone = New Model
            Dim objFieldPhone = New ModelField

            objFieldPhone = New ModelField
            objFieldPhone.Name = "PK_goAML_Ref_Phone"
            objFieldPhone.Type = ModelFieldType.Auto
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_Ref_Detail_Of"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "FK_for_Table_ID"
            objFieldPhone.Type = ModelFieldType.Int
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Contact_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "Tph_Communication_Type"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_country_prefix"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_number"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "tph_extension"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objFieldPhone = New ModelField
            objFieldPhone.Name = "comments"
            objFieldPhone.Type = ModelFieldType.String
            objModelPhone.Fields.Add(objFieldPhone)

            objStorePhone.Model.Add(objModelPhone)

            Dim objListColumnPhone As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnPhone.Add(objColumnNo)
            End Using

            Dim objColumnPhone As Column

            objColumnPhone = New Column
            objColumnPhone.Text = "Kategori Kontak"
            objColumnPhone.DataIndex = "Tph_Contact_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Jenis Alat Komunikasi"
            objColumnPhone.DataIndex = "Tph_Communication_Type"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            objColumnPhone = New Column
            objColumnPhone.Text = "Nomor Telepon"
            objColumnPhone.DataIndex = "tph_number"
            objColumnPhone.ClientIDMode = ClientIDMode.Static
            objColumnPhone.Flex = 1
            objListColumnPhone.Add(objColumnPhone)

            Dim objParamPhone(0) As SqlParameter
            objParamPhone(0) = New SqlParameter
            objParamPhone(0).ParameterName = "@PK_Customer_ID"
            objParamPhone(0).SqlDbType = SqlDbType.BigInt
            objParamPhone(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetPhoneDetailByWIC", objParamPhone)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Phone", objStorePhone, objListColumnPhone, objData)
            End Using



            '---------------- Address ----------------
            Dim objStoreAddress As New Store
            objStoreAddress.ID = strUnik & "StoreGridAddress"
            objStoreAddress.ClientIDMode = ClientIDMode.Static

            Dim objModelAddress As New Model
            Dim objFieldAddress As ModelField

            objFieldAddress = New ModelField
            objFieldAddress.Name = "PK_Customer_Address_ID"
            objFieldAddress.Type = ModelFieldType.Auto
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_Ref_detail_of"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "FK_to_Table_ID"
            objFieldAddress.Type = ModelFieldType.Int
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address_Type"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Address"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Town"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "City"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Zip"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "Country_Code"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "State"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objFieldAddress = New ModelField
            objFieldAddress.Name = "comments"
            objFieldAddress.Type = ModelFieldType.String
            objModelAddress.Fields.Add(objFieldAddress)

            objStoreAddress.Model.Add(objModelAddress)

            Dim objListColumnAddress As New List(Of ColumnBase)
            Using objColumnNo As New RowNumbererColumn
                objColumnNo.Text = "No."
                objColumnNo.ClientIDMode = ClientIDMode.Static
                objListColumnAddress.Add(objColumnNo)
            End Using

            Dim objColumnAddress As Column

            objColumnAddress = New Column
            objColumnAddress.Text = "Tipe Alamat"
            objColumnAddress.DataIndex = "Address_Type"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Alamat"
            objColumnAddress.DataIndex = "Address"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Kota/Kabupaten"
            objColumnAddress.DataIndex = "City"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            objColumnAddress = New Column
            objColumnAddress.Text = "Negara"
            objColumnAddress.DataIndex = "Country_Code"
            objColumnAddress.ClientIDMode = ClientIDMode.Static
            objColumnAddress.Flex = 1
            objListColumnAddress.Add(objColumnAddress)

            Dim objParamAddress(0) As SqlParameter
            objParamAddress(0) = New SqlParameter
            objParamAddress(0).ParameterName = "@PK_Customer_ID"
            objParamAddress(0).SqlDbType = SqlDbType.BigInt
            objParamAddress(0).Value = objWIC.PK_Customer_ID

            Using objData As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetAddressDetailByWIC", objParamAddress)
                BLL.NawaFramework.ExtGridPanel(formPanelWICDelete, "Address", objStoreAddress, objListColumnAddress, objData)
            End Using

        End If
    End Sub
    Public Shared Function GetWICByID(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_WIC Where x.PK_Customer_ID = strUnikKey Select x).FirstOrDefault
        End Using
    End Function
    Public Shared Function GetWICByWICNO(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_WIC Where x.WIC_No = strUnikKey Select x).FirstOrDefault
        End Using
    End Function

    Function GetWICByIDD(strUnikKey As String) As goAML_Ref_WIC
        Using objDb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_WIC
                    Join a In objDb.goAML_Ref_Nama_Negara On x.Corp_Incorporation_Country_Code Equals a.Kode
                    Where x.PK_Customer_ID = strUnikKey Select x).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetWICByPKID(ByVal id As Long) As goAML_Ref_WIC
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_WIC Where X.PK_Customer_ID = id Select X).FirstOrDefault
        End Using
    End Function
    Function GetWICByPKIDDetailAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 3 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailEmployerAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 10 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 3 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailEmployerPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 10 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirector(id As Long) As List(Of goAML_Ref_Walk_In_Customer_Director)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Walk_In_Customer_Director Where X.FK_Entity_ID = id Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorDetail(id As Long) As goAML_Ref_Walk_In_Customer_Director
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Walk_In_Customer_Director Where X.NO_ID = id Select X).FirstOrDefault
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorPhone(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorAddress(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorPhoneEmployer(id As Long) As List(Of goAML_Ref_Phone)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Phone Where X.FK_for_Table_ID = id And X.FK_Ref_Detail_Of = 4 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorAddressEmployer(id As Long) As List(Of goAML_Ref_Address)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Ref_Address Where X.FK_To_Table_ID = id And X.FK_Ref_Detail_Of = 4 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailIdentification(id As Long) As List(Of goAML_Person_Identification)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Person_Identification Where X.FK_Person_ID = id And X.FK_Person_Type = 8 Select X).ToList
        End Using
    End Function
    Function GetWICByPKIDDetailDirectorIdentification(id As Long) As List(Of goAML_Person_Identification)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.goAML_Person_Identification Where X.FK_Person_ID = id And X.FK_Person_Type = 7 Select X).ToList
        End Using
    End Function
    Function IsExistInPendingApproval(id As Long, objModule As NawaDAL.[Module]) As Boolean
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.ModuleApprovals Where X.ModuleKey = id And X.ModuleName = objModule.ModuleName).Count > 0
        End Using
    End Function

    Public Sub SaveEditTanpaApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    objSave.ApprovedBy = Common.SessionCurrentUser.UserID
                    objSave.ApprovedDate = Now
                    objDB.Entry(objSave).State = EntityState.Modified

                    objDB.SaveChanges()
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objSave)
                    objDB.SaveChanges()


                    For Each objDirectors As WICDirectorDataBLL In listDirector
                        Dim itemx As New goAML_Ref_Walk_In_Customer_Director
                        itemx = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.FK_Entity_ID = objSave.PK_Customer_ID Select x).FirstOrDefault
                        Dim objDir As New WICDirectorDataBLL
                        If itemx IsNot Nothing Then
                            objDir = listDirector.Find(Function(x) x.ObjDirector.NO_ID = itemx.NO_ID)
                        End If
                        'agam 22102020
                        'Dim objcek As New goAML_Ref_Walk_In_Customer_Director
                        'objcek = objDir.ObjDirector
                        If objDir.ObjDirector Is Nothing And itemx IsNot Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If

                        If itemx IsNot Nothing Then

                            For Each itemxx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList
                                Dim objcekk As goAML_Ref_Address = objDirectors.ListDirectorAddress.Find(Function(x) x.PK_Customer_Address_ID = itemxx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 8 Select x).ToList
                                Dim objcekk As goAML_Ref_Phone = objDirectors.ListDirectorPhone.Find(Function(x) x.PK_goAML_Ref_Phone = itemxx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList
                                Dim objcekk As goAML_Ref_Address = objDirectors.ListDirectorAddressEmployer.Find(Function(x) x.PK_Customer_Address_ID = itemxx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = itemx.NO_ID And x.FK_Ref_Detail_Of = 4 Select x).ToList
                                Dim objcekk As goAML_Ref_Phone = objDirectors.ListDirectorPhoneEmployer.Find(Function(x) x.PK_goAML_Ref_Phone = itemxx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4)
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                            For Each itemxx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = itemx.NO_ID And x.FK_Person_Type = 7 Select x).ToList
                                Dim objcekk As New goAML_Person_Identification
                                'objcekk = objDirectors.ListDirectorIdentification.Find(Function(x) x.PK_Person_Identification_ID = itemxx.PK_Person_Identification_ID)
                                objcekk = objDirectors.ListDirectorIdentification.Where(Function(x) x.PK_Person_Identification_ID = itemxx.PK_Person_Identification_ID).FirstOrDefault
                                If objcekk Is Nothing Then
                                    objDB.Entry(itemxx).State = EntityState.Deleted
                                    'agam 20102020
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemxx)
                                End If
                            Next

                        End If

                    Next

                    For Each objDirectors As WICDirectorDataBLL In listDirector
                        Dim item As goAML_Ref_Walk_In_Customer_Director = objDirectors.ObjDirector
                        Dim obcek As goAML_Ref_Walk_In_Customer_Director = (From x In objDB.goAML_Ref_Walk_In_Customer_Director Where x.NO_ID = item.NO_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            With item
                                .FK_Entity_ID = objSave.PK_Customer_ID
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                            End With
                            objDB.Entry(item).State = EntityState.Added
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                            objDB.SaveChanges()
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If

                        For Each itemDetail As goAML_Ref_Address In objDirectors.ListDirectorAddress
                            Dim obcekDetail As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemDetail.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 And x.FK_To_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_To_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                                objDB.Entry(itemDetail).State = EntityState.Added
                            Else
                                'agam 22102020
                                ' With item
                                With obcekDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Address In objDirectors.ListDirectorAddressEmployer
                            Dim obcekDetail As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemDetail.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 And x.FK_To_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_To_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Phone In objDirectors.ListDirectorPhone
                            Dim obcekDetail As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemDetail.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 And x.FK_for_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_for_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Ref_Phone In objDirectors.ListDirectorPhoneEmployer
                            Dim obcekDetail As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemDetail.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 And x.FK_for_Table_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_for_Table_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                        For Each itemDetail As goAML_Person_Identification In objDirectors.ListDirectorIdentification
                            Dim obcekDetail As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemDetail.PK_Person_Identification_ID And x.FK_Person_Type = 7 And x.FK_Person_ID = item.NO_ID Select x).FirstOrDefault
                            If obcekDetail Is Nothing Then
                                'agam 22102020
                                ' With item
                                With itemDetail
                                    .FK_Person_ID = item.NO_ID
                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                End With
                                objDB.Entry(itemDetail).State = EntityState.Added
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            Else
                                With itemDetail
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                objDB.Entry(obcekDetail).CurrentValues.SetValues(itemDetail)
                                objDB.Entry(obcekDetail).State = EntityState.Modified
                                'agam 20102020
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, obcekDetail)
                            End If
                        Next

                    Next



                    For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList
                        Dim objcek As goAML_Ref_Address = listAddressDetail.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020

                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Address In listAddressDetail
                        ' Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                        Dim obcek As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = item.PK_Customer_Address_ID).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3 Select x).ToList
                        Dim objcek As goAML_Ref_Phone = listPhoneDetail.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Phone In listPhoneDetail
                        Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList
                        Dim objcek As goAML_Ref_Address = listAddressEmployerDetail.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Address In listAddressEmployerDetail
                        Dim obcek As goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10 Select x).ToList
                        Dim objcek As goAML_Ref_Phone = listPhoneEmployerDetail.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next
                    For Each item As goAML_Ref_Phone In listPhoneEmployerDetail
                        Dim obcek As goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    For Each itemx As goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objSave.PK_Customer_ID And x.FK_Person_Type = 8 Select x).ToList
                        Dim objcek As goAML_Person_Identification = listIdentificationDetail.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID And x.FK_Person_Type = 8)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = EntityState.Deleted
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                        End If
                    Next

                    For Each item As goAML_Person_Identification In listIdentificationDetail
                        Dim obcek As goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 8 Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            objDB.Entry(item).State = EntityState.Added
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Else
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            objDB.Entry(obcek).State = EntityState.Modified
                            'agam 20102020
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        End If
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Sub SaveEditApproval(objSave As goAML_Ref_WIC, listDirector As List(Of WICDirectorDataBLL), listAddressDetail As List(Of goAML_Ref_Address), listPhoneDetail As List(Of goAML_Ref_Phone), listAddressEmployerDetail As List(Of goAML_Ref_Address), listPhoneEmployerDetail As List(Of goAML_Ref_Phone), listIdentificationDetail As List(Of goAML_Person_Identification), objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New WICDataBLL With
                    {
                        .ObjWIC = objSave,
                        .ObjWICAddress = listAddressDetail,
                        .ObjWICPhone = listPhoneDetail,
                        .ObjWICAddressEmployer = listAddressEmployerDetail,
                        .OBjWICPhoneEmployer = listPhoneEmployerDetail,
                        .ObjWICIdentification = listIdentificationDetail,
                        .ObjDirector = listDirector
                    }
                    Dim dataXML As String = Common.Serialize(objData)

                    Dim objWICBefore As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = objSave.PK_Customer_ID).FirstOrDefault
                    Dim objWICAddressBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim objWICPhoneBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim objWICAddressEmployerBefore As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim objWICPhoneEmployerBefore As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objSave.PK_Customer_ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim objWICIdentificationBefore As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objSave.PK_Customer_ID And x.FK_Person_Type = 8).ToList

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirectorBefore As goAML_Ref_Walk_In_Customer_Director = New goAML_Ref_Walk_In_Customer_Director
                    Dim objWICDirectorAddressBefore As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim objWICDirectorPhoneBefore As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim objWICDirectorEmployerAddressBefore As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim objWICDirectorEmployerPhoneBefore As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim objWICDirectorEmployerIdentificationBefore As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)

                    If listDirector.Count > 0 Then

                        Dim Director As New WICDirectorDataBLL
                        For Each DirectorClass In listDirector
                            objWICDirectorBefore = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = objSave.PK_Customer_ID).FirstOrDefault
                            objWICDirectorAddressBefore = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = DirectorClass.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            objWICDirectorPhoneBefore = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = DirectorClass.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            objWICDirectorEmployerAddressBefore = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = DirectorClass.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            objWICDirectorEmployerPhoneBefore = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = DirectorClass.ObjDirector.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            objWICDirectorEmployerIdentificationBefore = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = DirectorClass.ObjDirector.NO_ID And x.FK_Person_Type = 7).ToList

                            With Director
                                .ObjDirector = objWICDirectorBefore
                                .ListDirectorAddress = objWICDirectorAddressBefore
                                .ListDirectorPhone = objWICDirectorPhoneBefore
                                .ListDirectorAddressEmployer = objWICDirectorEmployerAddressBefore
                                .ListDirectorPhoneEmployer = objWICDirectorEmployerPhoneBefore
                                .ListDirectorIdentification = objWICDirectorEmployerIdentificationBefore
                            End With
                            objDirectorClass.Add(Director)
                        Next

                    End If

                    Dim objDataBefore As New WICDataBLL With
                    {
                        .ObjWIC = objWICBefore,
                        .ObjWICAddress = objWICAddressBefore,
                        .ObjWICPhone = objWICPhoneBefore,
                        .ObjWICAddressEmployer = objWICAddressEmployerBefore,
                        .OBjWICPhoneEmployer = objWICPhoneEmployerBefore,
                        .ObjDirector = objDirectorClass,
                        .ObjWICIdentification = objWICIdentificationBefore
                    }
                    Dim dataXMLBefore As String = Common.Serialize(objDataBefore)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objSave.PK_Customer_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objSave, objWICBefore)
                    objDB.SaveChanges()

                    If listDirector.Count > 0 Then

                        For Each itemDetail As WICDirectorDataBLL In listDirector
                            Dim item As goAML_Ref_Walk_In_Customer_Director = itemDetail.ObjDirector
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemDetail, objWICDirectorBefore)

                            If itemDetail.ListDirectorAddress.Count > 0 Then
                                For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddress
                                    Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 8 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemD As goAML_Ref_Address In itemDetail.ListDirectorAddressEmployer
                                    Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 4 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorPhone.Count > 0 Then
                                For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhone
                                    Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 8 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemD As goAML_Ref_Phone In itemDetail.ListDirectorPhoneEmployer
                                    Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 4 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If

                            If itemDetail.ListDirectorIdentification.Count > 0 Then
                                For Each itemD As goAML_Person_Identification In itemDetail.ListDirectorIdentification
                                    Dim items As goAML_Person_Identification = objDB.goAML_Person_Identification.Where(Function(x) x.PK_Person_Identification_ID = itemD.PK_Person_Identification_ID And x.FK_Person_Type = 7 And x.FK_Person_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                                Next
                            End If
                        Next

                    End If

                    If listAddressDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Address In listAddressDetail
                            Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 3 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listPhoneDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Phone In listPhoneDetail
                            Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 3 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listAddressEmployerDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Address In listAddressEmployerDetail
                            Dim items As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = itemD.PK_Customer_Address_ID And x.FK_Ref_Detail_Of = 10 And x.FK_To_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listPhoneEmployerDetail.Count > 0 Then
                        For Each itemD As goAML_Ref_Phone In listPhoneEmployerDetail
                            Dim items As goAML_Ref_Phone = objDB.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = itemD.PK_goAML_Ref_Phone And x.FK_Ref_Detail_Of = 10 And x.FK_for_Table_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    If listIdentificationDetail.Count > 0 Then
                        For Each itemD As goAML_Person_Identification In listIdentificationDetail
                            Dim items As goAML_Person_Identification = objDB.goAML_Person_Identification.Where(Function(x) x.PK_Person_Identification_ID = itemD.PK_Person_Identification_ID And x.FK_Person_Type = 8 And x.FK_Person_ID = objWICBefore.PK_Customer_ID).FirstOrDefault
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemD, items)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Sub DeleteTanpaapproval(ID As String, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objWIC As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
                    Dim listWICAddress As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICPhone As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICAddressEmployer As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICPhoneEmployer As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICIdentification As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID And x.FK_Person_Type = 8).ToList
                    Dim listWICDirector As List(Of goAML_Ref_Walk_In_Customer_Director) = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = ID).ToList

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirector As goAML_Ref_Walk_In_Customer_Director = New goAML_Ref_Walk_In_Customer_Director
                    Dim listWICDirectorAddress As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICDirectorPhone As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICDirectorEmployerAddress As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICDirectorEmployerPhone As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICDirectorIdentification As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)

                    If listWICDirector.Count > 0 Then

                        Dim DirectorClass As New WICDirectorDataBLL
                        For Each director In listWICDirector

                            listWICDirectorAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICDirectorEmployerAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICDirectorPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICDirectorEmployerPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICDirectorIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = director.NO_ID And x.FK_Person_Type = 7).ToList

                            With DirectorClass
                                .ObjDirector = director
                                .ListDirectorAddress = listWICDirectorAddress
                                .ListDirectorPhone = listWICDirectorPhone
                                .ListDirectorAddressEmployer = listWICDirectorEmployerAddress
                                .ListDirectorPhoneEmployer = listWICDirectorEmployerPhone
                                .ListDirectorIdentification = listWICDirectorIdentification
                            End With
                            objDirectorClass.Add(DirectorClass)
                        Next

                    End If
                    objDB.Entry(objWIC).State = EntityState.Deleted

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objWIC)

                    If objDirectorClass.Count > 0 Then

                        For Each objDir As WICDirectorDataBLL In objDirectorClass
                            Dim item As goAML_Ref_Walk_In_Customer_Director = objDir.ObjDirector
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)

                            If objDir.ListDirectorAddress.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddress
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddressEmployer
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhone.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhone
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhoneEmployer
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorIdentification.Count > 0 Then
                                For Each itemx As goAML_Person_Identification In objDir.ListDirectorIdentification
                                    objDB.Entry(itemx).State = EntityState.Deleted
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                        Next
                    End If

                    If listWICAddress.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddress
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhone.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhone
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICAddressEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddressEmployer
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhoneEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhoneEmployer
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICIdentification.Count > 0 Then
                        For Each item As goAML_Person_Identification In listWICIdentification
                            objDB.Entry(item).State = EntityState.Deleted
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Sub DeleteDenganapproval(ID As String, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objWIC As goAML_Ref_WIC = objDB.goAML_Ref_WIC.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
                    Dim listWICDirector As List(Of goAML_Ref_Walk_In_Customer_Director) = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = ID).ToList
                    Dim listWICAddress As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICPhone As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 3).ToList
                    Dim listWICAddressEmployer As List(Of goAML_Ref_Address) = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICPhoneEmployer As List(Of goAML_Ref_Phone) = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 10).ToList
                    Dim listWICIdentification As List(Of goAML_Person_Identification) = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = ID And x.FK_Person_Type = 8).ToList

                    Dim objDirectorClass As New List(Of WICDirectorDataBLL)
                    Dim objWICDirector As goAML_Ref_Walk_In_Customer_Director = New goAML_Ref_Walk_In_Customer_Director
                    Dim listWICAddressDirector As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICPhoneDirector As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICAddressDirectorEmployer As List(Of goAML_Ref_Address) = New List(Of goAML_Ref_Address)
                    Dim listWICPhoneDirectorEmployer As List(Of goAML_Ref_Phone) = New List(Of goAML_Ref_Phone)
                    Dim listWICIdentificationDirector As List(Of goAML_Person_Identification) = New List(Of goAML_Person_Identification)

                    If listWICDirector.Count > 0 Then

                        Dim DirectorClass As New WICDirectorDataBLL
                        For Each Director In listWICDirector

                            listWICAddressDirector = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICPhoneDirector = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 8).ToList
                            listWICAddressDirectorEmployer = objDB.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICPhoneDirectorEmployer = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = Director.NO_ID And x.FK_Ref_Detail_Of = 4).ToList
                            listWICIdentificationDirector = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = Director.NO_ID And x.FK_Person_Type = 7).ToList

                            With DirectorClass
                                .ObjDirector = Director
                                .ListDirectorAddress = listWICAddressDirector
                                .ListDirectorPhone = listWICPhoneDirector
                                .ListDirectorAddressEmployer = listWICAddressDirectorEmployer
                                .ListDirectorPhoneEmployer = listWICPhoneDirectorEmployer
                                .ListDirectorIdentification = listWICIdentificationDirector
                            End With
                            objDirectorClass.Add(DirectorClass)

                        Next

                    End If
                    Dim objWICDataBLL As New WICDataBLL With
                    {
                        .ObjWIC = objWIC,
                        .ObjWICAddress = listWICAddress,
                        .ObjWICPhone = listWICPhone,
                        .ObjWICAddressEmployer = listWICAddressEmployer,
                        .OBjWICPhoneEmployer = listWICPhoneEmployer,
                        .ObjWICIdentification = listWICIdentification,
                        .ObjDirector = objDirectorClass
                    }

                    Dim dataXML As String = Common.Serialize(objWICDataBLL)
                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Delete
                        .CreatedDate = Now
                        .CreatedBy = Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, objWIC)

                    If objDirectorClass.Count > 0 Then

                        For Each objDir As WICDirectorDataBLL In objDirectorClass
                            Dim item As goAML_Ref_Walk_In_Customer_Director = objDir.ObjDirector
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)

                            If objDir.ListDirectorAddress.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddress
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorAddressEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Address In objDir.ListDirectorAddressEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhone.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhone
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorPhoneEmployer.Count > 0 Then
                                For Each itemx As goAML_Ref_Phone In objDir.ListDirectorPhoneEmployer
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                            If objDir.ListDirectorIdentification.Count > 0 Then
                                For Each itemx As goAML_Person_Identification In objDir.ListDirectorIdentification
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, itemx)
                                Next
                            End If

                        Next
                    End If

                    If listWICAddress.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddress
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhone.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhone
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICAddressEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Address In listWICAddressEmployer
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICPhoneEmployer.Count > 0 Then
                        For Each item As goAML_Ref_Phone In listWICPhoneEmployer
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If listWICIdentification.Count > 0 Then
                        For Each item As goAML_Person_Identification In listWICIdentification
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
End Class
