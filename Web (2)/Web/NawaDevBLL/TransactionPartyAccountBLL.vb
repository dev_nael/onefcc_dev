﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionPartyAccountBLL
    Public Shared Sub InsertTransactionPartyAccount(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                    itemObjTransactionAccountParty = Transactions.objAccountParty

                    With itemObjTransactionAccountParty
                        .FK_Trn_Party_ID = Transactions.objTransactionParty.PK_Trn_Party_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionAccountParty).State = EntityState.Added

                    objdb.SaveChanges()

                    If Not Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID = 0 Then
                        TransactionPartyAccountEntityBLL.InsertTransactionPartyAccountEntity(Transactions, Audit)
                    End If

                    '------------------------------------Signatory PArty--------------------------------------------
                    TransactionPartyAccountSignatoryBLL.InsertTransactionPartyAccountSignatory(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception

                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyAccount(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                    itemObjTransactionAccountParty = Transactions.objAccountParty

                    objdb.Entry(itemObjTransactionAccountParty).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Not Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID = 0 Then
                        TransactionPartyAccountEntityBLL.DeleteTransactionPartyAccountEntity(Transactions, Audit)
                    End If

                    '----------------------------------------Signatory Account Transaction Party----------------------
                    TransactionPartyAccountSignatoryBLL.DeleteTransactionPartyAccountSignatory(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyAccount(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim TransactionPartyAccount As goAML_Trn_Party_Account = Transactions.objAccountParty
                    Dim TransactionPartyAccountCek As goAML_Trn_Party_Account = (From x In objdb.goAML_Trn_Party_Account Where x.PK_Trn_Party_Account_ID = TransactionPartyAccount.PK_Trn_Party_Account_ID Select x).FirstOrDefault

                    If TransactionPartyAccountCek Is Nothing Then
                        InsertTransactionPartyAccount(Transactions, Audit)
                    Else

                        objdb.Entry(TransactionPartyAccountCek).CurrentValues.SetValues(TransactionPartyAccount)
                        objdb.Entry(TransactionPartyAccountCek).State = EntityState.Modified

                        objdb.SaveChanges()

                        If Not Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID = 0 Then
                            Dim TransactionPartyAccountEntityx As goAML_Trn_Par_Acc_Entity = (From x In objdb.goAML_Trn_Par_Acc_Entity Where x.FK_Trn_Party_Account_ID = Transactions.objAccountParty.PK_Trn_Party_Account_ID Select x).FirstOrDefault
                            If Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID <> TransactionPartyAccountEntityx.PK_Trn_Par_acc_Entity_ID Then
                                TransactionPartyAccountEntityBLL.DeleteTransactionPartyAccountEntity(Transactions, Audit)
                            Else
                                TransactionPartyAccountEntityBLL.UpdateTransactionPartyAccountEntity(Transactions, Audit)
                            End If
                        End If

                        If Transactions.listSignatoryAccountParty.Count > 0 Then
                            TransactionPartyAccountSignatoryBLL.UpdateTransactionPartyAccountSignatory(Transactions, Audit)
                        End If
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using


    End Sub
End Class
