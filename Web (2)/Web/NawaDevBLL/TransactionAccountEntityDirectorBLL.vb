﻿Imports System.Data.Entity
Imports System.Reflection
Imports NawaBLL
Imports NawaDevDAL
Public Class TransactionAccountEntityDirectorBLL
    Public Shared Sub InsertTransactionAccountEntityDirectoryFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    '------------------------Director Entity Account Transaction From---------------------------------------
                    For Each objModuledataDirectorEntityAccountFrom As DirectorClass In Transactions.listDirectorAccountEntityFrom
                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector
                        With itemObjDirector
                            .FK_Entity_ID = Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With
                        objdb.Entry(itemObjDirector).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjDirector.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjDirector, Nothing) Is Nothing, item.GetValue(itemObjDirector, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                With DirectorPhone
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhone).State = EntityState.Added

                                'objtypeReport = DirectorPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhone, Nothing) Is Nothing, item.GetValue(DirectorPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                With DirectorAddress
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddress).State = EntityState.Added

                                'objtypeReport = DirectorAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddress, Nothing) Is Nothing, item.GetValue(DirectorAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                With DirectorAddressEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Added

                                'objtypeReport = DirectorAddressEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddressEmployer, Nothing) Is Nothing, item.GetValue(DirectorAddressEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector
                                With DirectorPhoneEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Added

                                'objtypeReport = DirectorPhoneEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhoneEmployer, Nothing) Is Nothing, item.GetValue(DirectorPhoneEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                With DirectorIdentification
                                    .FK_Person_ID = objModuledataDirectorEntityAccountFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorIdentification).State = EntityState.Added

                                'objtypeReport = DirectorIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorIdentification, Nothing) Is Nothing, item.GetValue(DirectorIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If
                    Next
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using

        End Using

    End Sub

    Public Shared Sub InsertTransactionAccountEntityDirectoryTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityAccountTo As DirectorClass In Transactions.listDirectorAccountEntityto

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector

                        With itemObjDirector
                            .FK_Entity_ID = Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With

                        objdb.Entry(itemObjDirector).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjDirector.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjDirector, Nothing) Is Nothing, item.GetValue(itemObjDirector, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                With DirectorPhone
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhone).State = EntityState.Added

                                'objtypeReport = DirectorPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhone, Nothing) Is Nothing, item.GetValue(DirectorPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                With DirectorAddress
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddress).State = EntityState.Added

                                'objtypeReport = DirectorAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddress, Nothing) Is Nothing, item.GetValue(DirectorAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                With DirectorAddressEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Added

                                'objtypeReport = DirectorAddressEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddressEmployer, Nothing) Is Nothing, item.GetValue(DirectorAddressEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                With DirectorPhoneEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityAccountTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Added

                                'objtypeReport = DirectorPhoneEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhoneEmployer, Nothing) Is Nothing, item.GetValue(DirectorPhoneEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                With DirectorIdentification
                                    .FK_Person_ID = objModuledataDirectorEntityAccountTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorIdentification).State = EntityState.Added

                                'objtypeReport = DirectorIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorIdentification, Nothing) Is Nothing, item.GetValue(DirectorIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If
                        objdb.SaveChanges()
                    Next
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountEntityDirectoryFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityAccountFrom As DirectorClass In Transactions.listDirectorAccountEntityFrom

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector

                        objdb.Entry(itemObjDirector).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                objdb.Entry(DirectorPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                objdb.Entry(DirectorAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                objdb.Entry(DirectorIdentification).State = EntityState.Deleted
                            Next
                        End If
                        objdb.SaveChanges()
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountEntityDirectoryTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityAccountTo As DirectorClass In Transactions.listDirectorAccountEntityto
                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector

                        objdb.Entry(itemObjDirector).State = EntityState.Deleted
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                objdb.Entry(DirectorPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                objdb.Entry(DirectorAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                objdb.Entry(DirectorIdentification).State = EntityState.Deleted
                            Next
                        End If
                        objdb.SaveChanges()
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
