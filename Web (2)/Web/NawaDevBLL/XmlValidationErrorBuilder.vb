﻿
Imports System.Xml.Schema

Public Class XmlValidationErrorBuilder
    Private _errors As New List(Of String)
    Public Sub ValidationEventHandler(ByVal sender As Object, ByVal args As ValidationEventArgs)
        If args.Severity = XmlSeverityType.Error Then
            _errors.Add(args.Message)

        End If
    End Sub
    Public Function GetErrors() As List(Of String)
        Return _errors
    End Function
End Class
