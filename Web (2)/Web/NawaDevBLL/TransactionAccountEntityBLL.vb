﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionAccountEntityBLL

    Public Shared Sub InsertTransactionAccountEntityFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                    itemObjTransactionAccountEntityFrom = Transactions.objAccountEntityFrom

                    With itemObjTransactionAccountEntityFrom
                        .FK_Account_ID = Transactions.objAccountFrom.PK_Account_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionAccountEntityFrom).State = EntityState.Added

                    'Dim objtypeReport As Type = itemObjTransactionAccountEntityFrom.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjTransactionAccountEntityFrom, Nothing) Is Nothing, item.GetValue(itemObjTransactionAccountEntityFrom, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneAccountEntityFrom.Count > 0 Then

                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In Transactions.listObjPhoneAccountEntityFrom
                            With EntityPhone
                                .FK_Trn_Acc_Entity = Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhone).State = EntityState.Added

                            'objtypeReport = EntityPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityPhone, Nothing) Is Nothing, item.GetValue(EntityPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next

                    End If

                    If Transactions.listObjAddressAccountEntityFrom.Count > 0 Then

                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In Transactions.listObjAddressAccountEntityFrom
                            With EntityAddress
                                .FK_Trn_Acc_Entity = Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddress).State = EntityState.Added

                            'objtypeReport = EntityAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityAddress, Nothing) Is Nothing, item.GetValue(EntityAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next

                    End If
                    objdb.SaveChanges()

                    TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryFrom(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub InsertTransactionAccountEntityTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                    itemObjTrnEntityAccount = Transactions.objAccountEntityTo

                    With itemObjTrnEntityAccount
                        .FK_Account_ID = Transactions.objAccountTo.PK_Account_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTrnEntityAccount).State = EntityState.Added

                    'Dim objtypeReport As Type = itemObjTrnEntityAccount.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjTrnEntityAccount, Nothing) Is Nothing, item.GetValue(itemObjTrnEntityAccount, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneAccountEntityto.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In Transactions.listObjPhoneAccountEntityto
                            With EntityPhone
                                .FK_Trn_Acc_Entity = Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhone).State = EntityState.Added

                            'objtypeReport = EntityPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityPhone, Nothing) Is Nothing, item.GetValue(EntityPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressAccountEntityto.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In Transactions.listObjAddressAccountEntityto
                            With EntityAddress
                                .FK_Trn_Acc_Entity = Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddress).State = EntityState.Added

                            'objtypeReport = EntityAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityAddress, Nothing) Is Nothing, item.GetValue(EntityAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If
                    objdb.SaveChanges()

                    '-------------------------------Director Entity Account Transaction----------------------------------------------
                    TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryTo(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountEntityFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                    itemObjTransactionAccountEntityFrom = Transactions.objAccountEntityFrom

                    objdb.Entry(itemObjTransactionAccountEntityFrom).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Transactions.listObjPhoneAccountEntityFrom.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In Transactions.listObjPhoneAccountEntityFrom
                            objdb.Entry(EntityPhone).State = EntityState.Deleted
                        Next
                    End If

                    If Transactions.listObjAddressAccountEntityFrom.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In Transactions.listObjAddressAccountEntityFrom
                            objdb.Entry(EntityAddress).State = EntityState.Deleted
                        Next
                    End If
                    objdb.SaveChanges()

                    '------------------------Director Entity Account Transaction From---------------------------------------
                    TransactionAccountEntityDirectorBLL.DeleteTransactionAccountEntityDirectoryFrom(Transactions, Audit)


                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountEntityTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                    itemObjTrnEntityAccount = Transactions.objAccountEntityTo

                    objdb.Entry(itemObjTrnEntityAccount).State = EntityState.Deleted

                    'Dim objtypeReport As Type = itemObjTrnEntityAccount.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '        {
                    '            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '            .FieldName = item.Name,
                    '            .OldValue = "",
                    '            .NewValue = If(Not item.GetValue(itemObjTrnEntityAccount, Nothing) Is Nothing, item.GetValue(itemObjTrnEntityAccount, Nothing), "")
                    '        }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneAccountEntityto.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In Transactions.listObjPhoneAccountEntityto
                            objdb.Entry(EntityPhone).State = EntityState.Deleted

                            'objtypeReport = EntityPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '        {
                            '            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '            .FieldName = item.Name,
                            '            .OldValue = "",
                            '            .NewValue = If(Not item.GetValue(EntityPhone, Nothing) Is Nothing, item.GetValue(EntityPhone, Nothing), "")
                            '        }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressAccountEntityto.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In Transactions.listObjAddressAccountEntityto
                            objdb.Entry(EntityAddress).State = EntityState.Deleted

                            'objtypeReport = EntityAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '        {
                            '            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '            .FieldName = item.Name,
                            '            .OldValue = "",
                            '            .NewValue = If(Not item.GetValue(EntityAddress, Nothing) Is Nothing, item.GetValue(EntityAddress, Nothing), "")
                            '        }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If
                    objdb.SaveChanges()

                    '-------------------------------Director Entity Account Transaction----------------------------------------------
                    TransactionAccountEntityDirectorBLL.DeleteTransactionAccountEntityDirectoryTo(Transactions, Audit)

                    '-------------------------------End Director Entity Account Transaction----------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

End Class
