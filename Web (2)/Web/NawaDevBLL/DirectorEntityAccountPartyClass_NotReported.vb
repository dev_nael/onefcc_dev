﻿Public Class DirectorEntityAccountPartyClass_NotReported
    Public objDirectorEntityAccountParty As New NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported
    Public listAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
    Public listPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
    Public listAddressEmployer As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
    Public listPhoneEmployer As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
    Public listIdentification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

End Class

