﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports NawaDevDAL
Imports System.IO
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml

Public Class NotReportedBLL
#Region "Get Data"
#Region "Bi-Party"
    Shared Function getNotReportedByPk(id As String) As NawaDevDAL.GoAML_Report_NotReported
        Dim objNotReported As New NawaDevDAL.GoAML_Report_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim NotReported As New NawaDevDAL.GoAML_Report_NotReported
            NotReported = objdb.Database.SqlQuery(Of NawaDevDAL.GoAML_Report_NotReported)("select * from GoAML_Report_NotReported with(nolock) where Pk_GoAML_Report_NotReported = " & id).FirstOrDefault
            If NotReported IsNot Nothing Then
                objNotReported = NotReported
            End If
        End Using
        Return objNotReported
    End Function
    Shared Function getListIndicatorByFk(Id As String) As List(Of NawaDevDAL.goAML_Report_Indicator_NotReported) ''Edited by Felix 11 Jan 2021, ganti parameter ID dari as Integer jadi as String, karena ganti jadi Bi
        Dim objIndicator As New List(Of NawaDevDAL.goAML_Report_Indicator_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator_NotReported)
            Indicator = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Report_Indicator_NotReported)("select * from goAML_Report_Indicator_NotReported with(nolock) where FK_Report = " & Id).ToList
            If Indicator IsNot Nothing Then
                objIndicator = Indicator
            End If
        End Using
        Return objIndicator
    End Function
    Shared Function getListTransactionById(id As Integer) As List(Of NawaDevDAL.goAML_Transaction_NotReported)
        Dim listTransaction As New List(Of NawaDevDAL.goAML_Transaction_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New List(Of NawaDevDAL.goAML_Transaction_NotReported)
            Transaction = objdb.goAML_Transaction_NotReported.Where(Function(x) x.FK_Report_ID = id).ToList
            If Transaction IsNot Nothing Then
                listTransaction = Transaction
            End If
        End Using
        Return listTransaction
    End Function

    'Start Conductor
    Shared Function getConductorById(id As Integer) As NawaDevDAL.goaml_Trn_Conductor_NotReported
        Dim objConductor As New NawaDevDAL.goaml_Trn_Conductor_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Conductor As New NawaDevDAL.goaml_Trn_Conductor_NotReported
            Conductor = objdb.goaml_Trn_Conductor_NotReported.Where(Function(x) x.FK_Transaction_ID = id).FirstOrDefault
            If Conductor IsNot Nothing Then
                objConductor = Conductor
            End If
        End Using
        Return objConductor
    End Function
    Shared Function getConductorPhoneById(id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)
        Dim listConductorPhone As New List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)

        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities

            'objdb.Database.CommandTimeout = 3600
            Dim ConductorPhone As New List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)
            ConductorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)("select * from goaml_trn_Conductor_Phone_NotReported with(nolock) where FK_Trn_Conductor_ID =" & id & " and isemployer ='" & isEmp & "'").ToList
            If ConductorPhone IsNot Nothing Then
                listConductorPhone = ConductorPhone
            End If
        End Using
        Return listConductorPhone
    End Function
    Shared Function getConductorAddressById(id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)
        Dim listConductorAddress As New List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ConductorAddress As New List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)
            ConductorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)("select * from goaml_Trn_Conductor_Address_NotReported with(nolock) where FK_Trn_Conductor_ID =" & id & " and isemployer ='" & isEmp & "'").ToList
            If ConductorAddress IsNot Nothing Then
                listConductorAddress = ConductorAddress
            End If
        End Using
        Return listConductorAddress
    End Function
    'End Conductor ----------------------------------------------------------------------------------------------------------------------

    'Start Transaction From Account
    Shared Function getTransactionAccountByFK(id As Integer, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Account_NotReported
        Dim objFromAccount As New NawaDevDAL.goAML_Transaction_Account_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Transaction_Account_NotReported
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Account_NotReported)("select * from goAML_Transaction_Account_NotReported with(nolock) where FK_Report_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    Shared Function getTransactionAccountEntityByFK(AccID As Integer, FromTO As Integer) As NawaDevDAL.goAML_Trn_Entity_account_NotReported
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Trn_Entity_account_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Trn_Entity_account_NotReported
            'AccountEntity = objdb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = AccID And x.FK_From_Or_To = FromTO).FirstOrDefault
            AccountEntity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_account_NotReported)("select * from goAML_Trn_Entity_account_NotReported with(nolock) where FK_Account_ID = " & AccID & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListTransactionAccountEntityPhoneById(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)
            'AccountEntityPhone = objdb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Id).ToList
            AccountEntityPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)("select * from goAML_Trn_Acc_Entity_Phone_NotReported with(nolock) where FK_Trn_Acc_Entity = " & Id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListTransactionAccountEntityAddressById(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)
            'AccountEntityAddress = objdb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Id).ToList
            AccountEntityAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)("select * from goAML_Trn_Acc_Entity_Address_NotReported with(nolock) where FK_Trn_Acc_Entity = " & Id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    Shared Function getListTransactionAccountEntityDirectorById(EntityId As Integer, FromTO As Integer, SenderId As Integer) As List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Director_NotReported)
            'AccountEntityDirector = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityId And x.FK_From_Or_To = FromTO And x.FK_Sender_Information = SenderId).ToList
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Director_NotReported)("select * from goAML_Trn_Director_NotReported with(nolock) where FK_Entity_ID = " & EntityId & " and FK_From_Or_To = " & FromTO & " and FK_Sender_Information = " & SenderId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionAccountEntityDirectorPhoneById(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)
            'AccountEntityDirectorPhone = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = Id And x.isEmployer = IsEmp).ToList
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goaml_trn_Director_Phone_NotReported)("select * from goaml_trn_Director_Phone_NotReported with(nolock) where FK_Trn_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListTransactionAccountEntityDirectorAddressById(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)
            'AccountEntityDirectorAddress = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = Id And x.isEmployer = IsEmp).ToList
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Director_Address_NotReported)("select * from goAML_Trn_Director_Address_NotReported with(nolock) where FK_Trn_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    Shared Function getListTransactionIdentificationById(Id As Integer, TypeID As Integer, FromTO As Integer) As List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)
            'Identification = objdb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Id And x.FK_Person_Type = TypeID And x.from_or_To_Type = FromTO).ToList
            Identification = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)("select * from goAML_Transaction_Person_Identification_NotReported with(nolock) where FK_Person_ID = " & Id & " and FK_Person_Type = " & TypeID & " and from_or_To_Type = " & FromTO).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Signatory
    Shared Function getListTransactionAccountSignatoryById(TranAccID As Integer, FromTO As Integer) As List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)
            'signatory = objdb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = TranAccID And x.FK_From_Or_To = FromTO).ToList
            signatory = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_acc_Signatory_NotReported)("select * from goAML_Trn_acc_Signatory_NotReported with(nolock) where FK_Transaction_Account_ID = " & TranAccID & " and FK_From_Or_To = " & FromTO).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListTransactionAccountSignatoryPhoneById(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)
            'Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Id And x.isEmployer = isEmp).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_acc_sign_Phone_NotReported)("select * from goAML_trn_acc_sign_Phone_NotReported with(nolock) where FK_Trn_Acc_Entity = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListTransactionAccountSignatoryAddressById(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)
            'Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Id And x.isEmployer = isEmp).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_sign_Address_NotReported)("select * from goAML_Trn_Acc_sign_Address_NotReported with(nolock) where FK_Trn_Acc_Entity = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'End Transaction From Account --------------------------------------------------------------------------------------------------------------

    'Start Transaction From Person
    Shared Function getTransactionPersonByFK(id As Integer, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Person_NotReported
        Dim objFromPerson As New NawaDevDAL.goAML_Transaction_Person_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Transaction_Person_NotReported
            'Person = objdb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Person_NotReported)("select * from goAML_Transaction_Person_NotReported with(nolock) where FK_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListTransactionPersonPhoneById(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
            'Phone = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = Id And x.isEmployer = isEmp).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)("select * from goAML_trn_Person_Phone_NotReported with(nolock) where FK_Trn_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionPersonAddressById(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
            'Address = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = Id And x.isEmployer = isEmp).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)("select * from goAML_Trn_Person_Address_NotReported with(nolock) where FK_Trn_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Transaction From Person ----------------------------------------------------------------------------------------------------------------
    'Start Transaction From Entity
    Shared Function getTransactionEntityByFK(id As Integer, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Entity_NotReported
        Dim objFromEntity As New NawaDevDAL.goAML_Transaction_Entity_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Transaction_Entity_NotReported
            'Entity = objdb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Entity_NotReported)("select * from goAML_Transaction_Entity_NotReported with(nolock) where FK_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Entity IsNot Nothing Then
                objFromEntity = Entity
            End If
        End Using
        Return objFromEntity
    End Function
    Shared Function getListTransactionEntityPhoneById(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)
            'Phone = objdb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = Id).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)("select * from goAML_Trn_Entity_Phone_NotReported with(nolock) where FK_Trn_Entity = " & Id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionEntityAddressById(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)
            'Address = objdb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = Id).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)("select * from goAML_Trn_Entity_Address_NotReported with(nolock) where FK_Trn_Entity = " & Id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Transaction From Entity ----------------------------------------------------------------------------------------------------------------
#End Region

#Region "Multi-Party"
    'Transaction Multi-Party
    Shared Function getTransactionByIdParty(id As Integer) As NawaDevDAL.goAML_Transaction_Party_NotReported
        Dim objTransaction As New NawaDevDAL.goAML_Transaction_Party_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New NawaDevDAL.goAML_Transaction_Party_NotReported
            Transaction = objdb.goAML_Transaction_Party_NotReported.Where(Function(x) x.FK_Transaction_ID = id).FirstOrDefault
            If Transaction IsNot Nothing Then
                objTransaction = Transaction
            End If
        End Using
        Return objTransaction
    End Function
    'Account Entity Director--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionEntityAccountDirectorByIdParty(EntityId As Integer) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_NotReported)
            AccountEntityDirector = objdb.goAML_Trn_Par_Acc_Ent_Director_NotReported.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionAccountEntityDirectorPhoneByIdParty(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported)
            AccountEntityDirectorPhone = objdb.goAML_Trn_Par_Acc_Ent_Director_Phone_NotReported.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = Id And x.isemployer = IsEmp).ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListTransactionAccountEntityDirectorAddressByIdParty(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported)
            AccountEntityDirectorAddress = objdb.goAML_Trn_Par_Acc_Ent_Director_Address_NotReported.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = Id And x.isemployer = IsEmp).ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    'Identification Party--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionIdentificationByIdParty(Id As Integer, TypeID As Integer, ReportID As String) As List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)
            Identification = objdb.goAML_Transaction_Party_Identification_NotReported.Where(Function(x) x.FK_Person_ID = Id And x.FK_Person_Type = TypeID And x.FK_Report_ID = ReportID).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Entity Director--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionEntitytDirectorByIdParty(EntityId As Integer) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_NotReported)
            AccountEntityDirector = objdb.goAML_Trn_Par_Entity_Director_NotReported.Where(Function(x) x.FK_Trn_Party_Entity_ID = EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionEntityDirectorPhoneByIdParty(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        Dim objFromEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone_NotReported)
            EntityDirectorPhone = objdb.goAML_Trn_Par_Entity_Director_Phone_NotReported.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = Id And x.isemployer = IsEmp).ToList
            If EntityDirectorPhone IsNot Nothing Then
                objFromEntityDirectorPhone = EntityDirectorPhone
            End If
        End Using
        Return objFromEntityDirectorPhone
    End Function
    Shared Function getListTransactionEntityDirectorAddressByIdParty(Id As Integer, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        Dim objFromEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address_NotReported)
            EntityDirectorAddress = objdb.goAML_Trn_Par_Entity_Director_Address_NotReported.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = Id And x.isemployer = IsEmp).ToList
            If EntityDirectorAddress IsNot Nothing Then
                objFromEntityDirectorAddress = EntityDirectorAddress
            End If
        End Using
        Return objFromEntityDirectorAddress
    End Function
    'Signatory--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionAccountSignatoryByIdParty(TranAccID As Integer) As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory_NotReported)
            signatory = objdb.goAML_Trn_par_acc_Signatory_NotReported.Where(Function(x) x.FK_Trn_Party_Account_ID = TranAccID).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListTransactionAccountSignatoryPhoneByIdParty(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone_NotReported)
            Phone = objdb.goAML_trn_par_acc_sign_Phone_NotReported.Where(Function(x) x.FK_Trn_Par_Acc_Sign = Id And x.isEmployer = isEmp).ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListTransactionAccountSignatoryAddressByIdParty(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address_NotReported)
            Address = objdb.goAML_Trn_par_Acc_sign_Address_NotReported.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = Id And x.isEmployer = isEmp).ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'Account--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionAccountByFKParty(id As Integer) As NawaDevDAL.goAML_Trn_Party_Account_NotReported
        Dim objFromAccount As New NawaDevDAL.goAML_Trn_Party_Account_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Trn_Party_Account_NotReported
            Account = objdb.goAML_Trn_Party_Account_NotReported.Where(Function(x) x.FK_Trn_Party_ID = id).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    Shared Function getTransactionAccountEntityByFKParty(AccID As Integer) As NawaDevDAL.goAML_Trn_Par_Acc_Entity_NotReported
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Trn_Par_Acc_Entity_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Trn_Par_Acc_Entity_NotReported
            AccountEntity = objdb.goAML_Trn_Par_Acc_Entity_NotReported.Where(Function(x) x.FK_Trn_Party_Account_ID = AccID).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListTransactionAccountEntityPhoneByIdParty(Id As Integer) As List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported)
            AccountEntityPhone = objdb.goAML_trn_par_acc_Entity_Phone_NotReported.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = Id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListTransactionAccountEntityAddressByIdParty(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported)
            AccountEntityAddress = objdb.goAML_Trn_par_Acc_Entity_Address_NotReported.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = Id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    'Person--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionPersonByFKParty(id As Integer) As NawaDevDAL.goAML_Trn_Party_Person_NotReported
        Dim objFromPerson As New NawaDevDAL.goAML_Trn_Party_Person_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Trn_Party_Person_NotReported
            Person = objdb.goAML_Trn_Party_Person_NotReported.Where(Function(x) x.FK_Transaction_Party_ID = id).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListTransactionPersonPhoneByIdParty(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported)
            Phone = objdb.goAML_Trn_Party_Person_Phone_NotReported.Where(Function(x) x.FK_Trn_Party_Person_ID = Id And x.isemployer = isEmp).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionPersonAddressByIdParty(Id As Integer, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported)
            Address = objdb.goAML_Trn_Party_Person_Address_NotReported.Where(Function(x) x.FK_Trn_Party_Person_ID = Id And x.isemployer = isEmp).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'Entity--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionEntityByFKParty(id As Integer) As NawaDevDAL.goAML_Trn_Party_Entity_NotReported
        Dim objEntity As New NawaDevDAL.goAML_Trn_Party_Entity_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Trn_Party_Entity_NotReported
            Entity = objdb.goAML_Trn_Party_Entity_NotReported.Where(Function(x) x.FK_Transaction_Party_ID = id).FirstOrDefault
            If Entity IsNot Nothing Then
                objEntity = Entity
            End If
        End Using
        Return objEntity
    End Function
    Shared Function getListTransactionEntityPhoneByIdParty(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported)
            Phone = objdb.goAML_Trn_Party_Entity_Phone_NotReported.Where(Function(x) x.FK_Trn_Party_Entity_ID = Id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionEntityAddressByIdParty(Id As Integer) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported)
            Address = objdb.goAML_Trn_Party_Entity_Address_NotReported.Where(Function(x) x.FK_Trn_Party_Entity_ID = Id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
#End Region

#Region "Get Data Activity"
    Shared Function getActReportPartyTypeByFK(id As String) As List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported)
        Dim objActReportType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ActReportType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported)
            ActReportType = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_ReportPartyType_NotReported)("select * from goAML_Act_ReportPartyType_NotReported with(nolock) where FK_Report_ID = " & id).ToList
            If ActReportType IsNot Nothing Then
                objActReportType = ActReportType
            End If
        End Using
        Return objActReportType
    End Function
    'Start Activity Account -------------------------------------------------------------------------------------------------------------
    Shared Function getActAccountByFK(id As String) As NawaDevDAL.goAML_Act_Account_NotReported
        Dim objFromAccount As New NawaDevDAL.goAML_Act_Account_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Act_Account_NotReported
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Account_NotReported)("select * from goAML_Act_Account_NotReported with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    'Activity Account Entity
    Shared Function getActAccountEntityByFK(AccID As String) As NawaDevDAL.goAML_Act_Entity_Account_NotReported
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Act_Entity_Account_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Act_Entity_Account_NotReported
            AccountEntity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Account_NotReported)("select * from goAML_Act_Entity_Account_NotReported with(nolock) where FK_Act_Account_ID = " & AccID).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListActAccountEntityPhoneById(id As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported)
            AccountEntityPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone_NotReported)("select * from goAML_Act_Acc_Entity_Phone_NotReported with(nolock) where FK_Act_Acc_Entity_ID = " & id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListActAccountEntityAddressById(id As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported)
            AccountEntityAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Address_NotReported)("select * from goAML_Act_Acc_Entity_Address_NotReported with(nolock) where FK_Act_Acc_Entity_ID = " & id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    'Activity Account Entity Director
    Shared Function getListActAccountEntityDirectorById(EntityId As String) As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported)
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Ent_Director_NotReported)("select * from goAML_Act_Acc_Ent_Director_NotReported with(nolock) where FK_Act_Acc_Entity_ID = " & EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListActAccountEntityDirectorPhoneById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone_NotReported)("select * from goAML_Act_Acc_Entity_Director_Phone_NotReported with(nolock) where FK_Act_Entity_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListActAccountEntityDirectorAddressById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address_NotReported)("select * from goAML_Act_Acc_Entity_Director_address_NotReported with(nolock) where FK_Act_Entity_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    Shared Function getListActIdentificationById(Id As String, TypeID As Integer) As List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)
            Identification = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_activity_Person_Identification_NotReported)("select * from goAML_activity_Person_Identification_NotReported with(nolock) where FK_Act_Person_ID = " & Id & " and FK_Person_Type = " & TypeID).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Account Signatory
    Shared Function getListActAccountSignatoryById(TranAccID As String) As List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported)
            signatory = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_acc_Signatory_NotReported)("select * from goAML_Act_acc_Signatory_NotReported with(nolock) where FK_Activity_Account_ID = " & TranAccID).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListActAccountSignatoryPhoneById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Phone_NotReported)("select * from goAML_Act_Acc_sign_Phone_NotReported with(nolock) where FK_Act_Acc_Entity_ID = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListActAccountSignatoryAddressById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Address_NotReported)("select * from goAML_Act_Acc_sign_Address_NotReported with(nolock) where FK_Act_Acc_Entity = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'End Activity Account -------------------------------------------------------------------------------------------------------------

    'Start Activity Person
    Shared Function getActPersonByFK(id As String) As NawaDevDAL.goAML_Act_Person_NotReported
        Dim objFromPerson As New NawaDevDAL.goAML_Act_Person_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Act_Person_NotReported
            Person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_NotReported)("select * from goAML_Act_Person_NotReported with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListActPersonPhoneById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Phone_NotReported)("select * from goAML_Act_Person_Phone_NotReported with(nolock) where FK_Act_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListActPersonAddressById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Address_NotReported)("select * from goAML_Act_Person_Address_NotReported with(nolock) where FK_Act_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Activity Person ----------------------------------------------------------------------------------------------------------------

    'Start Activity Entity
    Shared Function getActEntityByFK(id As String) As NawaDevDAL.goAML_Act_Entity_NotReported
        Dim objFromEntity As New NawaDevDAL.goAML_Act_Entity_NotReported
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Act_Entity_NotReported
            Entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_NotReported)("select * from goAML_Act_Entity_NotReported with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Entity IsNot Nothing Then
                objFromEntity = Entity
            End If
        End Using
        Return objFromEntity
    End Function
    Shared Function getListActEntityPhoneById(Id As String) As List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Phone_NotReported)("select * from goAML_Act_Entity_Phone_NotReported with(nolock) where FK_Act_Entity_ID = " & Id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListActEntityAddressById(Id As String) As List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Address_NotReported)("select * from goAML_Act_Entity_Address_NotReported with(nolock) where FK_Act_Entity_ID = " & Id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'Activity Entity Director
    Shared Function getListActEntityDirectorById(EntityId As String) As List(Of NawaDevDAL.goAML_Act_Director_NotReported)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Director_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Director_NotReported)
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_NotReported)("select * from goAML_Act_Director_NotReported with(nolock) where FK_Act_Entity_ID = " & EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListActEntityDirectorPhoneById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Phone_NotReported)("select * from goAML_Act_Director_Phone_NotReported with(nolock) where FK_Act_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListActEntityDirectorAddressById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Address_NotReported)("select * from goAML_Act_Director_Address_NotReported with(nolock) where FK_Act_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    'End Activity Entity ----------------------------------------------------------------------------------------------------------------
#End Region

    Shared Function getReportIndicatorByKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim kategori As NawaDevDAL.goAML_Ref_Indikator_Laporan = objdb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function
#End Region

    Shared Function ReportToNotReported(ID As Integer)
        Try
            Dim param(0) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report"
            param(0).Value = ID
            param(0).DbType = SqlDbType.BigInt

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_NotReported", param)
            '  Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_GetListInvalidValidationInitial_Temp", objListParam)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Shared Function SaveNotReportedApproval(ObjProposalReport As ProposalReport_NotReported, objmodule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    ObjProposalReport.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    ObjProposalReport.CreatedDate = DateTime.Now
                    Dim xmldata As String = NawaBLL.Common.Serialize(ObjProposalReport)

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Approval

                    With objModuleApproval
                        .ModuleName = objmodule.ModuleName
                        .ModuleKey = ObjProposalReport.Pk_ProposalReport_NotReported
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, objmodule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, ObjProposalReport)

                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function
    Shared Function SaveTanpaApprovalNotReported(ObjProposalReport As ProposalReport_NotReported, objmodule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert

                    With ObjProposalReport
                        .Active = True
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = DateTime.Now
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = DateTime.Now
                    End With
                    objdb.Entry(ObjProposalReport).State = EntityState.Added
                    objdb.SaveChanges()

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, objmodule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, ObjProposalReport)

                    ReportToNotReported(ObjProposalReport.Report_ID)

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

    Shared Function Accept(id As String)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert

                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = id).FirstOrDefault()
                    Dim objModule As New NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objModuledata As NawaDevDAL.ProposalReport_NotReported = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(NawaDevDAL.ProposalReport_NotReported))

                            With objModuledata
                                .Active = True
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                            End With
                            objdb.Entry(objModuledata).State = EntityState.Added
                            objdb.SaveChanges()

                            Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, objModule.ModuleLabel)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objModuledata)

                            ReportToNotReported(objModuledata.Report_ID)
                    End Select

                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

    Shared Function Reject(Id As String)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = Id).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objModuledata As NawaDevDAL.ProposalReport_NotReported = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(NawaDevDAL.ProposalReport_NotReported))

                            Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, objModule.ModuleLabel)
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objModuledata)
                    End Select

                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function
End Class
