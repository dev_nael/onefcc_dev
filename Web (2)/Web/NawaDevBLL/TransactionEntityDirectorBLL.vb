﻿Imports System.Data.Entity
Imports System.Reflection
Imports NawaBLL
Imports NawaDevDAL

Public Class TransactionEntityDirectorBLL
    Public Shared Sub InsertTransactionEntityDirectorFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityFrom As DirectorClass In Transactions.listDirectorEntityFrom

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityFrom.objDirector

                        With itemObjDirector
                            .FK_Entity_ID = Transactions.objEntityFrom.PK_Entity_ID
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With

                        objdb.Entry(itemObjDirector).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjDirector.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjDirector, Nothing) Is Nothing, item.GetValue(itemObjDirector, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityFrom.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                With DirectorPhone
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhone).State = EntityState.Added

                                'objtypeReport = DirectorPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhone, Nothing) Is Nothing, item.GetValue(DirectorPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityFrom.listAddressDirector.Count > 0 Then

                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                With DirectorAddress
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddress).State = EntityState.Added

                                'objtypeReport = DirectorAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddress, Nothing) Is Nothing, item.GetValue(DirectorAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataDirectorEntityFrom.listAddressEmployerDirector.Count > 0 Then

                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                With DirectorAddressEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Added

                                'objtypeReport = DirectorAddressEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddressEmployer, Nothing) Is Nothing, item.GetValue(DirectorAddressEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataDirectorEntityFrom.listPhoneEmployerDirector.Count > 0 Then

                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                With DirectorPhoneEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Added

                                'objtypeReport = DirectorPhoneEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhoneEmployer, Nothing) Is Nothing, item.GetValue(DirectorPhoneEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataDirectorEntityFrom.listIdentification.Count > 0 Then

                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                With DirectorIdentification
                                    .FK_Person_ID = objModuledataDirectorEntityFrom.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorIdentification).State = EntityState.Added
                            Next

                        End If
                        objdb.SaveChanges()

                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub InsertTransactionEntityDirectorTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityTo As DirectorClass In Transactions.listDirectorEntityto

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityTo.objDirector

                        With itemObjDirector
                            .FK_Entity_ID = Transactions.objEntityTo.PK_Entity_ID
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With

                        objdb.Entry(itemObjDirector).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjDirector.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjDirector, Nothing) Is Nothing, item.GetValue(itemObjDirector, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityTo.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                With DirectorPhone
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhone).State = EntityState.Added

                                'objtypeReport = DirectorPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhone, Nothing) Is Nothing, item.GetValue(DirectorPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                With DirectorAddress
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddress).State = EntityState.Added

                                'objtypeReport = DirectorAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddress, Nothing) Is Nothing, item.GetValue(DirectorAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                With DirectorAddressEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Added

                                'objtypeReport = DirectorAddressEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorAddressEmployer, Nothing) Is Nothing, item.GetValue(DirectorAddressEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                With DirectorPhoneEmployer
                                    .FK_Trn_Director_ID = objModuledataDirectorEntityTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Added

                                'objtypeReport = DirectorPhoneEmployer.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorPhoneEmployer, Nothing) Is Nothing, item.GetValue(DirectorPhoneEmployer, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                With DirectorIdentification
                                    .FK_Person_ID = objModuledataDirectorEntityTo.objDirector.PK_goAML_Trn_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorIdentification).State = EntityState.Added

                                'objtypeReport = DirectorIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(DirectorIdentification, Nothing) Is Nothing, item.GetValue(DirectorIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If
                        objdb.SaveChanges()
                    Next
                    '---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionEntityDirectorFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityFrom As DirectorClass In Transactions.listDirectorEntityFrom

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityFrom.objDirector

                        objdb.Entry(itemObjDirector).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataDirectorEntityFrom.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                objdb.Entry(DirectorPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityFrom.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                objdb.Entry(DirectorAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityFrom.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityFrom.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityFrom.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                objdb.Entry(DirectorIdentification).State = EntityState.Deleted
                            Next
                        End If
                        objdb.SaveChanges()
                    Next
                    '---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionEntityDirectorTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityTo As DirectorClass In Transactions.listDirectorEntityto

                        Dim itemObjDirector As New goAML_Trn_Director
                        itemObjDirector = objModuledataDirectorEntityTo.objDirector

                        objdb.Entry(itemObjDirector).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataDirectorEntityTo.listPhoneDirector.Count > 0 Then
                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                objdb.Entry(DirectorPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listAddressDirector.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                objdb.Entry(DirectorAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listAddressEmployerDirector.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Deleted
                            Next

                        End If

                        If objModuledataDirectorEntityTo.listPhoneEmployerDirector.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityTo.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                objdb.Entry(DirectorIdentification).State = EntityState.Deleted
                            Next
                        End If
                        objdb.SaveChanges()
                    Next
                    '---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub




End Class
