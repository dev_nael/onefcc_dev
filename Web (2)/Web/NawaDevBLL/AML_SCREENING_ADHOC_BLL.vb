﻿'--------------------------------------------
'Classname      : AML_SCREENING_ADHOC
'Description    : Screening AML ADHOC
'Created By     : Felix
'Created Date   : 08-Mar-2021
'--------------------------------------------

Imports System.ComponentModel
Imports NawaDevDAL
Imports System.Data.SqlClient
Imports NawaDAL

Public Class AML_SCREENING_ADHOC_BLL

    Shared Function ScreeningAdhoc(Name As String, DOB As DateTime, Nationality As String, Indentity_Number As String) As NawaDevBLL.AML_WATCHLIST_CLASS
        Using objDb As New NawaDevDAL.NawaDatadevEntities

            Dim strsort As String = ""
            Dim intStart As Integer = 1 ''e.Start
            Dim intLimit As Integer = 16 ''e.Limit
            Dim intTotalRowCount As Long = 0

            Dim objParam(1) As SqlParameter
            objParam(0) = New SqlParameter
            objParam(0).ParameterName = "@Name"
            objParam(0).Value = Name

            objParam(1) = New SqlParameter
            objParam(1).ParameterName = "@DOB"
            objParam(1).Value = DOB

            objParam(2) = New SqlParameter
            objParam(2).ParameterName = "@Nationality"
            objParam(2).Value = Nationality

            objParam(3) = New SqlParameter
            objParam(3).ParameterName = "@IdentityNumber"
            objParam(3).Value = Indentity_Number

            objParam(4) = New SqlParameter
            objParam(4).ParameterName = "@session"
            objParam(4).Value = NawaBLL.Common.SessionCurrentUser.UserID

            Dim incrementRow As Int32 ''edited by Felix 16 Nov 2020
            incrementRow = 0
            'Dim strresult As String = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_generate_XMLNew", objParam)
            'Dim xmlDataTable As DataTable = SQLHelper.ExecuteTabelPagingNew(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_AML_Screening_Adhoc", objParam) ''edited by Felix 16 Nov 2020
            Dim DataPaging As DataTable = NawaDAL.SQLHelper.ExecuteTabelPagingNew("usp_AML_Screening_Adhoc", strsort, intStart, intLimit, intTotalRowCount)

        End Using
    End Function

    Shared Function GetWatchlistTypeByID(ID As String) As NawaDevDAL.AML_WATCHLIST_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_WATCHLIST_TYPE.Where(Function(x) x.PK_AML_WATCHLIST_TYPE_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetWatchlistCategoryByID(ID As String) As NawaDevDAL.AML_WATCHLIST_CATEGORY
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_WATCHLIST_CATEGORY.Where(Function(x) x.PK_AML_WATCHLIST_CATEGORY_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetCountryByCountryCode(Kode As String) As NawaDevDAL.AML_COUNTRY
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByCode(Kode As String) As NawaDevDAL.AML_ADDRESS_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_ADDRESS_TYPE.Where(Function(x) x.FK_AML_ADDRES_TYPE_CODE = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetIdentityTypeByCode(Kode As String) As NawaDevDAL.AML_IDENTITY_TYPE
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.AML_IDENTITY_TYPE.Where(Function(x) x.FK_AML_IDENTITY_TYPE_CODE = Kode).FirstOrDefault
        End Using
    End Function



    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function


End Class

Public Class AML_SCREENING_ADHOC_RESULT

    Public FK_AML_WATCHLIST_SCREENING_ID As String
    Public Name As String
    Public Name_Watchlist As String
    Public Watchlist_Category As String
    Public Watchlist_Type As String
    Public Dob As DateTime
    Public Nationality As String
    Public Identity_number As String
    Public SIMILARITY As Decimal

End Class


