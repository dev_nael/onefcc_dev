﻿Imports NawaDevDAL
Imports NawaDevBLL
Imports System.Data
Imports NawaBLL
Public Class RuleBasicBLL
    Shared Function GetRuleBasic(ID As String) As OneFcc_MS_Rule_Basic
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.OneFcc_MS_Rule_Basic Where x.PK_Rule_Basic_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetRuleBasicDetail(ID As String) As List(Of OneFcc_MS_Rule_Basic_Detail)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.OneFcc_MS_Rule_Basic_Detail Where x.FK_Rule_Basic_ID = ID Select x).ToList
        End Using
    End Function

    Function SubmitDocument(objData As OneFcc_MS_Rule_Basic, objDetails As List(Of OneFcc_MS_Rule_Basic_Detail), objModule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    objData.[Active] = True
                    objData.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.CreatedDate = Now.ToString("yyyy-MM-dd")
                    objData.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                    If NawaBLL.Common.SessionAlternateUser Is Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
                    Else
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    objdb.Entry(objData).State = Entity.EntityState.Added

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaDevBLL.NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, Common.AuditTrailStatusEnum.AffectedToDatabase, Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    objdb.SaveChanges()

                    SaveDetail(objDetails, objData.PK_Rule_Basic_ID)
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function


    Function EditDocument(objData As OneFcc_MS_Rule_Basic, objDetails As List(Of OneFcc_MS_Rule_Basic_Detail), objDataOld As OneFcc_MS_Rule_Basic, objModule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    objData.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.Active = True
                    If NawaBLL.Common.SessionAlternateUser Is Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
                    Else
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    objdb.Entry(objData).State = Entity.EntityState.Modified

                    Dim objaudittrailheader As AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, Common.AuditTrailStatusEnum.AffectedToDatabase, Common.ModuleActionEnum.Insert, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objData, objDataOld)

                    objdb.SaveChanges()

                    SaveDetail(objDetails, objData.PK_Rule_Basic_ID)
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Function ActivationDocument(objData As OneFcc_MS_Rule_Basic, IsProcess As Boolean, objModule As NawaDAL.Module)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    objData.[Active] = IsProcess
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                    If NawaBLL.Common.SessionAlternateUser Is Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionCurrentUser.UserID
                    Else
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    objdb.Entry(objData).State = Entity.EntityState.Modified

                    Dim objaudittrailheader As NawaDevDAL.AuditTrailHeader = NawaDevBLL.NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, Common.AuditTrailStatusEnum.AffectedToDatabase, Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function
    Protected Sub SaveDetail(objDetails As List(Of OneFcc_MS_Rule_Basic_Detail), ID As String)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    For Each itemx As OneFcc_MS_Rule_Basic_Detail In (From x In objdb.OneFcc_MS_Rule_Basic_Detail Where x.FK_Rule_Basic_ID = ID).ToList
                        Dim objcek As OneFcc_MS_Rule_Basic_Detail = objDetails.Find(Function(x) x.PK_Rule_Basic_Detail_ID = itemx.PK_Rule_Basic_Detail_ID)
                        If objcek Is Nothing Then
                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                            objdb.SaveChanges()

                        End If
                    Next
                    For Each item As OneFcc_MS_Rule_Basic_Detail In objDetails
                        Dim obcek As OneFcc_MS_Rule_Basic_Detail = (From x In objdb.OneFcc_MS_Rule_Basic_Detail Where x.PK_Rule_Basic_Detail_ID = item.PK_Rule_Basic_Detail_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            Dim oNew As New OneFcc_MS_Rule_Basic_Detail
                            oNew.FK_Rule_Basic_ID = ID
                            oNew.Variable_Name = item.Variable_Name
                            oNew.Variable_Description = item.Variable_Description
                            oNew.Value_Data = item.Value_Data
                            oNew.CreatedDate = Now.ToString("yyyy-MM-dd")
                            oNew.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            oNew.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                            oNew.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            oNew.Active = True
                            objdb.Entry(oNew).State = Entity.EntityState.Added
                            objdb.SaveChanges()
                        Else
                            obcek.Variable_Name = item.Variable_Name
                            obcek.Variable_Description = item.Variable_Description
                            obcek.Value_Data = item.Value_Data

                            obcek.LastUpdateDate = Now.ToString("yyyy-MM-dd")
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objdb.Entry(obcek).State = Entity.EntityState.Modified
                            objdb.SaveChanges()

                        End If

                    Next
                    objtrans.Commit()
                Catch
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
End Class
