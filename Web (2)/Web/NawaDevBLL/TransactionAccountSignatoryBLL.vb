﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection
Imports NawaDevBLL

Public Class TransactionAccountSignatoryBLL
    Public Shared Sub InsertTransactionAccountSignatoryFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountFrom As SignatoryClass In Transactions.LisAccountSignatoryFrom

                        Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                        itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory

                        With itemObjSignatory
                            .FK_Transaction_Account_ID = Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With
                        objdb.Entry(itemObjSignatory).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjSignatory.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjSignatory, Nothing) Is Nothing, item.GetValue(itemObjSignatory, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountFrom.listPhoneSignatory.Count > 0 Then

                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                With SignatoryAccountPhone
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountFrom.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Added

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataSignatoryAccountFrom.listAddressSignatory.Count > 0 Then

                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                With SignatoryAccountAddress
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountFrom.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Added

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataSignatoryAccountFrom.listAddressEmployerSignatory.Count > 0 Then

                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                With SignatoryAccountAddress
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountFrom.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Added

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory.Count > 0 Then

                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                With SignatoryAccountPhone
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountFrom.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Added

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataSignatoryAccountFrom.listIdentification.Count > 0 Then

                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                With SignatoryAccountIdentification
                                    .FK_Person_ID = objModuledataSignatoryAccountFrom.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountIdentification).State = EntityState.Added

                                'objtypeReport = SignatoryAccountIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountIdentification, Nothing) Is Nothing, item.GetValue(SignatoryAccountIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If
                        objdb.SaveChanges()

                    Next
                    '----------------------------End Signatory Account Transaction From--------------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using


    End Sub

    Public Shared Sub InsertTransactionAccountSignatoryTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountTo As SignatoryClass In Transactions.LisAccountSignatoryTo

                        Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                        itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory

                        With itemObjSignatory
                            .FK_Transaction_Account_ID = Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With
                        objdb.Entry(itemObjSignatory).State = EntityState.Added

                        'Dim objtypeReport As Type = itemObjSignatory.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjSignatory, Nothing) Is Nothing, item.GetValue(itemObjSignatory, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountTo.listPhoneSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory
                                With SignatoryAccountPhone
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountTo.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Added

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listAddressSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                With SignatoryAccountAddress
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountTo.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Added

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listAddressEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                With SignatoryAccountAddress
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountTo.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Added

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listPhoneEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                With SignatoryAccountPhone
                                    .FK_Trn_Acc_Entity = objModuledataSignatoryAccountTo.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Added

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listIdentification.Count > 0 Then
                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                With SignatoryAccountIdentification
                                    .FK_Person_ID = objModuledataSignatoryAccountTo.objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAccountIdentification).State = EntityState.Added

                                'objtypeReport = SignatoryAccountIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountIdentification, Nothing) Is Nothing, item.GetValue(SignatoryAccountIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If
                        objdb.SaveChanges()
                    Next
                    '-----------------------------------End Signatory Account Transaction------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountSignatoryFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountFrom As SignatoryClass In Transactions.LisAccountSignatoryFrom

                        Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                        itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory

                        objdb.Entry(itemObjSignatory).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountFrom.listPhoneSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountFrom.listAddressSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountFrom.listAddressEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Deleted
                            Next

                        End If

                        If objModuledataSignatoryAccountFrom.listIdentification.Count > 0 Then
                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                objdb.Entry(SignatoryAccountIdentification).State = EntityState.Deleted
                            Next
                        End If
                        objdb.SaveChanges()

                    Next
                    '----------------------------End Signatory Account Transaction From--------------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountSignatoryTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountTo As SignatoryClass In Transactions.LisAccountSignatoryTo

                        Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                        itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory

                        objdb.Entry(itemObjSignatory).State = EntityState.Deleted

                        'Dim objtypeReport As Type = itemObjSignatory.GetType
                        'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        'For Each item As PropertyInfo In propertiesReport
                        '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        '    {
                        '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                        '        .FieldName = item.Name,
                        '        .OldValue = "",
                        '        .NewValue = If(Not item.GetValue(itemObjSignatory, Nothing) Is Nothing, item.GetValue(itemObjSignatory, Nothing), "")
                        '    }
                        '    objdb.Entry(objATDetail).State = EntityState.Added
                        'Next
                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountTo.listPhoneSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory

                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Deleted

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listAddressSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Deleted

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listAddressEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                objdb.Entry(SignatoryAccountAddress).State = EntityState.Deleted

                                'objtypeReport = SignatoryAccountAddress.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountAddress, Nothing) Is Nothing, item.GetValue(SignatoryAccountAddress, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If

                        If objModuledataSignatoryAccountTo.listPhoneEmployerSignatory.Count > 0 Then
                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                objdb.Entry(SignatoryAccountPhone).State = EntityState.Deleted

                                'objtypeReport = SignatoryAccountPhone.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountPhone, Nothing) Is Nothing, item.GetValue(SignatoryAccountPhone, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next

                        End If

                        If objModuledataSignatoryAccountTo.listIdentification.Count > 0 Then

                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                objdb.Entry(SignatoryAccountIdentification).State = EntityState.Deleted

                                'objtypeReport = SignatoryAccountIdentification.GetType
                                'propertiesReport = objtypeReport.GetProperties
                                'For Each item As PropertyInfo In propertiesReport
                                '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                '    {
                                '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                '        .FieldName = item.Name,
                                '        .OldValue = "",
                                '        .NewValue = If(Not item.GetValue(SignatoryAccountIdentification, Nothing) Is Nothing, item.GetValue(SignatoryAccountIdentification, Nothing), "")
                                '    }
                                '    objdb.Entry(objATDetail).State = EntityState.Added
                                'Next
                            Next
                        End If
                    Next
                    '-----------------------------------End Signatory Account Transaction------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

End Class
