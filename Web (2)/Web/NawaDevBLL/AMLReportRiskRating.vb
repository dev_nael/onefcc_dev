﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports NawaDevDAL
Public Class AMLReportRiskRating
    Public Shared Function GetCustomerBYCIFNO(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_CUSTOMER.Where(Function(x) x.CIFNo = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetBranchBYFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_BRANCH.Where(Function(x) x.FK_AML_BRANCH_CODE = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetAdressBYCIFNO(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_CUSTOMER_ADDRESS.Where(Function(x) x.CIFNO = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetBidangUsahaByFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_BIDANG_USAHA.Where(Function(x) x.FK_AML_BIDANG_USAHA_Code = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetCustomerTypeFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_CUSTOMER_TYPE.Where(Function(x) x.FK_AML_Customer_Type_Code = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetPekerjaanFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_PEKERJAAN.Where(Function(x) x.FK_AML_PEKERJAAN_Code = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function

    Public Shared Function GetIdentityNumberByCIFNo(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_CUSTOMER_IDENTITY.Where(Function(x) x.CIFNO = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function

    Public Shared Function GetCountryByFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_COUNTRY.Where(Function(x) x.FK_AML_COUNTRY_Code = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetRiskRatingByFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_RISK_RATING.Where(Function(x) x.RISK_RATING_CODE = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetScreeningResultByFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_SCREENING_RESULT.Where(Function(x) x.CIF_NO = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function

    Public Shared Function GetProductByCIF(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_PRODUCT.Where(Function(x) x.FK_AML_PRODUCT_CODE = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function

    Public Shared Function GetScreeningRequestByFK(idParam As String)
        Using db As New NawaDatadevEntities()
            Dim result1 = db.AML_SCREENING_REQUEST.Where(Function(x) x.CIF_NO = idParam).FirstOrDefault

            If Not IsNothing(result1) Then
                Return result1
            End If

            Return Nothing
        End Using
    End Function
    Public Shared Function GetAMLScreeningList(ByVal id As String) As List(Of AML_WATCHLIST)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Dim result1 = NawaDataEntitiy.AML_SCREENING_CUSTOMER_MATCH.Where(Function(x) x.CIF_NO = id).FirstOrDefault

            If Not IsNothing(result1) Then
                Return (From X In NawaDataEntitiy.AML_WATCHLIST Where X.PK_AML_WATCHLIST_ID = result1.FK_AML_WATCHLIST_ID Select X).ToList
            End If

        End Using
    End Function
    Public Shared Function GetAMLDeliveryList(ByVal id As String) As List(Of AML_DELIVERY_CHANNELS)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_DELIVERY_CHANNELS Where X.FK_AML_AML_DELIVERY_CHANNELS_CODE = id Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLProductList(ByVal id As String) As List(Of AML_PRODUCT)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_PRODUCT Where X.FK_AML_PRODUCT_CODE = id Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLAccountList(ByVal id As String) As List(Of AML_ACCOUNT)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_ACCOUNT Where X.CIFNO = id Select X).ToList
        End Using
    End Function
End Class
