﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Ext.Net
Imports NawaDAL
Imports NawaDevDAL

Public Class goAML_CustomerBLL
    Implements IDisposable

    Shared Function GetVWCustomerPhones(ID As String) As List(Of NawaDevDAL.Vw_Customer_Phones)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function
    '' Edited on 04 Dec 2020
    Shared Function getDataApproval(id As String, moduleName As String) As NawaDevDAL.ModuleApproval
        Dim objModuleApproval As New NawaDevDAL.ModuleApproval
        Using objdb As New NawaDatadevEntities
            Dim Approval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function

    Shared Function getPKCustomer(id As String) As String
        Dim PKCustomer = ""
        Using objdb As New NawaDatadevEntities
            Dim PK As String = objdb.goAML_Ref_Customer.Where(Function(x) x.CIF = id).FirstOrDefault.PK_Customer_ID
            If PK IsNot Nothing Then
                PKCustomer = PK
            End If
        End Using
        Return PKCustomer
    End Function
    '' End of edit on 04 Dec 2020

    Shared Function GetVWCustomerAddresses(ID As String) As List(Of NawaDevDAL.Vw_Customer_Addresses)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerAddressesbyID(ID As String) As NawaDevDAL.Vw_Customer_Addresses
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWCustomerAddressesbyPK(ID As String) As NawaDevDAL.Vw_Customer_Addresses
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.PK_Customer_Address_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWDirectorPhones(ID As String) As List(Of NawaDevDAL.Vw_Director_Phones)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Director_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorAddresses(ID As String) As List(Of NawaDevDAL.Vw_Director_Addresses)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Director_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorAddresses(ID As String) As List(Of NawaDevDAL.goAML_Ref_Address)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 6 Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorPhone(ID As String) As List(Of NawaDevDAL.goAML_Ref_Phone)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 6 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerAddresses(ID As String) As List(Of NawaDevDAL.goAML_Ref_Address)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerDirector(ID As String) As List(Of NawaDevDAL.Vw_Customer_Director)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Customer_Director Where x.FK_Entity_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerDirector(ID As String) As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerDirectorByPK(ID As String) As NawaDevDAL.goAML_Ref_Customer_Entity_Director
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerDirectorByPKDirector(ID As String) As NawaDevDAL.goAML_Ref_Customer_Entity_Director
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = ID Select x).FirstOrDefault
        End Using
    End Function

    Shared Function GetVWEmployerAddresses(ID As String) As List(Of NawaDevDAL.Vw_Employer_Addresses)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetEmployerAddresses(ID As String) As List(Of NawaDevDAL.goAML_Ref_Address)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 5 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWEmployerPhones(ID As String) As List(Of NawaDevDAL.Vw_Employer_Phones)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetEmployerPhones(ID As String) As List(Of NawaDevDAL.goAML_Ref_Phone)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 5 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorEmployerAddresses(ID As String) As List(Of NawaDevDAL.Vw_Director_Employer_Addresses)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Director_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorEmployerAddresses(ID As String) As List(Of NawaDevDAL.goAML_Ref_Address)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Address Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 7 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorEmployerPhones(ID As String) As List(Of NawaDevDAL.Vw_Director_Employer_Phones)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Director_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorEmployerPhones(ID As String) As List(Of NawaDevDAL.goAML_Ref_Phone)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 7 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerIdentifications(ID As String) As List(Of NawaDevDAL.Vw_Person_Identification)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorIdentifications(ID As String) As List(Of NawaDevDAL.Vw_Person_Identification)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWDirectorIdentificationsbypk(ID As String) As List(Of NawaDevDAL.Vw_Person_Identification)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Person_Identification Where x.PK_Person_Identification_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetVWCustomerIdentificationsbyPK(ID As String) As NawaDevDAL.Vw_Person_Identification
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetVWCustomerIdentificationsbyPKPersonIdentification(ID As String) As NawaDevDAL.Vw_Person_Identification
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.Vw_Person_Identification Where x.PK_Person_Identification_ID = ID And x.FK_Person_Type = 1 Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetCustomerIdentifications(ID As String) As List(Of NawaDevDAL.goAML_Person_Identification)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetDirectorIdentifications(ID As String) As List(Of NawaDevDAL.goAML_Person_Identification)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Person_Identification Where x.FK_Person_ID = ID And x.FK_Person_Type = 4 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomerPhones(ID As String) As List(Of NawaDevDAL.goAML_Ref_Phone)
        Using objDb As New NawaDevDAL.NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Phone Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomer(ID As Integer) As NawaDevDAL.goAML_Ref_Customer

        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerbyCIF(CIF As String) As NawaDevDAL.goAML_Ref_Customer

        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
        End Using
    End Function

    Shared Function GetListgoAML_Ref_PhoneByPKID(ID As Integer) As List(Of NawaDevDAL.goAML_Ref_Phone)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function

    Shared Function GetListgoAML_Ref_AddressByPKID(ID As Integer) As List(Of NawaDevDAL.goAML_Ref_Address)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function
    Shared Function GetTabelTypeForByID(id As Integer) As NawaDevDAL.goAML_For_Table
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_For_Table.Where(Function(x) x.PK_For_Table_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByID(id As String) As NawaDevDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByID(id As String) As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByID(id As String) As NawaDevDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressByID(id As String) As NawaDevDAL.goAML_Ref_Address
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetPhonesByID(id As String) As NawaDevDAL.goAML_Ref_Phone
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByID(id As String) As NawaDevDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByKeterangan(id As String) As NawaDevDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisDokumenByID(id As String) As NawaDevDAL.goAML_Ref_Jenis_Dokumen_Identitas
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNameDirectorByID(id As String) As NawaDevDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
        End Using
    End Function

    Shared Function GetRoleDirectorByID(id As String) As NawaDevDAL.goAML_Ref_Peran_orang_dalam_Korporasi
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_Korporasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetBentukBadanUsahaByID(id As String) As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha
        'done:code  GetListEmailTableTypes
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Bentuk_Badan_Usaha.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListKategoriKontak() As List(Of NawaDevDAL.goAML_Ref_Kategori_Kontak)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Kategori_Kontak.ToList
        End Using
    End Function
    'Shared Function GetListNamaNegara() As List(Of NawaDevDAL.goAML_Ref_Nama_Negara)
    '    Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    '        Return objDb.goAML_Ref_Nama_Negara.ToList
    '    End Using
    'End Function


    Shared Function GetListTypeKomunikasi() As List(Of NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.ToList
        End Using
    End Function

    Shared Function GetListCustomerType() As List(Of NawaDevDAL.goAML_Ref_Customer_Type)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Customer_Type.ToList
        End Using
    End Function

    Shared Function GetListStatusRekening() As List(Of NawaDevDAL.goAML_Ref_Status_Rekening)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Status_Rekening.ToList
        End Using
    End Function

    Shared Function GetStatusRekeningbyID(ID As String) As NawaDevDAL.goAML_Ref_Status_Rekening
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetPartyRolebyID(ID As String) As NawaDevDAL.goAML_Ref_Party_Role
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Party_Role.Where(Function(x) x.Kode = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetListJenisKelamin() As List(Of NawaDevDAL.goAML_Ref_Jenis_Kelamin)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Kelamin.ToList
        End Using
    End Function

    Shared Function GetJenisKelamin(Kode As String) As NawaDevDAL.goAML_Ref_Jenis_Kelamin
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = Kode).FirstOrDefault
        End Using
    End Function


    Shared Function GetNamaNegara(Kode As String) As NawaDevDAL.goAML_Ref_Nama_Negara
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = Kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetListNamaNegara() As List(Of NawaDevDAL.goAML_Ref_Nama_Negara)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Nama_Negara.ToList
        End Using
    End Function
    'Update: Zikri_14092020 Add Validation GCN
    Shared Function GetCheckGCN(id As String) As NawaDevDAL.goAML_Ref_Customer
        Using ObjDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim result As NawaDevDAL.goAML_Ref_Customer = ObjDb.goAML_Ref_Customer.Where(Function(x) x.GCN = id And x.isGCNPrimary = True).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update
    Shared Function GetListBentukBadanUsaha() As List(Of NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Bentuk_Badan_Usaha.ToList
        End Using
    End Function

    Shared Function GetListPartyRole() As List(Of NawaDevDAL.goAML_Ref_Party_Role)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Party_Role.ToList
        End Using
    End Function

    Shared Function getlistjenisdokumenidentitas() As List(Of NawaDevDAL.goAML_Ref_Jenis_Dokumen_Identitas)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.ToList
        End Using
    End Function

    Shared Function GetListPeranDalamKorporasi() As List(Of NawaDevDAL.goAML_Ref_Peran_orang_dalam_Korporasi)
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_Korporasi.ToList
        End Using
    End Function

    'Shared Function ifticopy(pkiftiid As Integer, userid As String)

    '    Dim objListParam(1) As SqlParameter
    '    objListParam(0) = New SqlParameter
    '    objListParam(0).ParameterName = "@Pkiftiid"
    '    objListParam(0).Value = pkiftiid

    '    objListParam(1) = New SqlParameter
    '    objListParam(1).ParameterName = "@userid"
    '    objListParam(1).Value = userid


    '    Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_IFTI_Copy", objListParam)
    'End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    Function SaveEditApproval(objData As NawaDevDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of NawaDevDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of NawaDevDAL.goAML_Person_Identification)) As Boolean
        'done:code  SaveAddApproval
        Using objdb As New NawaDAL.NawaDataEntities
            Using objdbDev As New NawaDevDAL.NawaDatadevEntities
                Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                    Try
                        Dim objxData As New NawaDevBLL.goAML_CustomerDataBLL
                        objxData.objgoAML_Ref_Customer = objData
                        objxData.objListgoAML_Ref_Director = objListgoAML_Ref_Director
                        objxData.objListgoAML_Ref_Phone = objListgoAML_Ref_Phone
                        objxData.objlistgoaml_Ref_Address = objListgoAML_Ref_Address
                        objxData.objListgoAML_Ref_Identification = objListgoAML_Ref_Identification
                        objxData.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_Phone
                        objxData.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_Address
                        objxData.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_Address
                        objxData.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_Phone
                        objxData.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_Phone
                        objxData.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_Address
                        objxData.objListgoAML_Ref_Identification_Director = objListgoAML_Ref_Identification_Director
                        Dim xmldata As String = NawaBLL.Common.Serialize(objxData)

                        Dim objgoAML_CustomerBefore As NawaDevDAL.goAML_Ref_Customer = objdbDev.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID).FirstOrDefault
                        Dim objListgoAML_Ref_DirectorBefore As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director) = objdbDev.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objxData.objgoAML_Ref_Customer.CIF).ToList
                        Dim objListgoAML_Ref_IdentificationBefore As List(Of NawaDevDAL.goAML_Person_Identification) = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Person_Type = 1).ToList
                        Dim objListgoAML_Ref_PhoneBefore As List(Of NawaDevDAL.goAML_Ref_Phone) = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "1").ToList
                        Dim objListgoAML_Ref_AddressBefore As List(Of NawaDevDAL.goAML_Ref_Address) = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "1").ToList
                        Dim objListgoAML_Ref_Employer_PhoneBefore As List(Of NawaDevDAL.goAML_Ref_Phone) = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "5").ToList
                        Dim objListgoAML_Ref_Employer_AddressBefore As List(Of NawaDevDAL.goAML_Ref_Address) = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = objxData.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = "5").ToList

                        Dim objListgoAML_Ref_Director_PhoneBefore As New List(Of NawaDevDAL.goAML_Ref_Phone)
                        Dim objListgoAML_Ref_Director_AddressBefore As New List(Of NawaDevDAL.goAML_Ref_Address)
                        Dim objListgoAML_Ref_Director_Employer_PhoneBefore As New List(Of NawaDevDAL.goAML_Ref_Phone)
                        Dim objListgoAML_Ref_Director_Employer_AddressBefore As New List(Of NawaDevDAL.goAML_Ref_Address)
                        Dim objListgoAML_Ref_IdentificationDirectorBefore As New List(Of NawaDevDAL.goAML_Person_Identification)

                        If objData.FK_Customer_Type_ID = 2 Then
                            'For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_DirectorBefore
                            '    objListgoAML_Ref_Director_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList
                            '    objListgoAML_Ref_Director_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList
                            '    objListgoAML_Ref_Director_Employer_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList
                            '    objListgoAML_Ref_Director_Employer_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList
                            '    objListgoAML_Ref_IdentificationDirectorBefore = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = "4").ToList
                            'Next
                            'Update: Try_28092020
                            For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_DirectorBefore

                                Dim objphonenew As New List(Of goAML_Ref_Phone)

                                Dim objaddressnew As New List(Of goAML_Ref_Address)

                                Dim objempaddressnew As New List(Of goAML_Ref_Address)

                                Dim objempphonenew As New List(Of goAML_Ref_Phone)

                                Dim objidentificationew As New List(Of goAML_Person_Identification)

                                objphonenew = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList

                                objaddressnew = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "6").ToList

                                objempaddressnew = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList

                                objempphonenew = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = "7").ToList

                                objidentificationew = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = "4").ToList

                                For Each item1 In objphonenew

                                    objListgoAML_Ref_Director_PhoneBefore.Add(item1)

                                Next

                                For Each item2 In objaddressnew

                                    objListgoAML_Ref_Director_AddressBefore.Add(item2)

                                Next

                                For Each item3 In objempaddressnew

                                    objListgoAML_Ref_Director_Employer_AddressBefore.Add(item3)

                                Next

                                For Each item4 In objempphonenew

                                    objListgoAML_Ref_Director_Employer_PhoneBefore.Add(item4)

                                Next

                                For Each item5 In objidentificationew

                                    objListgoAML_Ref_IdentificationDirectorBefore.Add(item5)

                                Next

                            Next
                            'End Update
                        Else
                            objListgoAML_Ref_Director_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_Employer_PhoneBefore = objdbDev.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = 0).ToList
                            objListgoAML_Ref_Director_Employer_AddressBefore = objdbDev.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = 0).ToList
                            objListgoAML_Ref_IdentificationDirectorBefore = objdbDev.goAML_Person_Identification.Where(Function(x) x.FK_Person_ID = 0).ToList
                        End If


                        Dim objxDatabefore As New NawaDevBLL.goAML_CustomerDataBLL
                        objxDatabefore.objgoAML_Ref_Customer = objgoAML_CustomerBefore
                        objxDatabefore.objListgoAML_Ref_Director = objListgoAML_Ref_DirectorBefore
                        objxDatabefore.objListgoAML_Ref_Identification = objListgoAML_Ref_IdentificationBefore
                        objxDatabefore.objListgoAML_Ref_Phone = objListgoAML_Ref_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Address = objListgoAML_Ref_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_PhoneBefore
                        objxDatabefore.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_PhoneBefore
                        objxDatabefore.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_AddressBefore
                        objxDatabefore.objListgoAML_Ref_Identification_Director = objListgoAML_Ref_IdentificationDirectorBefore

                        Dim xmlbefore As String = NawaBLL.Common.Serialize(objxDatabefore)

                        Dim objModuleApproval As New NawaDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = objxData.objgoAML_Ref_Customer.PK_Customer_ID
                            .ModuleField = xmldata
                            .ModuleFieldBefore = xmlbefore
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                        Using objDba As New NawaDatadevEntities
                            Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                            'AuditTrailDetail
                            If objListgoAML_Ref_Phone.Count > 0 Then
                                For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                                Next
                            End If

                            If objListgoAML_Ref_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objData.FK_Customer_Type_ID = 1 Then

                                If objListgoAML_Ref_Identification.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If
                            End If

                            If objData.FK_Customer_Type_ID = 2 Then
                                If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Address.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                            End If



                        End Using
                        'AuditTrail


                        objdb.SaveChanges()
                        objtrans.Commit()
                        NawaBLL.Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)

                    Catch ex As Exception
                        objtrans.Rollback()
                        Throw
                    End Try
                End Using
            End Using
        End Using
    End Function

    Sub SaveAddApproval(objData As NawaDevDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of NawaDevDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), Objlistgoaml_Ref_Identification_Director As List(Of NawaDevDAL.goAML_Person_Identification))
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objdatabll As New NawaDevBLL.goAML_CustomerDataBLL
                    objdatabll.objgoAML_Ref_Customer = objData
                    objdatabll.objListgoAML_Ref_Director = objListgoAML_Ref_Director
                    objdatabll.objlistgoaml_Ref_Address = objListgoAML_Ref_Address
                    objdatabll.objListgoAML_Ref_Phone = objListgoAML_Ref_Phone
                    objdatabll.objListgoAML_Ref_Employer_Phone = objListgoAML_Ref_Employer_Phone
                    objdatabll.objlistgoaml_Ref_Employer_Address = objListgoAML_Ref_Employer_Address
                    objdatabll.objListgoAML_Ref_Identification = objListgoAML_Ref_Identification
                    objdatabll.objListgoAML_Ref_Director_Address = objListgoAML_Ref_Director_Address
                    objdatabll.objListgoAML_Ref_Director_Phone = objListgoAML_Ref_Director_Phone
                    objdatabll.objListgoAML_Ref_Director_Employer_Phone = objListgoAML_Ref_Director_Employer_Phone
                    objdatabll.objlistgoaml_Ref_Director_Employer_Address = objListgoAML_Ref_Director_Employer_Address
                    objdatabll.objListgoAML_Ref_Identification_Director = Objlistgoaml_Ref_Identification_Director

                    Dim dataXML As String = NawaBLL.Common.Serialize(objdatabll)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0
                        .ModuleField = dataXML
                        .ModuleFieldBefore = ""
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Using objDba As New NawaDatadevEntities
                        Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                        'AuditTrailDetail
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                            Next
                        End If

                        If objListgoAML_Ref_Address.Count > 0 Then
                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then

                            If objListgoAML_Ref_Identification.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If Objlistgoaml_Ref_Identification_Director.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In Objlistgoaml_Ref_Identification_Director
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                        End If



                    End Using
                    'AuditTrail

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Sub SaveEditTanpaApproval(objData As NawaDevDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of NawaDevDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of NawaDevDAL.goAML_Person_Identification))
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Update: Zikri_14092020 Add Validation GCN
                    If objData.GCN IsNot Nothing Then
                        If objData.isGCNPrimary = True Then
                            Dim checkGCN As goAML_Ref_Customer = NawaDevBLL.goAML_CustomerBLL.GetCheckGCN(objData.GCN)
                            If checkGCN IsNot Nothing Then
                                If checkGCN.CIF <> objData.CIF Then
                                    checkGCN.isGCNPrimary = 0
                                    checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    checkGCN.ApprovedDate = Now
                                    checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    checkGCN.LastUpdateDate = Now
                                    objDB.Entry(checkGCN).State = Entity.EntityState.Modified
                                    objDB.SaveChanges()
                                End If
                            End If
                        End If
                    End If
                    'End Update
                    objData.Active = 1
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    If objData.FK_Customer_Type_ID = 2 Then
                        For Each itemx As NawaDevDAL.goAML_Ref_Customer_Entity_Director In (From x In objDB.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = objData.CIF Select x).ToList
                            Dim objcek As NawaDevDAL.goAML_Ref_Customer_Entity_Director = objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = itemx.PK_goAML_Ref_Customer_Entity_Director_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        objDB.SaveChanges()

                        For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                            Dim obcek As NawaDevDAL.goAML_Ref_Customer_Entity_Director = (From x In objDB.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                item.FK_Entity_ID = objData.CIF
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                            objDB.SaveChanges()

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4 Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Person_Identification = objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.LastUpdateDate = Now
                                    objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                Dim objcek As NawaDevDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_Person_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                Dim objcek As NawaDevDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                Dim objcek As NawaDevDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                Dim objcek As NawaDevDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.PK_Customer_Address_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                Dim objcek As NawaDevDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                If objcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        itemx.Alternateby = ""
                                    End If
                                    itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.ApprovedDate = Now
                                    itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    itemx.CreatedDate = Now
                                    itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID '' Edited on 04 Dec 2020
                                    objDB.Entry(itemx).State = Entity.EntityState.Added
                                Else
                                    Dim objcekdirector = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_To_Table_ID).ToList()
                                    If objcekdirector Is Nothing Then
                                        objDB.Entry(objcek).State = Entity.EntityState.Deleted
                                    Else
                                        objDB.Entry(objcek).CurrentValues.SetValues(itemx)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            objcek.Alternateby = ""
                                        End If
                                        objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.ApprovedDate = Now
                                        objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        objcek.LastUpdateDate = Now
                                        objDB.Entry(objcek).State = Entity.EntityState.Modified
                                    End If
                                End If
                            Next
                        Next
                    End If

                    If objData.FK_Customer_Type_ID = 1 Then
                        For Each itemx As NawaDevDAL.goAML_Person_Identification In (From x In objDB.goAML_Person_Identification Where x.FK_Person_ID = objData.PK_Customer_ID And x.FK_Person_Type = 1 Select x).ToList
                            Dim objcek As NawaDevDAL.goAML_Person_Identification = objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                            Dim obcek As NawaDevDAL.goAML_Person_Identification = (From x In objDB.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else

                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next

                        For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                            Dim objcek As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                            If objcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                            Dim obcek As NawaDevDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next

                        For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                            Dim objcek As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                            If objcek Is Nothing Then
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            End If
                        Next
                        For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                            Dim obcek As NawaDevDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                            If obcek Is Nothing Then
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                objDB.Entry(item).State = Entity.EntityState.Added
                            Else

                                objDB.Entry(obcek).CurrentValues.SetValues(item)
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    obcek.Alternateby = ""
                                End If
                                obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.ApprovedDate = Now
                                obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                obcek.LastUpdateDate = Now
                                objDB.Entry(obcek).State = Entity.EntityState.Modified
                            End If
                        Next
                    End If

                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objDB.goAML_Ref_Phone Where x.FK_for_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 1 Select x).ToList
                        Dim objcek As NawaDevDAL.goAML_Ref_Phone = objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                        Dim obcek As NawaDevDAL.goAML_Ref_Phone = (From x In objDB.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objDB.goAML_Ref_Address Where x.FK_To_Table_ID = objData.PK_Customer_ID And x.FK_Ref_Detail_Of = 1 Select x).ToList
                        Dim objcek As NawaDevDAL.goAML_Ref_Address = objListgoAML_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next

                    For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                        Dim obcek As NawaDevDAL.goAML_Ref_Address = (From x In objDB.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else

                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.LastUpdateDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    Using objDba As New NawaDatadevEntities
                        Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                        'AuditTrailDetail
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                            Next
                        End If

                        If objListgoAML_Ref_Address.Count > 0 Then
                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then

                            If objListgoAML_Ref_Identification.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                        End If

                    End Using
                    'AuditTrail



                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Sub SaveAddTanpaApproval(objData As NawaDevDAL.goAML_Ref_Customer, objListgoAML_Ref_Director As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director), objModule As NawaDAL.Module, objListgoAML_Ref_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification As List(Of NawaDevDAL.goAML_Person_Identification), objListgoAML_Ref_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Employer_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Employer_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Director_Address As List(Of NawaDevDAL.goAML_Ref_Address), objListgoAML_Ref_Director_Phone As List(Of NawaDevDAL.goAML_Ref_Phone), objListgoAML_Ref_Identification_Director As List(Of NawaDevDAL.goAML_Person_Identification))
        Using objDB As New NawaDevDAL.NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Update: Zikri_14092020 Add Validation GCN
                    If objData.GCN IsNot Nothing Then
                        Dim checkGCN As goAML_Ref_Customer = NawaDevBLL.goAML_CustomerBLL.GetCheckGCN(objData.GCN)
                        If checkGCN IsNot Nothing Then
                            If objData.isGCNPrimary = True Then
                                checkGCN.isGCNPrimary = 0
                                checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                checkGCN.ApprovedDate = Now
                                checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                checkGCN.LastUpdateDate = Now
                                objDB.Entry(checkGCN).State = Entity.EntityState.Modified
                                objDB.SaveChanges()
                            End If
                        End If
                    End If
                    'End Update
                    objData.Active = 1
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.CreatedDate = Now
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    If objData.FK_Customer_Type_ID = 2 Then
                        For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objListgoAML_Ref_Director
                            item.FK_Entity_ID = objData.CIF
                            Dim addressdirector = objListgoAML_Ref_Director_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim phonedirector = objListgoAML_Ref_Director_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim addressempdirector = objListgoAML_Ref_Director_Employer_Address.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim phoneempdirector = objListgoAML_Ref_Director_Employer_Phone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            Dim identificationdirector = objListgoAML_Ref_Identification_Director.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In addressdirector
                                itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In phonedirector

                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In addressempdirector
                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In phoneempdirector
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                            For Each itemx As NawaDevDAL.goAML_Person_Identification In identificationdirector
                                itemx.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    itemx.Alternateby = ""
                                End If
                                itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.CreatedDate = Now
                                itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                itemx.ApprovedDate = Now
                                objDB.Entry(itemx).State = Entity.EntityState.Added
                            Next

                        Next
                    End If

                    If objData.FK_Customer_Type_ID = 1 Then
                        For Each item As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                            item.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_Person_ID = objData.PK_Customer_ID
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                        For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                            item.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_To_Table_ID = objData.PK_Customer_ID
                            item.FK_Ref_Detail_Of = 5
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                        For Each item As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.FK_for_Table_ID = objData.PK_Customer_ID
                            item.FK_Ref_Detail_Of = 5
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Next

                    End If

                    For Each item As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                        item.Active = 1
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        Else
                            item.Alternateby = ""
                        End If
                        item.FK_To_Table_ID = objData.PK_Customer_ID
                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.CreatedDate = Now
                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        item.ApprovedDate = Now
                        objDB.Entry(item).State = Entity.EntityState.Added
                    Next

                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                        Else
                            itemx.Alternateby = ""
                        End If
                        itemx.FK_for_Table_ID = objData.PK_Customer_ID
                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        itemx.CreatedDate = Now
                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        itemx.ApprovedDate = Now
                        objDB.Entry(itemx).State = Entity.EntityState.Added
                    Next

                    Using objDba As New NawaDatadevEntities
                        Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                        'AuditTrailDetail
                        If objListgoAML_Ref_Phone.Count > 0 Then
                            For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Phone
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                            Next
                        End If

                        If objListgoAML_Ref_Address.Count > 0 Then
                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Address
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                            Next
                        End If

                        If objData.FK_Customer_Type_ID = 1 Then

                            If objListgoAML_Ref_Identification.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If
                        End If

                        If objData.FK_Customer_Type_ID = 2 Then
                            If objListgoAML_Ref_Director_Employer_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Employer_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Employer_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Address.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Address In objListgoAML_Ref_Director_Address
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Director_Phone.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objListgoAML_Ref_Director_Phone
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                            If objListgoAML_Ref_Identification_Director.Count > 0 Then
                                For Each itemheader As NawaDevDAL.goAML_Person_Identification In objListgoAML_Ref_Identification_Director
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                Next
                            End If

                        End If



                    End Using
                    'AuditTrail

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub


    Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)

        'done: loadPanel

        Dim objgoAML_CustomerDataBLL As NawaDevBLL.goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(NawaDevBLL.goAML_CustomerDataBLL))
        Dim objCustomer As NawaDevDAL.goAML_Ref_Customer = objgoAML_CustomerDataBLL.objgoAML_Ref_Customer

        'Dim objListEmailTemplatedetail As List(Of NawaDAL.EmailTemplateDetail) = GetListEmailTemplateDetailByPKID(unikkey)
        'Dim objListEmailTemplateAction As List(Of NawaDAL.EmailTemplateAction) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateActionByPKID(unikkey)
        'Dim objListEmailTemplateAttachment As List(Of NawaDAL.EmailTemplateAttachment) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateAttachmentByPKID(unikkey)

        Dim INDV_gender As NawaDevDAL.goAML_Ref_Jenis_Kelamin = Nothing
        Dim INDV_statusCode As NawaDevDAL.goAML_Ref_Status_Rekening = Nothing
        Dim INDV_nationality1 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality2 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality3 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_residence As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Incorporation_legal_form As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
        Dim CORP_incorporation_country_code As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Role As NawaDevDAL.goAML_Ref_Party_Role = Nothing

        Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If Not objCustomer Is Nothing Then
                Dim strunik As String = unikkey
                If objCustomer.FK_Customer_Type_ID = 1 Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objCustomer.CIF)
                    'dedy added 26082020
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN", "GCN" & strunik, objCustomer.GCN)
                    If objCustomer.isGCNPrimary = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "NO")
                    End If
                    'dedy end added 26082020
                    If objCustomer.isUpdateFromDataSource = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "NO")
                    End If

                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, objCustomer.INDV_Title)
                    If objCustomer.INDV_Title IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, objCustomer.INDV_Title)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "INDV_Title" & strunik, "")
                    End If
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, objCustomer.INDV_Last_Name)
                    If objCustomer.INDV_Last_Name IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, objCustomer.INDV_Last_Name)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Name", "INDV_Last_Name" & strunik, "")
                    End If
                    Dim gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objCustomer.INDV_Gender).FirstOrDefault()
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, gender.Keterangan)
                    If objCustomer.INDV_Gender IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, gender.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "INDV_Gender" & strunik, "")
                    End If

                    If objCustomer.INDV_BirthDate IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Date", "INDV_BirthDate" & strunik, objCustomer.INDV_BirthDate)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Date", "INDV_BirthDate" & strunik, "")
                    End If
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, objCustomer.INDV_Birth_Place)
                    If objCustomer.INDV_Birth_Place IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, objCustomer.INDV_Birth_Place)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Birth Place", "INDV_Birth_Place" & strunik, "")
                    End If
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    If objCustomer.INDV_Mothers_Name IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Mother Name", "INDV_Mothers_Name" & strunik, objCustomer.INDV_Mothers_Name)
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Alias", "INDV_Alias" & strunik, objCustomer.INDV_Alias)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "SSN", "INDV_SSN" & strunik, objCustomer.INDV_SSN)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Number", "INDV_Passport_Number" & strunik, objCustomer.INDV_Passport_Number)
                    If objCustomer.INDV_Passport_Country IsNot Nothing And Not objCustomer.INDV_Passport_Country = "" Then
                        Dim passportcountry = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Passport_Country).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Country", "INDV_Passport_Country" & strunik, passportcountry.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Passport Country", "INDV_Passport_Country" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID_Number", "INDV_ID_Number" & strunik, objCustomer.INDV_ID_Number)
                    If objCustomer.INDV_Nationality1 IsNot Nothing And Not objCustomer.INDV_Nationality1 = "" Then
                        Dim nasionality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality1).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 1", "INDV_Nationality1" & strunik, nasionality1.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 1", "INDV_Nationality1" & strunik, "")
                    End If
                    If objCustomer.INDV_Nationality2 IsNot Nothing And Not objCustomer.INDV_Nationality2 = "" Then
                        Dim nasionality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality2).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 2", "INDV_Nationality2" & strunik, nasionality2.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 2", "INDV_Nationality2" & strunik, "")
                    End If
                    If objCustomer.INDV_Nationality3 IsNot Nothing And Not objCustomer.INDV_Nationality3 = "" Then
                        Dim nasionality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Nationality3).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 3", "INDV_Nationality3" & strunik, nasionality3.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nasionality 3", "INDV_Nationality3" & strunik, "")
                    End If
                    Dim residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.INDV_Residence).FirstOrDefault()
                    If residence IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence", "INDV_Residence" & strunik, residence.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Residence", "INDV_Residence" & strunik, "")
                    End If

                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objCustomer.INDV_Email)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 2", "INDV_Email2" & strunik, objCustomer.INDV_Email2)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 3", "INDV_Email3" & strunik, objCustomer.INDV_Email3)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 4", "INDV_Email4" & strunik, objCustomer.INDV_Email4)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email 5", "INDV_Email5" & strunik, objCustomer.INDV_Email5)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "INDV_Tax_Number" & strunik, objCustomer.INDV_Tax_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Source of Wealth", "INDV_Source_of_Wealth" & strunik, objCustomer.INDV_Source_of_Wealth)

                    If objCustomer.INDV_Deceased = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased", "INDV_Deceased" & strunik, "YES")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased Date", "INDV_Deceased_Date" & strunik, objCustomer.INDV_Deceased_Date.GetValueOrDefault.ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased", "INDV_Deceased" & strunik, "No")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Deceased Date", "INDV_Deceased_Date" & strunik, "")

                    End If
                    If objCustomer.INDV_Tax_Reg_Number = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEP ?", "INDV_Tax_Reg_Number" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PEP ?", "INDV_Tax_Reg_Number" & strunik, "No")
                    End If

                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Occupation", "INDV_Occupation" & strunik, objCustomer.INDV_Occupation)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Employer Name", "INDV_Employer_Name" & strunik, objCustomer.INDV_Employer_Name)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objCustomer.CIF)
                    'dedy added 26082020
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN", "GCN" & strunik, objCustomer.GCN)
                    If objCustomer.isGCNPrimary = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "GCN Primary ?", "isGCNPrimary" & strunik, "NO")
                    End If
                    'dedy end added 26082020
                    If objCustomer.isUpdateFromDataSource = True Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "YES")
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source ?", "isUpdateFromDataSource" & strunik, "NO")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corp Name", "Corp_Name" & strunik, objCustomer.Corp_Name)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Commercial Name", "Corp_Commercial_Name" & strunik, objCustomer.Corp_Commercial_Name)
                    If objCustomer.Corp_Incorporation_Legal_Form IsNot Nothing And Not objCustomer.Corp_Incorporation_Legal_Form = "" Then
                        Dim Corp_Legal_Form = db.goAML_Ref_Bentuk_Badan_Usaha.Where(Function(x) x.Kode = objCustomer.Corp_Incorporation_Legal_Form).FirstOrDefault()
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Legal Form", "Corp_Incorporation_Legal_Form" & strunik, Corp_Legal_Form.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Legal Form", "Corp_Incorporation_Legal_Form" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Number", "Corp_Incorporation_Number" & strunik, objCustomer.Corp_Incorporation_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Corp Bussiness", "Corp_Business" & strunik, objCustomer.Corp_Business)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "Corp_Email" & strunik, objCustomer.Corp_Email)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "URL", "Corp_Url" & strunik, objCustomer.Corp_Url)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "State", "Corp_Incorporation_State" & strunik, objCustomer.Corp_Incorporation_State)
                    Dim countrycode = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objCustomer.Corp_Incorporation_Country_Code).FirstOrDefault()
                    If countrycode IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Code", "Corp_Incorporation_Country_Code" & strunik, countrycode.Keterangan)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Country Code", "Corp_Incorporation_Country_Code" & strunik, "")
                    End If
                    If objCustomer.Corp_Incorporation_Date IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Date", "Corp_Incorporation_Date" & strunik, objCustomer.Corp_Incorporation_Date)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Incorporation Date", "Corp_Incorporation_Date" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Closed ?", "Corp_Business_Closed" & strunik, objCustomer.Corp_Business_Closed)
                    If Not objCustomer.Corp_Business_Closed = 0 Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Bussiness Closed", "Corp_Date_Business_Closed" & strunik, objCustomer.Corp_Date_Business_Closed)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Bussiness Closed", "Corp_Date_Business_Closed" & strunik, "")
                    End If
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tax Number", "Corp_Tax_Number" & strunik, objCustomer.Corp_Tax_Number)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "Corp_Comments" & strunik, objCustomer.Corp_Comments)
                End If

                '''Phone
                Dim objStore As New Ext.Net.Store
                objStore.ID = strunik & "StoreGrid"
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModel As New Ext.Net.Model
                Dim objField As Ext.Net.ModelField

                objField = New Ext.Net.ModelField
                objField.Name = "PK_goAML_Ref_Phone"
                objField.Type = ModelFieldType.Auto
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_Ref_Detail_Of"
                objField.Type = ModelFieldType.Int
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_for_Table_ID"
                objField.Type = ModelFieldType.Int
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Tph_Contact_Type"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Tph_Communication_Type"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_country_prefix"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_number"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "tph_extension"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "comments"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objStore.Model.Add(objModel)

                Dim objListcolumn As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumn.Add(objcolumnNo)
                End Using

                Dim objColum As Ext.Net.Column

                objColum = New Ext.Net.Column
                objColum.Text = "Contact Type"
                objColum.DataIndex = "Tph_Contact_Type"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Communication Type"
                objColum.DataIndex = "Tph_Communication_Type"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Country Prefix"
                objColum.DataIndex = "tph_country_prefix"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Number"
                objColum.DataIndex = "tph_number"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Extension"
                objColum.DataIndex = "tph_extension"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "Comments"
                objColum.DataIndex = "comments"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                Dim objdt As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)

                For Each item As DataRow In objdt.Rows
                    item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                Next

                For Each item As DataRow In objdt.Rows
                    item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                Next

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Informasi Telepon", objStore, objListcolumn, objdt)


                '' Address
                Dim objStoreAddress As New Ext.Net.Store
                objStoreAddress.ID = strunik & "StoreGridAddress"
                objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelAddress As New Ext.Net.Model
                Dim objFieldAddress As Ext.Net.ModelField

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "PK_Customer_Address_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_Ref_Detail_Of"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_To_Table_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address_Type"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Town"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "City"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Zip"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Country_Code"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "State"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Comments"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objStoreAddress.Model.Add(objModelAddress)


                Dim objListcolumnAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnAddress.Add(objcolumnNo)
                End Using


                Dim objColumAddress As Ext.Net.Column

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address Type"
                objColumAddress.DataIndex = "Address_Type"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address"
                objColumAddress.DataIndex = "Address"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Town"
                objColumAddress.DataIndex = "Town"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "City"
                objColumAddress.DataIndex = "City"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Zip"
                objColumAddress.DataIndex = "Zip"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Country Code"
                objColumAddress.DataIndex = "Country_Code"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "State"
                objColumAddress.DataIndex = "State"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Comment"
                objColumAddress.DataIndex = "Comments"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                Dim objdtAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Address)

                For Each item As DataRow In objdtAddress.Rows
                    item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                Next

                For Each item As DataRow In objdtAddress.Rows
                    item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                Next


                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objdtAddress)
                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Informasi Alamat", objStoreAddress, objListcolumnAddress, objdtAddress)

                '' Identification
                Dim objStoreIdentification As New Ext.Net.Store
                objStoreIdentification.ID = strunik & "StoreGridIdentification"
                objStoreIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelIdentification As New Ext.Net.Model
                Dim objFieldIdentification As Ext.Net.ModelField

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "PK_Person_Identification_ID"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "isReportingPerson"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "FK_Person_ID"
                objFieldIdentification.Type = ModelFieldType.Auto
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Type"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Number"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issue_Date"
                objFieldIdentification.Type = ModelFieldType.Date
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Expiry_Date"
                objFieldIdentification.Type = ModelFieldType.Date
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issued_By"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Issued_Country"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objFieldIdentification = New Ext.Net.ModelField
                objFieldIdentification.Name = "Identification_Comment"
                objFieldIdentification.Type = ModelFieldType.String
                objModelIdentification.Fields.Add(objFieldIdentification)

                objStoreIdentification.Model.Add(objModelIdentification)


                Dim objListcolumnIdentification As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnIdentification.Add(objcolumnNo)
                End Using


                Dim objColumIdentification As Ext.Net.Column
                Dim objColumIdentificationDate As Ext.Net.DateColumn


                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Identification Type"
                objColumIdentification.DataIndex = "Type"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Number"
                objColumIdentification.DataIndex = "Number"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentificationDate = New Ext.Net.DateColumn
                objColumIdentificationDate.Text = "Issue Date"
                objColumIdentificationDate.DataIndex = "Issue_Date"
                objColumIdentificationDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDate.Flex = 1
                objListcolumnIdentification.Add(objColumIdentificationDate)

                objColumIdentificationDate = New Ext.Net.DateColumn
                objColumIdentificationDate.Text = "Expiry Date"
                objColumIdentificationDate.DataIndex = "Expiry_Date"
                objColumIdentificationDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDate.Flex = 1
                objListcolumnIdentification.Add(objColumIdentificationDate)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Issued By"
                objColumIdentification.DataIndex = "Issued_By"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Issued Country"
                objColumIdentification.DataIndex = "Issued_Country"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                objColumIdentification = New Ext.Net.Column
                objColumIdentification.Text = "Comment"
                objColumIdentification.DataIndex = "Identification_Comment"
                objColumIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentification.Flex = 1
                objListcolumnIdentification.Add(objColumIdentification)

                If objCustomer.FK_Customer_Type_ID = 1 Then
                    Dim objdtIdentification As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification)

                    For Each item As DataRow In objdtIdentification.Rows
                        item("Type") = GetJenisDokumenByID(item("Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtIdentification.Rows
                        item("Issued_Country") = GetNamaNegaraByID(item("Issued_Country")).Keterangan
                    Next

                    'For Each item As DataRow In objdtIdentification.Rows
                    '    item("Issue_Date") = item("Issue_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next

                    'For Each item As DataRow In objdtIdentification.Rows
                    '    item("Expiry_Date") = item("Expiry_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next



                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "identification", objStoreIdentification, objListcolumnIdentification, objdtIdentification)
                End If



                '' Director
                Dim objStoreDirector As New Ext.Net.Store
                objStoreDirector.ID = strunik & "StoreGridDirectorq"
                objStoreDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirector As New Ext.Net.Model
                Dim objFieldDirector As Ext.Net.ModelField

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "PK_goAML_Ref_Customer_Entity_Director_ID"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "Last_Name"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "Role"
                objFieldDirector.Type = ModelFieldType.Auto
                objModelDirector.Fields.Add(objFieldDirector)

                objFieldDirector = New Ext.Net.ModelField
                objFieldDirector.Name = "FK_Entity_ID"
                objFieldDirector.Type = ModelFieldType.String
                objModelDirector.Fields.Add(objFieldDirector)


                objStoreDirector.Model.Add(objModelDirector)


                Dim objListcolumnDirector As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirector.Add(objcolumnNo)
                End Using


                Dim objColumDirector As Ext.Net.Column

                objColumDirector = New Ext.Net.Column
                objColumDirector.Text = "Name"
                objColumDirector.DataIndex = "Last_Name"
                objColumDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirector.Flex = 1
                objListcolumnDirector.Add(objColumDirector)

                objColumDirector = New Ext.Net.Column
                objColumDirector.Text = "Role"
                objColumDirector.DataIndex = "Role"
                objColumDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirector.Flex = 1
                objListcolumnDirector.Add(objColumDirector)

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    Dim objdtDirector As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director)

                    For Each item As DataRow In objdtDirector.Rows
                        item("Role") = GetRoleDirectorByID(item("Role")).Keterangan
                    Next


                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director", objStoreDirector, objListcolumnDirector, objdtDirector)
                End If

                '' Identification
                Dim objStoreIdentificationDirector As New Ext.Net.Store
                objStoreIdentification.ID = strunik & "StoreGridIdentificationDirector"
                objStoreIdentification.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelIdentificationDirector As New Ext.Net.Model
                Dim objFieldIdentificationDirector As Ext.Net.ModelField

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "PK_Person_Identification_ID"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "isReportingPerson"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "FK_Person_ID"
                objFieldIdentificationDirector.Type = ModelFieldType.Auto
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Type"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Number"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issue_Date"
                objFieldIdentificationDirector.Type = ModelFieldType.Date
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Expiry_Date"
                objFieldIdentificationDirector.Type = ModelFieldType.Date
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issued_By"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Issued_Country"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objFieldIdentificationDirector = New Ext.Net.ModelField
                objFieldIdentificationDirector.Name = "Identification_Comment"
                objFieldIdentificationDirector.Type = ModelFieldType.String
                objModelIdentificationDirector.Fields.Add(objFieldIdentificationDirector)

                objStoreIdentificationDirector.Model.Add(objModelIdentificationDirector)


                Dim objListcolumnIdentificationDirector As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnIdentificationDirector.Add(objcolumnNo)
                End Using


                Dim objColumIdentificationDirector As Ext.Net.Column
                Dim objColumIdentificationDirectorDate As Ext.Net.DateColumn

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Type"
                objColumIdentificationDirector.DataIndex = "Type"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Number Document"
                objColumIdentificationDirector.DataIndex = "Number"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirectorDate = New Ext.Net.DateColumn
                objColumIdentificationDirectorDate.Text = "Date"
                objColumIdentificationDirectorDate.DataIndex = "Issue_Date"
                objColumIdentificationDirectorDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirectorDate.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirectorDate)

                objColumIdentificationDirectorDate = New Ext.Net.DateColumn
                objColumIdentificationDirectorDate.Text = "Tanggal Expire"
                objColumIdentificationDirectorDate.DataIndex = "Expiry_Date"
                objColumIdentificationDirectorDate.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirectorDate.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirectorDate)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "By"
                objColumIdentificationDirector.DataIndex = "Issued_By"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Country"
                objColumIdentificationDirector.DataIndex = "Issued_Country"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                objColumIdentificationDirector = New Ext.Net.Column
                objColumIdentificationDirector.Text = "Catatan"
                objColumIdentificationDirector.DataIndex = "Identification_Comment"
                objColumIdentificationDirector.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumIdentificationDirector.Flex = 1
                objListcolumnIdentificationDirector.Add(objColumIdentificationDirector)

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    Dim objdtIdentificationDirector As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Identification_Director)

                    For Each item As DataRow In objdtIdentificationDirector.Rows
                        item("Type") = GetJenisDokumenByID(item("Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtIdentificationDirector.Rows
                        item("Issued_Country") = GetNamaNegaraByID(item("Issued_Country")).Keterangan
                    Next

                    'For Each item As DataRow In objdtIdentificationDirector.Rows
                    '    item("Issue_Date") = item("Issue_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next

                    'For Each item As DataRow In objdtIdentificationDirector.Rows
                    '    item("Expiry_Date") = item("Expiry_Date").ToString(NawaBLL.SystemParameterBLL.GetDateFormat)
                    'Next


                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Identification", objStoreIdentificationDirector, objListcolumnIdentificationDirector, objdtIdentificationDirector)

                End If


                '''Phone
                Dim objStoreEmployerPhone As New Ext.Net.Store
                objStoreEmployerPhone.ID = strunik & "StoreGridPhoneIndividuEmployer"
                objStoreEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelEmployerPhone As New Ext.Net.Model
                Dim objFieldEmployerPhone As Ext.Net.ModelField

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "PK_goAML_Ref_Phone"
                objFieldEmployerPhone.Type = ModelFieldType.Auto
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "FK_Ref_Detail_Of"
                objFieldEmployerPhone.Type = ModelFieldType.Int
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "FK_for_Table_ID"
                objFieldEmployerPhone.Type = ModelFieldType.Int
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "Tph_Contact_Type"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "Tph_Communication_Type"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_country_prefix"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_number"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "tph_extension"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objFieldEmployerPhone = New Ext.Net.ModelField
                objFieldEmployerPhone.Name = "comments"
                objFieldEmployerPhone.Type = ModelFieldType.String
                objModelEmployerPhone.Fields.Add(objFieldEmployerPhone)

                objStoreEmployerPhone.Model.Add(objModelEmployerPhone)

                Dim objListcolumnEmployerPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnEmployerPhone.Add(objcolumnNo)
                End Using

                Dim objColumEmployerPhone As Ext.Net.Column

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Kategori Kontak"
                objColumEmployerPhone.DataIndex = "Tph_Contact_Type"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Jenis Alat Komunikasi"
                objColumEmployerPhone.DataIndex = "Tph_Communication_Type"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)


                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Kode Area Telepon"
                objColumEmployerPhone.DataIndex = "tph_country_prefix"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Nomor Telepon"
                objColumEmployerPhone.DataIndex = "tph_number"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Nomor Ekstensi"
                objColumEmployerPhone.DataIndex = "tph_extension"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                objColumEmployerPhone = New Ext.Net.Column
                objColumEmployerPhone.Text = "Catatan Telepon"
                objColumEmployerPhone.DataIndex = "comments"
                objColumEmployerPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerPhone.Flex = 1
                objListcolumnEmployerPhone.Add(objColumEmployerPhone)

                Dim objdtEmployerPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Employer_Phone)

                For Each item As DataRow In objdtEmployerPhone.Rows
                    item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                Next

                'For Each item As DataRow In objdtEmployerPhone.Rows
                '    item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                'Next

                '' Address
                Dim objStoreEmployerAddress As New Ext.Net.Store
                objStoreEmployerAddress.ID = strunik & "StoreGridAddressIndividuEmployer"
                objStoreEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelEmployerAddress As New Ext.Net.Model
                Dim objFieldEmployerAddress As Ext.Net.ModelField

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "PK_Customer_Address_ID"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "FK_Ref_Detail_Of"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "FK_To_Table_ID"
                objFieldEmployerAddress.Type = ModelFieldType.Auto
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Address_Type"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Address"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Town"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "City"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Zip"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Country_Code"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "State"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objFieldEmployerAddress = New Ext.Net.ModelField
                objFieldEmployerAddress.Name = "Comments"
                objFieldEmployerAddress.Type = ModelFieldType.String
                objModelEmployerAddress.Fields.Add(objFieldEmployerAddress)

                objStoreEmployerAddress.Model.Add(objModelEmployerAddress)


                Dim objListcolumnEmployerAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnEmployerAddress.Add(objcolumnNo)
                End Using


                Dim objColumEmployerAddress As Ext.Net.Column

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kategori Alamat"
                objColumEmployerAddress.DataIndex = "Address_Type"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Alamat"
                objColumEmployerAddress.DataIndex = "Address"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kabupaten"
                objColumEmployerAddress.DataIndex = "Town"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kota"
                objColumEmployerAddress.DataIndex = "City"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Kode Pos"
                objColumEmployerAddress.DataIndex = "Zip"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Negara"
                objColumEmployerAddress.DataIndex = "Country_Code"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Ibu Kota"
                objColumEmployerAddress.DataIndex = "State"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                objColumEmployerAddress = New Ext.Net.Column
                objColumEmployerAddress.Text = "Catatan Address"
                objColumEmployerAddress.DataIndex = "Comments"
                objColumEmployerAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumEmployerAddress.Flex = 1
                objListcolumnEmployerAddress.Add(objColumEmployerAddress)

                Dim objdtEmployerAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Employer_Address)

                For Each item As DataRow In objdtEmployerAddress.Rows
                    item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                Next

                For Each item As DataRow In objdtEmployerAddress.Rows
                    item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                Next

                '''INI Buat Employer
                If objCustomer.FK_Customer_Type_ID = 1 Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employer Phone", objStoreEmployerPhone, objListcolumnEmployerPhone, objdtEmployerPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Employer Address", objStoreEmployerAddress, objListcolumnEmployerAddress, objdtEmployerAddress)

                End If

                '''Phone
                Dim objStoreDirectorPhone As New Ext.Net.Store
                objStoreDirectorPhone.ID = strunik & "StoreGridDirectorPhone"
                objStoreDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelDirectorPhone As New Ext.Net.Model
                Dim objFieldDirectorPhone As Ext.Net.ModelField

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "PK_goAML_Ref_Phone"
                objFieldDirectorPhone.Type = ModelFieldType.Auto
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "FK_Ref_Detail_Of"
                objFieldDirectorPhone.Type = ModelFieldType.Int
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "FK_for_Table_ID"
                objFieldDirectorPhone.Type = ModelFieldType.Int
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "Tph_Contact_Type"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "Tph_Communication_Type"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_country_prefix"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_number"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "tph_extension"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objFieldDirectorPhone = New Ext.Net.ModelField
                objFieldDirectorPhone.Name = "comments"
                objFieldDirectorPhone.Type = ModelFieldType.String
                objModelDirectorPhone.Fields.Add(objFieldDirectorPhone)

                objStoreDirectorPhone.Model.Add(objModelDirectorPhone)

                Dim objListcolumnDirectorPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorPhone.Add(objcolumnNo)
                End Using

                Dim objColumDirectorPhone As Ext.Net.Column

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Contact Type"
                objColumDirectorPhone.DataIndex = "Tph_Contact_Type"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Communication Type"
                objColumDirectorPhone.DataIndex = "Tph_Communication_Type"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Country Prefix"
                objColumDirectorPhone.DataIndex = "tph_country_prefix"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Number"
                objColumDirectorPhone.DataIndex = "tph_number"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Extension"
                objColumDirectorPhone.DataIndex = "tph_extension"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)

                objColumDirectorPhone = New Ext.Net.Column
                objColumDirectorPhone.Text = "Director Comments Phone"
                objColumDirectorPhone.DataIndex = "comments"
                objColumDirectorPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorPhone.Flex = 1
                objListcolumnDirectorPhone.Add(objColumDirectorPhone)
                'Update: Zikri_28092020
                Dim objdtDirectorPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Phone)
                If objCustomer.FK_Customer_Type_ID = 2 Then
                    For Each item As DataRow In objdtDirectorPhone.Rows
                        item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorPhone.Rows
                        item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                    Next
                End If
                'End Update

                '' Address
                Dim objStoreDirectorAddress As New Ext.Net.Store
                objStoreDirectorAddress.ID = strunik & "StoreGridDirectorAddress"
                objStoreDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirectorAddress As New Ext.Net.Model
                Dim objFieldDirectorAddress As Ext.Net.ModelField

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "PK_Customer_Address_ID"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "FK_Ref_Detail_Of"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "FK_To_Table_ID"
                objFieldDirectorAddress.Type = ModelFieldType.Auto
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Address_Type"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Address"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Town"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "City"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Zip"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Country_Code"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "State"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objFieldDirectorAddress = New Ext.Net.ModelField
                objFieldDirectorAddress.Name = "Comments"
                objFieldDirectorAddress.Type = ModelFieldType.String
                objModelDirectorAddress.Fields.Add(objFieldDirectorAddress)

                objStoreDirectorAddress.Model.Add(objModelDirectorAddress)


                Dim objListcolumnDirectorAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorAddress.Add(objcolumnNo)
                End Using


                Dim objColumDirectorAddress As Ext.Net.Column

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Address Type"
                objColumDirectorAddress.DataIndex = "Address_Type"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Address"
                objColumDirectorAddress.DataIndex = "Address"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Town"
                objColumDirectorAddress.DataIndex = "Town"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director City"
                objColumDirectorAddress.DataIndex = "City"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Zip"
                objColumDirectorAddress.DataIndex = "Zip"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Country Code"
                objColumDirectorAddress.DataIndex = "Country_Code"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director State"
                objColumDirectorAddress.DataIndex = "State"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)

                objColumDirectorAddress = New Ext.Net.Column
                objColumDirectorAddress.Text = "Director Comment Address"
                objColumDirectorAddress.DataIndex = "Comments"
                objColumDirectorAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorAddress.Flex = 1
                objListcolumnDirectorAddress.Add(objColumDirectorAddress)
                'Update: Zikri_28-09-2020
                Dim objdtDirectorAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Address)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorAddress.Rows
                        item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorAddress.Rows
                        item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                    Next

                End If
                'End Update

                '''Phone
                Dim objStoreDirectorEmpPhone As New Ext.Net.Store
                objStoreDirectorEmpPhone.ID = strunik & "StoreGridDirectorEmpPhone"
                objStoreDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModelDirectorEmpPhone As New Ext.Net.Model
                Dim objFieldDirectorEmpPhone As Ext.Net.ModelField

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "PK_goAML_Ref_Phone"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Auto
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "FK_Ref_Detail_Of"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Int
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "FK_for_Table_ID"
                objFieldDirectorEmpPhone.Type = ModelFieldType.Int
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "Tph_Contact_Type"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "Tph_Communication_Type"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_country_prefix"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_number"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "tph_extension"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objFieldDirectorEmpPhone = New Ext.Net.ModelField
                objFieldDirectorEmpPhone.Name = "comments"
                objFieldDirectorEmpPhone.Type = ModelFieldType.String
                objModelDirectorEmpPhone.Fields.Add(objFieldDirectorEmpPhone)

                objStoreDirectorEmpPhone.Model.Add(objModelDirectorEmpPhone)

                Dim objListcolumnDirectorEmpPhone As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorEmpPhone.Add(objcolumnNo)
                End Using

                Dim objColumDirectorEmpPhone As Ext.Net.Column

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Contact Type"
                objColumDirectorEmpPhone.DataIndex = "Tph_Contact_Type"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Communication Type"
                objColumDirectorEmpPhone.DataIndex = "Tph_Communication_Type"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Country Prefix"
                objColumDirectorEmpPhone.DataIndex = "tph_country_prefix"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Number"
                objColumDirectorEmpPhone.DataIndex = "tph_number"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Extension"
                objColumDirectorEmpPhone.DataIndex = "tph_extension"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)

                objColumDirectorEmpPhone = New Ext.Net.Column
                objColumDirectorEmpPhone.Text = "Director Emp Comments Phone"
                objColumDirectorEmpPhone.DataIndex = "comments"
                objColumDirectorEmpPhone.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpPhone.Flex = 1
                objListcolumnDirectorEmpPhone.Add(objColumDirectorEmpPhone)
                'Update: Zikri_28092020
                Dim objdtDirectorEmpPhone As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Director_Employer_Phone)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorEmpPhone.Rows
                        item("Tph_Contact_Type") = GetContactTypeByID(item("Tph_Contact_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorEmpPhone.Rows
                        item("Tph_Communication_Type") = GetCommunicationTypeByID(item("Tph_Communication_Type")).Keterangan
                    Next

                End If
                'End Udpate
                '' Address
                Dim objStoreDirectorEmpAddress As New Ext.Net.Store
                objStoreDirectorEmpAddress.ID = strunik & "StoreGridDirectorEmpAddress"
                objStoreDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelDirectorEmpAddress As New Ext.Net.Model
                Dim objFieldDirectorEmpAddress As Ext.Net.ModelField

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "PK_Customer_Address_ID"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "FK_Ref_Detail_Of"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "FK_To_Table_ID"
                objFieldDirectorEmpAddress.Type = ModelFieldType.Auto
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Address_Type"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Address"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Town"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "City"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Zip"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Country_Code"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "State"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objFieldDirectorEmpAddress = New Ext.Net.ModelField
                objFieldDirectorEmpAddress.Name = "Comments"
                objFieldDirectorEmpAddress.Type = ModelFieldType.String
                objModelDirectorEmpAddress.Fields.Add(objFieldDirectorEmpAddress)

                objStoreDirectorEmpAddress.Model.Add(objModelDirectorEmpAddress)


                Dim objListcolumnDirectorEmpAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnDirectorEmpAddress.Add(objcolumnNo)
                End Using


                Dim objColumDirectorEmpAddress As Ext.Net.Column

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Address Type"
                objColumDirectorEmpAddress.DataIndex = "Address_Type"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Address"
                objColumDirectorEmpAddress.DataIndex = "Address"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Town"
                objColumDirectorEmpAddress.DataIndex = "Town"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp City"
                objColumDirectorEmpAddress.DataIndex = "City"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Zip"
                objColumDirectorEmpAddress.DataIndex = "Zip"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Country Code"
                objColumDirectorEmpAddress.DataIndex = "Country_Code"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp State"
                objColumDirectorEmpAddress.DataIndex = "State"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)

                objColumDirectorEmpAddress = New Ext.Net.Column
                objColumDirectorEmpAddress.Text = "Director Emp Comment Address"
                objColumDirectorEmpAddress.DataIndex = "Comments"
                objColumDirectorEmpAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumDirectorEmpAddress.Flex = 1
                objListcolumnDirectorEmpAddress.Add(objColumDirectorEmpAddress)
                'Update: Zikri_28092020
                Dim objdtDirectorEmpAddress As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_Ref_Director_Employer_Address)
                If objCustomer.FK_Customer_Type_ID = 2 Then

                    For Each item As DataRow In objdtDirectorEmpAddress.Rows
                        item("Address_Type") = GetAddressTypeByID(item("Address_Type")).Keterangan
                    Next

                    For Each item As DataRow In objdtDirectorEmpAddress.Rows
                        item("Country_Code") = GetNamaNegaraByID(item("Country_Code")).Keterangan
                    Next

                End If
                'End Update

                If objCustomer.FK_Customer_Type_ID = 2 Then
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Address", objStoreDirectorAddress, objListcolumnDirectorAddress, objdtDirectorAddress)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Phone", objStoreDirectorPhone, objListcolumnDirectorPhone, objdtDirectorPhone)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Empolyer Address", objStoreDirectorEmpAddress, objListcolumnDirectorEmpAddress, objdtDirectorEmpAddress)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Director Employer Phone", objStoreDirectorEmpPhone, objListcolumnDirectorEmpPhone, objdtDirectorEmpPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Director", objStoreDirectorAddress, objListcolumnDirectorAddress, objdtDirectorAddress)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Director", objStoreDirectorPhone, objListcolumnDirectorPhone, objdtDirectorPhone)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Alamat Tempat Bekerja Director", objStoreDirectorEmpAddress, objListcolumnDirectorEmpAddress, objdtDirectorEmpAddress)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Telepon Tempat Bekerja Director", objStoreDirectorEmpPhone, objListcolumnDirectorEmpPhone, objdtDirectorEmpPhone)


                End If

            End If
        End Using

    End Sub
    Shared Sub SettingColor(objPanelOld As FormPanel, objPanelNew As FormPanel, objdata As String, objdatabefore As String, unikkeyold As String, unikkeynew As String)

        If objdata.Length > 0 And objdatabefore.Length > 0 Then
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("PK_Customer_ID", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("CIF", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("AccountNo", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Opened", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Closed", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Gender", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("date_balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("status_code", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary_comment", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("comments", objPanelOld, objPanelNew, unikkeyold, unikkeynew)

        End If
    End Sub

    Shared Function Accept(ID As String) As Boolean
        'done: accept


        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objCustomer As NawaDevDAL.goAML_Ref_Customer = objModuledata.objgoAML_Ref_Customer
                            Dim objDirector As List(Of NawaDevDAL.goAML_Ref_Customer_Entity_Director) = objModuledata.objListgoAML_Ref_Director
                            Dim objPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Phone
                            Dim objAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Address
                            Dim objEmployerPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Employer_Phone
                            Dim objEmployerAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Employer_Address
                            Dim objIdentification As List(Of NawaDevDAL.goAML_Person_Identification) = objModuledata.objListgoAML_Ref_Identification
                            Dim objDirectorPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Director_Phone
                            Dim objDirectorAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objModuledata.objListgoAML_Ref_Director_Address
                            Dim objDirectorEmployerPhone As List(Of NawaDevDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Director_Employer_Phone
                            Dim objDirectorEmployerAddress As List(Of NawaDevDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Director_Employer_Address
                            Dim objIdentificationDirector As List(Of NawaDevDAL.goAML_Person_Identification) = objModuledata.objListgoAML_Ref_Identification_Director
                            'Update: Zikri_14092020 Add Validation GCN
                            If objCustomer.GCN IsNot Nothing Then
                                If objCustomer.isGCNPrimary = True Then
                                    Dim checkGCN As goAML_Ref_Customer = NawaDevBLL.goAML_CustomerBLL.GetCheckGCN(objCustomer.GCN)
                                    If checkGCN IsNot Nothing Then
                                        If checkGCN.CIF <> objCustomer.CIF Then
                                            checkGCN.isGCNPrimary = 0
                                            checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.ApprovedDate = Now
                                            checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.LastUpdateDate = Now
                                            objdb.Entry(checkGCN).State = Entity.EntityState.Modified
                                            objdb.SaveChanges()
                                        End If
                                    End If
                                End If
                            End If
                            'End Update
                            objCustomer.Active = 1
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objCustomer.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objCustomer.Alternateby = ""
                            End If
                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now
                            objdb.Entry(objCustomer).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            If objCustomer.FK_Customer_Type_ID = 2 Then
                                For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objDirector
                                    item.FK_Entity_ID = objCustomer.CIF
                                    Dim addressdirector = objDirectorAddress.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim phonedirector = objDirectorPhone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim addressempdirector = objDirectorEmployerAddress.Where(Function(x) x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim phoneempdirector = objDirectorEmployerPhone.Where(Function(x) x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID).ToList()
                                    Dim identificationdirector = objIdentificationDirector.Where(Function(x) x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4).ToList()
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()

                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In addressdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In phonedirector
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In addressempdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Person_Identification In identificationdirector
                                        itemx.Active = 1
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next

                                    objdb.SaveChanges()
                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In phoneempdirector
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            itemx.Alternateby = ""
                                        End If
                                        itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.ApprovedDate = Now
                                        itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        itemx.CreatedDate = Now
                                        itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                        objdb.Entry(itemx).State = Entity.EntityState.Added
                                    Next
                                    objdb.SaveChanges()
                                Next
                            End If

                            For Each item As NawaDevDAL.goAML_Ref_Phone In objPhone
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next
                            objdb.SaveChanges()

                            For Each item As NawaDevDAL.goAML_Ref_Address In objAddress
                                item.Active = 1
                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                    item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                Else
                                    item.Alternateby = ""
                                End If
                                item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.ApprovedDate = Now
                                item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                item.CreatedDate = Now
                                item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next
                            objdb.SaveChanges()

                            If objCustomer.FK_Customer_Type_ID = 1 Then
                                For Each item As NawaDevDAL.goAML_Person_Identification In objIdentification
                                    item.Active = 1
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_Person_ID = objCustomer.PK_Customer_ID
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                                For Each item As NawaDevDAL.goAML_Ref_Address In objEmployerAddress
                                    item.Active = 1
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                    item.FK_Ref_Detail_Of = 5
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                                For Each item As NawaDevDAL.goAML_Ref_Phone In objEmployerPhone
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                    item.FK_Ref_Detail_Of = 5
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Next
                                objdb.SaveChanges()
                            End If

                            Using objDba As New NawaDatadevEntities
                                Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                                'AuditTrailDetail
                                If objPhone.Count > 0 Then
                                    For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objPhone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                                    Next
                                End If

                                If objAddress.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Address In objAddress
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objCustomer.FK_Customer_Type_ID = 1 Then

                                    If objIdentification.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Person_Identification In objIdentification
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objEmployerAddress.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objEmployerAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objEmployerPhone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objEmployerPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If
                                End If

                                If objCustomer.FK_Customer_Type_ID = 2 Then
                                    If objDirectorEmployerAddress.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objDirectorEmployerAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objDirectorEmployerPhone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objDirectorEmployerPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objDirectorAddress.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objDirectorAddress
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objDirectorPhone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objDirectorPhone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objIdentificationDirector.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Person_Identification In objIdentificationDirector
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                End If

                            End Using
                        'AuditTrail

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objModuledataOld As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_CustomerDataBLL))

                            Dim objCustomer As NawaDevDAL.goAML_Ref_Customer = objModuledata.objgoAML_Ref_Customer
                            'Update: Zikri_14092020 Add Validation GCN
                            If objCustomer.GCN IsNot Nothing Then
                                If objCustomer.isGCNPrimary = True Then
                                    Dim checkGCN As goAML_Ref_Customer = NawaDevBLL.goAML_CustomerBLL.GetCheckGCN(objCustomer.GCN)
                                    If checkGCN IsNot Nothing Then
                                        If checkGCN.CIF <> objCustomer.CIF Then
                                            checkGCN.isGCNPrimary = 0
                                            checkGCN.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.ApprovedDate = Now
                                            checkGCN.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            checkGCN.LastUpdateDate = Now
                                            objdb.Entry(checkGCN).State = Entity.EntityState.Modified
                                            objdb.SaveChanges()
                                        End If
                                    End If
                                End If
                            End If
                            'End Update

                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objCustomer.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objCustomer.Alternateby = ""
                            End If
                            objCustomer.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.LastUpdateDate = Now
                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now
                            objdb.goAML_Ref_Customer.Attach(objCustomer)
                            objdb.Entry(objCustomer).State = Entity.EntityState.Modified

                            If objCustomer.FK_Customer_Type_ID = 2 Then
                                For Each itemx As NawaDevDAL.goAML_Ref_Customer_Entity_Director In (From x In objdb.goAML_Ref_Customer_Entity_Director Where x.FK_Entity_ID = objCustomer.CIF Select x).ToList
                                    Dim objcek As NawaDevDAL.goAML_Ref_Customer_Entity_Director = objModuledata.objListgoAML_Ref_Director.Find(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = itemx.PK_goAML_Ref_Customer_Entity_Director_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                objdb.SaveChanges()

                                For Each item As NawaDevDAL.goAML_Ref_Customer_Entity_Director In objModuledata.objListgoAML_Ref_Director
                                    Dim obcek As NawaDevDAL.goAML_Ref_Customer_Entity_Director = (From x In objdb.goAML_Ref_Customer_Entity_Director Where x.PK_goAML_Ref_Customer_Entity_Director_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        item.FK_Entity_ID = objCustomer.CIF
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                    objdb.SaveChanges()

                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                        Dim objcek As NawaDevDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                        Dim objcek As NawaDevDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Director_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 6 Select x).ToList
                                        Dim objcek As NawaDevDAL.goAML_Ref_Address = objModuledata.objListgoAML_Ref_Director_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Ref_Detail_Of = 7 Select x).ToList
                                        Dim objcek As NawaDevDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Director_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Person_Identification In (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID And x.FK_Person_Type = 4 Select x).ToList
                                        Dim objcek As NawaDevDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification_Director.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                        If objcek Is Nothing Then
                                            objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification_Director
                                        Dim objcek As NawaDevDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_Person_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_Person_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Employer_Phone
                                        Dim objcek As NawaDevDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else

                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Phone
                                        Dim objcek As NawaDevDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_for_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_for_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Director_Employer_Address
                                        Dim objcek As NawaDevDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.PK_Customer_Address_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next

                                    For Each itemx As NawaDevDAL.goAML_Ref_Address In objModuledata.objListgoAML_Ref_Director_Address
                                        Dim objcek As NawaDevDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID Select x).FirstOrDefault
                                        If objcek Is Nothing Then
                                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                itemx.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                            Else
                                                itemx.Alternateby = ""
                                            End If
                                            itemx.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.ApprovedDate = Now
                                            itemx.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            itemx.CreatedDate = Now
                                            itemx.FK_To_Table_ID = item.PK_goAML_Ref_Customer_Entity_Director_ID
                                            objdb.Entry(itemx).State = Entity.EntityState.Added
                                        Else
                                            Dim objcekdirector = objdb.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.PK_goAML_Ref_Customer_Entity_Director_ID = objcek.FK_To_Table_ID).ToList()
                                            If objcekdirector Is Nothing Then
                                                objdb.Entry(objcek).State = Entity.EntityState.Deleted
                                            Else
                                                objdb.Entry(objcek).CurrentValues.SetValues(itemx)
                                                If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                                    objcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                                Else
                                                    objcek.Alternateby = ""
                                                End If
                                                objcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.LastUpdateDate = Now
                                                objcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                objcek.ApprovedDate = Now
                                                objdb.Entry(objcek).State = Entity.EntityState.Modified
                                            End If
                                        End If
                                    Next
                                Next
                            End If


                            For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = objCustomer.PK_Customer_ID Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                Dim obcek As NawaDevDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID Select x).ToList
                                Dim objcek As NawaDevDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                Dim obcek As NawaDevDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.CreatedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            If objCustomer.FK_Customer_Type_ID = 1 Then
                                For Each itemx As NawaDevDAL.goAML_Person_Identification In (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Person_Type = 1 Select x).ToList
                                    'Fix fk_personID diubah menjadi pk Saad  22102020
                                    'Dim objcek As NawaDevDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification.Find(Function(x) x.FK_Person_ID = itemx.PK_Person_Identification_ID)
                                    Dim objcek As NawaDevDAL.goAML_Person_Identification = objModuledata.objListgoAML_Ref_Identification.Find(Function(x) x.PK_Person_Identification_ID = itemx.PK_Person_Identification_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As NawaDevDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification
                                    'Fix fk_personID diubah menjadi pk Saad  22102020
                                    'Dim obcek As NawaDevDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.FK_Person_ID = item.PK_Person_Identification_ID Select x).FirstOrDefault
                                    Dim obcek As NawaDevDAL.goAML_Person_Identification = (From x In objdb.goAML_Person_Identification Where x.PK_Person_Identification_ID = item.PK_Person_Identification_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                                For Each itemx As NawaDevDAL.goAML_Ref_Phone In (From x In objdb.goAML_Ref_Phone Where x.FK_for_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                                    Dim objcek As NawaDevDAL.goAML_Ref_Phone = objModuledata.objListgoAML_Ref_Employer_Phone.Find(Function(x) x.PK_goAML_Ref_Phone = itemx.PK_goAML_Ref_Phone)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Employer_Phone
                                    Dim obcek As NawaDevDAL.goAML_Ref_Phone = (From x In objdb.goAML_Ref_Phone Where x.PK_goAML_Ref_Phone = item.PK_goAML_Ref_Phone Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                                For Each itemx As NawaDevDAL.goAML_Ref_Address In (From x In objdb.goAML_Ref_Address Where x.FK_To_Table_ID = objModuledata.objgoAML_Ref_Customer.PK_Customer_ID And x.FK_Ref_Detail_Of = 5 Select x).ToList
                                    Dim objcek As NawaDevDAL.goAML_Ref_Address = objModuledata.objlistgoaml_Ref_Employer_Address.Find(Function(x) x.PK_Customer_Address_ID = itemx.PK_Customer_Address_ID)
                                    If objcek Is Nothing Then
                                        objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                    End If
                                Next
                                For Each item As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Employer_Address
                                    Dim obcek As NawaDevDAL.goAML_Ref_Address = (From x In objdb.goAML_Ref_Address Where x.PK_Customer_Address_ID = item.PK_Customer_Address_ID Select x).FirstOrDefault
                                    If obcek Is Nothing Then
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            item.Alternateby = ""
                                        End If
                                        item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.ApprovedDate = Now
                                        item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        item.CreatedDate = Now
                                        objdb.Entry(item).State = Entity.EntityState.Added
                                    Else
                                        objdb.Entry(obcek).CurrentValues.SetValues(item)
                                        If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                            obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                        Else
                                            obcek.Alternateby = ""
                                        End If
                                        obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.LastUpdateDate = Now
                                        obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        obcek.ApprovedDate = Now
                                        objdb.Entry(obcek).State = Entity.EntityState.Modified
                                    End If
                                Next
                                objdb.SaveChanges()
                            End If

                            Using objDba As New NawaDatadevEntities
                                Dim header As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)

                                'AuditTrailDetail
                                If objModuledata.objListgoAML_Ref_Phone.Count > 0 Then
                                    For Each userPhone As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, userPhone)
                                    Next
                                End If

                                If objModuledata.objlistgoaml_Ref_Address.Count > 0 Then
                                    For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                    Next
                                End If

                                If objModuledata.objgoAML_Ref_Customer.FK_Customer_Type_ID = 1 Then

                                    If objModuledata.objListgoAML_Ref_Identification.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objlistgoaml_Ref_Employer_Address.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Employer_Address
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Employer_Phone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Employer_Phone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If
                                End If

                                If objModuledata.objgoAML_Ref_Customer.FK_Customer_Type_ID = 2 Then
                                    If objModuledata.objlistgoaml_Ref_Director_Employer_Address.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Director_Employer_Address
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Employer_Phone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Employer_Phone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Address.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objListgoAML_Ref_Director_Address
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Director_Phone.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Director_Phone
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                    If objModuledata.objListgoAML_Ref_Identification_Director.Count > 0 Then
                                        For Each itemheader As NawaDevDAL.goAML_Person_Identification In objModuledata.objListgoAML_Ref_Identification_Director
                                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, itemheader)
                                        Next
                                    End If

                                End If

                            End Using
                        'AuditTrail


                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Deleted

                            For Each item As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            For Each item As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            'For Each item As EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'For Each item As EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'audittrail
                            Dim objaudittrailheader As New NawaDevDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next
                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next


                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next



                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Modified

                            'audittrail
                            Dim objaudittrailheader As New NawaDevDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDevDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    'Shared Sub LoadPanelActivation(objPanel As FormPanel, objmodulename As String, unikkey As String)
    '    'done: code anelActivation


    '    'Dim objEmailTemplateDataBLL As NawaBLL.EmailTemplateDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(NawaBLL.EmailTemplateDataBLL))
    '    Dim objgoAML_Customer As NawaDevDAL.goAML_Ref_Customer = GetCustomer(unikkey)
    '    Dim objListgoAML_Ref_Phone As List(Of NawaDevDAL.goAML_Ref_Phone) = GetListgoAML_Ref_PhoneByPKID(unikkey)
    '    Dim objListgoAML_Ref_Address As List(Of NawaDevDAL.goAML_Ref_Address) = GetListgoAML_Ref_AddressByPKID(unikkey)

    '    Dim INDV_gender As NawaDevDAL.goAML_Ref_Jenis_Kelamin = Nothing
    '    Dim INDV_statusCode As NawaDevDAL.goAML_Ref_Status_Rekening = Nothing
    '    Dim INDV_nationality1 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality2 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality3 As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_residence As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Incorporation_legal_form As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
    '    Dim CORP_incorporation_country_code As NawaDevDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Role As NawaDevDAL.goAML_Ref_Party_Role = Nothing

    '    Using db As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    '        If Not objgoAML_Customer Is Nothing Then
    '            Dim strunik As String = unikkey
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objgoAML_Customer.CIF)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objgoAML_Customer.AccountNo)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, CDate(objgoAML_Customer.Opened).ToString("dd-MMM-yyyy"))
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, CDate(objgoAML_Customer.Closed).ToString("dd-MMM-yyyy"))

    '            INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objgoAML_Customer.Gender).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objgoAML_Customer.Balance)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objgoAML_Customer.Date_Balance).ToString("dd-MMM-yyyy"))

    '            INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objgoAML_Customer.Status_Code).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objgoAML_Customer.Beneficiary)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, objgoAML_Customer.Comments)

    '            If objgoAML_Customer.FK_Customer_Type_ID = "1" Then
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "INDV_last_name" & strunik, objgoAML_Customer.INDV_Last_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "INDV_Birthdate" & strunik, CDate(objgoAML_Customer.INDV_BirthDate).ToString("dd-MMM-yyyy"))
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "INDV_Birth_place" & strunik, objgoAML_Customer.INDV_Birth_Place)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "INDV_Mothers_name" & strunik, objgoAML_Customer.INDV_Mothers_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "INDV_Alias" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "INDV_SSN" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Passport", "INDV_Passport_number" & strunik, objgoAML_Customer.INDV_Passport_Number)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Passport", "INDV_Passport_country" & strunik, objgoAML_Customer.INDV_Passport_Country)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lain", "INDV_ID_Number" & strunik, objgoAML_Customer.INDV_ID_Number)

    '                INDV_nationality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality1).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "INDV_Nationality1" & strunik, INDV_nationality1.Keterangan)

    '                INDV_nationality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality2).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "INDV_Nationality2" & strunik, INDV_nationality2.Keterangan)

    '                INDV_nationality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality3).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "INDV_Nationality3" & strunik, INDV_nationality3.Keterangan)

    '                INDV_residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Residence).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "INDV_residence" & strunik, INDV_residence.Keterangan)

    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objgoAML_Customer.INDV_Email)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "INDV_Occupation" & strunik, objgoAML_Customer.INDV_Occupation)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "INDV_employer_name" & strunik, objgoAML_Customer.INDV_Occupation)

    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

    '            Else
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

    '            End If

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objgoAML_Customer.Active.ToString & " -->" & (Not objgoAML_Customer.Active).ToString)

    '            Dim objStore As New Ext.Net.Store
    '            objStore.ID = strunik & "StoreGrid"
    '            objStore.ClientIDMode = Web.UI.ClientIDMode.Static

    '            Dim objModel As New Ext.Net.Model
    '            Dim objField As Ext.Net.ModelField

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "PK_goAML_ref_phone"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_Ref_detail_of"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_for_Table_ID"
    '            objField.Type = ModelFieldType.Int
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_contact_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_communication_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_country_prefix"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_number"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_extension"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "comments"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objStore.Model.Add(objModel)



    '            Dim objListcolumn As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumn.Add(objcolumnNo)
    '            End Using


    '            Dim objColum As Ext.Net.Column


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Detail Dari"
    '            objColum.DataIndex = "FK_Ref_detail_of"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)



    '            objColum = New Ext.Net.Column
    '            objColum.Text = "PK"
    '            objColum.DataIndex = "FK_for_Table_ID"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kategori Kontak"
    '            objColum.DataIndex = "tph_contact_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Jenis Alat Komunikasi"
    '            objColum.DataIndex = "tph_communication_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kode Area Telp"
    '            objColum.DataIndex = "tph_country_prefix"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Telepon"
    '            objColum.DataIndex = "tph_number"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Extensi"
    '            objColum.DataIndex = "tph_extension"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Catatan"
    '            objColum.DataIndex = "comments"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            Dim objdt As Data.DataTable = CopyGenericToDataTable(objListgoAML_Ref_Phone)
    '            Dim objcol As New Data.DataColumn
    '            objcol.ColumnName = "Detail of"
    '            objcol.DataType = GetType(String)
    '            objdt.Columns.Add(objcol)

    '            For Each item As DataRow In objdt.Rows
    '                Dim objtask As NawaDevDAL.goAML_For_Table = GetTabelTypeForByID(item("FK_Ref_Detail_Of"))
    '                If Not objtask Is Nothing Then
    '                    item("FK_Ref_Detail_Of") = objtask.Description
    '                End If
    '            Next

    '            '' ini
    '            Dim objStoreAddress As New Ext.Net.Store
    '            objStoreAddress.ID = strunik & "StoreGridReplacer"
    '            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            Dim objModelAddress As New Ext.Net.Model
    '            Dim objFieldAddress As Ext.Net.ModelField




    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "PK_Customer_Address_ID"
    '            objFieldAddress.Type = ModelFieldType.Auto
    '            objModelAddress.Fields.Add(objFieldAddress)


    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_Ref_Detail_Of"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_To_Table_ID"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address_Type"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Town"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "City"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Zip"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Country_Code"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "State"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Comments"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objStoreAddress.Model.Add(objModelAddress)


    '            Dim objListcolumnAddress As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumnAddress.Add(objcolumnNo)
    '            End Using

    '            '' Address
    '            Dim objColumAddress As Ext.Net.Column

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Detail dari"
    '            objColumAddress.DataIndex = "FK_Ref_Detail_Of"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "PK"
    '            objColumAddress.DataIndex = "FK_To_Table_ID"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address Type"
    '            objColumAddress.DataIndex = "Address_Type"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address"
    '            objColumAddress.DataIndex = "Address"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Town"
    '            objColumAddress.DataIndex = "Town"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "City"
    '            objColumAddress.DataIndex = "City"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Zip"
    '            objColumAddress.DataIndex = "Zip"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Country Code"
    '            objColumAddress.DataIndex = "Country_Code"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "State"
    '            objColumAddress.DataIndex = "State"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Comments"
    '            objColumAddress.DataIndex = "Comments"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)
    '            '' Address


    '            'Dim objStoreAction As New Store
    '            'objStoreAction.ID = strunik & "StoreGridAction"
    '            'objStoreAction.ClientIDMode = Web.UI.ClientIDMode.Static
    '            'Dim ObjModelAction As New Ext.Net.Model

    '            'ObjModelAction.Fields.Add(New ModelField("EmailActionType", ModelFieldType.String))
    '            'ObjModelAction.Fields.Add(New ModelField("TSQLtoExecute", ModelFieldType.String))

    '            'objStoreAction.Model.Add(ObjModelAction)

    '            'Dim objListcolumnAction As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAction.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "EmailActionType", .DataIndex = "EmailActionType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "TSQLtoExecute", .DataIndex = "TSQLtoExecute", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})


    '            'Dim objdtaction As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAction)
    '            'Dim objcolaction As New Data.DataColumn
    '            'objcolaction.ColumnName = "EmailActionType"
    '            'objcolaction.DataType = GetType(String)
    '            'objdtaction.Columns.Add(objcolaction)


    '            'For Each item As DataRow In objdtaction.Rows
    '            '    Dim objtask As NawaDAL.EmailActionType = NawaBLL.EmailTemplateBLL.GetEmailActionTypeByID(item("FK_EmailActionType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailActionType") = objtask.EmailActionTypeName
    '            '    End If

    '            'Next

    '            'Dim objStoreAttachment As New Store With {.ID = "StoreGridAttachment", .ClientIDMode = Web.UI.ClientIDMode.Static}
    '            'Dim ObjModelAttachment As New Ext.Net.Model

    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailAttachmentType", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("ParameterReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaFile", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailRenderAsName", ModelFieldType.String))
    '            'objStoreAttachment.Model.Add(ObjModelAttachment)



    '            'Dim objListcolumnAttachment As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAttachment.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Email Attachment Type", .DataIndex = "EmailAttachmentType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Name", .DataIndex = "NamaReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Parameter", .DataIndex = "ParameterReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report File Name", .DataIndex = "NamaFile", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Render As", .DataIndex = "EmailRenderAsName", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})



    '            'Dim objdtattachment As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAttachment)

    '            'Dim objcolAttachment As New Data.DataColumn
    '            'objcolAttachment.ColumnName = "EmailAttachmentType"
    '            'objcolAttachment.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment)

    '            'Dim objcolAttachment1 As New Data.DataColumn
    '            'objcolAttachment1.ColumnName = "EmailRenderAsName"
    '            'objcolAttachment1.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment1)

    '            'For Each item As DataRow In objdtattachment.Rows
    '            '    Dim objtask As NawaDAL.EmailAttachmentType = NawaBLL.EmailTemplateBLL.GetEMailAttachmentTypebypk(item("FK_EmailAttachmentType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailAttachmentType") = objtask.EmailAttachmentType1
    '            '    End If
    '            '    If item("FK_EmailRenderAs_Id").ToString <> "" Then
    '            '        Dim objtask1 As NawaDAL.EmailRenderA = NawaBLL.EmailTemplateBLL.GetEmailRenderAsbypk(item("FK_EmailRenderAs_Id"))
    '            '        If Not objtask1 Is Nothing Then
    '            '            item("EmailRenderAsName") = objtask1.EmailRenderAsName
    '            '        End If
    '            '    End If

    '            'Next


    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objListgoAML_Ref_Address)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Action", objStoreAction, objListcolumnAction, objdtaction)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Attachment", objStoreAttachment, objListcolumnAttachment, objdtattachment)

    '        End If
    '    End Using

    'End Sub

    Shared Function Reject(ID As String) As Boolean
        'done:reject
        Using objdb As New NawaDAL.NawaDataEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next




                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Update

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objModuledataOld As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_CustomerDataBLL))

                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledataOld.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledataOld, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledataOld, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objdb.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))


                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As NawaDevDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As NawaDevDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function
#End Region
End Class
