﻿Imports NawaDAL
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDevDAL
Imports System.Web

Public Class AMLJudgementBLL
    Public Shared Function GetAMLJudgementHeader(ByVal id As String) As AML_SCREENING_RESULT
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT Where X.PK_AML_SCREENING_RESULT_ID = id Select X).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetAMLJudgementHeaderByCIFNO(ByVal id As String) As AML_SCREENING_RESULT
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT Where X.CIF_NO = id Select X).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeaderByTransaction(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeader(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.PK_AML_SCREENING_RESULT_TRANSACTION_ID = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionListByTransactionNumber(ByVal id As String) As List(Of AML_SCREENING_RESULT_TRANSACTION)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id And X.FK_FromOrToOrConductor_Code = 5 Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionListyCIFNOLAWAN(ByVal id As String, ByVal cif As String) As List(Of AML_SCREENING_RESULT_TRANSACTION)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id And X.CIF_NO = cif Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionListByWICNOLAWAN(ByVal id As String, ByVal wic As String) As List(Of AML_SCREENING_RESULT_TRANSACTION)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id And X.WIC_NO = wic Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeaderByTransNumberAndCifno(ByVal id As String, ByVal cifno As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id And X.CIF_NO = cifno Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeaderByTransNumberAndRespondID(ByVal id As String, ByVal respon As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.TRANSACTION_NUMBER = id And X.RESPONSE_ID = respon And X.FK_FromOrToOrConductor_Code = 5 Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeaderByCIFNO(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.CIF_NO = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionHeaderByWICNO(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION Where X.WIC_NO = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementWatchlist(ByVal id As String) As AML_WIC
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_WIC Where X.WIC_No = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransaction(ByVal id As String) As AML_TRANSACTION
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_TRANSACTION Where X.TRANSACTION_NUMBER = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionDetailByPK(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION_DETAIL Where X.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionDetailByFK(ByVal id As String) As AML_SCREENING_RESULT_TRANSACTION_DETAIL
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION_DETAIL Where X.FK_AML_SCREENING_RESULT_TRANSACTION_ID = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionWICMATCH(ByVal id As String) As AML_SCREENING_WIC_MATCH
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_WIC_MATCH Where X.WIC_NO = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionCIFMATCH(ByVal id As String) As AML_SCREENING_CUSTOMER_MATCH
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_CUSTOMER_MATCH Where X.CIF_NO = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementCurrency(ByVal id As String) As AML_CURRENCY
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_CURRENCY Where X.FK_AML_CURRENCY_CODE = id Select X).FirstOrDefault
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionList(ByVal id As String) As List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION_DETAIL Where X.FK_AML_SCREENING_RESULT_TRANSACTION_ID = id Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLJudgementTransactionListByWIC(ByVal id As String) As List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_TRANSACTION_DETAIL Where X.FK_AML_SCREENING_RESULT_TRANSACTION_ID = id Select X).ToList
        End Using
    End Function

    Public Shared Function GetAMLJudgementList(ByVal id As String) As List(Of AML_SCREENING_RESULT_DETAIL)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Return (From X In NawaDataEntitiy.AML_SCREENING_RESULT_DETAIL Where X.FK_AML_SCREENING_RESULT_ID = id And X.FK_AML_WATCHLIST_ID IsNot Nothing Select X).ToList
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetAMLCustomerByCif(ByVal id As String) As AML_CUSTOMER
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Return (From X In NawaDataEntitiy.AML_CUSTOMER Where X.CIFNo = id Select X).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetWatchlistCategoryByID(Kode As String) As String
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Dim val = (From X In NawaDataEntitiy.AML_WATCHLIST_CATEGORY Where X.PK_AML_WATCHLIST_CATEGORY_ID = Kode).FirstOrDefault
                If val IsNot Nothing Then
                    Return val.CATEGORY_NAME
                Else
                    Return "Category Code is not Registered"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetWatchlistTypeByID(Kode As String) As String
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Dim val = (From X In NawaDataEntitiy.AML_WATCHLIST_TYPE Where X.PK_AML_WATCHLIST_TYPE_ID = Kode).FirstOrDefault
                If val IsNot Nothing Then
                    Return val.TYPE_NAME
                Else
                    Return "Type Code is not Registered"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetRequestForHeader(Kode As String) As AML_SCREENING_REQUEST
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Return (From X In NawaDataEntitiy.AML_SCREENING_REQUEST Where X.PK_AML_SCREENING_REQUEST_ID = Kode).FirstOrDefault
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    'daniel 20210419
    Public Shared Function GetWatchlistAlreadyMatchByCIF(ByVal id As String) As List(Of AML_SCREENING_CUSTOMER_MATCH)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Dim listinmatch As List(Of AML_SCREENING_CUSTOMER_MATCH) = NawaDataEntitiy.AML_SCREENING_CUSTOMER_MATCH.Where(Function(x) x.CIF_NO = id).ToList
                Return (listinmatch)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetWatchlistAlreadyMatchTransactionByWICNO(ByVal id As String) As List(Of AML_SCREENING_WIC_MATCH)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Try
                Dim listinmatch As List(Of AML_SCREENING_WIC_MATCH) = NawaDataEntitiy.AML_SCREENING_WIC_MATCH.Where(Function(x) x.WIC_NO = id).ToList
                Return (listinmatch)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

#Region "Save to Approval untuk request re open"
    Public Shared Sub RequestUpdateStatusCode(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                    data.AMLJudgementHeader.LastUpdateDate = Now
                    data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "5"
                    objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                    objDB.SaveChanges()

                    Dim dataXML As String = Common.Serialize(data)

                    Dim headerbefore As New AML_SCREENING_RESULT
                    Dim listitembefore As New List(Of AML_SCREENING_RESULT_DETAIL)
                    headerbefore = objDB.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID).FirstOrDefault
                    objDB.Entry(headerbefore).State = EntityState.Detached
                    For Each item In data.AMLListResult
                        Dim itembefore As New AML_SCREENING_RESULT_DETAIL
                        itembefore = objDB.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                        If itembefore IsNot Nothing Then
                            listitembefore.Add(itembefore)
                        End If
                    Next

                    Dim databefore As New AMLJudgementClass
                    With databefore
                        .AMLJudgementHeader = headerbefore
                        .AMLListResult = listitembefore
                    End With
                    Dim dataXMLBefore As String = Common.Serialize(databefore)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With
                    objDB.Entry(objModuleApproval).State = EntityState.Added
                    objDB.SaveChanges()
                    If data.AMLListResult.Count > 0 Then
                        data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                        data.AMLJudgementHeader.LastUpdateDate = Now
                        objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                        objDB.SaveChanges()
                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = objModule.ModuleLabel
                        Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, data.AMLJudgementHeader)
                        objDB.SaveChanges()
                        For Each item As AML_SCREENING_RESULT_DETAIL In data.AMLListResult
                            Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objDB.AML_SCREENING_RESULT_DETAIL
                                                                        Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                                                                        Select x).FirstOrDefault
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Next
                    End If
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
#End Region

    Public Shared Sub SaveJudgementApproval(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                    data.AMLJudgementHeader.LastUpdateDate = Now
                    objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                    objDB.SaveChanges()

                    Dim dataXML As String = Common.Serialize(data)

                    Dim headerbefore As New AML_SCREENING_RESULT
                    Dim listitembefore As New List(Of AML_SCREENING_RESULT_DETAIL)
                    headerbefore = objDB.AML_SCREENING_RESULT.Where(Function(x) x.PK_AML_SCREENING_RESULT_ID = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID).FirstOrDefault
                    objDB.Entry(headerbefore).State = EntityState.Detached
                    For Each item In data.AMLListResult
                        Dim itembefore As New AML_SCREENING_RESULT_DETAIL
                        itembefore = objDB.AML_SCREENING_RESULT_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                        If itembefore IsNot Nothing Then
                            listitembefore.Add(itembefore)
                        End If
                    Next

                    Dim databefore As New AMLJudgementClass
                    With databefore
                        .AMLJudgementHeader = headerbefore
                        .AMLListResult = listitembefore
                    End With
                    Dim dataXMLBefore As String = Common.Serialize(databefore)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = data.AMLJudgementHeader.PK_AML_SCREENING_RESULT_ID
                        .ModuleField = dataXML
                        .ModuleFieldBefore = dataXMLBefore
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                        .CreatedBy = Common.SessionCurrentUser.UserID
                        .CreatedDate = Now
                    End With
                    objDB.Entry(objModuleApproval).State = EntityState.Added
                    objDB.SaveChanges()
                    If data.AMLListResult.Count > 0 Then
                        data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                        data.AMLJudgementHeader.LastUpdateDate = Now
                        objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                        objDB.SaveChanges()
                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                        Dim modulename As String = objModule.ModuleLabel
                        Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, data.AMLJudgementHeader)
                        objDB.SaveChanges()
                        For Each item As AML_SCREENING_RESULT_DETAIL In data.AMLListResult
                            Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objDB.AML_SCREENING_RESULT_DETAIL
                                                                        Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                                                                        Select x).FirstOrDefault
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        Next
                    End If
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveJudgementApprovalTransaction(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim pkmodule As Integer
                    If data.AMLJudgementHeaderTransactionCounterParty IsNot Nothing And data.AMLJudgementHeaderTransaction IsNot Nothing Then
                        For Each item In data.AMLJudgementHeaderTransactionCounterParty
                            item.LastUpdateBy = Common.SessionCurrentUser.UserID
                            item.LastUpdateDate = Now
                            item.STATUS_JUDGEMENT_CODE = "1"
                            objDB.Entry(item).State = EntityState.Modified
                        Next
                        For Each item In data.AMLJudgementHeaderTransaction
                            item.LastUpdateBy = Common.SessionCurrentUser.UserID
                            item.LastUpdateDate = Now
                            item.STATUS_JUDGEMENT_CODE = "1"
                            objDB.Entry(item).State = EntityState.Modified
                        Next

                        objDB.SaveChanges()

                        Dim dataXML2 As String = Common.Serialize(data)
                        Dim headerbeforeList2 As New List(Of AML_SCREENING_RESULT_TRANSACTION)
                        Dim headerbefore2 As New AML_SCREENING_RESULT_TRANSACTION
                        Dim listitembefore2 As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                        For Each item In data.AMLJudgementHeaderTransactionCounterParty
                            headerbefore2 = objDB.AML_SCREENING_RESULT_TRANSACTION.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID).FirstOrDefault
                            objDB.Entry(headerbefore2).State = EntityState.Detached
                            For Each item2 In data.AMLListResultTransaction.Where(Function(x) item.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                Dim itembefore As New AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                itembefore = objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item2.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
                                If itembefore IsNot Nothing Then
                                    listitembefore2.Add(itembefore)
                                End If
                            Next
                            If headerbefore2 IsNot Nothing Then
                                headerbeforeList2.Add(headerbefore2)
                            End If
                        Next
                        Dim headerbeforeList As New List(Of AML_SCREENING_RESULT_TRANSACTION)
                        Dim headerbefore As New AML_SCREENING_RESULT_TRANSACTION
                        Dim listitembefore As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                        Dim hitung As Integer = 0
                        For Each item In data.AMLJudgementHeaderTransaction
                            headerbefore = objDB.AML_SCREENING_RESULT_TRANSACTION.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID).FirstOrDefault
                            objDB.Entry(headerbefore).State = EntityState.Detached
                            For Each item2 In data.AMLListResultTransaction.Where(Function(x) item.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                Dim itembefore As New AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                itembefore = objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item2.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
                                If itembefore IsNot Nothing Then
                                    listitembefore.Add(itembefore)
                                End If
                            Next
                            If hitung = 0 Then
                                pkmodule = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID
                                hitung = hitung + 1
                            End If
                            If headerbefore IsNot Nothing Then
                                headerbeforeList.Add(headerbefore)
                            End If
                        Next
                        hitung = 0

                        Dim databefore2 As New AMLJudgementClass
                        With databefore2
                            .AMLJudgementHeaderTransactionCounterParty = headerbeforeList2
                            .AMLJudgementHeaderTransaction = headerbeforeList
                            .AMLListResultTransaction = listitembefore.Concat(listitembefore2).ToList
                        End With
                        Dim dataXMLBefore2 As String = Common.Serialize(databefore2)

                        Dim objModuleApproval2 As New NawaDevDAL.ModuleApproval
                        With objModuleApproval2
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = pkmodule
                            .ModuleField = dataXML2
                            .ModuleFieldBefore = dataXMLBefore2
                            .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                            .CreatedBy = Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                        End With
                        objDB.Entry(objModuleApproval2).State = EntityState.Added
                        objDB.SaveChanges()
                        If data.AMLListResultTransaction.Count > 0 Then
                            For Each item In data.AMLJudgementHeaderTransactionCounterParty
                                item.LastUpdateBy = Common.SessionCurrentUser.UserID
                                item.LastUpdateDate = Now
                                objDB.Entry(item).State = EntityState.Modified
                            Next
                            For Each item In data.AMLJudgementHeaderTransaction
                                item.LastUpdateBy = Common.SessionCurrentUser.UserID
                                item.LastUpdateDate = Now
                                objDB.Entry(item).State = EntityState.Modified
                            Next
                            objDB.SaveChanges()

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            For Each item In data.AMLJudgementHeaderTransactionCounterParty
                                NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                            Next

                            objDB.SaveChanges()
                            For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In data.AMLListResultTransaction
                                Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = (From x In objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                                                                        Where x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID
                                                                                        Select x).FirstOrDefault
                                With item
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                            Next
                        End If
                    ElseIf data.AMLJudgementHeaderTransactionCounterParty Is Nothing And data.AMLJudgementHeaderTransaction IsNot Nothing Then
                        For Each item In data.AMLJudgementHeaderTransaction
                            item.LastUpdateBy = Common.SessionCurrentUser.UserID
                            item.LastUpdateDate = Now
                            item.STATUS_JUDGEMENT_CODE = "1"
                            objDB.Entry(item).State = EntityState.Modified
                        Next

                        objDB.SaveChanges()

                        Dim dataXML As String = Common.Serialize(data)

                        Dim headerbeforeList As New List(Of AML_SCREENING_RESULT_TRANSACTION)
                        Dim headerbefore As New AML_SCREENING_RESULT_TRANSACTION
                        Dim listitembefore As New List(Of AML_SCREENING_RESULT_TRANSACTION_DETAIL)
                        Dim hitung As Integer = 0
                        For Each item In data.AMLJudgementHeaderTransaction
                            headerbefore = objDB.AML_SCREENING_RESULT_TRANSACTION.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID).FirstOrDefault
                            objDB.Entry(headerbefore).State = EntityState.Detached
                            For Each item2 In data.AMLListResultTransaction.Where(Function(x) item.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                Dim itembefore As New AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                itembefore = objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item2.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
                                If itembefore IsNot Nothing Then
                                    listitembefore.Add(itembefore)
                                End If
                            Next
                            If hitung = 0 Then
                                pkmodule = item.PK_AML_SCREENING_RESULT_TRANSACTION_ID
                                hitung = hitung + 1
                            End If
                            If headerbefore IsNot Nothing Then
                                headerbeforeList.Add(headerbefore)
                            End If
                        Next
                        hitung = 0

                        Dim databefore As New AMLJudgementClass
                        With databefore
                            .AMLJudgementHeaderTransaction = headerbeforeList
                            .AMLListResultTransaction = listitembefore
                        End With
                        Dim dataXMLBefore As String = Common.Serialize(databefore)

                        Dim objModuleApproval As New NawaDevDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = pkmodule
                            .ModuleField = dataXML
                            .ModuleFieldBefore = dataXMLBefore
                            .PK_ModuleAction_ID = Common.ModuleActionEnum.Update
                            .CreatedBy = Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                        End With
                        objDB.Entry(objModuleApproval).State = EntityState.Added
                        objDB.SaveChanges()
                        If data.AMLListResultTransaction.Count > 0 Then
                            For Each item In data.AMLJudgementHeaderTransaction
                                item.LastUpdateBy = Common.SessionCurrentUser.UserID
                                item.LastUpdateDate = Now
                                objDB.Entry(item).State = EntityState.Modified
                            Next

                            objDB.SaveChanges()
                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            For Each item In data.AMLJudgementHeaderTransaction
                                NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                            Next

                            objDB.SaveChanges()
                            For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In data.AMLListResultTransaction
                                Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = (From x In objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                                                                        Where x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID
                                                                                        Select x).FirstOrDefault
                                With item
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                            Next
                        End If
                    End If
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveJudgementTanpaApproval(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities

            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim cif As String = data.AMLJudgementHeader.CIF_NO

                    '2023-08-04, Nael: Ngehilangin If Scope
                    data.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                    data.AMLJudgementHeader.LastUpdateDate = Now
                    objDB.Entry(data.AMLJudgementHeader).State = EntityState.Modified
                    objDB.SaveChanges()
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim modulename As String = objModule.ModuleLabel
                    Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, data.AMLJudgementHeader)
                    objDB.SaveChanges()

                    '2023-08-14, Nael: bikin list-list untuk menumpuk query, biar bisa dieksekusi sekali-gus
                    Dim customerMatchList As New List(Of AML_SCREENING_CUSTOMER_MATCH)
                    Dim customerInMatchList As New List(Of AML_SCREENING_CUSTOMER_MATCH)
                    customerInMatchList = objDB.AML_SCREENING_CUSTOMER_MATCH.Where(Function(x) x.CIF_NO.Equals(cif)).ToList()

                    Dim isCustomerMatch As Boolean = False

                    For Each item As AML_SCREENING_RESULT_DETAIL In data.AMLListResult
                        ' Mengambil record yg akan diedit, dan akan diinputkan ke audit trail sebagai 'old data'
                        Dim obcek As AML_SCREENING_RESULT_DETAIL = CType(item, AML_SCREENING_RESULT_DETAIL) '2023-08-14, Nael: dari pada select lagi ke database, mending clone ajh object sebelum di edit
                        With item
                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                            .LastUpdateDate = DateTime.Now
                        End With
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                        obcek = item
                        objDB.Entry(obcek).State = EntityState.Modified

                        If item.JUDGEMENT_ISMATCH Then
                            '2023-08-02, Nael
                            isCustomerMatch = True

                            Dim addresult As New AML_SCREENING_CUSTOMER_MATCH
                            With addresult
                                .CIF_NO = data.AMLJudgementHeader.CIF_NO
                                .FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID
                                .MATCH_SCORE = item.MATCH_SCORE
                                .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                .JUDGEMENT_BY = item.JUDGEMENT_BY
                                .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                .JUDGEMENT_DATE = item.JUDGEMENT_DATE
                                .MATCH_SCORE_NAME = item.MATCH_SCORE_NAME
                                .MATCH_SCORE_DOB = item.MATCH_SCORE_DOB
                                .MATCH_SCORE_NATIONALITY = item.MATCH_SCORE_NATIONALITY
                                .MATCH_SCORE_IDENTITY_NUMBER = item.MATCH_SCORE_IDENTITY_NUMBER
                                .CreatedBy = Common.SessionCurrentUser.UserID
                                .CreatedDate = Now
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = Now
                                .NAME_WATCHLIST = item.NAME_WATCHLIST
                                .SERVICE_SOURCE = data.AMLJudgementHeader.SERVICE '' Added by Felix 29 Jun 2021
                            End With
                            '2023-08-14, Nael: Ditumpuk dulu dalam satu list, nanti dieksekusi pake AddRange (semua sekaligus)
                            customerMatchList.Add(addresult)
                        End If
                    Next

                    '2023-08-14, Nael: Ekeskusi semua query yg sudah ditumpuk tadi
                    ' Update risk code, jika ada salah satu screening yg match
                    If isCustomerMatch Then
                        Dim queryUpdateCust As String = "UPDATE AML_CUSTOMER SET FK_AML_RISK_CODE = 'H', LastUpdateDate = GETDATE() WHERE CIFNo = '" & data.AMLJudgementHeader.CIF_NO & "'"
                        objDB.Database.ExecuteSqlCommand(queryUpdateCust)
                        objDB.SaveChanges()
                    End If
                    objDB.AML_SCREENING_CUSTOMER_MATCH.RemoveRange(customerInMatchList)
                    objDB.AML_SCREENING_CUSTOMER_MATCH.AddRange(customerMatchList)

                    '2023-08-02, Nael: Update status code
                    '2023-08-07, Nael: Update status dipindahin ke bawah
                    Dim queryStatusCode As String = "UPDATE AML_SCREENING_RESULT " _
                                & " SET STATUS_JUDGEMENT_CODE = '" & data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE & "'" _
                                & " WHERE CIF_NO = '" & data.AMLJudgementHeader.CIF_NO & "'"
                    objDB.Database.ExecuteSqlCommand(queryStatusCode)
                    objDB.SaveChanges()

                    ' 2023-09-25, Nael: bagian save judgement ke status history dipindahkan ke fungsi terpisah
                    SaveJudgementStatusHistory(data, objDB)

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Dim erroMsg As String = ex.Message
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

#Region "2023-09-25, Nael: Fungsi Untuk Save Perubahan Status dari Customer ke AML_Judgement_Customer_Status_History"
    Public Shared Sub SaveJudgementStatusHistory(data As NawaDevBLL.AMLJudgementClass, objDB As NawaDatadevEntities)
        Dim cif As String = data.AMLJudgementHeader.CIF_NO
        Dim userId As String = NawaBLL.Common.SessionCurrentUser.UserID
        Dim statusCode As String = data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE

        ' 2023-09-20, Nael: Insert ke AML_Judgement_Customer_Status_History
        If data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "2" OrElse data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "5" Then
            ' Menyiapkan variable creationDate dan completionDate
            Dim creationDate As String = "'1974-01-01'" ' default value
            Dim completionDate As String = "NULL" ' default value NULL

            ' Jika status dari customer 2 (Closed)
            If data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "2" Then
                ' Select dari database data terbaru order by NO_ID descending
                Dim latestStatusHistoryQuery As String = "SELECT TOP 1 CREATION_DATE FROM AML_Judgement_Customer_Status_History WHERE CIF = '" & data.AMLJudgementHeader.CIF_NO & "' ORDER BY NO_ID DESC"

                Dim latestStatusHistory As String = CStr(NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, latestStatusHistoryQuery, Nothing))

                ' JIka adalah yg terbaru (belum ada di AML_Judgement_Customer_Status_History
                If latestStatusHistory Is Nothing Then
                    Dim selectHistory As AML_SCREENING_RESULT = objDB.AML_SCREENING_RESULT.FirstOrDefault(Function(x) x.CIF_NO.Equals(cif))

                    If selectHistory IsNot Nothing Then
                        Dim creationDateTemp As Date = selectHistory.CreatedDate
                        creationDate = "'" & creationDateTemp.ToString("yyyy-MM-ddTHH:mm:ssZ") & "'"
                    End If
                Else
                    creationDate = "'" & latestStatusHistory & "'"
                End If
                completionDate = "GETDATE()"
            ElseIf data.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = "5" Then
                creationDate = "GETDATE()"
            End If

            ' Update
            Dim queryUpdateHistory As String = "UPDATE AML_Judgement_Customer_Status_History " _
                & "SET IS_LATEST_STATUS = 0" _
                & "WHERE CIF = '" & cif & "'"

            Dim updateLatestStatusParams(1) As SqlParameter
            objDB.Database.ExecuteSqlCommand(queryUpdateHistory, updateLatestStatusParams)
            objDB.SaveChanges()

            Dim queryInsertHistory As String = "INSERT INTO AML_Judgement_Customer_Status_History (" _
            & "CIF, STATUS, CREATION_DATE, COMPLETION_DATE," _
            & "IS_LATEST_STATUS, Active, CreatedBy, LastUpdateBy," _
            & "ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate, AlternateBy) " _
            & "VALUES " _
            & "(" _
            & "'" & cif & "', '" & statusCode & "', " & creationDate & ", " & completionDate _
            & ", 1, 1, '" & userId & "', '" & userId & "', " _
            & "'" & userId & "', GETDATE(), GETDATE(), GETDATE(), NULL" _
            & ")"

            Dim historyParams(1) As SqlParameter

            objDB.Database.ExecuteSqlCommand(queryInsertHistory, historyParams)
            objDB.SaveChanges()
        End If

    End Sub
#End Region

    Public Shared Sub SaveJudgementTanpaApprovalTransaction(data As NawaDevBLL.AMLJudgementClass, objModule As NawaDAL.Module)
        Using objDB As NawaDatadevEntities = New NawaDatadevEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    If data IsNot Nothing Then
                        If data.AMLListResultTransaction.Count > 0 Then
                            HttpContext.Current.Session("SaveTanpaApproval") = True
                            If data.AMLJudgementHeaderTransactionCounterParty IsNot Nothing Then
                                For Each item In data.AMLJudgementHeaderTransactionCounterParty
                                    item.LastUpdateBy = Common.SessionCurrentUser.UserID
                                    item.LastUpdateDate = Now
                                    item.STATUS_JUDGEMENT_CODE = "2"
                                    objDB.Entry(item).State = EntityState.Modified
                                Next
                                objDB.SaveChanges()

                                Dim user2 As String = NawaBLL.Common.SessionCurrentUser.UserID
                                Dim act2 As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                                Dim acts2 As Integer = NawaBLL.Common.ModuleActionEnum.Update
                                Dim modulename2 As String = objModule.ModuleLabel
                                Dim ObjAuditTrailHeader2 As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user2, act2, acts2, modulename2)
                                If ObjAuditTrailHeader2 IsNot Nothing Then
                                    For Each item In data.AMLJudgementHeaderTransaction
                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader2.PK_AuditTrail_ID, item)
                                    Next

                                End If

                                objDB.SaveChanges()
                                For Each item2 In data.AMLJudgementHeaderTransactionCounterParty
                                    For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In data.AMLListResultTransaction.Where(Function(x) item2.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                        Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = (From x In objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                                                                                Where x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID
                                                                                                Select x).FirstOrDefault
                                        With item
                                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                        End With
                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader2.PK_AuditTrail_ID, item, obcek)
                                        objDB.Entry(obcek).CurrentValues.SetValues(item)
                                        objDB.Entry(obcek).State = EntityState.Modified
                                        objDB.SaveChanges()
                                        If item.JUDGEMENT_ISMATCH Then

                                            If item2.FK_FromOrToOrConductor_Code = 1 Then
                                                Dim updatecustomer As AML_CUSTOMER = objDB.AML_CUSTOMER.Where(Function(x) x.CIFNo = item2.CIF_NO).FirstOrDefault
                                                'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                                If updatecustomer IsNot Nothing Then
                                                    With updatecustomer
                                                        .FK_AML_RISK_CODE = "H"
                                                    End With
                                                    objDB.Entry(updatecustomer).State = EntityState.Modified
                                                    objDB.SaveChanges()
                                                Else
                                                    objDB.Entry(updatecustomer).State = EntityState.Detached
                                                End If
                                                Dim checkalreadyinmatch As AML_SCREENING_CUSTOMER_MATCH = objDB.AML_SCREENING_CUSTOMER_MATCH.Where(Function(x) x.CIF_NO = item2.CIF_NO And x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).FirstOrDefault
                                                'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                                If checkalreadyinmatch Is Nothing Then
                                                    Dim addresult As New AML_SCREENING_CUSTOMER_MATCH
                                                    With addresult
                                                        .CIF_NO = item2.CIF_NO
                                                        .FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID
                                                        .MATCH_SCORE = item.MATCH_SCORE
                                                        .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                                        .JUDGEMENT_BY = item.JUDGEMENT_BY
                                                        .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                                        .JUDGEMENT_DATE = item.JUDGEMENT_DATE
                                                        .MATCH_SCORE_NAME = item.MATCH_SCORE_NAME
                                                        .MATCH_SCORE_DOB = item.MATCH_SCORE_DOB
                                                        .MATCH_SCORE_NATIONALITY = item.MATCH_SCORE_NATIONALITY
                                                        .MATCH_SCORE_IDENTITY_NUMBER = item.MATCH_SCORE_IDENTITY_NUMBER
                                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                                        .CreatedDate = Now
                                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = Now
                                                    End With
                                                    objDB.Entry(addresult).State = EntityState.Added
                                                    objDB.SaveChanges()
                                                Else
                                                    objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                                End If
                                            ElseIf item2.FK_FromOrToOrConductor_Code = 2 Then
                                                Dim checkalreadyinmatch As AML_SCREENING_WIC_MATCH = objDB.AML_SCREENING_WIC_MATCH.Where(Function(x) x.WIC_NO = item2.WIC_NO And x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).FirstOrDefault
                                                'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                                If checkalreadyinmatch Is Nothing Then
                                                    Dim addresult As New AML_SCREENING_WIC_MATCH
                                                    With addresult
                                                        .WIC_NO = item2.WIC_NO
                                                        .FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID
                                                        .MATCH_SCORE = item.MATCH_SCORE
                                                        .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                                        .JUDGEMENT_BY = item.JUDGEMENT_BY
                                                        .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                                        .JUDGEMENT_DATE = item.JUDGEMENT_DATE
                                                        .MATCH_SCORE_NAME = item.MATCH_SCORE_NAME
                                                        .MATCH_SCORE_DOB = item.MATCH_SCORE_DOB
                                                        .MATCH_SCORE_NATIONALITY = item.MATCH_SCORE_NATIONALITY
                                                        .MATCH_SCORE_IDENTITY_NUMBER = item.MATCH_SCORE_IDENTITY_NUMBER
                                                        .CreatedBy = Common.SessionCurrentUser.UserID
                                                        .CreatedDate = Now
                                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = Now
                                                    End With
                                                    objDB.Entry(addresult).State = EntityState.Added
                                                    objDB.SaveChanges()
                                                Else
                                                    objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                                End If
                                            End If
                                        End If
                                    Next
                                Next

                            End If
                            For Each item In data.AMLJudgementHeaderTransaction
                                item.LastUpdateBy = Common.SessionCurrentUser.UserID
                                item.LastUpdateDate = Now
                                item.STATUS_JUDGEMENT_CODE = "2"
                                objDB.Entry(item).State = EntityState.Modified

                            Next
                            objDB.SaveChanges()

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim ObjAuditTrailHeader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            If ObjAuditTrailHeader IsNot Nothing Then
                                For Each item In data.AMLJudgementHeaderTransaction
                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item)
                                Next

                            End If

                            objDB.SaveChanges()
                            For Each item2 In data.AMLJudgementHeaderTransaction
                                For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In data.AMLListResultTransaction.Where(Function(x) item2.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                    Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = (From x In objDB.AML_SCREENING_RESULT_TRANSACTION_DETAIL
                                                                                            Where x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID
                                                                                            Select x).FirstOrDefault
                                    With item
                                        .LastUpdateBy = Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                    End With
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, ObjAuditTrailHeader.PK_AuditTrail_ID, item, obcek)
                                    objDB.Entry(obcek).CurrentValues.SetValues(item)
                                    objDB.Entry(obcek).State = EntityState.Modified
                                    objDB.SaveChanges()
                                    If item.JUDGEMENT_ISMATCH Then

                                        If item2.FK_FromOrToOrConductor_Code = 1 Then
                                            Dim updatecustomer As AML_CUSTOMER = objDB.AML_CUSTOMER.Where(Function(x) x.CIFNo = item2.CIF_NO).FirstOrDefault
                                            'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                            If updatecustomer IsNot Nothing Then
                                                With updatecustomer
                                                    .FK_AML_RISK_CODE = "H"
                                                End With
                                                objDB.Entry(updatecustomer).State = EntityState.Modified
                                                objDB.SaveChanges()
                                            Else
                                                objDB.Entry(updatecustomer).State = EntityState.Detached
                                            End If
                                            Dim checkalreadyinmatch As AML_SCREENING_CUSTOMER_MATCH = objDB.AML_SCREENING_CUSTOMER_MATCH.Where(Function(x) x.CIF_NO = item2.CIF_NO And x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).FirstOrDefault
                                            'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                            If checkalreadyinmatch Is Nothing Then
                                                Dim addresult As New AML_SCREENING_CUSTOMER_MATCH
                                                With addresult
                                                    .CIF_NO = item2.CIF_NO
                                                    .FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID
                                                    .MATCH_SCORE = item.MATCH_SCORE
                                                    .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                                    .JUDGEMENT_BY = item.JUDGEMENT_BY
                                                    .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                                    .JUDGEMENT_DATE = item.JUDGEMENT_DATE
                                                    .MATCH_SCORE_NAME = item.MATCH_SCORE_NAME
                                                    .MATCH_SCORE_DOB = item.MATCH_SCORE_DOB
                                                    .MATCH_SCORE_NATIONALITY = item.MATCH_SCORE_NATIONALITY
                                                    .MATCH_SCORE_IDENTITY_NUMBER = item.MATCH_SCORE_IDENTITY_NUMBER
                                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                                    .CreatedDate = Now
                                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = Now
                                                End With
                                                objDB.Entry(addresult).State = EntityState.Added
                                                objDB.SaveChanges()
                                            Else
                                                objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                            End If
                                        ElseIf item2.FK_FromOrToOrConductor_Code = 2 Then
                                            Dim checkalreadyinmatch As AML_SCREENING_WIC_MATCH = objDB.AML_SCREENING_WIC_MATCH.Where(Function(x) x.WIC_NO = item2.WIC_NO And x.FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID).FirstOrDefault
                                            'objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                            If checkalreadyinmatch Is Nothing Then
                                                Dim addresult As New AML_SCREENING_WIC_MATCH
                                                With addresult
                                                    .WIC_NO = item2.WIC_NO
                                                    .FK_AML_WATCHLIST_ID = item.FK_AML_WATCHLIST_ID
                                                    .MATCH_SCORE = item.MATCH_SCORE
                                                    .JUDGEMENT_ISMATCH = item.JUDGEMENT_ISMATCH
                                                    .JUDGEMENT_BY = item.JUDGEMENT_BY
                                                    .JUDGEMENT_COMMENT = item.JUDGEMENT_COMMENT
                                                    .JUDGEMENT_DATE = item.JUDGEMENT_DATE
                                                    .MATCH_SCORE_NAME = item.MATCH_SCORE_NAME
                                                    .MATCH_SCORE_DOB = item.MATCH_SCORE_DOB
                                                    .MATCH_SCORE_NATIONALITY = item.MATCH_SCORE_NATIONALITY
                                                    .MATCH_SCORE_IDENTITY_NUMBER = item.MATCH_SCORE_IDENTITY_NUMBER
                                                    .CreatedBy = Common.SessionCurrentUser.UserID
                                                    .CreatedDate = Now
                                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = Now
                                                End With
                                                objDB.Entry(addresult).State = EntityState.Added
                                                objDB.SaveChanges()
                                            Else
                                                objDB.Entry(checkalreadyinmatch).State = EntityState.Detached
                                            End If
                                        End If
                                    End If
                                Next
                            Next
                        End If
                    End If
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Accept(ID As Long)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If
                    Dim objData As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLJudgementClass))
                    ''Save data dan Audit Trail sama dengan Without Approval
                    SaveJudgementTanpaApproval(objData, objModule)

                    ''Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Reject(ID As Long)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module = Nothing
                    If objApproval IsNot Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Dim objData As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLJudgementClass))
                    Dim objData_Old As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AMLJudgementClass))

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    With objAuditTrailheader
                        .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    End With
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.AMLJudgementHeader, objData_Old.AMLJudgementHeader)
                    objdb.SaveChanges()

                    objData.AMLJudgementHeader.LastUpdateBy = Common.SessionCurrentUser.UserID
                    objData.AMLJudgementHeader.LastUpdateDate = Now

                    ''=====================================================================
                    '2023-08-15, Nael: Jika request re-open direject maka akan balik lagi ke closed
                    Dim tmpStatusCode = objData.AMLJudgementHeader.STATUS_JUDGEMENT_CODE
                    objData.AMLJudgementHeader.STATUS_JUDGEMENT_CODE = If(tmpStatusCode = "5", "2", "3")
                    ''=====================================================================

                    objdb.Entry(objData.AMLJudgementHeader).State = EntityState.Modified

                    For Each item As AML_SCREENING_RESULT_DETAIL In objData.AMLListResult
                        Dim obcek As AML_SCREENING_RESULT_DETAIL = objData_Old.AMLListResult.Where(Function(x) x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID).FirstOrDefault
                        'Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objData_Old.AMLListResult
                        '                                            Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                        '                                            Select x).FirstOrDefault
                        With item
                            .LastUpdateBy = Common.SessionCurrentUser.UserID
                            .LastUpdateDate = DateTime.Now
                        End With
                        NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, obcek)
                    Next

                    ''2023-08-15, Nael: UPDATE STATUS CODE
                    Dim strUpdate As String = "UPDATE AML_SCREENING_RESULT " _
                        & "SET [STATUS_JUDGEMENT_CODE] = '" & objData.AMLJudgementHeader.STATUS_JUDGEMENT_CODE & "'," _
                        & "[LastUpdateDate] = GETDATE() " _
                        & "WHERE [CIF_NO] = '" & objData.AMLJudgementHeader.CIF_NO & "'"
                    objdb.Database.ExecuteSqlCommand(strUpdate)
                    objdb.SaveChanges()

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub AcceptTransaction(ID As Long)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If
                    Dim objData As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLJudgementClass))
                    If HttpContext.Current.Session("SaveTanpaApproval") IsNot Nothing Then
                        SaveJudgementTanpaApprovalTransaction(objData, objModule)
                        HttpContext.Current.Session("SaveTanpaApproval") = False
                    End If
                    'Save data dan Audit Trail sama dengan Without Approval

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub RejectTransaction(ID As Long)
        Using objdb As New NawaDevDAL.NawaDatadevEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module = Nothing
                    If objApproval IsNot Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Dim objData As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(AMLJudgementClass))
                    Dim objData_Old As AMLJudgementClass = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(AMLJudgementClass))

                    'Audit Trail Header
                    Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    With objAuditTrailheader
                        .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    End With
                    If objData.AMLJudgementHeaderTransactionCounterParty IsNot Nothing Then
                        NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.AMLJudgementHeaderTransactionCounterParty, objData_Old.AMLJudgementHeaderTransactionCounterParty)
                        objdb.SaveChanges()
                        For Each item In objData.AMLJudgementHeaderTransactionCounterParty
                            item.LastUpdateBy = Common.SessionCurrentUser.UserID
                            item.LastUpdateDate = Now
                            item.STATUS_JUDGEMENT_CODE = "3"
                            objdb.Entry(item).State = EntityState.Modified
                        Next

                        objdb.SaveChanges()
                        For Each item2 In objData.AMLJudgementHeaderTransactionCounterParty
                            For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In objData.AMLListResultTransaction.Where(Function(x) item2.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                                Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = objData_Old.AMLListResultTransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
                                'Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objData_Old.AMLListResult
                                '                                            Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                                '                                            Select x).FirstOrDefault
                                With item
                                    .LastUpdateBy = Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                End With
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, obcek)
                                objdb.SaveChanges()
                            Next


                        Next
                        'Delete Module Approval
                        objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                        objdb.SaveChanges()
                    End If
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.AMLJudgementHeaderTransaction, objData_Old.AMLJudgementHeaderTransaction)
                    objdb.SaveChanges()
                    For Each item In objData.AMLJudgementHeaderTransaction
                        item.LastUpdateBy = Common.SessionCurrentUser.UserID
                        item.LastUpdateDate = Now
                        item.STATUS_JUDGEMENT_CODE = "3"
                        objdb.Entry(item).State = EntityState.Modified
                    Next

                    objdb.SaveChanges()
                    For Each item2 In objData.AMLJudgementHeaderTransaction
                        For Each item As AML_SCREENING_RESULT_TRANSACTION_DETAIL In objData.AMLListResultTransaction.Where(Function(x) item2.PK_AML_SCREENING_RESULT_TRANSACTION_ID = x.FK_AML_SCREENING_RESULT_TRANSACTION_ID).ToList
                            Dim obcek As AML_SCREENING_RESULT_TRANSACTION_DETAIL = objData_Old.AMLListResultTransaction.Where(Function(x) x.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID = item.PK_AML_SCREENING_RESULT_TRANSACTION_DETAIL_ID).FirstOrDefault
                            'Dim obcek As AML_SCREENING_RESULT_DETAIL = (From x In objData_Old.AMLListResult
                            '                                            Where x.PK_AML_SCREENING_RESULT_DETAIL_ID = item.PK_AML_SCREENING_RESULT_DETAIL_ID
                            '                                            Select x).FirstOrDefault
                            With item
                                .LastUpdateBy = Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, obcek)
                            objdb.SaveChanges()
                        Next


                    Next
                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
