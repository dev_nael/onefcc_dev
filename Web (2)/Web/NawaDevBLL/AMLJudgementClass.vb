﻿Public Class AMLJudgementClass
    Public AMLJudgementHeader As New NawaDevDAL.AML_SCREENING_RESULT
    Public AMLJudgementHeaderTransaction As New List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
    Public AMLJudgementHeaderTransactionCounterParty As New List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION)
    Public AMLListResult As New List(Of NawaDevDAL.AML_SCREENING_RESULT_DETAIL)
    Public AMLListResultTransaction As New List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
    Public AMLListResultTransactionCounterParty As New List(Of NawaDevDAL.AML_SCREENING_RESULT_TRANSACTION_DETAIL)
End Class
