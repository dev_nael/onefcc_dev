﻿Imports System.Data.Entity
Imports System.Reflection
Imports NawaBLL
Imports NawaDevDAL

Public Class TransactionPersonBLL
    Public Shared Sub InsertTransactionPersonFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                    ItemObjTransactionPersonFrom = Transactions.objPersonFrom

                    With ItemObjTransactionPersonFrom
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(ItemObjTransactionPersonFrom).State = EntityState.Added

                    'Dim objtypeReport As Type = ItemObjTransactionPersonFrom.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(ItemObjTransactionPersonFrom, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPersonFrom, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhonePersonFrom.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhonePersonFrom
                            With PersonPhone
                                .FK_Trn_Person = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressPersonFrom.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressPersonFrom
                            With PersonAddress
                                .FK_Trn_Person = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerPersonFrom.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressEmployerPersonFrom
                            With PersonAddress
                                .FK_Trn_Person = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerPersonFrom.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhoneEmployerPersonFrom
                            With PersonPhone
                                .FK_Trn_Person = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjIdentificationPersonFrom.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationPersonFrom
                            With PersonIdentification
                                .FK_Person_ID = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonIdentification).State = EntityState.Added

                            'objtypeReport = PersonIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    objdb.SaveChanges()
                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub InsertTransactionPersonTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                    ItemObjTransactionPersonTo = Transactions.objPersonTo

                    With ItemObjTransactionPersonTo
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(ItemObjTransactionPersonTo).State = EntityState.Added

                    'Dim objtypeReport As Type = ItemObjTransactionPersonTo.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(ItemObjTransactionPersonTo, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPersonTo, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhonePersonTo.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhonePersonTo
                            With PersonPhone
                                .FK_Trn_Person = Transactions.objPersonTo.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressPersonTo.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressPersonTo
                            With PersonAddress
                                .FK_Trn_Person = Transactions.objPersonTo.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerPersonTo.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressEmployerPersonTo
                            With PersonAddress
                                .FK_Trn_Person = Transactions.objPersonTo.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerPersonTo.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhoneEmployerPersonTo
                            With PersonPhone
                                .FK_Trn_Person = Transactions.objPersonFrom.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjIdentificationPersonTo.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationPersonTo
                            With PersonIdentification
                                .FK_Person_ID = Transactions.objPersonTo.PK_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonIdentification).State = EntityState.Added

                            'objtypeReport = PersonIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    objdb.SaveChanges()
                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPersonFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                    ItemObjTransactionPersonFrom = Transactions.objPersonFrom

                    objdb.Entry(ItemObjTransactionPersonFrom).State = EntityState.Deleted

                    'Dim objtypeReport As Type = ItemObjTransactionPersonFrom.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(ItemObjTransactionPersonFrom, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPersonFrom, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhonePersonFrom.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhonePersonFrom
                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressPersonFrom.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressPersonFrom
                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerPersonFrom.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressEmployerPersonFrom
                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerPersonFrom.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhoneEmployerPersonFrom
                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjIdentificationPersonFrom.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationPersonFrom
                            objdb.Entry(PersonIdentification).State = EntityState.Deleted

                            'objtypeReport = PersonIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    objdb.SaveChanges()
                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPersonTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                    ItemObjTransactionPersonTo = Transactions.objPersonTo

                    objdb.Entry(ItemObjTransactionPersonTo).State = EntityState.Deleted

                    'Dim objtypeReport As Type = ItemObjTransactionPersonTo.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(ItemObjTransactionPersonTo, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPersonTo, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhonePersonTo.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhonePersonTo
                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressPersonTo.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressPersonTo
                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerPersonTo.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Person_Address In Transactions.listObjAddressEmployerPersonTo
                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerPersonTo.Count > 0 Then
                        For Each PersonPhone As goAML_trn_Person_Phone In Transactions.listObjPhoneEmployerPersonTo
                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjIdentificationPersonTo.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationPersonTo
                            objdb.Entry(PersonIdentification).State = EntityState.Deleted

                            'objtypeReport = PersonIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If
                    objdb.SaveChanges()


                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
