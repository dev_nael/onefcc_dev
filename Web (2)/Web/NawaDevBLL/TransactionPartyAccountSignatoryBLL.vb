﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionPartyAccountSignatoryBLL
    Public Shared Sub InsertTransactionPartyAccountSignatory(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In Transactions.listSignatoryAccountParty
                        Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                        itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty

                        With itemObjSignatoryParty
                            .FK_Trn_Party_Account_ID = Transactions.objAccountParty.PK_Trn_Party_Account_ID
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With

                        objdb.Entry(itemObjSignatoryParty).State = EntityState.Added

                        Dim objtypeReport As Type = itemObjSignatoryParty.GetType
                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        For Each item As PropertyInfo In propertiesReport
                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            {
                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                .FieldName = item.Name,
                                .OldValue = "",
                                .NewValue = If(Not item.GetValue(itemObjSignatoryParty, Nothing) Is Nothing, item.GetValue(itemObjSignatoryParty, Nothing), "")
                            }
                            objdb.Entry(objATDetail).State = EntityState.Added
                        Next
                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountParty.listPhone.Count > 0 Then
                            For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                With SignatoryPhone
                                    .FK_Trn_Par_Acc_Sign = objModuledataSignatoryAccountParty.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryPhone).State = EntityState.Added

                                objtypeReport = SignatoryPhone.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(SignatoryPhone, Nothing) Is Nothing, item.GetValue(SignatoryPhone, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listAddress.Count > 0 Then
                            For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                With SignatoryAddress
                                    .FK_Trn_par_acc_Signatory_ID = objModuledataSignatoryAccountParty.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAddress).State = EntityState.Added

                                objtypeReport = SignatoryAddress.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(SignatoryAddress, Nothing) Is Nothing, item.GetValue(SignatoryAddress, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listAddressEmployer.Count > 0 Then
                            For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                With SignatoryAddressEmployer
                                    .FK_Trn_par_acc_Signatory_ID = objModuledataSignatoryAccountParty.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryAddressEmployer).State = EntityState.Added

                                objtypeReport = SignatoryAddressEmployer.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(SignatoryAddressEmployer, Nothing) Is Nothing, item.GetValue(SignatoryAddressEmployer, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listPhoneEmployer.Count > 0 Then
                            For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                With SignatoryPhoneEmployer
                                    .FK_Trn_Par_Acc_Sign = objModuledataSignatoryAccountParty.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryPhoneEmployer).State = EntityState.Added

                                objtypeReport = SignatoryPhoneEmployer.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(SignatoryPhoneEmployer, Nothing) Is Nothing, item.GetValue(SignatoryPhoneEmployer, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listIdentification.Count > 0 Then
                            For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                With SignatoryIdentification
                                    .FK_Person_ID = objModuledataSignatoryAccountParty.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(SignatoryIdentification).State = EntityState.Added

                                objtypeReport = SignatoryIdentification.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(SignatoryIdentification, Nothing) Is Nothing, item.GetValue(SignatoryIdentification, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If
                        objdb.SaveChanges()
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyAccountSignatory(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In Transactions.listSignatoryAccountParty
                        Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                        itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty

                        objdb.Entry(itemObjSignatoryParty).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataSignatoryAccountParty.listPhone.Count > 0 Then
                            For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                objdb.Entry(SignatoryPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listAddress.Count > 0 Then
                            For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                objdb.Entry(SignatoryAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listAddressEmployer.Count > 0 Then
                            For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                objdb.Entry(SignatoryAddressEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listPhoneEmployer.Count > 0 Then
                            For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                objdb.Entry(SignatoryPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataSignatoryAccountParty.listIdentification.Count > 0 Then
                            For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                objdb.Entry(SignatoryIdentification).State = EntityState.Deleted
                            Next
                        End If
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyAccountSignatory(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each Signatoryx As goAML_Trn_par_acc_Signatory In (From x In objdb.goAML_Trn_par_acc_Signatory Where x.FK_Trn_Party_Account_ID = Transactions.objAccountParty.PK_Trn_Party_Account_ID).ToList
                        Dim objSignatoryClassCek As SignatoryAccountPartyClass = Transactions.listSignatoryAccountParty.Find(Function(x) x.objSignatoryAccountParty.PK_Trn_par_acc_Signatory_ID = Signatoryx.PK_Trn_par_acc_Signatory_ID)
                        Dim objSignatoryCek As goAML_Trn_par_acc_Signatory = objSignatoryClassCek.objSignatoryAccountParty

                        If objSignatoryCek Is Nothing Then

                            DeleteTransactionPartyAccountSignatory(Transactions, Audit)

                        Else

                            Dim TransactionPartyAccountSignatory As goAML_Trn_par_acc_Signatory = objSignatoryClassCek.objSignatoryAccountParty
                            Dim TransactionPartyAccountSignatoryCek As goAML_Trn_par_acc_Signatory = (From x In objdb.goAML_Trn_par_acc_Signatory Where x.PK_Trn_par_acc_Signatory_ID = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID Select x).FirstOrDefault

                            If TransactionPartyAccountSignatoryCek Is Nothing Then

                                InsertTransactionPartyAccountSignatory(Transactions, Audit)

                            Else

                                objdb.Entry(TransactionPartyAccountSignatoryCek).CurrentValues.SetValues(TransactionPartyAccountSignatory)
                                objdb.Entry(TransactionPartyAccountSignatoryCek).State = EntityState.Modified

                                'Signatory Account Phone Party
                                If objSignatoryClassCek.listPhone.Count > 0 Then

                                    'Delete
                                    For Each itemSignatoryPhonex As goAML_trn_par_acc_sign_Phone In (From x In objdb.goAML_trn_par_acc_sign_Phone Where x.FK_Trn_Par_Acc_Sign = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID And x.isEmployer = False)
                                        Dim objCekSignatoryPhone As goAML_trn_par_acc_sign_Phone = objSignatoryClassCek.listPhone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = itemSignatoryPhonex.PK_trn_Par_Acc_Sign_Phone And x.isEmployer = False)
                                        If objCekSignatoryPhone Is Nothing Then
                                            objdb.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemSignatoryPhone As goAML_trn_par_acc_sign_Phone In objSignatoryClassCek.listPhone
                                        Dim objCek As goAML_Trn_Par_Acc_Ent_Director_Phone = (From x In objdb.goAML_Trn_Par_Acc_Ent_Director_Phone Where x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = itemSignatoryPhone.PK_trn_Par_Acc_Sign_Phone And x.isemployer = False Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemSignatoryPhone).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_trn_par_acc_sign_Phone In objSignatoryClassCek.listPhone
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Signatory Account Address Party
                                If objSignatoryClassCek.listAddress.Count > 0 Then

                                    'Delete
                                    For Each itemSignatoryAddressx As goAML_Trn_par_Acc_sign_Address In (From x In objdb.goAML_Trn_par_Acc_sign_Address Where x.FK_Trn_par_acc_Signatory_ID = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID And x.isEmployer = False)
                                        Dim objCekSignatoryPhone As goAML_Trn_par_Acc_sign_Address = objSignatoryClassCek.listAddress.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = itemSignatoryAddressx.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = False)
                                        If objCekSignatoryPhone Is Nothing Then
                                            objdb.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemSignatoryAddress As goAML_Trn_par_Acc_sign_Address In objSignatoryClassCek.listAddress
                                        Dim objCek As goAML_Trn_par_Acc_sign_Address = (From x In objdb.goAML_Trn_par_Acc_sign_Address Where x.PK_Trn_Par_Acc_sign_Address_ID = itemSignatoryAddress.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = False Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemSignatoryAddress).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_trn_par_acc_sign_Phone In objSignatoryClassCek.listPhone
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Signatory Account Address Employer Party
                                If objSignatoryClassCek.listAddressEmployer.Count > 0 Then

                                    'Delete
                                    For Each itemSignatoryAddressx As goAML_Trn_par_Acc_sign_Address In (From x In objdb.goAML_Trn_par_Acc_sign_Address Where x.FK_Trn_par_acc_Signatory_ID = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID And x.isEmployer = True)
                                        Dim objCekSignatoryPhone As goAML_Trn_par_Acc_sign_Address = objSignatoryClassCek.listAddress.Find(Function(x) x.PK_Trn_Par_Acc_sign_Address_ID = itemSignatoryAddressx.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = True)
                                        If objCekSignatoryPhone Is Nothing Then
                                            objdb.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemSignatoryAddress As goAML_Trn_par_Acc_sign_Address In objSignatoryClassCek.listAddressEmployer
                                        Dim objCek As goAML_Trn_par_Acc_sign_Address = (From x In objdb.goAML_Trn_par_Acc_sign_Address Where x.PK_Trn_Par_Acc_sign_Address_ID = itemSignatoryAddress.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = True Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemSignatoryAddress).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Trn_par_Acc_sign_Address In objSignatoryClassCek.listAddressEmployer
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Signatory Account Phone Employer Party
                                If objSignatoryClassCek.listPhoneEmployer.Count > 0 Then

                                    'Delete
                                    For Each itemSignatoryPhonex As goAML_trn_par_acc_sign_Phone In (From x In objdb.goAML_trn_par_acc_sign_Phone Where x.FK_Trn_Par_Acc_Sign = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID And x.isEmployer = True)
                                        Dim objCekSignatoryPhone As goAML_trn_par_acc_sign_Phone = objSignatoryClassCek.listPhone.Find(Function(x) x.PK_trn_Par_Acc_Sign_Phone = itemSignatoryPhonex.PK_trn_Par_Acc_Sign_Phone And x.isEmployer = True)
                                        If objCekSignatoryPhone Is Nothing Then
                                            objdb.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemSignatoryPhone As goAML_trn_par_acc_sign_Phone In objSignatoryClassCek.listPhone
                                        Dim objCek As goAML_Trn_Par_Acc_Ent_Director_Phone = (From x In objdb.goAML_Trn_Par_Acc_Ent_Director_Phone Where x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = itemSignatoryPhone.PK_trn_Par_Acc_Sign_Phone And x.isemployer = True Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemSignatoryPhone).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_trn_par_acc_sign_Phone In objSignatoryClassCek.listPhoneEmployer
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Signatory Account Identification Party
                                If objSignatoryClassCek.listIdentification.Count > 0 Then

                                    'Delete
                                    For Each itemSignatoryIdentificationx As goAML_Transaction_Party_Identification In (From x In objdb.goAML_Transaction_Party_Identification Where x.FK_Person_ID = TransactionPartyAccountSignatory.PK_Trn_par_acc_Signatory_ID And x.FK_Person_Type = True)
                                        Dim objCekSignatoryIdentification As goAML_Transaction_Party_Identification = objSignatoryClassCek.listIdentification.Find(Function(x) x.PK_Transaction_Party_Identification_ID = itemSignatoryIdentificationx.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 2)
                                        If objCekSignatoryIdentification Is Nothing Then
                                            objdb.Entry(itemSignatoryIdentificationx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemSignatoryIdentification As goAML_Transaction_Party_Identification In objSignatoryClassCek.listIdentification
                                        Dim objCek As goAML_Transaction_Party_Identification = (From x In objdb.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = itemSignatoryIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 2 Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemSignatoryIdentification).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemSignatoryIdentification)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Transaction_Party_Identification In objSignatoryClassCek.listIdentification
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                            End If

                        End If
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
