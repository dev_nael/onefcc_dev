﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionPartyPersonBLL
    Public Shared Sub InsertTransactionPartyPerson(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                    ItemObjTransactionPartyPerson = Transactions.objPersonParty

                    With ItemObjTransactionPartyPerson
                        .FK_Transaction_Party_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(ItemObjTransactionPartyPerson).State = EntityState.Added

                    Dim objtypeReport As Type = ItemObjTransactionPartyPerson.GetType
                    Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    For Each item As PropertyInfo In propertiesReport
                        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        {
                            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            .FieldName = item.Name,
                            .OldValue = "",
                            .NewValue = If(Not item.GetValue(ItemObjTransactionPartyPerson, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPartyPerson, Nothing), "")
                        }
                        objdb.Entry(objATDetail).State = EntityState.Added
                    Next
                    objdb.SaveChanges()

                    If Transactions.listPhonePersonParty.Count > 0 Then
                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhonePersonParty
                            With PersonPhone
                                .FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            objtypeReport = PersonPhone.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    If Transactions.listAddressPersonParty.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressPersonParty
                            With PersonAddress
                                .FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            objtypeReport = PersonAddress.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    If Transactions.listAddressEmployerPersonParty.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressEmployerPersonParty
                            With PersonAddress
                                .FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonAddress).State = EntityState.Added

                            objtypeReport = PersonAddress.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    If Transactions.listPhoneEmployerPersonParty.Count > 0 Then
                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhoneEmployerPersonParty
                            With PersonPhone
                                .FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonPhone).State = EntityState.Added

                            objtypeReport = PersonPhone.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    If Transactions.listIdentificationPersonParty.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Party_Identification In Transactions.listIdentificationPersonParty
                            With PersonIdentification
                                .FK_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(PersonIdentification).State = EntityState.Added

                            objtypeReport = PersonIdentification.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyPerson(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim TransactionPartyPerson As goAML_Trn_Party_Person = Transactions.objPersonParty
                    Dim TransactionPartyPersonCek As goAML_Trn_Party_Person = (From x In objdb.goAML_Trn_Party_Person Where x.PK_Trn_Party_Person_ID = TransactionPartyPerson.PK_Trn_Party_Person_ID Select x).FirstOrDefault
                    If TransactionPartyPersonCek Is Nothing Then

                        InsertTransactionPartyPerson(Transactions, Audit)

                    Else

                        objdb.Entry(TransactionPartyPersonCek).CurrentValues.SetValues(TransactionPartyPerson)
                        objdb.Entry(TransactionPartyPersonCek).State = EntityState.Modified

                        Dim objtypeReport As Type = TransactionPartyPerson.GetType
                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        For Each item As PropertyInfo In propertiesReport
                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            {
                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                .FieldName = item.Name,
                                .OldValue = "",
                                .NewValue = If(Not item.GetValue(TransactionPartyPerson, Nothing) Is Nothing, item.GetValue(TransactionPartyPerson, Nothing), "")
                            }
                            objdb.Entry(objATDetail).State = EntityState.Added
                        Next
                        objdb.SaveChanges()

                        If Transactions.listPhonePersonParty.Count > 0 Then

                            'Delete
                            For Each itemPersonPhonex As goAML_Trn_Party_Person_Phone In (From x In objdb.goAML_Trn_Party_Person_Phone Where x.FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID And x.isemployer = False).ToList
                                Dim objCekPersonPhone As goAML_Trn_Party_Person_Phone = Transactions.listPhonePersonParty.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = itemPersonPhonex.PK_Trn_Party_Person_Phone_ID And x.isemployer = False)
                                If objCekPersonPhone Is Nothing Then
                                    objdb.Entry(itemPersonPhonex).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemPersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhonePersonParty
                                Dim objCek As goAML_Trn_Party_Person_Phone = (From x In objdb.goAML_Trn_Party_Person_Phone Where x.PK_Trn_Party_Person_Phone_ID = itemPersonPhone.PK_Trn_Party_Person_Phone_ID And x.isemployer = False Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemPersonPhone).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Person_Phone In Transactions.listPhonePersonParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listAddressPersonParty.Count > 0 Then

                            'Delete
                            For Each itemPersonAddressx As goAML_Trn_Party_Person_Address In (From x In objdb.goAML_Trn_Party_Person_Address Where x.FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID And x.isemployer = False).ToList
                                Dim objCekPersonAddress As goAML_Trn_Party_Person_Address = Transactions.listAddressPersonParty.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = itemPersonAddressx.PK_Trn_Party_Person_Address_ID And x.isemployer = False)
                                If objCekPersonAddress Is Nothing Then
                                    objdb.Entry(itemPersonAddressx).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemPersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressPersonParty
                                Dim objCek As goAML_Trn_Party_Person_Address = (From x In objdb.goAML_Trn_Party_Person_Address Where x.PK_Trn_Party_Person_Address_ID = itemPersonAddress.PK_Trn_Party_Person_Address_ID And x.isemployer = False Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemPersonAddress).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Person_Address In Transactions.listAddressPersonParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listAddressEmployerPersonParty.Count > 0 Then

                            'Delete
                            For Each itemPersonAddressx As goAML_Trn_Party_Person_Address In (From x In objdb.goAML_Trn_Party_Person_Address Where x.FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID And x.isemployer = True).ToList
                                Dim objCekPersonAddress As goAML_Trn_Party_Person_Address = Transactions.listAddressEmployerPersonParty.Find(Function(x) x.PK_Trn_Party_Person_Address_ID = itemPersonAddressx.PK_Trn_Party_Person_Address_ID And x.isemployer = True)
                                If objCekPersonAddress Is Nothing Then
                                    objdb.Entry(itemPersonAddressx).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemPersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressEmployerPersonParty
                                Dim objCek As goAML_Trn_Party_Person_Address = (From x In objdb.goAML_Trn_Party_Person_Address Where x.PK_Trn_Party_Person_Address_ID = itemPersonAddress.PK_Trn_Party_Person_Address_ID And x.isemployer = True Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemPersonAddress).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Person_Address In Transactions.listAddressEmployerPersonParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listPhoneEmployerPersonParty.Count > 0 Then

                            'Delete
                            For Each itemPersonPhonex As goAML_Trn_Party_Person_Phone In (From x In objdb.goAML_Trn_Party_Person_Phone Where x.FK_Trn_Party_Person_ID = Transactions.objPersonParty.PK_Trn_Party_Person_ID And x.isemployer = True).ToList
                                Dim objCekPersonPhone As goAML_Trn_Party_Person_Phone = Transactions.listPhoneEmployerPersonParty.Find(Function(x) x.PK_Trn_Party_Person_Phone_ID = itemPersonPhonex.PK_Trn_Party_Person_Phone_ID And x.isemployer = True)
                                If objCekPersonPhone Is Nothing Then
                                    objdb.Entry(itemPersonPhonex).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemPersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhoneEmployerPersonParty
                                Dim objCek As goAML_Trn_Party_Person_Phone = (From x In objdb.goAML_Trn_Party_Person_Phone Where x.PK_Trn_Party_Person_Phone_ID = itemPersonPhone.PK_Trn_Party_Person_Phone_ID And x.isemployer = True Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemPersonPhone).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Person_Phone In Transactions.listPhoneEmployerPersonParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listIdentificationPersonParty.Count > 0 Then

                            'Delete
                            For Each itemPersonIdentificationx As goAML_Transaction_Party_Identification In (From x In objdb.goAML_Transaction_Party_Identification Where x.FK_Person_Type = Transactions.objPersonParty.PK_Trn_Party_Person_ID And x.FK_Person_Type = 1).ToList
                                Dim objCekPersonIdentification As goAML_Transaction_Party_Identification = Transactions.listIdentificationPersonParty.Find(Function(x) x.PK_Transaction_Party_Identification_ID = itemPersonIdentificationx.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 1)
                                If objCekPersonIdentification Is Nothing Then
                                    objdb.Entry(itemPersonIdentificationx).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemPersonIdentification As goAML_Transaction_Party_Identification In Transactions.listIdentificationPersonParty
                                Dim objCek As goAML_Transaction_Party_Identification = (From x In objdb.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = itemPersonIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemPersonIdentification).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemPersonIdentification)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Person_Phone In Transactions.listPhoneEmployerPersonParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyPerson(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                    ItemObjTransactionPartyPerson = Transactions.objPersonParty

                    objdb.Entry(ItemObjTransactionPartyPerson).State = EntityState.Deleted

                    'Dim objtypeReport As Type = ItemObjTransactionPartyPerson.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(ItemObjTransactionPartyPerson, Nothing) Is Nothing, item.GetValue(ItemObjTransactionPartyPerson, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listPhonePersonParty.Count > 0 Then
                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhonePersonParty

                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listAddressPersonParty.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressPersonParty

                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listAddressEmployerPersonParty.Count > 0 Then
                        For Each PersonAddress As goAML_Trn_Party_Person_Address In Transactions.listAddressEmployerPersonParty

                            objdb.Entry(PersonAddress).State = EntityState.Deleted

                            'objtypeReport = PersonAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonAddress, Nothing) Is Nothing, item.GetValue(PersonAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listPhoneEmployerPersonParty.Count > 0 Then
                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In Transactions.listPhoneEmployerPersonParty

                            objdb.Entry(PersonPhone).State = EntityState.Deleted

                            'objtypeReport = PersonPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonPhone, Nothing) Is Nothing, item.GetValue(PersonPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listIdentificationPersonParty.Count > 0 Then
                        For Each PersonIdentification As goAML_Transaction_Party_Identification In Transactions.listIdentificationPersonParty

                            objdb.Entry(PersonIdentification).State = EntityState.Deleted

                            'objtypeReport = PersonIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(PersonIdentification, Nothing) Is Nothing, item.GetValue(PersonIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

End Class
