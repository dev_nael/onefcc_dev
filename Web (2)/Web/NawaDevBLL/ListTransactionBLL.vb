﻿Public Class ListTransactionBLL
    Public List_goAML_Transaction As New List(Of NawaDevDAL.goAML_Transaction)

    Public list_goAML_Transaction_Person As New List(Of NawaDevDAL.goAML_Transaction_Person)
    Public list_goAML_Transaction_Entity As New List(Of NawaDevDAL.goAML_Transaction_Entity)
    Public list_goAML_Transaction_Account As New List(Of NawaDevDAL.goAML_Transaction_Account)

    Public list_goAML_Trn_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
    Public list_goAML_Trn_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
    Public list_goAML_trn_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)
    Public list_goAML_Transaction_Person_Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)

    Public list_goAML_Trn_Conductor As New List(Of NawaDevDAL.goAML_Trn_Conductor)
    Public list_goAML_Trn_Conductor_Address As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
    Public list_goAML_trn_Conductor_Phone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)
    Public list_goAML_Trn_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
    Public list_goAML_trn_Person_Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)

    Public list_goAML_Trn_Entity_account As New List(Of NawaDevDAL.goAML_Trn_Entity_account)
    Public list_goAML_Trn_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
    Public list_goAML_Trn_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)

    Public list_goAML_Trn_Director As New List(Of NawaDevDAL.goAML_Trn_Director)
    Public list_goAML_Trn_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
    Public list_goAML_trn_Director_Phone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)
    Public List_goAML_Trn_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
    Public list_goAML_Trn_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)

    Public list_goAML_Transaction_Party As New List(Of NawaDevDAL.goAML_Transaction_Party)
    Public list_goAML_Trn_Par_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
    Public list_goAML_Trn_Par_Acc_Ent_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)

    Public list_goAML_Trn_Par_Entity_Director As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
    Public list_goAML_Trn_Par_Entity_Director_Address As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public list_goAML_Trn_Par_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
    Public list_goAML_Trn_par_acc_Signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
    Public list_goAML_Trn_par_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
    Public list_goAML_trn_par_acc_sign_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
    Public list_goAML_Trn_Party_Account As New List(Of NawaDevDAL.goAML_Trn_Party_Account)
    Public list_goAML_Trn_Par_Acc_Entity As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)
    Public list_goAML_Trn_par_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
    Public list_goAML_trn_par_acc_Entity_Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
    Public list_goAML_Trn_Party_Person As New List(Of NawaDevDAL.goAML_Trn_Party_Person)
    Public list_goAML_Trn_Party_Person_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
    Public list_goAML_Trn_Party_Person_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
    Public list_goAML_Transaction_Party_Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
    Public list_goAML_Trn_Party_Entity As New List(Of NawaDevDAL.goAML_Trn_Party_Entity)
    Public list_goAML_Trn_Party_Entity_Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
    Public list_goAML_Trn_Party_Entity_Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)

End Class