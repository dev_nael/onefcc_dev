﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionAccountBLL
    Public Shared Sub InsertTransactionAccountFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                    itemObjTransactionAccountFrom = Transactions.objAccountFrom

                    With itemObjTransactionAccountFrom
                        .FK_Report_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objdb.Entry(itemObjTransactionAccountFrom).State = EntityState.Added

                    'Dim objtypeReport As Type = itemObjTransactionAccountFrom.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjTransactionAccountFrom, Nothing) Is Nothing, item.GetValue(itemObjTransactionAccountFrom, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    '------------------------------------------------------------ Transaction Account Entity From -----------------------------------------
                    If Not Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account = 0 Then
                        TransactionAccountEntityBLL.InsertTransactionAccountEntityFrom(Transactions, Audit)
                    End If
                    '---------------------------------------- end Transaction Account Entity From --------------------------------------------------------------

                    '----------------------------Signatory Account Transaction From--------------------------------------------------
                    TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryFrom(Transactions, Audit)
                    ''----------------------------End Signatory Account Transaction From--------------------------------------------------
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub InsertTransactionAccountTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                    itemObjTransactionAccountTo = Transactions.objAccountTo

                    With itemObjTransactionAccountTo
                        .FK_Report_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionAccountTo).State = EntityState.Added

                    'Dim objtypeReport As Type = itemObjTransactionAccountTo.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjTransactionAccountTo, Nothing) Is Nothing, item.GetValue(itemObjTransactionAccountTo, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Not Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account = 0 Then
                        TransactionAccountEntityBLL.InsertTransactionAccountEntityTo(Transactions, Audit)

                        '-------------------------------End Director Entity Account Transaction----------------------------------------------

                    End If

                    '-----------------------------------Signatory Account Transaction------------------------------------------
                    TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryTo(Transactions, Audit)
                    ''-----------------------------------End Signatory Account Transaction------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                    itemObjTransactionAccountFrom = Transactions.objAccountFrom

                    objdb.Entry(itemObjTransactionAccountFrom).State = EntityState.Deleted

                    objdb.SaveChanges()

                    '------------------------------------------------------------ Transaction Account Entity From -----------------------------------------
                    If Not Transactions.objAccountEntityFrom.PK_goAML_Trn_Entity_account = 0 Then
                        TransactionAccountEntityBLL.DeleteTransactionAccountEntityFrom(Transactions, Audit)
                    End If
                    '---------------------------------------- end Transaction Account Entity From --------------------------------------------------------------

                    '----------------------------Signatory Account Transaction From--------------------------------------------------
                    TransactionAccountSignatoryBLL.DeleteTransactionAccountSignatoryFrom(Transactions, Audit)

                    ''----------------------------End Signatory Account Transaction From--------------------------------------------------
                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionAccountTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                    itemObjTransactionAccountTo = Transactions.objAccountTo

                    objdb.Entry(itemObjTransactionAccountTo).State = EntityState.Deleted
                    objdb.SaveChanges()

                    objdb.SaveChanges()

                    If Not Transactions.objAccountEntityTo.PK_goAML_Trn_Entity_account = 0 Then
                        TransactionAccountEntityBLL.DeleteTransactionAccountEntityTo(Transactions, Audit)
                    End If

                    '-----------------------------------Signatory Account Transaction------------------------------------------
                    TransactionAccountSignatoryBLL.DeleteTransactionAccountSignatoryTo(Transactions, Audit)
                    ''-----------------------------------End Signatory Account Transaction------------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
