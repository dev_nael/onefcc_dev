﻿Imports NawaDevDAL

Public Class ConductorDataBLL
    Public objConductor As goAML_Trn_Conductor

    Public listobjPhoneConductor As List(Of goAML_trn_Conductor_Phone)
    Public listobjAddressConductor As List(Of goAML_Trn_Conductor_Address)

    Public listobjPhoneEmployerConductor As List(Of goAML_trn_Conductor_Phone)
    Public listobjAddressEmployerConductor As List(Of goAML_Trn_Conductor_Address)

    Public listObjIdentificationConductor As List(Of goAML_Transaction_Person_Identification)
End Class
