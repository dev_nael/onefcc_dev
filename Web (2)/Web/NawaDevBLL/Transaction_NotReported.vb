﻿Public Class Transaction_NotReported
    Public objTransaction As New NawaDevDAL.goAML_Transaction_NotReported
    'Pengirim
    Public objPersonFrom As New NawaDevDAL.goAML_Transaction_Person_NotReported
    Public objEntityFrom As New NawaDevDAL.goAML_Transaction_Entity_NotReported
    Public objAccountFrom As New NawaDevDAL.goAML_Transaction_Account_NotReported
    Public LisAccountSignatoryFrom As New List(Of SignatoryClass_NotReported)

    'Conductor
    Public ObjConductor As New NawaDevDAL.goaml_Trn_Conductor_NotReported
    Public listObjAddressConductor As New List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)
    Public listObjPhoneConductor As New List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)
    Public listObjAddressEmployerConductor As New List(Of NawaDevDAL.goaml_Trn_Conductor_Address_NotReported)
    Public listObjPhoneEmployerConductor As New List(Of NawaDevDAL.goaml_trn_Conductor_Phone_NotReported)
    Public listObjIdentificationConductor As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)

    'PersonFrom
    Public listObjAddressPersonFrom As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
    Public listObjPhonePersonFrom As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
    Public listObjAddressEmployerPersonFrom As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
    Public listObjPhoneEmployerPersonFrom As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
    Public listObjIdentificationPersonFrom As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)

    'Account
    Public objAccountEntityFrom As New NawaDevDAL.goAML_Trn_Entity_account_NotReported
    Public listObjAddressAccountEntityFrom As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)
    Public listObjPhoneAccountEntityFrom As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)

    Public listDirectorAccountEntityFrom As New List(Of DirectorClass_NotReported)

    'Entity
    Public ListObjAddressEntityFrom As New List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)
    Public listObjPhoneEntityFrom As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)

    Public listDirectorEntityFrom As New List(Of DirectorClass_NotReported)

    'Penerima-------------------------------------------------------------------
    Public objPersonTo As New NawaDevDAL.goAML_Transaction_Person_NotReported
    Public objEntityTo As New NawaDevDAL.goAML_Transaction_Entity_NotReported
    Public objAccountTo As New NawaDevDAL.goAML_Transaction_Account_NotReported
    Public LisAccountSignatoryTo As New List(Of SignatoryClass_NotReported)

    'Person
    Public listObjAddressPersonTo As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
    Public listObjPhonePersonTo As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
    Public listObjAddressEmployerPersonTo As New List(Of NawaDevDAL.goAML_Trn_Person_Address_NotReported)
    Public listObjPhoneEmployerPersonTo As New List(Of NawaDevDAL.goAML_trn_Person_Phone_NotReported)
    Public listObjIdentificationPersonTo As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification_NotReported)

    'Account
    Public objAccountEntityTo As New NawaDevDAL.goAML_Trn_Entity_account_NotReported
    Public listObjAddressAccountEntityto As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address_NotReported)
    Public listObjPhoneAccountEntityto As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone_NotReported)
    Public listDirectorAccountEntityto As New List(Of DirectorClass_NotReported)

    'Entity
    Public ListObjAddressEntityto As New List(Of NawaDevDAL.goAML_Trn_Entity_Address_NotReported)
    Public listObjPhoneEntityto As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone_NotReported)
    Public listDirectorEntityto As New List(Of DirectorClass_NotReported)

    'Multiparty
    Public objTransactionParty As New NawaDevDAL.goAML_Transaction_Party_NotReported
    Public listDirectorEntytAccountparty As New List(Of DirectorEntityAccountPartyClass_NotReported)
    Public listDirectorEntityParty As New List(Of DirectorEntityPartyClass_NotReported)
    Public listSignatoryAccountParty As New List(Of SignatoryAccountPartyClass_NotReported)

    ' multiparty account
    Public objAccountParty As New NawaDevDAL.goAML_Trn_Party_Account_NotReported
    Public objAccountEntityParty As New NawaDevDAL.goAML_Trn_Par_Acc_Entity_NotReported
    Public listAddresAccountEntityParty As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address_NotReported)
    Public listPhoneAccountEntityParty As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone_NotReported)

    ' multiparty person
    Public objPersonParty As New NawaDevDAL.goAML_Trn_Party_Person_NotReported
    Public listAddressPersonParty As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported)
    Public listPhonePersonParty As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported)
    Public listAddressEmployerPersonParty As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address_NotReported)
    Public listPhoneEmployerPersonParty As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone_NotReported)
    Public listIdentificationPersonParty As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification_NotReported)

    ' multiparty entity
    Public objentityParty As New NawaDevDAL.goAML_Trn_Party_Entity_NotReported
    Public listAddressEntityparty As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address_NotReported)
    Public listPhoneEntityParty As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone_NotReported)

End Class

