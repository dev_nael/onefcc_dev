﻿Imports System.Data.Entity
Imports Ext.Net
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection
Imports NawaBLL.Nawa

<Serializable()>
Public Class ReportAddress
    Public oPanelinput As FormPanel
    Sub New(ObjPanel As FormPanel)
        oPanelinput = ObjPanel
    End Sub
    Sub New()

    End Sub
    Public Sub Accept(pK_ModuleApproval_ID As String)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))
                            With objModuledata.ObjReportAddress
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledata.ObjReportAddress).State = EntityState.Added

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Insert,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(objModuledata, Nothing) Is Nothing, item.GetValue(objModuledata, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Update

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))
                            Dim objModuledataOld As ReportAddressData = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(ReportAddressData))

                            With objModuledata.ObjReportAddress
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledata.ObjReportAddress).State = EntityState.Modified

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = If(Not item.GetValue(objModuledataOld, Nothing) Is Nothing, item.GetValue(objModuledataOld, Nothing), ""),
                                    .NewValue = If(Not item.GetValue(objModuledata, Nothing) Is Nothing, item.GetValue(objModuledata, Nothing), "")
                                }

                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Delete

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))
                            objDB.Entry(objModuledata.ObjReportAddress).State = EntityState.Deleted

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Delete,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Activation

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))
                            objDB.Entry(objModuledata.ObjReportAddress).State = EntityState.Modified

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader
                            objATHeader.ApproveBy = Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase
                            objATHeader.FK_ModuleAction_ID = Common.ModuleActionEnum.Activation
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
    Public Sub Reject(pK_ModuleApproval_ID As Long)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.Rejected,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Insert,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.ObjReportAddress.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(objModuledata.ObjReportAddress, Nothing) Is Nothing, item.GetValue(objModuledata.ObjReportAddress, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Update

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))
                            Dim objModuledataOld As ReportAddressData = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(ReportAddressData))

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.Rejected,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.ObjReportAddress.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = If(Not item.GetValue(objModuledataOld.ObjReportAddress, Nothing) Is Nothing, item.GetValue(objModuledataOld.ObjReportAddress, Nothing), ""),
                                    .NewValue = If(Not item.GetValue(objModuledata.ObjReportAddress, Nothing) Is Nothing, item.GetValue(objModuledata.ObjReportAddress, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                            {
                                .ApproveBy = Common.SessionCurrentUser.UserID,
                                .CreatedBy = Common.SessionCurrentUser.UserID,
                                .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.Rejected,
                                .FK_ModuleAction_ID = Common.ModuleActionEnum.Delete,
                                .ModuleLabel = objModule.ModuleLabel
                            }
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.ObjReportAddress.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(objModuledata.ObjReportAddress, Nothing) Is Nothing, item.GetValue(objModuledata.ObjReportAddress, Nothing), "")
                                }
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                        Case Common.ModuleActionEnum.Activation

                            Dim objModuledata As ReportAddressData = Common.Deserialize(objApproval.ModuleField, GetType(ReportAddressData))

                            Dim objATHeader As New NawaDevDAL.AuditTrailHeader
                            objATHeader.ApproveBy = Common.SessionCurrentUser.UserID
                            objATHeader.CreatedBy = Common.SessionCurrentUser.UserID
                            objATHeader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objATHeader.FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.Rejected
                            objATHeader.FK_ModuleAction_ID = Common.ModuleActionEnum.Activation
                            objATHeader.ModuleLabel = objModule.ModuleLabel
                            objDB.Entry(objATHeader).State = EntityState.Added
                            objDB.SaveChanges()

                            Dim objtype As Type = objModuledata.ObjReportAddress.GetType
                            Dim properties() As PropertyInfo = objtype.GetProperties
                            For Each item As PropertyInfo In properties
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledata.ObjReportAddress, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledata.ObjReportAddress, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objDB.Entry(objATDetail).State = EntityState.Added
                            Next

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub
    Public Sub LoadPanel(formPanelNew As FormPanel, moduleField As String, unikID As String)
        Dim objReportAddressBLL As ReportAddressData = Common.Deserialize(moduleField, GetType(ReportAddressData))
        Dim kategori, negara As String
        If Not objReportAddressBLL Is Nothing Then
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "ID", "PK_Customer_Address_ID" & unikID, objReportAddressBLL.ObjReportAddress.PK_Customer_Address_ID)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Detail Dari", "FK_Ref_detail_of" & unikID, objReportAddressBLL.ObjReportAddress.FK_Ref_Detail_Of)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "address of", "FK_to_Table_ID" & unikID, objReportAddressBLL.ObjReportAddress.FK_To_Table_ID)
            kategori = GetContactCategory(objReportAddressBLL.ObjReportAddress.Address_Type)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Tipe Alamat", "Address_type" & unikID, kategori)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Alamat", "Address" & unikID, objReportAddressBLL.ObjReportAddress.Address)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kecamatan", "Town" & unikID, objReportAddressBLL.ObjReportAddress.Town)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kota/Kabupaten", "City" & unikID, objReportAddressBLL.ObjReportAddress.City)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Kode Pos", "Kode Pos" & unikID, objReportAddressBLL.ObjReportAddress.Zip)
            negara = GetNational(objReportAddressBLL.ObjReportAddress.Country_Code)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Negara", "country_code" & unikID, negara)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Provinsi", "State" & unikID, objReportAddressBLL.ObjReportAddress.State)
            BLL.NawaFramework.ExtDisplayField(formPanelNew, "Catatan", "comments" & unikID, objReportAddressBLL.ObjReportAddress.Comments)
        End If
    End Sub
    Sub LoadPanelDetail(formPanelReportAddressDetail As FormPanel, moduleName As String, strUnikKey As String)
        Dim objReportAddress As goAML_Ref_Address = GetReportAddressByID(strUnikKey)
        Dim negara, kategori As String
        If Not objReportAddress Is Nothing Then
            Dim strUnik As String = Guid.NewGuid.ToString()
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "ID", "PK_Customer_Address_ID" & strUnik, objReportAddress.PK_Customer_Address_ID)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Detail Dari", "FK_Ref_detail_of" & strUnik, objReportAddress.FK_Ref_Detail_Of)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "address of", "FK_to_Table_ID" & strUnik, objReportAddress.FK_To_Table_ID)
            kategori = GetContactCategory(objReportAddress.Address_Type)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Tipe Alamat", "Address_type" & strUnik, kategori)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Alamat", "Address" & strUnik, objReportAddress.Address)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Kecamatan", "Town" & strUnik, objReportAddress.Town)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Kota/Kabupaten", "City" & strUnik, objReportAddress.City)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Kode Pos", "Kode Pos" & strUnik, objReportAddress.Zip)
            negara = GetNational(objReportAddress.Country_Code)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Negara", "country_code" & strUnik, negara)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Provinsi", "State" & strUnik, objReportAddress.State)
            BLL.NawaFramework.ExtDisplayField(formPanelReportAddressDetail, "Catatan", "comments" & strUnik, objReportAddress.Comments)
        End If
    End Sub
    Function GetNational(kode As String) As String
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objdb.goAML_Ref_Nama_Negara Where x.Kode = kode Select x.Keterangan).FirstOrDefault
        End Using
    End Function
    Function GetContactCategory(kode As String) As String
        Using objdb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objdb.goAML_Ref_Kategori_Kontak Where x.Kode = kode Select x.Keterangan).FirstOrDefault
        End Using
    End Function
    Function GetReportAddressByID(strUnikKey As String) As goAML_Ref_Address
        Using objDb As NawaDatadevEntities = New NawaDatadevEntities
            Return (From x In objDb.goAML_Ref_Address Where x.PK_Customer_Address_ID = strUnikKey Select x).FirstOrDefault
        End Using
    End Function
    Public Sub SaveEditTanpaApproval(objSave As goAML_Ref_Address, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    objSave.ApprovedBy = Common.SessionCurrentUser.UserID
                    objSave.ApprovedDate = Now
                    objDB.Entry(objSave).State = EntityState.Modified

                    Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                    {
                        .ApproveBy = Common.SessionCurrentUser.UserID,
                        .CreatedBy = Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.AffectedToDatabase,
                        .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                        .ModuleLabel = objModule.ModuleLabel
                    }

                    objDB.Entry(objATHeader).State = EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = objSave.GetType
                    Dim properties() As PropertyInfo = objtype.GetProperties
                    For Each item As PropertyInfo In properties
                        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        {
                            .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                            .FieldName = item.Name,
                            .OldValue = "",
                            .NewValue = If(Not item.GetValue(objSave, Nothing) Is Nothing, item.GetValue(objSave, Nothing), "")
                        }
                        objDB.Entry(objATDetail).State = EntityState.Added
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
    Public Sub SaveEditApproval(objSave As goAML_Ref_Address, objModule As NawaDAL.Module)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objData As New ReportAddressData
                    objData.ObjReportAddress = objSave
                    Dim dataXML As String = Common.Serialize(objData)

                    Dim objWICBefore As goAML_Ref_Address = objDB.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = objSave.PK_Customer_Address_ID).FirstOrDefault

                    Dim objDataBefore As New ReportAddressData
                    objDataBefore.ObjReportAddress = objWICBefore
                    Dim dataXMLBefore As String = Common.Serialize(objDataBefore)

                    Dim objModuleApproval As New NawaDevDAL.ModuleApproval With
                    {
                        .ModuleName = objModule.ModuleName,
                        .ModuleKey = objSave.PK_Customer_Address_ID,
                        .ModuleField = dataXML,
                        .ModuleFieldBefore = dataXMLBefore,
                        .PK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                        .CreatedBy = Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now
                    }

                    objDB.Entry(objModuleApproval).State = EntityState.Added

                    Dim objATHeader As New NawaDevDAL.AuditTrailHeader With
                    {
                        .ApproveBy = Common.SessionCurrentUser.UserID,
                        .CreatedBy = Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        .FK_AuditTrailStatus_ID = Common.AuditTrailStatusEnum.WaitingToApproval,
                        .FK_ModuleAction_ID = Common.ModuleActionEnum.Update,
                        .ModuleLabel = objModule.ModuleLabel
                    }
                    objDB.Entry(objATHeader).State = EntityState.Added
                    objDB.SaveChanges()

                    Dim objtype As Type = objData.GetType
                    Dim properties() As PropertyInfo = objtype.GetProperties
                    For Each item As PropertyInfo In properties
                        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        {
                            .FK_AuditTrailHeader_ID = objATHeader.PK_AuditTrail_ID,
                            .FieldName = item.Name,
                            .OldValue = If(Not item.GetValue(objWICBefore, Nothing) Is Nothing, item.GetValue(objWICBefore, Nothing), ""),
                            .NewValue = If(Not item.GetValue(objData, Nothing) Is Nothing, item.GetValue(objSave, Nothing), "")
                        }

                        objDB.Entry(objATDetail).State = EntityState.Added
                    Next

                    objDB.SaveChanges()
                    objTrans.Commit()

                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
