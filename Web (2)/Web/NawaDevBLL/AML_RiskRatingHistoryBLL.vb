﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports NawaDevDAL

Public Class AML_RiskRatingHistoryBLL
    Public Shared Function GetAMLRiskHistoryHeader(ByVal id As String) As AML_RISK_RATING_RESULT_HISTORY
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_RISK_RATING_RESULT_HISTORY Where X.PK_AML_RISK_RATING_RESULT_HISTORY_ID = id Select X).FirstOrDefault
        End Using
    End Function
    'detail list'
    Public Shared Function GetAMLHistoryList(ByVal id As String) As List(Of AML_RISK_RATING_RESULT_HISTORY)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_RISK_RATING_RESULT_HISTORY Where X.CIF_NO = id Select X).ToList
        End Using

        'Return NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM AML_RISK_RATING_RESULT_HISTORY where CIF_NO = " & id.ToString, Nothing)

    End Function

    Public Shared Function GetAMLHistoryDetail(id As String) As List(Of AML_RISK_RATING_RESULT_DETAIL_HISTORY)
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_RISK_RATING_RESULT_DETAIL_HISTORY Where X.FK_AML_RISK_RATING_RESULT_HISTORY = id Select X).ToList
        End Using

        'Return NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM AML_RISK_RATING_RESULT_DETAIL_HISTORY where FK_AML_RISK_RATING_RESULT_HISTORY = " & id.ToString, Nothing)
    End Function

    'untuk halaman view
    Public Shared Function GetAMLHistoryResult(id As Integer) As DataTable
        'Using objdb As New NawaDatadevEntities
        '    objdb.Database.ExecuteSqlCommand("SELECT * FROM vw_AML_RiskRatingHistory where pk_Id = " & id.ToString())
        'End Using
        Return NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_AML_RISK_RATING_HISTORY where PK_AML_RISK_RATING_RESULT_ID = " & id.ToString, Nothing)
    End Function
    'untuk windows popup detail
    Public Shared Function GetAMLHistoryResultDetail(id As String) As DataTable
        'Using objdb As New NawaDatadevEntities
        '    objdb.Database.ExecuteSqlCommand("SELECT * FROM vw_AML_RiskRatingHistory where pk_Id = " & id.ToString())
        'End Using
        Return NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text,
           "SELECT 
            A.CIF_NO,
            CUSTOMER_NAME,
            RISK_RATING_TOTAL_SCORE,
            RISK_RATING_NAME,
            A.CreatedDate
            FROM AML_RISK_RATING_RESULT_HISTORY A
            join AML_CUSTOMER B on a.CIF_NO = B.CIF_NO where a.PK_AML_RISK_RATING_RESULT_HISTORY_ID = '" & id.ToString() & "'")
    End Function
    Public Shared Function GetRequestForHeader(Kode As String) As AML_RISK_RATING_RESULT_HISTORY
        Using NawaDataEntitiy As NawaDatadevEntities = New NawaDatadevEntities
            Return (From X In NawaDataEntitiy.AML_RISK_RATING_RESULT_HISTORY Where X.PK_AML_RISK_RATING_RESULT_HISTORY_ID = Kode).FirstOrDefault
        End Using
    End Function
End Class


Public Class AMLRiskRatingHistoryClass
        Public AMLRiskRatingHistory As New NawaDevDAL.AML_RISK_RATING_RESULT_HISTORY
    End Class