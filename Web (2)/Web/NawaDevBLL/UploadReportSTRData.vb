﻿<Serializable()>
Public Class UploadReportSTRData
    Public objlistReport As New List(Of NawaDevDAL.goAML_ReportSTR)
    Public objlistIndikatorLaporan As New List(Of NawaDevDAL.goAML_Indikator_LaporanSTR)
    Public objlistTransactionBiparty As New List(Of NawaDevDAL.goAML_Transaction_BiPartySTR)
    Public objlistTransactionMultiparty As New List(Of NawaDevDAL.goAML_Transaction_MultiPartySTR)
    Public objlistActivity As New List(Of NawaDevDAL.goAML_ActivitySTR)
End Class
