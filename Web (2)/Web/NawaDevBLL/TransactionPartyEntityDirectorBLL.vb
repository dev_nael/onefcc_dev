﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection
Imports NawaDevBLL

Public Class TransactionPartyEntityDirectorBLL
    Public Shared Sub InsertTransactionPartyEntityDirector(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In Transactions.listDirectorEntityParty
                        Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                        itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty

                        With itemObjDirectorParty
                            .FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID
                            .ApprovedBy = Common.SessionCurrentUser.UserID
                            .ApprovedDate = Now
                        End With

                        objdb.Entry(itemObjDirectorParty).State = EntityState.Added

                        Dim objtypeReport As Type = itemObjDirectorParty.GetType
                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        For Each item As PropertyInfo In propertiesReport
                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            {
                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                .FieldName = item.Name,
                                .OldValue = "",
                                .NewValue = If(Not item.GetValue(itemObjDirectorParty, Nothing) Is Nothing, item.GetValue(itemObjDirectorParty, Nothing), "")
                            }
                            objdb.Entry(objATDetail).State = EntityState.Added
                        Next
                        objdb.SaveChanges()

                        If objModuledataDirectorEntityParty.listPhone.Count > 0 Then
                            For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                With DirectorPhone
                                    .FK_Trn_par_Entity_Director_ID = objModuledataDirectorEntityParty.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhone).State = EntityState.Added

                                objtypeReport = DirectorPhone.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(DirectorPhone, Nothing) Is Nothing, item.GetValue(DirectorPhone, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listAddress.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                With DirectorAddress
                                    .FK_Trn_par_Entity_Director_ID = objModuledataDirectorEntityParty.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddress).State = EntityState.Added

                                objtypeReport = DirectorAddress.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(DirectorAddress, Nothing) Is Nothing, item.GetValue(DirectorAddress, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listAddressEmployer.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                With DirectorAddressEmployer
                                    .FK_Trn_par_Entity_Director_ID = objModuledataDirectorEntityParty.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Added

                                objtypeReport = DirectorAddressEmployer.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(DirectorAddressEmployer, Nothing) Is Nothing, item.GetValue(DirectorAddressEmployer, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listPhoneEmployer.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                With DirectorPhoneEmployer
                                    .FK_Trn_par_Entity_Director_ID = objModuledataDirectorEntityParty.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Added

                                objtypeReport = DirectorPhoneEmployer.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(DirectorPhoneEmployer, Nothing) Is Nothing, item.GetValue(DirectorPhoneEmployer, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                With DirectorIdentification
                                    .FK_Person_ID = objModuledataDirectorEntityParty.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID
                                    .ApprovedBy = Common.SessionCurrentUser.UserID
                                    .ApprovedDate = Now
                                End With
                                objdb.Entry(DirectorIdentification).State = EntityState.Added

                                objtypeReport = DirectorIdentification.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(DirectorIdentification, Nothing) Is Nothing, item.GetValue(DirectorIdentification, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next
                        End If
                        objdb.SaveChanges()
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyEntityDirector(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In Transactions.listDirectorEntityParty
                        Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                        itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty

                        objdb.Entry(itemObjDirectorParty).State = EntityState.Deleted

                        objdb.SaveChanges()

                        If objModuledataDirectorEntityParty.listPhone.Count > 0 Then
                            For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                objdb.Entry(DirectorPhone).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listAddress.Count > 0 Then
                            For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                objdb.Entry(DirectorAddress).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listAddressEmployer.Count > 0 Then
                            For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                objdb.Entry(DirectorAddressEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listPhoneEmployer.Count > 0 Then
                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                objdb.Entry(DirectorPhoneEmployer).State = EntityState.Deleted
                            Next
                        End If

                        If objModuledataDirectorEntityParty.listIdentification.Count > 0 Then
                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                objdb.Entry(DirectorIdentification).State = EntityState.Deleted
                            Next
                        End If
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyEntityDirector(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    For Each Directorx As goAML_Trn_Par_Entity_Director In (From x In objdb.goAML_Trn_Par_Entity_Director Where x.FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID).ToList

                        Dim objDirectorClassCek As DirectorEntityPartyClass = Transactions.listDirectorEntityParty.Find(Function(x) x.objDirectorEntityParty.PK_Trn_Par_Entity_Director_ID = Directorx.PK_Trn_Par_Entity_Director_ID)

                        Dim objDirectorCek As goAML_Trn_Par_Entity_Director = objDirectorClassCek.objDirectorEntityParty

                        If objDirectorCek Is Nothing Then
                            DeleteTransactionPartyEntityDirector(Transactions, Audit)
                        Else
                            Dim TransactionPartyEntityDirector As goAML_Trn_Par_Entity_Director = objDirectorClassCek.objDirectorEntityParty
                            Dim TransactionPartyEntityDirectorCek As goAML_Trn_Par_Entity_Director = (From x In objdb.goAML_Trn_Par_Entity_Director Where x.PK_Trn_Par_Entity_Director_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID Select x).FirstOrDefault

                            If TransactionPartyEntityDirectorCek Is Nothing Then
                                InsertTransactionPartyEntityDirector(Transactions, Audit)
                            Else
                                objdb.Entry(TransactionPartyEntityDirectorCek).CurrentValues.SetValues(TransactionPartyEntityDirector)
                                objdb.Entry(TransactionPartyEntityDirectorCek).State = EntityState.Modified

                                If objDirectorClassCek.listPhone.Count > 0 Then

                                    For Each itemDirectorPhonex As goAML_Trn_Par_Entity_Director_Phone In (From x In objdb.goAML_Trn_Par_Entity_Director_Phone Where x.FK_Trn_par_Entity_Director_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID And x.isemployer = False)
                                        Dim objCekDirectorPhone As goAML_Trn_Par_Entity_Director_Phone = objDirectorClassCek.listPhone.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = itemDirectorPhonex.PK_Trn_Par_Entity_Director_Phone_ID And x.isemployer = False)
                                        If objCekDirectorPhone Is Nothing Then
                                            objdb.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                        End If
                                    Next

                                    For Each itemDirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objDirectorClassCek.listPhone
                                        Dim objCek As goAML_Trn_Par_Entity_Director_Phone = (From x In objdb.goAML_Trn_Par_Entity_Director_Phone Where x.PK_Trn_Par_Entity_Director_Phone_ID = itemDirectorPhone.PK_Trn_Par_Entity_Director_Phone_ID And x.isemployer = False Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemDirectorPhone).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next
                                End If

                                'Director Account Address Party
                                If objDirectorClassCek.listAddress.Count > 0 Then

                                    'Delete
                                    For Each itemDirectorAddressx As goAML_Trn_Par_Entity_Director_Address In (From x In objdb.goAML_Trn_Par_Entity_Director_Address Where x.FK_Trn_par_Entity_Director_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID And x.isemployer = False)
                                        Dim objCekDirectorAddress As goAML_Trn_Par_Entity_Director_Address = objDirectorClassCek.listAddress.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = itemDirectorAddressx.PK_Trn_Par_Entity_Address_ID And x.isemployer = False)
                                        If objCekDirectorAddress Is Nothing Then
                                            objdb.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemDirectorAddress As goAML_Trn_Par_Entity_Director_Address In objDirectorClassCek.listAddress
                                        Dim objCek As goAML_Trn_Par_Acc_Ent_Director_Address = (From x In objdb.goAML_Trn_Par_Acc_Ent_Director_Address Where x.PK_Trn_Par_Acc_Entity_Address_ID = itemDirectorAddress.PK_Trn_Par_Entity_Address_ID And x.isemployer = False Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemDirectorAddress).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemDirectorAddress)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Trn_Par_Entity_Director_Address In objDirectorClassCek.listAddress
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Director Account Employer Address Party
                                If objDirectorClassCek.listAddressEmployer.Count > 0 Then

                                    'Delete
                                    For Each itemDirectorAddressx As goAML_Trn_Par_Entity_Director_Address In (From x In objdb.goAML_Trn_Par_Entity_Director_Address Where x.FK_Trn_par_Entity_Director_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID And x.isemployer = True)
                                        Dim objCekDirectorAddress As goAML_Trn_Par_Entity_Director_Address = objDirectorClassCek.listAddressEmployer.Find(Function(x) x.PK_Trn_Par_Entity_Address_ID = itemDirectorAddressx.PK_Trn_Par_Entity_Address_ID And x.isemployer = True)
                                        If objCekDirectorAddress Is Nothing Then
                                            objdb.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemDirectorAddress As goAML_Trn_Par_Entity_Director_Address In objDirectorClassCek.listAddressEmployer
                                        Dim objCek As goAML_Trn_Par_Entity_Director_Address = (From x In objdb.goAML_Trn_Par_Entity_Director_Address Where x.PK_Trn_Par_Entity_Address_ID = itemDirectorAddress.PK_Trn_Par_Entity_Address_ID And x.isemployer = True Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemDirectorAddress).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemDirectorAddress)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Trn_Par_Entity_Director_Address In objDirectorClassCek.listAddressEmployer
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Director Account Employer Phone Party
                                If objDirectorClassCek.listPhoneEmployer.Count > 0 Then

                                    'Delete
                                    For Each itemDirectorPhonex As goAML_Trn_Par_Entity_Director_Phone In (From x In objdb.goAML_Trn_Par_Entity_Director_Phone Where x.FK_Trn_par_Entity_Director_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID And x.isemployer = True)
                                        Dim objCekDirectorPhone As goAML_Trn_Par_Entity_Director_Phone = objDirectorClassCek.listPhoneEmployer.Find(Function(x) x.PK_Trn_Par_Entity_Director_Phone_ID = itemDirectorPhonex.PK_Trn_Par_Entity_Director_Phone_ID And x.isemployer = True)
                                        If objCekDirectorPhone Is Nothing Then
                                            objdb.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemDirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objDirectorClassCek.listPhoneEmployer
                                        Dim objCek As goAML_Trn_Par_Entity_Director_Phone = (From x In objdb.goAML_Trn_Par_Entity_Director_Phone Where x.PK_Trn_Par_Entity_Director_Phone_ID = itemDirectorPhone.PK_Trn_Par_Entity_Director_Phone_ID Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemDirectorPhone).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Trn_Par_Entity_Director_Phone In objDirectorClassCek.listPhoneEmployer
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                                'Director Account Entity Identification Party
                                If objDirectorClassCek.listIdentification.Count > 0 Then

                                    'Delete
                                    For Each itemDirectorIdentificationx As goAML_Transaction_Party_Identification In (From x In objdb.goAML_Transaction_Party_Identification Where x.FK_Person_ID = TransactionPartyEntityDirector.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = 4)
                                        Dim objCekDirectorIdentification As goAML_Transaction_Party_Identification = objDirectorClassCek.listIdentification.Find(Function(x) x.PK_Transaction_Party_Identification_ID = itemDirectorIdentificationx.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 4)
                                        If objCekDirectorIdentification Is Nothing Then
                                            objdb.Entry(itemDirectorIdentificationx).State = EntityState.Deleted
                                        End If
                                    Next

                                    'Add Modified
                                    For Each itemDirectorIdentification As goAML_Transaction_Party_Identification In objDirectorClassCek.listIdentification
                                        Dim objCek As goAML_Transaction_Party_Identification = (From x In objdb.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = itemDirectorIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 4 Select x).FirstOrDefault
                                        If objCek Is Nothing Then
                                            objdb.Entry(itemDirectorIdentification).State = EntityState.Added
                                        Else
                                            objdb.Entry(objCek).CurrentValues.SetValues(itemDirectorIdentification)
                                            objdb.Entry(objCek).State = EntityState.Modified
                                        End If
                                    Next

                                    'AuditTrail
                                    For Each itemDetail As goAML_Transaction_Party_Identification In objDirectorClassCek.listIdentification
                                        Dim objtypeReport As Type = itemDetail.GetType
                                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                                        For Each item As PropertyInfo In propertiesReport
                                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                            {
                                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                                .FieldName = item.Name,
                                                .OldValue = "",
                                                .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                            }
                                            objdb.Entry(objATDetail).State = EntityState.Added
                                        Next
                                    Next

                                End If

                            End If

                        End If
                    Next

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception

                End Try
            End Using
        End Using
    End Sub
End Class
