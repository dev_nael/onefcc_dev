﻿Imports System.Data.Entity
Imports System.Reflection
Imports NawaBLL
Imports NawaDevDAL

Public Class TransactionConductorBLL
    Public Shared Sub InsertTransactionConductor(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjConductor As New goAML_Trn_Conductor
                    itemObjConductor = Transactions.ObjConductor

                    With itemObjConductor
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjConductor).State = EntityState.Added

                    'Dim objtypeReport As Type = itemObjConductor.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjConductor, Nothing) Is Nothing, item.GetValue(itemObjConductor, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneConductor.Count > 0 Then
                        For Each ConductorPhone As goAML_trn_Conductor_Phone In Transactions.listObjPhoneConductor
                            With ConductorPhone
                                .FK_Trn_Conductor_ID = Transactions.ObjConductor.PK_goAML_Trn_Conductor_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(ConductorPhone).State = EntityState.Added

                            '    objtypeReport = ConductorPhone.GetType
                            '    propertiesReport = objtypeReport.GetProperties
                            '    For Each item As PropertyInfo In propertiesReport
                            '        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '        {
                            '            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '            .FieldName = item.Name,
                            '            .OldValue = "",
                            '            .NewValue = If(Not item.GetValue(ConductorPhone, Nothing) Is Nothing, item.GetValue(ConductorPhone, Nothing), "")
                            '        }
                            '        objdb.Entry(objATDetail).State = EntityState.Added
                            '    Next
                        Next
                    End If

                    If Transactions.listObjAddressConductor.Count > 0 Then
                        For Each ConductorAddress As goAML_Trn_Conductor_Address In Transactions.listObjAddressConductor
                            With ConductorAddress
                                .FK_Trn_Conductor_ID = Transactions.ObjConductor.PK_goAML_Trn_Conductor_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(ConductorAddress).State = EntityState.Added

                            'objtypeReport = ConductorAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorAddress, Nothing) Is Nothing, item.GetValue(ConductorAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerConductor.Count > 0 Then
                        For Each ConductorAddress As goAML_Trn_Conductor_Address In Transactions.listObjAddressEmployerConductor
                            With ConductorAddress
                                .FK_Trn_Conductor_ID = Transactions.ObjConductor.PK_goAML_Trn_Conductor_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(ConductorAddress).State = EntityState.Added

                            'objtypeReport = ConductorAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorAddress, Nothing) Is Nothing, item.GetValue(ConductorAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerConductor.Count > 0 Then

                        For Each ConductorPhone As goAML_trn_Conductor_Phone In Transactions.listObjPhoneEmployerConductor
                            With ConductorPhone
                                .FK_Trn_Conductor_ID = Transactions.ObjConductor.PK_goAML_Trn_Conductor_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(ConductorPhone).State = EntityState.Added

                            'objtypeReport = ConductorPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorPhone, Nothing) Is Nothing, item.GetValue(ConductorPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjIdentificationConductor.Count > 0 Then
                        For Each ConductorIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationConductor
                            With ConductorIdentification
                                .FK_Person_ID = Transactions.ObjConductor.PK_goAML_Trn_Conductor_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(ConductorIdentification).State = EntityState.Added

                            'objtypeReport = ConductorIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorIdentification, Nothing) Is Nothing, item.GetValue(ConductorIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next

                    End If

                    objdb.SaveChanges()
                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionConductor(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjConductor As New goAML_Trn_Conductor
                    itemObjConductor = Transactions.ObjConductor

                    objdb.Entry(itemObjConductor).State = EntityState.Deleted

                    'Dim objtypeReport As Type = itemObjConductor.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjConductor, Nothing) Is Nothing, item.GetValue(itemObjConductor, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneConductor.Count > 0 Then
                        For Each ConductorPhone As goAML_trn_Conductor_Phone In Transactions.listObjPhoneConductor
                            objdb.Entry(ConductorPhone).State = EntityState.Deleted

                            'objtypeReport = ConductorPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorPhone, Nothing) Is Nothing, item.GetValue(ConductorPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressConductor.Count > 0 Then
                        For Each ConductorAddress As goAML_Trn_Conductor_Address In Transactions.listObjAddressConductor
                            objdb.Entry(ConductorAddress).State = EntityState.Deleted

                            'objtypeReport = ConductorAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorAddress, Nothing) Is Nothing, item.GetValue(ConductorAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjAddressEmployerConductor.Count > 0 Then
                        For Each ConductorAddress As goAML_Trn_Conductor_Address In Transactions.listObjAddressEmployerConductor
                            objdb.Entry(ConductorAddress).State = EntityState.Deleted

                            'objtypeReport = ConductorAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorAddress, Nothing) Is Nothing, item.GetValue(ConductorAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.listObjPhoneEmployerConductor.Count > 0 Then
                        For Each ConductorPhone As goAML_trn_Conductor_Phone In Transactions.listObjPhoneEmployerConductor
                            objdb.Entry(ConductorPhone).State = EntityState.Deleted

                            'objtypeReport = ConductorPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorPhone, Nothing) Is Nothing, item.GetValue(ConductorPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next

                    End If

                    If Transactions.listObjIdentificationConductor.Count > 0 Then
                        For Each ConductorIdentification As goAML_Transaction_Person_Identification In Transactions.listObjIdentificationConductor
                            objdb.Entry(ConductorIdentification).State = EntityState.Deleted

                            'objtypeReport = ConductorIdentification.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(ConductorIdentification, Nothing) Is Nothing, item.GetValue(ConductorIdentification, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
