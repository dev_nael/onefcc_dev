﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection
Imports NawaDevBLL

Public Class TransactionPartyAccountEntityBLL
    Public Shared Sub InsertTransactionPartyAccountEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                    itemObjTransactionAccountEntityParty = Transactions.objAccountEntityParty

                    With itemObjTransactionAccountEntityParty
                        .FK_Trn_Party_Account_ID = Transactions.objAccountParty.PK_Trn_Party_Account_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionAccountEntityParty).State = EntityState.Added

                    objdb.SaveChanges()

                    If Transactions.listPhoneAccountEntityParty.Count > 0 Then
                        For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In Transactions.listPhoneAccountEntityParty
                            With EntityPhoneParty
                                .FK_Trn_par_acc_Entity_ID = Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhoneParty).State = EntityState.Added
                        Next
                    End If

                    If Transactions.listAddresAccountEntityParty.Count > 0 Then
                        For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In Transactions.listAddresAccountEntityParty
                            With EntityAddressParty
                                .FK_Trn_par_acc_Entity_ID = Transactions.objAccountEntityParty.PK_Trn_Par_acc_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddressParty).State = EntityState.Added
                        Next
                    End If
                    objdb.SaveChanges()

                    '-------------------------------Director Entity Account Party---------------------------------------------------
                    TransactionPartyAccountEntityDirectorBLL.InsertTransactionPartyAccountEntityDirector(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyAccountEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                    itemObjTransactionAccountEntityParty = Transactions.objAccountEntityParty

                    objdb.Entry(itemObjTransactionAccountEntityParty).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Transactions.listPhoneAccountEntityParty.Count > 0 Then
                        For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In Transactions.listPhoneAccountEntityParty
                            objdb.Entry(EntityPhoneParty).State = EntityState.Deleted
                        Next
                    End If

                    If Transactions.listAddresAccountEntityParty.Count > 0 Then
                        For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In Transactions.listAddresAccountEntityParty
                            objdb.Entry(EntityAddressParty).State = EntityState.Deleted
                        Next
                    End If
                    objdb.SaveChanges()

                    '-------------------------------Director Entity Account Party---------------------------------------------------
                    TransactionPartyAccountEntityDirectorBLL.DeleteTransactionPartyAccountEntityDirector(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyAccountEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim TransactionPartyAccountEntity As goAML_Trn_Par_Acc_Entity = Transactions.objAccountEntityParty
                    Dim TransactionPartyAccountEntityCek As goAML_Trn_Par_Acc_Entity = (From x In objdb.goAML_Trn_Par_Acc_Entity Where x.PK_Trn_Par_acc_Entity_ID = TransactionPartyAccountEntity.PK_Trn_Par_acc_Entity_ID Select x).FirstOrDefault

                    If TransactionPartyAccountEntityCek Is Nothing Then
                        InsertTransactionPartyAccountEntity(Transactions, Audit)
                    Else
                        objdb.Entry(TransactionPartyAccountEntityCek).CurrentValues.SetValues(TransactionPartyAccountEntity)
                        objdb.Entry(TransactionPartyAccountEntityCek).State = EntityState.Modified

                        objdb.SaveChanges()

                        If Transactions.listDirectorEntytAccountparty.Count > 0 Then
                            TransactionPartyAccountEntityDirectorBLL.UpdateTransactionPartyAccountEntityDirector(Transactions, Audit)
                        End If
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using


    End Sub
End Class
