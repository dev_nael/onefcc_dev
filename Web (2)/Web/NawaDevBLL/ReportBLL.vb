﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class ReportBLL
    Sub New()

    End Sub
    Shared Function getPKModuleApprovalByID(pk As Long) As Long
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = pk.ToString()).FirstOrDefault()
            Return obj.PK_ModuleApproval_ID
        End Using
    End Function
    Public Sub Accept(pK_ModuleApproval_ID As String, note As String)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = x.ModuleName).FirstOrDefault()
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert


                            '----------------------------------Report-----------------------------------------------------------
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))

                            With objModuledataReport.objReport
                                .status = 1
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Modified

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Accept, ReportRemarkBLL.actionForm.Add, objModuledataReport.objReport.PK_Report_ID, note)
                            objDB.SaveChanges()

                        Case Common.ModuleActionEnum.Update

                            '----------------------------------Report-----------------------------------------------------------
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            Dim objModuledataReportOld As Report = Common.Deserialize(objApproval.ModuleFieldBefore, GetType(Report))

                            With objModuledataReport.objReport
                                .status = 1
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Modified

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport, objModuledataReportOld)

                            objDB.SaveChanges()

                            '----------------------------------End Report-----------------------------------------------------------

                            '--------------------------------------Transaction-------------------------------------------------------
                            If objModuledataReport.listObjTransaction.Count > 0 Then

                                For Each objModuledataTransaction As Transaction In objModuledataReport.listObjTransaction
                                    Dim itemTransactionx As goAML_Transaction = (From x In objDB.goAML_Transaction Where x.FK_Report_ID = objModuledataReport.objReport.PK_Report_ID Select x).FirstOrDefault
                                    Dim objTran As Transaction = objModuledataReport.listObjTransaction.Find(Function(x) x.objTransaction.PK_Transaction_ID = itemTransactionx.PK_Transaction_ID)
                                    Dim objTransactionCek As goAML_Transaction = objTran.objTransaction
                                    If objTransactionCek Is Nothing Then
                                        objDB.Entry(itemTransactionx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransactionx)

                                        If objModuledataTransaction.objTransaction.FK_Transaction_Type = 1 Then

                                            '-------FUncrion delete ALl Transaction-----------------------------

                                            '---Conductor
                                            Dim itemConductorx As goAML_Trn_Conductor = (From x In objDB.goAML_Trn_Conductor Where x.FK_Transaction_ID = itemTransactionx.PK_Transaction_ID Select x).FirstOrDefault
                                            If itemConductorx.PK_goAML_Trn_Conductor_ID <> objModuledataTransaction.ObjConductor.PK_goAML_Trn_Conductor_ID Then
                                                objDB.Entry(itemConductorx).State = EntityState.Deleted
                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataTransaction.ObjConductor)
                                            End If

                                            For Each itemConductorPhonex As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = False).ToList
                                                If Not itemConductorPhonex Is Nothing Then
                                                    objDB.Entry(itemConductorPhonex).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhonex)
                                                End If
                                            Next

                                            For Each itemConductorAddressx As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = False)
                                                If Not itemConductorAddressx Is Nothing Then
                                                    objDB.Entry(itemConductorAddressx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddressx)
                                                End If
                                            Next

                                            For Each itemConductorAddressx As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = True)
                                                If Not itemConductorAddressx Is Nothing Then
                                                    objDB.Entry(itemConductorAddressx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddressx)
                                                End If
                                            Next

                                            For Each itemConductorPhonex As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = True)
                                                If Not itemConductorPhonex Is Nothing Then
                                                    objDB.Entry(itemConductorPhonex).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhonex)
                                                End If
                                            Next

                                            For Each itemConductorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = 3 And x.from_or_To_Type = 1)
                                                If Not itemConductorIdentificationx Is Nothing Then
                                                    objDB.Entry(itemConductorIdentificationx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentificationx)
                                                End If
                                            Next

                                            '-------------------------------------------------------Pengirim
                                            If itemTransactionx.FK_Sender_From_Information = 1 Then
                                                Dim itemAccountx As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                If Not itemAccountx Is Nothing Then
                                                    objDB.Entry(itemAccountx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccountx)

                                                    Dim itemAccountEntityx As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = itemAccountx.PK_Account_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                    If Not itemAccountEntityx Is Nothing Then
                                                        objDB.Entry(itemAccountEntityx).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccountEntityx)

                                                        For Each Phonex As goAML_Trn_Acc_Entity_Phone In (From x In objDB.goAML_Trn_Acc_Entity_Phone Where x.FK_Trn_Acc_Entity = itemAccountEntityx.PK_goAML_Trn_Entity_account).ToList
                                                            If Not Phonex Is Nothing Then
                                                                objDB.Entry(Phonex).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                            End If
                                                        Next

                                                        For Each Addressx As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                                            If Not Addressx Is Nothing Then
                                                                objDB.Entry(Addressx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                            End If
                                                        Next

                                                        Dim objListDirector As List(Of goAML_Trn_Director) = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemAccountEntityx.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 1).ToList
                                                        For Each Directorx As goAML_Trn_Director In objListDirector
                                                            If Not Directorx Is Nothing Then
                                                                objDB.Entry(Directorx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)

                                                                For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                    If Not Phonex Is Nothing Then
                                                                        objDB.Entry(Phonex).State = EntityState.Deleted
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                    End If
                                                                Next
                                                                For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                    If Not Addressx Is Nothing Then
                                                                        objDB.Entry(Addressx).State = EntityState.Deleted
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)

                                                                    End If
                                                                Next
                                                                For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Addressx.PK_goAML_Trn_Director_Address_ID And x.isEmployer = True).ToList
                                                                    If Not Addressx Is Nothing Then
                                                                        objDB.Entry(Addressx).State = EntityState.Deleted
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                    End If
                                                                Next
                                                                For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                    If Not Phonex Is Nothing Then
                                                                        objDB.Entry(Phonex).State = EntityState.Deleted
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                    End If
                                                                Next
                                                                For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Directorx.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 1).ToList
                                                                    If Not Identificationx Is Nothing Then
                                                                        objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                    End If
                                                                Next
                                                            End If
                                                        Next
                                                    End If

                                                    Dim objListSignatory As List(Of goAML_Trn_acc_Signatory) = (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = itemAccountx.PK_Account_ID And x.FK_From_Or_To = 1).ToList
                                                    For Each Signatoryx As goAML_Trn_acc_Signatory In objListSignatory
                                                        If Not Signatoryx Is Nothing Then
                                                            objDB.Entry(Signatoryx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Signatoryx)

                                                            For Each Phonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = True).ToList
                                                                If Addressx.PK_goAML_Trn_Acc_Entity_Address_ID = 0 Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Phonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 1).ToList
                                                                If Not Identificationx Is Nothing Then
                                                                    objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                End If
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            End If
                                            If itemTransactionx.FK_Sender_From_Information = 2 Then
                                                Dim itemPersonx As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                If Not itemPersonx Is Nothing Then
                                                    objDB.Entry(itemPersonx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonx)

                                                    For Each Phonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = False).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next
                                                    For Each Addressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = False).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next
                                                    For Each Addressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = True).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next
                                                    For Each Phonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = False).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next
                                                    For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = itemPersonx.PK_Person_ID And x.FK_Person_ID = 1 And x.from_or_To_Type = 1).ToList
                                                        If Not Identificationx Is Nothing Then
                                                            objDB.Entry(Identificationx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                        End If
                                                    Next
                                                End If
                                            End If
                                            If itemTransactionx.FK_Sender_From_Information = 3 Then
                                                Dim itemEntityX As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                If Not itemEntityX Is Nothing Then
                                                    objDB.Entry(itemEntityX).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityX)
                                                    For Each Phonex As goAML_Trn_Entity_Phone In (From x In objDB.goAML_Trn_Entity_Phone Where x.PK_goAML_Trn_Entity_Phone = itemEntityX.PK_Entity_ID).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next
                                                    For Each Addressx As goAML_Trn_Entity_Address In (From x In objDB.goAML_Trn_Entity_Address Where x.PK_goAML_Trn_Entity_Address_ID = itemEntityX.PK_Entity_ID).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next

                                                    Dim objListDIrector As List(Of goAML_Trn_Director) = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemEntityX.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 3).ToList
                                                    For Each Directorx As goAML_Trn_Director In objListDIrector
                                                        If Not Directorx Is Nothing Then
                                                            objDB.Entry(Directorx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)

                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Directorx.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 1).ToList
                                                                If Not Identificationx Is Nothing Then
                                                                    objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                End If
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            End If

                                            '-------------Penerima---------------

                                            If itemTransactionx.FK_Sender_To_Information = 1 Then
                                                Dim itemAccountx As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                If Not itemAccountx Is Nothing Then
                                                    objDB.Entry(itemAccountx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccountx)

                                                    Dim itemAccountEntityx As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = itemAccountx.PK_Account_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                    If Not itemAccountEntityx Is Nothing Then
                                                        objDB.Entry(itemAccountEntityx).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccountEntityx)

                                                        For Each Phonex As goAML_Trn_Acc_Entity_Phone In (From x In objDB.goAML_Trn_Acc_Entity_Phone Where x.FK_Trn_Acc_Entity = itemAccountEntityx.PK_goAML_Trn_Entity_account).ToList
                                                            If Not Phonex Is Nothing Then
                                                                objDB.Entry(Phonex).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                            End If
                                                        Next

                                                        For Each Addressx As goAML_Trn_Acc_Entity_Address In (From x In objDB.goAML_Trn_Acc_Entity_Address Where x.FK_Trn_Acc_Entity = itemAccountEntityx.PK_goAML_Trn_Entity_account).ToList
                                                            If Not Addressx Is Nothing Then
                                                                objDB.Entry(Addressx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                            End If
                                                        Next

                                                        Dim objListDirector As List(Of goAML_Trn_Director) = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemAccountEntityx.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 1).ToList
                                                        For Each Directorx As goAML_Trn_Director In objListDirector
                                                            If Not Directorx Is Nothing Then
                                                                objDB.Entry(Directorx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)
                                                            End If
                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Directorx.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 2).ToList
                                                                If Not Identificationx Is Nothing Then
                                                                    objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                End If
                                                            Next
                                                        Next
                                                    End If
                                                    Dim objListSignatory As List(Of goAML_Trn_acc_Signatory) = (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = itemAccountx.PK_Account_ID And x.FK_From_Or_To = 2).ToList
                                                    For Each Signatoryx As goAML_Trn_acc_Signatory In objListSignatory
                                                        If Not Signatoryx Is Nothing Then
                                                            objDB.Entry(Signatoryx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Signatoryx)

                                                            For Each Phonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = True).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Phonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = True).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 2).ToList
                                                                If Not Identificationx Is Nothing Then
                                                                    objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                End If
                                                            Next
                                                        End If
                                                    Next
                                                End If

                                            End If
                                            If itemTransactionx.FK_Sender_To_Information = 2 Then
                                                Dim itemPersonx As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                If Not itemPersonx Is Nothing Then
                                                    objDB.Entry(itemPersonx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonx)

                                                    For Each Phonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = False).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next
                                                    For Each Addressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = False).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next
                                                    For Each Phonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = True).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next
                                                    For Each Addressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = itemPersonx.PK_Person_ID And x.isEmployer = True).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next
                                                    For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = itemPersonx.PK_Person_ID And x.FK_Person_Type = 1 And x.from_or_To_Type = 2).ToList
                                                        If Not Identificationx Is Nothing Then
                                                            objDB.Entry(Identificationx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                        End If
                                                    Next

                                                End If
                                            End If
                                            If itemTransactionx.FK_Sender_To_Information = 3 Then
                                                Dim itemEntityX As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = itemTransactionx.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                If Not itemEntityX Is Nothing Then
                                                    objDB.Entry(itemEntityX).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityX)

                                                    For Each Phonex As goAML_Trn_Entity_Phone In (From x In objDB.goAML_Trn_Entity_Phone Where x.FK_Trn_Entity = itemEntityX.PK_Entity_ID).ToList
                                                        If Not Phonex Is Nothing Then
                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                        End If
                                                    Next

                                                    For Each Addressx As goAML_Trn_Entity_Address In (From x In objDB.goAML_Trn_Entity_Address Where x.FK_Trn_Entity = itemEntityX.PK_Entity_ID).ToList
                                                        If Not Addressx Is Nothing Then
                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                        End If
                                                    Next

                                                    Dim objListDirector As List(Of goAML_Trn_Director) = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemEntityX.PK_Entity_ID And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 3).ToList
                                                    For Each Directorx As goAML_Trn_Director In objListDirector
                                                        If Not Directorx Is Nothing Then
                                                            objDB.Entry(Directorx).State = EntityState.Deleted
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)

                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = False).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Phonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                If Not Phonex Is Nothing Then
                                                                    objDB.Entry(Phonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Phonex)
                                                                End If
                                                            Next
                                                            For Each Addressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.isEmployer = True).ToList
                                                                If Not Addressx Is Nothing Then
                                                                    objDB.Entry(Addressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Addressx)
                                                                End If
                                                            Next
                                                            For Each Identificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Directorx.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 2).ToList
                                                                If Not Identificationx Is Nothing Then
                                                                    objDB.Entry(Identificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Identificationx)
                                                                End If
                                                            Next
                                                        End If
                                                    Next
                                                End If
                                            End If


                                        Else

                                            TransactionPartyBLL.DeleteTransactionParty(objModuledataTransaction, objATHeaderReport)

                                        End If

                                    End If


                                    '------------Baca Transaction-------------------------
                                    For Each Transaction As Transaction In objModuledataReport.listObjTransaction
                                        Dim itemTransaction As goAML_Transaction = Transaction.objTransaction
                                        Dim objcekTransaction As goAML_Transaction = (From x In objDB.goAML_Transaction Where x.PK_Transaction_ID = itemTransaction.PK_Transaction_ID Select x).FirstOrDefault
                                        '------------------------jika belum ada transaksi yg di edit dan melakukan penambahan transaksi----------------------------
                                        If objcekTransaction Is Nothing Then
                                            With objModuledataTransaction.objTransaction
                                                .FK_Report_ID = objModuledataReport.objReport.PK_Report_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = Now
                                            End With
                                            objDB.Entry(itemTransaction).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransaction)
                                            If itemTransaction.FK_Transaction_Type = 1 Then


                                                '-------------------------Function transaksi add All---------------------------

                                                If Not objModuledataTransaction.ObjConductor Is Nothing Then
                                                    TransactionConductorBLL.InsertTransactionConductor(objModuledataTransaction, objATHeaderReport)
                                                    Dim itemObjConductor As New goAML_Trn_Conductor
                                                    itemObjConductor = objModuledataTransaction.ObjConductor
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjConductor)

                                                    For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneConductor
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                    Next
                                                    For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressConductor
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                    Next
                                                    For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressEmployerConductor
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                    Next
                                                    For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneEmployerConductor
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                    Next
                                                    For Each ConductorIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationConductor
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorIdentification)
                                                    Next
                                                End If

                                                '-------------------------------------Pengirim----------------------------------------------------------

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 1 Then

                                                    TransactionAccountBLL.InsertTransactionAccountFrom(objModuledataTransaction, objATHeaderReport)
                                                    Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                                                    itemObjTransactionAccountFrom = objModuledataTransaction.objAccountFrom
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountFrom)

                                                    If Not objModuledataTransaction.objAccountEntityFrom Is Nothing Then
                                                        Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                                                        itemObjTransactionAccountEntityFrom = objModuledataTransaction.objAccountEntityFrom
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityFrom)

                                                        If objModuledataTransaction.listObjPhoneAccountEntityFrom.Count > 0 Then
                                                            For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                            Next
                                                        End If

                                                        If objModuledataTransaction.listObjAddressAccountEntityFrom.Count > 0 Then
                                                            For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                            Next
                                                        End If

                                                        If objModuledataTransaction.listDirectorAccountEntityFrom.Count > 0 Then
                                                            For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                                                Dim itemObjDirector As New goAML_Trn_Director
                                                                itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                                                                    For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                    Next
                                                                End If
                                                            Next
                                                        End If
                                                    End If

                                                    If Not objModuledataTransaction.LisAccountSignatoryFrom.Count > 0 Then
                                                        TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryFrom(objModuledataTransaction, objATHeaderReport)
                                                        For Each objModuledataSignatoryAccountFrom As SignatoryClass In objModuledataTransaction.LisAccountSignatoryFrom
                                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                                            itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                                            If objModuledataSignatoryAccountFrom.listPhoneSignatory.Count > 0 Then
                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountFrom.listAddressSignatory.Count > 0 Then
                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory.Count > 0 Then
                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountFrom.listAddressEmployerSignatory.Count > 0 Then
                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountFrom.listIdentification.Count > 0 Then
                                                                For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                                                Next
                                                            End If
                                                        Next
                                                    End If
                                                    ''----------------------------End Signatory Account Transaction From--------------------------------------------------
                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 2 Then
                                                    TransactionPersonBLL.InsertTransactionPersonFrom(objModuledataTransaction, objATHeaderReport)
                                                    Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                                                    ItemObjTransactionPersonFrom = objModuledataTransaction.objPersonFrom
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonFrom)
                                                    If objModuledataTransaction.listObjPhonePersonFrom.Count > 0 Then
                                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjAddressPersonFrom.Count > 0 Then
                                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjPhoneEmployerPersonFrom.Count > 0 Then
                                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjAddressEmployerPersonFrom.Count > 0 Then
                                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjIdentificationPersonFrom.Count > 0 Then
                                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                                        Next
                                                    End If
                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 3 Then

                                                    TransactionEntityBLL.InsertTransactionEntityFrom(objModuledataTransaction, objATHeaderReport)
                                                    Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                                                    itemObjTransactionEntityFrom = objModuledataTransaction.objEntityFrom
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityFrom)

                                                    If objModuledataTransaction.listObjPhoneEntityFrom.Count > 0 Then
                                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.ListObjAddressEntityFrom.Count > 0 Then
                                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listDirectorEntityFrom.Count > 0 Then
                                                        For Each objModuledataDirectorEntityFrom As DirectorClass In objModuledataTransaction.listDirectorEntityFrom
                                                            Dim itemObjDirector As New goAML_Trn_Director
                                                            itemObjDirector = objModuledataDirectorEntityFrom.objDirector
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                            If objModuledataDirectorEntityFrom.listPhoneDirector.Count > 0 Then
                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityFrom.listAddressDirector.Count > 0 Then
                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityFrom.listAddressEmployerDirector.Count > 0 Then
                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityFrom.listIdentification.Count > 0 Then
                                                                For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                Next
                                                            End If
                                                        Next


                                                    End If
                                                    ''---------------------------------End Director Entity Transaction-----------------------------------------

                                                End If



                                                '----------------------------------------Penerima---------------------------------------------------------

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 1 Then

                                                    TransactionAccountBLL.InsertTransactionAccountTo(objModuledataTransaction, objATHeaderReport)
                                                    Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                                                    itemObjTransactionAccountTo = objModuledataTransaction.objAccountTo
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountTo)

                                                    If Not objModuledataTransaction.objAccountEntityTo Is Nothing Then
                                                        Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                                                        itemObjTrnEntityAccount = objModuledataTransaction.objAccountEntityTo
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTrnEntityAccount)
                                                        If objModuledataTransaction.listObjPhoneAccountEntityto.Count > 0 Then
                                                            For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityto
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listObjAddressAccountEntityto.Count > 0 Then
                                                            For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityto
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listDirectorAccountEntityto.Count > 0 Then
                                                            For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto
                                                                Dim itemObjDirector As New goAML_Trn_Director
                                                                itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                    Next
                                                                End If
                                                                If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                                                                    For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                    Next
                                                                End If
                                                            Next



                                                        End If
                                                    End If

                                                    If Not objModuledataTransaction.LisAccountSignatoryTo.Count > 0 Then
                                                        TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryTo(objModuledataTransaction, objATHeaderReport)
                                                        For Each objModuledataSignatoryAccountTo As SignatoryClass In objModuledataTransaction.LisAccountSignatoryTo
                                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                                            itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                                            If objModuledataSignatoryAccountTo.listPhoneSignatory.Count > 0 Then
                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountTo.listAddressSignatory.Count > 0 Then
                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountTo.listPhoneEmployerSignatory.Count > 0 Then
                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountTo.listAddressEmployerSignatory.Count > 0 Then
                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                Next
                                                            End If
                                                            If objModuledataSignatoryAccountTo.listIdentification.Count > 0 Then
                                                                For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                                                Next
                                                            End If
                                                        Next
                                                    End If
                                                    ''-----------------------------------End Signatory Account Transaction------------------------------------------

                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 2 Then

                                                    TransactionPersonBLL.InsertTransactionPersonTo(objModuledataTransaction, objATHeaderReport)
                                                    Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                                                    ItemObjTransactionPersonTo = objModuledataTransaction.objPersonTo
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonTo)

                                                    If objModuledataTransaction.listObjPhonePersonTo.Count > 0 Then
                                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjAddressPersonTo.Count > 0 Then
                                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjPhoneEmployerPersonTo.Count > 0 Then
                                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjAddressEmployerPersonTo.Count > 0 Then
                                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listObjIdentificationPersonTo.Count > 0 Then
                                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                                        Next
                                                    End If
                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 3 Then

                                                    TransactionEntityBLL.InsertTransactionEntityTo(objModuledataTransaction, objATHeaderReport)
                                                    Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                                                    itemObjTransactionEntityTo = objModuledataTransaction.objEntityTo
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityTo)

                                                    If objModuledataTransaction.listObjPhoneEntityto.Count > 0 Then
                                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityto
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.ListObjAddressEntityto.Count > 0 Then
                                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityto
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                        Next
                                                    End If
                                                    If objModuledataTransaction.listDirectorEntityto.Count > 0 Then
                                                        For Each objModuledataDirectorEntityTo As DirectorClass In objModuledataTransaction.listDirectorEntityto
                                                            Dim itemObjDirector As New goAML_Trn_Director
                                                            itemObjDirector = objModuledataDirectorEntityTo.objDirector
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                            If objModuledataDirectorEntityTo.listPhoneDirector.Count > 0 Then
                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityTo.listAddressDirector.Count > 0 Then
                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityTo.listPhoneEmployerDirector.Count > 0 Then
                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityTo.listAddressEmployerDirector.Count > 0 Then
                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                Next
                                                            End If
                                                            If objModuledataDirectorEntityTo.listIdentification.Count > 0 Then
                                                                For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                Next
                                                            End If
                                                        Next


                                                    End If
                                                    ''---------------------------------End Director Entity Transaction-----------------------------------------

                                                End If

                                            Else

                                                TransactionPartyBLL.InsertTransactionParty(objModuledataTransaction, objATHeaderReport)

                                                Dim itemObjTransactionParty As New goAML_Transaction_Party
                                                itemObjTransactionParty = objModuledataTransaction.objTransactionParty
                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionParty)

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 1 Then


                                                    Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                                                    itemObjTransactionAccountParty = objModuledataTransaction.objAccountParty
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountParty)

                                                    Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                                                    itemObjTransactionAccountEntityParty = objModuledataTransaction.objAccountEntityParty
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityParty)

                                                    For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In objModuledataTransaction.listPhoneAccountEntityParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                                    Next

                                                    For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In objModuledataTransaction.listAddresAccountEntityParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                                    Next

                                                    For Each objModuledataDirectorEntityAccountParty As DirectorEntityAccountPartyClass In objModuledataTransaction.listDirectorEntytAccountparty
                                                        Dim itemObjDirectorParty As New goAML_Trn_Par_Acc_Ent_Director
                                                        itemObjDirectorParty = objModuledataDirectorEntityAccountParty.objDirectorEntityAccountParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                                        For Each DirectorPhone As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhone
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                        Next

                                                        For Each DirectorAddress As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddress
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                        Next

                                                        For Each DirectorAddressEmployer As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddressEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                                        Next

                                                        For Each DirectorPhoneEmployer As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhoneEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                                        Next

                                                        For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityAccountParty.listIdentification
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                        Next
                                                    Next

                                                    For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In objModuledataTransaction.listSignatoryAccountParty
                                                        Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                                                        itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatoryParty)

                                                        For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhone)
                                                        Next

                                                        For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddress)
                                                        Next

                                                        For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddressEmployer)
                                                        Next

                                                        For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhoneEmployer)
                                                        Next

                                                        For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryIdentification)
                                                        Next
                                                    Next

                                                End If

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 2 Then
                                                    Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                                                    ItemObjTransactionPartyPerson = objModuledataTransaction.objPersonParty
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPartyPerson)

                                                    For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhonePersonParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                    Next

                                                    For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressPersonParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                    Next

                                                    For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressEmployerPersonParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                    Next

                                                    For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhoneEmployerPersonParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                    Next

                                                    For Each PersonIdentification As goAML_Transaction_Party_Identification In objModuledataTransaction.listIdentificationPersonParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                                    Next

                                                End If

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 3 Then

                                                    Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                                                    itemObjTransactionEntityParty = objModuledataTransaction.objentityParty
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityParty)

                                                    For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In objModuledataTransaction.listPhoneEntityParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                                    Next

                                                    For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In objModuledataTransaction.listAddressEntityparty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                                    Next

                                                    For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In objModuledataTransaction.listDirectorEntityParty
                                                        Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                                                        itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                                        For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                        Next

                                                        For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                        Next

                                                        For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                                        Next

                                                        For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                                        Next

                                                        For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                        Next

                                                    Next

                                                End If

                                            End If

                                        Else

                                            '---------------------Update Transaction---------------
                                            objDB.Entry(objcekTransaction).CurrentValues.SetValues(itemTransaction)
                                            objDB.Entry(objcekTransaction).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransaction, objcekTransaction)

                                            If objModuledataTransaction.objTransaction.FK_Transaction_Type = 1 Then

                                                '--------------------Cek COnductor--------------------------------

                                                Dim itemConductorx As goAML_Trn_Conductor = (From x In objDB.goAML_Trn_Conductor Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID Select x).FirstOrDefault
                                                If objModuledataTransaction.ObjConductor Is Nothing Then
                                                    objDB.Entry(itemConductorx).State = EntityState.Deleted
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorx)
                                                    '--------------------Function Conductor All Delete--------------------------

                                                    For Each ConductorPhone As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = False).ToList
                                                        objDB.Entry(ConductorPhone).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                    Next

                                                    For Each ConductorAddress As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = False).ToList
                                                        objDB.Entry(ConductorAddress).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                    Next

                                                    For Each ConductorPhone As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = True).ToList
                                                        objDB.Entry(ConductorPhone).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                    Next

                                                    For Each ConductorAddress As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.isEmployer = True).ToList
                                                        objDB.Entry(ConductorAddress).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                    Next

                                                    For Each ConductorIdentification As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = itemConductorx.PK_goAML_Trn_Conductor_ID And x.FK_Person_ID = 3 And x.from_or_To_Type = 2).ToList
                                                        objDB.Entry(ConductorIdentification).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorIdentification)
                                                    Next

                                                    objDB.SaveChanges()

                                                Else

                                                    Dim Conductor As goAML_Trn_Conductor = objModuledataTransaction.ObjConductor
                                                    Dim ConductorCek As goAML_Trn_Conductor = (From x In objDB.goAML_Trn_Conductor Where x.PK_goAML_Trn_Conductor_ID = Conductor.PK_goAML_Trn_Conductor_ID)

                                                    If ConductorCek Is Nothing Then
                                                        TransactionConductorBLL.InsertTransactionConductor(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, Conductor)
                                                        '----------------------Function All Transaction Conductor Insert---------------------------------

                                                        If objModuledataTransaction.listObjPhoneConductor.Count > 0 Then
                                                            For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneConductor
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listObjAddressConductor.Count > 0 Then
                                                            For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressConductor
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listObjAddressEmployerConductor.Count > 0 Then
                                                            For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressEmployerConductor
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listObjPhoneEmployerConductor.Count > 0 Then
                                                            For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneEmployerConductor
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                                            Next
                                                        End If
                                                        If objModuledataTransaction.listObjIdentificationConductor.Count > 0 Then
                                                            For Each ConductorIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationConductor
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorIdentification)
                                                            Next
                                                            objDB.SaveChanges()
                                                        End If

                                                    Else
                                                        objDB.Entry(ConductorCek).CurrentValues.SetValues(Conductor)
                                                        objDB.Entry(ConductorCek).State = EntityState.Modified
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, Conductor, ConductorCek)

                                                        'Conductor Phone From
                                                        'Delete
                                                        For Each itemConductorPhonex As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = Conductor.PK_goAML_Trn_Conductor_ID)
                                                            Dim objCekConductorPhone As goAML_trn_Conductor_Phone = objModuledataTransaction.listObjPhoneConductor.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = itemConductorPhonex.PK_goAML_trn_Conductor_Phone)
                                                            If objCekConductorPhone Is Nothing Then
                                                                objDB.Entry(itemConductorPhonex).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhonex)
                                                            End If
                                                        Next

                                                        'Add Modified
                                                        For Each itemConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneConductor
                                                            Dim objCek As goAML_trn_Conductor_Phone = (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorPhone.PK_goAML_trn_Conductor_Phone Select x).FirstOrDefault
                                                            If objCek Is Nothing Then
                                                                objDB.Entry(itemConductorPhone).State = EntityState.Added
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone)
                                                            Else
                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorPhone)
                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone, objCek)
                                                            End If
                                                        Next

                                                        'Conductor Address From
                                                        'delete
                                                        For Each itemConductorAddressx As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = Conductor.PK_goAML_Trn_Conductor_ID)
                                                            Dim objCekConductorAddress As goAML_Trn_Conductor_Address = objModuledataTransaction.listObjAddressConductor.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = itemConductorAddressx.PK_goAML_Trn_Conductor_Address_ID)
                                                            If objCekConductorAddress Is Nothing Then
                                                                objDB.Entry(itemConductorAddressx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddressx)
                                                            End If
                                                        Next

                                                        'add modified
                                                        For Each itemConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressConductor
                                                            Dim objCek As goAML_Trn_Conductor_Address = (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorAddress.PK_goAML_Trn_Conductor_Address_ID).FirstOrDefault
                                                            If objCek Is Nothing Then
                                                                objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                            Else
                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                            End If
                                                        Next

                                                        'Conductor Employer Address From
                                                        'delete
                                                        For Each itemConductorAddressx As goAML_Trn_Conductor_Address In (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = Conductor.PK_goAML_Trn_Conductor_ID)
                                                            Dim objCekConductorAddress As goAML_Trn_Conductor_Address = objModuledataTransaction.listObjAddressEmployerConductor.Find(Function(x) x.PK_goAML_Trn_Conductor_Address_ID = itemConductorAddressx.PK_goAML_Trn_Conductor_Address_ID)
                                                            If objCekConductorAddress Is Nothing Then
                                                                objDB.Entry(itemConductorAddressx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddressx)
                                                            End If
                                                        Next

                                                        'add modified
                                                        For Each itemConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressEmployerConductor
                                                            Dim objCek As goAML_Trn_Conductor_Address = (From x In objDB.goAML_Trn_Conductor_Address Where x.FK_Trn_Conductor_ID = itemConductorAddress.PK_goAML_Trn_Conductor_Address_ID).FirstOrDefault
                                                            If objCek Is Nothing Then
                                                                objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                            Else
                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                            End If
                                                        Next

                                                        'Conductor Employer Phone From
                                                        'delete
                                                        For Each itemConductorPhonex As goAML_trn_Conductor_Phone In (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = Conductor.PK_goAML_Trn_Conductor_ID)
                                                            Dim objCekConductorPhone As goAML_trn_Conductor_Phone = objModuledataTransaction.listObjPhoneEmployerConductor.Find(Function(x) x.PK_goAML_trn_Conductor_Phone = itemConductorPhonex.PK_goAML_trn_Conductor_Phone)
                                                            If objCekConductorPhone Is Nothing Then
                                                                objDB.Entry(itemConductorPhonex).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhonex)
                                                            End If
                                                        Next

                                                        'add modified
                                                        For Each itemConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneEmployerConductor
                                                            Dim objCek As goAML_trn_Conductor_Phone = (From x In objDB.goAML_trn_Conductor_Phone Where x.FK_Trn_Conductor_ID = itemConductorPhone.PK_goAML_trn_Conductor_Phone Select x).FirstOrDefault
                                                            If objCek Is Nothing Then
                                                                objDB.Entry(itemConductorPhone).State = EntityState.Added
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone)
                                                            Else
                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorPhone)
                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone, objCek)
                                                            End If
                                                        Next

                                                        'Conductor Identification From
                                                        'delete
                                                        For Each itemConductorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = Conductor.PK_goAML_Trn_Conductor_ID)
                                                            Dim objCekConductorIdentification As goAML_Transaction_Person_Identification = objModuledataTransaction.listObjIdentificationConductor.Find(Function(x) x.FK_Person_ID = itemConductorIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                            If objCekConductorIdentification Is Nothing Then
                                                                objDB.Entry(itemConductorIdentificationx).State = EntityState.Deleted
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentificationx)
                                                            End If
                                                        Next

                                                        'add modified
                                                        For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationConductor
                                                            Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                            If objCek Is Nothing Then
                                                                objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                            Else
                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                            End If
                                                        Next
                                                    End If
                                                End If

                                                '--------------------End Cek COnductor--------------------------------

                                                '-------------------------------------Pengirim----------------------------------------------------------

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 1 Then

                                                    Dim itemx As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                    Dim TransactionAccount As goAML_Transaction_Account = objModuledataTransaction.objAccountFrom
                                                    If TransactionAccount Is Nothing Then
                                                        objDB.Entry(itemx).State = EntityState.Deleted

                                                        TransactionAccountBLL.DeleteTransactionAccountFrom(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)
                                                        '-------------------------------Function All Transaction Account Delete-----------------------

                                                    Else

                                                        Dim TransactionAccountCek As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
                                                        If TransactionAccountCek Is Nothing Then

                                                            TransactionAccountBLL.InsertTransactionAccountFrom(objModuledataTransaction, objATHeaderReport)
                                                            Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                                                            itemObjTransactionAccountFrom = objModuledataTransaction.objAccountFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountFrom)

                                                            If Not objModuledataTransaction.objAccountEntityFrom Is Nothing Then
                                                                Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                                                                itemObjTransactionAccountEntityFrom = objModuledataTransaction.objAccountEntityFrom
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityFrom)

                                                                If objModuledataTransaction.listObjPhoneAccountEntityFrom.Count > 0 Then
                                                                    For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                    Next
                                                                End If

                                                                If objModuledataTransaction.listObjAddressAccountEntityFrom.Count > 0 Then
                                                                    For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                    Next
                                                                End If

                                                                If objModuledataTransaction.listDirectorAccountEntityFrom.Count > 0 Then
                                                                    For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                                                        Dim itemObjDirector As New goAML_Trn_Director
                                                                        itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                        If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                                                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                            Next
                                                                        End If
                                                                    Next
                                                                End If
                                                            End If

                                                            If Not objModuledataTransaction.LisAccountSignatoryFrom.Count > 0 Then
                                                                TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryFrom(objModuledataTransaction, objATHeaderReport)
                                                                For Each objModuledataSignatoryAccountFrom As SignatoryClass In objModuledataTransaction.LisAccountSignatoryFrom
                                                                    Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                                                    itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                                                    If objModuledataSignatoryAccountFrom.listPhoneSignatory.Count > 0 Then
                                                                        For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataSignatoryAccountFrom.listAddressSignatory.Count > 0 Then
                                                                        For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory.Count > 0 Then
                                                                        For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataSignatoryAccountFrom.listAddressEmployerSignatory.Count > 0 Then
                                                                        For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataSignatoryAccountFrom.listIdentification.Count > 0 Then
                                                                        For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                                                        Next
                                                                    End If
                                                                Next
                                                            End If

                                                        Else
                                                            objDB.Entry(TransactionAccountCek).CurrentValues.SetValues(TransactionAccount)
                                                            objDB.Entry(TransactionAccountCek).State = EntityState.Modified
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx, TransactionAccountCek)


                                                            Dim itemTransactionAccountEntityFromx As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = TransactionAccount.PK_Account_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                            If objModuledataTransaction.objAccountEntityFrom Is Nothing Then

                                                                TransactionAccountEntityBLL.DeleteTransactionAccountEntityFrom(objModuledataTransaction, objATHeaderReport)
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransactionAccountEntityFromx)
                                                                '----------------------Function All Transaction Account Entity Delete-----------------

                                                            Else
                                                                Dim itemTransactionAccountEntityFrom As goAML_Trn_Entity_account = objModuledataTransaction.objAccountEntityFrom
                                                                Dim itemTransactionAccountEntityFromCek As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = objModuledataTransaction.objAccountFrom.PK_Account_ID And x.FK_From_Or_To = 1).FirstOrDefault

                                                                'Transaction Account Entity From
                                                                If itemTransactionAccountEntityFromCek Is Nothing Then

                                                                    TransactionAccountEntityBLL.InsertTransactionAccountEntityFrom(objModuledataTransaction, objATHeaderReport)
                                                                    Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                                                                    itemObjTransactionAccountEntityFrom = objModuledataTransaction.objAccountEntityFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityFrom)

                                                                    If objModuledataTransaction.listObjPhoneAccountEntityFrom.Count > 0 Then
                                                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                        Next
                                                                    End If

                                                                    If objModuledataTransaction.listObjAddressAccountEntityFrom.Count > 0 Then
                                                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                        Next
                                                                    End If

                                                                    If objModuledataTransaction.listDirectorAccountEntityFrom.Count > 0 Then
                                                                        TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryFrom(objModuledataTransaction, objATHeaderReport)
                                                                        For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                                                            Dim itemObjDirector As New goAML_Trn_Director
                                                                            itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                            If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                                                                                For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                Next
                                                                            End If
                                                                        Next
                                                                    End If

                                                                Else

                                                                    objDB.Entry(itemTransactionAccountEntityFromCek).CurrentValues.SetValues(itemTransactionAccountEntityFrom)
                                                                    objDB.Entry(itemTransactionAccountEntityFromCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransactionAccountEntityFrom, itemTransactionAccountEntityFromCek)

                                                                    For Each Phonex As goAML_Trn_Acc_Entity_Phone In (From x In objDB.goAML_Trn_Acc_Entity_Phone Where x.FK_Trn_Acc_Entity = itemTransactionAccountEntityFrom.PK_goAML_Trn_Entity_account)
                                                                        Dim objCekAccEntPhone As goAML_Trn_Acc_Entity_Phone = objModuledataTransaction.listObjPhoneAccountEntityFrom.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Phone = Phonex.PK_goAML_Trn_Acc_Entity_Phone)
                                                                        If objCekAccEntPhone Is Nothing Then
                                                                            objDB.Entry(Phonex).State = EntityState.Deleted
                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objCekAccEntPhone)
                                                                        End If
                                                                    Next

                                                                    'Add Modified
                                                                    For Each itemAccEntPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                                                        Dim objCek As goAML_Trn_Acc_Entity_Phone = (From x In objDB.goAML_Trn_Acc_Entity_Phone Where x.PK_goAML_Trn_Acc_Entity_Phone = itemAccEntPhone.PK_goAML_Trn_Acc_Entity_Phone Select x).FirstOrDefault
                                                                        If objCek Is Nothing Then
                                                                            objDB.Entry(itemAccEntPhone).State = EntityState.Added
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccEntPhone)
                                                                        Else
                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemAccEntPhone)
                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccEntPhone, objCek)
                                                                        End If
                                                                    Next

                                                                    For Each Addressx As goAML_Trn_Acc_Entity_Address In (From x In objDB.goAML_Trn_Acc_Entity_Address Where x.FK_Trn_Acc_Entity = itemTransactionAccountEntityFrom.PK_goAML_Trn_Entity_account)
                                                                        Dim objCekAccEntAddress As goAML_Trn_Acc_Entity_Address = objModuledataTransaction.listObjAddressAccountEntityFrom.Find(Function(x) x.PK_Trn_Acc_Entity_Address_ID = Addressx.PK_Trn_Acc_Entity_Address_ID)
                                                                        If objCekAccEntAddress Is Nothing Then
                                                                            objDB.Entry(Addressx).State = EntityState.Deleted
                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objCekAccEntAddress)
                                                                        End If
                                                                    Next

                                                                    'Add Modified
                                                                    For Each itemAccEntAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                                                        Dim objCek As goAML_Trn_Acc_Entity_Address = (From x In objDB.goAML_Trn_Acc_Entity_Address Where x.PK_Trn_Acc_Entity_Address_ID = itemAccEntAddress.PK_Trn_Acc_Entity_Address_ID Select x).FirstOrDefault
                                                                        If objCek Is Nothing Then
                                                                            objDB.Entry(itemAccEntAddress).State = EntityState.Added
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccEntAddress)
                                                                        Else
                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemAccEntAddress)
                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemAccEntAddress, objCek)
                                                                        End If
                                                                    Next

                                                                    'Director Entity Account From
                                                                    For Each Directorx As goAML_Trn_Director In (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = objModuledataTransaction.objAccountEntityFrom.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 1).ToList
                                                                        Dim objDirectorClassCek As DirectorClass = objModuledataTransaction.listDirectorAccountEntityFrom.Find(Function(x) x.objDirector.PK_goAML_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.objDirector.FK_From_Or_To = 1 And x.objDirector.FK_Sender_Information = 1)
                                                                        Dim objDirectorCek As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                        If objDirectorCek Is Nothing Then
                                                                            objDB.Entry(Directorx).State = EntityState.Deleted
                                                                            TransactionAccountEntityDirectorBLL.DeleteTransactionAccountEntityDirectoryFrom(objModuledataTransaction, objATHeaderReport)
                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)
                                                                            '--------------------FUcntion All Transaction Account Entity Delete---------------------------

                                                                        Else

                                                                            Dim TransactionAccountEntityDirector As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                            Dim TransactionAccountEntityDirectorCek As goAML_Trn_Director = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemTransactionAccountEntityFromCek.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 1).FirstOrDefault

                                                                            If TransactionAccountEntityDirectorCek Is Nothing Then

                                                                                For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                                                                    Dim itemObjDirector As New goAML_Trn_Director
                                                                                    itemObjDirector = objModuledataDirectorEntityAccountFrom.objDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                                    If objModuledataDirectorEntityAccountFrom.listPhoneDirector.Count > 0 Then
                                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                        Next
                                                                                    End If
                                                                                    If objModuledataDirectorEntityAccountFrom.listAddressDirector.Count > 0 Then
                                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                        Next
                                                                                    End If
                                                                                    If objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                        Next
                                                                                    End If
                                                                                    If objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector.Count > 0 Then
                                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                        Next
                                                                                    End If
                                                                                    If objModuledataDirectorEntityAccountFrom.listIdentification.Count > 0 Then
                                                                                        For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                        Next
                                                                                    End If
                                                                                Next
                                                                                '-----------------------FUnction All Transaction Account Entity Director Insert-----

                                                                            Else
                                                                                objDB.Entry(TransactionAccountEntityDirectorCek).CurrentValues.SetValues(TransactionAccountEntityDirector)
                                                                                objDB.Entry(TransactionAccountEntityDirectorCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionAccountEntityDirector, TransactionAccountEntityDirectorCek)

                                                                                'Director Account Entity Phone From
                                                                                If objDirectorClassCek.listPhoneDirector.Count > 0 Then

                                                                                    For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                        If objCekDirectorPhone Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneDirector
                                                                                        Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.PK_goAML_trn_Director_Phone = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If

                                                                                'Director Account Entity Address From
                                                                                If objDirectorClassCek.listAddressDirector.Count > 0 Then

                                                                                    'Delete
                                                                                    For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                        If objCekDirectorAddress Is Nothing Then
                                                                                            objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressDirector
                                                                                        Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If

                                                                                'Director Account Entity Address Employer From
                                                                                If objDirectorClassCek.listAddressEmployerDirector.Count > 0 Then

                                                                                    'Delete
                                                                                    For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressEmployerDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                        If objCekDirectorAddress Is Nothing Then
                                                                                            objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressEmployerDirector
                                                                                        Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If

                                                                                'Director Account Entity Phone Employer From
                                                                                If objDirectorClassCek.listPhoneEmployerDirector.Count > 0 Then

                                                                                    'Delete
                                                                                    For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneEmployerDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                        If objCekDirectorPhone Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneEmployerDirector
                                                                                        Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If

                                                                                'Director Account Entity Identification From
                                                                                If objDirectorClassCek.listIdentification.Count > 0 Then

                                                                                    'Delete
                                                                                    For Each itemDirectorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objDirectorClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemDirectorIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                                                        If objCekDirectorIdentification Is Nothing Then
                                                                                            objDB.Entry(itemDirectorIdentificationx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentificationx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objDirectorClassCek.listIdentification
                                                                                        Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If

                                                                            End If
                                                                        End If
                                                                    Next
                                                                End If
                                                            End If


                                                            If objModuledataTransaction.LisAccountSignatoryFrom.Count > 0 Then

                                                                For Each Signatoryx As goAML_Trn_acc_Signatory In (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = objModuledataTransaction.objAccountFrom.PK_Account_ID And x.FK_From_Or_To = 1).ToList
                                                                    Dim objSignatoryClassCek As SignatoryClass = objModuledataTransaction.LisAccountSignatoryFrom.Find(Function(x) x.objSignatory.PK_goAML_Trn_acc_Signatory_ID = Signatoryx.PK_goAML_Trn_acc_Signatory_ID And x.objSignatory.FK_From_Or_To = 1)
                                                                    Dim objSignatoryCek As goAML_Trn_acc_Signatory = objSignatoryClassCek.objSignatory

                                                                    'Delete
                                                                    If objSignatoryCek Is Nothing Then

                                                                        TransactionAccountSignatoryBLL.DeleteTransactionAccountSignatoryFrom(objModuledataTransaction, objATHeaderReport)
                                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Signatoryx)
                                                                        '---------------------Function All Sognatory Account Delete-----------------------------

                                                                    Else

                                                                        Dim TransactionAccountSignatory As goAML_Trn_acc_Signatory = objSignatoryClassCek.objSignatory
                                                                        Dim TransactionAccountSignatoryCek As goAML_Trn_acc_Signatory = (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = objModuledataTransaction.objAccountFrom.PK_Account_ID And x.FK_From_Or_To = 1).FirstOrDefault

                                                                        If TransactionAccountSignatoryCek Is Nothing Then

                                                                            TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryFrom(objModuledataTransaction, objATHeaderReport)
                                                                            For Each objModuledataSignatoryAccountFrom As SignatoryClass In objModuledataTransaction.LisAccountSignatoryFrom
                                                                                Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                                                                itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                                                                If objModuledataSignatoryAccountFrom.listPhoneSignatory.Count > 0 Then
                                                                                    For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataSignatoryAccountFrom.listAddressSignatory.Count > 0 Then
                                                                                    For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory.Count > 0 Then
                                                                                    For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataSignatoryAccountFrom.listAddressEmployerSignatory.Count > 0 Then
                                                                                    For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataSignatoryAccountFrom.listIdentification.Count > 0 Then
                                                                                    For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                                                                    Next
                                                                                End If
                                                                            Next

                                                                        Else

                                                                            objDB.Entry(TransactionAccountSignatoryCek).CurrentValues.SetValues(TransactionAccountSignatory)
                                                                            objDB.Entry(TransactionAccountSignatoryCek).State = EntityState.Modified
                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionAccountSignatory, TransactionAccountSignatoryCek)

                                                                            'Signatory Account Phone From
                                                                            'Delete
                                                                            For Each itemSignatoryPhonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                                Dim objCekDirectorPhone As goAML_trn_acc_sign_Phone = objSignatoryClassCek.listPhoneSignatory.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhonex.PK_goAML_trn_acc_sign_Phone And x.isEmployer = False)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemSignatoryPhone As goAML_trn_acc_sign_Phone In objSignatoryClassCek.listPhoneSignatory
                                                                                Dim objCek As goAML_trn_acc_sign_Phone = (From x In objDB.goAML_trn_acc_sign_Phone Where x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhone.PK_goAML_trn_acc_sign_Phone And x.isEmployer = False Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone, objCek)
                                                                                End If
                                                                            Next

                                                                            'Signatory Account Address From
                                                                            'Delete
                                                                            For Each itemSignatoryAddressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = False).ToList
                                                                                Dim objCekDirectorAddress As goAML_Trn_Acc_sign_Address = objSignatoryClassCek.listAddressSignatory.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddressx.PK_goAML_Trn_Acc_Entity_Address_ID And x.isEmployer = False)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemSignatoryAddress As goAML_Trn_Acc_sign_Address In objSignatoryClassCek.listAddressSignatory
                                                                                Dim objCek As goAML_Trn_Acc_sign_Address = (From x In objDB.goAML_Trn_Acc_sign_Address Where x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddress.PK_goAML_Trn_Acc_Entity_Address_ID And x.isEmployer = False Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress, objCek)
                                                                                End If
                                                                            Next

                                                                            'Signatory Account Employer Address From
                                                                            'Delete
                                                                            For Each itemSignatoryAddressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = True).ToList
                                                                                Dim objCekDirectorAddress As goAML_Trn_Acc_sign_Address = objSignatoryClassCek.listAddressEmployerSignatory.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddressx.PK_goAML_Trn_Acc_Entity_Address_ID And x.isEmployer = True)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemSignatoryAddress As goAML_Trn_Acc_sign_Address In objSignatoryClassCek.listAddressEmployerSignatory
                                                                                Dim objCek As goAML_Trn_Acc_sign_Address = (From x In objDB.goAML_Trn_Acc_sign_Address Where x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddress.PK_goAML_Trn_Acc_Entity_Address_ID And x.isEmployer = True Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress, objCek)
                                                                                End If
                                                                            Next

                                                                            'Signatory Account Employer Phone From
                                                                            'Delete
                                                                            For Each itemSignatoryPhonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = True).ToList
                                                                                Dim objCekDirectorPhone As goAML_trn_acc_sign_Phone = objSignatoryClassCek.listPhoneEmployerSignatory.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhonex.PK_goAML_trn_acc_sign_Phone And x.isEmployer = True)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemSignatoryPhone As goAML_trn_acc_sign_Phone In objSignatoryClassCek.listPhoneEmployerSignatory
                                                                                Dim objCek As goAML_trn_acc_sign_Phone = (From x In objDB.goAML_trn_acc_sign_Phone Where x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhone.PK_goAML_trn_acc_sign_Phone Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone, objCek)
                                                                                End If
                                                                            Next

                                                                            'Signatory Account Entity Identification From
                                                                            'Delete
                                                                            For Each itemSignatoryIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 1).ToList
                                                                                Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objSignatoryClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemSignatoryIdentificationx.PK_goAML_Transaction_Person_Identification_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 1)
                                                                                If objCekDirectorIdentification Is Nothing Then
                                                                                    objDB.Entry(itemSignatoryIdentificationx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryIdentificationx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objSignatoryClassCek.listIdentification
                                                                                Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                                                End If
                                                                            Next
                                                                        End If
                                                                    End If
                                                                Next
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 2 Then
                                                    Dim TransactionPerson As goAML_Transaction_Person = objModuledataTransaction.objPersonFrom
                                                    Dim itemx As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                    If TransactionPerson Is Nothing Then

                                                        TransactionPersonBLL.DeleteTransactionPersonFrom(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)
                                                        '--------------------------------FUnction All Transaction Person Delete----------------------------

                                                    Else

                                                        Dim TransactionPersonCek As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
                                                        If TransactionPersonCek Is Nothing Then

                                                            TransactionPersonBLL.InsertTransactionPersonFrom(objModuledataTransaction, objATHeaderReport)
                                                            Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                                                            ItemObjTransactionPersonFrom = objModuledataTransaction.objPersonFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonFrom)
                                                            If objModuledataTransaction.listObjPhonePersonFrom.Count > 0 Then
                                                                For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjAddressPersonFrom.Count > 0 Then
                                                                For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjPhoneEmployerPersonFrom.Count > 0 Then
                                                                For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjAddressEmployerPersonFrom.Count > 0 Then
                                                                For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjIdentificationPersonFrom.Count > 0 Then
                                                                For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                                                Next
                                                            End If

                                                        Else
                                                            objDB.Entry(TransactionPersonCek).CurrentValues.SetValues(TransactionPerson)
                                                            objDB.Entry(TransactionPersonCek).State = EntityState.Modified
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionPerson, TransactionPersonCek)

                                                            'Delete
                                                            For Each itemPersonPhonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = False Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_trn_Person_Phone = objModuledataTransaction.listObjPhonePersonFrom.Find(Function(x) x.PK_goAML_trn_Person_Phone = itemPersonPhonex.PK_goAML_trn_Person_Phone And x.isEmployer = False)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemPersonPhonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhonex)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonFrom
                                                                Dim objCek As goAML_trn_Person_Phone = (From x In objDB.goAML_trn_Person_Phone Where x.PK_goAML_trn_Person_Phone = itemPersonPhone.PK_goAML_trn_Person_Phone And x.isEmployer = False Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonPhone).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonAddressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = False Select x).ToList()
                                                                Dim objCekDirectorAddress As goAML_Trn_Person_Address = objModuledataTransaction.listObjAddressPersonFrom.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = itemPersonAddressx.PK_goAML_Trn_Person_Address_ID And x.isEmployer = False)
                                                                If objCekDirectorAddress Is Nothing Then
                                                                    objDB.Entry(itemPersonAddressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddressx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonFrom
                                                                Dim objCek As goAML_Trn_Person_Address = (From x In objDB.goAML_Trn_Person_Address Where x.PK_goAML_Trn_Person_Address_ID = itemPersonAddress.PK_goAML_Trn_Person_Address_ID And x.isEmployer = False Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonAddress).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonAddressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = True Select x).ToList()
                                                                Dim objCekDirectorAddress As goAML_Trn_Person_Address = objModuledataTransaction.listObjAddressEmployerPersonFrom.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = itemPersonAddressx.PK_goAML_Trn_Person_Address_ID And x.isEmployer = True)
                                                                If objCekDirectorAddress Is Nothing Then
                                                                    objDB.Entry(itemPersonAddressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddressx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonFrom
                                                                Dim objCek As goAML_Trn_Person_Address = (From x In objDB.goAML_Trn_Person_Address Where x.PK_goAML_Trn_Person_Address_ID = itemPersonAddress.PK_goAML_Trn_Person_Address_ID And x.isEmployer = True Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonAddress).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonPhonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = True Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_trn_Person_Phone = objModuledataTransaction.listObjPhoneEmployerPersonFrom.Find(Function(x) x.PK_goAML_trn_Person_Phone = itemPersonPhonex.PK_goAML_trn_Person_Phone And x.isEmployer = True)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemPersonPhonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhonex)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonFrom
                                                                Dim objCek As goAML_trn_Person_Phone = (From x In objDB.goAML_trn_Person_Phone Where x.PK_goAML_trn_Person_Phone = itemPersonPhone.PK_goAML_trn_Person_Phone And x.isEmployer = True Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonPhone).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemIPersondentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionPerson.PK_Person_ID And x.FK_Person_Type = 1 Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_Transaction_Person_Identification = objModuledataTransaction.listObjIdentificationPersonFrom.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemIPersondentificationx.PK_goAML_Transaction_Person_Identification_ID And x.FK_Person_Type = 1)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemIPersondentificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemIPersondentificationx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonFrom
                                                                Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemPersonIdentification.PK_goAML_Transaction_Person_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonIdentification).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonIdentification)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonIdentification)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorx, objCek)
                                                                End If
                                                            Next

                                                        End If
                                                    End If

                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 3 Then
                                                    Dim TransactionEntity As goAML_Transaction_Entity = objModuledataTransaction.objEntityFrom
                                                    Dim itemx As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1 Select x).FirstOrDefault
                                                    If TransactionEntity Is Nothing Then

                                                        TransactionEntityBLL.DeleteTransactionEntityFrom(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)
                                                        '----------------Function All Delete Transaction Entity-----------------------------

                                                    Else

                                                        Dim TransactionEntityCek As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
                                                        If TransactionEntityCek Is Nothing Then

                                                            TransactionEntityBLL.InsertTransactionEntityFrom(objModuledataTransaction, objATHeaderReport)
                                                            Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                                                            itemObjTransactionEntityFrom = objModuledataTransaction.objEntityFrom
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityFrom)

                                                            If objModuledataTransaction.listObjPhoneEntityFrom.Count > 0 Then
                                                                For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.ListObjAddressEntityFrom.Count > 0 Then
                                                                For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityFrom
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listDirectorEntityFrom.Count > 0 Then
                                                                TransactionEntityDirectorBLL.InsertTransactionEntityDirectorFrom(objModuledataTransaction, objATHeaderReport)
                                                                For Each objModuledataDirectorEntityFrom As DirectorClass In objModuledataTransaction.listDirectorEntityFrom
                                                                    Dim itemObjDirector As New goAML_Trn_Director
                                                                    itemObjDirector = objModuledataDirectorEntityFrom.objDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                    If objModuledataDirectorEntityFrom.listPhoneDirector.Count > 0 Then
                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityFrom.listAddressDirector.Count > 0 Then
                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityFrom.listAddressEmployerDirector.Count > 0 Then
                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityFrom.listIdentification.Count > 0 Then
                                                                        For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                        Next
                                                                    End If
                                                                Next


                                                            End If
                                                            '-------------Function All Transaction Entity Insert---------------------------

                                                        Else

                                                            objDB.Entry(TransactionEntityCek).CurrentValues.SetValues(TransactionEntity)
                                                            objDB.Entry(TransactionEntityCek).State = EntityState.Modified
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionEntity, TransactionEntityCek)

                                                            'Entity Phone From
                                                            'Delete
                                                            For Each itemEntityPhonex As goAML_Trn_Entity_Phone In (From x In objDB.goAML_Trn_Entity_Phone Where x.FK_Trn_Entity = TransactionEntity.PK_Entity_ID).ToList
                                                                Dim objCekEntityPhone As goAML_Trn_Entity_Phone = objModuledataTransaction.listObjPhoneEntityFrom.Find(Function(x) x.PK_goAML_Trn_Entity_Phone = itemEntityPhonex.PK_goAML_Trn_Entity_Phone)
                                                                If objCekEntityPhone Is Nothing Then
                                                                    objDB.Entry(itemEntityPhonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityPhonex)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemEntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityFrom
                                                                Dim objCek As goAML_Trn_Entity_Phone = (From x In objDB.goAML_Trn_Entity_Phone Where x.PK_goAML_Trn_Entity_Phone = itemEntityPhone.PK_goAML_Trn_Entity_Phone Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemEntityPhone).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityPhone)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemEntityPhone)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityPhone)
                                                                End If
                                                            Next


                                                            'Entity Address From
                                                            'Delete
                                                            For Each itemEntityAddressx As goAML_Trn_Entity_Address In (From x In objDB.goAML_Trn_Entity_Address Where x.FK_Trn_Entity = TransactionEntity.PK_Entity_ID).ToList
                                                                Dim objCekEntityAddress As goAML_Trn_Entity_Address = objModuledataTransaction.ListObjAddressEntityFrom.Find(Function(x) x.PK_goAML_Trn_Entity_Address_ID = itemEntityAddressx.PK_goAML_Trn_Entity_Address_ID)
                                                                If objCekEntityAddress Is Nothing Then
                                                                    objDB.Entry(itemEntityAddressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityAddressx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemEntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityFrom
                                                                Dim objCek As goAML_Trn_Entity_Address = (From x In objDB.goAML_Trn_Entity_Address Where x.PK_goAML_Trn_Entity_Address_ID = itemEntityAddress.PK_goAML_Trn_Entity_Address_ID Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemEntityAddress).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityAddress)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemEntityAddress)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemEntityAddress, objCek)
                                                                End If
                                                            Next


                                                            'Director Entity From
                                                            If Not objModuledataTransaction.listDirectorEntityFrom.Count > 0 Then

                                                                For Each Directorx As goAML_Trn_Director In (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = objModuledataTransaction.objEntityFrom.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 3).ToList
                                                                    Dim objDirectorClassCek As DirectorClass = objModuledataTransaction.listDirectorAccountEntityFrom.Find(Function(x) x.objDirector.PK_goAML_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.objDirector.FK_From_Or_To = 1 And x.objDirector.FK_Sender_Information = 3)
                                                                    Dim objDirectorCek As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                    If objDirectorCek Is Nothing Then

                                                                        TransactionEntityDirectorBLL.DeleteTransactionEntityDirectorFrom(objModuledataTransaction, objATHeaderReport)
                                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)
                                                                        '--------------------FUcntion All Transaction Account Entity Delete---------------------------

                                                                    Else

                                                                        Dim TransactionEntityDirector As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                        Dim TransactionEntityDirectorCek As goAML_Trn_Director = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemx.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 3).FirstOrDefault

                                                                        If TransactionEntityDirectorCek Is Nothing Then

                                                                            For Each objModuledataDirectorEntityFrom As DirectorClass In objModuledataTransaction.listDirectorEntityFrom
                                                                                Dim itemObjDirector As New goAML_Trn_Director
                                                                                itemObjDirector = objModuledataDirectorEntityFrom.objDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                                If objModuledataDirectorEntityFrom.listPhoneDirector.Count > 0 Then
                                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityFrom.listAddressDirector.Count > 0 Then
                                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityFrom.listPhoneEmployerDirector.Count > 0 Then
                                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityFrom.listAddressEmployerDirector.Count > 0 Then
                                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityFrom.listIdentification.Count > 0 Then
                                                                                    For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                    Next
                                                                                End If
                                                                            Next
                                                                            '-----------------------FUnction All Transaction Account Entity Director Insert-----

                                                                        Else
                                                                            objDB.Entry(TransactionEntityDirectorCek).CurrentValues.SetValues(TransactionEntityDirector)
                                                                            objDB.Entry(TransactionEntityDirectorCek).State = EntityState.Modified
                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionEntityDirector, TransactionEntityDirectorCek)

                                                                            'Director Entity Phone From
                                                                            'Delete
                                                                            For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneDirector
                                                                                Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.PK_goAML_trn_Director_Phone = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                End If
                                                                            Next

                                                                            'Director Entity Address From
                                                                            'Delete
                                                                            For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressDirector
                                                                                Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddress, objCek)
                                                                                End If
                                                                            Next

                                                                            'Director Entity Address Employer From
                                                                            'Delete
                                                                            For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressEmployerDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressEmployerDirector
                                                                                Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddress, objCek)
                                                                                End If
                                                                            Next

                                                                            'Director Entity Phone Employer From
                                                                            'Delete
                                                                            For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneEmployerDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneEmployerDirector
                                                                                Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                End If
                                                                            Next

                                                                            'Director Entity Identification From
                                                                            'Delete
                                                                            For Each itemDirectorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objDirectorClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemDirectorIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                                                If objCekDirectorIdentification Is Nothing Then
                                                                                    objDB.Entry(itemDirectorIdentificationx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentificationx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorIdentification As goAML_Transaction_Person_Identification In objDirectorClassCek.listIdentification
                                                                                Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemDirectorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorIdentification).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentification)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorIdentification)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentification, objCek)
                                                                                End If
                                                                            Next

                                                                        End If
                                                                    End If
                                                                Next
                                                            End If
                                                        End If
                                                    End If
                                                    '---------------------------------End Director Entity Transaction-----------------------------------------
                                                End If



                                                '----------------------------------------Penerima---------------------------------------------------------

                                                If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 1 Then

                                                    Dim TransactionAccount As goAML_Transaction_Account = objModuledataTransaction.objAccountTo
                                                    Dim itemx As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                    If TransactionAccount Is Nothing Then
                                                        objDB.Entry(itemx).State = EntityState.Deleted
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)
                                                        TransactionAccountBLL.DeleteTransactionAccountTo(objModuledataTransaction, objATHeaderReport)
                                                        '-------------------------------Function All Transaction Account Delete-----------------------

                                                    Else

                                                        Dim TransactionAccountCek As goAML_Transaction_Account = (From x In objDB.goAML_Transaction_Account Where x.FK_Report_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
                                                        If TransactionAccountCek Is Nothing Then

                                                            TransactionAccountBLL.InsertTransactionAccountTo(objModuledataTransaction, objATHeaderReport)
                                                            Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                                                            itemObjTransactionAccountTo = objModuledataTransaction.objAccountTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountTo)

                                                            If Not objModuledataTransaction.objAccountEntityTo Is Nothing Then
                                                                TransactionAccountEntityBLL.InsertTransactionAccountEntityTo(objModuledataTransaction, objATHeaderReport)
                                                                Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                                                                itemObjTrnEntityAccount = objModuledataTransaction.objAccountEntityTo
                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTrnEntityAccount)
                                                                If objModuledataTransaction.listObjPhoneAccountEntityto.Count > 0 Then
                                                                    For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityto
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                    Next
                                                                End If
                                                                If objModuledataTransaction.listObjAddressAccountEntityto.Count > 0 Then
                                                                    For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityto
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                    Next
                                                                End If
                                                                If objModuledataTransaction.listDirectorAccountEntityto.Count > 0 Then
                                                                    TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryTo(objModuledataTransaction, objATHeaderReport)
                                                                    For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto
                                                                        Dim itemObjDirector As New goAML_Trn_Director
                                                                        itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                        If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                            Next
                                                                        End If
                                                                        If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                                                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                            Next
                                                                        End If
                                                                    Next



                                                                End If
                                                            End If

                                                        Else
                                                            objDB.Entry(TransactionAccountCek).CurrentValues.SetValues(TransactionAccount)
                                                            objDB.Entry(TransactionAccountCek).State = EntityState.Modified
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionAccount, TransactionAccountCek)

                                                            Dim itemTransactionAccountEntityTox As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = TransactionAccount.PK_Account_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                            If objModuledataTransaction.objAccountEntityTo Is Nothing Then

                                                                TransactionAccountEntityBLL.DeleteTransactionAccountEntityTo(objModuledataTransaction, objATHeaderReport)
                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransactionAccountEntityTox)
                                                                '----------------------Function All Transaction Account Entity Delete-----------------

                                                            Else
                                                                Dim itemTransactionAccountEntityTo As goAML_Trn_Entity_account = objModuledataTransaction.objAccountEntityTo
                                                                Dim itemTransactionAccountEntityToCek As goAML_Trn_Entity_account = (From x In objDB.goAML_Trn_Entity_account Where x.FK_Account_ID = objModuledataTransaction.objAccountTo.PK_Account_ID And x.FK_From_Or_To = 2).FirstOrDefault

                                                                'Transaction Account Entity To
                                                                If itemTransactionAccountEntityToCek Is Nothing Then

                                                                    TransactionAccountEntityBLL.InsertTransactionAccountEntityTo(objModuledataTransaction, objATHeaderReport)
                                                                    Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                                                                    itemObjTrnEntityAccount = objModuledataTransaction.objAccountEntityTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTrnEntityAccount)
                                                                    If objModuledataTransaction.listObjPhoneAccountEntityto.Count > 0 Then
                                                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityto
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataTransaction.listObjAddressAccountEntityto.Count > 0 Then
                                                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityto
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataTransaction.listDirectorAccountEntityto.Count > 0 Then
                                                                        TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryTo(objModuledataTransaction, objATHeaderReport)
                                                                        For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto
                                                                            Dim itemObjDirector As New goAML_Trn_Director
                                                                            itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                            If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                                                                                For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                                                                                For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                                                                                For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                Next
                                                                            End If
                                                                        Next
                                                                    End If

                                                                Else

                                                                    objDB.Entry(itemTransactionAccountEntityToCek).CurrentValues.SetValues(itemTransactionAccountEntityTo)
                                                                    objDB.Entry(itemTransactionAccountEntityToCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemTransactionAccountEntityTo, itemTransactionAccountEntityToCek)

                                                                    'Director Entity Account To
                                                                    If objModuledataTransaction.listDirectorAccountEntityto.Count > 0 Then

                                                                        For Each Directorx As goAML_Trn_Director In (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = objModuledataTransaction.objAccountEntityTo.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 1).ToList
                                                                            Dim objDirectorClassCek As DirectorClass = objModuledataTransaction.listDirectorAccountEntityto.Find(Function(x) x.objDirector.PK_goAML_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.objDirector.FK_From_Or_To = 2 And x.objDirector.FK_Sender_Information = 1)
                                                                            Dim objDirectorCek As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                            If objDirectorCek Is Nothing Then
                                                                                objDB.Entry(Directorx).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)
                                                                                TransactionAccountEntityDirectorBLL.DeleteTransactionAccountEntityDirectoryTo(objModuledataTransaction, objATHeaderReport)
                                                                                '--------------------FUcntion All Transaction Account Entity Delete---------------------------

                                                                            Else

                                                                                Dim TransactionAccountEntityDirector As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                                Dim TransactionAccountEntityDirectorCek As goAML_Trn_Director = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemTransactionAccountEntityToCek.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 1).FirstOrDefault

                                                                                If TransactionAccountEntityDirectorCek Is Nothing Then

                                                                                    TransactionAccountEntityDirectorBLL.InsertTransactionAccountEntityDirectoryTo(objModuledataTransaction, objATHeaderReport)
                                                                                    For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto
                                                                                        Dim itemObjDirector As New goAML_Trn_Director
                                                                                        itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                                        If objModuledataDirectorEntityAccountTo.listPhoneDirector.Count > 0 Then
                                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                            Next
                                                                                        End If
                                                                                        If objModuledataDirectorEntityAccountTo.listAddressDirector.Count > 0 Then
                                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                            Next
                                                                                        End If
                                                                                        If objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector.Count > 0 Then
                                                                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                            Next
                                                                                        End If
                                                                                        If objModuledataDirectorEntityAccountTo.listAddressEmployerDirector.Count > 0 Then
                                                                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                            Next
                                                                                        End If
                                                                                        If objModuledataDirectorEntityAccountTo.listIdentification.Count > 0 Then
                                                                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                            Next
                                                                                        End If
                                                                                    Next
                                                                                    '-----------------------FUnction All Transaction Account Entity Director Insert-----

                                                                                Else
                                                                                    objDB.Entry(TransactionAccountEntityDirectorCek).CurrentValues.SetValues(TransactionAccountEntityDirector)
                                                                                    objDB.Entry(TransactionAccountEntityDirectorCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionAccountEntityDirector, TransactionAccountEntityDirectorCek)

                                                                                    'Director Account Entity Phone To
                                                                                    'Delete
                                                                                    For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                        If objCekDirectorPhone Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneDirector
                                                                                        Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.PK_goAML_trn_Director_Phone = itemConductorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorPhone).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorPhone)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone, objCek)
                                                                                        End If
                                                                                    Next

                                                                                    'Director Account Entity Address From
                                                                                    'Delete
                                                                                    For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                        If objCekDirectorAddress Is Nothing Then
                                                                                            objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressDirector
                                                                                        Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                        End If
                                                                                    Next


                                                                                    'Director Account Entity Address Employer From
                                                                                    'Delete
                                                                                    For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressEmployerDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                        If objCekDirectorAddress Is Nothing Then
                                                                                            objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressEmployerDirector
                                                                                        Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                        End If
                                                                                    Next

                                                                                    'Director Account Entity Phone Employer From
                                                                                    'Delete
                                                                                    For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneEmployerDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                        If objCekDirectorPhone Is Nothing Then
                                                                                            objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneEmployerDirector
                                                                                        Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = itemConductorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorPhone).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorPhone)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorPhone, objCek)
                                                                                        End If
                                                                                    Next

                                                                                    'Director Account Entity Identification From
                                                                                    'Delete
                                                                                    For Each itemDirectorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionAccountEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                        Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objDirectorClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemDirectorIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                                                        If objCekDirectorIdentification Is Nothing Then
                                                                                            objDB.Entry(itemDirectorIdentificationx).State = EntityState.Deleted
                                                                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentificationx)
                                                                                        End If
                                                                                    Next

                                                                                    'Add Modified
                                                                                    For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objDirectorClassCek.listIdentification
                                                                                        Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                                        If objCek Is Nothing Then
                                                                                            objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                                                        Else
                                                                                            objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                                            objDB.Entry(objCek).State = EntityState.Modified
                                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                                                        End If
                                                                                    Next

                                                                                End If
                                                                            End If
                                                                        Next
                                                                    End If
                                                                End If
                                                            End If

                                                            For Each Signatoryx As goAML_Trn_acc_Signatory In (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = objModuledataTransaction.objAccountTo.PK_Account_ID).ToList
                                                                Dim objSignatoryClassCek As SignatoryClass = objModuledataTransaction.LisAccountSignatoryTo.Find(Function(x) x.objSignatory.PK_goAML_Trn_acc_Signatory_ID = Signatoryx.PK_goAML_Trn_acc_Signatory_ID)
                                                                Dim objSignatoryCek As goAML_Trn_acc_Signatory = objSignatoryClassCek.objSignatory

                                                                'Delete
                                                                If objSignatoryCek Is Nothing Then
                                                                    objDB.Entry(Signatoryx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Signatoryx)
                                                                    TransactionAccountSignatoryBLL.DeleteTransactionAccountSignatoryTo(objModuledataTransaction, objATHeaderReport)
                                                                    '---------------------Function All Sognatory Account Delete-----------------------------

                                                                Else

                                                                    Dim TransactionAccountSignatory As goAML_Trn_acc_Signatory = objSignatoryClassCek.objSignatory
                                                                    Dim TransactionAccountSignatoryCek As goAML_Trn_acc_Signatory = (From x In objDB.goAML_Trn_acc_Signatory Where x.FK_Transaction_Account_ID = objModuledataTransaction.objAccountFrom.PK_Account_ID).FirstOrDefault

                                                                    If TransactionAccountSignatoryCek Is Nothing Then
                                                                        TransactionAccountSignatoryBLL.InsertTransactionAccountSignatoryTo(objModuledataTransaction, objATHeaderReport)
                                                                        For Each objModuledataSignatoryAccountTo As SignatoryClass In objModuledataTransaction.LisAccountSignatoryTo
                                                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                                                            itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                                                            If objModuledataSignatoryAccountTo.listPhoneSignatory.Count > 0 Then
                                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataSignatoryAccountTo.listAddressSignatory.Count > 0 Then
                                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataSignatoryAccountTo.listPhoneEmployerSignatory.Count > 0 Then
                                                                                For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                                                                Next
                                                                            End If
                                                                            If objModuledataSignatoryAccountTo.listAddressEmployerSignatory.Count > 0 Then
                                                                                For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                                                                Next
                                                                            End If
                                                                            If objModuledataSignatoryAccountTo.listIdentification.Count > 0 Then
                                                                                For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                                                                Next
                                                                            End If
                                                                        Next

                                                                    Else

                                                                        objDB.Entry(TransactionAccountSignatoryCek).CurrentValues.SetValues(TransactionAccountSignatory)
                                                                        objDB.Entry(TransactionAccountSignatoryCek).State = EntityState.Modified
                                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionAccountSignatory, TransactionAccountSignatoryCek)

                                                                        'Signatory Account Phone From
                                                                        'Delete
                                                                        For Each itemSignatoryPhonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID)
                                                                            Dim objCekDirectorPhone As goAML_trn_acc_sign_Phone = objSignatoryClassCek.listPhoneSignatory.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhonex.PK_goAML_trn_acc_sign_Phone)
                                                                            If objCekDirectorPhone Is Nothing Then
                                                                                objDB.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhonex)
                                                                            End If
                                                                        Next

                                                                        'Add Modified
                                                                        For Each itemSignatoryPhone As goAML_trn_acc_sign_Phone In objSignatoryClassCek.listPhoneSignatory
                                                                            Dim objCek As goAML_trn_acc_sign_Phone = (From x In objDB.goAML_trn_acc_sign_Phone Where x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhone.PK_goAML_trn_acc_sign_Phone Select x).FirstOrDefault
                                                                            If objCek Is Nothing Then
                                                                                objDB.Entry(itemSignatoryPhone).State = EntityState.Added
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone)
                                                                            Else
                                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone, objCek)
                                                                            End If
                                                                        Next

                                                                        'Signatory Account Address From
                                                                        'Delete
                                                                        For Each itemSignatoryAddressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID)
                                                                            Dim objCekDirectorAddress As goAML_Trn_Acc_sign_Address = objSignatoryClassCek.listAddressSignatory.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddressx.PK_goAML_Trn_Acc_Entity_Address_ID)
                                                                            If objCekDirectorAddress Is Nothing Then
                                                                                objDB.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddressx)
                                                                            End If
                                                                        Next

                                                                        'Add Modified
                                                                        For Each itemSignatoryAddress As goAML_Trn_Acc_sign_Address In objSignatoryClassCek.listAddressSignatory
                                                                            Dim objCek As goAML_Trn_Acc_sign_Address = (From x In objDB.goAML_Trn_Acc_sign_Address Where x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddress.PK_goAML_Trn_Acc_Entity_Address_ID Select x).FirstOrDefault
                                                                            If objCek Is Nothing Then
                                                                                objDB.Entry(itemSignatoryAddress).State = EntityState.Added
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress)
                                                                            Else
                                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress, objCek)
                                                                            End If
                                                                        Next

                                                                        'Signatory Account Employer Address From
                                                                        'Delete
                                                                        For Each itemSignatoryAddressx As goAML_Trn_Acc_sign_Address In (From x In objDB.goAML_Trn_Acc_sign_Address Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID)
                                                                            Dim objCekDirectorAddress As goAML_Trn_Acc_sign_Address = objSignatoryClassCek.listAddressEmployerSignatory.Find(Function(x) x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddressx.PK_goAML_Trn_Acc_Entity_Address_ID)
                                                                            If objCekDirectorAddress Is Nothing Then
                                                                                objDB.Entry(itemSignatoryAddressx).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddressx)
                                                                            End If
                                                                        Next

                                                                        'Add Modified
                                                                        For Each itemSignatoryAddress As goAML_Trn_Acc_sign_Address In objSignatoryClassCek.listAddressEmployerSignatory
                                                                            Dim objCek As goAML_Trn_Acc_sign_Address = (From x In objDB.goAML_Trn_Acc_sign_Address Where x.PK_goAML_Trn_Acc_Entity_Address_ID = itemSignatoryAddress.PK_goAML_Trn_Acc_Entity_Address_ID Select x).FirstOrDefault
                                                                            If objCek Is Nothing Then
                                                                                objDB.Entry(itemSignatoryAddress).State = EntityState.Added
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress)
                                                                            Else
                                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryAddress)
                                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryAddress, objCek)
                                                                            End If
                                                                        Next

                                                                        'Signatory Account Employer Phone From
                                                                        'Delete
                                                                        For Each itemSignatoryPhonex As goAML_trn_acc_sign_Phone In (From x In objDB.goAML_trn_acc_sign_Phone Where x.FK_Trn_Acc_Entity = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID)
                                                                            Dim objCekDirectorPhone As goAML_trn_acc_sign_Phone = objSignatoryClassCek.listPhoneEmployerSignatory.Find(Function(x) x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhonex.PK_goAML_trn_acc_sign_Phone)
                                                                            If objCekDirectorPhone Is Nothing Then
                                                                                objDB.Entry(itemSignatoryPhonex).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhonex)
                                                                            End If
                                                                        Next

                                                                        'Add Modified
                                                                        For Each itemSignatoryPhone As goAML_trn_acc_sign_Phone In objSignatoryClassCek.listPhoneEmployerSignatory
                                                                            Dim objCek As goAML_trn_acc_sign_Phone = (From x In objDB.goAML_trn_acc_sign_Phone Where x.PK_goAML_trn_acc_sign_Phone = itemSignatoryPhone.PK_goAML_trn_acc_sign_Phone Select x).FirstOrDefault
                                                                            If objCek Is Nothing Then
                                                                                objDB.Entry(itemSignatoryPhone).State = EntityState.Added
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone)
                                                                            Else
                                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemSignatoryPhone)
                                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryPhone, objCek)
                                                                            End If
                                                                        Next

                                                                        'Signatory Account Entity Identification From
                                                                        'Delete
                                                                        For Each itemSignatoryIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionAccountSignatory.PK_goAML_Trn_acc_Signatory_ID)
                                                                            Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objSignatoryClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemSignatoryIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                                            If objCekDirectorIdentification Is Nothing Then
                                                                                objDB.Entry(itemSignatoryIdentificationx).State = EntityState.Deleted
                                                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemSignatoryIdentificationx)
                                                                            End If
                                                                        Next

                                                                        'Add Modified
                                                                        For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objSignatoryClassCek.listIdentification
                                                                            Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                            If objCek Is Nothing Then
                                                                                objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                                            Else
                                                                                objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                                objDB.Entry(objCek).State = EntityState.Modified
                                                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                                            End If
                                                                        Next
                                                                    End If
                                                                End If
                                                            Next
                                                        End If
                                                    End If

                                                    '-----------------------------------End Signatory Account Transaction------------------------------------------

                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 2 Then
                                                    Dim TransactionPerson As goAML_Transaction_Person = objModuledataTransaction.objPersonTo
                                                    Dim itemx As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                    If TransactionPerson Is Nothing Then

                                                        TransactionPersonBLL.DeleteTransactionPersonTo(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)
                                                        '--------------------------------FUnction All Transaction Person Delete----------------------------

                                                    Else

                                                        Dim TransactionPersonCek As goAML_Transaction_Person = (From x In objDB.goAML_Transaction_Person Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
                                                        If TransactionPersonCek Is Nothing Then

                                                            TransactionPersonBLL.InsertTransactionPersonTo(objModuledataTransaction, objATHeaderReport)
                                                            Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                                                            ItemObjTransactionPersonTo = objModuledataTransaction.objPersonTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonTo)

                                                            If objModuledataTransaction.listObjPhonePersonTo.Count > 0 Then
                                                                For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjAddressPersonTo.Count > 0 Then
                                                                For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjPhoneEmployerPersonTo.Count > 0 Then
                                                                For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjAddressEmployerPersonTo.Count > 0 Then
                                                                For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listObjIdentificationPersonTo.Count > 0 Then
                                                                For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonTo
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                                                Next
                                                            End If

                                                        Else
                                                            objDB.Entry(TransactionPersonCek).CurrentValues.SetValues(TransactionPerson)
                                                            objDB.Entry(TransactionPersonCek).State = EntityState.Modified
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionPerson, TransactionPersonCek)

                                                            'Delete
                                                            For Each itemPersonPhonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = False Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_trn_Person_Phone = objModuledataTransaction.listObjPhonePersonTo.Find(Function(x) x.PK_goAML_trn_Person_Phone = itemPersonPhonex.PK_goAML_trn_Person_Phone And x.isEmployer = False)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemPersonPhonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhonex)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonTo
                                                                Dim objCek As goAML_trn_Person_Phone = (From x In objDB.goAML_trn_Person_Phone Where x.PK_goAML_trn_Person_Phone = itemPersonPhone.PK_goAML_trn_Person_Phone And x.isEmployer = False Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonPhone).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonAddressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = False Select x).ToList()
                                                                Dim objCekDirectorAddress As goAML_Trn_Person_Address = objModuledataTransaction.listObjAddressPersonTo.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = itemPersonAddressx.PK_goAML_Trn_Person_Address_ID And x.isEmployer = False)
                                                                If objCekDirectorAddress Is Nothing Then
                                                                    objDB.Entry(itemPersonAddressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddressx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonTo
                                                                Dim objCek As goAML_Trn_Person_Address = (From x In objDB.goAML_Trn_Person_Address Where x.PK_goAML_Trn_Person_Address_ID = itemPersonAddress.PK_goAML_Trn_Person_Address_ID And x.isEmployer = False Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonAddress).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonAddressx As goAML_Trn_Person_Address In (From x In objDB.goAML_Trn_Person_Address Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = True Select x).ToList()
                                                                Dim objCekDirectorAddress As goAML_Trn_Person_Address = objModuledataTransaction.listObjAddressEmployerPersonTo.Find(Function(x) x.PK_goAML_Trn_Person_Address_ID = itemPersonAddressx.PK_goAML_Trn_Person_Address_ID And x.isEmployer = True)
                                                                If objCekDirectorAddress Is Nothing Then
                                                                    objDB.Entry(itemPersonAddressx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddressx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonTo
                                                                Dim objCek As goAML_Trn_Person_Address = (From x In objDB.goAML_Trn_Person_Address Where x.PK_goAML_Trn_Person_Address_ID = itemPersonAddress.PK_goAML_Trn_Person_Address_ID And x.isEmployer = True Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonAddress).State = EntityState.Added
                                                                    objDB.SaveChanges()
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonAddress)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonAddress, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemPersonPhonex As goAML_trn_Person_Phone In (From x In objDB.goAML_trn_Person_Phone Where x.FK_Trn_Person = TransactionPerson.PK_Person_ID And x.isEmployer = True Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_trn_Person_Phone = objModuledataTransaction.listObjPhoneEmployerPersonTo.Find(Function(x) x.PK_goAML_trn_Person_Phone = itemPersonPhonex.PK_goAML_trn_Person_Phone And x.isEmployer = True)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemPersonPhonex).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhonex)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonTo
                                                                Dim objCek As goAML_trn_Person_Phone = (From x In objDB.goAML_trn_Person_Phone Where x.PK_goAML_trn_Person_Phone = itemPersonPhone.PK_goAML_trn_Person_Phone And x.isEmployer = True Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonPhone).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonPhone)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonPhone, objCek)
                                                                End If
                                                            Next


                                                            'Delete
                                                            For Each itemIPersondentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionPerson.PK_Person_ID And x.FK_Person_Type = 1 Select x).ToList()
                                                                Dim objCekDirectorPhone As goAML_Transaction_Person_Identification = objModuledataTransaction.listObjIdentificationPersonTo.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemIPersondentificationx.PK_goAML_Transaction_Person_Identification_ID And x.FK_Person_Type = 1)
                                                                If objCekDirectorPhone Is Nothing Then
                                                                    objDB.Entry(itemIPersondentificationx).State = EntityState.Deleted
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemIPersondentificationx)
                                                                End If
                                                            Next

                                                            'Add Modified
                                                            For Each itemPersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonTo
                                                                Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemPersonIdentification.PK_goAML_Transaction_Person_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                                                                If objCek Is Nothing Then
                                                                    objDB.Entry(itemPersonIdentification).State = EntityState.Added
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonIdentification)
                                                                Else
                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemPersonIdentification)
                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemPersonIdentification, objCek)
                                                                End If
                                                            Next

                                                        End If
                                                    End If

                                                End If

                                                If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 3 Then

                                                    Dim itemx As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2 Select x).FirstOrDefault
                                                    If objModuledataTransaction.objEntityTo Is Nothing Then
                                                        TransactionEntityBLL.DeleteTransactionEntityTo(objModuledataTransaction, objATHeaderReport)
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemx)

                                                        '----------------Function All Delete Transaction Entity-----------------------------

                                                    Else

                                                        Dim TransactionEntity As goAML_Transaction_Entity = objModuledataTransaction.objEntityTo
                                                        Dim TransactionEntityCek As goAML_Transaction_Entity = (From x In objDB.goAML_Transaction_Entity Where x.FK_Transaction_ID = objModuledataTransaction.objTransaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
                                                        If TransactionEntityCek Is Nothing Then

                                                            TransactionEntityBLL.InsertTransactionEntityTo(objModuledataTransaction, objATHeaderReport)
                                                            Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                                                            itemObjTransactionEntityTo = objModuledataTransaction.objEntityTo
                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityTo)

                                                            If objModuledataTransaction.listObjPhoneEntityto.Count > 0 Then
                                                                For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityto
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.ListObjAddressEntityto.Count > 0 Then
                                                                For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityto
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                                                Next
                                                            End If
                                                            If objModuledataTransaction.listDirectorEntityto.Count > 0 Then
                                                                TransactionEntityDirectorBLL.InsertTransactionEntityDirectorTo(objModuledataTransaction, objATHeaderReport)
                                                                For Each objModuledataDirectorEntityTo As DirectorClass In objModuledataTransaction.listDirectorEntityto
                                                                    Dim itemObjDirector As New goAML_Trn_Director
                                                                    itemObjDirector = objModuledataDirectorEntityTo.objDirector
                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                    If objModuledataDirectorEntityTo.listPhoneDirector.Count > 0 Then
                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityTo.listAddressDirector.Count > 0 Then
                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityTo.listPhoneEmployerDirector.Count > 0 Then
                                                                        For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityTo.listAddressEmployerDirector.Count > 0 Then
                                                                        For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                        Next
                                                                    End If
                                                                    If objModuledataDirectorEntityTo.listIdentification.Count > 0 Then
                                                                        For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                        Next
                                                                    End If
                                                                Next
                                                            End If

                                                            '-------------Function All Transaction Entity Insert---------------------------

                                                        Else

                                                            objDB.Entry(TransactionEntityCek).CurrentValues.SetValues(TransactionEntity)
                                                            objDB.Entry(TransactionEntityCek).State = EntityState.Modified

                                                            'Director Entity To
                                                            If Not objModuledataTransaction.listDirectorEntityFrom.Count > 0 Then

                                                                For Each Directorx As goAML_Trn_Director In (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = objModuledataTransaction.objEntityTo.PK_Entity_ID And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 3).ToList
                                                                    Dim objDirectorClassCek As DirectorClass = objModuledataTransaction.listDirectorAccountEntityto.Find(Function(x) x.objDirector.PK_goAML_Trn_Director_ID = Directorx.PK_goAML_Trn_Director_ID And x.objDirector.FK_From_Or_To = 2 And x.objDirector.FK_Sender_Information = 3)
                                                                    Dim objDirectorCek As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                    If objDirectorCek Is Nothing Then

                                                                        TransactionEntityDirectorBLL.DeleteTransactionEntityDirectorTo(objModuledataTransaction, objATHeaderReport)
                                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, Directorx)
                                                                        '--------------------FUcntion All Transaction Account Entity Delete---------------------------

                                                                    Else

                                                                        Dim TransactionEntityDirector As goAML_Trn_Director = objDirectorClassCek.objDirector
                                                                        Dim TransactionEntityDirectorCek As goAML_Trn_Director = (From x In objDB.goAML_Trn_Director Where x.FK_Entity_ID = itemx.PK_Entity_ID And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 3).FirstOrDefault

                                                                        If TransactionEntityDirectorCek Is Nothing Then

                                                                            TransactionEntityDirectorBLL.InsertTransactionEntityDirectorTo(objModuledataTransaction, objATHeaderReport)
                                                                            For Each objModuledataDirectorEntityTo As DirectorClass In objModuledataTransaction.listDirectorEntityto
                                                                                Dim itemObjDirector As New goAML_Trn_Director
                                                                                itemObjDirector = objModuledataDirectorEntityTo.objDirector
                                                                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                                                                If objModuledataDirectorEntityTo.listPhoneDirector.Count > 0 Then
                                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityTo.listAddressDirector.Count > 0 Then
                                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityTo.listPhoneEmployerDirector.Count > 0 Then
                                                                                    For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityTo.listAddressEmployerDirector.Count > 0 Then
                                                                                    For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                                                                    Next
                                                                                End If
                                                                                If objModuledataDirectorEntityTo.listIdentification.Count > 0 Then
                                                                                    For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                                                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                                                                    Next
                                                                                End If
                                                                            Next

                                                                            '-----------------------FUnction All Transaction Account Entity Director Insert-----

                                                                        Else
                                                                            objDB.Entry(TransactionEntityDirectorCek).CurrentValues.SetValues(TransactionEntityDirector)
                                                                            objDB.Entry(TransactionEntityDirectorCek).State = EntityState.Modified
                                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, TransactionEntityDirector, TransactionEntityDirectorCek)

                                                                            'Director Entity Phone From
                                                                            For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneDirector
                                                                                Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.PK_goAML_trn_Director_Phone = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                End If
                                                                            Next


                                                                            'Director Entity Address From
                                                                            'Delete
                                                                            For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressDirector
                                                                                Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                End If
                                                                            Next


                                                                            'Director Entity Address Employer From
                                                                            'Delete
                                                                            For Each itemDirectorAddressx As goAML_Trn_Director_Address In (From x In objDB.goAML_Trn_Director_Address Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorAddress As goAML_Trn_Director_Address = objDirectorClassCek.listAddressEmployerDirector.Find(Function(x) x.PK_goAML_Trn_Director_Address_ID = itemDirectorAddressx.PK_goAML_Trn_Director_Address_ID)
                                                                                If objCekDirectorAddress Is Nothing Then
                                                                                    objDB.Entry(itemDirectorAddressx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorAddressx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemConductorAddress As goAML_Trn_Director_Address In objDirectorClassCek.listAddressEmployerDirector
                                                                                Dim objCek As goAML_Trn_Director_Address = (From x In objDB.goAML_Trn_Director_Address Where x.PK_goAML_Trn_Director_Address_ID = itemConductorAddress.PK_goAML_Trn_Director_Address_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemConductorAddress).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemConductorAddress)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorAddress, objCek)
                                                                                End If
                                                                            Next


                                                                            'Director Entity Phone Employer From
                                                                            'Delete
                                                                            For Each itemDirectorPhonex As goAML_trn_Director_Phone In (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorPhone As goAML_trn_Director_Phone = objDirectorClassCek.listPhoneEmployerDirector.Find(Function(x) x.PK_goAML_trn_Director_Phone = itemDirectorPhonex.PK_goAML_trn_Director_Phone)
                                                                                If objCekDirectorPhone Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhonex).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhonex)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemDirectorPhone As goAML_trn_Director_Phone In objDirectorClassCek.listPhoneEmployerDirector
                                                                                Dim objCek As goAML_trn_Director_Phone = (From x In objDB.goAML_trn_Director_Phone Where x.FK_Trn_Director_ID = itemDirectorPhone.PK_goAML_trn_Director_Phone Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemDirectorPhone).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemDirectorPhone)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorPhone, objCek)
                                                                                End If
                                                                            Next


                                                                            'Director Entity Identification From
                                                                            'Delete
                                                                            For Each itemDirectorIdentificationx As goAML_Transaction_Person_Identification In (From x In objDB.goAML_Transaction_Person_Identification Where x.FK_Person_ID = TransactionEntityDirector.PK_goAML_Trn_Director_ID)
                                                                                Dim objCekDirectorIdentification As goAML_Transaction_Person_Identification = objDirectorClassCek.listIdentification.Find(Function(x) x.PK_goAML_Transaction_Person_Identification_ID = itemDirectorIdentificationx.PK_goAML_Transaction_Person_Identification_ID)
                                                                                If objCekDirectorIdentification Is Nothing Then
                                                                                    objDB.Entry(itemDirectorIdentificationx).State = EntityState.Deleted
                                                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemDirectorIdentificationx)
                                                                                End If
                                                                            Next

                                                                            'Add Modified
                                                                            For Each itemConductorIdentification As goAML_Transaction_Person_Identification In objDirectorClassCek.listIdentification
                                                                                Dim objCek As goAML_Transaction_Person_Identification = (From x In objDB.goAML_Transaction_Person_Identification Where x.PK_goAML_Transaction_Person_Identification_ID = itemConductorIdentification.PK_goAML_Transaction_Person_Identification_ID Select x).FirstOrDefault
                                                                                If objCek Is Nothing Then
                                                                                    objDB.Entry(itemConductorIdentification).State = EntityState.Added
                                                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification)
                                                                                Else
                                                                                    objDB.Entry(objCek).CurrentValues.SetValues(itemConductorIdentification)
                                                                                    objDB.Entry(objCek).State = EntityState.Modified
                                                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemConductorIdentification, objCek)
                                                                                End If
                                                                            Next

                                                                        End If
                                                                    End If
                                                                Next
                                                            End If
                                                        End If
                                                    End If
                                                    '---------------------------------End Director Entity Transaction-----------------------------------------

                                                End If

                                            Else

                                                TransactionPartyBLL.UpdateTransactionParty(objModuledataTransaction, objATHeaderReport)

                                                Dim itemObjTransactionParty As New goAML_Transaction_Party
                                                itemObjTransactionParty = objModuledataTransaction.objTransactionParty
                                                Dim TransactionPartyCek As goAML_Transaction_Party = (From x In objDB.goAML_Transaction_Party Where x.PK_Trn_Party_ID = itemObjTransactionParty.PK_Trn_Party_ID Select x).FirstOrDefault
                                                NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionParty, TransactionPartyCek)

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 1 Then

                                                    Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                                                    itemObjTransactionAccountParty = objModuledataTransaction.objAccountParty
                                                    Dim itemObjTransactionAccountPartyCek As goAML_Trn_Party_Account = (From x In objDB.goAML_Trn_Party_Account Where x.PK_Trn_Party_Account_ID = itemObjTransactionAccountParty.PK_Trn_Party_Account_ID Select x).FirstOrDefault
                                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountParty, itemObjTransactionAccountPartyCek)

                                                    Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                                                    itemObjTransactionAccountEntityParty = objModuledataTransaction.objAccountEntityParty
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityParty)

                                                    For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In objModuledataTransaction.listPhoneAccountEntityParty
                                                        Dim EntityPhonePartyCek As goAML_trn_par_acc_Entity_Phone = (From x In objDB.goAML_trn_par_acc_Entity_Phone Where x.PK_trn_Par_Acc_Entity_Phone_ID = EntityPhoneParty.PK_trn_Par_Acc_Entity_Phone_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty, EntityPhonePartyCek)
                                                    Next

                                                    For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In objModuledataTransaction.listAddresAccountEntityParty
                                                        Dim EntityAddressPartyCek As goAML_Trn_par_Acc_Entity_Address = (From x In objDB.goAML_Trn_par_Acc_Entity_Address Where x.PK_Trn_Par_Acc_Entity_Address_ID = EntityAddressParty.PK_Trn_Par_Acc_Entity_Address_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty, EntityAddressPartyCek)
                                                    Next

                                                    For Each objModuledataDirectorEntityAccountParty As DirectorEntityAccountPartyClass In objModuledataTransaction.listDirectorEntytAccountparty
                                                        Dim itemObjDirectorParty As New goAML_Trn_Par_Acc_Ent_Director
                                                        itemObjDirectorParty = objModuledataDirectorEntityAccountParty.objDirectorEntityAccountParty
                                                        Dim itemObjDirectorPartyCek As goAML_Trn_Par_Acc_Ent_Director = (From x In objDB.goAML_Trn_Par_Acc_Ent_Director Where x.PK_Trn_Par_Acc_Ent_Director_ID = itemObjDirectorParty.PK_Trn_Par_Acc_Ent_Director_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty, itemObjDirectorPartyCek)

                                                        For Each DirectorPhone As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhone
                                                            Dim DirectorPhoneCek As goAML_Trn_Par_Acc_Ent_Director_Phone = (From x In objDB.goAML_Trn_Par_Acc_Ent_Director_Phone Where x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = DirectorPhone.PK_Trn_Par_Acc_Ent_Director_Phone_ID And x.isemployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone, DirectorPhoneCek)
                                                        Next

                                                        For Each DirectorAddress As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddress
                                                            Dim DirectorAddressCek As goAML_Trn_Par_Acc_Ent_Director_Address = (From x In objDB.goAML_Trn_Par_Acc_Ent_Director_Address Where x.PK_Trn_Par_Acc_Entity_Address_ID = DirectorAddress.PK_Trn_Par_Acc_Entity_Address_ID And x.isemployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress, DirectorAddressCek)
                                                        Next

                                                        For Each DirectorAddressEmployer As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddressEmployer
                                                            Dim DirectorAddressEmployerCek As goAML_Trn_Par_Acc_Ent_Director_Address = (From x In objDB.goAML_Trn_Par_Acc_Ent_Director_Address Where x.PK_Trn_Par_Acc_Entity_Address_ID = DirectorAddressEmployer.PK_Trn_Par_Acc_Entity_Address_ID And x.isemployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer, DirectorAddressEmployerCek)
                                                        Next

                                                        For Each DirectorPhoneEmployer As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhoneEmployer
                                                            Dim DirectorPhoneEmployerCek As goAML_Trn_Par_Acc_Ent_Director_Phone = (From x In objDB.goAML_Trn_Par_Acc_Ent_Director_Phone Where x.PK_Trn_Par_Acc_Ent_Director_Phone_ID = DirectorPhoneEmployer.PK_Trn_Par_Acc_Ent_Director_Phone_ID And x.isemployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer, DirectorPhoneEmployerCek)
                                                        Next

                                                        For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityAccountParty.listIdentification
                                                            Dim DirectorIdentificationCek As goAML_Transaction_Party_Identification = (From x In objDB.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = DirectorIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 4 Select x).FirstOrDefault

                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification, DirectorIdentificationCek)
                                                        Next
                                                    Next

                                                    For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In objModuledataTransaction.listSignatoryAccountParty
                                                        Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                                                        itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty
                                                        Dim itemObjSignatoryPartyCek As goAML_Trn_par_acc_Signatory = (From x In objDB.goAML_Trn_par_acc_Signatory Where x.PK_Trn_par_acc_Signatory_ID = itemObjSignatoryParty.PK_Trn_par_acc_Signatory_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatoryParty, itemObjSignatoryPartyCek)

                                                        For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                                            Dim SignatoryPhoneCek As goAML_trn_par_acc_sign_Phone = (From x In objDB.goAML_trn_par_acc_sign_Phone Where x.PK_trn_Par_Acc_Sign_Phone = SignatoryPhone.PK_trn_Par_Acc_Sign_Phone And x.isEmployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhone, SignatoryPhoneCek)
                                                        Next

                                                        For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                                            Dim SignatoryAddressCek As goAML_Trn_par_Acc_sign_Address = (From x In objDB.goAML_Trn_par_Acc_sign_Address Where x.PK_Trn_Par_Acc_sign_Address_ID = SignatoryAddress.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddress, SignatoryAddressCek)
                                                        Next

                                                        For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                                            Dim SignatoryAddressEmployerCek As goAML_Trn_par_Acc_sign_Address = (From x In objDB.goAML_Trn_par_Acc_sign_Address Where x.PK_Trn_Par_Acc_sign_Address_ID = SignatoryAddressEmployer.PK_Trn_Par_Acc_sign_Address_ID And x.isEmployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddressEmployer, SignatoryAddressEmployerCek)
                                                        Next

                                                        For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                                            Dim SignatoryPhoneEmployerCek As goAML_trn_par_acc_sign_Phone = (From x In objDB.goAML_trn_par_acc_sign_Phone Where x.PK_trn_Par_Acc_Sign_Phone = SignatoryPhoneEmployer.PK_trn_Par_Acc_Sign_Phone And x.isEmployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhoneEmployer, SignatoryPhoneEmployerCek)
                                                        Next

                                                        For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                                            Dim SignatoryIdentificationCek As goAML_Transaction_Party_Identification = (From x In objDB.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = SignatoryIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 2 Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryIdentification, SignatoryIdentificationCek)
                                                        Next
                                                    Next

                                                End If

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 2 Then
                                                    Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                                                    ItemObjTransactionPartyPerson = objModuledataTransaction.objPersonParty
                                                    Dim ItemObjTransactionPartyPersonCek As goAML_Trn_Party_Person = (From x In objDB.goAML_Trn_Party_Person Where x.PK_Trn_Party_Person_ID = ItemObjTransactionPartyPerson.PK_Trn_Party_Person_ID Select x).FirstOrDefault
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPartyPerson)

                                                    For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhonePersonParty
                                                        Dim PersonPhoneCek As goAML_Trn_Party_Person_Phone = (From x In objDB.goAML_Trn_Party_Person_Phone Where x.PK_Trn_Party_Person_Phone_ID = PersonPhone.PK_Trn_Party_Person_Phone_ID And x.isemployer = False Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone, PersonPhoneCek)
                                                    Next

                                                    For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressPersonParty
                                                        Dim PersonAddressCek As goAML_Trn_Party_Person_Address = (From x In objDB.goAML_Trn_Party_Person_Address Where x.PK_Trn_Party_Person_Address_ID = PersonAddress.PK_Trn_Party_Person_Address_ID And x.isemployer = False Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress, PersonAddressCek)
                                                    Next

                                                    For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressEmployerPersonParty
                                                        Dim PersonAddressCek As goAML_Trn_Party_Person_Address = (From x In objDB.goAML_Trn_Party_Person_Address Where x.PK_Trn_Party_Person_Address_ID = PersonAddress.PK_Trn_Party_Person_Address_ID And x.isemployer = True Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress, PersonAddressCek)
                                                    Next

                                                    For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhoneEmployerPersonParty
                                                        Dim PersonPhoneCek As goAML_Trn_Party_Person_Phone = (From x In objDB.goAML_Trn_Party_Person_Phone Where x.PK_Trn_Party_Person_Phone_ID = PersonPhone.PK_Trn_Party_Person_Phone_ID And x.isemployer = True Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone, PersonPhoneCek)
                                                    Next

                                                    For Each PersonIdentification As goAML_Transaction_Party_Identification In objModuledataTransaction.listIdentificationPersonParty
                                                        Dim PersonIdentificationCek As goAML_Transaction_Party_Identification = (From x In objDB.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = PersonIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 1 Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification, PersonIdentificationCek)
                                                    Next

                                                End If

                                                If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 3 Then

                                                    Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                                                    itemObjTransactionEntityParty = objModuledataTransaction.objentityParty
                                                    Dim itemObjTransactionEntityPartyCek As goAML_Trn_Party_Entity = (From x In objDB.goAML_Trn_Party_Entity Where x.PK_Trn_Party_Entity_ID = itemObjTransactionEntityParty.PK_Trn_Party_Entity_ID Select x).FirstOrDefault
                                                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityParty)

                                                    For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In objModuledataTransaction.listPhoneEntityParty
                                                        Dim EntityPhonePartyCek As goAML_Trn_Party_Entity_Phone = (From x In objDB.goAML_Trn_Party_Entity_Phone Where x.PK_Trn_Party_Entity_Phone_ID = EntityPhoneParty.PK_Trn_Party_Entity_Phone_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty, EntityPhonePartyCek)
                                                    Next

                                                    For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In objModuledataTransaction.listAddressEntityparty
                                                        Dim EntityAddressPartyCek As goAML_Trn_Party_Entity_Address = (From x In objDB.goAML_Trn_Party_Entity_Address Where x.PK_Trn_Party_Entity_Address_ID = EntityAddressParty.PK_Trn_Party_Entity_Address_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty, EntityAddressPartyCek)
                                                    Next

                                                    For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In objModuledataTransaction.listDirectorEntityParty
                                                        Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                                                        itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty
                                                        Dim itemObjDirectorPartyCek As goAML_Trn_Par_Entity_Director = (From x In objDB.goAML_Trn_Par_Entity_Director Where x.PK_Trn_Par_Entity_Director_ID = itemObjDirectorParty.PK_Trn_Par_Entity_Director_ID Select x).FirstOrDefault
                                                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty, itemObjDirectorPartyCek)

                                                        For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                                            Dim DirectorPhoneCek As goAML_Trn_Par_Entity_Director_Phone = (From x In objDB.goAML_Trn_Par_Entity_Director_Phone Where x.PK_Trn_Par_Entity_Director_Phone_ID = DirectorPhone.PK_Trn_Par_Entity_Director_Phone_ID And x.isemployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone, DirectorPhoneCek)
                                                        Next

                                                        For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                                            Dim DirectorAddressCek As goAML_Trn_Par_Entity_Director_Address = (From x In objDB.goAML_Trn_Par_Entity_Director_Address Where x.PK_Trn_Par_Entity_Address_ID = DirectorAddress.PK_Trn_Par_Entity_Address_ID And x.isemployer = False Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress, DirectorAddressCek)
                                                        Next

                                                        For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                                            Dim DirectorAddressEmployerCek As goAML_Trn_Par_Entity_Director_Address = (From x In objDB.goAML_Trn_Par_Entity_Director_Address Where x.PK_Trn_Par_Entity_Address_ID = DirectorAddressEmployer.PK_Trn_Par_Entity_Address_ID And x.isemployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer, DirectorAddressEmployerCek)
                                                        Next

                                                        For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                                            Dim DirectorPhoneEmployerCek As goAML_Trn_Par_Entity_Director_Phone = (From x In objDB.goAML_Trn_Par_Entity_Director_Phone Where x.PK_Trn_Par_Entity_Director_Phone_ID = DirectorPhoneEmployer.PK_Trn_Par_Entity_Director_Phone_ID And x.isemployer = True Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer, DirectorPhoneEmployerCek)
                                                        Next

                                                        For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                                            Dim DirectorIdentificationCek As goAML_Transaction_Party_Identification = (From x In objDB.goAML_Transaction_Party_Identification Where x.PK_Transaction_Party_Identification_ID = DirectorIdentification.PK_Transaction_Party_Identification_ID And x.FK_Person_Type = 4 Select x).FirstOrDefault
                                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification, DirectorIdentificationCek)
                                                        Next

                                                    Next

                                                End If

                                            End If
                                        End If
                                    Next

                                    '---------------------------------------------------------end Transaction------------------------------------------------------------

                                Next

                            End If

                            If objModuledataReport.listIndicator.Count > 0 Then
                                For Each objModuledataReportIndicator As goAML_Report_Indicator In objModuledataReport.listIndicator
                                    Dim itemObjIndicatorx As goAML_Report_Indicator = (From x In objDB.goAML_Report_Indicator Where x.FK_Report = objModuledataReport.objReport.PK_Report_ID Select x).FirstOrDefault
                                    Dim objIndik As goAML_Report_Indicator = objModuledataReport.listIndicator.Find(Function(x) x.PK_Report_Indicator = itemObjIndicatorx.PK_Report_Indicator)
                                    If objIndik Is Nothing Then
                                        objDB.Entry(itemObjIndicatorx).State = EntityState.Deleted
                                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjIndicatorx)
                                    Else
                                        Dim IndikatorCek As goAML_Report_Indicator = (From x In objDB.goAML_Report_Indicator Where x.FK_Report = objModuledataReport.objReport.PK_Report_ID).FirstOrDefault
                                        If IndikatorCek Is Nothing Then
                                            With objModuledataReportIndicator
                                                .FK_Report = objModuledataReport.objReport.PK_Report_ID
                                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                                .ApprovedDate = Now
                                            End With
                                            objDB.Entry(objModuledataReportIndicator).State = EntityState.Added
                                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReportIndicator)
                                        Else
                                            objDB.Entry(IndikatorCek).CurrentValues.SetValues(objModuledataReportIndicator)
                                            objDB.Entry(IndikatorCek).State = EntityState.Modified
                                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReportIndicator, IndikatorCek)
                                        End If
                                    End If

                                    objDB.SaveChanges()
                                Next
                            End If


                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Accept, ReportRemarkBLL.actionForm.Edit, objModuledataReport.objReport.PK_Report_ID, note)

                            '--------------------------------------End Transaction-------------------------------------------------------

                        Case Common.ModuleActionEnum.Delete

                            '----------------------------------Report-----------------------------------------------------------
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Deleted

                            GenerateSarBLL.UpdateStatusGenerateSAR(objModuledataReport.objReport.PK_Report_ID)

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport)
                            objDB.SaveChanges()
                            '------------------------------end Report-----------------------------------------------------------------------------'


                            '--------------------------------------------Transaction----------------------------------------------------------------'

                            For Each objModuledataTransaction As Transaction In objModuledataReport.listObjTransaction
                                Dim itemObjTransaction As New goAML_Transaction
                                itemObjTransaction = objModuledataTransaction.objTransaction
                                objDB.Entry(itemObjTransaction).State = EntityState.Deleted
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransaction)

                                If objModuledataTransaction.objTransaction.FK_Transaction_Type = 1 Then

                                    If Not objModuledataTransaction.ObjConductor.PK_goAML_Trn_Conductor_ID = 0 Then
                                        TransactionConductorBLL.DeleteTransactionConductor(objModuledataTransaction, objATHeaderReport)

                                        Dim itemObjConductor As New goAML_Trn_Conductor
                                        itemObjConductor = objModuledataTransaction.ObjConductor
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjConductor)

                                        For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                        Next

                                        For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                        Next

                                        For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressEmployerConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                        Next

                                        For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneEmployerConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                        Next

                                        For Each ConductorIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorIdentification)
                                        Next
                                    End If

                                    '-------------------------------------Pengirim----------------------------------------------------------

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 1 Then

                                        TransactionAccountBLL.DeleteTransactionAccountFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                                        itemObjTransactionAccountFrom = objModuledataTransaction.objAccountFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountFrom)

                                        '------------------------------------------------------------ Transaction Account Entity From -----------------------------------------

                                        Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                                        itemObjTransactionAccountEntityFrom = objModuledataTransaction.objAccountEntityFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityFrom)

                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataDirectorEntityAccountFrom)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                        '    '------------------------End Director Entity Account Transaction From---------------------------------------
                                        ''---------------------------------------- end Transaction Account Entity From --------------------------------------------------------------

                                        ''----------------------------Signatory Account Transaction From--------------------------------------------------
                                        For Each objModuledataSignatoryAccountFrom As SignatoryClass In objModuledataTransaction.LisAccountSignatoryFrom

                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                            itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                            Next
                                        Next
                                        ''----------------------------End Signatory Account Transaction From--------------------------------------------------
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 2 Then
                                        TransactionPersonBLL.DeleteTransactionPersonFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                                        ItemObjTransactionPersonFrom = objModuledataTransaction.objPersonFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonFrom)
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 3 Then
                                        TransactionEntityBLL.DeleteTransactionEntityFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                                        itemObjTransactionEntityFrom = objModuledataTransaction.objEntityFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityFrom)
                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityFrom As DirectorClass In objModuledataTransaction.listDirectorEntityFrom

                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityFrom.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                    End If

                                    '----------------------------------------Penerima---------------------------------------------------------

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 1 Then
                                        TransactionAccountBLL.DeleteTransactionAccountTo(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                                        itemObjTransactionAccountTo = objModuledataTransaction.objAccountTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountTo)

                                        Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                                        itemObjTrnEntityAccount = objModuledataTransaction.objAccountEntityTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTrnEntityAccount)

                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto

                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                        For Each objModuledataSignatoryAccountTo As SignatoryClass In objModuledataTransaction.LisAccountSignatoryTo

                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                            itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)

                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                            Next
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 2 Then
                                        TransactionPersonBLL.DeleteTransactionPersonTo(objModuledataTransaction, objATHeaderReport)
                                        Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                                        ItemObjTransactionPersonTo = objModuledataTransaction.objPersonTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonTo)

                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 3 Then
                                        TransactionEntityBLL.DeleteTransactionEntityTo(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                                        itemObjTransactionEntityTo = objModuledataTransaction.objEntityTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityTo)

                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityTo As DirectorClass In objModuledataTransaction.listDirectorEntityto
                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityTo.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                    End If
                                Else

                                    TransactionPartyBLL.DeleteTransactionParty(objModuledataTransaction, objATHeaderReport)

                                    Dim itemObjTransactionParty As New goAML_Transaction_Party
                                    itemObjTransactionParty = objModuledataTransaction.objTransactionParty
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionParty)

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 1 Then

                                        Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                                        itemObjTransactionAccountParty = objModuledataTransaction.objAccountParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountParty)

                                        Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                                        itemObjTransactionAccountEntityParty = objModuledataTransaction.objAccountEntityParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityParty)

                                        For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In objModuledataTransaction.listPhoneAccountEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                        Next

                                        For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In objModuledataTransaction.listAddresAccountEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                        Next

                                        For Each objModuledataDirectorEntityAccountParty As DirectorEntityAccountPartyClass In objModuledataTransaction.listDirectorEntytAccountparty
                                            Dim itemObjDirectorParty As New goAML_Trn_Par_Acc_Ent_Director
                                            itemObjDirectorParty = objModuledataDirectorEntityAccountParty.objDirectorEntityAccountParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                            For Each DirectorPhone As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next

                                            For Each DirectorAddress As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next

                                            For Each DirectorAddressEmployer As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next

                                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next

                                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityAccountParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next

                                        For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In objModuledataTransaction.listSignatoryAccountParty
                                            Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                                            itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatoryParty)

                                            For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhone)
                                            Next

                                            For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddress)
                                            Next

                                            For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddressEmployer)
                                            Next

                                            For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhoneEmployer)
                                            Next

                                            For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryIdentification)
                                            Next
                                        Next

                                    End If

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 2 Then
                                        Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                                        ItemObjTransactionPartyPerson = objModuledataTransaction.objPersonParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPartyPerson)

                                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhonePersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next

                                        For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next

                                        For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressEmployerPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next

                                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhoneEmployerPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next

                                        For Each PersonIdentification As goAML_Transaction_Party_Identification In objModuledataTransaction.listIdentificationPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next

                                    End If

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 3 Then

                                        Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                                        itemObjTransactionEntityParty = objModuledataTransaction.objentityParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityParty)

                                        For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In objModuledataTransaction.listPhoneEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                        Next

                                        For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In objModuledataTransaction.listAddressEntityparty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                        Next

                                        For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In objModuledataTransaction.listDirectorEntityParty
                                            Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                                            itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                            For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next

                                            For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next

                                            For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next

                                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next

                                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next

                                        Next

                                    End If

                                End If
                                '---------------------------------------------------------end Transaction------------------------------------------------------------
                            Next

                            For Each objModuledataReportIndicator As goAML_Report_Indicator In objModuledataReport.listIndicator
                                Dim itemObjReportIndicator As New goAML_Report_Indicator
                                itemObjReportIndicator = objModuledataReportIndicator
                                objDB.Entry(itemObjReportIndicator).State = EntityState.Deleted
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjReportIndicator)
                            Next

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Accept, ReportRemarkBLL.actionForm.Delete, objModuledataReport.objReport.PK_Report_ID, note)

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Sub Revise(pK_ModuleApproval_ID As String, note As String)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()

                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = x.ModuleName).FirstOrDefault()
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            Dim objReport As GoAML_Report = (From x In objDB.GoAML_Report Where x.PK_Report_ID = objModuledataReport.objReport.PK_Report_ID).FirstOrDefault

                            With objReport
                                .status = 5
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objReport).State = EntityState.Modified

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport, objReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Revise, ReportRemarkBLL.actionForm.Add, objModuledataReport.objReport.PK_Report_ID, note)
                            objDB.SaveChanges()

                        Case Common.ModuleActionEnum.Update
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            Dim objReport As GoAML_Report = (From x In objDB.GoAML_Report Where x.PK_Report_ID = objModuledataReport.objReport.PK_Report_ID).FirstOrDefault

                            With objReport
                                .status = 5
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objReport).State = EntityState.Modified

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport, objReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Revise, ReportRemarkBLL.actionForm.Edit, objModuledataReport.objReport.PK_Report_ID, note)
                            objDB.SaveChanges()

                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            Dim objReport As GoAML_Report = (From x In objDB.GoAML_Report Where x.PK_Report_ID = objModuledataReport.objReport.PK_Report_ID).FirstOrDefault

                            With objReport
                                .status = 5
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objDB.Entry(objReport).State = EntityState.Modified

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailEdit(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport, objReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Revise, ReportRemarkBLL.actionForm.Delete, objModuledataReport.objReport.PK_Report_ID, note)
                            objDB.SaveChanges()

                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

    End Sub

    Public Sub Reject(pK_ModuleApproval_ID As String, note As String)
        Using objDB As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDB.Database.BeginTransaction()

                Try
                    Dim objApproval As NawaDevDAL.ModuleApproval = objDB.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = pK_ModuleApproval_ID).FirstOrDefault()
                    Dim objModule As NawaDevDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objDB.Modules.Where(Function(x) x.ModuleName = x.ModuleName).FirstOrDefault()
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case Common.ModuleActionEnum.Insert
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Deleted

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport)
                            objDB.SaveChanges()
                            '------------------------------end Report-----------------------------------------------------------------------------'

                            '--------------------------------------------Transaction----------------------------------------------------------------'

                            For Each objModuledataTransaction As Transaction In objModuledataReport.listObjTransaction
                                Dim itemObjTransaction As New goAML_Transaction
                                itemObjTransaction = objModuledataTransaction.objTransaction
                                objDB.Entry(itemObjTransaction).State = EntityState.Deleted
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransaction)

                                If objModuledataTransaction.objTransaction.FK_Transaction_Type = 1 Then

                                    If Not objModuledataTransaction.ObjConductor.PK_goAML_Trn_Conductor_ID = 0 Then
                                        TransactionConductorBLL.DeleteTransactionConductor(objModuledataTransaction, objATHeaderReport)

                                        Dim itemObjConductor As New goAML_Trn_Conductor
                                        itemObjConductor = objModuledataTransaction.ObjConductor
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjConductor)

                                        For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                        Next

                                        For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                        Next

                                        For Each ConductorAddress As goAML_Trn_Conductor_Address In objModuledataTransaction.listObjAddressEmployerConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorAddress)
                                        Next

                                        For Each ConductorPhone As goAML_trn_Conductor_Phone In objModuledataTransaction.listObjPhoneEmployerConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorPhone)
                                        Next

                                        For Each ConductorIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationConductor
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ConductorIdentification)
                                        Next
                                    End If

                                    '-------------------------------------Pengirim----------------------------------------------------------

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 1 Then

                                        TransactionAccountBLL.DeleteTransactionAccountFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionAccountFrom As New goAML_Transaction_Account
                                        itemObjTransactionAccountFrom = objModuledataTransaction.objAccountFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountFrom)

                                        '------------------------------------------------------------ Transaction Account Entity From -----------------------------------------

                                        Dim itemObjTransactionAccountEntityFrom As New goAML_Trn_Entity_account
                                        itemObjTransactionAccountEntityFrom = objModuledataTransaction.objAccountEntityFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityFrom)

                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityAccountFrom As DirectorClass In objModuledataTransaction.listDirectorAccountEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataDirectorEntityAccountFrom)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountFrom.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountFrom.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                        '    '------------------------End Director Entity Account Transaction From---------------------------------------
                                        ''---------------------------------------- end Transaction Account Entity From --------------------------------------------------------------

                                        ''----------------------------Signatory Account Transaction From--------------------------------------------------
                                        For Each objModuledataSignatoryAccountFrom As SignatoryClass In objModuledataTransaction.LisAccountSignatoryFrom

                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                            itemObjSignatory = objModuledataSignatoryAccountFrom.objSignatory
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountFrom.listAddressEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountFrom.listPhoneEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                            Next
                                        Next
                                        ''----------------------------End Signatory Account Transaction From--------------------------------------------------
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 2 Then
                                        TransactionPersonBLL.DeleteTransactionPersonFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim ItemObjTransactionPersonFrom As New goAML_Transaction_Person
                                        ItemObjTransactionPersonFrom = objModuledataTransaction.objPersonFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonFrom)
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_From_Information = 3 Then
                                        TransactionEntityBLL.DeleteTransactionEntityFrom(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                                        itemObjTransactionEntityFrom = objModuledataTransaction.objEntityFrom
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityFrom)
                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityFrom
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityFrom As DirectorClass In objModuledataTransaction.listDirectorEntityFrom

                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityFrom.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityFrom.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityFrom.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityFrom.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                    End If

                                    '----------------------------------------Penerima---------------------------------------------------------

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 1 Then
                                        TransactionAccountBLL.DeleteTransactionAccountTo(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionAccountTo As New goAML_Transaction_Account
                                        itemObjTransactionAccountTo = objModuledataTransaction.objAccountTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountTo)

                                        Dim itemObjTrnEntityAccount As New goAML_Trn_Entity_account
                                        itemObjTrnEntityAccount = objModuledataTransaction.objAccountEntityTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTrnEntityAccount)

                                        For Each EntityPhone As goAML_Trn_Acc_Entity_Phone In objModuledataTransaction.listObjPhoneAccountEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Acc_Entity_Address In objModuledataTransaction.listObjAddressAccountEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityAccountTo As DirectorClass In objModuledataTransaction.listDirectorAccountEntityto

                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityAccountTo.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityAccountTo.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityAccountTo.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityAccountTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                        For Each objModuledataSignatoryAccountTo As SignatoryClass In objModuledataTransaction.LisAccountSignatoryTo

                                            Dim itemObjSignatory As New goAML_Trn_acc_Signatory
                                            itemObjSignatory = objModuledataSignatoryAccountTo.objSignatory
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatory)

                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountAddress As goAML_Trn_Acc_sign_Address In objModuledataSignatoryAccountTo.listAddressEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountAddress)
                                            Next
                                            For Each SignatoryAccountPhone As goAML_trn_acc_sign_Phone In objModuledataSignatoryAccountTo.listPhoneEmployerSignatory
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountPhone)
                                            Next
                                            For Each SignatoryAccountIdentification As goAML_Transaction_Person_Identification In objModuledataSignatoryAccountTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAccountIdentification)
                                            Next
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 2 Then
                                        TransactionPersonBLL.DeleteTransactionPersonTo(objModuledataTransaction, objATHeaderReport)
                                        Dim ItemObjTransactionPersonTo As New goAML_Transaction_Person
                                        ItemObjTransactionPersonTo = objModuledataTransaction.objPersonTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPersonTo)

                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhonePersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonAddress As goAML_Trn_Person_Address In objModuledataTransaction.listObjAddressEmployerPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next
                                        For Each PersonPhone As goAML_trn_Person_Phone In objModuledataTransaction.listObjPhoneEmployerPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next
                                        For Each PersonIdentification As goAML_Transaction_Person_Identification In objModuledataTransaction.listObjIdentificationPersonTo
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next
                                    End If

                                    If objModuledataTransaction.objTransaction.FK_Sender_To_Information = 3 Then
                                        TransactionEntityBLL.DeleteTransactionEntityTo(objModuledataTransaction, objATHeaderReport)
                                        Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                                        itemObjTransactionEntityTo = objModuledataTransaction.objEntityTo
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityTo)

                                        For Each EntityPhone As goAML_Trn_Entity_Phone In objModuledataTransaction.listObjPhoneEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhone)
                                        Next
                                        For Each EntityAddress As goAML_Trn_Entity_Address In objModuledataTransaction.ListObjAddressEntityto
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddress)
                                        Next
                                        For Each objModuledataDirectorEntityTo As DirectorClass In objModuledataTransaction.listDirectorEntityto
                                            Dim itemObjDirector As New goAML_Trn_Director
                                            itemObjDirector = objModuledataDirectorEntityTo.objDirector
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirector)
                                            For Each DirectorPhone As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next
                                            For Each DirectorAddress As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next
                                            For Each DirectorAddressEmployer As goAML_Trn_Director_Address In objModuledataDirectorEntityTo.listAddressEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next
                                            For Each DirectorPhoneEmployer As goAML_trn_Director_Phone In objModuledataDirectorEntityTo.listPhoneEmployerDirector
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next
                                            For Each DirectorIdentification As goAML_Transaction_Person_Identification In objModuledataDirectorEntityTo.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next
                                    End If
                                Else

                                    TransactionPartyBLL.DeleteTransactionParty(objModuledataTransaction, objATHeaderReport)

                                    Dim itemObjTransactionParty As New goAML_Transaction_Party
                                    itemObjTransactionParty = objModuledataTransaction.objTransactionParty
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionParty)

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 1 Then

                                        Dim itemObjTransactionAccountParty As New goAML_Trn_Party_Account
                                        itemObjTransactionAccountParty = objModuledataTransaction.objAccountParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountParty)

                                        Dim itemObjTransactionAccountEntityParty As New goAML_Trn_Par_Acc_Entity
                                        itemObjTransactionAccountEntityParty = objModuledataTransaction.objAccountEntityParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionAccountEntityParty)

                                        For Each EntityPhoneParty As goAML_trn_par_acc_Entity_Phone In objModuledataTransaction.listPhoneAccountEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                        Next

                                        For Each EntityAddressParty As goAML_Trn_par_Acc_Entity_Address In objModuledataTransaction.listAddresAccountEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                        Next

                                        For Each objModuledataDirectorEntityAccountParty As DirectorEntityAccountPartyClass In objModuledataTransaction.listDirectorEntytAccountparty
                                            Dim itemObjDirectorParty As New goAML_Trn_Par_Acc_Ent_Director
                                            itemObjDirectorParty = objModuledataDirectorEntityAccountParty.objDirectorEntityAccountParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                            For Each DirectorPhone As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next

                                            For Each DirectorAddress As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next

                                            For Each DirectorAddressEmployer As goAML_Trn_Par_Acc_Ent_Director_Address In objModuledataDirectorEntityAccountParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next

                                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Acc_Ent_Director_Phone In objModuledataDirectorEntityAccountParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next

                                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityAccountParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next
                                        Next

                                        For Each objModuledataSignatoryAccountParty As SignatoryAccountPartyClass In objModuledataTransaction.listSignatoryAccountParty
                                            Dim itemObjSignatoryParty As New goAML_Trn_par_acc_Signatory
                                            itemObjSignatoryParty = objModuledataSignatoryAccountParty.objSignatoryAccountParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjSignatoryParty)

                                            For Each SignatoryPhone As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhone)
                                            Next

                                            For Each SignatoryAddress As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddress)
                                            Next

                                            For Each SignatoryAddressEmployer As goAML_Trn_par_Acc_sign_Address In objModuledataSignatoryAccountParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryAddressEmployer)
                                            Next

                                            For Each SignatoryPhoneEmployer As goAML_trn_par_acc_sign_Phone In objModuledataSignatoryAccountParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryPhoneEmployer)
                                            Next

                                            For Each SignatoryIdentification As goAML_Transaction_Party_Identification In objModuledataSignatoryAccountParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, SignatoryIdentification)
                                            Next
                                        Next

                                    End If

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 2 Then
                                        Dim ItemObjTransactionPartyPerson As New goAML_Trn_Party_Person
                                        ItemObjTransactionPartyPerson = objModuledataTransaction.objPersonParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, ItemObjTransactionPartyPerson)

                                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhonePersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next

                                        For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next

                                        For Each PersonAddress As goAML_Trn_Party_Person_Address In objModuledataTransaction.listAddressEmployerPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonAddress)
                                        Next

                                        For Each PersonPhone As goAML_Trn_Party_Person_Phone In objModuledataTransaction.listPhoneEmployerPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonPhone)
                                        Next

                                        For Each PersonIdentification As goAML_Transaction_Party_Identification In objModuledataTransaction.listIdentificationPersonParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, PersonIdentification)
                                        Next

                                    End If

                                    If objModuledataTransaction.objTransactionParty.fk_ref_detail_of = 3 Then

                                        Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                                        itemObjTransactionEntityParty = objModuledataTransaction.objentityParty
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjTransactionEntityParty)

                                        For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In objModuledataTransaction.listPhoneEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityPhoneParty)
                                        Next

                                        For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In objModuledataTransaction.listAddressEntityparty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, EntityAddressParty)
                                        Next

                                        For Each objModuledataDirectorEntityParty As DirectorEntityPartyClass In objModuledataTransaction.listDirectorEntityParty
                                            Dim itemObjDirectorParty As New goAML_Trn_Par_Entity_Director
                                            itemObjDirectorParty = objModuledataDirectorEntityParty.objDirectorEntityParty
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjDirectorParty)

                                            For Each DirectorPhone As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhone
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhone)
                                            Next

                                            For Each DirectorAddress As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddress
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddress)
                                            Next

                                            For Each DirectorAddressEmployer As goAML_Trn_Par_Entity_Director_Address In objModuledataDirectorEntityParty.listAddressEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorAddressEmployer)
                                            Next

                                            For Each DirectorPhoneEmployer As goAML_Trn_Par_Entity_Director_Phone In objModuledataDirectorEntityParty.listPhoneEmployer
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorPhoneEmployer)
                                            Next

                                            For Each DirectorIdentification As goAML_Transaction_Party_Identification In objModuledataDirectorEntityParty.listIdentification
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, DirectorIdentification)
                                            Next

                                        Next

                                    End If

                                End If
                                '---------------------------------------------------------end Transaction------------------------------------------------------------
                            Next

                            For Each objModuledataReportIndicator As goAML_Report_Indicator In objModuledataReport.listIndicator
                                Dim itemObjReportIndicator As New goAML_Report_Indicator
                                itemObjReportIndicator = objModuledataReportIndicator
                                objDB.Entry(itemObjReportIndicator).State = EntityState.Deleted
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, itemObjReportIndicator)
                            Next

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Reject, ReportRemarkBLL.actionForm.Add, objModuledataReport.objReport.PK_Report_ID, note)

                        Case Common.ModuleActionEnum.Update
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Deleted

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Reject, ReportRemarkBLL.actionForm.Edit, objModuledataReport.objReport.PK_Report_ID, note)
                        Case Common.ModuleActionEnum.Delete
                            Dim objModuledataReport As Report = Common.Deserialize(objApproval.ModuleField, GetType(Report))
                            objDB.Entry(objModuledataReport.objReport).State = EntityState.Deleted

                            Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                            Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Update
                            Dim modulename As String = objModule.ModuleLabel
                            Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objATHeaderReport.PK_AuditTrail_ID, objModuledataReport)

                            ReportRemarkBLL.CreateNotes(objModule.PK_Module_ID, objModule.ModuleLabel, ReportRemarkBLL.actionApproval.Reject, ReportRemarkBLL.actionForm.Delete, objModuledataReport.objReport.PK_Report_ID, note)
                    End Select

                    objDB.Entry(objApproval).State = EntityState.Deleted
                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

End Class
