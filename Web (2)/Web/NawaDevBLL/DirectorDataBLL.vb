﻿Imports NawaDevDAL

Public Class DirectorDataBLL
    Public objDirectorEntityFrom As goAML_Trn_Director
    Public listobjPhoneDirectorEntityFrom As List(Of goAML_trn_Director_Phone)
    Public listobjAddressDirectorEntityFrom As List(Of goAML_Trn_Director_Address)

    Public listobjPhoneEmployerDirectorEntityFrom As List(Of goAML_trn_Director_Phone)
    Public listobjAddressEmployerDirectorEntityFrom As List(Of goAML_Trn_Director_Address)
    Public listobjIdentificationDirectorEntityFrom As List(Of goAML_Transaction_Person_Identification)

    Public objDirectorEntityTo As goAML_Trn_Director
    Public listobjPhoneDirectorEntityTo As List(Of goAML_trn_Director_Phone)
    Public listobjAddressDirectorEntityTo As List(Of goAML_Trn_Director_Address)

    Public listobjPhoneEmployerDirectorEntityTo As List(Of goAML_trn_Director_Phone)
    Public listobjAddressEmployerDirectorEntityTo As List(Of goAML_Trn_Director_Address)
    Public listobjIdentificationDirectorEntityTo As List(Of goAML_Transaction_Person_Identification)

End Class
