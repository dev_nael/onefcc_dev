﻿Public Class DirectorEntityPartyClass
    Public objDirectorEntityParty As New NawaDevDAL.goAML_Trn_Par_Entity_Director
    Public listAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public listPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
    Public listAddressEmployer As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
    Public listPhoneEmployer As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
    Public listIdentification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)

End Class
