﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionPartyEntityBLL
    Public Shared Sub InsertTransactionPartyEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                    itemObjTransactionEntityParty = Transactions.objentityParty

                    With itemObjTransactionEntityParty
                        .FK_Transaction_Party_ID = Transactions.objTransactionParty.PK_Trn_Party_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionEntityParty).State = EntityState.Added

                    Dim objtypeReport As Type = itemObjTransactionEntityParty.GetType
                    Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    For Each item As PropertyInfo In propertiesReport
                        Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                        {
                            .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            .FieldName = item.Name,
                            .OldValue = "",
                            .NewValue = If(Not item.GetValue(itemObjTransactionEntityParty, Nothing) Is Nothing, item.GetValue(itemObjTransactionEntityParty, Nothing), "")
                        }
                        objdb.Entry(objATDetail).State = EntityState.Added
                    Next
                    objdb.SaveChanges()

                    If Transactions.listPhoneEntityParty.Count > 0 Then
                        For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In Transactions.listPhoneEntityParty
                            With EntityPhoneParty
                                .FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhoneParty).State = EntityState.Added

                            objtypeReport = EntityPhoneParty.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(EntityPhoneParty, Nothing) Is Nothing, item.GetValue(EntityPhoneParty, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If

                    If Transactions.listAddressEntityparty.Count > 0 Then
                        For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In Transactions.listAddressEntityparty
                            With EntityAddressParty
                                .FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddressParty).State = EntityState.Added

                            objtypeReport = EntityAddressParty.GetType
                            propertiesReport = objtypeReport.GetProperties
                            For Each item As PropertyInfo In propertiesReport
                                Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                {
                                    .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                    .FieldName = item.Name,
                                    .OldValue = "",
                                    .NewValue = If(Not item.GetValue(EntityAddressParty, Nothing) Is Nothing, item.GetValue(EntityAddressParty, Nothing), "")
                                }
                                objdb.Entry(objATDetail).State = EntityState.Added
                            Next
                        Next
                    End If
                    objdb.SaveChanges()

                    '--------------------------------------Director Entity Transaction Party------------------------------------
                    TransactionPartyEntityDirectorBLL.InsertTransactionPartyEntityDirector(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionPartyEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityParty As New goAML_Trn_Party_Entity
                    itemObjTransactionEntityParty = Transactions.objentityParty

                    objdb.Entry(itemObjTransactionEntityParty).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Transactions.listPhoneEntityParty.Count > 0 Then
                        For Each EntityPhoneParty As goAML_Trn_Party_Entity_Phone In Transactions.listPhoneEntityParty
                            objdb.Entry(EntityPhoneParty).State = EntityState.Deleted
                        Next
                    End If

                    If Transactions.listAddressEntityparty.Count > 0 Then
                        For Each EntityAddressParty As goAML_Trn_Party_Entity_Address In Transactions.listAddressEntityparty
                            objdb.Entry(EntityAddressParty).State = EntityState.Deleted
                        Next
                    End If
                    objdb.SaveChanges()

                    '--------------------------------------Director Entity Transaction Party------------------------------------
                    TransactionPartyEntityDirectorBLL.DeleteTransactionPartyEntityDirector(Transactions, Audit)

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub UpdateTransactionPartyEntity(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim TransactionPartyEntity As goAML_Trn_Party_Entity = Transactions.objentityParty
                    Dim TransactionPartyEntityCek As goAML_Trn_Party_Entity = (From x In objdb.goAML_Trn_Party_Entity Where x.PK_Trn_Party_Entity_ID = TransactionPartyEntity.PK_Trn_Party_Entity_ID Select x).FirstOrDefault
                    If TransactionPartyEntityCek Is Nothing Then

                        InsertTransactionPartyEntity(Transactions, Audit)

                    Else

                        objdb.Entry(TransactionPartyEntityCek).CurrentValues.SetValues(TransactionPartyEntity)
                        objdb.Entry(TransactionPartyEntityCek).State = EntityState.Modified

                        Dim objtypeReport As Type = TransactionPartyEntity.GetType
                        Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                        For Each item As PropertyInfo In propertiesReport
                            Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            {
                                .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                .FieldName = item.Name,
                                .OldValue = "",
                                .NewValue = If(Not item.GetValue(TransactionPartyEntity, Nothing) Is Nothing, item.GetValue(TransactionPartyEntity, Nothing), "")
                            }
                            objdb.Entry(objATDetail).State = EntityState.Added
                        Next
                        objdb.SaveChanges()

                        If Transactions.listPhoneEntityParty.Count > 0 Then

                            'Delete
                            For Each itemEntityPhonex As goAML_Trn_Party_Entity_Phone In (From x In objdb.goAML_Trn_Party_Entity_Phone Where x.FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID).ToList
                                Dim objCekEntityPhone As goAML_Trn_Party_Entity_Phone = Transactions.listPhoneEntityParty.Find(Function(x) x.PK_Trn_Party_Entity_Phone_ID = itemEntityPhonex.PK_Trn_Party_Entity_Phone_ID)
                                If objCekEntityPhone Is Nothing Then
                                    objdb.Entry(itemEntityPhonex).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemEntityPhone As goAML_Trn_Party_Entity_Phone In Transactions.listPhoneEntityParty
                                Dim objCek As goAML_Trn_Party_Entity_Phone = (From x In objdb.goAML_Trn_Party_Entity_Phone Where x.PK_Trn_Party_Entity_Phone_ID = itemEntityPhone.PK_Trn_Party_Entity_Phone_ID Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemEntityPhone).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemEntityPhone)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Entity_Phone In Transactions.listPhoneEntityParty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listAddressEntityparty.Count > 0 Then

                            'Delete
                            For Each itemEntityAddressx As goAML_Trn_Party_Entity_Address In (From x In objdb.goAML_Trn_Party_Entity_Address Where x.FK_Trn_Party_Entity_ID = Transactions.objentityParty.PK_Trn_Party_Entity_ID).ToList
                                Dim objCekEntityAddress As goAML_Trn_Party_Entity_Address = Transactions.listAddressEntityparty.Find(Function(x) x.PK_Trn_Party_Entity_Address_ID = itemEntityAddressx.PK_Trn_Party_Entity_Address_ID)
                                If objCekEntityAddress Is Nothing Then
                                    objdb.Entry(itemEntityAddressx).State = EntityState.Deleted
                                End If
                            Next

                            'Add Modified
                            For Each itemEntityAddress As goAML_Trn_Party_Entity_Address In Transactions.listAddressEntityparty
                                Dim objCek As goAML_Trn_Party_Entity_Address = (From x In objdb.goAML_Trn_Party_Entity_Address Where x.PK_Trn_Party_Entity_Address_ID = itemEntityAddress.PK_Trn_Party_Entity_Address_ID Select x).FirstOrDefault
                                If objCek Is Nothing Then
                                    objdb.Entry(itemEntityAddress).State = EntityState.Added
                                Else
                                    objdb.Entry(objCek).CurrentValues.SetValues(itemEntityAddress)
                                    objdb.Entry(objCek).State = EntityState.Modified
                                End If
                            Next

                            'AuditTrail
                            For Each itemDetail As goAML_Trn_Party_Entity_Address In Transactions.listAddressEntityparty
                                objtypeReport = itemDetail.GetType
                                propertiesReport = objtypeReport.GetProperties
                                For Each item As PropertyInfo In propertiesReport
                                    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                                    {
                                        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                                        .FieldName = item.Name,
                                        .OldValue = "",
                                        .NewValue = If(Not item.GetValue(itemDetail, Nothing) Is Nothing, item.GetValue(itemDetail, Nothing), "")
                                    }
                                    objdb.Entry(objATDetail).State = EntityState.Added
                                Next
                            Next

                        End If

                        If Transactions.listDirectorEntityParty.Count > 0 Then

                            TransactionPartyEntityDirectorBLL.UpdateTransactionPartyEntityDirector(Transactions, Audit)

                        End If

                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using


    End Sub
End Class
