﻿Public Class GlobalReportFunctionBLL
#Region "Get Data Bi-Party"


    Shared Function getListALLActiviyByReportIDALLHierarky(reportid As Long) As NawaDevBLL.ListActivityBLL
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Using objcommand As Common.DbCommand = objdb.Database.Connection.CreateCommand()
                objcommand.CommandType = CommandType.StoredProcedure
                objcommand.CommandText = "usp_getActivityGoAMLByReportID"
                Dim objsqlparam As New SqlClient.SqlParameter
                objsqlparam.ParameterName = "@pkreportid"
                objsqlparam.Value = reportid
                objcommand.Parameters.Add(objsqlparam)
                Try
                    objdb.Database.Connection.Open()
                    Dim objreader As Common.DbDataReader = objcommand.ExecuteReader()
                    Dim objListActivityBLL As New NawaDevBLL.ListActivityBLL

                    With objListActivityBLL

                        .list_goAML_Act_ReportPartyType = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_ReportPartyType)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Account = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Account)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Entity_Account = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Entity_Account)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Person = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Person)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Person_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Person_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Person_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Person_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Activity_Person_Identification = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Activity_Person_Identification)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Entity = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Entity)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Entity_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_Ent_Director = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_Entity_Director_address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_Entity_Director_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)(objreader).ToList
                        objreader.NextResult()

                        .list_goAML_Act_Director = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Director)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Director_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Director_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Director_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Director_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_acc_Signatory = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_acc_Signatory)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_sign_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_sign_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Act_Acc_sign_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)(objreader).ToList

                        Return objListActivityBLL
                    End With


                Catch ex As Exception
                    Throw
                Finally
                    objdb.Database.Connection.Close()
                End Try
            End Using
        End Using
    End Function
    Shared Function getListALLTransactionByReportIDALLHierarky(reportid As Long) As NawaDevBLL.ListTransactionBLL
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Using objcommand As Common.DbCommand = objdb.Database.Connection.CreateCommand()
                objcommand.CommandType = CommandType.StoredProcedure
                objcommand.CommandText = "usp_getTransactionGoAMLByReportID"
                Dim objsqlparam As New SqlClient.SqlParameter
                objsqlparam.ParameterName = "@pkreportid"
                objsqlparam.Value = reportid
                objcommand.Parameters.Add(objsqlparam)
                Try
                    objdb.Database.Connection.Open()
                    Dim objreader As Common.DbDataReader = objcommand.ExecuteReader()
                    Dim objListTransactionBLL As New NawaDevBLL.ListTransactionBLL

                    With objListTransactionBLL

                        .List_goAML_Transaction = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Person = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Person)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Entity = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Entity)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Account = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Account)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_acc_Signatory = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_acc_Signatory)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Acc_sign_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_acc_sign_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_acc_sign_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Person_Identification = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Person_Identification)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Conductor = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Conductor)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Conductor_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Conductor_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_Conductor_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_Conductor_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Person_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Person_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_Person_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_Person_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Entity_account = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Entity_account)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Acc_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Acc_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Director = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Director)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Director_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Director_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_Director_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_Director_Phone)(objreader).ToList
                        objreader.NextResult()
                        .List_goAML_Trn_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Entity_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Party = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Party)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Acc_Ent_Director = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Acc_Ent_Director_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Acc_Ent_Director_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Entity_Director = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Entity_Director_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Entity_Director_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_par_acc_Signatory = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_par_Acc_sign_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_par_acc_sign_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Account = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Account)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Par_Acc_Entity = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Par_Acc_Entity)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_par_Acc_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_trn_par_acc_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Person = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Person)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Person_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Person_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Person_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Transaction_Party_Identification = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Transaction_Party_Identification)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Entity = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Entity)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Entity_Address = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)(objreader).ToList
                        objreader.NextResult()
                        .list_goAML_Trn_Party_Entity_Phone = CType(objdb, System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.Translate(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)(objreader).ToList

                        Return objListTransactionBLL
                    End With


                Catch ex As Exception
                    Throw
                Finally
                    objdb.Database.Connection.Close()
                End Try
            End Using
        End Using
    End Function
    Shared Function getReportById(id As String) As NawaDevDAL.goAML_Report
        Dim objReport As New NawaDevDAL.goAML_Report
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Report As NawaDevDAL.goAML_Report = objdb.goAML_Report.Where(Function(x) x.PK_Report_ID = id).FirstOrDefault
            If Report IsNot Nothing Then
                objReport = Report
            End If
        End Using
        Return objReport
    End Function
    Shared Function getListTransactionById(id As String) As List(Of NawaDevDAL.goAML_Transaction)
        Dim listTransaction As New List(Of NawaDevDAL.goAML_Transaction)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New List(Of NawaDevDAL.goAML_Transaction)
            Transaction = objdb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = id).ToList
            If Transaction IsNot Nothing Then
                listTransaction = Transaction
            End If
        End Using
        Return listTransaction
    End Function
    Shared Function getListIndicatorById(id As String) As List(Of NawaDevDAL.goAML_Report_Indicator)
        Dim listIndicator As New List(Of NawaDevDAL.goAML_Report_Indicator)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator)
            Indicator = objdb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = id).ToList
            If Indicator IsNot Nothing Then
                listIndicator = Indicator
            End If
        End Using
        Return listIndicator
    End Function

    'tambah function getTransactionById, jefry.
    Shared Function getTransactionById(id As String) As NawaDevDAL.goAML_Transaction
        Dim objTransaction As New NawaDevDAL.goAML_Transaction
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New NawaDevDAL.goAML_Transaction
            Transaction = objdb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = id).FirstOrDefault
            If Transaction IsNot Nothing Then
                objTransaction = Transaction
            End If
        End Using
        Return objTransaction
    End Function

    'Start Conductor
    Shared Function getConductorById(id As String) As NawaDevDAL.goAML_Trn_Conductor
        Dim objConductor As New NawaDevDAL.goAML_Trn_Conductor
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Conductor As New NawaDevDAL.goAML_Trn_Conductor
            Conductor = objdb.goAML_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = id).FirstOrDefault
            If Conductor IsNot Nothing Then
                objConductor = Conductor
            End If
        End Using
        Return objConductor
    End Function
    Shared Function getConductorPhoneById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_Conductor_Phone)
        Dim listConductorPhone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)

        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities

            'objdb.Database.CommandTimeout = 3600
            Dim ConductorPhone As New List(Of NawaDevDAL.goAML_trn_Conductor_Phone)
            ConductorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_Conductor_Phone)("select * from goaml_trn_conductor_phone with(nolock) where FK_Trn_Conductor_ID =" & id & " and isemployer ='" & isEmp & "'").ToList
            'ConductorPhone = objdb.goAML_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = id And x.isEmployer = isEmp).ToList
            If ConductorPhone IsNot Nothing Then
                listConductorPhone = ConductorPhone
            End If
        End Using
        Return listConductorPhone
    End Function
    Shared Function getConductorAddressById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
        Dim listConductorAddress As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ConductorAddress As New List(Of NawaDevDAL.goAML_Trn_Conductor_Address)
            'ConductorAddress = objdb.goAML_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = id And x.isEmployer = isEmp).ToList
            ConductorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Conductor_Address)("select * from goaml_trn_conductor_address with(nolock) where FK_Trn_Conductor_ID =" & id & " and isemployer ='" & isEmp & "'").ToList
            If ConductorAddress IsNot Nothing Then
                listConductorAddress = ConductorAddress
            End If
        End Using
        Return listConductorAddress
    End Function
    'End Conductor ----------------------------------------------------------------------------------------------------------------------

    'Start Transaction From Account
    Shared Function getTransactionAccountByFK(id As String, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Account
        Dim objFromAccount As New NawaDevDAL.goAML_Transaction_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Transaction_Account
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Account)("select * from goAML_Transaction_Account with(nolock) where FK_Report_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    Shared Function getTransactionAccountEntityByFK(AccID As String, FromTO As Integer) As NawaDevDAL.goAML_Trn_Entity_account
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Trn_Entity_account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Trn_Entity_account
            'AccountEntity = objdb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = AccID And x.FK_From_Or_To = FromTO).FirstOrDefault
            AccountEntity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_account)("select * from goAML_Trn_Entity_account with(nolock) where FK_Account_ID = " & AccID & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListTransactionAccountEntityPhoneById(id As String) As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)
            'AccountEntityPhone = objdb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Id).ToList
            AccountEntityPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone)("select * from goAML_Trn_Acc_Entity_Phone with(nolock) where FK_Trn_Acc_Entity = " & id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListTransactionAccountEntityAddressById(id As String) As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)
            'AccountEntityAddress = objdb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Id).ToList
            AccountEntityAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address)("select * from goAML_Trn_Acc_Entity_Address with(nolock) where FK_Trn_Acc_Entity = " & id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    Shared Function getListTransactionAccountEntityDirectorById(Entityid As String, FromTO As Integer, SenderId As Integer) As List(Of NawaDevDAL.goAML_Trn_Director)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Director)
            'AccountEntityDirector = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityId And x.FK_From_Or_To = FromTO And x.FK_Sender_Information = SenderId).ToList
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Director)("select * from goAML_Trn_Director with(nolock) where FK_Entity_ID = " & Entityid & " and FK_From_Or_To = " & FromTO & " and FK_Sender_Information = " & SenderId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionAccountEntityDirectorPhoneById(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_Director_Phone)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_trn_Director_Phone)
            'AccountEntityDirectorPhone = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = Id And x.isEmployer = IsEmp).ToList
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_Director_Phone)("select * from goAML_trn_Director_Phone with(nolock) where FK_Trn_Director_ID =" & id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListTransactionAccountEntityDirectorAddressById(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Director_Address)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Director_Address)
            'AccountEntityDirectorAddress = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = Id And x.isEmployer = IsEmp).ToList
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Director_Address)("select * from goAML_Trn_Director_Address with(nolock) where FK_Trn_Director_ID =" & id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    Shared Function getListTransactionIdentificationById(id As String, TypeID As Integer, FromTO As Integer) As List(Of NawaDevDAL.goAML_Transaction_Person_Identification)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Transaction_Person_Identification)
            'Identification = objdb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Id And x.FK_Person_Type = TypeID And x.from_or_To_Type = FromTO).ToList
            Identification = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Person_Identification)("select * from goAML_Transaction_Person_Identification with(nolock) where FK_Person_ID = " & id & " and FK_Person_Type = " & TypeID & " and from_or_To_Type = " & FromTO).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Signatory
    Shared Function getListTransactionAccountSignatoryById(TranAccID As String, FromTO As Integer) As List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Trn_acc_Signatory)
            'signatory = objdb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = TranAccID And x.FK_From_Or_To = FromTO).ToList
            signatory = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_acc_Signatory)("select * from goAML_Trn_acc_Signatory with(nolock) where FK_Transaction_Account_ID = " & TranAccID & " and FK_From_Or_To = " & FromTO).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListTransactionAccountSignatoryPhoneById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_acc_sign_Phone)
            'Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Id And x.isEmployer = isEmp).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_acc_sign_Phone)("select * from goAML_trn_acc_sign_Phone with(nolock) where FK_Trn_Acc_Entity = " & id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListTransactionAccountSignatoryAddressById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)
            'Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Id And x.isEmployer = isEmp).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Acc_sign_Address)("select * from goAML_Trn_Acc_sign_Address with(nolock) where FK_Trn_Acc_Entity = " & id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'End Transaction From Account --------------------------------------------------------------------------------------------------------------

    'Start Transaction From Person
    Shared Function getTransactionPersonByFK(id As String, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Person
        Dim objFromPerson As New NawaDevDAL.goAML_Transaction_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Transaction_Person
            'Person = objdb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Person)("select * from goAML_Transaction_Person with(nolock) where FK_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListTransactionPersonPhoneById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_Person_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_Person_Phone)
            'Phone = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = Id And x.isEmployer = isEmp).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_Person_Phone)("select * from goAML_trn_Person_Phone with(nolock) where FK_Trn_Person = " & id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionPersonAddressById(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Person_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Person_Address)
            'Address = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = Id And x.isEmployer = isEmp).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Person_Address)("select * from goAML_Trn_Person_Address with(nolock) where FK_Trn_Person = " & id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Transaction From Person ----------------------------------------------------------------------------------------------------------------

    'Start Transaction From Entity
    Shared Function getTransactionEntityByFK(id As String, FromTO As Integer) As NawaDevDAL.goAML_Transaction_Entity
        Dim objFromEntity As New NawaDevDAL.goAML_Transaction_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Transaction_Entity
            'Entity = objdb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Entity)("select * from goAML_Transaction_Entity with(nolock) where FK_Transaction_ID = " & id & " and FK_From_Or_To = " & FromTO).FirstOrDefault
            If Entity IsNot Nothing Then
                objFromEntity = Entity
            End If
        End Using
        Return objFromEntity
    End Function
    Shared Function getListTransactionEntityPhoneById(id As String) As List(Of NawaDevDAL.goAML_Trn_Entity_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Entity_Phone)
            'Phone = objdb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = Id).ToList
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_Phone)("select * from goAML_Trn_Entity_Phone with(nolock) where FK_Trn_Entity = " & id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionEntityAddressById(id As String) As List(Of NawaDevDAL.goAML_Trn_Entity_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Entity_Address)
            'Address = objdb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = Id).ToList
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Entity_Address)("select * from goAML_Trn_Entity_Address with(nolock) where FK_Trn_Entity = " & id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Transaction From Entity ----------------------------------------------------------------------------------------------------------------
    Shared Function getJenisTransactionByCode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Transaksi = objdb.goAML_Ref_Jenis_Transaksi.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
#End Region

#Region "Get Data Multy-Party"
    'Multiparty
    'Transaction--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionByIdParty(id As String) As NawaDevDAL.goAML_Transaction_Party
        Dim objTransaction As New NawaDevDAL.goAML_Transaction_Party
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New NawaDevDAL.goAML_Transaction_Party
            Transaction = objdb.goAML_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = id).FirstOrDefault
            If Transaction IsNot Nothing Then
                objTransaction = Transaction
            End If
        End Using
        Return objTransaction
    End Function
    Shared Function getListTransactionByIdParty(id As String) As List(Of NawaDevDAL.goAML_Transaction_Party)
        Dim listTransaction As New List(Of NawaDevDAL.goAML_Transaction_Party)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Transaction As New List(Of NawaDevDAL.goAML_Transaction_Party)
            Transaction = objdb.goAML_Transaction_Party.Where(Function(x) x.FK_Report_ID = id).ToList
            If Transaction IsNot Nothing Then
                listTransaction = Transaction
            End If
        End Using
        Return listTransaction
    End Function
    'Account Entity Director--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionEntityAccountDirectorByIdParty(Entityid As String) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
            AccountEntityDirector = objdb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = Entityid).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionAccountEntityDirectorPhoneByIdParty(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
            AccountEntityDirectorPhone = objdb.goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = id And x.isemployer = IsEmp).ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListTransactionAccountEntityDirectorAddressByIdParty(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
            AccountEntityDirectorAddress = objdb.goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = id And x.isemployer = IsEmp).ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    'Identification Party--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionIdentificationByIdParty(id As String, TypeID As Integer, ReportID As String) As List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
            Identification = objdb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = TypeID And x.FK_Report_ID = ReportID).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Entity Director--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionEntitytDirectorByIdParty(EntityId As String) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
            AccountEntityDirector = objdb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListTransactionEntityDirectorPhoneByIdParty(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
        Dim objFromEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityDirectorPhone As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
            EntityDirectorPhone = objdb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = id And x.isemployer = IsEmp).ToList
            If EntityDirectorPhone IsNot Nothing Then
                objFromEntityDirectorPhone = EntityDirectorPhone
            End If
        End Using
        Return objFromEntityDirectorPhone
    End Function
    Shared Function getListTransactionEntityDirectorAddressByIdParty(id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
        Dim objFromEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityDirectorAddress As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
            EntityDirectorAddress = objdb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = id And x.isemployer = IsEmp).ToList
            If EntityDirectorAddress IsNot Nothing Then
                objFromEntityDirectorAddress = EntityDirectorAddress
            End If
        End Using
        Return objFromEntityDirectorAddress
    End Function
    'Signatory--------------------------------------------------------------------------------------------------------------
    Shared Function getListTransactionAccountSignatoryByIdParty(TranAccID As String) As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
            signatory = objdb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = TranAccID).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListTransactionAccountSignatoryPhoneByIdParty(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
            Phone = objdb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = id And x.isEmployer = isEmp).ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListTransactionAccountSignatoryAddressByIdParty(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
            Address = objdb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = id And x.isEmployer = isEmp).ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'Account--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionAccountByFKParty(id As String) As NawaDevDAL.goAML_Trn_Party_Account
        Dim objFromAccount As New NawaDevDAL.goAML_Trn_Party_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Trn_Party_Account
            Account = objdb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = id).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    Shared Function getTransactionAccountEntityByFKParty(AccID As String) As NawaDevDAL.goAML_Trn_Par_Acc_Entity
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Trn_Par_Acc_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Trn_Par_Acc_Entity
            AccountEntity = objdb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = AccID).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListTransactionAccountEntityPhoneByIdParty(id As String) As List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
            AccountEntityPhone = objdb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListTransactionAccountEntityAddressByIdParty(id As String) As List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
            AccountEntityAddress = objdb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    'Person--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionPersonByFKParty(id As String) As NawaDevDAL.goAML_Trn_Party_Person
        Dim objFromPerson As New NawaDevDAL.goAML_Trn_Party_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Trn_Party_Person
            Person = objdb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = id).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListTransactionPersonPhoneByIdParty(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
            Phone = objdb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = id And x.isemployer = isEmp).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionPersonAddressByIdParty(id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
            Address = objdb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = id And x.isemployer = isEmp).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'Entity--------------------------------------------------------------------------------------------------------------
    Shared Function getTransactionEntityByFKParty(id As String) As NawaDevDAL.goAML_Trn_Party_Entity
        Dim objEntity As New NawaDevDAL.goAML_Trn_Party_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Trn_Party_Entity
            Entity = objdb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = id).FirstOrDefault
            If Entity IsNot Nothing Then
                objEntity = Entity
            End If
        End Using
        Return objEntity
    End Function
    Shared Function getListTransactionEntityPhoneByIdParty(id As String) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
            Phone = objdb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListTransactionEntityAddressByIdParty(id As String) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
            Address = objdb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
#End Region

#Region "Get Data Activity"
    Shared Function getActReportPartyTypeByFK(id As String) As List(Of NawaDevDAL.goAML_Act_ReportPartyType)
        Dim objActReportType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ActReportType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)
            ActReportType = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_ReportPartyType)("select * from goAML_Act_ReportPartyType with(nolock) where FK_Report_ID = " & id).ToList
            If ActReportType IsNot Nothing Then
                objActReportType = ActReportType
            End If
        End Using
        Return objActReportType
    End Function
    'Start Activity Account -------------------------------------------------------------------------------------------------------------
    Shared Function getActAccountByFK(id As String) As NawaDevDAL.goAML_Act_Account
        Dim objFromAccount As New NawaDevDAL.goAML_Act_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Act_Account
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Account)("select * from goAML_Act_Account with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Account IsNot Nothing Then
                objFromAccount = Account
            End If
        End Using
        Return objFromAccount
    End Function
    'Activity Account Entity
    Shared Function getActAccountEntityByFK(AccReport As String) As NawaDevDAL.goAML_Act_Entity_Account
        Dim objFromAccountEntity As New NawaDevDAL.goAML_Act_Entity_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntity As New NawaDevDAL.goAML_Act_Entity_Account
            AccountEntity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Account)("select * from goAML_Act_Entity_Account with(nolock) where FK_Activity_ReportParty_ID = " & AccReport).FirstOrDefault
            If AccountEntity IsNot Nothing Then
                objFromAccountEntity = AccountEntity
            End If
        End Using
        Return objFromAccountEntity
    End Function
    Shared Function getListActAccountEntityPhoneById(id As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
        Dim objFromAccountEntityPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
            AccountEntityPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)("select * from goAML_Act_Acc_Entity_Phone with(nolock) where FK_Act_Acc_Entity_ID = " & id).ToList
            If AccountEntityPhone IsNot Nothing Then
                objFromAccountEntityPhone = AccountEntityPhone
            End If
        End Using
        Return objFromAccountEntityPhone
    End Function
    Shared Function getListActAccountEntityAddressById(id As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
        Dim objFromAccountEntityAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
            AccountEntityAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)("select * from goAML_Act_Acc_Entity_Address with(nolock) where FK_Act_Acc_Entity_ID = " & id).ToList
            If AccountEntityAddress IsNot Nothing Then
                objFromAccountEntityAddress = AccountEntityAddress
            End If
        End Using
        Return objFromAccountEntityAddress
    End Function
    'Activity Account Entity Director
    Shared Function getListActAccountEntityDirectorById(EntityId As String) As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)("select * from goAML_Act_Acc_Ent_Director with(nolock) where FK_Act_Acc_Entity_ID = " & EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListActAccountEntityDirectorPhoneById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)("select * from goAML_Act_Acc_Entity_Director_Phone with(nolock) where FK_Act_Entity_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListActAccountEntityDirectorAddressById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)("select * from goAML_Act_Acc_Entity_Director_address with(nolock) where FK_Act_Entity_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    Shared Function getListActIdentificationById(Id As String, TypeID As Integer) As List(Of NawaDevDAL.goAML_Activity_Person_Identification)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
            Identification = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Activity_Person_Identification)("select * from goAML_activity_Person_Identification with(nolock) where FK_Act_Person_ID = " & Id & " and FK_Person_Type = " & TypeID).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
    'Account Signatory
    Shared Function getListActAccountSignatoryById(TranAccID As String) As List(Of NawaDevDAL.goAML_Act_acc_Signatory)
        Dim objTransactionAccSignatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
            signatory = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_acc_Signatory)("select * from goAML_Act_acc_Signatory with(nolock) where FK_Activity_Account_ID = " & TranAccID).ToList
            If signatory IsNot Nothing Then
                objTransactionAccSignatory = signatory
            End If
        End Using
        Return objTransactionAccSignatory
    End Function
    Shared Function getListActAccountSignatoryPhoneById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
        Dim objTransactionAccSignatoryPhone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)("select * from goAML_Act_Acc_sign_Phone with(nolock) where FK_Act_Acc_Entity_ID = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionAccSignatoryPhone = Phone
            End If
        End Using
        Return objTransactionAccSignatoryPhone
    End Function
    Shared Function getListActAccountSignatoryAddressById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
        Dim objTransactionAccSignatoryAddress As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Address)("select * from goAML_Act_Acc_sign_Address with(nolock) where FK_Act_Acc_Entity = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionAccSignatoryAddress = Address
            End If
        End Using
        Return objTransactionAccSignatoryAddress
    End Function
    'End Activity Account -------------------------------------------------------------------------------------------------------------

    'Start Activity Person
    Shared Function getActPersonByFK(id As String) As NawaDevDAL.goAML_Act_Person
        Dim objFromPerson As New NawaDevDAL.goAML_Act_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Person As New NawaDevDAL.goAML_Act_Person
            Person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person)("select * from goAML_Act_Person with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Person IsNot Nothing Then
                objFromPerson = Person
            End If
        End Using
        Return objFromPerson
    End Function
    Shared Function getListActPersonPhoneById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Phone)("select * from goAML_Act_Person_Phone with(nolock) where FK_Act_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListActPersonAddressById(Id As String, isEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Act_Person_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Person_Address)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Address)("select * from goAML_Act_Person_Address with(nolock) where FK_Act_Person = " & Id & " and isEmployer = '" & isEmp & "'").ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'End Activity Person ----------------------------------------------------------------------------------------------------------------

    'Start Activity Entity
    Shared Function getActEntityByFK(id As String) As NawaDevDAL.goAML_Act_Entity
        Dim objFromEntity As New NawaDevDAL.goAML_Act_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Entity As New NawaDevDAL.goAML_Act_Entity
            Entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity)("select * from goAML_Act_Entity with(nolock) where FK_Act_ReportParty_ID = " & id).FirstOrDefault
            If Entity IsNot Nothing Then
                objFromEntity = Entity
            End If
        End Using
        Return objFromEntity
    End Function
    Shared Function getListActEntityPhoneById(Id As String) As List(Of NawaDevDAL.goAML_Act_Entity_Phone)
        Dim objTransactionPersonPhone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)
            Phone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Phone)("select * from goAML_Act_Entity_Phone with(nolock) where FK_Act_Entity_ID = " & Id).ToList
            If Phone IsNot Nothing Then
                objTransactionPersonPhone = Phone
            End If
        End Using
        Return objTransactionPersonPhone
    End Function
    Shared Function getListActEntityAddressById(Id As String) As List(Of NawaDevDAL.goAML_Act_Entity_Address)
        Dim objTransactionPersonAddress As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
            Address = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Address)("select * from goAML_Act_Entity_Address with(nolock) where FK_Act_Entity_ID = " & Id).ToList
            If Address IsNot Nothing Then
                objTransactionPersonAddress = Address
            End If
        End Using
        Return objTransactionPersonAddress
    End Function
    'Activity Entity Director
    Shared Function getListActEntityDirectorById(EntityId As String) As List(Of NawaDevDAL.goAML_Act_Director)
        Dim objFromAccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirector As New List(Of NawaDevDAL.goAML_Act_Director)
            AccountEntityDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director)("select * from goAML_Act_Director with(nolock) where FK_Act_Entity_ID = " & EntityId).ToList
            If AccountEntityDirector IsNot Nothing Then
                objFromAccountEntityDirector = AccountEntityDirector
            End If
        End Using
        Return objFromAccountEntityDirector
    End Function
    Shared Function getListActEntityDirectorPhoneById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Phone)
        Dim objFromAccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorPhone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)
            AccountEntityDirectorPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Phone)("select * from goAML_Act_Director_Phone with(nolock) where FK_Act_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorPhone IsNot Nothing Then
                objFromAccountEntityDirectorPhone = AccountEntityDirectorPhone
            End If
        End Using
        Return objFromAccountEntityDirectorPhone
    End Function
    Shared Function getListActEntityDirectorAddressById(Id As String, IsEmp As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Address)
        Dim objFromAccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim AccountEntityDirectorAddress As New List(Of NawaDevDAL.goAML_Act_Director_Address)
            AccountEntityDirectorAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Address)("select * from goAML_Act_Director_Address with(nolock) where FK_Act_Director_ID =" & Id & " and isEmployer ='" & IsEmp & "'").ToList
            If AccountEntityDirectorAddress IsNot Nothing Then
                objFromAccountEntityDirectorAddress = AccountEntityDirectorAddress
            End If
        End Using
        Return objFromAccountEntityDirectorAddress
    End Function
    'End Activity Entity ----------------------------------------------------------------------------------------------------------------
#End Region
    '-------------------------------------------------------------------------------------------------------------------------------
    Shared Function getRolePartybyCode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Party_Role = objdb.goAML_Ref_Party_Role.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getCountryByCode(code As String) As String
        Dim countryName As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim country As NawaDevDAL.goAML_Ref_Nama_Negara = objdb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = code).FirstOrDefault
            If country IsNot Nothing Then
                countryName = country.Keterangan
            End If
        End Using
        Return countryName
    End Function
    Shared Function getStatusReportByCode(code As String) As String
        Dim statusName As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim status As NawaDevDAL.goAML_Ref_Status_Report = objdb.goAML_Ref_Status_Report.Where(Function(x) x.PK_Status_Report_ID = code).FirstOrDefault
            If status IsNot Nothing Then
                statusName = status.Description
            End If
        End Using
        Return statusName
    End Function
    Shared Function getStatusReportByCodeInt(code As String) As Integer
        Dim statusName As Integer = 0
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim status As NawaDevDAL.goAML_Ref_Status_Report = objdb.goAML_Ref_Status_Report.Where(Function(x) x.Description = code).FirstOrDefault
            If status IsNot Nothing Then
                statusName = status.PK_Status_Report_ID
            End If
        End Using
        Return statusName
    End Function
    Shared Function getSessionReportingPerson(code As Integer) As Integer
        Dim status As Integer = 0
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim report As NawaDevDAL.goAML_Report = objdb.goAML_Report.Where(Function(x) x.PK_Report_ID = code).FirstOrDefault
            Dim mapping As NawaDevDAL.goAML_ref_mapping_JenisLaporan_JenisTransaksi = objdb.goAML_ref_mapping_JenisLaporan_JenisTransaksi.Where(Function(x) x.KODE_LAPORAN = report.Report_Code).FirstOrDefault
            Dim jenis As NawaDevDAL.goAML_Ref_Jenis_Laporan = objdb.goAML_Ref_Jenis_Laporan.Where(Function(x) x.Kode = mapping.KODE_LAPORAN).FirstOrDefault
            Dim mode As NawaDevDAL.goAML_Ref_ReportGenerateMode = objdb.goAML_Ref_ReportGenerateMode.Where(Function(x) x.ReportCodeType = jenis.ReportType).FirstOrDefault
            If mode IsNot Nothing Then
                status = mode.goAML_Ref_GenerateMode
            End If
        End Using
        Return status
    End Function
    Shared Function getJenisLaporanAttachment(code As String) As String
        Dim status As String
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim jenis As NawaDevDAL.goAML_Ref_Jenis_Laporan = objdb.goAML_Ref_Jenis_Laporan.Where(Function(x) x.Kode = code).FirstOrDefault
            If jenis IsNot Nothing Then
                status = jenis.ReportType
            End If
        End Using
        Return status
    End Function
    Shared Function getAddressbyFKRefDetail(fk As Integer) As NawaDevDAL.goAML_Ref_Address
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim addressLocation As NawaDevDAL.goAML_Ref_Address = objdb.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = fk).FirstOrDefault
            Return addressLocation
        End Using
    End Function
    Shared Function getTypeKontakAlamatbyKode(Kode As String) As String
        Dim strKategori As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim kategori As NawaDevDAL.goAML_Ref_Kategori_Kontak = objdb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If kategori IsNot Nothing Then
                strKategori = kategori.Keterangan
            End If
        End Using
        Return strKategori
    End Function
    Shared Function getSubmissionTypeByCode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Submission_Type = objdb.goAML_Ref_Submission_Type.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getJenisLaporanByCode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Laporan = objdb.goAML_Ref_Jenis_Laporan.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getInstrumenByCode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Instrumen_Transaksi = objdb.goAML_Ref_Instrumen_Transaksi.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getTransactionTypebyPK(Kode As Integer) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Transaction_Type = objdb.goAML_Ref_Transaction_Type.Where(Function(x) x.PK_Transaction_Type_ID = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Transaction_Type
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getRoleTypebyCode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Peran_orang_dalam_rekening = objdb.goAML_Ref_Peran_orang_dalam_rekening.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getRoleKorporasiTypebyCode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Peran_orang_dalam_Korporasi = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getCurrencyByCode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Mata_Uang = objdb.goAML_Ref_Mata_Uang.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getSenderInformationByPK(PK As Integer) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Sender_To_Information = objdb.goAML_Ref_Sender_To_Information.Where(Function(x) x.PK_Sender_To_Information = PK).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Sender_To_Information
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getStatusRekeningByKode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Status_Rekening = objdb.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getReportTypebyPK(pk As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Report_Type = objdb.goAML_Ref_Report_Type.Where(Function(x) x.PK_Ref_Report_Type = pk).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Description
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getNotReportTypeByPk(pk As Integer) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Report_Type = objdb.goAML_Ref_Report_Type.Where(Function(x) x.PK_Ref_Report_Type = pk).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Description
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getAccountTyperByKode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Rekening = objdb.goAML_Ref_Jenis_Rekening.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getBentukKorporasiByKode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Bentuk_Badan_Usaha = objdb.goAML_Ref_Bentuk_Badan_Usaha.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getPeranDalamKorporasiByKode(Kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Peran_orang_dalam_Korporasi = objdb.goAML_Ref_Peran_orang_dalam_Korporasi.Where(Function(x) x.Kode = Kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getGenderbyKode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Kelamin = objdb.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function

    Shared Function getjenisDokumenByKode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Dokumen_Identitas = objdb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function
    Shared Function getjenisAlatKomunikasiByKode(kode As String) As String
        Dim StringKeterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim obj As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi = objdb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = kode).FirstOrDefault
            If obj IsNot Nothing Then
                StringKeterangan = obj.Keterangan
            End If
            Return StringKeterangan
        End Using
    End Function

    '==============Function Multiparty untuk Edit==========================
    Shared Function getAccountPartyByFKReportID(id As String) As NawaDevDAL.goAML_Trn_Party_Account
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Account
            objfromDB = objdb.goAML_Trn_Party_Account.Where(Function(x) x.FK_Report_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getAccountPartyByFKID(id As String) As NawaDevDAL.goAML_Trn_Party_Account
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Account
            objfromDB = objdb.goAML_Trn_Party_Account.Where(Function(x) x.PK_Trn_Party_Account_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getAPersonPartyByFKReportID(id As String) As NawaDevDAL.goAML_Trn_Party_Person
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Person
            objfromDB = objdb.goAML_Trn_Party_Person.Where(Function(x) x.FK_Report_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getAPersonPartyByID(id As String) As NawaDevDAL.goAML_Trn_Party_Person
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Person
            objfromDB = objdb.goAML_Trn_Party_Person.Where(Function(x) x.PK_Trn_Party_Person_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getAEntityPartyByFKReportID(id As String) As NawaDevDAL.goAML_Trn_Party_Entity
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Entity
            objfromDB = objdb.goAML_Trn_Party_Entity.Where(Function(x) x.FK_Report_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getAEntityPartyByID(id As String) As NawaDevDAL.goAML_Trn_Party_Entity
        Dim obj As New NawaDevDAL.goAML_Trn_Party_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Party_Entity
            objfromDB = objdb.goAML_Trn_Party_Entity.Where(Function(x) x.PK_Trn_Party_Entity_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getEntityAccountPartyByFKAccount(id As String) As NawaDevDAL.goAML_Trn_Par_Acc_Entity
        Dim obj As New NawaDevDAL.goAML_Trn_Par_Acc_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim objfromDB As New NawaDevDAL.goAML_Trn_Par_Acc_Entity
            objfromDB = objdb.goAML_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = id).FirstOrDefault
            If objfromDB IsNot Nothing Then
                obj = objfromDB
            End If
        End Using
        Return obj
    End Function
    Shared Function getListReportGenerateXML(Dates As String, Code As String) As List(Of NawaDevDAL.goAML_Report)
        Dim listObj As New List(Of NawaDevDAL.goAML_Report)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Report)
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Report)("SELECT * FROM goAML_Report WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND report_code = '" & Code & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListReportGenerateXML(Dates As String, Code As String, datelast As String) As List(Of NawaDevDAL.goAML_Report)
        Dim listObj As New List(Of NawaDevDAL.goAML_Report)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Report)
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Report)("SELECT * FROM goAML_Report WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND report_code = '" & Code & "' AND CONVERT(DATE,LastUpdateDate) = '" & datelast & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListReportGenerateXML(Dates As String, Code As String, listReport As List(Of NawaDevDAL.goAML_Report)) As List(Of NawaDevDAL.goAML_Report)
        Dim listObj As New List(Of NawaDevDAL.goAML_Report)
        Dim a As Integer = 0
        Dim id As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Report)
            For Each item In listReport
                If a < listReport.Count Then
                    id = id & "'" & item.LastUpdateDate & "'"
                    a = a + listReport.Count
                ElseIf a > listReport.Count - 1 Then
                    id = id & ",'" & item.LastUpdateDate & "'"
                End If
            Next
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Report)("SELECT * FROM goAML_Report WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND report_code = '" & Code & "' AND CONVERT(DATE,LastUpdateDate) IN (" & id & ")").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListReportGenerateXML(Dates As String, listReport As List(Of NawaDevDAL.goAML_Report)) As List(Of NawaDevDAL.goAML_Report)
        Dim listObj As New List(Of NawaDevDAL.goAML_Report)
        Dim a As Integer = 0
        Dim id As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Report)
            For Each item In listReport
                If a < listReport.Count Then
                    id = id & "'" & item.LastUpdateDate & "'"
                    a = a + listReport.Count
                ElseIf a > listReport.Count - 1 Then
                    id = id & ",'" & item.LastUpdateDate & "'"
                End If
            Next
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Report)("SELECT * FROM goAML_Report WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND CONVERT(DATE,LastUpdateDate) IN (" & id & ")").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListGenerateXML(Dates As String, Code As String, tgl As String) As List(Of NawaDevDAL.goAML_Generate_XML)
        Dim listObj As New List(Of NawaDevDAL.goAML_Generate_XML)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Generate_XML)
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Generate_XML)("SELECT * FROM goAML_Generate_XML WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND Jenis_Laporan = '" & Code & "' AND CONVERT(DATE,Last_Update_Date) = '" & tgl & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListGenerateXMLOld(Dates As String, Code As String) As NawaDevDAL.goAML_Generate_XML
        Dim listObj As New NawaDevDAL.goAML_Generate_XML
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New NawaDevDAL.goAML_Generate_XML
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Generate_XML)("SELECT * FROM goAML_Generate_XML WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND Jenis_Laporan = '" & Code & "' AND Last_Update_Date IS NOT NULL").FirstOrDefault
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListGenerateXMLNew(Dates As String, Code As String) As NawaDevDAL.goAML_Generate_XML
        Dim listObj As New NawaDevDAL.goAML_Generate_XML
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New NawaDevDAL.goAML_Generate_XML
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Generate_XML)("SELECT * FROM goAML_Generate_XML WHERE CONVERT(DATE,transaction_date) = '" & Dates & "' AND Jenis_Laporan = '" & Code & "' AND Last_Update_Date IS NULL").FirstOrDefault
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getListAttachment(pk As String) As NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
        Dim ListFromDB As New NawaDevDAL.goAML_ODM_Generate_STR_SAR_Attachment
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            ListFromDB = objdb.goAML_ODM_Generate_STR_SAR_Attachment.Where(Function(x) x.Fk_goAML_ODM_Generate_STR_SAR = pk).FirstOrDefault
        End Using
        Return ListFromDB
    End Function
    Shared Function getAddressEntityAccountPartyByFKEntity(id As String) As List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)
            'ListFromDB = objdb.goAML_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = id).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_par_Acc_Entity_Address)("select * from goAML_Trn_par_Acc_Entity_Address with(nolock) where FK_Trn_par_acc_Entity_ID =" & id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getPhoneEntityAccountPartyByFKEntity(id As String) As List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_par_acc_Entity_Phone)("select * from goAML_trn_par_acc_Entity_Phone with(nolock) where FK_Trn_par_acc_Entity_ID =" & id).ToList
            'ListFromDB = objdb.goAML_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getSignatoryAccountPartyByFKAccount(id As String) As List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)
            'ListFromDB = objdb.goAML_Trn_par_acc_Signatory.Where(Function(x) x.FK_Trn_Party_Account_ID = id).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_par_acc_Signatory)("select * from goAML_Trn_par_acc_Signatory with(nolock) where FK_Trn_Party_Account_ID =" & id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getAddressSignatoryAccountPartyByFKSignatory(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)
            ' ListFromDB = objdb.goAML_Trn_par_Acc_sign_Address.Where(Function(x) x.FK_Trn_par_acc_Signatory_ID = id And x.isEmployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_par_Acc_sign_Address)("select * from goAML_Trn_par_Acc_sign_Address with(nolock) where FK_Trn_par_acc_Signatory_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getPhoneSignatoryAccountPartyByFKSignatory(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)
            '  ListFromDB = objdb.goAML_trn_par_acc_sign_Phone.Where(Function(x) x.FK_Trn_Par_Acc_Sign = id And x.isEmployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_trn_par_acc_sign_Phone)("select * from goAML_trn_par_acc_sign_Phone with(nolock) where FK_Trn_Par_Acc_Sign =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getIdentificationPartyByFKTableAndFKPersonType(id As String, FKPersonType As Integer) As List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
        Dim listObj As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Transaction_Party_Identification)
            'ListFromDB = objdb.goAML_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = id And x.FK_Person_Type = FKPersonType).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Transaction_Party_Identification)("select * from goAML_Transaction_Party_Identification with(nolock) where FK_Person_ID =" & id & " and FK_Person_Type ='" & FKPersonType & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function

    Shared Function getDirectorEntityAccountPartyByFKEntityID(id As String) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director)
            ListFromDB = objdb.goAML_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getAddressDirectorEntityAccountPartyByFKDirectorID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)
            ' ListFromDB = objdb.goAML_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Address)("select * from goAML_Trn_Par_Acc_Ent_Director_Address with(nolock) where FK_Trn_par_acc_Entity_Director_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getPhoneDirectorEntityAccountPartyByFKDirectorID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)
            'ListFromDB = objdb.goAML_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Par_Acc_Ent_Director_Phone)("select * from goAML_Trn_Par_Acc_Ent_Director_Phone with(nolock) where FK_Trn_par_acc_Entity_Director_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getPhonePersonPartyByFKPersonID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)
            'ListFromDB = objdb.goAML_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Party_Person_Phone)("select * from goAML_Trn_Party_Person_Phone with(nolock) where FK_Trn_Party_Person_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getAddressPersonPartyByFKPersonID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Party_Person_Address)
            ' ListFromDB = objdb.goAML_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Party_Person_Address)("select * from goAML_Trn_Party_Person_Address with(nolock) where FK_Trn_Party_Person_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getAddressEntityPartyByFKEntityID(id As String) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)
            'ListFromDB = objdb.goAML_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = id).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Party_Entity_Address)("select * from goAML_Trn_Party_Entity_Address with(nolock) where FK_Trn_Party_Entity_ID =" & id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj

    End Function
    Shared Function getPhoneEntityPartyByFKEntityID(id As String) As List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)
            ' ListFromDB = objdb.goAML_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = id).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Party_Entity_Phone)("select * from goAML_Trn_Party_Entity_Phone with(nolock) where FK_Trn_Party_Entity_ID =" & id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj

    End Function
    Shared Function getDirectorEntityPartyByFKEntityID(id As String) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director)
            ListFromDB = objdb.goAML_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = id).ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getAddressDirectorEntityPartyByFKDirectorID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)
            ' ListFromDB = objdb.goAML_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Address)("select * from goAML_Trn_Par_Entity_Director_Address with(nolock) where FK_Trn_par_Entity_Director_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function
    Shared Function getPhoneDirectorEntityPartyByFKDirectorID(id As String, isemployer As Boolean) As List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
        Dim listObj As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ListFromDB As New List(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)
            ' ListFromDB = objdb.goAML_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = id And x.isemployer = isemployer).ToList
            ListFromDB = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Trn_Par_Entity_Director_Phone)("select * from goAML_Trn_Par_Entity_Director_Phone with(nolock) where FK_Trn_par_Entity_Director_ID =" & id & " and isEmployer ='" & isemployer & "'").ToList
            If ListFromDB IsNot Nothing Then
                listObj = ListFromDB
            End If
        End Using
        Return listObj
    End Function

    Shared Function getListIndicator(id As String) As List(Of NawaDevDAL.goAML_Report_Indicator)
        Dim ListIndicator As New List(Of NawaDevDAL.goAML_Report_Indicator)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Indicator As New List(Of NawaDevDAL.goAML_Report_Indicator)
            Indicator = objdb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = id).ToList
            If Indicator IsNot Nothing Then
                ListIndicator = Indicator
            End If
        End Using
        Return ListIndicator
    End Function
    Shared Function getIndicator(id As String) As NawaDevDAL.goAML_Report_Indicator
        Dim ListIndicator As New NawaDevDAL.goAML_Report_Indicator
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Indicator As New NawaDevDAL.goAML_Report_Indicator
            Indicator = objdb.goAML_Report_Indicator.Where(Function(x) x.PK_Report_Indicator = id).FirstOrDefault
            If Indicator IsNot Nothing Then
                ListIndicator = Indicator
            End If
        End Using
        Return ListIndicator
    End Function

    Shared Function getValidationFromType(transmodeCode As String, instrumenFrom As String, SenderInformation As Integer, myClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If SenderInformation = 1 Then ' Account
                If myClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Account MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Account NotMyClient"
                    End If
                End If

            ElseIf SenderInformation = 2 Then 'Person
                If myClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Person MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Person NotMyClient"
                    End If
                End If

            ElseIf SenderInformation = 3 Then 'Entity
                If myClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_MyClient_From_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Entity MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.BiParty_NotMyClient_From_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi pengirim Entity NotMyClient"
                    End If
                End If
            End If
        End Using

        Return keterangan
    End Function
    Shared Function getValidationToType(transmodeCode As String, instrumenFrom As String, instrumenTo As String, PenerimaInformation As Integer, TomyClient As Boolean) As String
        Dim keterangan As String = ""
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            If PenerimaInformation = 1 Then ' Account
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Account MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Account = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Account NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 2 Then 'Person
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Person MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Person = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Person NotMyClient"
                    End If
                End If

            ElseIf PenerimaInformation = 3 Then 'Entity
                If TomyClient Then
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_MyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Entity MyClient"
                    End If
                Else
                    Dim obj As NawaDevDAL.goAML_ODM_FROM_TO_Validation = objdb.goAML_ODM_FROM_TO_Validation.Where(Function(x) x.Kode_Jenis_Transaksi = transmodeCode And x.Kode_Instrumen_Dari = instrumenFrom And x.Kode_Instrumen_Kepada = instrumenTo And x.BiParty_NotMyClient_To_Entity = True).FirstOrDefault
                    If obj Is Nothing Then
                        keterangan = " Tidak bisa memilih informasi penerima Entity NotMyClient"
                    End If
                End If

            End If
        End Using

        Return keterangan
    End Function

    'Shared Function getReportByPK(PK As Integer) As NawaDevBLL.Report
    '    Dim reportClass As New NawaDevBLL.Report
    '    Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    '        Dim Report As NawaDevDAL.GoAML_Report = objDb.GoAML_Report.Where(Function(x) x.PK_Report_ID = PK).FirstOrDefault
    '        Dim listTransaction As List(Of NawaDevDAL.goAML_Transaction) = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = Report.PK_Report_ID).ToList
    '        Dim listTransactionClass As New List(Of NawaDevBLL.Transaction)
    '        For Each item In listTransaction
    '            Dim transactionClass As New NawaDevBLL.Transaction
    '            Dim transaction As NawaDevDAL.goAML_Transaction = objDb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = item.PK_Transaction_ID).FirstOrDefault
    '            If transaction.FK_Sender_From_Information = 1 Then ' account
    '                Dim account As NawaDevDAL.goAML_Transaction_Account = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = transaction.PK_Transaction_ID).FirstOrDefault

    '                Dim EntityAccount As NawaDevDAL.goAML_Trn_Entity_account = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = account.PK_Account_ID And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '                Dim listAddressEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address) = Nothing
    '                Dim listPhoneEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone) = Nothing

    '                Dim listDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director) = Nothing
    '                Dim listAddressDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listPhoneEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listAddressEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing

    '                Dim listSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory) = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = account.PK_Account_ID And x.FK_From_Or_To = 1).ToList
    '                Dim listAddressSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '                Dim listPhoneSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing
    '                Dim listAddressEmployerSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '                Dim listPhoneEmployerSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing

    '                For Each sign In listSignatory
    '                    Dim addressSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault
    '                    Dim phoneSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault

    '                    Dim addressEmployerSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault
    '                    Dim phoneEmployerSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault

    '                    If addressSignatory IsNot Nothing Then
    '                        listAddressSignatory.Add(addressSignatory)
    '                    End If
    '                    If phoneSignatory IsNot Nothing Then
    '                        listPhoneSignatory.Add(phoneSignatory)
    '                    End If
    '                    If addressEmployerSignatory IsNot Nothing Then
    '                        listAddressEmployerSignatory.Add(addressEmployerSignatory)
    '                    End If
    '                    If phoneEmployerSignatory IsNot Nothing Then
    '                        listPhoneEmployerSignatory.Add(phoneEmployerSignatory)
    '                    End If

    '                Next
    '                If account.isRekeningKorporasi = 1 Then
    '                    listAddressEntityAccount = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                    listPhoneEntityAccount = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                    listDirectorEntityAccount = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityAccount.PK_goAML_Trn_Entity_account And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).ToList
    '                    If listDirectorEntityAccount.Count > 0 Then
    '                        For Each directorEntity In listDirectorEntityAccount
    '                            Dim listAddressEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                            Dim listPhoneEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                            Dim listAddressEmployerEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                            Dim listPhoneEMployerEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                            If listAddressEntityAccountX.Count > 0 Then
    '                                For Each AEA In listAddressEntityAccountX
    '                                    listAddressDirectorEntityAccount.Add(AEA)
    '                                Next
    '                            End If
    '                            If listPhoneEntityAccountX.Count > 0 Then
    '                                For Each PEAX In listPhoneEntityAccountX
    '                                    listPhoneDirectorEntityAccount.Add(PEAX)
    '                                Next
    '                            End If
    '                            If listAddressEmployerEntityAccountX.Count > 0 Then
    '                                For Each AEEAX In listAddressEmployerEntityAccountX
    '                                    listAddressEmployerDirectorEntityAccount.Add(AEEAX)
    '                                Next
    '                            End If
    '                            If listPhoneEMployerEntityAccountX.Count Then
    '                                For Each PEEAX In listPhoneEMployerEntityAccountX
    '                                    listPhoneEmployerDirectorEntityAccount.Add(PEEAX)
    '                                Next
    '                            End If
    '                        Next
    '                    End If
    '                    With transactionClass
    '                        .objAccountFrom = account
    '                        .listObjAddressAccountEntityFrom = listAddressEntityAccount
    '                        .listObjPhoneAccountEntityFrom = listPhoneEntityAccount
    '                        .LisAccountSignatoryFrom = listSignatory
    '                        .listAddressAccountSignatoryFrom = listAddressSignatory
    '                        .listPhoneAccountSignatoryFrom = listPhoneSignatory
    '                        .listPhoneAccountEmployerSignatoryFrom = listPhoneEmployerSignatory
    '                        .listAddressAccountEmployerSignatoryFrom = listAddressEmployerSignatory
    '                        .objAccountEntityFrom = EntityAccount
    '                        .listDirectorAccountEntityFrom = listDirectorEntityAccount
    '                        .listAddressDirectorAccountEntityFrom = listAddressDirectorEntityAccount
    '                        .listPhoneDirectorAccountEntityFrom = listPhoneDirectorEntityAccount
    '                        .listAddressEmployerDirectorAccountEntityFrom = listAddressEmployerDirectorEntityAccount
    '                        .listPhoneEmployerDirectorAccountEntityFrom = listPhoneEmployerDirectorEntityAccount

    '                    End With
    '                End If
    '            ElseIf transaction.FK_Sender_From_Information = 2 Then 'person
    '                Dim person As NawaDevDAL.goAML_Transaction_Person = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '                Dim listAddressPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '                Dim listPhonePerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '                Dim listAddressEMployerPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '                Dim listPhoneEmployerPerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '                Dim IdentificationPerson As List(Of NawaDevDAL.goAML_Transaction_Person_Identification) = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = person.PK_Person_ID And x.FK_Person_Type = 1).ToList
    '                With transactionClass
    '                    .objPersonFrom = person
    '                    If listAddressPerson.Count > 0 Then
    '                        .listObjAddressPersonFrom = listAddressPerson
    '                    End If
    '                    If listPhonePerson.Count > 0 Then
    '                        .listObjPhonePersonFrom = listPhonePerson
    '                    End If
    '                    If listPhoneEmployerPerson.Count > 0 Then
    '                        .listObjPhoneEmployerPersonFrom = listPhoneEmployerPerson
    '                    End If
    '                    If listAddressEMployerPerson.Count > 0 Then
    '                        .listObjAddressEmployerPersonFrom = listAddressEMployerPerson
    '                    End If
    '                    If IdentificationPerson.Count > 0 Then
    '                        .listObjIdentificationFrom = IdentificationPerson
    '                    End If
    '                End With

    '            ElseIf transaction.FK_Sender_From_Information = 3 Then 'entity
    '                Dim entity As NawaDevDAL.goAML_Transaction_Entity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '                Dim listAddressEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Address) = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '                Dim listPhoneEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Phone) = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '                Dim listDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director) = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = entity.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Transaction_ID = transaction.PK_Transaction_ID).ToList
    '                Dim listAddressDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listAddressEmployerDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneEmployerDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                For Each directorEntity In listDirectorEntity
    '                    Dim listAddress As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                    Dim listPhone As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                    Dim listAddressEmployer As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                    Dim listPhoneEmployer As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                    If listAddress.Count > 0 Then
    '                        For Each address In listAddress
    '                            listAddressDirectorEntity.Add(address)
    '                        Next
    '                    End If
    '                    If listPhone.Count > 0 Then
    '                        For Each phone In listPhone
    '                            listPhoneDirectorEntity.Add(phone)
    '                        Next
    '                    End If
    '                    If listAddressEmployer.Count > 0 Then
    '                        For Each address In listAddressEmployer
    '                            listAddressEmployerDirectorEntity.Add(address)
    '                        Next
    '                    End If
    '                    If listPhoneEmployer.Count > 0 Then
    '                        For Each phone In listPhoneEmployer
    '                            listPhoneEmployerDirectorEntity.Add(phone)
    '                        Next
    '                    End If
    '                Next
    '                With transactionClass
    '                    .objEntityFrom = entity
    '                    .ListObjAddressEntityFrom = listAddressEntity
    '                    .listObjPhoneEntityFrom = listPhoneEntity
    '                    .listDirectorEntityFrom = listDirectorEntity
    '                    .listAddressDirectorEntityFrom = listAddressDirectorEntity
    '                    .listPhoneDirectorEntityFrom = listPhoneDirectorEntity
    '                    .listAddressEmployerDirectorEntityFrom = listAddressEmployerDirectorEntity
    '                    .listPhoneEmployerDirectorEntityFrom = listPhoneEmployerDirectorEntity

    '                End With

    '            End If
    '            If transaction.FK_Sender_To_Information = 1 Then ' Account
    '                Dim account As NawaDevDAL.goAML_Transaction_Account = objDb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = transaction.PK_Transaction_ID).FirstOrDefault

    '                Dim EntityAccount As NawaDevDAL.goAML_Trn_Entity_account = objDb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = account.PK_Account_ID And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '                Dim listAddressEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address) = Nothing
    '                Dim listPhoneEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone) = Nothing

    '                Dim listDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director) = Nothing
    '                Dim listAddressDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listPhoneEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listAddressEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing

    '                Dim listSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory) = objDb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = account.PK_Account_ID And x.FK_From_Or_To = 1).ToList
    '                Dim listAddressSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '                Dim listPhoneSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing
    '                Dim listAddressEmployerSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '                Dim listPhoneEmployerSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing

    '                For Each sign In listSignatory
    '                    Dim addressSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault
    '                    Dim phoneSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault

    '                    Dim addressEmployerSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objDb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault
    '                    Dim phoneEmployerSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objDb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault

    '                    If addressSignatory IsNot Nothing Then
    '                        listAddressSignatory.Add(addressSignatory)
    '                    End If
    '                    If phoneSignatory IsNot Nothing Then
    '                        listPhoneSignatory.Add(phoneSignatory)
    '                    End If
    '                    If addressEmployerSignatory IsNot Nothing Then
    '                        listAddressEmployerSignatory.Add(addressEmployerSignatory)
    '                    End If
    '                    If phoneEmployerSignatory IsNot Nothing Then
    '                        listPhoneEmployerSignatory.Add(phoneEmployerSignatory)
    '                    End If

    '                Next
    '                If account.isRekeningKorporasi = 1 Then
    '                    listAddressEntityAccount = objDb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                    listPhoneEntityAccount = objDb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                    listDirectorEntityAccount = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityAccount.PK_goAML_Trn_Entity_account And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).ToList
    '                    If listDirectorEntityAccount.Count > 0 Then
    '                        For Each directorEntity In listDirectorEntityAccount
    '                            Dim listAddressEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                            Dim listPhoneEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                            Dim listAddressEmployerEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                            Dim listPhoneEMployerEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                            If listAddressEntityAccountX.Count > 0 Then
    '                                For Each AEA In listAddressEntityAccountX
    '                                    listAddressDirectorEntityAccount.Add(AEA)
    '                                Next
    '                            End If
    '                            If listPhoneEntityAccountX.Count > 0 Then
    '                                For Each PEAX In listPhoneEntityAccountX
    '                                    listPhoneDirectorEntityAccount.Add(PEAX)
    '                                Next
    '                            End If
    '                            If listAddressEmployerEntityAccountX.Count > 0 Then
    '                                For Each AEEAX In listAddressEmployerEntityAccountX
    '                                    listAddressEmployerDirectorEntityAccount.Add(AEEAX)
    '                                Next
    '                            End If
    '                            If listPhoneEMployerEntityAccountX.Count Then
    '                                For Each PEEAX In listPhoneEMployerEntityAccountX
    '                                    listPhoneEmployerDirectorEntityAccount.Add(PEEAX)
    '                                Next
    '                            End If
    '                        Next
    '                    End If
    '                    With transactionClass
    '                        .objAccountTo = account
    '                        .listObjAddressAccountEntityto = listAddressEntityAccount
    '                        .listObjPhoneAccountEntityto = listPhoneEntityAccount
    '                        .LisAccountSignatoryto = listSignatory
    '                        .listAddressAccountSignatoryto = listAddressSignatory
    '                        .listObjPhoneSignatoryto = listPhoneSignatory
    '                        .listObjPhoneEmployerSignatoryto = listPhoneEmployerSignatory
    '                        .listObjAddressEmployerSignatoryto = listAddressEmployerSignatory
    '                        .objAccountEntityto = EntityAccount
    '                        .listDirectorAccountEntityto = listDirectorEntityAccount
    '                        .listAddressDirectorAccountEntityto = listAddressDirectorEntityAccount
    '                        .listPhoneDirectorAccountEntityto = listPhoneDirectorEntityAccount
    '                        .listAddressEmployerDirectorAccountEntityto = listAddressEmployerDirectorEntityAccount
    '                        .listPhoneEmployerDirectorAccountEntityto = listPhoneEmployerDirectorEntityAccount

    '                    End With
    '                End If
    '            ElseIf transaction.FK_Sender_To_Information = 2 Then ' person
    '                Dim person As NawaDevDAL.goAML_Transaction_Person = objDb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
    '                Dim listAddressPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '                Dim listPhonePerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '                Dim listAddressEmployerPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objDb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '                Dim listPhoneEmployerPerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objDb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '                Dim IdentificationPerson As List(Of NawaDevDAL.goAML_Transaction_Person_Identification) = objDb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = person.PK_Person_ID And x.FK_Person_Type = 1).ToList
    '                With transactionClass
    '                    .objPersonTo = person
    '                    If listAddressPerson.Count > 0 Then
    '                        .listObjAddressPersonTo = listAddressPerson
    '                    End If
    '                    If listPhonePerson.Count > 0 Then
    '                        .listObjPhonePersonTo = listPhonePerson
    '                    End If
    '                    If listPhoneEmployerPerson.Count > 0 Then
    '                        .listObjPhoneEmployerPersonTo = listPhoneEmployerPerson
    '                    End If
    '                    If listAddressEMployerPerson.Count > 0 Then
    '                        .listObjAddressEmployerPersonTo = listAddressEMployerPerson
    '                    End If
    '                    If IdentificationPerson.Count > 0 Then
    '                        .listObjIdentificationPersonTo = IdentificationPerson
    '                    End If
    '                End With
    '            ElseIf transaction.FK_Sender_To_Information = 3 Then 'Entity
    '                Dim entity As NawaDevDAL.goAML_Transaction_Entity = objDb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
    '                Dim listAddressEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Address) = objDb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '                Dim listPhoneEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Phone) = objDb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '                Dim listDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director) = objDb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = entity.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Transaction_ID = transaction.PK_Transaction_ID).ToList
    '                Dim listAddressDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                Dim listAddressEmployerDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '                Dim listPhoneEmployerDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '                For Each directorEntity In listDirectorEntity
    '                    Dim listAddress As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                    Dim listPhone As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                    Dim listAddressEmployer As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objDb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                    Dim listPhoneEmployer As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objDb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                    If listAddress.Count > 0 Then
    '                        For Each address In listAddress
    '                            listAddressDirectorEntity.Add(address)
    '                        Next
    '                    End If
    '                    If listPhone.Count > 0 Then
    '                        For Each phone In listPhone
    '                            listPhoneDirectorEntity.Add(phone)
    '                        Next
    '                    End If
    '                    If listAddressEmployer.Count > 0 Then
    '                        For Each address In listAddressEmployer
    '                            listAddressEmployerDirectorEntity.Add(address)
    '                        Next
    '                    End If
    '                    If listPhoneEmployer.Count > 0 Then
    '                        For Each phone In listPhoneEmployer
    '                            listPhoneEmployerDirectorEntity.Add(phone)
    '                        Next
    '                    End If
    '                Next
    '                With transactionClass
    '                    .objEntityTo = entity
    '                    .ListObjAddressEntityto = listAddressEntity
    '                    .listObjPhoneEntityto = listPhoneEntity
    '                    .listDirectorEntityto = listDirectorEntity
    '                    .listAddressDirectorEntityto = listAddressDirectorEntity
    '                    .listPhoneDirectorEntityto = listPhoneDirectorEntity
    '                    .listAddressEmployerDirectorEntityto = listAddressEmployerDirectorEntity
    '                    .listPhoneEmployerDirectorEntityto = listPhoneEmployerDirectorEntity

    '                End With

    '            End If
    '            transactionClass.objTransaction = transaction
    '            listTransactionClass.Add(transactionClass)
    '        Next
    '        reportClass.objReport = Report
    '        reportClass.listObjTransaction = listTransactionClass
    '        Return reportClass
    '    End Using

    'End Function
    'Shared Function getTransactionByPK(PK As Integer) As NawaDevBLL.Transaction
    '    Dim transactionClass As NawaDevBLL.Transaction = Nothing
    '    Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
    '        Dim transaction As NawaDevDAL.goAML_Transaction = objdb.goAML_Transaction.Where(Function(x) x.PK_Transaction_ID = PK).FirstOrDefault
    '        If transaction.FK_Sender_From_Information = 1 Then ' account
    '            Dim account As NawaDevDAL.goAML_Transaction_Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = transaction.PK_Transaction_ID).FirstOrDefault

    '            Dim EntityAccount As NawaDevDAL.goAML_Trn_Entity_account = objdb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = account.PK_Account_ID And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '            Dim listAddressEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address) = Nothing
    '            Dim listPhoneEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone) = Nothing

    '            Dim listDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director) = Nothing
    '            Dim listAddressDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listPhoneEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listAddressEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing

    '            Dim listSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory) = objdb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = account.PK_Account_ID And x.FK_From_Or_To = 1).ToList
    '            Dim listAddressSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '            Dim listPhoneSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing
    '            Dim listAddressEmployerSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '            Dim listPhoneEmployerSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing

    '            For Each sign In listSignatory
    '                Dim addressSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault
    '                Dim phoneSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault

    '                Dim addressEmployerSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault
    '                Dim phoneEmployerSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault

    '                If addressSignatory IsNot Nothing Then
    '                    listAddressSignatory.Add(addressSignatory)
    '                End If
    '                If phoneSignatory IsNot Nothing Then
    '                    listPhoneSignatory.Add(phoneSignatory)
    '                End If
    '                If addressEmployerSignatory IsNot Nothing Then
    '                    listAddressEmployerSignatory.Add(addressEmployerSignatory)
    '                End If
    '                If phoneEmployerSignatory IsNot Nothing Then
    '                    listPhoneEmployerSignatory.Add(phoneEmployerSignatory)
    '                End If

    '            Next
    '            If account.isRekeningKorporasi = 1 Then
    '                listAddressEntityAccount = objdb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                listPhoneEntityAccount = objdb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                listDirectorEntityAccount = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityAccount.PK_goAML_Trn_Entity_account And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).ToList
    '                If listDirectorEntityAccount.Count > 0 Then
    '                    For Each directorEntity In listDirectorEntityAccount
    '                        Dim listAddressEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                        Dim listPhoneEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                        Dim listAddressEmployerEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                        Dim listPhoneEMployerEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                        If listAddressEntityAccountX.Count > 0 Then
    '                            For Each AEA In listAddressEntityAccountX
    '                                listAddressDirectorEntityAccount.Add(AEA)
    '                            Next
    '                        End If
    '                        If listPhoneEntityAccountX.Count > 0 Then
    '                            For Each PEAX In listPhoneEntityAccountX
    '                                listPhoneDirectorEntityAccount.Add(PEAX)
    '                            Next
    '                        End If
    '                        If listAddressEmployerEntityAccountX.Count > 0 Then
    '                            For Each AEEAX In listAddressEmployerEntityAccountX
    '                                listAddressEmployerDirectorEntityAccount.Add(AEEAX)
    '                            Next
    '                        End If
    '                        If listPhoneEMployerEntityAccountX.Count Then
    '                            For Each PEEAX In listPhoneEMployerEntityAccountX
    '                                listPhoneEmployerDirectorEntityAccount.Add(PEEAX)
    '                            Next
    '                        End If
    '                    Next
    '                End If
    '                With transactionClass
    '                    .objAccountFrom = account
    '                    .listObjAddressAccountEntityFrom = listAddressEntityAccount
    '                    .listObjPhoneAccountEntityFrom = listPhoneEntityAccount
    '                    .LisAccountSignatoryFrom = listSignatory
    '                    .listAddressAccountSignatoryFrom = listAddressSignatory
    '                    .listPhoneAccountSignatoryFrom = listPhoneSignatory
    '                    .listPhoneAccountEmployerSignatoryFrom = listPhoneEmployerSignatory
    '                    .listAddressAccountEmployerSignatoryFrom = listAddressEmployerSignatory
    '                    .objAccountEntityFrom = EntityAccount
    '                    .listDirectorAccountEntityFrom = listDirectorEntityAccount
    '                    .listAddressDirectorAccountEntityFrom = listAddressDirectorEntityAccount
    '                    .listPhoneDirectorAccountEntityFrom = listPhoneDirectorEntityAccount
    '                    .listAddressEmployerDirectorAccountEntityFrom = listAddressEmployerDirectorEntityAccount
    '                    .listPhoneEmployerDirectorAccountEntityFrom = listPhoneEmployerDirectorEntityAccount

    '                End With
    '            End If
    '        ElseIf transaction.FK_Sender_From_Information = 2 Then 'person
    '            Dim person As NawaDevDAL.goAML_Transaction_Person = objdb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '            Dim listAddressPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '            Dim listPhonePerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '            Dim listAddressEMployerPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '            Dim listPhoneEmployerPerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '            Dim IdentificationPerson As List(Of NawaDevDAL.goAML_Transaction_Person_Identification) = objdb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = person.PK_Person_ID And x.FK_Person_Type = 1).ToList
    '            With transactionClass
    '                .objPersonFrom = person
    '                If listAddressPerson.Count > 0 Then
    '                    .listObjAddressPersonFrom = listAddressPerson
    '                End If
    '                If listPhonePerson.Count > 0 Then
    '                    .listObjPhonePersonFrom = listPhonePerson
    '                End If
    '                If listPhoneEmployerPerson.Count > 0 Then
    '                    .listObjPhoneEmployerPersonFrom = listPhoneEmployerPerson
    '                End If
    '                If listAddressEMployerPerson.Count > 0 Then
    '                    .listObjAddressEmployerPersonFrom = listAddressEMployerPerson
    '                End If
    '                If IdentificationPerson.Count > 0 Then
    '                    .listObjIdentificationFrom = IdentificationPerson
    '                End If
    '            End With

    '        ElseIf transaction.FK_Sender_From_Information = 3 Then 'entity
    '            Dim entity As NawaDevDAL.goAML_Transaction_Entity = objdb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '            Dim listAddressEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Address) = objdb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '            Dim listPhoneEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Phone) = objdb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '            Dim listDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director) = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = entity.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Transaction_ID = transaction.PK_Transaction_ID).ToList
    '            Dim listAddressDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listAddressEmployerDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneEmployerDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            For Each directorEntity In listDirectorEntity
    '                Dim listAddress As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                Dim listPhone As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                Dim listAddressEmployer As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                Dim listPhoneEmployer As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                If listAddress.Count > 0 Then
    '                    For Each address In listAddress
    '                        listAddressDirectorEntity.Add(address)
    '                    Next
    '                End If
    '                If listPhone.Count > 0 Then
    '                    For Each phone In listPhone
    '                        listPhoneDirectorEntity.Add(phone)
    '                    Next
    '                End If
    '                If listAddressEmployer.Count > 0 Then
    '                    For Each address In listAddressEmployer
    '                        listAddressEmployerDirectorEntity.Add(address)
    '                    Next
    '                End If
    '                If listPhoneEmployer.Count > 0 Then
    '                    For Each phone In listPhoneEmployer
    '                        listPhoneEmployerDirectorEntity.Add(phone)
    '                    Next
    '                End If
    '            Next
    '            With transactionClass
    '                .objEntityFrom = entity
    '                .ListObjAddressEntityFrom = listAddressEntity
    '                .listObjPhoneEntityFrom = listPhoneEntity
    '                .listDirectorEntityFrom = listDirectorEntity
    '                .listAddressDirectorEntityFrom = listAddressDirectorEntity
    '                .listPhoneDirectorEntityFrom = listPhoneDirectorEntity
    '                .listAddressEmployerDirectorEntityFrom = listAddressEmployerDirectorEntity
    '                .listPhoneEmployerDirectorEntityFrom = listPhoneEmployerDirectorEntity

    '            End With

    '        End If
    '        If transaction.FK_Sender_To_Information = 1 Then ' Account
    '            Dim account As NawaDevDAL.goAML_Transaction_Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = transaction.PK_Transaction_ID).FirstOrDefault

    '            Dim EntityAccount As NawaDevDAL.goAML_Trn_Entity_account = objdb.goAML_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = account.PK_Account_ID And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
    '            Dim listAddressEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Address) = Nothing
    '            Dim listPhoneEntityAccount As List(Of NawaDevDAL.goAML_Trn_Acc_Entity_Phone) = Nothing

    '            Dim listDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director) = Nothing
    '            Dim listAddressDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listPhoneEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listAddressEmployerDirectorEntityAccount As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing

    '            Dim listSignatory As List(Of NawaDevDAL.goAML_Trn_acc_Signatory) = objdb.goAML_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = account.PK_Account_ID And x.FK_From_Or_To = 1).ToList
    '            Dim listAddressSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '            Dim listPhoneSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing
    '            Dim listAddressEmployerSignatory As List(Of NawaDevDAL.goAML_Trn_Acc_sign_Address) = Nothing
    '            Dim listPhoneEmployerSignatory As List(Of NawaDevDAL.goAML_trn_acc_sign_Phone) = Nothing

    '            For Each sign In listSignatory
    '                Dim addressSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault
    '                Dim phoneSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 0).FirstOrDefault

    '                Dim addressEmployerSignatory As NawaDevDAL.goAML_Trn_Acc_sign_Address = objdb.goAML_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault
    '                Dim phoneEmployerSignatory As NawaDevDAL.goAML_trn_acc_sign_Phone = objdb.goAML_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = sign.PK_goAML_Trn_acc_Signatory_ID And x.isEmployer = 1).FirstOrDefault

    '                If addressSignatory IsNot Nothing Then
    '                    listAddressSignatory.Add(addressSignatory)
    '                End If
    '                If phoneSignatory IsNot Nothing Then
    '                    listPhoneSignatory.Add(phoneSignatory)
    '                End If
    '                If addressEmployerSignatory IsNot Nothing Then
    '                    listAddressEmployerSignatory.Add(addressEmployerSignatory)
    '                End If
    '                If phoneEmployerSignatory IsNot Nothing Then
    '                    listPhoneEmployerSignatory.Add(phoneEmployerSignatory)
    '                End If

    '            Next
    '            If account.isRekeningKorporasi = 1 Then
    '                listAddressEntityAccount = objdb.goAML_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                listPhoneEntityAccount = objdb.goAML_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = EntityAccount.PK_goAML_Trn_Entity_account).ToList
    '                listDirectorEntityAccount = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = EntityAccount.PK_goAML_Trn_Entity_account And x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 1).ToList
    '                If listDirectorEntityAccount.Count > 0 Then
    '                    For Each directorEntity In listDirectorEntityAccount
    '                        Dim listAddressEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                        Dim listPhoneEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                        Dim listAddressEmployerEntityAccountX As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                        Dim listPhoneEMployerEntityAccountX As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                        If listAddressEntityAccountX.Count > 0 Then
    '                            For Each AEA In listAddressEntityAccountX
    '                                listAddressDirectorEntityAccount.Add(AEA)
    '                            Next
    '                        End If
    '                        If listPhoneEntityAccountX.Count > 0 Then
    '                            For Each PEAX In listPhoneEntityAccountX
    '                                listPhoneDirectorEntityAccount.Add(PEAX)
    '                            Next
    '                        End If
    '                        If listAddressEmployerEntityAccountX.Count > 0 Then
    '                            For Each AEEAX In listAddressEmployerEntityAccountX
    '                                listAddressEmployerDirectorEntityAccount.Add(AEEAX)
    '                            Next
    '                        End If
    '                        If listPhoneEMployerEntityAccountX.Count Then
    '                            For Each PEEAX In listPhoneEMployerEntityAccountX
    '                                listPhoneEmployerDirectorEntityAccount.Add(PEEAX)
    '                            Next
    '                        End If
    '                    Next
    '                End If
    '                With transactionClass
    '                    .objAccountTo = account
    '                    .listObjAddressAccountEntityto = listAddressEntityAccount
    '                    .listObjPhoneAccountEntityto = listPhoneEntityAccount
    '                    .LisAccountSignatoryTo = listSignatory
    '                    .listAddressAccountSignatoryto = listAddressSignatory
    '                    .listObjPhoneSignatoryto = listPhoneSignatory
    '                    .listObjPhoneEmployerSignatoryto = listPhoneEmployerSignatory
    '                    .listObjAddressEmployerSignatoryto = listAddressEmployerSignatory
    '                    .objAccountEntityTo = EntityAccount
    '                    .listDirectorAccountEntityto = listDirectorEntityAccount
    '                    .listAddressDirectorAccountEntityto = listAddressDirectorEntityAccount
    '                    .listPhoneDirectorAccountEntityto = listPhoneDirectorEntityAccount
    '                    .listAddressEmployerDirectorAccountEntityto = listAddressEmployerDirectorEntityAccount
    '                    .listPhoneEmployerDirectorAccountEntityto = listPhoneEmployerDirectorEntityAccount

    '                End With
    '            End If
    '        ElseIf transaction.FK_Sender_To_Information = 2 Then ' person
    '            Dim person As NawaDevDAL.goAML_Transaction_Person = objdb.goAML_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
    '            Dim listAddressPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '            Dim listPhonePerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 0).ToList
    '            Dim listAddressEmployerPerson As List(Of NawaDevDAL.goAML_Trn_Person_Address) = objdb.goAML_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '            Dim listPhoneEmployerPerson As List(Of NawaDevDAL.goAML_trn_Person_Phone) = objdb.goAML_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = person.PK_Person_ID And x.isEmployer = 1).ToList
    '            Dim IdentificationPerson As List(Of NawaDevDAL.goAML_Transaction_Person_Identification) = objdb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = person.PK_Person_ID And x.FK_Person_Type = 1).ToList
    '            With transactionClass
    '                .objPersonTo = person
    '                If listAddressPerson.Count > 0 Then
    '                    .listObjAddressPersonTo = listAddressPerson
    '                End If
    '                If listPhonePerson.Count > 0 Then
    '                    .listObjPhonePersonTo = listPhonePerson
    '                End If
    '                If listPhoneEmployerPerson.Count > 0 Then
    '                    .listObjPhoneEmployerPersonTo = listPhoneEmployerPerson
    '                End If
    '                If listAddressEmployerPerson.Count > 0 Then
    '                    .listObjAddressEmployerPersonTo = listAddressEmployerPerson
    '                End If
    '                If IdentificationPerson.Count > 0 Then
    '                    .listObjIdentificationPersonTo = IdentificationPerson
    '                End If
    '            End With
    '        ElseIf transaction.FK_Sender_To_Information = 3 Then 'Entity
    '            Dim entity As NawaDevDAL.goAML_Transaction_Entity = objdb.goAML_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = transaction.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
    '            Dim listAddressEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Address) = objdb.goAML_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '            Dim listPhoneEntity As List(Of NawaDevDAL.goAML_Trn_Entity_Phone) = objdb.goAML_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = entity.PK_Entity_ID).ToList
    '            Dim listDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director) = objdb.goAML_Trn_Director.Where(Function(x) x.FK_Entity_ID = entity.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Transaction_ID = transaction.PK_Transaction_ID).ToList
    '            Dim listAddressDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            Dim listAddressEmployerDirectorEntity As List(Of NawaDevDAL.goAML_Trn_Director_Address) = Nothing
    '            Dim listPhoneEmployerDirectorEntity As List(Of NawaDevDAL.goAML_trn_Director_Phone) = Nothing
    '            For Each directorEntity In listDirectorEntity
    '                Dim listAddress As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                Dim listPhone As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 0).ToList
    '                Dim listAddressEmployer As List(Of NawaDevDAL.goAML_Trn_Director_Address) = objdb.goAML_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                Dim listPhoneEmployer As List(Of NawaDevDAL.goAML_trn_Director_Phone) = objdb.goAML_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = directorEntity.PK_goAML_Trn_Director_ID And x.isEmployer = 1).ToList
    '                If listAddress.Count > 0 Then
    '                    For Each address In listAddress
    '                        listAddressDirectorEntity.Add(address)
    '                    Next
    '                End If
    '                If listPhone.Count > 0 Then
    '                    For Each phone In listPhone
    '                        listPhoneDirectorEntity.Add(phone)
    '                    Next
    '                End If
    '                If listAddressEmployer.Count > 0 Then
    '                    For Each address In listAddressEmployer
    '                        listAddressEmployerDirectorEntity.Add(address)
    '                    Next
    '                End If
    '                If listPhoneEmployer.Count > 0 Then
    '                    For Each phone In listPhoneEmployer
    '                        listPhoneEmployerDirectorEntity.Add(phone)
    '                    Next
    '                End If
    '            Next
    '            With transactionClass
    '                .objEntityTo = entity
    '                .ListObjAddressEntityto = listAddressEntity
    '                .listObjPhoneEntityto = listPhoneEntity
    '                .listDirectorEntityto = listDirectorEntity
    '                .listAddressDirectorEntityto = listAddressDirectorEntity
    '                .listPhoneDirectorEntityto = listPhoneDirectorEntity
    '                .listAddressEmployerDirectorEntityto = listAddressEmployerDirectorEntity
    '                .listPhoneEmployerDirectorEntityto = listPhoneEmployerDirectorEntity
    '            End With

    '        End If
    '    End Using
    '    Return transactionClass
    'End Function
#Region "Activity"
    Shared Function getlistActifity(reportID As String) As List(Of NawaDevDAL.goAML_Act_ReportPartyType)
        Dim ListActivity As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ActivityList As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)
            ActivityList = objdb.goAML_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = reportID).ToList
            If ActivityList IsNot Nothing Then
                ListActivity = ActivityList
            End If
        End Using
        Return ListActivity
    End Function
    Shared Function getActifity(PK As String) As NawaDevDAL.goAML_Act_ReportPartyType
        Dim Activity As New NawaDevDAL.goAML_Act_ReportPartyType
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim ObjActivity As New NawaDevDAL.goAML_Act_ReportPartyType
            ObjActivity = objdb.goAML_Act_ReportPartyType.Where(Function(x) x.PK_goAML_Act_ReportPartyType_ID = PK).FirstOrDefault
            If ObjActivity IsNot Nothing Then
                Activity = ObjActivity
            End If
        End Using
        Return Activity
    End Function

    Shared Function getAccountActivity(FKActId As String) As NawaDevDAL.goAML_Act_Account
        Dim objData As New NawaDevDAL.goAML_Act_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Act_Account
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Account)("select * from goAML_Act_Account with(nolock) where FK_Act_ReportParty_ID = " & FKActId).FirstOrDefault
            If Account IsNot Nothing Then
                objData = Account
            End If
        End Using
        Return objData
    End Function
    Shared Function getAccountActivityByPK(PK As String) As NawaDevDAL.goAML_Act_Account
        Dim objData As New NawaDevDAL.goAML_Act_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Account As New NawaDevDAL.goAML_Act_Account
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            Account = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Account)("select * from goAML_Act_Account with(nolock) where PK_goAML_Act_Account_ID = " & PK).FirstOrDefault
            If Account IsNot Nothing Then
                objData = Account
            End If
        End Using
        Return objData
    End Function
    Shared Function getEntityAccountActivity(FKActId As String) As NawaDevDAL.goAML_Act_Entity_Account
        Dim objData As New NawaDevDAL.goAML_Act_Entity_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityAccount As New NawaDevDAL.goAML_Act_Entity_Account
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            EntityAccount = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Account)("select * from goAML_Act_Entity_Account with(nolock) where FK_Act_Account_ID = " & FKActId).FirstOrDefault
            If EntityAccount IsNot Nothing Then
                objData = EntityAccount
            End If
        End Using
        Return objData
    End Function
    Shared Function getEntityAccountActivityByPK(PK As String) As NawaDevDAL.goAML_Act_Entity_Account
        Dim objData As New NawaDevDAL.goAML_Act_Entity_Account
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim EntityAccount As New NawaDevDAL.goAML_Act_Entity_Account
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            EntityAccount = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Account)("select * from goAML_Act_Entity_Account with(nolock) where PK_goAML_Act_Entity_account = " & PK).FirstOrDefault
            If EntityAccount IsNot Nothing Then
                objData = EntityAccount
            End If
        End Using
        Return objData
    End Function
    Shared Function getListAddressEntityAccountActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
            listaddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)("select * from goAML_Act_Acc_Entity_Address with(nolock) where FK_Act_Acc_Entity_ID = " & FkID).ToList
            If listaddress IsNot Nothing Then
                ListobjData = listaddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhoneEntityAccountActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
            listaPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)("select * from goAML_Act_Acc_Entity_Phone with(nolock) where FK_Act_Acc_Entity_ID = " & FkID).ToList
            If listaPhone IsNot Nothing Then
                ListobjData = listaPhone
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getPersonActivity(FKActId As String) As NawaDevDAL.goAML_Act_Person
        Dim objData As New NawaDevDAL.goAML_Act_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim person As New NawaDevDAL.goAML_Act_Person
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person)("select * from goAML_Act_Person with(nolock) where FK_Act_ReportParty_ID = " & FKActId).FirstOrDefault
            If person IsNot Nothing Then
                objData = person
            End If
        End Using
        Return objData
    End Function
    Shared Function getPersonActivityByPK(PK As String) As NawaDevDAL.goAML_Act_Person
        Dim objData As New NawaDevDAL.goAML_Act_Person
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim person As New NawaDevDAL.goAML_Act_Person
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            person = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person)("select * from goAML_Act_Person with(nolock) where PK_goAML_Act_Person_ID = " & PK).FirstOrDefault
            If person IsNot Nothing Then
                objData = person
            End If
        End Using
        Return objData
    End Function
    Shared Function getListAddressPersonActivityByFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Person_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaddress As New List(Of NawaDevDAL.goAML_Act_Person_Address)
            listaddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Address)("select * from goAML_Act_Person_Address with(nolock) where FK_Act_Person = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listaddress IsNot Nothing Then
                ListobjData = listaddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhonePersonActivityByFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Person_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Person_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaPhone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)
            listaPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Person_Phone)("select * from goAML_Act_Person_Phone with(nolock) where FK_Act_Person = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listaPhone IsNot Nothing Then
                ListobjData = listaPhone
            End If
        End Using
        Return ListobjData
    End Function

    Shared Function getEntityActivity(FKActId As String) As NawaDevDAL.goAML_Act_Entity
        Dim objData As New NawaDevDAL.goAML_Act_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim entity As New NawaDevDAL.goAML_Act_Entity
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity)("select * from goAML_Act_Entity with(nolock) where FK_Act_ReportParty_ID = " & FKActId).FirstOrDefault
            If entity IsNot Nothing Then
                objData = entity
            End If
        End Using
        Return objData
    End Function
    Shared Function getEntityActivityByPK(PK As String) As NawaDevDAL.goAML_Act_Entity
        Dim objData As New NawaDevDAL.goAML_Act_Entity
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim entity As New NawaDevDAL.goAML_Act_Entity
            'Account = objdb.goAML_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = id And x.FK_From_Or_To = FromTO).FirstOrDefault
            entity = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity)("select * from goAML_Act_Entity with(nolock) where PK_goAML_Act_Entity_ID = " & PK).FirstOrDefault
            If entity IsNot Nothing Then
                objData = entity
            End If
        End Using
        Return objData
    End Function
    Shared Function getListAddressEntityActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Entity_Address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaddress As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
            listaddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Address)("select * from goAML_Act_Entity_Address with(nolock) where FK_Act_Entity_ID = " & FkID).ToList
            If listaddress IsNot Nothing Then
                ListobjData = listaddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhoneEntityActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Entity_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listaPhone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)
            listaPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Entity_Phone)("select * from goAML_Act_Entity_Phone with(nolock) where FK_Act_Entity_ID = " & FkID).ToList
            If listaPhone IsNot Nothing Then
                ListobjData = listaPhone
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListDirectorEntityActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Director)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listDirector As New List(Of NawaDevDAL.goAML_Act_Director)
            listDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director)("select * from goAML_Act_Director with(nolock) where FK_Act_Entity_ID = " & FkID).ToList
            If listDirector IsNot Nothing Then
                ListobjData = listDirector
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListAddressDirectorEntityActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Director_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listAddress As New List(Of NawaDevDAL.goAML_Act_Director_Address)
            listAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Address)("select * from goAML_Act_Director_Address with(nolock) where FK_Act_Director_ID = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listAddress IsNot Nothing Then
                ListobjData = listAddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhoneDirectorEntityActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Director_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listPhone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)
            listPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Director_Phone)("select * from goAML_Act_Director_Phone with(nolock) where FK_Act_Director_ID = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listPhone IsNot Nothing Then
                ListobjData = listPhone
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListDirectorEntityAccountActivityByFK(FkID As String) As List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listDirector As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
            listDirector = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)("select * from goAML_Act_Acc_Ent_Director with(nolock) where FK_Act_Acc_Entity_ID = " & FkID).ToList
            If listDirector IsNot Nothing Then
                ListobjData = listDirector
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListAddressDirectorEntityAccountActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listAddress As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
            listAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)("select * from goAML_Act_Acc_Entity_Director_address with(nolock) where FK_Act_Entity_Director_ID = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listAddress IsNot Nothing Then
                ListobjData = listAddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhoneDirectorEntityAccountActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listPhone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
            listPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)("select * from goAML_Act_Acc_Entity_Director_Phone with(nolock) where FK_Act_Entity_Director_ID = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listPhone IsNot Nothing Then
                ListobjData = listPhone
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListSignatoryAccountActivityByFK(fkID As String) As List(Of NawaDevDAL.goAML_Act_acc_Signatory)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listSignatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
            listSignatory = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_acc_Signatory)("select * from goAML_Act_acc_Signatory with(nolock) where FK_Activity_Account_ID = " & fkID).ToList
            If listSignatory IsNot Nothing Then
                ListobjData = listSignatory
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListAddressSignatoryActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listAddress As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
            listAddress = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Address)("select * from goAML_Act_Acc_sign_Address with(nolock) where FK_Act_Acc_Entity = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listAddress IsNot Nothing Then
                ListobjData = listAddress
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListPhoneSignatoryActivitybyFK(FkID As String, isEmployer As Boolean) As List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
        Dim ListobjData As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim listPhone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)
            listPhone = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)("select * from goAML_Act_Acc_sign_Phone with(nolock) where FK_Act_Acc_Entity_ID = " & FkID & " and isEmployer='" & isEmployer & "'").ToList
            If listPhone IsNot Nothing Then
                ListobjData = listPhone
            End If
        End Using
        Return ListobjData
    End Function
    Shared Function getListTransactionIdentificationActivityById(Id As String, TypeID As Integer) As List(Of NawaDevDAL.goAML_Activity_Person_Identification)
        Dim objTransactionIdentification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
        Using objdb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Dim Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
            'Identification = objdb.goAML_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Id And x.FK_Person_Type = TypeID And x.from_or_To_Type = FromTO).ToList
            Identification = objdb.Database.SqlQuery(Of NawaDevDAL.goAML_Activity_Person_Identification)("select * from goAML_Activity_Person_Identification with(nolock) where FK_Act_Person_ID = " & Id & " and FK_Person_Type = " & TypeID).ToList
            If Identification IsNot Nothing Then
                objTransactionIdentification = Identification
            End If
        End Using
        Return objTransactionIdentification
    End Function
#End Region
End Class
