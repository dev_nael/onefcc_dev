﻿<Serializable()>
Public Class FileGenerationDataBLL
    Public objGenerateFileTemplate As NawaDevDAL.GenerateFileTemplate
    Public objListGenerateFileTemplateAdditional As List(Of NawaDevDAL.GenerateFileTemplateAdditional)
    Public objListGenerateFileTemplateDetail As List(Of NawaDevDAL.GenerateFileTemplateDetail)

End Class
