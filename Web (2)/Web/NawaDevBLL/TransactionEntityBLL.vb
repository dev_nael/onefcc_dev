﻿Imports System.Data.Entity
Imports NawaDevDAL
Imports NawaBLL
Imports System.Reflection

Public Class TransactionEntityBLL
    Public Shared Sub InsertTransactionEntityFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                    itemObjTransactionEntityFrom = Transactions.objEntityFrom

                    With itemObjTransactionEntityFrom
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionEntityFrom).State = EntityState.Added
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneEntityFrom.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Entity_Phone In Transactions.listObjPhoneEntityFrom
                            With EntityPhone
                                .FK_Trn_Entity = Transactions.objEntityFrom.PK_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhone).State = EntityState.Added
                        Next
                    End If

                    If Transactions.ListObjAddressEntityFrom.Count > 0 Then

                        For Each EntityAddress As goAML_Trn_Entity_Address In Transactions.ListObjAddressEntityFrom
                            With EntityAddress
                                .FK_Trn_Entity = Transactions.objEntityFrom.PK_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddress).State = EntityState.Added
                        Next

                    End If
                    objdb.SaveChanges()

                    '---------------------------------Director Entity Transaction-----------------------------------------
                    If Transactions.listDirectorEntityFrom.Count > 0 Then
                        TransactionEntityDirectorBLL.InsertTransactionEntityDirectorFrom(Transactions, Audit)
                    End If

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub InsertTransactionEntityTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                    itemObjTransactionEntityTo = Transactions.objEntityTo

                    With itemObjTransactionEntityTo
                        .FK_Transaction_ID = Transactions.objTransaction.PK_Transaction_ID
                        .ApprovedBy = Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With
                    objdb.Entry(itemObjTransactionEntityTo).State = EntityState.Added
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneEntityto.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Entity_Phone In Transactions.listObjPhoneEntityto
                            With EntityPhone
                                .FK_Trn_Entity = Transactions.objEntityTo.PK_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityPhone).State = EntityState.Added
                        Next
                    End If

                    If Transactions.ListObjAddressEntityto.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Entity_Address In Transactions.ListObjAddressEntityto
                            With EntityAddress
                                .FK_Trn_Entity = Transactions.objEntityTo.PK_Entity_ID
                                .ApprovedBy = Common.SessionCurrentUser.UserID
                                .ApprovedDate = Now
                            End With
                            objdb.Entry(EntityAddress).State = EntityState.Added
                        Next
                    End If
                    objdb.SaveChanges()

                    '---------------------------------Director Entity Transaction-----------------------------------------
                    If Transactions.listDirectorEntityto.Count > 0 Then
                        TransactionEntityDirectorBLL.InsertTransactionEntityDirectorTo(Transactions, Audit)
                    End If

                    ''---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionEntityFrom(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityFrom As New goAML_Transaction_Entity
                    itemObjTransactionEntityFrom = Transactions.objEntityFrom

                    objdb.Entry(itemObjTransactionEntityFrom).State = EntityState.Deleted

                    objdb.SaveChanges()

                    If Transactions.listObjPhoneEntityFrom.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Entity_Phone In Transactions.listObjPhoneEntityFrom
                            objdb.Entry(EntityPhone).State = EntityState.Deleted
                        Next
                    End If

                    If Transactions.ListObjAddressEntityFrom.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Entity_Address In Transactions.ListObjAddressEntityFrom
                            objdb.Entry(EntityAddress).State = EntityState.Deleted
                        Next

                    End If
                    objdb.SaveChanges()

                    '---------------------------------Director Entity Transaction-----------------------------------------
                    TransactionEntityDirectorBLL.DeleteTransactionEntityDirectorFrom(Transactions, Audit)
                    '---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub DeleteTransactionEntityTo(Transactions As Transaction, Audit As AuditTrailHeader)
        Using objdb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objdb.Database.BeginTransaction
                Try

                    Dim itemObjTransactionEntityTo As New goAML_Transaction_Entity
                    itemObjTransactionEntityTo = Transactions.objEntityTo

                    objdb.Entry(itemObjTransactionEntityTo).State = EntityState.Deleted

                    'Dim objtypeReport As Type = itemObjTransactionEntityTo.GetType
                    'Dim propertiesReport() As PropertyInfo = objtypeReport.GetProperties
                    'For Each item As PropertyInfo In propertiesReport
                    '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                    '    {
                    '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                    '        .FieldName = item.Name,
                    '        .OldValue = "",
                    '        .NewValue = If(Not item.GetValue(itemObjTransactionEntityTo, Nothing) Is Nothing, item.GetValue(itemObjTransactionEntityTo, Nothing), "")
                    '    }
                    '    objdb.Entry(objATDetail).State = EntityState.Added
                    'Next
                    objdb.SaveChanges()

                    If Transactions.listObjPhoneEntityto.Count > 0 Then
                        For Each EntityPhone As goAML_Trn_Entity_Phone In Transactions.listObjPhoneEntityto
                            objdb.Entry(EntityPhone).State = EntityState.Deleted

                            'objtypeReport = EntityPhone.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityPhone, Nothing) Is Nothing, item.GetValue(EntityPhone, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If

                    If Transactions.ListObjAddressEntityto.Count > 0 Then
                        For Each EntityAddress As goAML_Trn_Entity_Address In Transactions.ListObjAddressEntityto
                            objdb.Entry(EntityAddress).State = EntityState.Deleted

                            'objtypeReport = EntityAddress.GetType
                            'propertiesReport = objtypeReport.GetProperties
                            'For Each item As PropertyInfo In propertiesReport
                            '    Dim objATDetail As New NawaDevDAL.AuditTrailDetail With
                            '    {
                            '        .FK_AuditTrailHeader_ID = Audit.PK_AuditTrail_ID,
                            '        .FieldName = item.Name,
                            '        .OldValue = "",
                            '        .NewValue = If(Not item.GetValue(EntityAddress, Nothing) Is Nothing, item.GetValue(EntityAddress, Nothing), "")
                            '    }
                            '    objdb.Entry(objATDetail).State = EntityState.Added
                            'Next
                        Next
                    End If
                    objdb.SaveChanges()

                    '---------------------------------Director Entity Transaction-----------------------------------------
                    TransactionEntityDirectorBLL.DeleteTransactionEntityDirectorTo(Transactions, Audit)
                    ''---------------------------------End Director Entity Transaction-----------------------------------------

                    objdb.SaveChanges()
                    'objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
