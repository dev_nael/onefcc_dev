﻿Imports System.Data.Entity
Imports NawaDevDAL

Public Class ReportRemarkBLL
    Enum actionApproval
        Accept = 0
        Reject = 1
        Revise = 2
    End Enum
    Enum actionForm
        Add = 0
        Edit = 1
        Delete = 2
    End Enum
    Public Shared Sub CreateNotes(ModuleID As Integer, ModuleLabel As String, actionApproval As Integer, actionForm As Integer, unikID As String, notes As String)
        Dim approvalH As String
        Dim formH As String

        Select Case actionApproval
            Case 0
                approvalH = "Accept"
            Case 1
                approvalH = "Reject"
            Case 2
                approvalH = "Request"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Select Case actionForm
            Case 0
                formH = "Add:<br/> "
            Case 1
                formH = "Edit:<br/> "
            Case 2
                formH = "Delete:<br/> "
            Case 3
                formH = ""
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Using objDb As New NawaDatadevEntities
            Using objTrans As DbContextTransaction = objDb.Database.BeginTransaction()
                Try
                    Dim objHistory As New NawaDevDAL.goAML_History_Report With
                    {
                        .FK_Module_ID = ModuleID,
                        .unikID = unikID,
                        .UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID,
                        .UserName = NawaBLL.Common.SessionCurrentUser.UserName,
                        .RoleID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID,
                        .Status = approvalH,
                        .notes = formH & notes,
                        .Active = 1,
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID,
                        .CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    }

                    objDb.Entry(objHistory).State = Entity.EntityState.Added

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = ModuleLabel
                    Dim objATHeaderReport As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, objATHeaderReport.PK_AuditTrail_ID, objHistory)

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub
End Class
