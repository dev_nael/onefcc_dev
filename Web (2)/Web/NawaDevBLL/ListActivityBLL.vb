﻿Public Class ListActivityBLL
    Public list_goAML_Act_ReportPartyType As New List(Of NawaDevDAL.goAML_Act_ReportPartyType)
    Public list_goAML_Act_Account As New List(Of NawaDevDAL.goAML_Act_Account)
    Public list_goAML_Act_Entity_Account As New List(Of NawaDevDAL.goAML_Act_Entity_Account)
    Public list_goAML_Act_Acc_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Address)
    Public list_goAML_Act_Acc_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Phone)
    Public list_goAML_Act_Person As New List(Of NawaDevDAL.goAML_Act_Person)
    Public list_goAML_Act_Person_Address As New List(Of NawaDevDAL.goAML_Act_Person_Address)
    Public list_goAML_Act_Person_Phone As New List(Of NawaDevDAL.goAML_Act_Person_Phone)
    Public list_goAML_Activity_Person_Identification As New List(Of NawaDevDAL.goAML_Activity_Person_Identification)
    Public list_goAML_Act_Entity As New List(Of NawaDevDAL.goAML_Act_Entity)
    Public list_goAML_Act_Entity_Address As New List(Of NawaDevDAL.goAML_Act_Entity_Address)
    Public list_goAML_Act_Entity_Phone As New List(Of NawaDevDAL.goAML_Act_Entity_Phone)
    Public list_goAML_Act_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
    Public list_goAML_Act_Acc_Entity_Director_address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
    Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)
    Public list_goAML_Act_Director As New List(Of NawaDevDAL.goAML_Act_Director)
    Public list_goAML_Act_Director_Address As New List(Of NawaDevDAL.goAML_Act_Director_Address)
    Public list_goAML_Act_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Director_Phone)
    Public list_goAML_Act_acc_Signatory As New List(Of NawaDevDAL.goAML_Act_acc_Signatory)
    Public list_goAML_Act_Acc_sign_Address As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Address)
    Public list_goAML_Act_Acc_sign_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_sign_Phone)



End Class
