'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class SIPENDAR_Transaction_MultiPartySTR_Upload
    Public Property PK_upload_ID As Long
    Public Property nawa_userid As String
    Public Property nawa_recordnumber As Nullable(Of Long)
    Public Property KeteranganError As String
    Public Property nawa_Action As String
    Public Property PK_Transaction_ID As String
    Public Property FK_Report_ID As String
    Public Property FK_Jenis_Laporan_ID As String
    Public Property isValid As String
    Public Property TransactionNumber As String
    Public Property Internal_Ref_Number As String
    Public Property Transaction_Location As String
    Public Property Transaction_Remark As String
    Public Property Date_Transaction As String
    Public Property Teller As String
    Public Property Authorized As String
    Public Property Late_Deposit As String
    Public Property Date_Posting As String
    Public Property Value_Date As String
    Public Property Transmode_Code As String
    Public Property Transmode_Comment As String
    Public Property Amount_Local As String
    Public Property isMyClient As String
    Public Property Funds_Code As String
    Public Property Funds_Comment As String
    Public Property Foreign_Currency_Code As String
    Public Property Foreign_Currency_Amount As String
    Public Property Foreign_Currency_Exchange_Rate As String
    Public Property FK_Person_ID As String
    Public Property FK_Sender_Information As String
    Public Property Country As String
    Public Property Comments As String
    Public Property Generate_Date As String
    Public Property isSwift As String
    Public Property FK_Source_Data_ID As String
    Public Property FK_Report_Type_ID As String
    Public Property Value_Report As String
    Public Property Jenis_Transaksi As String
    Public Property Peran As String
    Public Property Signifikasi As String
    Public Property Account_No As String
    Public Property CIFNO As String
    Public Property WIC_NO As String
    Public Property IsUsedConductor As String
    Public Property Condutor_ID As String
    Public Property Case_ID As String
    Public Property MessageValidation As String
    Public Property Active As Nullable(Of Boolean)

End Class
