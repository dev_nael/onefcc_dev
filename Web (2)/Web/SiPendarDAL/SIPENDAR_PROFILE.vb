'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class SIPENDAR_PROFILE
    Public Property PK_SIPENDAR_PROFILE_ID As Long
    Public Property FK_SIPENDAR_TYPE_ID As Nullable(Of Integer)
    Public Property Fk_goAML_Ref_Customer_Type_id As Nullable(Of Integer)
    Public Property GCN As String
    Public Property FK_SIPENDAR_JENIS_WATCHLIST_CODE As String
    Public Property FK_SIPENDAR_TINDAK_PIDANA_CODE As String
    Public Property FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE As String
    Public Property FK_SIPENDAR_ORGANISASI_CODE As String
    Public Property Keterangan As String
    Public Property INDV_NAME As String
    Public Property INDV_FK_goAML_Ref_Nama_Negara_CODE As String
    Public Property INDV_ADDRESS As String
    Public Property INDV_PLACEOFBIRTH As String
    Public Property INDV_DOB As Nullable(Of Date)
    Public Property CORP_NAME As String
    Public Property CORP_FK_goAML_Ref_Nama_Negara_CODE As String
    Public Property CORP_NPWP As String
    Public Property CORP_NO_IZIN_USAHA As String
    Public Property AGING As Nullable(Of Integer)
    Public Property IsHaveReportTransaction As Nullable(Of Boolean)
    Public Property IsAlreadyGeneratedXML As Nullable(Of Boolean)
    Public Property LastGenerateXMLDate As Nullable(Of Date)
    Public Property LastGenerateXMLBy As String
    Public Property XMLDataSIPENDAR As String
    Public Property IsValid As Nullable(Of Boolean)
    Public Property ErrorMessage As String
    Public Property FK_Report_ID As Nullable(Of Long)
    Public Property IsValidXSD As Nullable(Of Boolean)
    Public Property ErrorMessageXSD As String
    Public Property Similarity As Nullable(Of Double)
    Public Property Status As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property FK_SIPENDAR_SUMBER_TYPE_CODE As String
    Public Property SUMBER_ID As Nullable(Of Long)
    Public Property FK_SIPENDAR_SUBMISSION_TYPE_CODE As String
    Public Property ID_Laporan_PPATK_Result As String
    Public Property Submission_Date As Nullable(Of Date)
    Public Property Submit_Date As Nullable(Of Date)
    Public Property Transaction_DateFrom As Nullable(Of Date)
    Public Property Transaction_DateTo As Nullable(Of Date)
    Public Property Report_Indicator As String
    Public Property FK_SIPENDAR_SCREENING_REQUEST_ID As Nullable(Of Long)
    Public Property FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID As Nullable(Of Long)

End Class
