'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class SIPENDAR_WATCHLIST_APPROVAL_HISTORY
    Public Property PK_Sipendar_Watchlist_Approval_History_ID As Long
    Public Property ModuleName As String
    Public Property ModuleKey As String
    Public Property ModuleField As String
    Public Property ModuleFieldBefore As String
    Public Property PK_ModuleAction_ID As Nullable(Of Integer)
    Public Property FK_MRole_ID As Nullable(Of Integer)
    Public Property UserID As Nullable(Of Long)
    Public Property Status As String
    Public Property Notes As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class
