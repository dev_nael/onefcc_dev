'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class MUser
    Public Property PK_MUser_ID As Integer
    Public Property UserID As String
    Public Property UserName As String
    Public Property FK_MRole_ID As Nullable(Of Integer)
    Public Property FK_MGroupMenu_ID As Nullable(Of Integer)
    Public Property UserPasword As String
    Public Property UserPasswordSalt As String
    Public Property UserEmailAddress As String
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property IPAddress As String
    Public Property Active As Nullable(Of Boolean)
    Public Property InUsed As Nullable(Of Boolean)
    Public Property IsDisabled As Nullable(Of Boolean)
    Public Property LastLogin As Nullable(Of Date)
    Public Property LastChangePassword As Nullable(Of Date)
    Public Property Alternateby As String

End Class
