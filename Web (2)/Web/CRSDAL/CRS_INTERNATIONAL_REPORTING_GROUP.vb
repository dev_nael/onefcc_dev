'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class CRS_INTERNATIONAL_REPORTING_GROUP
    Public Property PK_CRS_INTERNATIONAL_REPORTING_GROUP_ID As Long
    Public Property FK_CRS_INTERNATIONAL_REPORT_HEADER_ID As Nullable(Of Long)
    Public Property DocSpec_DocRefID As String
    Public Property DocSpec_DocTypeIndic As String
    Public Property DocSpec_CorrDocRefID As String
    Public Property AccountNumber As String
    Public Property Account_AcctNumberType As String
    Public Property Account_UndocumentedAccount As Nullable(Of Boolean)
    Public Property Account_ClosedAccount As Nullable(Of Boolean)
    Public Property Account_DormantAccount As Nullable(Of Boolean)
    Public Property IsIndividualYN As Nullable(Of Boolean)
    Public Property ORG_AcctHolderType As String
    Public Property INDV_ResCountryCode As String
    Public Property ORG_ResCountryCode As String
    Public Property INDV_Nationality1 As String
    Public Property INDV_Nationality2 As String
    Public Property INDV_Nationality3 As String
    Public Property INDV_Nationality4 As String
    Public Property INDV_Nationality5 As String
    Public Property AccountBalance_CurrencyCode As String
    Public Property AccountBalance As Nullable(Of Double)
    Public Property INDV_TIN As String
    Public Property INDV_TIN_ISSUEDBY As String
    Public Property ORG_IN As String
    Public Property ORG_IN_IssuedBy As String
    Public Property ORG_IN_INType As String
    Public Property INDV_Name As String
    Public Property INDV_Name_Type As String
    Public Property INDV_Name_PrecedingTitle As String
    Public Property INDV_Name_Title As String
    Public Property INDV_Name_FirstName As String
    Public Property INDV_Name_FirstName_Type As String
    Public Property INDV_Name_MiddleName As String
    Public Property INDV_Name_MiddleName_Type As String
    Public Property INDV_Name_NamePrefix As String
    Public Property INDV_Name_LastName As String
    Public Property INDV_Name_GenerationIdentifier As String
    Public Property INDV_Name_LastName_Type As String
    Public Property INDV_Name_Suffix As String
    Public Property INDV_Name_GeneralSuffix As String
    Public Property ORG_Name As String
    Public Property ORG_Name_NameType As String
    Public Property INDV_BirthInfo_BirthDate As Nullable(Of Date)
    Public Property INDV_BirthInfo_City As String
    Public Property INDV_BirthInfo_CitySubEntity As String
    Public Property INDV_BirthInfo_CountryInfo_CountryCode As String
    Public Property INDV_BirthInfo_CountryInfo_FormerCountryName As String
    Public Property Address_LegalAddressType As String
    Public Property Address_CountryCode As String
    Public Property Address_AddressFree As String
    Public Property Address_AddressFix_Street As String
    Public Property Address_AddressFix_BuildingIdentifier As String
    Public Property Address_AddressFix_SuiteIdentifier As String
    Public Property Address_AddressFix_FloorIdentifier As String
    Public Property Address_AddressFix_DistrictName As String
    Public Property Address_AddressFix_POB As String
    Public Property Address_AddressFix_PostCode As String
    Public Property Address_AddressFix_City As String
    Public Property Address_AddressFix_CountrySubentity As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property FK_CRS_ODM_ACCOUNT_ID As Nullable(Of Long)
    Public Property IsValid As Nullable(Of Boolean)
    Public Property IsChanges As Nullable(Of Boolean)

End Class
