'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Vw_AML_WATCHLIST
    Public Property PK_AML_WATCHLIST_ID As Long
    Public Property FK_AML_WATCHLIST_TYPE_ID As Nullable(Of Long)
    Public Property FK_AML_WATCHLIST_CATEGORY_ID As Nullable(Of Long)
    Public Property FK_AML_WATCHLIST_SOURCE_ID As Nullable(Of Long)
    Public Property NAME As String
    Public Property DATE_OF_BIRTH As Nullable(Of Date)
    Public Property BIRTH_PLACE As String
    Public Property NATIONALITY As String
    Public Property CUSTOM_REMARK_1 As String
    Public Property CUSTOM_REMARK_2 As String
    Public Property CUSTOM_REMARK_3 As String
    Public Property CUSTOM_REMARK_4 As String
    Public Property CUSTOM_REMARK_5 As String
    Public Property ACTIVE As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property TYPE_NAME As String
    Public Property CATEGORY_NAME As String
    Public Property PHONE_NUMBER As String

End Class
