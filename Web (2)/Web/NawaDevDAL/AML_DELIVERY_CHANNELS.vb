'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class AML_DELIVERY_CHANNELS
    Public Property PK_AML_AML_DELIVERY_CHANNELS_ID As Long
    Public Property FK_AML_AML_DELIVERY_CHANNELS_CODE As String
    Public Property AML_DELIVERY_CHANNELS_NAME As String
    Public Property KETERANGAN As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property FK_AML_RISK_RATING_CODE As String

End Class
