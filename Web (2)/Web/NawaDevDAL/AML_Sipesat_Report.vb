'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class AML_Sipesat_Report
    Public Property PK_ID As Nullable(Of Long)
    Public Property PK_Customer_Type_ID As Nullable(Of Integer)
    Public Property Name As String
    Public Property Place_Birth As String
    Public Property PK_Date_Birth As Nullable(Of Date)
    Public Property Address As String
    Public Property No_KTP As String
    Public Property PK_Other_ID As String
    Public Property No_CIF As String
    Public Property No_NPWP As String
    Public Property Period As String
    Public Property Jenis_Informasi As Nullable(Of Integer)
    Public Property IsValid As Nullable(Of Boolean)
    Public Property PK_Report_ID As Long
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property Status As Nullable(Of Integer)
    Public Property GCN As String
    Public Property isEdited As Nullable(Of Boolean)
    Public Property OPEN_CUSTOMER As Nullable(Of Date)
    Public Property OPEN_REKENING As Nullable(Of Date)
    Public Property Source_Data As String

End Class
