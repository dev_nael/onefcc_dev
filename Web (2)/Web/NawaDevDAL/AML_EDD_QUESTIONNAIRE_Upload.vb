'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class AML_EDD_QUESTIONNAIRE_Upload
    Public Property PK_upload_ID As Long
    Public Property nawa_userid As String
    Public Property nawa_recordnumber As Nullable(Of Long)
    Public Property KeteranganError As String
    Public Property nawa_Action As String
    Public Property FK_AML_CUSTOMER_TYPE_ID As String
    Public Property FK_ANSWER_TYPE_ID As String
    Public Property QUESTION As String
    Public Property SEQUENCE As String
    Public Property IS_REQUIRED As String
    Public Property FK_PARENT_QUESTION_ID As String
    Public Property Active As Nullable(Of Boolean)

End Class
