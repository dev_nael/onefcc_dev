'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class GenerateFileTemplate
    Public Property PK_GenerationFileTemplate_ID As Long
    Public Property GenerateFileTemplateName As String
    Public Property FileNameFormat As String
    Public Property OutputFormat As String
    Public Property LHBUFormName As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property FK_DelivMethod_ID As Nullable(Of Integer)

End Class
