'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class AML_TRANSACTION
    Public Property PK_AML_TRANSACTION_ID As Integer
    Public Property TRANSACTION_NUMBER As String
    Public Property TRANSACTION_REFERENCE_NO As String
    Public Property TRANSACTION_TIKET_NO As String
    Public Property DateCreated As Nullable(Of Date)
    Public Property FK_AML_TRANSACTION_CHANNEL_TYPE_CODE As String
    Public Property FK_AML_COUNTRY_CODE As String
    Public Property FK_AML_ENTITYBANK_CODE As String
    Public Property EntityBank_Name As String
    Public Property CIFNO As String
    Public Property ACCOUNT_NO As String
    Public Property ACCOUNT_NAME As String
    Public Property FK_AML_ACCOUNT_TYPE As String
    Public Property WICNo As String
    Public Property FK_AML_BRANCH_CODE As String
    Public Property FK_AML_TRANSACTION_CODE As String
    Public Property TRANSACTION_AMOUNT As Nullable(Of Double)
    Public Property FK_AML_CURRENCY_CODE As String
    Public Property TRANSACTION_EXCHANGE_RATE As Nullable(Of Double)
    Public Property TRANSACTION_AMOUNT_LOCAL_EQUIVALENT As Nullable(Of Double)
    Public Property FK_DebitOrCredit_Code As String
    Public Property TRANSACTION_DATE As Nullable(Of Date)
    Public Property Effective_Date As Nullable(Of Date)
    Public Property TIME_ENTERED As String
    Public Property User_ID As String
    Public Property User_Autorize As String
    Public Property TRANSACTION_REMARKS As String
    Public Property MEMO_REMARKS As String
    Public Property FK_AML_FUND_TYPE_CODE As String
    Public Property FK_AML_TRANS_MODE_CODE As String
    Public Property OTHER_TRANS_MODE As String
    Public Property PK_AML_TRANSACTION_lAWAN_ID As Nullable(Of Long)
    Public Property ACCOUNT_NO_LAWAN As String
    Public Property ACCOUNT_NAME_LAWAN As String
    Public Property CIFNO_LAWAN As String
    Public Property FK_AML_ENTITYBANK_CODE_LAWAN As String
    Public Property ENTITYBANK_NAME_LAWAN As String
    Public Property WICNo_LAWAN As String
    Public Property WICNo_Conductor1 As String
    Public Property SOURCE_OF_FUND As String
    Public Property DATA_SOURCE As String
    Public Property MESSAGE_TYPE_SWIFT As String
    Public Property TAG_20 As String
    Public Property DEBIT_ACC_NO As String
    Public Property CREDIT_ACC_NO As String
    Public Property OVERBOOKING_FLAG As Nullable(Of Boolean)
    Public Property CORRESPONDENT_BANK As String
    Public Property CHEQUE_TYPE As String
    Public Property CHEQUE_NUMBER As String
    Public Property LATE_DEPOSIT As Nullable(Of Boolean)
    Public Property FK_AML_COUNTRY_CODE_LAWAN As String
    Public Property FK_AML_CURRENCY_CODE_LAWAN As String
    Public Property FK_AML_FUND_TYPE_CODE_LAWAN As String
    Public Property TRANSACTION_AMOUNT_LAWAN As String
    Public Property TRANSACTION_AMOUNT_LOCAL_EQUIVALENT_LAWAN As String
    Public Property TRANSACTION_EXCHANGE_RATE_LAWAN As Nullable(Of Double)
    Public Property FK_AML_MERCHANT_NO As String
    Public Property FK_AML_CARD_NO As String
    Public Property CIC As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class
