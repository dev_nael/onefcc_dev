﻿Public Class GoAMLEnum
    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        Director = 4
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
        EntityAccount = 9
        RelatedPersonTrnAccountSignatory = 10
        RelatedPersonTrnPerson = 11
        RelatedPersonTrnMultiAccountSignatory = 12
        RelatedPersonTrnMultiPerson = 13
        RelatedPersonActivityAccountSignatory = 14
        RelatedPersonActivityPerson = 15
        RelatedEntityTrnAccountEntityAccount = 16
        RelatedEntityTrnEntity = 17
        RelatedEntityTrnMultiAccountEntityAccount = 18
        RelatedEntityTrnMultiEntity = 19
        RelatedEntityActivityAccountEntityAccount = 20
        RelatedEntityActivityEntity = 21
        RelatedAccountMultipartyAccount = 22
        RelatedAccountActivityAccount = 23

    End Enum
    Public Enum enumSenderInformation
        Account = 1
        Person = 2
        Entity = 3
    End Enum

    Enum enumActionApproval
        Accept = 1
        Reject = 2
        Request = 3
    End Enum

    Enum enumActionForm
        Add = 1
        Edit = 2
        Delete = 3
        Approval = 4
    End Enum
End Class
