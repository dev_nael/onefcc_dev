
'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic


Partial Public Class goAML_Ref_Customer_PEP

    Public Property PK_goAML_Ref_Customer_PEP_ID As Long

    Public Property CIF As String

    Public Property PEP_COUNTRY As String

    Public Property FUNCTION_NAME As String

    Public Property FUNCTION_DESCRIPTION As String

    Public Property PEP_DATE_RANGE_VALID_FROM As Nullable(Of Date)

    Public Property PEP_DATE_RANGE_IS_APPROX_FROM_DATE As Nullable(Of Boolean)

    Public Property PEP_DATE_RANGE_VALID_TO As Nullable(Of Date)

    Public Property PEP_DATE_RANGE_IS_APPROX_TO_DATE As Nullable(Of Boolean)

    Public Property COMMENTS As String

    Public Property Active As Nullable(Of Boolean)

    Public Property CreatedBy As String

    Public Property LastUpdateBy As String

    Public Property ApprovedBy As String

    Public Property CreatedDate As Nullable(Of Date)

    Public Property LastUpdateDate As Nullable(Of Date)

    Public Property ApprovedDate As Nullable(Of Date)

    Public Property Alternateby As String

    Public Property FK_REF_DETAIL_OF As Nullable(Of Integer)

    Public Property FK_FOR_TABLE_ID As Nullable(Of Long)


End Class
