
'------------------------------------------------------------------------------
' <auto-generated>
'    This code was generated from a template.
'
'    Manual changes to this file may cause unexpected behavior in your application.
'    Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic


Partial Public Class goAML_Report_Related_Account

    Public Property PK_goAML_Report_Related_Account_ID As Long

    Public Property FK_From_Or_To As Nullable(Of Integer)

    Public Property account_account_relation As String

    Public Property relation_date_range_valid_from As Nullable(Of Date)

    Public Property relation_date_range_is_approx_from_date As Nullable(Of Boolean)

    Public Property relation_date_range_valid_to As Nullable(Of Date)

    Public Property relation_date_range_is_approx_to_date As Nullable(Of Boolean)

    Public Property comments As String

    Public Property FK_Report_ID As Long

    Public Property institution_name As String

    Public Property institution_code As String

    Public Property swift As String

    Public Property institution_country As String

    Public Property non_bank_institution As Nullable(Of Boolean)

    Public Property collection_account As Nullable(Of Boolean)

    Public Property branch As String

    Public Property account_category As String

    Public Property account As String

    Public Property currency_code As String

    Public Property account_name As String

    Public Property iban As String

    Public Property client_number As String

    Public Property account_type As String

    Public Property opened_date As Nullable(Of Date)

    Public Property closed_date As Nullable(Of Date)

    Public Property balance As Nullable(Of Double)

    Public Property date_balance As Nullable(Of Date)

    Public Property status_code As String

    Public Property status_date As Nullable(Of Date)

    Public Property beneficiary As String

    Public Property beneficiary_comment As String

    Public Property FK_Parent_Table_ID As Nullable(Of Long)

    Public Property Parent_Table_Name As String

    Public Property Active As Nullable(Of Boolean)

    Public Property CreatedBy As String

    Public Property LastUpdateBy As String

    Public Property ApprovedBy As String

    Public Property CreatedDate As Nullable(Of Date)

    Public Property LastUpdateDate As Nullable(Of Date)

    Public Property ApprovedDate As Nullable(Of Date)

    Public Property Alternateby As String

    Public Property relation_comments As String


End Class
