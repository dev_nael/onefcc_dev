﻿Imports System.Data.SqlClient
Imports NawaDAL


Public Class SIPESATGoAML_Report_BLL

    Public Enum enumIdentificationPersonType
        Person = 1
        Signatory = 2
        Conductor = 3
        Director = 4
        DirectorAccount = 5
        DirectorEntity = 6
        DirectorWIC = 7
        WIC = 8
    End Enum

    Enum enumActionApproval
        Accept = 1
        Reject = 2
        Request = 3
    End Enum

    Enum enumActionForm
        Add = 1
        Edit = 2
        Delete = 3
        Approval = 4
    End Enum

    Shared Function GetCustomerbyCIF(CIF As String) As SIPESATGoAMLDAL.goAML_Ref_Customer
        Using objDb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = CIF).FirstOrDefault
        End Using
    End Function

    Shared Function getValidationReportByReportID(strReportID As String) As String
        Try
            ''Dim param(0) As SqlParameter
            ''param(0) = New SqlParameter
            ''param(0).ParameterName = "@reportId"
            ''param(0).Value = strReportID
            ''param(0).DbType = SqlDbType.Int

            ''Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_goAML_ResponseValidateReportByID", param)
            Dim ErrorResponse As String = ""
            Dim SQLQueries As String = "select ERROR_MESSAGE as ErrorReturn from SIPESATGOAML_REPORT_ERROR_MESSAGE where FK_SIPESATGOAML_REPORT_HEADER_ID = " & strReportID
            Dim drResponse As DataRow = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.PullDataAsDataRowSingleString(SQLQueries)
            If drResponse IsNot Nothing AndAlso Not IsDBNull(drResponse("ErrorReturn")) Then
                ErrorResponse = drResponse("ErrorReturn")
            End If
            Return ErrorResponse
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ' 26 Dec 2022 Ari : Hide untuk get data Account
    '    Shared Function GetActivityAccountSignatoryClassByAccountID(objData As SIPESATGoAML_Activity_Class, strPK_Account_ID As String) As SIPESATGoAMLBLL.SIPESATGoAML_Activity_Class
    '        Try
    '            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = strPK_Account_ID).FirstOrDefault

    '            'Hapus data2 Signatory yang lama (jika ada)
    '            If objAccount IsNot Nothing Then
    '                Dim listSignatory_Old = objData.list_goAML_Act_acc_Signatory.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).ToList
    '                If listSignatory_Old IsNot Nothing Then
    '                    For Each objSignatory_Old In listSignatory_Old
    '                        objData.list_goAML_Act_acc_Signatory.Remove(objSignatory_Old)

    '                        'Signatory Address
    '                        Dim listAddress_Old = objData.list_goAML_Act_Acc_sign_Address.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory_Old.PK_ACT_ACC_SIGNATORY_ID).ToList
    '                        If listAddress_Old IsNot Nothing Then
    '                            For Each itemAddress_Old In listAddress_Old
    '                                objData.list_goAML_Act_Acc_sign_Address.Remove(itemAddress_Old)
    '                            Next
    '                        End If

    '                        'Signatory Phone
    '                        Dim listPhone_Old = objData.list_goAML_Act_Acc_sign_Phone.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = objSignatory_Old.PK_ACT_ACC_SIGNATORY_ID).ToList
    '                        If listPhone_Old IsNot Nothing Then
    '                            For Each itemPhone_Old In listPhone_Old
    '                                objData.list_goAML_Act_Acc_sign_Phone.Remove(itemPhone_Old)
    '                            Next
    '                        End If

    '                        'Signatory Identification
    '                        Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = objSignatory_Old.PK_ACT_ACC_SIGNATORY_ID).ToList
    '                        If listIdentification_Old IsNot Nothing Then
    '                            For Each objIdentification_Old In listIdentification_Old
    '                                objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
    '                            Next
    '                        End If
    '                    Next
    '                End If
    '            End If

    '            'Masukkan data2 baru ke dalam Class
    '            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
    '                If objAccount IsNot Nothing Then
    '                    'Get Last PK
    '                    Dim lngPK_Signatory As Long = 0
    '                    Dim lngPK_Address As Long = 0
    '                    Dim lngPK_Phone As Long = 0
    '                    Dim lngPK_Identification As Long = 0

    '                    'PK Signatory
    '                    If objData.list_goAML_Act_acc_Signatory.Count > 0 Then
    '                        lngPK_Signatory = objData.list_goAML_Act_acc_Signatory.Min(Function(x) x.PK_ACT_ACC_SIGNATORY_ID)
    '                        If lngPK_Signatory > 0 Then
    '                            lngPK_Signatory = 0
    '                        End If
    '                    End If

    '                    'PK Signatory Address
    '                    If objData.list_goAML_Act_Acc_sign_Address.Count > 0 Then
    '                        lngPK_Address = objData.list_goAML_Act_Acc_sign_Address.Min(Function(x) x.PK_ACT_ACC_SIGN_ADDRESS_ID)
    '                        If lngPK_Address > 0 Then
    '                            lngPK_Address = 0
    '                        End If
    '                    End If

    '                    'PK Signatory Phone
    '                    If objData.list_goAML_Act_Acc_sign_Phone.Count > 0 Then
    '                        lngPK_Phone = objData.list_goAML_Act_Acc_sign_Phone.Min(Function(x) x.PK_ACT_ACC_SIGN_PHONE_ID)
    '                        If lngPK_Phone > 0 Then
    '                            lngPK_Phone = 0
    '                        End If
    '                    End If

    '                    'PK Signatory Identification
    '                    If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
    '                        lngPK_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID)
    '                        If lngPK_Identification > 0 Then
    '                            lngPK_Identification = 0
    '                        End If
    '                    End If

    '                    'Masukkand data2 Signatory dari goAML Ref pindah flow karna semua signatory berasan dari customer
    '                    Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.CLIENT_NUMBER).FirstOrDefault
    '                    If objCustomer IsNot Nothing Then
    '                        Dim objSignatory_New As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY
    '                        lngPK_Signatory = lngPK_Signatory - 1
    '                        With objSignatory_New
    '                            .PK_ACT_ACC_SIGNATORY_ID = lngPK_Signatory
    '                            .FK_Report_ID = objAccount.FK_REPORT_ID
    '                            .FK_ACT_REPORTPARTYTYPE_ID = objAccount.FK_ACT_REPORTPARTY_ID
    '                            .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID
    '                            .LAST_NAME = objCustomer.INDV_First_name & objCustomer.INDV_Middle_name & objCustomer.INDV_Last_Name
    '                            .BIRTH_PLACE = objCustomer.INDV_Birth_Place
    '                            .BIRTHDATE = objCustomer.INDV_BirthDate
    '                            .GENDER = objCustomer.INDV_Gender
    '                            .TAX_REG_NUMBER = objCustomer.INDV_Tax_Reg_Number
    '                            .RESIDENCE = objCustomer.INDV_Residence
    '                            .SOURCE_OF_WEALTH = objCustomer.INDV_Source_of_Wealth
    '                            .OCCUPATION = objCustomer.INDV_Occupation
    '                            .EMPLOYER_NAME = objCustomer.INDV_Employer_Name
    '                            .NATIONALITY1 = objCustomer.INDV_Nationality1
    '                            .SSN = objCustomer.INDV_SSN
    '                            .ID_NUMBER = objCustomer.INDV_ID_Number
    '                            .PASSPORT_NUMBER = objCustomer.INDV_Passport_Number
    '                            .PASSPORT_COUNTRY = objCustomer.INDV_Passport_Country
    '                        End With
    '                        objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

    '                        'Signatory Address(es)  (1=Address, 5=Employer Address)
    '                        'Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1) And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
    '                            For Each itemAddress In objListAddress
    '                                Dim objNewSignatoryAddress As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS
    '                                With objNewSignatoryAddress
    '                                    lngPK_Address = lngPK_Address - 1

    '                                    .PK_ACT_ACC_SIGN_ADDRESS_ID = lngPK_Address
    '                                    .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                                    .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                                    '--ini wajib ADDRESS_TYPE, ADDRESS, TOWN, CITY, COUNTRY_CODE, STATE
    '                                    .ADDRESS_TYPE = itemAddress.Address_Type
    '                                    .ADDRESS = itemAddress.Address
    '                                    .TOWN = itemAddress.Town
    '                                    .CITY = itemAddress.City
    '                                    .COUNTRY_CODE = itemAddress.Country_Code
    '                                    .STATE = itemAddress.State

    '                                    .ZIP = itemAddress.Zip
    '                                    .COMMENTS = itemAddress.Comments

    '                                    If itemAddress.FK_Ref_Detail_Of = 1 Then
    '                                        .ISEMPLOYER = False
    '                                    Else
    '                                        .ISEMPLOYER = True
    '                                    End If

    '                                    .Active = 1
    '                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                                    .CreatedDate = DateTime.Now
    '                                End With
    '                                objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
    '                            Next
    '                        End If

    '                        'Signatory Phone(s) (1=Phone, 5=Employer Phone)
    '                        'Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1) And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
    '                            For Each itemPhone In objListPhone
    '                                Dim objNewSignatoryPhone As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE
    '                                With objNewSignatoryPhone
    '                                    lngPK_Phone = lngPK_Phone - 1

    '                                    .PK_ACT_ACC_SIGN_PHONE_ID = lngPK_Phone
    '                                    .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                                    .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                                    '--ini wajib TPH_CONTACT_TYPE, TPH_COMMUNICATION_TYPE, TPH_NUMBER
    '                                    .TPH_CONTACT_TYPE = itemPhone.Tph_Contact_Type
    '                                    .TPH_COMMUNICATION_TYPE = itemPhone.Tph_Communication_Type
    '                                    .TPH_NUMBER = itemPhone.tph_number

    '                                    .TPH_COUNTRY_PREFIX = itemPhone.tph_country_prefix
    '                                    .TPH_EXTENSION = itemPhone.tph_extension
    '                                    .COMMENTS = itemPhone.comments

    '                                    If itemPhone.FK_Ref_Detail_Of = 1 Then
    '                                        .ISEMPLOYER = False
    '                                    Else
    '                                        .ISEMPLOYER = True
    '                                    End If

    '                                    .Active = 1
    '                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                                    .CreatedDate = DateTime.Now
    '                                End With
    '                                objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
    '                            Next
    '                        End If

    '                        'Signatory Identification(s)
    '                        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCustomer.PK_Customer_ID).ToList
    '                        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
    '                            For Each objIdentification In objListIdentification
    '                                Dim objNewSignatoryIdentification As New SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
    '                                With objNewSignatoryIdentification
    '                                    lngPK_Identification = lngPK_Identification - 1

    '                                    .PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK_Identification
    '                                    .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                                    '.FK_Person_Type = enumIdentificationPersonType.Signatory
    '                                    .FK_ACT_ACC_SIGN_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID
    '                                    '--ini wajib TYPE, NUMBER, ISSUED_COUNTRY
    '                                    .TYPE = objIdentification.Type
    '                                    .NUMBER = objIdentification.Number
    '                                    .ISSUED_COUNTRY = objIdentification.Issued_Country

    '                                    .ISSUED_DATE = objIdentification.Issue_Date
    '                                    .EXPIRY_DATE = objIdentification.Expiry_Date
    '                                    .ISSUED_BY = objIdentification.Issued_By
    '                                    .IDENTIFICATION_COMMENT = objIdentification.Identification_Comment

    '                                    .Active = 1
    '                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                                    .CreatedDate = DateTime.Now
    '                                End With
    '                                objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
    '                            Next
    '                        End If
    '                    End If
    '#Region "Signatory Account dengan cara GoAML"
    '                    'Dim objListgoAML_Ref_Account_Signatory As List(Of SIPESATGoAMLDAL.goAML_Ref_Account_Signatory) = objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objAccount.ACCOUNT).ToList
    '                    'If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
    '                    '    For Each objSignatory In objListgoAML_Ref_Account_Signatory

    '                    '        If objSignatory.isCustomer Then     'Jika Sigantory adalah Nasabah
    '                    '            Dim objSignatory_New As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY

    '                    '            'Get Signatory Nasabah
    '                    '            Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objSignatory.FK_CIF_Person_ID).FirstOrDefault
    '                    '            If objCIFPerson IsNot Nothing Then
    '                    '                With objSignatory_New
    '                    '                    lngPK_Signatory = lngPK_Signatory - 1

    '                    '                    .PK_ACT_ACC_SIGNATORY_ID = lngPK_Signatory
    '                    '                    .FK_Report_ID = objAccount.FK_REPORT_ID
    '                    '                    .FK_ACT_REPORTPARTYTYPE_ID = objAccount.FK_ACT_REPORTPARTY_ID
    '                    '                    .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID
    '                    '                    .CIF = objSignatory.FK_CIF_Person_ID

    '                    '                    'Load Person CIF Detail
    '                    '                    If objCIFPerson IsNot Nothing Then
    '                    '                        .GENDER = objCIFPerson.INDV_Gender
    '                    '                        '.Title = objCIFPerson.INDV_Title
    '                    '                        '.First_Name = objCIFPerson.INDV_First_name
    '                    '                        '.Middle_Name = objCIFPerson.INDV_Middle_name
    '                    '                        '.Prefix = objCIFPerson.INDV_Prefix
    '                    '                        .LAST_NAME = objCIFPerson.INDV_Last_Name
    '                    '                        .BIRTHDATE = objCIFPerson.INDV_BirthDate
    '                    '                        .BIRTH_PLACE = objCIFPerson.INDV_Birth_Place
    '                    '                        '.Mothers_Name = objCIFPerson.INDV_Mothers_Name
    '                    '                        '.Alias = objCIFPerson.INDV_Alias
    '                    '                        .SSN = objCIFPerson.INDV_SSN
    '                    '                        .PASSPORT_NUMBER = objCIFPerson.INDV_Passport_Number
    '                    '                        .PASSPORT_COUNTRY = objCIFPerson.INDV_Passport_Country
    '                    '                        .ID_NUMBER = objCIFPerson.INDV_ID_Number
    '                    '                        .NATIONALITY1 = objCIFPerson.INDV_Nationality1
    '                    '                        '.Nationality2 = objCIFPerson.INDV_Nationality2
    '                    '                        '.Nationality3 = objCIFPerson.INDV_Nationality3
    '                    '                        .RESIDENCE = objCIFPerson.INDV_Residence
    '                    '                        .EMAIL = objCIFPerson.INDV_Email
    '                    '                        '.email2 = objCIFPerson.INDV_Email2
    '                    '                        '.email3 = objCIFPerson.INDV_Email3
    '                    '                        '.email4 = objCIFPerson.INDV_Email4
    '                    '                        '.email5 = objCIFPerson.INDV_Email5
    '                    '                        .OCCUPATION = objCIFPerson.INDV_Occupation
    '                    '                        .EMPLOYER_NAME = objCIFPerson.INDV_Employer_Name
    '                    '                        .DECEASED = objCIFPerson.INDV_Deceased
    '                    '                        '.Deceased_Date = objCIFPerson.INDV_Deceased_Date
    '                    '                        .TAX_NUMBER = objCIFPerson.INDV_Tax_Number
    '                    '                        .TAX_REG_NUMBER = objCIFPerson.INDV_Tax_Reg_Number
    '                    '                        .SOURCE_OF_WEALTH = objCIFPerson.INDV_Source_of_Wealth
    '                    '                        .COMMENT = objCIFPerson.INDV_Comments

    '                    '                        .ISPRIMARY = objSignatory.isPrimary
    '                    '                        .ROLE = objSignatory.Role
    '                    '                        .Active = 1
    '                    '                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                        .CreatedDate = DateTime.Now
    '                    '                    End If
    '                    '                    objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

    '                    '                    'Signatory Address(es)  (1=Address, 5=Employer Address)
    '                    '                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_To_Table_ID = objCIFPerson.PK_Customer_ID).ToList
    '                    '                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
    '                    '                        For Each itemAddress In objListAddress
    '                    '                            Dim objNewSignatoryAddress As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS
    '                    '                            With objNewSignatoryAddress
    '                    '                                lngPK_Address = lngPK_Address - 1

    '                    '                                .PK_ACT_ACC_SIGN_ADDRESS_ID = lngPK_Address
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .ADDRESS_TYPE = itemAddress.Address_Type
    '                    '                                .ADDRESS = itemAddress.Address
    '                    '                                .TOWN = itemAddress.Town
    '                    '                                .CITY = itemAddress.City
    '                    '                                .ZIP = itemAddress.Zip
    '                    '                                .COUNTRY_CODE = itemAddress.Country_Code
    '                    '                                .STATE = itemAddress.State
    '                    '                                .COMMENTS = itemAddress.Comments

    '                    '                                If itemAddress.FK_Ref_Detail_Of = 1 Then
    '                    '                                    .ISEMPLOYER = False
    '                    '                                Else
    '                    '                                    .ISEMPLOYER = True
    '                    '                                End If

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
    '                    '                        Next
    '                    '                    End If

    '                    '                    'Signatory Phone(s) (1=Phone, 5=Employer Phone)
    '                    '                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 1 Or x.FK_Ref_Detail_Of = 5) And x.FK_for_Table_ID = objCIFPerson.PK_Customer_ID).ToList
    '                    '                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
    '                    '                        For Each itemPhone In objListPhone
    '                    '                            Dim objNewSignatoryPhone As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE
    '                    '                            With objNewSignatoryPhone
    '                    '                                lngPK_Phone = lngPK_Phone - 1

    '                    '                                .PK_ACT_ACC_SIGN_PHONE_ID = lngPK_Phone
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .TPH_CONTACT_TYPE = itemPhone.Tph_Contact_Type
    '                    '                                .TPH_COMMUNICATION_TYPE = itemPhone.Tph_Communication_Type
    '                    '                                .TPH_COUNTRY_PREFIX = itemPhone.tph_country_prefix
    '                    '                                .TPH_NUMBER = itemPhone.tph_number
    '                    '                                .TPH_EXTENSION = itemPhone.tph_extension
    '                    '                                .COMMENTS = itemPhone.comments

    '                    '                                If itemPhone.FK_Ref_Detail_Of = 1 Then
    '                    '                                    .ISEMPLOYER = False
    '                    '                                Else
    '                    '                                    .ISEMPLOYER = True
    '                    '                                End If

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
    '                    '                        Next
    '                    '                    End If

    '                    '                    'Signatory Identification(s)
    '                    '                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 1 And x.FK_Person_ID = objCIFPerson.PK_Customer_ID).ToList
    '                    '                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
    '                    '                        For Each objIdentification In objListIdentification
    '                    '                            Dim objNewSignatoryIdentification As New SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
    '                    '                            With objNewSignatoryIdentification
    '                    '                                lngPK_Identification = lngPK_Identification - 1

    '                    '                                .PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK_Identification
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                '.FK_Person_Type = enumIdentificationPersonType.Signatory
    '                    '                                .FK_ACT_ACC_SIGN_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .TYPE = objIdentification.Type
    '                    '                                .NUMBER = objIdentification.Number
    '                    '                                .ISSUED_DATE = objIdentification.Issue_Date
    '                    '                                .EXPIRY_DATE = objIdentification.Expiry_Date
    '                    '                                .ISSUED_BY = objIdentification.Issued_By
    '                    '                                .ISSUED_COUNTRY = objIdentification.Issued_Country
    '                    '                                .IDENTIFICATION_COMMENT = objIdentification.Identification_Comment

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
    '                    '                        Next
    '                    '                    End If
    '                    '                End With
    '                    '            End If
    '                    '        Else        '====== SIGNATORY NON NASABAH ======
    '                    '            Dim objSignatory_New As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY

    '                    '            'Get Signatory Non Nasabah
    '                    '            Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = objSignatory.WIC_No).FirstOrDefault
    '                    '            If objWICPerson IsNot Nothing Then
    '                    '                With objSignatory_New
    '                    '                    lngPK_Signatory = lngPK_Signatory - 1

    '                    '                    .PK_ACT_ACC_SIGNATORY_ID = lngPK_Signatory
    '                    '                    .FK_Report_ID = objAccount.FK_REPORT_ID
    '                    '                    .FK_ACT_REPORTPARTYTYPE_ID = objAccount.FK_ACT_REPORTPARTY_ID
    '                    '                    .FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID
    '                    '                    .WIC_No = objSignatory.WIC_No

    '                    '                    'Load Person CIF Detail
    '                    '                    If objWICPerson IsNot Nothing Then
    '                    '                        .GENDER = objWICPerson.INDV_Gender
    '                    '                        '.Title = objWICPerson.INDV_Title
    '                    '                        '.First_Name = objWICPerson.INDV_First_Name
    '                    '                        '.Middle_Name = objWICPerson.INDV_Middle_Name
    '                    '                        '.Prefix = objWICPerson.INDV_Prefix
    '                    '                        .LAST_NAME = objWICPerson.INDV_Last_Name
    '                    '                        .BIRTHDATE = objWICPerson.INDV_BirthDate
    '                    '                        .BIRTH_PLACE = objWICPerson.INDV_Birth_Place
    '                    '                        '.Mothers_Name = objWICPerson.INDV_Mothers_Name
    '                    '                        '.Alias = objWICPerson.INDV_Alias
    '                    '                        .SSN = objWICPerson.INDV_SSN
    '                    '                        .PASSPORT_NUMBER = objWICPerson.INDV_Passport_Number
    '                    '                        .PASSPORT_COUNTRY = objWICPerson.INDV_Passport_Country
    '                    '                        .ID_NUMBER = objWICPerson.INDV_ID_Number
    '                    '                        .NATIONALITY1 = objWICPerson.INDV_Nationality1
    '                    '                        '.Nationality2 = objWICPerson.INDV_Nationality2
    '                    '                        '.Nationality3 = objWICPerson.INDV_Nationality3
    '                    '                        .RESIDENCE = objWICPerson.INDV_Residence
    '                    '                        .EMAIL = objWICPerson.INDV_Email
    '                    '                        '.email2 = objWICPerson.INDV_Email2
    '                    '                        '.email3 = objWICPerson.INDV_Email3
    '                    '                        '.email4 = objWICPerson.INDV_Email4
    '                    '                        '.email5 = objWICPerson.INDV_Email5
    '                    '                        .OCCUPATION = objWICPerson.INDV_Occupation
    '                    '                        .EMPLOYER_NAME = objWICPerson.INDV_Employer_Name
    '                    '                        .DECEASED = objWICPerson.INDV_Deceased
    '                    '                        '.Deceased_Date = objWICPerson.INDV_Deceased_Date
    '                    '                        .TAX_NUMBER = objWICPerson.INDV_Tax_Number
    '                    '                        .TAX_REG_NUMBER = objWICPerson.INDV_Tax_Reg_Number
    '                    '                        .SOURCE_OF_WEALTH = objWICPerson.INDV_SumberDana
    '                    '                        .COMMENT = objWICPerson.INDV_Comment

    '                    '                        .ISPRIMARY = objSignatory.isPrimary
    '                    '                        .ROLE = objSignatory.Role
    '                    '                        .Active = 1
    '                    '                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                        .CreatedDate = DateTime.Now
    '                    '                    End If
    '                    '                    objData.list_goAML_Act_acc_Signatory.Add(objSignatory_New)

    '                    '                    'Signatory Address(es)  (3=Address, 10=Employer Address)
    '                    '                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
    '                    '                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
    '                    '                        For Each itemAddress In objListAddress
    '                    '                            Dim objNewSignatoryAddress As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS
    '                    '                            With objNewSignatoryAddress
    '                    '                                lngPK_Address = lngPK_Address - 1

    '                    '                                .PK_ACT_ACC_SIGN_ADDRESS_ID = lngPK_Address
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .ADDRESS_TYPE = itemAddress.Address_Type
    '                    '                                .ADDRESS = itemAddress.Address
    '                    '                                .TOWN = itemAddress.Town
    '                    '                                .CITY = itemAddress.City
    '                    '                                .ZIP = itemAddress.Zip
    '                    '                                .COUNTRY_CODE = itemAddress.Country_Code
    '                    '                                .STATE = itemAddress.State
    '                    '                                .COMMENTS = itemAddress.Comments

    '                    '                                If itemAddress.FK_Ref_Detail_Of = 3 Then
    '                    '                                    .ISEMPLOYER = False
    '                    '                                Else
    '                    '                                    .ISEMPLOYER = True
    '                    '                                End If

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Act_Acc_sign_Address.Add(objNewSignatoryAddress)
    '                    '                        Next
    '                    '                    End If

    '                    '                    'Signatory Phone(s) (3=Phone, 10=Employer Phone)
    '                    '                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 3 Or x.FK_Ref_Detail_Of = 10) And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
    '                    '                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
    '                    '                        For Each itemPhone In objListPhone
    '                    '                            Dim objNewSignatoryPhone As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE
    '                    '                            With objNewSignatoryPhone
    '                    '                                lngPK_Phone = lngPK_Phone - 1

    '                    '                                .PK_ACT_ACC_SIGN_PHONE_ID = lngPK_Phone
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                .FK_ACT_ACC_SIGNATORY_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .TPH_CONTACT_TYPE = itemPhone.Tph_Contact_Type
    '                    '                                .TPH_COMMUNICATION_TYPE = itemPhone.Tph_Communication_Type
    '                    '                                .TPH_COUNTRY_PREFIX = itemPhone.tph_country_prefix
    '                    '                                .TPH_NUMBER = itemPhone.tph_number
    '                    '                                .TPH_EXTENSION = itemPhone.tph_extension
    '                    '                                .COMMENTS = itemPhone.comments

    '                    '                                If itemPhone.FK_Ref_Detail_Of = 3 Then
    '                    '                                    .ISEMPLOYER = False
    '                    '                                Else
    '                    '                                    .ISEMPLOYER = True
    '                    '                                End If

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Act_Acc_sign_Phone.Add(objNewSignatoryPhone)
    '                    '                        Next
    '                    '                    End If

    '                    '                    'Signatory Identification(s)
    '                    '                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID).ToList
    '                    '                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
    '                    '                        For Each objIdentification In objListIdentification
    '                    '                            Dim objNewSignatoryIdentification As New SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
    '                    '                            With objNewSignatoryIdentification
    '                    '                                lngPK_Identification = lngPK_Identification - 1

    '                    '                                .PK_ACTIVITY_PERSON_IDENTIFICATION_ID = lngPK_Identification
    '                    '                                .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                    '                                '.FK_Person_Type = enumIdentificationPersonType.Signatory
    '                    '                                .FK_ACT_ACC_SIGN_ID = objSignatory_New.PK_ACT_ACC_SIGNATORY_ID

    '                    '                                .TYPE = objIdentification.Type
    '                    '                                .NUMBER = objIdentification.Number
    '                    '                                .ISSUED_DATE = objIdentification.Issue_Date
    '                    '                                .EXPIRY_DATE = objIdentification.Expiry_Date
    '                    '                                .ISSUED_BY = objIdentification.Issued_By
    '                    '                                .ISSUED_COUNTRY = objIdentification.Issued_Country
    '                    '                                .IDENTIFICATION_COMMENT = objIdentification.Identification_Comment

    '                    '                                .Active = 1
    '                    '                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                    '                                .CreatedDate = DateTime.Now
    '                    '                            End With
    '                    '                            objData.list_goAML_Activity_Person_Identification.Add(objNewSignatoryIdentification)
    '                    '                        Next
    '                    '                    End If
    '                    '                End With
    '                    '            End If
    '                    '        End If
    '                    '    Next
    '                    'End If
    '#End Region
    '                End If
    '            End Using
    '            'Balikin lagi datanya ke Front End
    '            Return objData
    '        Catch ex As Exception
    '            Throw
    '            Return Nothing
    '        End Try
    '    End Function

    '    Shared Function GetActivityAccountEntityClassByAccountID(objData As SIPESATGoAML_Activity_Class, strPK_Account_ID As String) As SIPESATGoAML_Activity_Class
    '        Try
    '            Dim objAccount = objData.list_goAML_Act_Account.Where(Function(x) x.PK_ACT_ACCOUNT_ID = strPK_Account_ID).FirstOrDefault

    '            'Hapus data2 yang lama (jika ada)
    '            If objAccount IsNot Nothing Then
    '                Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
    '                If objAccountEntity IsNot Nothing Then
    '                    'Account Entity Address
    '                    Dim listAccountEntityAddress = objData.list_goAML_Act_Acc_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ACCOUNT_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
    '                    If listAccountEntityAddress IsNot Nothing Then
    '                        For Each itemAddress_Old In listAccountEntityAddress
    '                            objData.list_goAML_Act_Acc_Entity_Address.Remove(itemAddress_Old)
    '                        Next
    '                    End If

    '                    'Account Entity Phone
    '                    Dim listAccountEntityPhone = objData.list_goAML_Act_Acc_Entity_Phone.Where(Function(x) x.FK_ACT_ACC_ENTITY_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID).ToList
    '                    If listAccountEntityPhone IsNot Nothing Then
    '                        For Each itemPhone_Old In listAccountEntityPhone
    '                            objData.list_goAML_Act_Acc_Entity_Phone.Remove(itemPhone_Old)
    '                        Next
    '                    End If

    '                    'Director
    '                    'sementara ga ada director
    '                    'Dim listDirector_Old = objData.list_goAML_Act_Acc_Ent_Director.Where(Function(x) x.FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account).ToList
    '                    'If listDirector_Old IsNot Nothing Then
    '                    '    For Each objDirector_Old In listDirector_Old
    '                    '        objData.list_goAML_Act_Acc_Ent_Director.Remove(objDirector_Old)

    '                    '        'Director Address
    '                    '        Dim listAddress_Old = objData.list_goAML_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
    '                    '        If listAddress_Old IsNot Nothing Then
    '                    '            For Each itemAddress_Old In listAddress_Old
    '                    '                objData.list_goAML_Act_Acc_Entity_Director_address.Remove(itemAddress_Old)
    '                    '            Next
    '                    '        End If

    '                    '        'Director Phone
    '                    '        Dim listPhone_Old = objData.list_goAML_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
    '                    '        If listPhone_Old IsNot Nothing Then
    '                    '            For Each itemPhone_Old In listPhone_Old
    '                    '                objData.list_goAML_Act_Acc_Entity_Director_Phone.Remove(itemPhone_Old)
    '                    '            Next
    '                    '        End If

    '                    '        'Director Identification
    '                    '        Dim listIdentification_Old = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_Person_Type = enumIdentificationPersonType.DirectorAccount And x.FK_Act_Person_ID = objDirector_Old.PK_Act_Acc_Ent_Director_ID).ToList
    '                    '        If listIdentification_Old IsNot Nothing Then
    '                    '            For Each objIdentification_Old In listIdentification_Old
    '                    '                objData.list_goAML_Activity_Person_Identification.Remove(objIdentification_Old)
    '                    '            Next
    '                    '        End If
    '                    '    Next
    '                    'End If
    '                End If
    '            End If

    '            'Masukkan data2 baru ke dalam Class
    '            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
    '                If objAccount IsNot Nothing Then
    '                    Dim objAccountEntity = objData.list_goAML_Act_Entity_Account.Where(Function(x) x.FK_ACT_ACCOUNT_ID = objAccount.PK_ACT_ACCOUNT_ID).FirstOrDefault
    '                    If objAccount IsNot Nothing Then
    '                        'Get Last PK
    '                        Dim lngPK_Entity_Address As Long = 0
    '                        Dim lngPK_Entity_Phone As Long = 0

    '#Region "Part Director dengan cara GoAML"
    '                        'Dim lngPK_Director As Long = 0
    '                        'Dim lngPK_Director_Address As Long = 0
    '                        'Dim lngPK_Director_Phone As Long = 0
    '                        'Dim lngPK_Director_Identification As Long = 0
    '#End Region

    '                        'PK Entity Address
    '                        If objData.list_goAML_Act_Acc_Entity_Address.Count > 0 Then
    '                            lngPK_Entity_Address = objData.list_goAML_Act_Acc_Entity_Address.Min(Function(x) x.PK_ACT_ACC_ENTITY_ADDRESS_ID)
    '                            If lngPK_Entity_Address > 0 Then
    '                                lngPK_Entity_Address = 0
    '                            End If
    '                        End If

    '                        'PK Entity Phone
    '                        If objData.list_goAML_Act_Acc_Entity_Phone.Count > 0 Then
    '                            lngPK_Entity_Phone = objData.list_goAML_Act_Acc_Entity_Phone.Min(Function(x) x.PK_ACT_ACC_ENTITY_PHONE_ID)
    '                            If lngPK_Entity_Phone > 0 Then
    '                                lngPK_Entity_Phone = 0
    '                            End If
    '                        End If

    '#Region "Part Director dengan cara GoAML"
    '                        'PK Director
    '                        'sementara ga ada director
    '                        'If objData.list_goAML_Act_Acc_Ent_Director.Count > 0 Then
    '                        '    lngPK_Director = objData.list_goAML_Act_Acc_Ent_Director.Min(Function(x) x.PK_Act_Acc_Ent_Director_ID)
    '                        '    If lngPK_Director > 0 Then
    '                        '        lngPK_Director = 0
    '                        '    End If
    '                        'End If

    '                        'PK Director Address
    '                        'sementara ga ada director
    '                        'If objData.list_goAML_Act_Acc_Entity_Director_address.Count > 0 Then
    '                        '    lngPK_Director_Address = objData.list_goAML_Act_Acc_Entity_Director_address.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_address_ID)
    '                        '    If lngPK_Director_Address > 0 Then
    '                        '        lngPK_Director_Address = 0
    '                        '    End If
    '                        'End If

    '                        'PK Director Phone
    '                        'sementara ga ada director
    '                        'If objData.list_goAML_Act_Acc_Entity_Director_Phone.Count > 0 Then
    '                        '    lngPK_Director_Phone = objData.list_goAML_Act_Acc_Entity_Director_Phone.Min(Function(x) x.PK_goAML_Act_Acc_Entity_Director_Phone_ID)
    '                        '    If lngPK_Director_Phone > 0 Then
    '                        '        lngPK_Director_Phone = 0
    '                        '    End If
    '                        'End If

    '                        'PK Director Identification
    '                        'If objData.list_goAML_Activity_Person_Identification.Count > 0 Then
    '                        '    lngPK_Director_Identification = objData.list_goAML_Activity_Person_Identification.Min(Function(x) x.PK_ACTIVITY_PERSON_IDENTIFICATION_ID)
    '                        '    If lngPK_Director_Identification > 0 Then
    '                        '        lngPK_Director_Identification = 0
    '                        '    End If
    '                        'End If
    '#End Region

    '                        'Masukkan data2 Entity Address & Phone dari goAML Ref
    '                        'Account Entity Address(es)
    '                        Dim objCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.CLIENT_NUMBER).FirstOrDefault
    '                        If objCustomer IsNot Nothing Then
    '                            Dim objListEntityAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                            If objListEntityAddress IsNot Nothing AndAlso objListEntityAddress.Count > 0 Then
    '                                For Each itemAddress In objListEntityAddress
    '                                    Dim objNewEntityAddress As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS
    '                                    With objNewEntityAddress
    '                                        lngPK_Entity_Address = lngPK_Entity_Address - 1

    '                                        .PK_ACT_ACC_ENTITY_ADDRESS_ID = lngPK_Entity_Address
    '                                        .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                                        .FK_ACT_ENTITY_ACCOUNT_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID

    '                                        .ADDRESS_TYPE = itemAddress.Address_Type
    '                                        .ADDRESS = itemAddress.Address
    '                                        .TOWN = itemAddress.Town
    '                                        .CITY = itemAddress.City
    '                                        .ZIP = itemAddress.Zip
    '                                        .COUNTRY_CODE = itemAddress.Country_Code
    '                                        .STATE = itemAddress.State
    '                                        .COMMENTS = itemAddress.Comments

    '                                        .Active = 1
    '                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                                        .CreatedDate = DateTime.Now
    '                                    End With
    '                                    objData.list_goAML_Act_Acc_Entity_Address.Add(objNewEntityAddress)
    '                                Next
    '                            End If

    '                            'Entity Phone(s)
    '                            Dim objListEntityPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objCustomer.PK_Customer_ID).ToList
    '                            If objListEntityPhone IsNot Nothing AndAlso objListEntityPhone.Count > 0 Then
    '                                For Each itemPhone In objListEntityPhone
    '                                    Dim objNewEntityPhone As New SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE
    '                                    With objNewEntityPhone
    '                                        lngPK_Entity_Phone = lngPK_Entity_Phone - 1

    '                                        .PK_ACT_ACC_ENTITY_PHONE_ID = lngPK_Entity_Phone
    '                                        .FK_REPORT_ID = objAccount.FK_REPORT_ID
    '                                        .FK_ACT_ACC_ENTITY_ID = objAccountEntity.PK_ACT_ENTITY_ACCOUNT_ID

    '                                        .TPH_CONTACT_TYPE = itemPhone.Tph_Contact_Type
    '                                        .TPH_COMMUNICATION_TYPE = itemPhone.Tph_Communication_Type
    '                                        .TPH_COUNTRY_PREFIX = itemPhone.tph_country_prefix
    '                                        .TPH_NUMBER = itemPhone.tph_number
    '                                        .TPH_EXTENSION = itemPhone.tph_extension
    '                                        .COMMENTS = itemPhone.comments

    '                                        .Active = 1
    '                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                                        .CreatedDate = DateTime.Now
    '                                    End With
    '                                    objData.list_goAML_Act_Acc_Entity_Phone.Add(objNewEntityPhone)
    '                                Next
    '                            End If
    '                        End If

    '#Region "Part Director dengan cara GoAML"
    '                        'Masukkand data2 Director dari goAML Ref
    '                        'sementara ga ada director
    '                        'Dim objListgoAML_Ref_Account_Director As List(Of goAML_Ref_Customer_Entity_Director) = objDB.goAML_Ref_Customer_Entity_Director.Where(Function(x) x.FK_Entity_ID = objAccount.CLIENT_NUMBER).ToList
    '                        'If objListgoAML_Ref_Account_Director IsNot Nothing Then
    '                        '    For Each objDirector In objListgoAML_Ref_Account_Director
    '                        '        Dim objDirector_New As New goAML_Act_Acc_Ent_Director
    '                        '        With objDirector_New
    '                        '            lngPK_Director = lngPK_Director - 1

    '                        '            .PK_Act_Acc_Ent_Director_ID = lngPK_Director
    '                        '            .FK_Report_ID = objAccount.FK_REPORT_ID
    '                        '            .FK_Act_Acc_Entity_ID = objAccountEntity.PK_goAML_Act_Entity_account

    '                        '            .Gender = objDirector.Gender
    '                        '            .Title = objDirector.Title
    '                        '            .First_Name = objDirector.First_name
    '                        '            .Middle_Name = objDirector.Middle_Name
    '                        '            .Prefix = objDirector.Prefix
    '                        '            .Last_Name = objDirector.Last_Name
    '                        '            .BirthDate = objDirector.BirthDate
    '                        '            .Birth_Place = objDirector.Birth_Place
    '                        '            .Mothers_Name = objDirector.Mothers_Name
    '                        '            .Alias = objDirector.Alias
    '                        '            .SSN = objDirector.SSN
    '                        '            .Passport_Number = objDirector.Passport_Number
    '                        '            .Passport_Country = objDirector.Passport_Country
    '                        '            .Id_Number = objDirector.ID_Number
    '                        '            .Nationality1 = objDirector.Nationality1
    '                        '            .Nationality2 = objDirector.Nationality2
    '                        '            .Nationality3 = objDirector.Nationality3
    '                        '            .Residence = objDirector.Residence
    '                        '            .Email = objDirector.Email
    '                        '            .email2 = objDirector.Email2
    '                        '            .email3 = objDirector.Email3
    '                        '            .email4 = objDirector.Email4
    '                        '            .email5 = objDirector.Email5
    '                        '            .Occupation = objDirector.Occupation
    '                        '            .Employer_Name = objDirector.Employer_Name
    '                        '            .Deceased = objDirector.Deceased
    '                        '            .Deceased_Date = objDirector.Deceased_Date
    '                        '            .Tax_Number = objDirector.Tax_Number
    '                        '            .Tax_Reg_Number = objDirector.Tax_Reg_Number
    '                        '            .Source_Of_Wealth = objDirector.Source_of_Wealth
    '                        '            .Comment = objDirector.Comments

    '                        '            .role = objDirector.Role
    '                        '            .Active = 1
    '                        '            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        '            .CreatedDate = DateTime.Now
    '                        '        End With
    '                        '        objData.list_goAML_Act_Acc_Ent_Director.Add(objDirector_New)

    '                        '        'Director Address(es)  (6=Address, 7=Employer Address)
    '                        '        Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_To_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
    '                        '        If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
    '                        '            For Each itemAddress In objListAddress
    '                        '                Dim objNewDirectorAddress As New goAML_Act_Acc_Entity_Director_address
    '                        '                With objNewDirectorAddress
    '                        '                    lngPK_Director_Address = lngPK_Director_Address - 1

    '                        '                    .PK_goAML_Act_Acc_Entity_Director_address_ID = lngPK_Director_Address
    '                        '                    .FK_Report_ID = objAccount.FK_REPORT_ID
    '                        '                    .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

    '                        '                    .Address_Type = itemAddress.Address_Type
    '                        '                    .Address = itemAddress.Address
    '                        '                    .Town = itemAddress.Town
    '                        '                    .City = itemAddress.City
    '                        '                    .Zip = itemAddress.Zip
    '                        '                    .Country_Code = itemAddress.Country_Code
    '                        '                    .State = itemAddress.State
    '                        '                    .Comments = itemAddress.Comments

    '                        '                    If itemAddress.FK_Ref_Detail_Of = 6 Then
    '                        '                        .isEmployer = False
    '                        '                    Else
    '                        '                        .isEmployer = True
    '                        '                    End If

    '                        '                    .Active = 1
    '                        '                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        '                    .CreatedDate = DateTime.Now
    '                        '                End With
    '                        '                objData.list_goAML_Act_Acc_Entity_Director_address.Add(objNewDirectorAddress)
    '                        '            Next
    '                        '        End If

    '                        '        'Director Phone(s) (6=Phone, 7=Employer Phone)
    '                        '        Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) (x.FK_Ref_Detail_Of = 6 Or x.FK_Ref_Detail_Of = 7) And x.FK_for_Table_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
    '                        '        If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
    '                        '            For Each itemPhone In objListPhone
    '                        '                Dim objNewDirectorPhone As New goAML_Act_Acc_Entity_Director_Phone
    '                        '                With objNewDirectorPhone
    '                        '                    lngPK_Director_Phone = lngPK_Director_Phone - 1

    '                        '                    .PK_goAML_Act_Acc_Entity_Director_Phone_ID = lngPK_Director_Phone
    '                        '                    .FK_Report_ID = objAccount.FK_REPORT_ID
    '                        '                    .FK_Act_Entity_Director_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

    '                        '                    .Tph_Contact_Type = itemPhone.Tph_Contact_Type
    '                        '                    .Tph_Communication_Type = itemPhone.Tph_Communication_Type
    '                        '                    .tph_country_prefix = itemPhone.tph_country_prefix
    '                        '                    .tph_number = itemPhone.tph_number
    '                        '                    .tph_extension = itemPhone.tph_extension
    '                        '                    .comments = itemPhone.comments

    '                        '                    If itemPhone.FK_Ref_Detail_Of = 6 Then
    '                        '                        .isEmployer = False
    '                        '                    Else
    '                        '                        .isEmployer = True
    '                        '                    End If

    '                        '                    .Active = 1
    '                        '                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        '                    .CreatedDate = DateTime.Now
    '                        '                End With
    '                        '                objData.list_goAML_Act_Acc_Entity_Director_Phone.Add(objNewDirectorPhone)
    '                        '            Next
    '                        '        End If

    '                        '        'Director Identification(s)
    '                        '        'Ada 2 faham yang berbeda untuk FK_Person_Type Director Account di goAML_Person_Identification. Yang diketahui SCB pakai 4, BSIM pakai 5
    '                        '        'Sesuaikan global parameter 3001. Karena kalau mau disesuaikan semua harus patches data, ubah SP generate CTR, IFTI, dll.
    '                        '        Dim intDirectrorAccountPersonType As Integer = getDirectorAccountPersonType()
    '                        '        Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intDirectrorAccountPersonType And x.FK_Person_ID = objDirector.PK_goAML_Ref_Customer_Entity_Director_ID).ToList
    '                        '        If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
    '                        '            For Each objIdentification In objListIdentification
    '                        '                Dim objNewDirectorIdentification As New goAML_Activity_Person_Identification
    '                        '                With objNewDirectorIdentification
    '                        '                    lngPK_Director_Identification = lngPK_Director_Identification - 1

    '                        '                    .PK_goAML_Activity_Person_Identification_ID = lngPK_Director_Identification
    '                        '                    .FK_Report_ID = objAccount.FK_REPORT_ID
    '                        '                    .FK_Person_Type = enumIdentificationPersonType.DirectorAccount
    '                        '                    .FK_Act_Person_ID = objDirector_New.PK_Act_Acc_Ent_Director_ID

    '                        '                    .Type = objIdentification.Type
    '                        '                    .Number = objIdentification.Number
    '                        '                    .Issue_Date = objIdentification.Issue_Date
    '                        '                    .Expiry_Date = objIdentification.Expiry_Date
    '                        '                    .Issued_By = objIdentification.Issued_By
    '                        '                    .Issued_Country = objIdentification.Issued_Country
    '                        '                    .Identification_Comment = objIdentification.Identification_Comment

    '                        '                    .Active = 1
    '                        '                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
    '                        '                    .CreatedDate = DateTime.Now
    '                        '                End With
    '                        '                objData.list_goAML_Activity_Person_Identification.Add(objNewDirectorIdentification)
    '                        '            Next
    '                        '        End If
    '                        '    Next
    '                        'End If
    '#End Region
    '                    End If
    '                End If
    '            End Using

    '            'Balikin lagi datanya ke Front End
    '            Return objData
    '        Catch ex As Exception
    '            Throw
    '            Return Nothing
    '        End Try
    '    End Function

    Shared Sub CreateNotes(actionApproval As Integer, actionForm As Integer, unikID As String, notes As String)
        Dim objHistory As New SIPESATGoAML_Note_History

        Dim approvalH As String
        Dim formH As String

        Select Case actionApproval
            Case 1
                approvalH = "Accept"
            Case 2
                approvalH = "Reject"
            Case 3
                approvalH = "Request Approval"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Select Case actionForm
            Case 1
                formH = "Add:<br/> "
            Case 2
                formH = "Edit:<br/> "
            Case 3
                formH = "Delete:<br/> "
            Case 4
                formH = ""
            Case Else
                Throw New ApplicationException("Action not found")
        End Select
        Dim strQuery2 As String
        strQuery2 = "INSERT INTO SIPESATGoAML_Note_History "
        strQuery2 += "("
        strQuery2 += "UnikID,"
        strQuery2 += "UserID,"
        strQuery2 += "UserName,"
        strQuery2 += "RoleID,"
        strQuery2 += "Status,"
        strQuery2 += "Notes,"
        strQuery2 += "Active,"
        strQuery2 += "CreatedBy,"
        strQuery2 += "CreatedDate"
        strQuery2 += ") VALUES ( "
        strQuery2 += " '" & unikID & "'"
        strQuery2 += " , '" & NawaBLL.Common.SessionCurrentUser.PK_MUser_ID & "'"
        strQuery2 += " , '" & NawaBLL.Common.SessionCurrentUser.UserName & "'"
        strQuery2 += " , '" & NawaBLL.Common.SessionCurrentUser.FK_MRole_ID & "'"
        strQuery2 += " , '" & approvalH & "'"
        strQuery2 += " , '" & formH & notes & "'"
        strQuery2 += " , 1"
        strQuery2 += " , '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
        strQuery2 += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)


    End Sub

    Shared Function GetGoAMLReportClassByID(ID As Long) As SIPESATGoAML_Report_Class
        Try
            Dim objGoAMLReportClass = New SIPESATGoAML_Report_Class
            Dim objGoAMLReportDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_REPORT where PK_REPORT_ID = " & ID, Nothing)
            Dim NewObjGoAMLReport As New SIPESATGOAML_REPORT
            For Each item As DataRow In objGoAMLReportDataTable.Rows
                NewObjGoAMLReport.CreatedBy = item("CreatedBy")
                NewObjGoAMLReport.CreatedDate = item("CreatedDate")
                If Not IsDBNull(item("CURRENCY_CODE_LOCAL")) Then
                    NewObjGoAMLReport.CURRENCY_CODE_LOCAL = item("CURRENCY_CODE_LOCAL")
                End If
                If Not IsDBNull(item("PK_REPORT_ID")) Then
                    NewObjGoAMLReport.PK_REPORT_ID = item("PK_REPORT_ID")
                End If


                If Not IsDBNull(item("ENTITY_REFERENCE")) Then
                    NewObjGoAMLReport.ENTITY_REFERENCE = item("ENTITY_REFERENCE")
                End If
                If Not IsDBNull(item("ID_LAPORAN_PPATK_RESULT")) Then
                    NewObjGoAMLReport.ID_LAPORAN_PPATK_RESULT = item("ID_LAPORAN_PPATK_RESULT")
                End If
                If Not IsDBNull(item("RENTITY_ID")) Then
                    NewObjGoAMLReport.RENTITY_ID = item("RENTITY_ID")
                End If
                If Not IsDBNull(item("UNIKREFERENCE")) Then
                    NewObjGoAMLReport.UNIKREFERENCE = item("UNIKREFERENCE")
                End If
                If Not IsDBNull(item("MONTH_PERIOD")) Then
                    NewObjGoAMLReport.MONTH_PERIOD = item("MONTH_PERIOD")
                End If
                If Not IsDBNull(item("MarkedAsDelete")) Then
                    NewObjGoAMLReport.MarkedAsDelete = item("MarkedAsDelete")
                End If
                If Not IsDBNull(item("ISVALID")) Then
                    NewObjGoAMLReport.ISVALID = item("ISVALID")
                End If
                If Not IsDBNull(item("LastUpdateBy")) Then
                    NewObjGoAMLReport.LastUpdateBy = item("LastUpdateBy")
                End If



                If Not IsDBNull(item("REPORT_CODE")) Then
                    NewObjGoAMLReport.REPORT_CODE = item("REPORT_CODE")
                End If
                If Not IsDBNull(item("STATUS")) Then
                    NewObjGoAMLReport.STATUS = item("STATUS")
                End If
                If Not IsDBNull(item("SUBMISSION_CODE")) Then
                    NewObjGoAMLReport.SUBMISSION_CODE = item("SUBMISSION_CODE")
                End If
                If Not IsDBNull(item("SUBMISSION_DATE")) Then
                    NewObjGoAMLReport.SUBMISSION_DATE = item("SUBMISSION_DATE")
                End If


            Next

            Dim objGoAMLReportTypeDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_REPORTPARTYTYPE where FK_Report_ID = " & NewObjGoAMLReport.PK_REPORT_ID, Nothing)
            Dim NewListObjGoAMLReportType As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)
            For Each item As DataRow In objGoAMLReportTypeDataTable.Rows
                Dim NewObjGoAMLReportType As New SIPESATGOAML_ACT_REPORTPARTYTYPE
                If Not IsDBNull(item("ApprovedBy")) Then
                    NewObjGoAMLReportType.ApprovedBy = item("ApprovedBy")
                End If
                If Not IsDBNull(item("ApprovedDate")) Then
                    NewObjGoAMLReportType.ApprovedDate = item("ApprovedDate")
                End If
                If Not IsDBNull(item("COMMENTS")) Then
                    NewObjGoAMLReportType.COMMENTS = item("COMMENTS")
                End If
                If Not IsDBNull(item("CreatedBy")) Then
                    NewObjGoAMLReportType.CreatedBy = item("CreatedBy")
                End If
                If Not IsDBNull(item("CreatedDate")) Then
                    NewObjGoAMLReportType.CreatedDate = item("CreatedDate")
                End If
                If Not IsDBNull(item("FK_Report_ID")) Then
                    NewObjGoAMLReportType.FK_Report_ID = item("FK_Report_ID")
                End If
                If Not IsDBNull(item("PK_ACT_REPORTPARTYTYPE_ID")) Then
                    NewObjGoAMLReportType.PK_ACT_REPORTPARTYTYPE_ID = item("PK_ACT_REPORTPARTYTYPE_ID")
                End If
                If Not IsDBNull(item("REASON")) Then
                    NewObjGoAMLReportType.REASON = item("REASON")
                End If
                If Not IsDBNull(item("SIGNIFICANCE")) Then
                    NewObjGoAMLReportType.SIGNIFICANCE = item("SIGNIFICANCE")
                End If
                If Not IsDBNull(item("SUBNODETYPE")) Then
                    NewObjGoAMLReportType.SUBNODETYPE = item("SUBNODETYPE")
                End If

                NewListObjGoAMLReportType.Add(NewObjGoAMLReportType)
            Next
            With objGoAMLReportClass
                'GoAML Report
                If NewObjGoAMLReport.PK_REPORT_ID <> 0 Then
                    .obj_goAML_Report = NewObjGoAMLReport
                End If



                'Transaction
                'Dim listTransaction = objDb.goAML_Transaction.Where(Function(x) x.FK_Report_ID = ID).ToList
                'If listTransaction IsNot Nothing Then
                '    .list_goAML_Transaction = listTransaction
                'End If

                'Activity
                Dim listActivity = NewListObjGoAMLReportType
                If listActivity IsNot Nothing Then
                    .list_goAML_Act_ReportPartyType = listActivity
                End If

                'Indicator
                'Dim listIndicator = objDb.goAML_Report_Indicator.Where(Function(x) x.FK_Report = ID).ToList
                'If listIndicator IsNot Nothing Then
                '    .list_goAML_Report_Indicator = listIndicator
                'End If
            End With

            Return objGoAMLReportClass
        Catch ex As Exception

        End Try

    End Function

    Shared Function GetGoAMLActivityClassByID(ID As Long) As SIPESATGoAML_Activity_Class
        Using objDb As New SIPESATGoAMLDAL.SIPESATGoAMLEntities

            Dim objGoAMLActivityClass = New SIPESATGoAML_Activity_Class
            Dim objGoAMLReportTypeDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_REPORTPARTYTYPE where PK_ACT_REPORTPARTYTYPE_ID = " & ID, Nothing)
            Dim NewObjGoAMLReportType As New SIPESATGOAML_ACT_REPORTPARTYTYPE
            For Each item As DataRow In objGoAMLReportTypeDataTable.Rows
                If Not IsDBNull(item("ApprovedBy")) Then
                    NewObjGoAMLReportType.ApprovedBy = item("ApprovedBy")
                End If
                If Not IsDBNull(item("ApprovedDate")) Then
                    NewObjGoAMLReportType.ApprovedDate = item("ApprovedDate")
                End If
                If Not IsDBNull(item("COMMENTS")) Then
                    NewObjGoAMLReportType.COMMENTS = item("COMMENTS")
                End If
                If Not IsDBNull(item("CreatedBy")) Then
                    NewObjGoAMLReportType.CreatedBy = item("CreatedBy")
                End If
                If Not IsDBNull(item("CreatedDate")) Then
                    NewObjGoAMLReportType.CreatedDate = item("CreatedDate")
                End If
                If Not IsDBNull(item("FK_Report_ID")) Then
                    NewObjGoAMLReportType.FK_Report_ID = item("FK_Report_ID")
                End If
                If Not IsDBNull(item("PK_ACT_REPORTPARTYTYPE_ID")) Then
                    NewObjGoAMLReportType.PK_ACT_REPORTPARTYTYPE_ID = item("PK_ACT_REPORTPARTYTYPE_ID")
                End If
                If Not IsDBNull(item("REASON")) Then
                    NewObjGoAMLReportType.REASON = item("REASON")
                End If
                If Not IsDBNull(item("SIGNIFICANCE")) Then
                    NewObjGoAMLReportType.SIGNIFICANCE = item("SIGNIFICANCE")
                End If
                If Not IsDBNull(item("SUBNODETYPE")) Then
                    NewObjGoAMLReportType.SUBNODETYPE = item("SUBNODETYPE")
                End If

            Next



            'Entity

            'Director


            With objGoAMLActivityClass
                'GoAML Activity
                .obj_goAML_Act_ReportPartyType = NewObjGoAMLReportType

                If .obj_goAML_Act_ReportPartyType IsNot Nothing Then
                    Dim objActivity = .obj_goAML_Act_ReportPartyType

                    ' 27 Dec 2022 Ari : Account Di hide
                    'Person
                    Dim objPersonDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_PERSON where FK_ACT_REPORTPARTY_ID = " & objActivity.PK_ACT_REPORTPARTYTYPE_ID, Nothing)
                    Dim ListNewPerson As New List(Of SIPESATGOAML_ACT_PERSON)
                    For Each item As DataRow In objPersonDataTable.Rows
                        Dim NewPerson As New SIPESATGOAML_ACT_PERSON
                        If Not IsDBNull(item("ALIAS")) Then
                            NewPerson.ALIAS = item("ALIAS")
                        End If
                        If Not IsDBNull(item("BIRTHDATE")) Then
                            NewPerson.BIRTHDATE = item("BIRTHDATE")
                        End If
                        If Not IsDBNull(item("BITH_PLACE")) Then
                            NewPerson.BITH_PLACE = item("BITH_PLACE")
                        End If
                        If Not IsDBNull(item("COMMENT")) Then
                            NewPerson.COMMENT = item("COMMENT")
                        End If
                        If Not IsDBNull(item("DECEASED_DATE")) Then
                            NewPerson.DECEASED_DATE = item("DECEASED_DATE")
                        End If
                        If Not IsDBNull(item("CreatedDate")) Then
                            NewPerson.CreatedDate = item("CreatedDate")
                        End If
                        If Not IsDBNull(item("CreatedBy")) Then
                            NewPerson.CreatedBy = item("CreatedBy")
                        End If
                        If Not IsDBNull(item("DECEASED")) Then
                            NewPerson.DECEASED = item("DECEASED")
                        End If
                        If Not IsDBNull(item("EMAIL")) Then
                            NewPerson.EMAIL = item("EMAIL")
                        End If
                        If Not IsDBNull(item("EMAIL2")) Then
                            NewPerson.EMAIL2 = item("EMAIL2")
                        End If
                        If Not IsDBNull(item("EMAIL3")) Then
                            NewPerson.EMAIL3 = item("EMAIL3")
                        End If
                        If Not IsDBNull(item("EMAIL4")) Then
                            NewPerson.EMAIL4 = item("EMAIL4")
                        End If
                        If Not IsDBNull(item("EMAIL5")) Then
                            NewPerson.EMAIL5 = item("EMAIL5")
                        End If
                        If Not IsDBNull(item("EMPLOYER_NAME")) Then
                            NewPerson.EMPLOYER_NAME = item("EMPLOYER_NAME")
                        End If
                        If Not IsDBNull(item("FIRST_NAME")) Then
                            NewPerson.FIRST_NAME = item("FIRST_NAME")
                        End If
                        If Not IsDBNull(item("FK_ACT_REPORTPARTY_ID")) Then
                            NewPerson.FK_ACT_REPORTPARTY_ID = item("FK_ACT_REPORTPARTY_ID")
                        End If
                        If Not IsDBNull(item("FK_REPORT_ID")) Then
                            NewPerson.FK_REPORT_ID = item("FK_REPORT_ID")
                        End If
                        If Not IsDBNull(item("GENDER")) Then
                            NewPerson.GENDER = item("GENDER")
                        End If
                        If Not IsDBNull(item("ID_NUMBER")) Then
                            NewPerson.ID_NUMBER = item("ID_NUMBER")
                        End If
                        If Not IsDBNull(item("LAST_NAME")) Then
                            NewPerson.LAST_NAME = item("LAST_NAME")
                        End If
                        If Not IsDBNull(item("MIDDLE_NAME")) Then
                            NewPerson.MIDDLE_NAME = item("MIDDLE_NAME")
                        End If
                        If Not IsDBNull(item("MOTHERS_NAME")) Then
                            NewPerson.MOTHERS_NAME = item("MOTHERS_NAME")
                        End If
                        If Not IsDBNull(item("NATIONALITY1")) Then
                            NewPerson.NATIONALITY1 = item("NATIONALITY1")
                        End If
                        If Not IsDBNull(item("NATIONALITY2")) Then
                            NewPerson.NATIONALITY2 = item("NATIONALITY2")
                        End If
                        If Not IsDBNull(item("NATIONALITY3")) Then
                            NewPerson.NATIONALITY3 = item("NATIONALITY3")
                        End If
                        If Not IsDBNull(item("OCCUPATION")) Then
                            NewPerson.OCCUPATION = item("OCCUPATION")
                        End If
                        If Not IsDBNull(item("PASSPORT_COUNTRY")) Then
                            NewPerson.PASSPORT_COUNTRY = item("PASSPORT_COUNTRY")
                        End If
                        If Not IsDBNull(item("PASSPORT_NUMBER")) Then
                            NewPerson.PASSPORT_NUMBER = item("PASSPORT_NUMBER")
                        End If
                        If Not IsDBNull(item("PK_SIPESATGOAML_ACT_PERSON_ID")) Then
                            NewPerson.PK_SIPESATGOAML_ACT_PERSON_ID = item("PK_SIPESATGOAML_ACT_PERSON_ID")
                        End If
                        If Not IsDBNull(item("PREFIX")) Then
                            NewPerson.PREFIX = item("PREFIX")
                        End If
                        If Not IsDBNull(item("RESIDENCE")) Then
                            NewPerson.RESIDENCE = item("RESIDENCE")
                        End If
                        If Not IsDBNull(item("SOURCE_OF_WEALTH")) Then
                            NewPerson.SOURCE_OF_WEALTH = item("SOURCE_OF_WEALTH")
                        End If
                        If Not IsDBNull(item("SSN")) Then
                            NewPerson.SSN = item("SSN")
                        End If
                        If Not IsDBNull(item("TAX_NUMBER")) Then
                            NewPerson.TAX_NUMBER = item("TAX_NUMBER")
                        End If
                        If Not IsDBNull(item("TAX_REG_NUMBER")) Then
                            NewPerson.TAX_REG_NUMBER = item("TAX_REG_NUMBER")
                        End If
                        If Not IsDBNull(item("TITLE")) Then
                            NewPerson.TITLE = item("TITLE")
                        End If

                        ListNewPerson.Add(NewPerson)
                    Next



                    Dim listPerson = ListNewPerson
                    If listPerson IsNot Nothing Then .list_goAML_Act_Person.AddRange(listPerson)


                    If .list_goAML_Act_Person IsNot Nothing AndAlso .list_goAML_Act_Person.Count > 0 Then
                        For Each item In .list_goAML_Act_Person
                            'Address
                            Dim objPersonAddressDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_PERSON_ADDRESS where FK_ACT_PERSON = " & item.PK_SIPESATGOAML_ACT_PERSON_ID, Nothing)
                            Dim ListNewPersonAddress As New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
                            For Each itemAddress As DataRow In objPersonAddressDataTable.Rows
                                Dim NewPersonAddress As New SIPESATGOAML_ACT_PERSON_ADDRESS
                                If Not IsDBNull(itemAddress("Comments")) Then
                                    NewPersonAddress.COMMENTS = itemAddress("Comments")
                                End If
                                If Not IsDBNull(itemAddress("State")) Then
                                    NewPersonAddress.STATE = itemAddress("State")
                                End If
                                If Not IsDBNull(itemAddress("Country_Code")) Then
                                    NewPersonAddress.COUNTRY_CODE = itemAddress("Country_Code")
                                End If
                                If Not IsDBNull(itemAddress("Zip")) Then
                                    NewPersonAddress.ZIP = itemAddress("Zip")
                                End If
                                If Not IsDBNull(itemAddress("City")) Then
                                    NewPersonAddress.CITY = itemAddress("City")
                                End If
                                If Not IsDBNull(itemAddress("Town")) Then
                                    NewPersonAddress.TOWN = itemAddress("Town")
                                End If
                                If Not IsDBNull(itemAddress("Address")) Then
                                    NewPersonAddress.ADDRESS = itemAddress("Address")
                                End If
                                If Not IsDBNull(itemAddress("Address_Type")) Then
                                    NewPersonAddress.ADDRESS_TYPE = itemAddress("Address_Type")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedBy")) Then
                                    NewPersonAddress.ApprovedBy = itemAddress("ApprovedBy")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedDate")) Then
                                    NewPersonAddress.ApprovedDate = itemAddress("ApprovedDate")
                                End If
                                If Not IsDBNull(itemAddress("CreatedBy")) Then
                                    NewPersonAddress.CreatedBy = itemAddress("CreatedBy")
                                End If
                                If Not IsDBNull(itemAddress("CreatedDate")) Then
                                    NewPersonAddress.CreatedDate = itemAddress("CreatedDate")
                                End If
                                If Not IsDBNull(itemAddress("FK_ACT_PERSON")) Then
                                    NewPersonAddress.FK_ACT_PERSON = itemAddress("FK_ACT_PERSON")
                                End If
                                If Not IsDBNull(itemAddress("FK_REPORT_ID")) Then
                                    NewPersonAddress.FK_REPORT_ID = itemAddress("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemAddress("ISEMPLOYER")) Then
                                    NewPersonAddress.ISEMPLOYER = itemAddress("ISEMPLOYER")
                                End If

                                NewPersonAddress.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = itemAddress("PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID")

                                ListNewPersonAddress.Add(NewPersonAddress)
                            Next
                            Dim listAddress = ListNewPersonAddress

                            'Phone
                            Dim objPersonPhoneDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_PERSON_Phone where FK_ACT_PERSON = " & item.PK_SIPESATGOAML_ACT_PERSON_ID, Nothing)
                            Dim ListNewPersonPhone As New List(Of SIPESATGOAML_ACT_PERSON_PHONE)
                            For Each itemPhone As DataRow In objPersonPhoneDataTable.Rows
                                Dim NewPersonPhone As New SIPESATGOAML_ACT_PERSON_PHONE
                                If Not IsDBNull(itemPhone("ApprovedDate")) Then
                                    NewPersonPhone.ApprovedDate = itemPhone("ApprovedDate")
                                End If
                                If Not IsDBNull(itemPhone("COMMENTS")) Then
                                    NewPersonPhone.COMMENTS = itemPhone("COMMENTS")
                                End If
                                If Not IsDBNull(itemPhone("CreatedBy")) Then
                                    NewPersonPhone.CreatedBy = itemPhone("CreatedBy")
                                End If
                                If Not IsDBNull(itemPhone("CreatedDate")) Then
                                    NewPersonPhone.CreatedDate = itemPhone("CreatedDate")
                                End If
                                If Not IsDBNull(itemPhone("FK_ACT_PERSON")) Then
                                    NewPersonPhone.FK_ACT_PERSON = itemPhone("FK_ACT_PERSON")
                                End If
                                If Not IsDBNull(itemPhone("FK_REPORT_ID")) Then
                                    NewPersonPhone.FK_REPORT_ID = itemPhone("FK_REPORT_ID")
                                End If
                                NewPersonPhone.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = itemPhone("PK_SIPESATGOAML_ACT_PERSON_PHONE_ID")
                                If Not IsDBNull(itemPhone("ISEMPLOYER")) Then
                                    NewPersonPhone.ISEMPLOYER = itemPhone("ISEMPLOYER")
                                End If
                                If Not IsDBNull(itemPhone("Tph_Contact_Type")) Then
                                    NewPersonPhone.TPH_CONTACT_TYPE = itemPhone("Tph_Contact_Type")
                                End If
                                If Not IsDBNull(itemPhone("Tph_Communication_Type")) Then
                                    NewPersonPhone.TPH_COMMUNICATION_TYPE = itemPhone("Tph_Communication_Type")
                                End If
                                If Not IsDBNull(itemPhone("tph_country_prefix")) Then
                                    NewPersonPhone.TPH_COUNTRY_PREFIX = itemPhone("tph_country_prefix")
                                End If
                                If Not IsDBNull(itemPhone("tph_number")) Then
                                    NewPersonPhone.TPH_NUMBER = itemPhone("tph_number")
                                End If
                                If Not IsDBNull(itemPhone("tph_extension")) Then
                                    NewPersonPhone.TPH_EXTENSION = itemPhone("tph_extension")
                                End If
                                If Not IsDBNull(itemPhone("comments")) Then
                                    NewPersonPhone.COMMENTS = itemPhone("comments")
                                End If
                                ListNewPersonPhone.Add(NewPersonPhone)
                            Next
                            Dim listPhone = ListNewPersonPhone

                            'Relationship
                            Dim objPersonRelationshipDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_PERSON_Relationship where FK_ACT_PERSON_ID = " & item.PK_SIPESATGOAML_ACT_PERSON_ID, Nothing)
                            Dim ListNewPersonRelationship As New List(Of SIPESATGOAML_ACT_PERSON_RELATIONSHIP)
                            For Each itemRelationship As DataRow In objPersonRelationshipDataTable.Rows
                                Dim NewPersonRelationship As New SIPESATGOAML_ACT_PERSON_RELATIONSHIP
                                If Not IsDBNull(itemRelationship("ApprovedDate")) Then
                                    NewPersonRelationship.ApprovedDate = itemRelationship("ApprovedDate")
                                End If
                                If Not IsDBNull(itemRelationship("COMMENTS")) Then
                                    NewPersonRelationship.COMMENTS = itemRelationship("COMMENTS")
                                End If
                                If Not IsDBNull(itemRelationship("CreatedBy")) Then
                                    NewPersonRelationship.CreatedBy = itemRelationship("CreatedBy")
                                End If
                                If Not IsDBNull(itemRelationship("CreatedDate")) Then
                                    NewPersonRelationship.CreatedDate = itemRelationship("CreatedDate")
                                End If
                                If Not IsDBNull(itemRelationship("CLIENT_NUMBER")) Then
                                    NewPersonRelationship.CLIENT_NUMBER = itemRelationship("CLIENT_NUMBER")
                                End If
                                If Not IsDBNull(itemRelationship("FK_ACT_PERSON_ID")) Then
                                    NewPersonRelationship.FK_ACT_PERSON_ID = itemRelationship("FK_ACT_PERSON_ID")
                                End If
                                If Not IsDBNull(itemRelationship("FK_REPORT_ID")) Then
                                    NewPersonRelationship.FK_REPORT_ID = itemRelationship("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemRelationship("IS_APPROX_FROM_DATE")) Then
                                    NewPersonRelationship.IS_APPROX_FROM_DATE = itemRelationship("IS_APPROX_FROM_DATE")
                                End If
                                If Not IsDBNull(itemRelationship("PK_SIPESATGOAML_ACT_PERSON_Relationship_ID")) Then
                                    NewPersonRelationship.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID = itemRelationship("PK_SIPESATGOAML_ACT_PERSON_Relationship_ID")
                                End If
                                If Not IsDBNull(itemRelationship("VALID_FROM")) Then
                                    NewPersonRelationship.VALID_FROM = itemRelationship("VALID_FROM")
                                End If
                                If Not IsDBNull(itemRelationship("IS_APPROX_TO_DATE")) Then
                                    NewPersonRelationship.IS_APPROX_TO_DATE = itemRelationship("IS_APPROX_TO_DATE")
                                End If
                                If Not IsDBNull(itemRelationship("VALID_TO")) Then
                                    NewPersonRelationship.VALID_TO = itemRelationship("VALID_TO")
                                End If


                                ListNewPersonRelationship.Add(NewPersonRelationship)
                            Next
                            Dim listRelationship = ListNewPersonRelationship

                            'Identification
                            Dim objPersonIdentificationDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION where FK_ACT_PERSON = " & item.PK_SIPESATGOAML_ACT_PERSON_ID, Nothing)
                            Dim ListNewPersonIdentification As New List(Of SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                            For Each itemIdentification As DataRow In objPersonIdentificationDataTable.Rows
                                Dim NewPersonIdentification As New SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
                                If Not IsDBNull(itemIdentification("ApprovedDate")) Then
                                    NewPersonIdentification.ApprovedDate = itemIdentification("ApprovedDate")
                                End If
                                If Not IsDBNull(itemIdentification("EXPIRY_DATE")) Then
                                    NewPersonIdentification.EXPIRY_DATE = itemIdentification("EXPIRY_DATE")
                                End If
                                If Not IsDBNull(itemIdentification("CreatedBy")) Then
                                    NewPersonIdentification.CreatedBy = itemIdentification("CreatedBy")
                                End If
                                If Not IsDBNull(itemIdentification("CreatedDate")) Then
                                    NewPersonIdentification.CreatedDate = itemIdentification("CreatedDate")
                                End If
                                If Not IsDBNull(itemIdentification("FK_ACT_PERSON")) Then
                                    NewPersonIdentification.FK_ACT_PERSON = itemIdentification("FK_ACT_PERSON")
                                End If
                                If Not IsDBNull(itemIdentification("FK_REPORT_ID")) Then
                                    NewPersonIdentification.FK_REPORT_ID = itemIdentification("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemIdentification("IDENTIFICATION_COMMENT")) Then
                                    NewPersonIdentification.IDENTIFICATION_COMMENT = itemIdentification("IDENTIFICATION_COMMENT")
                                End If
                                If Not IsDBNull(itemIdentification("PK_ACTIVITY_PERSON_IDENTIFICATION_ID")) Then
                                    NewPersonIdentification.PK_ACTIVITY_PERSON_IDENTIFICATION_ID = itemIdentification("PK_ACTIVITY_PERSON_IDENTIFICATION_ID")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_BY")) Then
                                    NewPersonIdentification.ISSUED_BY = itemIdentification("ISSUED_BY")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_COUNTRY")) Then
                                    NewPersonIdentification.ISSUED_COUNTRY = itemIdentification("ISSUED_COUNTRY")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_DATE")) Then
                                    NewPersonIdentification.ISSUED_DATE = itemIdentification("ISSUED_DATE")
                                End If
                                If Not IsDBNull(itemIdentification("NUMBER")) Then
                                    NewPersonIdentification.NUMBER = itemIdentification("NUMBER")
                                End If
                                If Not IsDBNull(itemIdentification("TYPE")) Then
                                    NewPersonIdentification.TYPE = itemIdentification("TYPE")
                                End If

                                ListNewPersonIdentification.Add(NewPersonIdentification)
                            Next
                            Dim listIdentification = ListNewPersonIdentification

                            If listAddress IsNot Nothing Then .list_goAML_Act_Person_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Person_Phone.AddRange(listPhone)
                            If listRelationship IsNot Nothing Then .list_goAML_Act_Person_Relationship.AddRange(listRelationship)
                            If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)

                        Next
                    End If

                    'Entity
                    Dim objEntityDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Entity where FK_ACT_REPORTPARTY_ID = " & objActivity.PK_ACT_REPORTPARTYTYPE_ID, Nothing)
                    Dim ListNewEntity As New List(Of SIPESATGOAML_ACT_ENTITY)
                    For Each item As DataRow In objEntityDataTable.Rows
                        Dim NewEntity As New SIPESATGOAML_ACT_ENTITY
                        If Not IsDBNull(item("ApprovedBy")) Then
                            NewEntity.ApprovedBy = item("ApprovedBy")
                        End If
                        If Not IsDBNull(item("ApprovedDate")) Then
                            NewEntity.ApprovedDate = item("ApprovedDate")
                        End If
                        If Not IsDBNull(item("BUSINESS")) Then
                            NewEntity.BUSINESS = item("BUSINESS")
                        End If
                        If Not IsDBNull(item("BUSINESS_CLOSED")) Then
                            NewEntity.BUSINESS_CLOSED = item("BUSINESS_CLOSED")
                        End If
                        If Not IsDBNull(item("COMMENTS")) Then
                            NewEntity.COMMENTS = item("COMMENTS")
                        End If
                        If Not IsDBNull(item("COMMERCIAL_NAME")) Then
                            NewEntity.COMMERCIAL_NAME = item("COMMERCIAL_NAME")
                        End If
                        If Not IsDBNull(item("CreatedBy")) Then
                            NewEntity.CreatedBy = item("CreatedBy")
                        End If
                        If Not IsDBNull(item("CreatedDate")) Then
                            NewEntity.CreatedDate = item("CreatedDate")
                        End If
                        If Not IsDBNull(item("DATE_BUSINESS_CLOSED")) Then
                            NewEntity.DATE_BUSINESS_CLOSED = item("DATE_BUSINESS_CLOSED")
                        End If
                        If Not IsDBNull(item("DIRECTOR_ID")) Then
                            NewEntity.DIRECTOR_ID = item("DIRECTOR_ID")
                        End If
                        If Not IsDBNull(item("EMAIL")) Then
                            NewEntity.EMAIL = item("EMAIL")
                        End If
                        If Not IsDBNull(item("FK_ACT_REPORTPARTY_ID")) Then
                            NewEntity.FK_ACT_REPORTPARTY_ID = item("FK_ACT_REPORTPARTY_ID")
                        End If
                        If Not IsDBNull(item("FK_REPORT_ID")) Then
                            NewEntity.FK_REPORT_ID = item("FK_REPORT_ID")
                        End If
                        If Not IsDBNull(item("INCORPORATION_COUNTRY_CODE")) Then
                            NewEntity.INCORPORATION_COUNTRY_CODE = item("INCORPORATION_COUNTRY_CODE")
                        End If
                        If Not IsDBNull(item("INCORPORATION_DATE")) Then
                            NewEntity.INCORPORATION_DATE = item("INCORPORATION_DATE")
                        End If
                        If Not IsDBNull(item("INCORPORATION_LEGAL_FORM")) Then
                            NewEntity.INCORPORATION_LEGAL_FORM = item("INCORPORATION_LEGAL_FORM")
                        End If
                        If Not IsDBNull(item("INCORPORATION_NUMBER")) Then
                            NewEntity.INCORPORATION_NUMBER = item("INCORPORATION_NUMBER")
                        End If
                        If Not IsDBNull(item("INCORPORATION_STATE")) Then
                            NewEntity.INCORPORATION_STATE = item("INCORPORATION_STATE")
                        End If
                        If Not IsDBNull(item("LastUpdateBy")) Then
                            NewEntity.LastUpdateBy = item("LastUpdateBy")
                        End If
                        If Not IsDBNull(item("NAME")) Then
                            NewEntity.NAME = item("NAME")
                        End If
                        If Not IsDBNull(item("PK_SIPESATGOAML_ACT_ENTITY_ID")) Then
                            NewEntity.PK_SIPESATGOAML_ACT_ENTITY_ID = item("PK_SIPESATGOAML_ACT_ENTITY_ID")
                        End If
                        If Not IsDBNull(item("TAX_NUMBER")) Then
                            NewEntity.TAX_NUMBER = item("TAX_NUMBER")
                        End If
                        If Not IsDBNull(item("TAX_REGISTRATION_NUMBER")) Then
                            NewEntity.TAX_REGISTRATION_NUMBER = item("TAX_REGISTRATION_NUMBER")
                        End If
                        If Not IsDBNull(item("URL")) Then
                            NewEntity.URL = item("URL")
                        End If

                        ListNewEntity.Add(NewEntity)
                    Next

                    Dim listEntity = ListNewEntity
                    If listEntity IsNot Nothing Then .list_goAML_Act_Entity.AddRange(listEntity)


                    If .list_goAML_Act_Entity IsNot Nothing AndAlso .list_goAML_Act_Entity.Count > 0 Then
                        For Each item In .list_goAML_Act_Entity
                            'Address
                            Dim objEntityAddressDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Entity_ADDRESS where FK_ACT_Entity_ID = " & item.PK_SIPESATGOAML_ACT_ENTITY_ID, Nothing)
                            Dim ListNewEntityAddress As New List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS)
                            For Each itemAddress As DataRow In objEntityAddressDataTable.Rows
                                Dim NewEntityAddress As New SIPESATGOAML_ACT_ENTITY_ADDRESS
                                If Not IsDBNull(itemAddress("ADDRESS")) Then
                                    NewEntityAddress.ADDRESS = itemAddress("ADDRESS")
                                End If
                                If Not IsDBNull(itemAddress("ADDRESS_TYPE")) Then
                                    NewEntityAddress.ADDRESS_TYPE = itemAddress("ADDRESS_TYPE")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedBy")) Then
                                    NewEntityAddress.ApprovedBy = itemAddress("ApprovedBy")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedDate")) Then
                                    NewEntityAddress.ApprovedDate = itemAddress("ApprovedDate")
                                End If
                                If Not IsDBNull(itemAddress("CITY")) Then
                                    NewEntityAddress.CITY = itemAddress("CITY")
                                End If
                                If Not IsDBNull(itemAddress("COMMENTS")) Then
                                    NewEntityAddress.COMMENTS = itemAddress("COMMENTS")
                                End If
                                If Not IsDBNull(itemAddress("COUNTRY_CODE")) Then
                                    NewEntityAddress.COUNTRY_CODE = itemAddress("COUNTRY_CODE")
                                End If
                                If Not IsDBNull(itemAddress("CreatedBy")) Then
                                    NewEntityAddress.CreatedBy = itemAddress("CreatedBy")
                                End If
                                If Not IsDBNull(itemAddress("CreatedDate")) Then
                                    NewEntityAddress.CreatedDate = itemAddress("CreatedDate")
                                End If
                                If Not IsDBNull(itemAddress("FK_ACT_ENTITY_ID")) Then
                                    NewEntityAddress.FK_ACT_ENTITY_ID = itemAddress("FK_ACT_ENTITY_ID")
                                End If
                                If Not IsDBNull(itemAddress("FK_REPORT_ID")) Then
                                    NewEntityAddress.FK_REPORT_ID = itemAddress("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemAddress("PK_SIPESATGOAML_ACT_Entity_ADDRESS_ID")) Then
                                    NewEntityAddress.PK_SIPESATGOAML_ACT_ENTITY_Address_ID = itemAddress("PK_SIPESATGOAML_ACT_Entity_ADDRESS_ID")
                                End If
                                If Not IsDBNull(itemAddress("STATE")) Then
                                    NewEntityAddress.STATE = itemAddress("STATE")
                                End If
                                If Not IsDBNull(itemAddress("TOWN")) Then
                                    NewEntityAddress.TOWN = itemAddress("TOWN")
                                End If
                                If Not IsDBNull(itemAddress("ZIP")) Then
                                    NewEntityAddress.ZIP = itemAddress("ZIP")
                                End If

                                ListNewEntityAddress.Add(NewEntityAddress)
                            Next
                            Dim listAddress = ListNewEntityAddress

                            'Phone
                            Dim objEntityPhoneDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Entity_Phone where FK_ACT_Entity_ID = " & item.PK_SIPESATGOAML_ACT_ENTITY_ID, Nothing)
                            Dim ListNewEntityPhone As New List(Of SIPESATGOAML_ACT_ENTITY_PHONE)
                            For Each itemPhone As DataRow In objEntityPhoneDataTable.Rows
                                Dim NewEntityPhone As New SIPESATGOAML_ACT_ENTITY_PHONE
                                If Not IsDBNull(itemPhone("ApprovedDate")) Then
                                    NewEntityPhone.ApprovedDate = itemPhone("ApprovedDate")
                                End If
                                If Not IsDBNull(itemPhone("CreatedBy")) Then
                                    NewEntityPhone.CreatedBy = itemPhone("CreatedBy")
                                End If
                                If Not IsDBNull(itemPhone("COMMENTS")) Then
                                    NewEntityPhone.COMMENTS = itemPhone("COMMENTS")
                                End If
                                If Not IsDBNull(itemPhone("CreatedDate")) Then
                                    NewEntityPhone.CreatedDate = itemPhone("CreatedDate")
                                End If
                                If Not IsDBNull(itemPhone("FK_ACT_ENTITY_ID")) Then
                                    NewEntityPhone.FK_ACT_ENTITY_ID = itemPhone("FK_ACT_ENTITY_ID")
                                End If
                                If Not IsDBNull(itemPhone("FK_REPORT_ID")) Then
                                    NewEntityPhone.FK_REPORT_ID = itemPhone("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemPhone("PK_SIPESATGOAML_ACT_ENTITY_PHONE")) Then
                                    NewEntityPhone.PK_SIPESATGOAML_ACT_ENTITY_PHONE = itemPhone("PK_SIPESATGOAML_ACT_ENTITY_PHONE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_COMMUNICATION_TYPE")) Then
                                    NewEntityPhone.TPH_COMMUNICATION_TYPE = itemPhone("TPH_COMMUNICATION_TYPE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_CONTACT_TYPE")) Then
                                    NewEntityPhone.TPH_CONTACT_TYPE = itemPhone("TPH_CONTACT_TYPE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_COUNTRY_PREFIX")) Then
                                    NewEntityPhone.TPH_COUNTRY_PREFIX = itemPhone("TPH_COUNTRY_PREFIX")
                                End If
                                If Not IsDBNull(itemPhone("TPH_EXTENSION")) Then
                                    NewEntityPhone.TPH_EXTENSION = itemPhone("TPH_EXTENSION")
                                End If
                                If Not IsDBNull(itemPhone("TPH_NUMBER")) Then
                                    NewEntityPhone.TPH_NUMBER = itemPhone("TPH_NUMBER")
                                End If

                                ListNewEntityPhone.Add(NewEntityPhone)
                            Next
                            Dim listPhone = ListNewEntityPhone

                            'Relationship
                            Dim objEntityRelationshipDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Entity_Relationship where FK_ACT_Entity_ID = " & item.PK_SIPESATGOAML_ACT_ENTITY_ID, Nothing)
                            Dim ListNewEntityRelationship As New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
                            For Each itemRelationship As DataRow In objEntityRelationshipDataTable.Rows
                                Dim NewEntityRelationship As New SIPESATGOAML_ACT_ENTITY_RELATIONSHIP
                                If Not IsDBNull(itemRelationship("ApprovedDate")) Then
                                    NewEntityRelationship.ApprovedDate = itemRelationship("ApprovedDate")
                                End If
                                If Not IsDBNull(itemRelationship("COMMENTS")) Then
                                    NewEntityRelationship.COMMENTS = itemRelationship("COMMENTS")
                                End If
                                If Not IsDBNull(itemRelationship("CLIENT_NUMBER")) Then
                                    NewEntityRelationship.CLIENT_NUMBER = itemRelationship("CLIENT_NUMBER")
                                End If
                                If Not IsDBNull(itemRelationship("CreatedBy")) Then
                                    NewEntityRelationship.CreatedBy = itemRelationship("CreatedBy")
                                End If
                                If Not IsDBNull(itemRelationship("CreatedDate")) Then
                                    NewEntityRelationship.CreatedDate = itemRelationship("CreatedDate")
                                End If
                                If Not IsDBNull(itemRelationship("FK_ACT_Entity_ID")) Then
                                    NewEntityRelationship.FK_ACT_ENTITY_ID = itemRelationship("FK_ACT_Entity_ID")
                                End If
                                If Not IsDBNull(itemRelationship("FK_REPORT_ID")) Then
                                    NewEntityRelationship.FK_REPORT_ID = itemRelationship("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemRelationship("IS_APPROX_FROM_DATE")) Then
                                    NewEntityRelationship.IS_APPROX_FROM_DATE = itemRelationship("IS_APPROX_FROM_DATE")
                                End If
                                If Not IsDBNull(itemRelationship("PK_SIPESATGOAML_ACT_Entity_Relationship_ID")) Then
                                    NewEntityRelationship.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID = itemRelationship("PK_SIPESATGOAML_ACT_Entity_Relationship_ID")
                                End If
                                If Not IsDBNull(itemRelationship("VALID_FROM")) Then
                                    NewEntityRelationship.VALID_FROM = itemRelationship("VALID_FROM")
                                End If
                                If Not IsDBNull(itemRelationship("IS_APPROX_TO_DATE")) Then
                                    NewEntityRelationship.IS_APPROX_TO_DATE = itemRelationship("IS_APPROX_TO_DATE")
                                End If
                                If Not IsDBNull(itemRelationship("VALID_TO")) Then
                                    NewEntityRelationship.VALID_TO = itemRelationship("VALID_TO")
                                End If

                                ListNewEntityRelationship.Add(NewEntityRelationship)
                            Next
                            Dim listRelationship = ListNewEntityRelationship

                            If listAddress IsNot Nothing Then .list_goAML_Act_Entity_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Entity_Phone.AddRange(listPhone)
                            If listRelationship IsNot Nothing Then .list_goAML_Act_Entity_Relationship.AddRange(listRelationship)

                        Next
                    End If

                    'Director
                    Dim objDirectorDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Director where FK_REPORTPARTY_ID = " & objActivity.PK_ACT_REPORTPARTYTYPE_ID, Nothing)
                    Dim ListNewDirector As New List(Of SIPESATGOAML_ACT_DIRECTOR)
                    For Each item As DataRow In objDirectorDataTable.Rows
                        Dim NewDirector As New SIPESATGOAML_ACT_DIRECTOR
                        If Not IsDBNull(item("ALIAS")) Then
                            NewDirector.ALIAS = item("ALIAS")
                        End If
                        If Not IsDBNull(item("ApprovedBy")) Then
                            NewDirector.ApprovedBy = item("ApprovedBy")
                        End If
                        If Not IsDBNull(item("ApprovedDate")) Then
                            NewDirector.ApprovedDate = item("ApprovedDate")
                        End If
                        If Not IsDBNull(item("BIRTHDATE")) Then
                            NewDirector.BIRTHDATE = item("BIRTHDATE")
                        End If
                        If Not IsDBNull(item("BIRTH_PLACE")) Then
                            NewDirector.BIRTH_PLACE = item("BIRTH_PLACE")
                        End If
                        If Not IsDBNull(item("COMMENT")) Then
                            NewDirector.COMMENT = item("COMMENT")
                        End If
                        If Not IsDBNull(item("CreatedBy")) Then
                            NewDirector.CreatedBy = item("CreatedBy")
                        End If
                        If Not IsDBNull(item("CreatedDate")) Then
                            NewDirector.CreatedDate = item("CreatedDate")
                        End If
                        If Not IsDBNull(item("DECEASED_DATE")) Then
                            NewDirector.DECEASED_DATE = item("DECEASED_DATE")
                        End If
                        If Not IsDBNull(item("DECEASED")) Then
                            NewDirector.DECEASED = item("DECEASED")
                        End If
                        If Not IsDBNull(item("EMAIL")) Then
                            NewDirector.EMAIL = item("EMAIL")
                        End If
                        If Not IsDBNull(item("EMAIL2")) Then
                            NewDirector.EMAIL2 = item("EMAIL2")
                        End If
                        If Not IsDBNull(item("EMAIL3")) Then
                            NewDirector.EMAIL3 = item("EMAIL3")
                        End If
                        If Not IsDBNull(item("EMAIL4")) Then
                            NewDirector.EMAIL4 = item("EMAIL4")
                        End If
                        If Not IsDBNull(item("EMAIL5")) Then
                            NewDirector.EMAIL5 = item("EMAIL5")
                        End If
                        If Not IsDBNull(item("EMPLOYER_NAME")) Then
                            NewDirector.EMPLOYER_NAME = item("EMPLOYER_NAME")
                        End If
                        If Not IsDBNull(item("FIRST_NAME")) Then
                            NewDirector.FIRST_NAME = item("FIRST_NAME")
                        End If
                        If Not IsDBNull(item("FK_ACT_ENTITY_ID")) Then
                            NewDirector.FK_ACT_ENTITY_ID = item("FK_ACT_ENTITY_ID")
                        End If
                        If Not IsDBNull(item("FK_REPORTPARTY_ID")) Then
                            NewDirector.FK_REPORTPARTY_ID = item("FK_REPORTPARTY_ID")
                        End If
                        If Not IsDBNull(item("FK_REPORT_ID")) Then
                            NewDirector.FK_REPORT_ID = item("FK_REPORT_ID")
                        End If
                        If Not IsDBNull(item("FK_SENDER_INFORMATION")) Then
                            NewDirector.FK_SENDER_INFORMATION = item("FK_SENDER_INFORMATION")
                        End If
                        If Not IsDBNull(item("GENDER")) Then
                            NewDirector.GENDER = item("GENDER")
                        End If
                        If Not IsDBNull(item("ID_NUMBER")) Then
                            NewDirector.ID_NUMBER = item("ID_NUMBER")
                        End If
                        If Not IsDBNull(item("LastUpdateBy")) Then
                            NewDirector.LastUpdateBy = item("LastUpdateBy")
                        End If
                        If Not IsDBNull(item("LastUpdateDate")) Then
                            NewDirector.LastUpdateDate = item("LastUpdateDate")
                        End If
                        If Not IsDBNull(item("LAST_NAME")) Then
                            NewDirector.LAST_NAME = item("LAST_NAME")
                        End If
                        If Not IsDBNull(item("MIDDLE_NAME")) Then
                            NewDirector.MIDDLE_NAME = item("MIDDLE_NAME")
                        End If
                        If Not IsDBNull(item("MOTHERS_NAME")) Then
                            NewDirector.MOTHERS_NAME = item("MOTHERS_NAME")
                        End If
                        If Not IsDBNull(item("NATIONALITY1")) Then
                            NewDirector.NATIONALITY1 = item("NATIONALITY1")
                        End If
                        If Not IsDBNull(item("NATIONALITY2")) Then
                            NewDirector.NATIONALITY2 = item("NATIONALITY2")
                        End If
                        If Not IsDBNull(item("NATIONALITY3")) Then
                            NewDirector.NATIONALITY3 = item("NATIONALITY3")
                        End If
                        If Not IsDBNull(item("OCCUPATION")) Then
                            NewDirector.OCCUPATION = item("OCCUPATION")
                        End If
                        If Not IsDBNull(item("PASSPORT_COUNTRY")) Then
                            NewDirector.PASSPORT_COUNTRY = item("PASSPORT_COUNTRY")
                        End If
                        If Not IsDBNull(item("PASSPORT_NUMBER")) Then
                            NewDirector.PASSPORT_NUMBER = item("PASSPORT_NUMBER")
                        End If
                        If Not IsDBNull(item("PK_SIPESATGOAML_ACT_DIRECTOR_ID")) Then
                            NewDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID = item("PK_SIPESATGOAML_ACT_DIRECTOR_ID")
                        End If
                        If Not IsDBNull(item("PREFIX")) Then
                            NewDirector.PREFIX = item("PREFIX")
                        End If
                        If Not IsDBNull(item("RESIDENCE")) Then
                            NewDirector.RESIDENCE = item("RESIDENCE")
                        End If
                        If Not IsDBNull(item("ROLE")) Then
                            NewDirector.ROLE = item("ROLE")
                        End If
                        If Not IsDBNull(item("SOURCE_OF_WEALTH")) Then
                            NewDirector.SOURCE_OF_WEALTH = item("SOURCE_OF_WEALTH")
                        End If
                        If Not IsDBNull(item("SSN")) Then
                            NewDirector.SSN = item("SSN")
                        End If
                        If Not IsDBNull(item("TAX_NUMBER")) Then
                            NewDirector.TAX_NUMBER = item("TAX_NUMBER")
                        End If
                        If Not IsDBNull(item("TAX_REG_NUMBER")) Then
                            NewDirector.TAX_REG_NUMBER = item("TAX_REG_NUMBER")
                        End If
                        If Not IsDBNull(item("TITLE")) Then
                            NewDirector.TITLE = item("TITLE")
                        End If

                        ListNewDirector.Add(NewDirector)
                    Next

                    Dim listDirector = ListNewDirector
                    If listDirector IsNot Nothing Then .list_goAML_Act_Director.AddRange(listDirector)


                    If .list_goAML_Act_Director IsNot Nothing AndAlso .list_goAML_Act_Director.Count > 0 Then
                        For Each item In .list_goAML_Act_Director
                            'Address
                            Dim objDirectorAddressDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Director_ADDRESS where FK_ACT_Director_ID = " & item.PK_SIPESATGOAML_ACT_DIRECTOR_ID, Nothing)
                            Dim ListNewDirectorAddress As New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                            For Each itemAddress As DataRow In objDirectorAddressDataTable.Rows
                                Dim NewDirectorAddress As New SIPESATGOAML_ACT_DIRECTOR_ADDRESS
                                If Not IsDBNull(itemAddress("ADDRESS")) Then
                                    NewDirectorAddress.ADDRESS = itemAddress("ADDRESS")
                                End If
                                If Not IsDBNull(itemAddress("ADDRESS_TYPE")) Then
                                    NewDirectorAddress.ADDRESS_Type = itemAddress("ADDRESS_TYPE")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedBy")) Then
                                    NewDirectorAddress.ApprovedBy = itemAddress("ApprovedBy")
                                End If
                                If Not IsDBNull(itemAddress("ApprovedDate")) Then
                                    NewDirectorAddress.ApprovedDate = itemAddress("ApprovedDate")
                                End If
                                If Not IsDBNull(itemAddress("CITY")) Then
                                    NewDirectorAddress.CITY = itemAddress("CITY")
                                End If
                                If Not IsDBNull(itemAddress("COMMENTS")) Then
                                    NewDirectorAddress.COMMENTS = itemAddress("COMMENTS")
                                End If
                                If Not IsDBNull(itemAddress("COUNTRY_CODE")) Then
                                    NewDirectorAddress.COUNTRY_CODE = itemAddress("COUNTRY_CODE")
                                End If
                                If Not IsDBNull(itemAddress("CreatedBy")) Then
                                    NewDirectorAddress.CreatedBy = itemAddress("CreatedBy")
                                End If
                                If Not IsDBNull(itemAddress("CreatedDate")) Then
                                    NewDirectorAddress.CreatedDate = itemAddress("CreatedDate")
                                End If
                                If Not IsDBNull(itemAddress("FK_ACT_Director_ID")) Then
                                    NewDirectorAddress.FK_ACT_DIRECTOR_ID = itemAddress("FK_ACT_Director_ID")
                                End If
                                If Not IsDBNull(itemAddress("FK_REPORT_ID")) Then
                                    NewDirectorAddress.FK_REPORT_ID = itemAddress("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemAddress("PK_SIPESATGOAML_ACT_Director_ADDRESS_ID")) Then
                                    NewDirectorAddress.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = itemAddress("PK_SIPESATGOAML_ACT_Director_ADDRESS_ID")
                                End If
                                If Not IsDBNull(itemAddress("STATE")) Then
                                    NewDirectorAddress.STATE = itemAddress("STATE")
                                End If
                                If Not IsDBNull(itemAddress("TOWN")) Then
                                    NewDirectorAddress.TOWN = itemAddress("TOWN")
                                End If
                                If Not IsDBNull(itemAddress("ZIP")) Then
                                    NewDirectorAddress.ZIP = itemAddress("ZIP")
                                End If
                                If Not IsDBNull(itemAddress("ISEMPLOYER")) Then
                                    NewDirectorAddress.ISEMPLOYER = itemAddress("ISEMPLOYER")
                                End If

                                ListNewDirectorAddress.Add(NewDirectorAddress)
                            Next
                            Dim listAddress = ListNewDirectorAddress

                            'Phone
                            Dim objDirectorPhoneDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Director_Phone where FK_ACT_Director_ID = " & item.PK_SIPESATGOAML_ACT_DIRECTOR_ID, Nothing)
                            Dim ListNewDirectorPhone As New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
                            For Each itemPhone As DataRow In objDirectorPhoneDataTable.Rows
                                Dim NewDirectorPhone As New SIPESATGOAML_ACT_DIRECTOR_PHONE
                                If Not IsDBNull(itemPhone("ApprovedDate")) Then
                                    NewDirectorPhone.ApprovedDate = itemPhone("ApprovedDate")
                                End If
                                If Not IsDBNull(itemPhone("COMMENTS")) Then
                                    NewDirectorPhone.COMMENTS = itemPhone("COMMENTS")
                                End If
                                If Not IsDBNull(itemPhone("CreatedBy")) Then
                                    NewDirectorPhone.CreatedBy = itemPhone("CreatedBy")
                                End If
                                If Not IsDBNull(itemPhone("CreatedDate")) Then
                                    NewDirectorPhone.CreatedDate = itemPhone("CreatedDate")
                                End If
                                If Not IsDBNull(itemPhone("FK_ACT_Director_ID")) Then
                                    NewDirectorPhone.FK_ACT_DIRECTOR_ID = itemPhone("FK_ACT_Director_ID")
                                End If
                                If Not IsDBNull(itemPhone("FK_REPORT_ID")) Then
                                    NewDirectorPhone.FK_REPORT_ID = itemPhone("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemPhone("PK_SIPESATGOAML_ACT_Director_PHONE")) Then
                                    NewDirectorPhone.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = itemPhone("PK_SIPESATGOAML_ACT_Director_PHONE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_COMMUNICATION_TYPE")) Then
                                    NewDirectorPhone.TPH_COMMUNICATION_TYPE = itemPhone("TPH_COMMUNICATION_TYPE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_CONTACT_TYPE")) Then
                                    NewDirectorPhone.TPH_CONTACT_TYPE = itemPhone("TPH_CONTACT_TYPE")
                                End If
                                If Not IsDBNull(itemPhone("TPH_COUNTRY_PREFIX")) Then
                                    NewDirectorPhone.TPH_COUNTRY_PREFIX = itemPhone("TPH_COUNTRY_PREFIX")
                                End If
                                If Not IsDBNull(itemPhone("TPH_EXTENSION")) Then
                                    NewDirectorPhone.TPH_EXTENSION = itemPhone("TPH_EXTENSION")
                                End If
                                If Not IsDBNull(itemPhone("TPH_NUMBER")) Then
                                    NewDirectorPhone.TPH_NUMBER = itemPhone("TPH_NUMBER")
                                End If
                                If Not IsDBNull(itemPhone("ISEMPLOYER")) Then
                                    NewDirectorPhone.ISEMPLOYER = itemPhone("ISEMPLOYER")
                                End If

                                ListNewDirectorPhone.Add(NewDirectorPhone)
                            Next
                            Dim listPhone = ListNewDirectorPhone

                            'Identification
                            Dim objDirectorIdentificationDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select * FROM SIPESATGOAML_ACT_Director_Identification where FK_ACT_Director_ID = " & item.PK_SIPESATGOAML_ACT_DIRECTOR_ID, Nothing)
                            Dim ListNewDirectorIdentification As New List(Of SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION)
                            For Each itemIdentification As DataRow In objDirectorIdentificationDataTable.Rows
                                Dim NewDirectorIdentification As New SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION
                                If Not IsDBNull(itemIdentification("ApprovedDate")) Then
                                    NewDirectorIdentification.ApprovedDate = itemIdentification("ApprovedDate")
                                End If
                                If Not IsDBNull(itemIdentification("EXPIRY_DATE")) Then
                                    NewDirectorIdentification.EXPIRY_DATE = itemIdentification("EXPIRY_DATE")
                                End If
                                If Not IsDBNull(itemIdentification("CreatedBy")) Then
                                    NewDirectorIdentification.CreatedBy = itemIdentification("CreatedBy")
                                End If
                                If Not IsDBNull(itemIdentification("CreatedDate")) Then
                                    NewDirectorIdentification.CreatedDate = itemIdentification("CreatedDate")
                                End If
                                If Not IsDBNull(itemIdentification("FK_ACT_Director_ID")) Then
                                    NewDirectorIdentification.FK_ACT_DIRECTOR_ID = itemIdentification("FK_ACT_Director_ID")
                                End If
                                If Not IsDBNull(itemIdentification("FK_REPORT_ID")) Then
                                    NewDirectorIdentification.FK_REPORT_ID = itemIdentification("FK_REPORT_ID")
                                End If
                                If Not IsDBNull(itemIdentification("PK_ACT_DIRECTOR_IDENTIFICATION_ID")) Then
                                    NewDirectorIdentification.PK_ACT_DIRECTOR_IDENTIFICATION_ID = itemIdentification("PK_ACT_DIRECTOR_IDENTIFICATION_ID")
                                End If
                                If Not IsDBNull(itemIdentification("IDENTIFICATION_COMMENT")) Then
                                    NewDirectorIdentification.IDENTIFICATION_COMMENT = itemIdentification("IDENTIFICATION_COMMENT")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_BY")) Then
                                    NewDirectorIdentification.ISSUED_BY = itemIdentification("ISSUED_BY")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_COUNTRY")) Then
                                    NewDirectorIdentification.ISSUED_COUNTRY = itemIdentification("ISSUED_COUNTRY")
                                End If
                                If Not IsDBNull(itemIdentification("ISSUED_DATE")) Then
                                    NewDirectorIdentification.ISSUED_DATE = itemIdentification("ISSUED_DATE")
                                End If
                                If Not IsDBNull(itemIdentification("NUMBER")) Then
                                    NewDirectorIdentification.NUMBER = itemIdentification("NUMBER")
                                End If
                                If Not IsDBNull(itemIdentification("TYPE")) Then
                                    NewDirectorIdentification.TYPE = itemIdentification("TYPE")
                                End If


                                ListNewDirectorIdentification.Add(NewDirectorIdentification)
                            Next
                            Dim listIdentification = ListNewDirectorIdentification

                            If listAddress IsNot Nothing Then .list_goAML_Act_Director_Address.AddRange(listAddress)
                            If listPhone IsNot Nothing Then .list_goAML_Act_Director_Phone.AddRange(listPhone)
                            If listIdentification IsNot Nothing Then .list_goAML_Act_Director_Identification.AddRange(listIdentification)
                        Next
                    End If


                    ''Signatory Address, Phone, Identification
                    'If .list_goAML_Act_acc_Signatory IsNot Nothing AndAlso .list_goAML_Act_acc_Signatory.Count > 0 Then
                    '    For Each item In .list_goAML_Act_acc_Signatory
                    '        Dim listAddress = objDb.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = item.PK_ACT_ACC_SIGNATORY_ID).ToList
                    '        Dim listPhone = objDb.SIPESATGOAML_ACT_ACC_SIGN_PHONE.Where(Function(x) x.FK_ACT_ACC_SIGNATORY_ID = item.PK_ACT_ACC_SIGNATORY_ID).ToList
                    '        Dim listIdentification = objDb.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION.Where(Function(x) x.FK_ACT_ACC_SIGN_ID = item.PK_ACT_ACC_SIGNATORY_ID).ToList

                    '        If listAddress IsNot Nothing Then .list_goAML_Act_Acc_sign_Address.AddRange(listAddress)
                    '        If listPhone IsNot Nothing Then .list_goAML_Act_Acc_sign_Phone.AddRange(listPhone)
                    '        If listIdentification IsNot Nothing Then .list_goAML_Activity_Person_Identification.AddRange(listIdentification)
                    '    Next
                    'End If
                End If
            End With

            Return objGoAMLActivityClass
        End Using
    End Function

    Shared Function getValidationReportByTransactionOrActivityID(strReportID As String, strID As String) As String
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.Int

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_TransactionOrActivity_ID"
            param(1).Value = strID
            param(1).DbType = SqlDbType.Int

            Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGOAML_GetValidationReport_PerActivityID", param)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Sub SaveReportEdit(objModule As NawaDAL.Module, objData As SIPESATGoAML_Report_Class)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Get Old Data for Audit Trail
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_REPORT_ID)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Jalankan save state saat status berubah ke 9 untuk pertama kali
            'Hanya dijalankan jika membutuhkan Approval
            If Not (NawaBLL.Common.SessionCurrentUser.FK_MRole_ID = 1 OrElse Not objModule.IsUseApproval) Then
                If objData_Old.obj_goAML_Report.STATUS <> 9 And objData.obj_goAML_Report.STATUS = 9 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
                ElseIf objData_Old.obj_goAML_Report.STATUS <> 9 And objData.obj_goAML_Report.STATUS = 4 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
                End If
            Else
                If objData_Old.obj_goAML_Report.STATUS <> 9 And objData.obj_goAML_Report.STATUS = 9 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
                ElseIf objData_Old.obj_goAML_Report.STATUS <> 9 And objData.obj_goAML_Report.STATUS = 4 Then
                    SaveStateByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
                End If
            End If

            'Save New Data
            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                        objDB.SaveChanges()

                        objTrans.Commit()
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                'Save Audit Trail
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName, objData.obj_goAML_Report.CreatedBy)
                '18-Mar-2022 untuk action edit, jika status = 1 (Waiting for Generate) artinya checker approved or status = 5 (Need Correction) artinya Reject.
                'Jika tidak maka Approver dikosongkan
                Dim objAuditTrailheader As SIPESATGoAMLDAL.AuditTrailHeader = Nothing
                Dim strUserCreator As String = objData.obj_goAML_Report.LastUpdateBy
                Dim strUserApprover As String = Nothing

                If Not IsNothing(objData.obj_goAML_Report.STATUS) Then
                    Dim intReportStatus = objData.obj_goAML_Report.STATUS
                    'AuditTrailStatus 1 = Affected to Database, 2 = Rejected
                    'ModuleAction 6 = Approval
                    If intReportStatus = 1 Then     'Waiting for Generate (Approved)
                        intAuditTrailStatus = 1
                        intModuleAction = 6
                        strUserApprover = strUserID
                    ElseIf intReportStatus = 5 Then   'Need Correction
                        intAuditTrailStatus = 2
                        intModuleAction = 6
                        strUserApprover = strUserID
                    End If
                End If
                Dim strQueryAudit As String
                ' Audit Trail
                strQueryAudit = "INSERT INTO AuditTrailHeader ( "
                strQueryAudit += "ApproveBy,"
                strQueryAudit += "CreatedBy,"
                strQueryAudit += "CreatedDate,"
                strQueryAudit += "FK_AuditTrailStatus_ID,"
                strQueryAudit += "FK_ModuleAction_ID,"
                strQueryAudit += "ModuleLabel"
                strQueryAudit += " ) VALUES ( "
                strQueryAudit += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                strQueryAudit += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                strQueryAudit += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
                strQueryAudit += " , '" & objModule.ModuleLabel & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAudit, Nothing)

                Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)


                ' Audit SAR Memo
                strQuery = ""
                Dim objectdata As Object
                objectdata = objData.obj_goAML_Report
                Dim objtype As Type = objectdata.GetType
                Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                For Each item As System.Reflection.PropertyInfo In properties
                    Dim objaudittraildetail As New SIPESATGoAMLDAL.AuditTrailDetail
                    If Not item.GetValue(objectdata, Nothing) Is Nothing Then
                        If item.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                            objaudittraildetail.NewValue = item.GetValue(objectdata, Nothing)
                        Else
                            objaudittraildetail.NewValue = ""
                        End If
                    Else
                        objaudittraildetail.NewValue = ""
                    End If
                    strQuery = "INSERT INTO AuditTrailDetail(  "
                    strQuery += "FK_AuditTrailHeader_ID,"
                    strQuery += "FieldName,"
                    strQuery += "OldValue,"
                    strQuery += "NewValue"
                    strQuery += " ) VALUES ( "
                    strQuery += " " & PKAudit & ""
                    strQuery += " ,'" & item.Name & "'"
                    strQuery += " , ''"
                    strQuery += " , '" & objaudittraildetail.NewValue & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Next


                If objData.obj_goAML_Report IsNot Nothing Then
                    With objData.obj_goAML_Report
                        strQuery = ""
                        Dim ObjectdataReport As Object
                        ObjectdataReport = objData.obj_goAML_Report
                        Dim ObjtypeReport As Type = ObjectdataReport.GetType
                        Dim PropertiesReport() As System.Reflection.PropertyInfo = ObjtypeReport.GetProperties
                        For Each itemReport As System.Reflection.PropertyInfo In properties
                            Dim objaudittraildetail As New SIPESATGoAMLDAL.AuditTrailDetail
                            If Not itemReport.GetValue(objectdata, Nothing) Is Nothing Then
                                If itemReport.GetValue(objectdata, Nothing).GetType.ToString <> "System.Byte[]" Then
                                    objaudittraildetail.NewValue = itemReport.GetValue(objectdata, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                            Else
                                objaudittraildetail.NewValue = ""
                            End If
                            strQuery = "INSERT INTO AuditTrailDetail(  "
                            strQuery += "FK_AuditTrailHeader_ID,"
                            strQuery += "FieldName,"
                            strQuery += "OldValue,"
                            strQuery += "NewValue"
                            strQuery += " ) VALUES ( "
                            strQuery += " " & PKAudit & ""
                            strQuery += " ,'" & itemReport.Name & "'"
                            strQuery += " , ''"
                            strQuery += " , '" & objaudittraildetail.NewValue & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Next

                    End With
                End If

                'Update Entity Reference
                objDB.Database.ExecuteSqlCommand("exec usp_SIPESATGoAML_updateEntityReference")

            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Function SaveReportAdd(objModule As NawaDAL.Module, objData As SIPESATGoAML_Report_Class) As String
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Report Started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data (Karena saat save transaction dia metodenya langsung simpan, maka harus dikondisikan logicnya).
            'Jika Old data exists, Secara intAction tetap 1 (Add) tapi secara data dia Modified.
            Dim objData_Old = GetGoAMLReportClassByID(objData.obj_goAML_Report.PK_REPORT_ID)

            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Additional Information for Header
                        With objData.obj_goAML_Report
                            .Active = True
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = DateTime.Now
                            .Alternateby = strAlternateID
                        End With

                        'Save objData
                        If objData_Old Is Nothing OrElse objData_Old.obj_goAML_Report Is Nothing Then
                            objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Added
                        Else
                            If objData_Old.obj_goAML_Report.PK_REPORT_ID = 0 Then
                                objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Added
                            Else
                                objDB.Entry(objData.obj_goAML_Report).State = Entity.EntityState.Modified
                            End If

                        End If

                        Try
                            objDB.SaveChanges()
                        Catch ex As System.Data.Entity.Validation.DbEntityValidationException
                            Dim errorMessages As String = ""
                            For Each eve In ex.EntityValidationErrors
                                errorMessages &= " ( Entity of type " &
                                    eve.Entry.Entity.GetType.Name & " in state " &
                                    eve.Entry.State & " has the following validation errors:"
                                For Each ve In eve.ValidationErrors
                                    errorMessages &= " -> Property " &
                                        ve.PropertyName & " , Error " &
                                        ve.ErrorMessage
                                Next
                                errorMessages &= " ) "
                            Next
                            Throw New ApplicationException(errorMessages)
                        End Try

                        objTrans.Commit()

                        Return objData.obj_goAML_Report.PK_REPORT_ID
                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                        Return Nothing
                    End Try

                    objDB.Database.ExecuteSqlCommand("exec usp_SIPESATGoAML_updateEntityReference")
                End Using

                'Save Audit Trail
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                '18-Mar-2022 untuk action add, hilangkan Approver, tambahkan Creator
                Dim strQueryAudit As String
                ' Audit Trail
                strQueryAudit = "INSERT INTO AuditTrailHeader ( "
                strQueryAudit += "ApproveBy,"
                strQueryAudit += "CreatedBy,"
                strQueryAudit += "CreatedDate,"
                strQueryAudit += "FK_AuditTrailStatus_ID,"
                strQueryAudit += "FK_ModuleAction_ID,"
                strQueryAudit += "ModuleLabel"
                strQueryAudit += " ) VALUES ( "
                strQueryAudit += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                strQueryAudit += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                strQueryAudit += " , '" & NawaBLL.Common.ModuleActionEnum.Insert & "'"
                strQueryAudit += " , '" & objModule.ModuleLabel & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAudit, Nothing)

                Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = PKAudit
                Dim listReport As New List(Of SIPESATGOAML_REPORT)

                listReport.Add(objData.obj_goAML_Report)
                NawaFramework.CreateAuditTrailDetailXML(PKAudit, listReport, Nothing)

            End Using

            'Dim strTRN_OR_ACT As String = getTRNorACT(objData.obj_goAML_Report.REPORT_CODE)
            'Dim strTRN_OR_ACT_Old As String = getTRNorACT(objData_Old.obj_goAML_Report.REPORT_CODE)
            'If strTRN_OR_ACT <> strTRN_OR_ACT_Old Then
            '    If strTRN_OR_ACT = "TRN" Then
            '        DeleteActivityByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
            '    ElseIf strTRN_OR_ACT = "ACT" Then
            '        DeleteTransactionByReportID(objData.obj_goAML_Report.PK_REPORT_ID)
            '    End If
            'End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'goAML Report', 'Save Report Finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Sub SaveStateByReportID(strReportID As String)
        Try
            Dim strQuery As String
            'global param 19 = Restore State GoAML Report when Checker Reject Approval = 1
            Dim strRestoreState As String = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(19)
            If String.IsNullOrWhiteSpace(strRestoreState) Then
                strRestoreState = "0"
            End If

            'Save State if RestoreState feature set to ON
            If strRestoreState = "1" Then
                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Report State for later Restore if Rejected by Checker started')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGoAML_Report_XML_SaveState", param)

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Report State for later Restore if Rejected by Checker finished')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub RestoreStateByReportID(strReportID As String)
        Try


            'global param 19 = Restore State SIPESATGoAML Report when Checker Reject Approval = 1
            Dim strRestoreState As String = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(19)
            If String.IsNullOrWhiteSpace(strRestoreState) Then
                strRestoreState = "0"
            End If

            'Restore State
            If strRestoreState = "1" Then       'Restore State On
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter
                param(0).ParameterName = "@PK_Report_ID"
                param(0).Value = strReportID
                param(0).DbType = SqlDbType.BigInt

                param(1) = New SqlParameter
                param(1).ParameterName = "@UserID"
                param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
                param(1).DbType = SqlDbType.VarChar
                NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGoAML_Report_XML_RestoreState", param)
            End If

            'Hapus GoAML_Report_XML_SavedState by PK Report ID tidak perlu ada lagi soalnya sudah terhandle dari dalam sp usp_SIPESATGoAML_Report_XML_RestoreState
            Dim strQuery As String = "DELETE FROM SIPESATGoAML_Report_XML_SavedState WHERE FK_Report_ID = " & strReportID
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            ''global param 19 = Restore State SIPESATGoAML Report when Checker Reject Approval = 1
            'Dim strRestoreState As String = SIPESATGoAMLBLL.SIPESATGoAML_Global_BLL.getGlobalParameterValueByID(19)
            'If String.IsNullOrWhiteSpace(strRestoreState) Then
            '    strRestoreState = "0"
            'End If

            ''Restore State
            'If strRestoreState = "1" Then       'Restore State On
            '    Dim objDataReport_Old = GetGoAMLReportClassByID(strReportID)
            '    Dim objDataActivity_Old As New List(Of SIPESATGoAML_Activity_Class)
            '    If objDataReport_Old.list_goAML_Act_ReportPartyType.Count > 0 Then
            '        For Each Activity In objDataReport_Old.list_goAML_Act_ReportPartyType
            '            Dim tempObjDataActivity_Old = GetGoAMLActivityClassByID(Activity.PK_ACT_REPORTPARTYTYPE_ID)
            '            objDataActivity_Old.Add(tempObjDataActivity_Old)
            '        Next
            '    End If

            '    Dim param(1) As SqlParameter
            '    param(0) = New SqlParameter
            '    param(0).ParameterName = "@PK_Report_ID"
            '    param(0).Value = strReportID
            '    param(0).DbType = SqlDbType.BigInt

            '    param(1) = New SqlParameter
            '    param(1).ParameterName = "@UserID"
            '    param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            '    param(1).DbType = SqlDbType.VarChar
            '    NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGoAML_Report_XML_RestoreState", param)

            '    Dim objDataReport = GetGoAMLReportClassByID(strReportID)
            '    Dim objDataActivity As New List(Of SIPESATGoAML_Activity_Class)
            '    If objDataReport.list_goAML_Act_ReportPartyType.Count > 0 Then
            '        For Each Activity In objDataReport.list_goAML_Act_ReportPartyType
            '            Dim tempObjDataActivity = GetGoAMLActivityClassByID(Activity.PK_ACT_REPORTPARTYTYPE_ID)
            '            objDataActivity.Add(tempObjDataActivity)
            '        Next
            '    End If
            '    SaveAuditTrialReportAndActivity(objDataReport, objDataReport_Old, objDataActivity, objDataActivity_Old)
            'End If

            ''Hapus GoAML_Report_XML_SavedState by PK Report ID tidak perlu ada lagi soalnya sudah terhandle dari dalam sp usp_SIPESATGoAML_Report_XML_RestoreState
            'Dim strQuery As String = "DELETE FROM SIPESATGoAML_Report_XML_SavedState WHERE FK_Report_ID = " & strReportID
            'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity(objModule As NawaDAL.Module, objData As SIPESATGoAML_Activity_Class, strReportID As String)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            If objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID < 0 Then
                intModuleAction = NawaBLL.Common.ModuleActionEnum.Insert
            End If

            'Get Old Data for Audit Trail
            Dim objData_Old = New SIPESATGoAML_Activity_Class
            If intModuleAction = NawaBLL.Common.ModuleActionEnum.Update Then
                objData_Old = GetGoAMLActivityClassByID(objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID)
            End If

            With objData.obj_goAML_Act_ReportPartyType
                .Alternateby = strAlternateID
            End With
            Dim strQuery2 As String = ""
            If objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID < 0 Then
                strQuery2 = "INSERT INTO SIPESATGOAML_ACT_REPORTPARTYTYPE "
                strQuery2 += "("
                strQuery2 += "FK_Report_ID,"
                strQuery2 += "SIGNIFICANCE,"
                strQuery2 += "REASON,"
                strQuery2 += "COMMENTS,"
                strQuery2 += "SUBNODETYPE,"
                strQuery2 += "Active,"
                strQuery2 += "CreatedBy,"
                strQuery2 += "LastUpdateBy,"
                strQuery2 += "ApprovedBy,"
                strQuery2 += "CreatedDate,"
                strQuery2 += "LastUpdateDate,"
                strQuery2 += "ApprovedDate,"
                strQuery2 += "Alternateby"
                strQuery2 += ") VALUES ( "
                strQuery2 += " '" & objData.obj_goAML_Act_ReportPartyType.FK_Report_ID & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.SIGNIFICANCE & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.REASON & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.COMMENTS & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.SUBNODETYPE & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.Active & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.CreatedBy & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.LastUpdateBy & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.ApprovedBy & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.CreatedDate & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.LastUpdateDate & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.ApprovedDate & "'"
                strQuery2 += " , '" & objData.obj_goAML_Act_ReportPartyType.Alternateby & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

            Else
                strQuery2 = "UPDATE SIPESATGOAML_ACT_REPORTPARTYTYPE "
                strQuery2 += "SET "
                strQuery2 += "FK_Report_ID			= '" & objData.obj_goAML_Act_ReportPartyType.FK_Report_ID & "'"
                strQuery2 += ",SIGNIFICANCE            = '" & objData.obj_goAML_Act_ReportPartyType.SIGNIFICANCE & "'"
                strQuery2 += ",REASON                  = '" & objData.obj_goAML_Act_ReportPartyType.REASON & "'"
                strQuery2 += ",COMMENTS                = '" & objData.obj_goAML_Act_ReportPartyType.COMMENTS & "'"
                strQuery2 += ",SUBNODETYPE             = '" & objData.obj_goAML_Act_ReportPartyType.SUBNODETYPE & "'"
                strQuery2 += ",Active                  = '" & objData.obj_goAML_Act_ReportPartyType.Active & "'"
                strQuery2 += ",CreatedBy               = '" & objData.obj_goAML_Act_ReportPartyType.CreatedBy & "'"
                strQuery2 += ",LastUpdateBy            = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQuery2 += ",ApprovedBy              = '" & objData.obj_goAML_Act_ReportPartyType.ApprovedBy & "'"
                strQuery2 += ",CreatedDate             = '" & objData.obj_goAML_Act_ReportPartyType.CreatedDate & "'"
                strQuery2 += ",LastUpdateDate          = '" & DateTime.Now & "'"
                strQuery2 += ",ApprovedDate            = '" & objData.obj_goAML_Act_ReportPartyType.ApprovedDate & "'"
                strQuery2 += ",Alternateby              = '" & objData.obj_goAML_Act_ReportPartyType.Alternateby & "'"
                strQuery2 += "from SIPESATGOAML_ACT_REPORTPARTYTYPE  where PK_ACT_REPORTPARTYTYPE_ID = " & objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

            End If


            Dim OldPK As Long = 0
            Dim OldPKSub As Long = 0
            Dim OldPKSubSubID As Long = 0
            Dim PKReportType As Long
            If objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID > 0 Then
                PKReportType = objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
            Else
                PKReportType = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_ACT_REPORTPARTYTYPE_ID FROM SIPESATGOAML_ACT_REPORTPARTYTYPE order by PK_ACT_REPORTPARTYTYPE_ID desc", Nothing)

            End If



            '1. Person
            For Each objPerson In objData.list_goAML_Act_Person
                Dim NewPk As Integer = -1
                objPerson.FK_REPORT_ID = strReportID
                objPerson.FK_ACT_REPORTPARTY_ID = objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
                strQuery2 = ""
                If objPerson.PK_SIPESATGOAML_ACT_PERSON_ID < 0 Then
                    strQuery2 = "INSERT INTO SIPESATGOAML_ACT_PERSON "
                    strQuery2 += "("
                    strQuery2 += "FK_ACT_REPORTPARTY_ID"
                    strQuery2 += ",FK_REPORT_ID"
                    strQuery2 += ",GENDER"
                    strQuery2 += ",TITLE"
                    strQuery2 += ",FIRST_NAME"
                    strQuery2 += ",MIDDLE_NAME"
                    strQuery2 += ",PREFIX"
                    strQuery2 += ",LAST_NAME"
                    strQuery2 += ",BIRTHDATE"
                    strQuery2 += ",BITH_PLACE"
                    strQuery2 += ",MOTHERS_NAME"
                    strQuery2 += ",ALIAS"
                    strQuery2 += ",SSN"
                    strQuery2 += ",PASSPORT_NUMBER"
                    strQuery2 += ",PASSPORT_COUNTRY"
                    strQuery2 += ",ID_NUMBER"
                    strQuery2 += ",NATIONALITY1"
                    strQuery2 += ",NATIONALITY2"
                    strQuery2 += ",NATIONALITY3"
                    strQuery2 += ",RESIDENCE"
                    strQuery2 += ",EMAIL"
                    strQuery2 += ",OCCUPATION"
                    strQuery2 += ",EMPLOYER_NAME"
                    strQuery2 += ",DECEASED"
                    strQuery2 += ",DECEASED_DATE"
                    strQuery2 += ",TAX_NUMBER"
                    strQuery2 += ",TAX_REG_NUMBER"
                    strQuery2 += ",SOURCE_OF_WEALTH"
                    strQuery2 += ",COMMENT"
                    strQuery2 += ",EMAIL2"
                    strQuery2 += ",EMAIL3"
                    strQuery2 += ",EMAIL4"
                    strQuery2 += ",EMAIL5"
                    strQuery2 += ",CreatedBy"
                    strQuery2 += ",LastUpdateBy"
                    strQuery2 += ",CreatedDate"
                    strQuery2 += ",LastUpdateDate"
                    strQuery2 += ") VALUES ( "
                    strQuery2 += "  '" & PKReportType & "'"
                    strQuery2 += ", '" & objPerson.FK_REPORT_ID & "'"
                    strQuery2 += ", '" & objPerson.GENDER & "'"
                    strQuery2 += ", '" & objPerson.TITLE & "'"
                    strQuery2 += ", '" & objPerson.FIRST_NAME & "'"
                    strQuery2 += ", '" & objPerson.MIDDLE_NAME & "'"
                    strQuery2 += ", '" & objPerson.PREFIX & "'"
                    strQuery2 += ", '" & objPerson.LAST_NAME & "'"
                    strQuery2 += ", '" & objPerson.BIRTHDATE & "'"
                    strQuery2 += ", '" & objPerson.BITH_PLACE & "'"
                    strQuery2 += ", '" & objPerson.MOTHERS_NAME & "'"
                    strQuery2 += ", '" & objPerson.ALIAS & "'"
                    strQuery2 += ", '" & objPerson.SSN & "'"
                    strQuery2 += ", '" & objPerson.PASSPORT_NUMBER & "'"
                    strQuery2 += ", '" & objPerson.PASSPORT_COUNTRY & "'"
                    strQuery2 += ", '" & objPerson.ID_NUMBER & "'"
                    strQuery2 += ", '" & objPerson.NATIONALITY1 & "'"
                    strQuery2 += ", '" & objPerson.NATIONALITY2 & "'"
                    strQuery2 += ", '" & objPerson.NATIONALITY3 & "'"
                    strQuery2 += ", '" & objPerson.RESIDENCE & "'"
                    strQuery2 += ", '" & objPerson.EMAIL & "'"
                    strQuery2 += ", '" & objPerson.OCCUPATION & "'"
                    strQuery2 += ", '" & objPerson.EMPLOYER_NAME & "'"
                    strQuery2 += ", '" & objPerson.DECEASED & "'"
                    strQuery2 += ", '" & objPerson.DECEASED_DATE & "'"
                    strQuery2 += ", '" & objPerson.TAX_NUMBER & "'"
                    strQuery2 += ", '" & objPerson.TAX_REG_NUMBER & "'"
                    strQuery2 += ", '" & objPerson.SOURCE_OF_WEALTH & "'"
                    strQuery2 += ", '" & objPerson.COMMENT & "'"
                    strQuery2 += ", '" & objPerson.EMAIL2 & "'"
                    strQuery2 += ", '" & objPerson.EMAIL3 & "'"
                    strQuery2 += ", '" & objPerson.EMAIL4 & "'"
                    strQuery2 += ", '" & objPerson.EMAIL5 & "'"
                    strQuery2 += ", '" & objPerson.CreatedBy & "'"
                    strQuery2 += ", '" & objPerson.LastUpdateBy & "'"
                    strQuery2 += ", '" & objPerson.CreatedDate & "'"
                    strQuery2 += ", '" & objPerson.LastUpdateDate & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_SIPESATGOAML_ACT_PERSON_ID FROM SIPESATGOAML_ACT_PERSON order by PK_SIPESATGOAML_ACT_PERSON_ID desc", Nothing)


                Else
                    strQuery2 = "UPDATE SIPESATGOAML_ACT_PERSON "
                    strQuery2 += "SET "
                    strQuery2 += "FK_ACT_REPORTPARTY_ID			 ='" & PKReportType & "'"
                    strQuery2 += ",FK_REPORT_ID                   ='" & objPerson.FK_REPORT_ID & "'"
                    strQuery2 += ",GENDER                         ='" & objPerson.GENDER & "'"
                    strQuery2 += ",TITLE                          ='" & objPerson.TITLE & "'"
                    strQuery2 += ",FIRST_NAME                     ='" & objPerson.FIRST_NAME & "'"
                    strQuery2 += ",MIDDLE_NAME                    ='" & objPerson.MIDDLE_NAME & "'"
                    strQuery2 += ",PREFIX                         ='" & objPerson.PREFIX & "'"
                    strQuery2 += ",LAST_NAME                      ='" & objPerson.LAST_NAME & "'"
                    strQuery2 += ",BIRTHDATE                      ='" & objPerson.BIRTHDATE & "'"
                    strQuery2 += ",BITH_PLACE                     ='" & objPerson.BITH_PLACE & "'"
                    strQuery2 += ",MOTHERS_NAME                   ='" & objPerson.MOTHERS_NAME & "'"
                    strQuery2 += ",ALIAS                          ='" & objPerson.ALIAS & "'"
                    strQuery2 += ",SSN                            ='" & objPerson.SSN & "'"
                    strQuery2 += ",PASSPORT_NUMBER                ='" & objPerson.PASSPORT_NUMBER & "'"
                    strQuery2 += ",PASSPORT_COUNTRY               ='" & objPerson.PASSPORT_COUNTRY & "'"
                    strQuery2 += ",ID_NUMBER                      ='" & objPerson.ID_NUMBER & "'"
                    strQuery2 += ",NATIONALITY1                   ='" & objPerson.NATIONALITY1 & "'"
                    strQuery2 += ",NATIONALITY2                   ='" & objPerson.NATIONALITY2 & "'"
                    strQuery2 += ",NATIONALITY3                   ='" & objPerson.NATIONALITY3 & "'"
                    strQuery2 += ",RESIDENCE                      ='" & objPerson.RESIDENCE & "'"
                    strQuery2 += ",EMAIL                          ='" & objPerson.EMAIL & "'"
                    strQuery2 += ",OCCUPATION                     ='" & objPerson.OCCUPATION & "'"
                    strQuery2 += ",EMPLOYER_NAME                  ='" & objPerson.EMPLOYER_NAME & "'"
                    strQuery2 += ",DECEASED                       ='" & objPerson.DECEASED & "'"
                    strQuery2 += ",DECEASED_DATE                  ='" & objPerson.DECEASED_DATE & "'"
                    strQuery2 += ",TAX_NUMBER                     ='" & objPerson.TAX_NUMBER & "'"
                    strQuery2 += ",TAX_REG_NUMBER                 ='" & objPerson.TAX_REG_NUMBER & "'"
                    strQuery2 += ",SOURCE_OF_WEALTH               ='" & objPerson.SOURCE_OF_WEALTH & "'"
                    strQuery2 += ",COMMENT                        ='" & objPerson.COMMENT & "'"
                    strQuery2 += ",EMAIL2                         ='" & objPerson.EMAIL2 & "'"
                    strQuery2 += ",EMAIL3                         ='" & objPerson.EMAIL3 & "'"
                    strQuery2 += ",EMAIL4                         ='" & objPerson.EMAIL4 & "'"
                    strQuery2 += ",EMAIL5                         ='" & objPerson.EMAIL5 & "'"
                    strQuery2 += ",CreatedBy                      ='" & objPerson.CreatedBy & "'"
                    strQuery2 += ",LastUpdateBy                   ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery2 += ",CreatedDate                    ='" & objPerson.CreatedDate & "'"
                    strQuery2 += ",LastUpdateDate                 ='" & DateTime.Now & "'"
                    strQuery2 += "from SIPESATGOAML_ACT_PERSON  where PK_SIPESATGOAML_ACT_PERSON_ID = " & objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                    NewPk = OldPK
                End If


                'Person Address
                Dim listPersonAddress = objData.list_goAML_Act_Person_Address.Where(Function(x) x.FK_ACT_PERSON = NewPk).ToList
                If listPersonAddress IsNot Nothing Then
                    For Each itemAddress In listPersonAddress
                        itemAddress.FK_REPORT_ID = strReportID
                        itemAddress.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        strQuery2 = ""
                        If itemAddress.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_PERSON_ADDRESS "
                            strQuery2 += "("
                            strQuery2 += "FK_REPORT_ID"
                            strQuery2 += ",FK_ACT_PERSON"
                            strQuery2 += ",ADDRESS_TYPE"
                            strQuery2 += ",ADDRESS"
                            strQuery2 += ",TOWN"
                            strQuery2 += ",CITY"
                            strQuery2 += ",ZIP"
                            strQuery2 += ",COUNTRY_CODE"
                            strQuery2 += ",STATE"
                            strQuery2 += ",COMMENTS"
                            strQuery2 += ",ISEMPLOYER"
                            strQuery2 += ",CreatedBy"
                            strQuery2 += ",LastUpdateBy"
                            strQuery2 += ",CreatedDate"
                            strQuery2 += ",LastUpdateDate"
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & OldPK & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS_TYPE & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS & "'"
                            strQuery2 += ", '" & itemAddress.TOWN & "'"
                            strQuery2 += ", '" & itemAddress.CITY & "'"
                            strQuery2 += ", '" & itemAddress.ZIP & "'"
                            strQuery2 += ", '" & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", '" & itemAddress.STATE & "'"
                            strQuery2 += ", '" & itemAddress.COMMENTS & "'"
                            strQuery2 += ", '" & itemAddress.ISEMPLOYER & "'"
                            strQuery2 += ", '" & itemAddress.CreatedBy & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemAddress.CreatedDate & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_PERSON_ADDRESS "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_REPORT_ID		 = ' " & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", FK_ACT_PERSON                 = ' " & OldPK & "'"
                            strQuery2 += ", ADDRESS_TYPE                  = ' " & itemAddress.ADDRESS_TYPE & "'"
                            strQuery2 += ", ADDRESS                       = ' " & itemAddress.ADDRESS & "'"
                            strQuery2 += ", TOWN                          = ' " & itemAddress.TOWN & "'"
                            strQuery2 += ", CITY                          = ' " & itemAddress.CITY & "'"
                            strQuery2 += ", ZIP                           = ' " & itemAddress.ZIP & "'"
                            strQuery2 += ", COUNTRY_CODE                  = ' " & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", STATE                         = ' " & itemAddress.STATE & "'"
                            strQuery2 += ", COMMENTS                      = ' " & itemAddress.COMMENTS & "'"
                            strQuery2 += ", ISEMPLOYER                    = ' " & itemAddress.ISEMPLOYER & "'"
                            strQuery2 += ", CreatedBy                     = ' " & itemAddress.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy                  = ' " & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate                   = ' " & itemAddress.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate                = ' " & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_PERSON_ADDRESS  where PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID = " & itemAddress.PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If

                'Person Signatory Phone
                Dim listPhone = objData.list_goAML_Act_Person_Phone.Where(Function(x) x.FK_ACT_PERSON = NewPk).ToList
                If listPhone IsNot Nothing Then
                    For Each itemPhone In listPhone
                        itemPhone.FK_REPORT_ID = strReportID
                        itemPhone.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        strQuery2 = ""
                        If itemPhone.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_PERSON_PHONE "
                            strQuery2 += "("
                            strQuery2 += "FK_ACT_PERSON"
                            strQuery2 += ",TPH_CONTACT_TYPE"
                            strQuery2 += ",TPH_COMMUNICATION_TYPE"
                            strQuery2 += ",TPH_COUNTRY_PREFIX"
                            strQuery2 += ",TPH_NUMBER"
                            strQuery2 += ",TPH_EXTENSION"
                            strQuery2 += ",COMMENTS"
                            strQuery2 += ",ISEMPLOYER"
                            strQuery2 += ",FK_REPORT_ID"
                            strQuery2 += ",CreatedBy"
                            strQuery2 += ",LastUpdateBy"
                            strQuery2 += ",CreatedDate"
                            strQuery2 += ",LastUpdateDate"
                            strQuery2 += ") VALUES ( "
                            strQuery2 += " '" & OldPK & "'"
                            strQuery2 += ", '" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", '" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", '" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", '" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", '" & itemPhone.ISEMPLOYER & "'"
                            strQuery2 += ", '" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_PERSON_PHONE "
                            strQuery2 += "SET "
                            strQuery2 += " FK_ACT_PERSON='" & OldPK & "'"
                            strQuery2 += ", TPH_CONTACT_TYPE='" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", TPH_COMMUNICATION_TYPE='" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", TPH_COUNTRY_PREFIX='" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", TPH_NUMBER='" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", TPH_EXTENSION='" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", COMMENTS='" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", ISEMPLOYER='" & itemPhone.ISEMPLOYER & "'"
                            strQuery2 += ", FK_REPORT_ID='" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy='" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy='" & itemPhone.LastUpdateBy & "'"
                            strQuery2 += ", CreatedDate='" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate='" & itemPhone.LastUpdateDate & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_PERSON_PHONE  where PK_SIPESATGOAML_ACT_PERSON_PHONE_ID = " & itemPhone.PK_SIPESATGOAML_ACT_PERSON_PHONE_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        End If

                    Next
                End If

                'Person IDentification
                Dim listIdentification = objData.list_goAML_Activity_Person_Identification.Where(Function(x) x.FK_ACT_PERSON = NewPk).ToList
                If listIdentification IsNot Nothing Then
                    For Each objIdentification In listIdentification
                        objIdentification.FK_REPORT_ID = strReportID
                        objIdentification.FK_ACT_PERSON = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        strQuery2 = ""
                        If objIdentification.PK_ACTIVITY_PERSON_IDENTIFICATION_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_ACC_SIGN_ID 			"
                            strQuery2 += ", FK_ACT_ENTITY_ID             "
                            strQuery2 += ", FK_ACT_PERSON                "
                            strQuery2 += ", TYPE                         "
                            strQuery2 += ", NUMBER                       "
                            strQuery2 += ", ISSUED_DATE                  "
                            strQuery2 += ", EXPIRY_DATE                  "
                            strQuery2 += ", ISSUED_BY                    "
                            strQuery2 += ", ISSUED_COUNTRY               "
                            strQuery2 += ", IDENTIFICATION_COMMENT       "
                            strQuery2 += ", FK_REPORT_ID                 "
                            strQuery2 += ", CreatedBy                    "
                            strQuery2 += ", LastUpdateBy                 "
                            strQuery2 += ", CreatedDate                  "
                            strQuery2 += ", LastUpdateDate               "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += " '" & objIdentification.FK_ACT_ACC_SIGN_ID & "'"
                            strQuery2 += ", '" & objIdentification.FK_ACT_ENTITY_ID & "'"
                            strQuery2 += ", '" & OldPK & "'"
                            strQuery2 += ", '" & objIdentification.TYPE & "'"
                            strQuery2 += ", '" & objIdentification.NUMBER & "'"
                            strQuery2 += ", '" & objIdentification.ISSUED_DATE & "'"
                            strQuery2 += ", '" & objIdentification.EXPIRY_DATE & "'"
                            strQuery2 += ", '" & objIdentification.ISSUED_BY & "'"
                            strQuery2 += ", '" & objIdentification.ISSUED_COUNTRY & "'"
                            strQuery2 += ", '" & objIdentification.IDENTIFICATION_COMMENT & "'"
                            strQuery2 += ", '" & objIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & objIdentification.CreatedBy & "'"
                            strQuery2 += ", '" & objIdentification.LastUpdateBy & "'"
                            strQuery2 += ", '" & objIdentification.CreatedDate & "'"
                            strQuery2 += ", '" & objIdentification.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_ACC_SIGN_ID 			='" & objIdentification.FK_ACT_ACC_SIGN_ID & "'"
                            strQuery2 += ", FK_ACT_ENTITY_ID             ='" & objIdentification.FK_ACT_ENTITY_ID & "'"
                            strQuery2 += ", FK_ACT_PERSON                ='" & OldPK & "'"
                            strQuery2 += ", TYPE                         ='" & objIdentification.TYPE & "'"
                            strQuery2 += ", NUMBER                       ='" & objIdentification.NUMBER & "'"
                            strQuery2 += ", ISSUED_DATE                  ='" & objIdentification.ISSUED_DATE & "'"
                            strQuery2 += ", EXPIRY_DATE                  ='" & objIdentification.EXPIRY_DATE & "'"
                            strQuery2 += ", ISSUED_BY                    ='" & objIdentification.ISSUED_BY & "'"
                            strQuery2 += ", ISSUED_COUNTRY               ='" & objIdentification.ISSUED_COUNTRY & "'"
                            strQuery2 += ", IDENTIFICATION_COMMENT       ='" & objIdentification.IDENTIFICATION_COMMENT & "'"
                            strQuery2 += ", FK_REPORT_ID                 ='" & objIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                    ='" & objIdentification.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy                 ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate                  ='" & objIdentification.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate               ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION  where PK_ACTIVITY_PERSON_IDENTIFICATION_ID = " & objIdentification.PK_ACTIVITY_PERSON_IDENTIFICATION_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If

                'Person Signatory Relationship
                Dim listRelationship = objData.list_goAML_Act_Person_Relationship.Where(Function(x) x.FK_ACT_PERSON_ID = NewPk).ToList
                If listRelationship IsNot Nothing Then
                    For Each objRelationship In listRelationship
                        objRelationship.FK_REPORT_ID = strReportID
                        objRelationship.FK_ACT_PERSON_ID = objPerson.PK_SIPESATGOAML_ACT_PERSON_ID
                        strQuery2 = ""
                        If objRelationship.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_PERSON_RELATIONSHIP "
                            strQuery2 += "("
                            strQuery2 += " FK_ACT_PERSON_ID			 "
                            strQuery2 += ", CLIENT_NUMBER             "
                            strQuery2 += ", VALID_FROM                "
                            strQuery2 += ", IS_APPROX_FROM_DATE       "
                            strQuery2 += ", VALID_TO                  "
                            strQuery2 += ", IS_APPROX_TO_DATE         "
                            strQuery2 += ", COMMENTS                  "
                            strQuery2 += ", FK_REPORT_ID              "
                            strQuery2 += ", CreatedBy                 "
                            strQuery2 += ", LastUpdateBy              "
                            strQuery2 += ", CreatedDate               "
                            strQuery2 += ", LastUpdateDate            "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & objRelationship.CLIENT_NUMBER & "'"
                            strQuery2 += ", '" & objRelationship.VALID_FROM & "'"
                            strQuery2 += ", '" & objRelationship.IS_APPROX_FROM_DATE & "'"
                            strQuery2 += ", '" & objRelationship.VALID_TO & "'"
                            strQuery2 += ", '" & objRelationship.IS_APPROX_TO_DATE & "'"
                            strQuery2 += ", '" & objRelationship.COMMENTS & "'"
                            strQuery2 += ", '" & objRelationship.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & objRelationship.CreatedBy & "'"
                            strQuery2 += ", '" & objRelationship.LastUpdateBy & "'"
                            strQuery2 += ", '" & objRelationship.CreatedDate & "'"
                            strQuery2 += ", '" & objRelationship.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_PERSON_RELATIONSHIP "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_PERSON_ID			 ='" & OldPK & "'"
                            strQuery2 += ", CLIENT_NUMBER             ='" & objRelationship.CLIENT_NUMBER & "'"
                            strQuery2 += ", VALID_FROM                ='" & objRelationship.VALID_FROM & "'"
                            strQuery2 += ", IS_APPROX_FROM_DATE       ='" & objRelationship.IS_APPROX_FROM_DATE & "'"
                            strQuery2 += ", VALID_TO                  ='" & objRelationship.VALID_TO & "'"
                            strQuery2 += ", IS_APPROX_TO_DATE         ='" & objRelationship.IS_APPROX_TO_DATE & "'"
                            strQuery2 += ", COMMENTS                  ='" & objRelationship.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID              ='" & objRelationship.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                 ='" & objRelationship.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy              ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate               ='" & objRelationship.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate            ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_PERSON_RELATIONSHIP  where PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID = " & objRelationship.PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If
            Next

            Dim PKEntity As Integer
            ''2. Entity
            For Each objEntity In objData.list_goAML_Act_Entity
                Dim NewPk As Integer = -1
                objEntity.FK_REPORT_ID = strReportID
                objEntity.FK_ACT_REPORTPARTY_ID = objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
                strQuery2 = ""
                If objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID < 0 Then
                    strQuery2 = "INSERT INTO SIPESATGOAML_ACT_ENTITY "
                    strQuery2 += "("
                    strQuery2 += "  FK_ACT_REPORTPARTY_ID			"
                    strQuery2 += ", FK_REPORT_ID                     "
                    strQuery2 += ", NAME                             "
                    strQuery2 += ", COMMERCIAL_NAME                  "
                    strQuery2 += ", INCORPORATION_LEGAL_FORM         "
                    strQuery2 += ", INCORPORATION_NUMBER             "
                    strQuery2 += ", BUSINESS                         "
                    strQuery2 += ", EMAIL                            "
                    strQuery2 += ", URL                              "
                    strQuery2 += ", INCORPORATION_STATE              "
                    strQuery2 += ", INCORPORATION_COUNTRY_CODE       "
                    strQuery2 += ", DIRECTOR_ID                      "
                    strQuery2 += ", INCORPORATION_DATE               "
                    strQuery2 += ", BUSINESS_CLOSED                  "
                    strQuery2 += ", DATE_BUSINESS_CLOSED             "
                    strQuery2 += ", TAX_NUMBER                       "
                    strQuery2 += ", TAX_REGISTRATION_NUMBER          "
                    strQuery2 += ", COMMENTS                         "
                    strQuery2 += ", CreatedBy                        "
                    strQuery2 += ", LastUpdateBy                     "
                    strQuery2 += ", CreatedDate                      "
                    strQuery2 += ", LastUpdateDate                   "
                    strQuery2 += ") VALUES ( "
                    strQuery2 += "  '" & PKReportType & "'"
                    strQuery2 += ", '" & objEntity.FK_REPORT_ID & "'"
                    strQuery2 += ", '" & objEntity.NAME & "'"
                    strQuery2 += ", '" & objEntity.COMMERCIAL_NAME & "'"
                    strQuery2 += ", '" & objEntity.INCORPORATION_LEGAL_FORM & "'"
                    strQuery2 += ", '" & objEntity.INCORPORATION_NUMBER & "'"
                    strQuery2 += ", '" & objEntity.BUSINESS & "'"
                    strQuery2 += ", '" & objEntity.EMAIL & "'"
                    strQuery2 += ", '" & objEntity.URL & "'"
                    strQuery2 += ", '" & objEntity.INCORPORATION_STATE & "'"
                    strQuery2 += ", '" & objEntity.INCORPORATION_COUNTRY_CODE & "'"
                    strQuery2 += ", '" & objEntity.DIRECTOR_ID & "'"
                    strQuery2 += ", '" & objEntity.INCORPORATION_DATE & "'"
                    strQuery2 += ", '" & objEntity.BUSINESS_CLOSED & "'"
                    strQuery2 += ", '" & objEntity.DATE_BUSINESS_CLOSED & "'"
                    strQuery2 += ", '" & objEntity.TAX_NUMBER & "'"
                    strQuery2 += ", '" & objEntity.TAX_REGISTRATION_NUMBER & "'"
                    strQuery2 += ", '" & objEntity.COMMENTS & "'"
                    strQuery2 += ", '" & objEntity.CreatedBy & "'"
                    strQuery2 += ", '" & objEntity.LastUpdateBy & "'"
                    strQuery2 += ", '" & objEntity.CreatedDate & "'"
                    strQuery2 += ", '" & objEntity.LastUpdateDate & "')"
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_SIPESATGOAML_ACT_ENTITY_ID FROM SIPESATGOAML_ACT_ENTITY order by PK_SIPESATGOAML_ACT_ENTITY_ID desc", Nothing)


                Else
                    strQuery2 = "UPDATE SIPESATGOAML_ACT_ENTITY "
                    strQuery2 += "SET "
                    strQuery2 += "  FK_ACT_REPORTPARTY_ID='" & PKReportType & "'"
                    strQuery2 += ", FK_REPORT_ID='" & objEntity.FK_REPORT_ID & "'"
                    strQuery2 += ", NAME='" & objEntity.NAME & "'"
                    strQuery2 += ", COMMERCIAL_NAME='" & objEntity.COMMERCIAL_NAME & "'"
                    strQuery2 += ", INCORPORATION_LEGAL_FORM='" & objEntity.INCORPORATION_LEGAL_FORM & "'"
                    strQuery2 += ", INCORPORATION_NUMBER='" & objEntity.INCORPORATION_NUMBER & "'"
                    strQuery2 += ", BUSINESS='" & objEntity.BUSINESS & "'"
                    strQuery2 += ", EMAIL='" & objEntity.EMAIL & "'"
                    strQuery2 += ", URL='" & objEntity.URL & "'"
                    strQuery2 += ", INCORPORATION_STATE='" & objEntity.INCORPORATION_STATE & "'"
                    strQuery2 += ", INCORPORATION_COUNTRY_CODE='" & objEntity.INCORPORATION_COUNTRY_CODE & "'"
                    strQuery2 += ", DIRECTOR_ID='" & objEntity.DIRECTOR_ID & "'"
                    strQuery2 += ", INCORPORATION_DATE='" & objEntity.INCORPORATION_DATE & "'"
                    strQuery2 += ", BUSINESS_CLOSED='" & objEntity.BUSINESS_CLOSED & "'"
                    strQuery2 += ", DATE_BUSINESS_CLOSED='" & objEntity.DATE_BUSINESS_CLOSED & "'"
                    strQuery2 += ", TAX_NUMBER='" & objEntity.TAX_NUMBER & "'"
                    strQuery2 += ", TAX_REGISTRATION_NUMBER='" & objEntity.TAX_REGISTRATION_NUMBER & "'"
                    strQuery2 += ", COMMENTS='" & objEntity.COMMENTS & "'"
                    strQuery2 += ", CreatedBy='" & objEntity.CreatedBy & "'"
                    strQuery2 += ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery2 += ", CreatedDate='" & objEntity.CreatedDate & "'"
                    strQuery2 += ", LastUpdateDate='" & DateTime.Now & "'"
                    strQuery2 += "from SIPESATGOAML_ACT_ENTITY  where PK_SIPESATGOAML_ACT_ENTITY_ID = " & objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                    NewPk = OldPK
                End If

                PKEntity = OldPK
                'Entity Address
                Dim listEntityAddress = objData.list_goAML_Act_Entity_Address.Where(Function(x) x.FK_ACT_ENTITY_ID = NewPk).ToList
                If listEntityAddress IsNot Nothing Then
                    For Each itemAddress In listEntityAddress
                        itemAddress.FK_REPORT_ID = strReportID
                        itemAddress.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                        strQuery2 = ""
                        If itemAddress.PK_SIPESATGOAML_ACT_ENTITY_Address_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_ENTITY_ADDRESS "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_ENTITY_ID		"
                            strQuery2 += ", ADDRESS_TYPE         "
                            strQuery2 += ", ADDRESS              "
                            strQuery2 += ", TOWN                 "
                            strQuery2 += ", CITY                 "
                            strQuery2 += ", ZIP                  "
                            strQuery2 += ", COUNTRY_CODE         "
                            strQuery2 += ", STATE                "
                            strQuery2 += ", COMMENTS             "
                            strQuery2 += ", FK_REPORT_ID         "
                            strQuery2 += ", CreatedBy			"
                            strQuery2 += ", LastUpdateBy         "
                            strQuery2 += ", CreatedDate          "
                            strQuery2 += ", LastUpdateDate       "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS_TYPE & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS & "'"
                            strQuery2 += ", '" & itemAddress.TOWN & "'"
                            strQuery2 += ", '" & itemAddress.CITY & "'"
                            strQuery2 += ", '" & itemAddress.ZIP & "'"
                            strQuery2 += ", '" & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", '" & itemAddress.STATE & "'"
                            strQuery2 += ", '" & itemAddress.COMMENTS & "'"
                            strQuery2 += ", '" & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemAddress.CreatedBy & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemAddress.CreatedDate & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_ENTITY_ADDRESS "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_ENTITY_ID='" & OldPK & "'"
                            strQuery2 += ", ADDRESS_TYPE ='" & itemAddress.ADDRESS_TYPE & "'"
                            strQuery2 += ", ADDRESS ='" & itemAddress.ADDRESS & "'"
                            strQuery2 += ", TOWN ='" & itemAddress.TOWN & "'"
                            strQuery2 += ", CITY ='" & itemAddress.CITY & "'"
                            strQuery2 += ", ZIP ='" & itemAddress.ZIP & "'"
                            strQuery2 += ", COUNTRY_CODE ='" & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", STATE ='" & itemAddress.STATE & "'"
                            strQuery2 += ", COMMENTS ='" & itemAddress.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID ='" & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy='" & itemAddress.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate='" & itemAddress.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_ENTITY_ADDRESS  where PK_SIPESATGOAML_ACT_ENTITY_Address_ID = " & itemAddress.PK_SIPESATGOAML_ACT_ENTITY_Address_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If

                'Entity Signatory Phone
                Dim listEntityPhone = objData.list_goAML_Act_Entity_Phone.Where(Function(x) x.FK_ACT_ENTITY_ID = NewPk).ToList
                If listEntityPhone IsNot Nothing Then
                    For Each itemPhone In listEntityPhone
                        itemPhone.FK_REPORT_ID = strReportID
                        itemPhone.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                        strQuery2 = ""
                        If itemPhone.PK_SIPESATGOAML_ACT_ENTITY_PHONE < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_ENTITY_PHONE "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_ENTITY_ID 			 "
                            strQuery2 += ", TPH_CONTACT_TYPE              "
                            strQuery2 += ", TPH_COMMUNICATION_TYPE        "
                            strQuery2 += ", TPH_COUNTRY_PREFIX            "
                            strQuery2 += ", TPH_NUMBER                    "
                            strQuery2 += ", TPH_EXTENSION                 "
                            strQuery2 += ", COMMENTS                      "
                            strQuery2 += ", FK_REPORT_ID                  "
                            strQuery2 += ", CreatedBy                     "
                            strQuery2 += ", LastUpdateBy                  "
                            strQuery2 += ", CreatedDate                   "
                            strQuery2 += ", LastUpdateDate                "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", '" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", '" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", '" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", '" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateDate & "')"

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_ENTITY_PHONE "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_ENTITY_ID 			='" & OldPK & "'"
                            strQuery2 += ", TPH_CONTACT_TYPE             ='" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", TPH_COMMUNICATION_TYPE       ='" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", TPH_COUNTRY_PREFIX           ='" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", TPH_NUMBER                   ='" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", TPH_EXTENSION                ='" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", COMMENTS                     ='" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID                 ='" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                    ='" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy                 ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate                  ='" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate               ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_ENTITY_PHONE  where PK_SIPESATGOAML_ACT_ENTITY_PHONE = " & itemPhone.PK_SIPESATGOAML_ACT_ENTITY_PHONE
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        End If

                    Next
                End If

                'Entity Signatory Relationship
                Dim listRelationship = objData.list_goAML_Act_Entity_Relationship.Where(Function(x) x.FK_ACT_ENTITY_ID = NewPk).ToList
                If listRelationship IsNot Nothing Then
                    For Each objIdentification In listRelationship
                        objIdentification.FK_REPORT_ID = strReportID
                        objIdentification.FK_ACT_ENTITY_ID = objEntity.PK_SIPESATGOAML_ACT_ENTITY_ID
                        strQuery2 = ""
                        If objIdentification.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_ENTITY_RELATIONSHIP "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_ENTITY_ID 		"
                            strQuery2 += ", CLIENT_NUMBER            "
                            strQuery2 += ", VALID_FROM               "
                            strQuery2 += ", IS_APPROX_FROM_DATE      "
                            strQuery2 += ", VALID_TO                 "
                            strQuery2 += ", IS_APPROX_TO_DATE        "
                            strQuery2 += ", COMMENTS                 "
                            strQuery2 += ", FK_REPORT_ID             "
                            strQuery2 += ", CreatedBy                "
                            strQuery2 += ", LastUpdateBy             "
                            strQuery2 += ", CreatedDate              "
                            strQuery2 += ", LastUpdateDate           "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & objIdentification.CLIENT_NUMBER & "'"
                            strQuery2 += ", '" & objIdentification.VALID_FROM & "'"
                            strQuery2 += ", '" & objIdentification.IS_APPROX_FROM_DATE & "'"
                            strQuery2 += ", '" & objIdentification.VALID_TO & "'"
                            strQuery2 += ", '" & objIdentification.IS_APPROX_TO_DATE & "'"
                            strQuery2 += ", '" & objIdentification.COMMENTS & "'"
                            strQuery2 += ", '" & objIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & objIdentification.CreatedBy & "'"
                            strQuery2 += ", '" & objIdentification.LastUpdateBy & "'"
                            strQuery2 += ", '" & objIdentification.CreatedDate & "'"
                            strQuery2 += ", '" & objIdentification.LastUpdateDate & "')"

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_ENTITY_RELATIONSHIP "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_ENTITY_ID 		='" & OldPK & "'"
                            strQuery2 += ", CLIENT_NUMBER            ='" & objIdentification.CLIENT_NUMBER & "'"
                            strQuery2 += ", VALID_FROM               ='" & objIdentification.VALID_FROM & "'"
                            strQuery2 += ", IS_APPROX_FROM_DATE      ='" & objIdentification.IS_APPROX_FROM_DATE & "'"
                            strQuery2 += ", VALID_TO                 ='" & objIdentification.VALID_TO & "'"
                            strQuery2 += ", IS_APPROX_TO_DATE        ='" & objIdentification.IS_APPROX_TO_DATE & "'"
                            strQuery2 += ", COMMENTS                 ='" & objIdentification.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID             ='" & objIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                ='" & objIdentification.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy             ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate              ='" & objIdentification.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate           ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_ENTITY_RELATIONSHIP  where PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID = " & objIdentification.PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If
            Next

            '1. Director
            For Each objDirector In objData.list_goAML_Act_Director
                Dim NewPk As Integer = -1
                objDirector.FK_REPORT_ID = strReportID
                objDirector.FK_REPORTPARTY_ID = objData.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
                strQuery2 = ""
                If objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID < 0 Then
                    strQuery2 = "INSERT INTO SIPESATGOAML_ACT_DIRECTOR "
                    strQuery2 += "("
                    strQuery2 += "  FK_REPORTPARTY_ID 			"
                    strQuery2 += ", FK_ACT_ENTITY_ID             "
                    strQuery2 += ", GENDER                       "
                    strQuery2 += ", TITLE                        "
                    strQuery2 += ", FIRST_NAME                   "
                    strQuery2 += ", MIDDLE_NAME                  "
                    strQuery2 += ", PREFIX                       "
                    strQuery2 += ", LAST_NAME                    "
                    strQuery2 += ", BIRTHDATE                    "
                    strQuery2 += ", BIRTH_PLACE                  "
                    strQuery2 += ", MOTHERS_NAME                 "
                    strQuery2 += ", [ALIAS]                      "
                    strQuery2 += ", SSN                          "
                    strQuery2 += ", PASSPORT_NUMBER              "
                    strQuery2 += ", PASSPORT_COUNTRY             "
                    strQuery2 += ", ID_NUMBER                    "
                    strQuery2 += ", NATIONALITY1                 "
                    strQuery2 += ", NATIONALITY2                 "
                    strQuery2 += ", NATIONALITY3                 "
                    strQuery2 += ", RESIDENCE                    "
                    strQuery2 += ", EMAIL                        "
                    strQuery2 += ", OCCUPATION                   "
                    strQuery2 += ", EMPLOYER_NAME                "
                    strQuery2 += ", DECEASED                     "
                    strQuery2 += ", DECEASED_DATE                "
                    strQuery2 += ", TAX_NUMBER                   "
                    strQuery2 += ", TAX_REG_NUMBER               "
                    strQuery2 += ", SOURCE_OF_WEALTH             "
                    strQuery2 += ", COMMENT                      "
                    strQuery2 += ", ROLE                         "
                    strQuery2 += ", EMAIL2                       "
                    strQuery2 += ", EMAIL3                       "
                    strQuery2 += ", EMAIL4                       "
                    strQuery2 += ", EMAIL5                       "
                    strQuery2 += ", FK_SENDER_INFORMATION        "
                    strQuery2 += ", FK_REPORT_ID                 "
                    strQuery2 += ", CreatedBy                    "
                    strQuery2 += ", LastUpdateBy                 "
                    strQuery2 += ", CreatedDate                  "
                    strQuery2 += ", LastUpdateDate               "
                    strQuery2 += ") VALUES ( "
                    strQuery2 += "  '" & PKReportType & "'"
                    strQuery2 += ", '" & PKEntity & "'"
                    strQuery2 += ", '" & objDirector.GENDER & "'"
                    strQuery2 += ", '" & objDirector.TITLE & "'"
                    strQuery2 += ", '" & objDirector.FIRST_NAME & "'"
                    strQuery2 += ", '" & objDirector.MIDDLE_NAME & "'"
                    strQuery2 += ", '" & objDirector.PREFIX & "'"
                    strQuery2 += ", '" & objDirector.LAST_NAME & "'"
                    strQuery2 += ", '" & objDirector.BIRTHDATE & "'"
                    strQuery2 += ", '" & objDirector.BIRTH_PLACE & "'"
                    strQuery2 += ", '" & objDirector.MOTHERS_NAME & "'"
                    strQuery2 += ", '" & objDirector.[ALIAS] & "'"
                    strQuery2 += ", '" & objDirector.SSN & "'"
                    strQuery2 += ", '" & objDirector.PASSPORT_NUMBER & "'"
                    strQuery2 += ", '" & objDirector.PASSPORT_COUNTRY & "'"
                    strQuery2 += ", '" & objDirector.ID_NUMBER & "'"
                    strQuery2 += ", '" & objDirector.NATIONALITY1 & "'"
                    strQuery2 += ", '" & objDirector.NATIONALITY2 & "'"
                    strQuery2 += ", '" & objDirector.NATIONALITY3 & "'"
                    strQuery2 += ", '" & objDirector.RESIDENCE & "'"
                    strQuery2 += ", '" & objDirector.EMAIL & "'"
                    strQuery2 += ", '" & objDirector.OCCUPATION & "'"
                    strQuery2 += ", '" & objDirector.EMPLOYER_NAME & "'"
                    strQuery2 += ", '" & objDirector.DECEASED & "'"
                    strQuery2 += ", '" & objDirector.DECEASED_DATE & "'"
                    strQuery2 += ", '" & objDirector.TAX_NUMBER & "'"
                    strQuery2 += ", '" & objDirector.TAX_REG_NUMBER & "'"
                    strQuery2 += ", '" & objDirector.SOURCE_OF_WEALTH & "'"
                    strQuery2 += ", '" & objDirector.COMMENT & "'"
                    strQuery2 += ", '" & objDirector.ROLE & "'"
                    strQuery2 += ", '" & objDirector.EMAIL2 & "'"
                    strQuery2 += ", '" & objDirector.EMAIL3 & "'"
                    strQuery2 += ", '" & objDirector.EMAIL4 & "'"
                    strQuery2 += ", '" & objDirector.EMAIL5 & "'"
                    strQuery2 += ", '" & objDirector.FK_SENDER_INFORMATION & "'"
                    strQuery2 += ", '" & objDirector.FK_REPORT_ID & "'"
                    strQuery2 += ", '" & objDirector.CreatedBy & "'"
                    strQuery2 += ", '" & objDirector.LastUpdateBy & "'"
                    strQuery2 += ", '" & objDirector.CreatedDate & "'"
                    strQuery2 += ", '" & objDirector.LastUpdateDate & "')"

                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_SIPESATGOAML_ACT_DIRECTOR_ID FROM SIPESATGOAML_ACT_DIRECTOR order by PK_SIPESATGOAML_ACT_DIRECTOR_ID desc", Nothing)


                Else
                    strQuery2 = "UPDATE SIPESATGOAML_ACT_DIRECTOR "
                    strQuery2 += "SET "
                    strQuery2 += "  FK_REPORTPARTY_ID 			='" & PKReportType & "'"
                    strQuery2 += ", FK_ACT_ENTITY_ID             ='" & PKEntity & "'"
                    strQuery2 += ", GENDER                       ='" & objDirector.GENDER & "'"
                    strQuery2 += ", TITLE                        ='" & objDirector.TITLE & "'"
                    strQuery2 += ", FIRST_NAME                   ='" & objDirector.FIRST_NAME & "'"
                    strQuery2 += ", MIDDLE_NAME                  ='" & objDirector.MIDDLE_NAME & "'"
                    strQuery2 += ", PREFIX                       ='" & objDirector.PREFIX & "'"
                    strQuery2 += ", LAST_NAME                    ='" & objDirector.LAST_NAME & "'"
                    strQuery2 += ", BIRTHDATE                    ='" & objDirector.BIRTHDATE & "'"
                    strQuery2 += ", BIRTH_PLACE                  ='" & objDirector.BIRTH_PLACE & "'"
                    strQuery2 += ", MOTHERS_NAME                 ='" & objDirector.MOTHERS_NAME & "'"
                    strQuery2 += ", [ALIAS]                      ='" & objDirector.[ALIAS] & "'"
                    strQuery2 += ", SSN                          ='" & objDirector.SSN & "'"
                    strQuery2 += ", PASSPORT_NUMBER              ='" & objDirector.PASSPORT_NUMBER & "'"
                    strQuery2 += ", PASSPORT_COUNTRY             ='" & objDirector.PASSPORT_COUNTRY & "'"
                    strQuery2 += ", ID_NUMBER                    ='" & objDirector.ID_NUMBER & "'"
                    strQuery2 += ", NATIONALITY1                 ='" & objDirector.NATIONALITY1 & "'"
                    strQuery2 += ", NATIONALITY2                 ='" & objDirector.NATIONALITY2 & "'"
                    strQuery2 += ", NATIONALITY3                 ='" & objDirector.NATIONALITY3 & "'"
                    strQuery2 += ", RESIDENCE                    ='" & objDirector.RESIDENCE & "'"
                    strQuery2 += ", EMAIL                        ='" & objDirector.EMAIL & "'"
                    strQuery2 += ", OCCUPATION                   ='" & objDirector.OCCUPATION & "'"
                    strQuery2 += ", EMPLOYER_NAME                ='" & objDirector.EMPLOYER_NAME & "'"
                    strQuery2 += ", DECEASED                     ='" & objDirector.DECEASED & "'"
                    strQuery2 += ", DECEASED_DATE                ='" & objDirector.DECEASED_DATE & "'"
                    strQuery2 += ", TAX_NUMBER                   ='" & objDirector.TAX_NUMBER & "'"
                    strQuery2 += ", TAX_REG_NUMBER               ='" & objDirector.TAX_REG_NUMBER & "'"
                    strQuery2 += ", SOURCE_OF_WEALTH             ='" & objDirector.SOURCE_OF_WEALTH & "'"
                    strQuery2 += ", COMMENT                      ='" & objDirector.COMMENT & "'"
                    strQuery2 += ", ROLE                         ='" & objDirector.ROLE & "'"
                    strQuery2 += ", EMAIL2                       ='" & objDirector.EMAIL2 & "'"
                    strQuery2 += ", EMAIL3                       ='" & objDirector.EMAIL3 & "'"
                    strQuery2 += ", EMAIL4                       ='" & objDirector.EMAIL4 & "'"
                    strQuery2 += ", EMAIL5                       ='" & objDirector.EMAIL5 & "'"
                    strQuery2 += ", FK_SENDER_INFORMATION        ='" & objDirector.FK_SENDER_INFORMATION & "'"
                    strQuery2 += ", FK_REPORT_ID                 ='" & objDirector.FK_REPORT_ID & "'"
                    strQuery2 += ", CreatedBy                    ='" & objDirector.CreatedBy & "'"
                    strQuery2 += ", LastUpdateBy                 ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                    strQuery2 += ", CreatedDate                  ='" & objDirector.CreatedDate & "'"
                    strQuery2 += ", LastUpdateDate               ='" & DateTime.Now & "'"
                    strQuery2 += "from SIPESATGOAML_ACT_DIRECTOR  where PK_SIPESATGOAML_ACT_DIRECTOR_ID = " & objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                    OldPK = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID
                    NewPk = OldPK
                End If


                'Director Address
                Dim listDirectorAddress = objData.list_goAML_Act_Director_Address.Where(Function(x) x.FK_ACT_DIRECTOR_ID = NewPk).ToList
                If listDirectorAddress IsNot Nothing Then
                    For Each itemAddress In listDirectorAddress
                        itemAddress.FK_REPORT_ID = strReportID
                        itemAddress.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID

                        If itemAddress.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_DIRECTOR_ADDRESS "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_DIRECTOR_ID		"
                            strQuery2 += ", ADDRESS_TYPE         "
                            strQuery2 += ", ADDRESS              "
                            strQuery2 += ", TOWN                 "
                            strQuery2 += ", CITY                 "
                            strQuery2 += ", ZIP                  "
                            strQuery2 += ", COUNTRY_CODE         "
                            strQuery2 += ", STATE                "
                            strQuery2 += ", COMMENTS             "
                            strQuery2 += ", ISEMPLOYER           "
                            strQuery2 += ", FK_REPORT_ID         "
                            strQuery2 += ", CreatedBy			"
                            strQuery2 += ", LastUpdateBy         "
                            strQuery2 += ", CreatedDate          "
                            strQuery2 += ", LastUpdateDate       "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS_Type & "'"
                            strQuery2 += ", '" & itemAddress.ADDRESS & "'"
                            strQuery2 += ", '" & itemAddress.TOWN & "'"
                            strQuery2 += ", '" & itemAddress.CITY & "'"
                            strQuery2 += ", '" & itemAddress.ZIP & "'"
                            strQuery2 += ", '" & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", '" & itemAddress.STATE & "'"
                            strQuery2 += ", '" & itemAddress.COMMENTS & "'"
                            strQuery2 += ", '" & itemAddress.ISEMPLOYER & "'"
                            strQuery2 += ", '" & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemAddress.CreatedBy & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemAddress.CreatedDate & "'"
                            strQuery2 += ", '" & itemAddress.LastUpdateDate & "')"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_DIRECTOR_ADDRESS "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_DIRECTOR_ID='" & OldPK & "'"
                            strQuery2 += ", ADDRESS_TYPE ='" & itemAddress.ADDRESS_Type & "'"
                            strQuery2 += ", ADDRESS ='" & itemAddress.ADDRESS & "'"
                            strQuery2 += ", TOWN ='" & itemAddress.TOWN & "'"
                            strQuery2 += ", CITY ='" & itemAddress.CITY & "'"
                            strQuery2 += ", ZIP ='" & itemAddress.ZIP & "'"
                            strQuery2 += ", COUNTRY_CODE ='" & itemAddress.COUNTRY_CODE & "'"
                            strQuery2 += ", STATE ='" & itemAddress.STATE & "'"
                            strQuery2 += ", COMMENTS ='" & itemAddress.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID ='" & itemAddress.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy='" & itemAddress.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate='" & itemAddress.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_DIRECTOR_ADDRESS  where PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID = " & itemAddress.PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)
                        End If

                    Next
                End If

                'Director Signatory Phone
                Dim listDirectorPhone = objData.list_goAML_Act_Director_Phone.Where(Function(x) x.FK_ACT_DIRECTOR_ID = NewPk).ToList
                If listDirectorPhone IsNot Nothing Then
                    For Each itemPhone In listDirectorPhone
                        itemPhone.FK_REPORT_ID = strReportID
                        itemPhone.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID
                        strQuery2 = ""
                        If itemPhone.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_DIRECTOR_PHONE "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_DIRECTOR_ID 			 "
                            strQuery2 += ", TPH_CONTACT_TYPE              "
                            strQuery2 += ", TPH_COMMUNICATION_TYPE        "
                            strQuery2 += ", TPH_COUNTRY_PREFIX            "
                            strQuery2 += ", TPH_NUMBER                    "
                            strQuery2 += ", TPH_EXTENSION                 "
                            strQuery2 += ", COMMENTS                      "
                            strQuery2 += ", ISEMPLOYER                      "
                            strQuery2 += ", FK_REPORT_ID                  "
                            strQuery2 += ", CreatedBy                     "
                            strQuery2 += ", LastUpdateBy                  "
                            strQuery2 += ", CreatedDate                   "
                            strQuery2 += ", LastUpdateDate                "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", '" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", '" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", '" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", '" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", '" & itemPhone.ISEMPLOYER & "'"
                            strQuery2 += ", '" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", '" & itemPhone.LastUpdateDate & "')"

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_DIRECTOR_PHONE "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_DIRECTOR_ID 			='" & OldPK & "'"
                            strQuery2 += ", TPH_CONTACT_TYPE             ='" & itemPhone.TPH_CONTACT_TYPE & "'"
                            strQuery2 += ", TPH_COMMUNICATION_TYPE       ='" & itemPhone.TPH_COMMUNICATION_TYPE & "'"
                            strQuery2 += ", TPH_COUNTRY_PREFIX           ='" & itemPhone.TPH_COUNTRY_PREFIX & "'"
                            strQuery2 += ", TPH_NUMBER                   ='" & itemPhone.TPH_NUMBER & "'"
                            strQuery2 += ", TPH_EXTENSION                ='" & itemPhone.TPH_EXTENSION & "'"
                            strQuery2 += ", COMMENTS                     ='" & itemPhone.COMMENTS & "'"
                            strQuery2 += ", FK_REPORT_ID                 ='" & itemPhone.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                    ='" & itemPhone.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy                 ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate                  ='" & itemPhone.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate               ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_DIRECTOR_PHONE  where PK_SIPESATGOAML_ACT_DIRECTOR_PHONE = " & itemPhone.PK_SIPESATGOAML_ACT_DIRECTOR_PHONE
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        End If

                    Next
                End If

                'Director  Identification
                Dim listDirectorIdentification = objData.list_goAML_Act_Director_Identification.Where(Function(x) x.FK_ACT_DIRECTOR_ID = NewPk).ToList
                If listDirectorIdentification IsNot Nothing Then
                    For Each itemIdentification In listDirectorIdentification
                        itemIdentification.FK_REPORT_ID = strReportID
                        itemIdentification.FK_ACT_DIRECTOR_ID = objDirector.PK_SIPESATGOAML_ACT_DIRECTOR_ID
                        strQuery2 = ""
                        If itemIdentification.PK_ACT_DIRECTOR_IDENTIFICATION_ID < 0 Then
                            strQuery2 = "INSERT INTO SIPESATGOAML_ACT_DIRECTOR_Identification "
                            strQuery2 += "("
                            strQuery2 += "  FK_ACT_DIRECTOR_ID			 "
                            strQuery2 += ", TYPE                         "
                            strQuery2 += ", NUMBER                       "
                            strQuery2 += ", ISSUED_DATE                  "
                            strQuery2 += ", EXPIRY_DATE                  "
                            strQuery2 += ", ISSUED_BY                    "
                            strQuery2 += ", ISSUED_COUNTRY               "
                            strQuery2 += ", IDENTIFICATION_COMMENT       "
                            strQuery2 += ", FK_REPORT_ID                 "
                            strQuery2 += ", CreatedBy                    "
                            strQuery2 += ", LastUpdateBy                 "
                            strQuery2 += ", CreatedDate                  "
                            strQuery2 += ", LastUpdateDate               "
                            strQuery2 += ") VALUES ( "
                            strQuery2 += "  '" & OldPK & "'"
                            strQuery2 += ", '" & itemIdentification.TYPE & "'"
                            strQuery2 += ", '" & itemIdentification.NUMBER & "'"
                            strQuery2 += ", '" & itemIdentification.ISSUED_DATE & "'"
                            strQuery2 += ", '" & itemIdentification.EXPIRY_DATE & "'"
                            strQuery2 += ", '" & itemIdentification.ISSUED_BY & "'"
                            strQuery2 += ", '" & itemIdentification.ISSUED_COUNTRY & "'"
                            strQuery2 += ", '" & itemIdentification.IDENTIFICATION_COMMENT & "'"
                            strQuery2 += ", '" & itemIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", '" & itemIdentification.CreatedBy & "'"
                            strQuery2 += ", '" & itemIdentification.LastUpdateBy & "'"
                            strQuery2 += ", '" & itemIdentification.CreatedDate & "'"
                            strQuery2 += ", '" & itemIdentification.LastUpdateDate & "')"

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        Else
                            strQuery2 = "UPDATE SIPESATGOAML_ACT_DIRECTOR_Identification "
                            strQuery2 += "SET "
                            strQuery2 += "  FK_ACT_DIRECTOR_ID			 ='" & OldPK & "'"
                            strQuery2 += ", TYPE                         ='" & itemIdentification.TYPE & "'"
                            strQuery2 += ", NUMBER                       ='" & itemIdentification.NUMBER & "'"
                            strQuery2 += ", ISSUED_DATE                  ='" & itemIdentification.ISSUED_DATE & "'"
                            strQuery2 += ", EXPIRY_DATE                  ='" & itemIdentification.EXPIRY_DATE & "'"
                            strQuery2 += ", ISSUED_BY                    ='" & itemIdentification.ISSUED_BY & "'"
                            strQuery2 += ", ISSUED_COUNTRY               ='" & itemIdentification.ISSUED_COUNTRY & "'"
                            strQuery2 += ", IDENTIFICATION_COMMENT       ='" & itemIdentification.IDENTIFICATION_COMMENT & "'"
                            strQuery2 += ", FK_REPORT_ID                 ='" & itemIdentification.FK_REPORT_ID & "'"
                            strQuery2 += ", CreatedBy                    ='" & itemIdentification.CreatedBy & "'"
                            strQuery2 += ", LastUpdateBy                 ='" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                            strQuery2 += ", CreatedDate                  ='" & itemIdentification.CreatedDate & "'"
                            strQuery2 += ", LastUpdateDate               ='" & DateTime.Now & "'"
                            strQuery2 += "from SIPESATGOAML_ACT_DIRECTOR_Identification  where PK_ACT_DIRECTOR_IDENTIFICATION_ID = " & itemIdentification.PK_ACT_DIRECTOR_IDENTIFICATION_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery2, Nothing)

                        End If

                    Next
                End If

            Next

            'Jalankan validasi satuan untuk replace IsValid dan Error Message
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Validate Satuan Started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).DbType = SqlDbType.BigInt
            NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGoAML_ValidasiSatuan", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Validate Satuan Finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveActivity_AuditTrail(objData As SIPESATGoAML_Activity_Class, objData_Old As SIPESATGoAML_Activity_Class, objDB As SIPESATGoAMLDAL.SIPESATGoAMLEntities, objModule As NawaDAL.Module, intModuleAction As Integer)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("SIPESATGOAML_ACT_REPORTPARTYTYPE")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
            '18-Mar-2022 Adi : Fixing Audit Trail
            If objData_Old Is Nothing OrElse IsNothing(objData_Old.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID) OrElse objData_Old.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID = 0 Then
                intModuleAction = 1
            Else
                intModuleAction = 2
            End If
            Dim strQueryAudit As String
            ' Audit Trail
            strQueryAudit = "INSERT INTO AuditTrailHeader ( "
            strQueryAudit += "ApproveBy,"
            strQueryAudit += "CreatedBy,"
            strQueryAudit += "CreatedDate,"
            strQueryAudit += "FK_AuditTrailStatus_ID,"
            strQueryAudit += "FK_ModuleAction_ID,"
            strQueryAudit += "ModuleLabel"
            strQueryAudit += " ) VALUES ( "
            strQueryAudit += "'" & strUserID & "'"
            strQueryAudit += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
            strQueryAudit += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            strQueryAudit += " , '" & intAuditTrailStatus & "'"
            strQueryAudit += " , '" & intModuleAction & "'"
            strQueryAudit += " , '" & objModule.ModuleLabel & "')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAudit, Nothing)

            Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)


            'Save Audit Trail Detail by XML
            Dim lngPKAuditTrail As Long = PKAudit

            Dim listActivity As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)
            Dim listActivity_Old As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)

            listActivity.Add(objData.obj_goAML_Act_ReportPartyType)
            listActivity_Old.Add(objData_Old.obj_goAML_Act_ReportPartyType)
            NawaFramework.CreateAuditTrailDetailXML(PKAudit, listActivity, listActivity_Old)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person, objData_Old.list_goAML_Act_Person)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Address, objData_Old.list_goAML_Act_Person_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Phone, objData_Old.list_goAML_Act_Person_Phone)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Relationship, objData_Old.list_goAML_Act_Person_Relationship)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity, objData_Old.list_goAML_Act_Entity)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Address, objData_Old.list_goAML_Act_Entity_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Phone, objData_Old.list_goAML_Act_Entity_Phone)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Relationship, objData_Old.list_goAML_Act_Entity_Relationship)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director, objData_Old.list_goAML_Act_Director)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Address, objData_Old.list_goAML_Act_Director_Address)
            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Phone, objData_Old.list_goAML_Act_Director_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Account, objData_Old.list_goAML_Act_Account)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_acc_Signatory, objData_Old.list_goAML_Act_acc_Signatory)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_sign_Address, objData_Old.list_goAML_Act_Acc_sign_Address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_sign_Phone, objData_Old.list_goAML_Act_Acc_sign_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Account, objData_Old.list_goAML_Act_Entity_Account)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Address, objData_Old.list_goAML_Act_Acc_Entity_Address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Phone, objData_Old.list_goAML_Act_Acc_Entity_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Ent_Director, objData_Old.list_goAML_Act_Acc_Ent_Director)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Director_address, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Acc_Entity_Director_Phone, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person, objData_Old.list_goAML_Act_Person)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Address, objData_Old.list_goAML_Act_Person_Address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Person_Phone, objData_Old.list_goAML_Act_Person_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity, objData_Old.list_goAML_Act_Entity)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Address, objData_Old.list_goAML_Act_Entity_Address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Entity_Phone, objData_Old.list_goAML_Act_Entity_Phone)

            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director, objData_Old.list_goAML_Act_Director)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Address, objData_Old.list_goAML_Act_Director_Address)
            'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Act_Director_Phone, objData_Old.list_goAML_Act_Director_Phone)

            NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, objData.list_goAML_Activity_Person_Identification, objData_Old.list_goAML_Activity_Person_Identification)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            'End of 01-Sep-2021 Adi : Save Audit Trail Detail by XML
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID(strActivityID As String)
        Try
            'Define local variables
            Dim objModule As NawaDAL.Module = NawaBLL.ModuleBLL.GetModuleByModuleName("SIPESATGOAML_ACT_REPORTPARTYTYPE")
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
            End If

            'Get Old Data to Delete
            Dim objData_Old = GetGoAMLActivityClassByID(strActivityID)
            If objData_Old Is Nothing Then
                objData_Old = New SIPESATGoAML_Activity_Class
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Delete Activity started (Activity ID = " & strActivityID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                    Try
                        'Delete Activity
                        strQuery += "DELETE FROM SIPESATGOAML_ACT_REPORTPARTYTYPE		"
                        strQuery += "where PK_ACT_REPORTPARTYTYPE_ID =  " & strActivityID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        '1. Person
                        For Each item_old In objData_Old.list_goAML_Act_Person
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_PERSON		"
                            strQuery += "where FK_ACT_REPORTPARTY_ID =  " & item_old.FK_ACT_REPORTPARTY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Next

                        'Person Address
                        For Each item_old In objData_Old.list_goAML_Act_Person_Address
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_PERSON_ADDRESS		"
                            strQuery += "where FK_ACT_PERSON =  " & item_old.FK_ACT_PERSON
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Next

                        'Person Phone
                        For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_PERSON_PHONE		"
                            strQuery += "where FK_ACT_PERSON =  " & item_old.FK_ACT_PERSON
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Next

                        'Person Identification
                        For Each item_old In objData_Old.list_goAML_Activity_Person_Identification
                            strQuery += "DELETE FROM SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION		"
                            strQuery += "where FK_ACT_PERSON =  " & item_old.FK_ACT_PERSON
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        Next


                        'Person Relationship
                        For Each item_old In objData_Old.list_goAML_Act_Person_Relationship
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_PERSON_RELATIONSHIP		"
                            strQuery += "where FK_ACT_PERSON_ID =  " & item_old.FK_ACT_PERSON_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        '2. Entity
                        For Each item_old In objData_Old.list_goAML_Act_Entity
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_ENTITY		"
                            strQuery += "where FK_ACT_REPORTPARTY_ID =  " & item_old.FK_ACT_REPORTPARTY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Entity Address
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_ENTITY_ADDRESS		"
                            strQuery += "where FK_ACT_ENTITY_ID =  " & item_old.FK_ACT_ENTITY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Entity Phone
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_ENTITY_PHONE		"
                            strQuery += "where FK_ACT_ENTITY_ID =  " & item_old.FK_ACT_ENTITY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Entity Relationship
                        For Each item_old In objData_Old.list_goAML_Act_Entity_Relationship
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_ENTITY_RELATIONSHIP		"
                            strQuery += "where FK_ACT_ENTITY_ID =  " & item_old.FK_ACT_ENTITY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next


                        '3. Director
                        For Each item_old In objData_Old.list_goAML_Act_Director
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_DIRECTOR		"
                            strQuery += "where FK_ACT_ENTITY_ID =  " & item_old.FK_ACT_ENTITY_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Director Address
                        For Each item_old In objData_Old.list_goAML_Act_Director_Address
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_DIRECTOR_ADDRESS		"
                            strQuery += "where FK_ACT_DIRECTOR_ID =  " & item_old.FK_ACT_DIRECTOR_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Director Phone
                        For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_DIRECTOR_PHONE		"
                            strQuery += "where FK_ACT_DIRECTOR_ID =  " & item_old.FK_ACT_DIRECTOR_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next

                        'Director Identification
                        For Each item_old In objData_Old.list_goAML_Act_Director_Identification
                            strQuery += "DELETE FROM SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION		"
                            strQuery += "where FK_ACT_DIRECTOR_ID =  " & item_old.FK_ACT_DIRECTOR_ID
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        Next



                        ''Account Signatory
                        'For Each item_old In objData_Old.list_goAML_Act_acc_Signatory
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        ''Account Signatory Address
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        ''Account Signatory Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_sign_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        ''Account Entity
                        'For Each item_old In objData_Old.list_goAML_Act_Entity_Account
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        ''Account Entity Address
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        ''Account Entity Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Account Entity Director
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_Ent_Director
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Account Entity Director Address
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Account Entity Director Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Acc_Entity_Director_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        '2. Person
                        'For Each item_old In objData_Old.list_goAML_Act_Person
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Person Address
                        'For Each item_old In objData_Old.list_goAML_Act_Person_Address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Person Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Person_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        '3. Entity
                        'For Each item_old In objData_Old.list_goAML_Act_Entity
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Entity Address
                        'For Each item_old In objData_Old.list_goAML_Act_Entity_Address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Entity Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Entity_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Entity Director
                        'For Each item_old In objData_Old.list_goAML_Act_Director
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Entity Director Address
                        'For Each item_old In objData_Old.list_goAML_Act_Director_Address
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next

                        'Entity Director Phone
                        'For Each item_old In objData_Old.list_goAML_Act_Director_Phone
                        '    objDB.Entry(item_old).State = Entity.EntityState.Deleted
                        'Next


                    Catch ex As Exception
                        objTrans.Rollback()
                        Throw ex
                    End Try
                End Using

                strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Delete Activity finished (Activity ID = " & strActivityID & ")')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
            End Using

            'Save Audit Trail
            DeleteActivityByActivityID_AuditTrail(objData_Old, objModule)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub DeleteActivityByActivityID_AuditTrail(objData_Old As SIPESATGoAML_Activity_Class, objModule As NawaDAL.Module)
        Try
            'Define local variables
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("SIPESATGOAML_ACT_REPORTPARTYTYPE")

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                'Dim objAuditTrailheader As NawaDevDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                '18-Mar-2022 Adi : Fixing Audit Trail
                Dim strQueryAudit As String
                ' Audit Trail
                strQueryAudit = "INSERT INTO AuditTrailHeader ( "
                strQueryAudit += "ApproveBy,"
                strQueryAudit += "CreatedBy,"
                strQueryAudit += "CreatedDate,"
                strQueryAudit += "FK_AuditTrailStatus_ID,"
                strQueryAudit += "FK_ModuleAction_ID,"
                strQueryAudit += "ModuleLabel"
                strQueryAudit += " ) VALUES ( "
                strQueryAudit += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                strQueryAudit += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                strQueryAudit += " , '" & NawaBLL.Common.ModuleActionEnum.Delete & "'"
                strQueryAudit += " , '" & objModule.ModuleLabel & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAudit, Nothing)

                Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                'Dim objAuditTrailheader As SIPESATGoAMLDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, Nothing, intAuditTrailStatus, 3, objModule.ModuleLabel, strUserID)

                'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.obj_goAML_Act_ReportPartyType)

                'Save Audit Trail Detail by XML
                Dim lngPKAuditTrail As Long = PKAudit

                Dim listActivityToDelete As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)
                listActivityToDelete.Add(objData_Old.obj_goAML_Act_ReportPartyType)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, listActivityToDelete)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Phone)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Relationship)


                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Phone)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Relationship)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Address)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Phone)

                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Account)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Address)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Phone)

                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Ent_Director)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_address)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Acc_Entity_Director_Phone)

                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Address)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Person_Phone)

                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Address)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Entity_Phone)

                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Address)
                'NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Act_Director_Phone)

                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, Nothing, objData_Old.list_goAML_Activity_Person_Identification)
            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Sub SaveAuditTrialReportAndActivity(objDataReport As SIPESATGoAML_Report_Class, objDataReport_Old As SIPESATGoAML_Report_Class, objDataActivity As List(Of SIPESATGoAML_Activity_Class), objDataActivity_Old As List(Of SIPESATGoAML_Activity_Class))
        Try
            Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
            Dim strAlternateID As String = Nothing
            Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
            Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
            Dim objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("SIPESATGOAML_REPORT")
            Dim strModuleName As String = objModule.ModuleLabel
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report & Activity', 'Save Audit Trail started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)


            Using objDB As New SIPESATGoAMLDAL.SIPESATGoAMLEntities
                Dim strQueryAudit As String
                ' Audit Trail
                strQueryAudit = "INSERT INTO AuditTrailHeader ( "
                strQueryAudit += "ApproveBy,"
                strQueryAudit += "CreatedBy,"
                strQueryAudit += "CreatedDate,"
                strQueryAudit += "FK_AuditTrailStatus_ID,"
                strQueryAudit += "FK_ModuleAction_ID,"
                strQueryAudit += "ModuleLabel"
                strQueryAudit += " ) VALUES ( "
                strQueryAudit += "'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " ,'" & NawaBLL.Common.SessionCurrentUser.UserID & "'"
                strQueryAudit += " , '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                strQueryAudit += " , '" & NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase & "'"
                strQueryAudit += " , '" & NawaBLL.Common.ModuleActionEnum.Update & "'"
                strQueryAudit += " , '" & objModule.ModuleLabel & "')"
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryAudit, Nothing)

                Dim PKAudit As Long = SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, "select TOP 1 PK_AuditTrail_ID FROM AuditTrailHeader order by pk_AuditTrail_id desc", Nothing)

                Dim lngPKAuditTrail As Long = PKAudit

                Dim listReport As New List(Of SIPESATGOAML_REPORT)
                Dim listReport_Old As New List(Of SIPESATGOAML_REPORT)

                listReport.Add(objDataReport.obj_goAML_Report)
                listReport_Old.Add(objDataReport_Old.obj_goAML_Report)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listReport, listReport_Old)

                Dim listActivity As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)
                Dim listActivity_Old As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)
                Dim listActivity_list_goAML_Act_Person As New List(Of SIPESATGOAML_ACT_PERSON)
                Dim listActivity_list_goAML_Act_Person_Old As New List(Of SIPESATGOAML_ACT_PERSON)
                Dim listActivity_list_goAML_Act_Person_Address As New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
                Dim listActivity_list_goAML_Act_Person_Address_Old As New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
                Dim listActivity_list_goAML_Act_Person_Phone As New List(Of SIPESATGOAML_ACT_PERSON_PHONE)
                Dim listActivity_list_goAML_Act_Person_Phone_Old As New List(Of SIPESATGOAML_ACT_PERSON_PHONE)
                Dim listActivity_list_goAML_Act_Person_Relationship As New List(Of SIPESATGOAML_ACT_PERSON_RELATIONSHIP)
                Dim listActivity_list_goAML_Act_Person_Relationship_Old As New List(Of SIPESATGOAML_ACT_PERSON_RELATIONSHIP)
                Dim listActivity_list_goAML_Act_Entity As New List(Of SIPESATGOAML_ACT_ENTITY)
                Dim listActivity_list_goAML_Act_Entity_Old As New List(Of SIPESATGOAML_ACT_ENTITY)
                Dim listActivity_list_goAML_Act_Entity_Address As New List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS)
                Dim listActivity_list_goAML_Act_Entity_Address_Old As New List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS)
                Dim listActivity_list_goAML_Act_Entity_Phone As New List(Of SIPESATGOAML_ACT_ENTITY_PHONE)
                Dim listActivity_list_goAML_Act_Entity_Phone_Old As New List(Of SIPESATGOAML_ACT_ENTITY_PHONE)
                Dim listActivity_list_goAML_Act_Entity_Relationship As New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
                Dim listActivity_list_goAML_Act_Entity_Relationship_Old As New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
                Dim listActivity_list_goAML_Act_Director As New List(Of SIPESATGOAML_ACT_DIRECTOR)
                Dim listActivity_list_goAML_Act_Director_Old As New List(Of SIPESATGOAML_ACT_DIRECTOR)
                Dim listActivity_list_goAML_Act_Director_Address As New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                Dim listActivity_list_goAML_Act_Director_Address_Old As New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
                Dim listActivity_list_goAML_Act_Director_Phone As New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
                Dim listActivity_list_goAML_Act_Director_Phone_Old As New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)

                'Dim listActivity_list_goAML_Act_acc_Signatory As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
                'Dim listActivity_list_goAML_Act_acc_Signatory_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
                'Dim listActivity_list_goAML_Act_Acc_sign_Address As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
                'Dim listActivity_list_goAML_Act_Acc_sign_Address_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
                'Dim listActivity_list_goAML_Act_Acc_sign_Phone As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)
                'Dim listActivity_list_goAML_Act_Acc_sign_Phone_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)
                'Dim listActivity_list_goAML_Act_Entity_Account As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT)
                'Dim listActivity_list_goAML_Act_Entity_Account_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT)
                'Dim listActivity_list_goAML_Act_Acc_Entity_Address As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS)
                'Dim listActivity_list_goAML_Act_Acc_Entity_Address_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS)
                'Dim listActivity_list_goAML_Act_Acc_Entity_Phone As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE)
                'Dim listActivity_list_goAML_Act_Acc_Entity_Phone_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE)
                'Dim listActivity_list_goAML_Activity_Person_Identification As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)
                'Dim listActivity_list_goAML_Activity_Person_Identification_Old As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)

                For Each Activity In objDataActivity
                    listActivity.Add(Activity.obj_goAML_Act_ReportPartyType)
                    listActivity_list_goAML_Act_Person.AddRange(Activity.list_goAML_Act_Person)
                    listActivity_list_goAML_Act_Person_Address.AddRange(Activity.list_goAML_Act_Person_Address)
                    listActivity_list_goAML_Act_Person_Phone.AddRange(Activity.list_goAML_Act_Person_Phone)
                    listActivity_list_goAML_Act_Person_Relationship.AddRange(Activity.list_goAML_Act_Person_Relationship)
                    listActivity_list_goAML_Act_Entity.AddRange(Activity.list_goAML_Act_Entity)
                    listActivity_list_goAML_Act_Entity_Address.AddRange(Activity.list_goAML_Act_Entity_Address)
                    listActivity_list_goAML_Act_Entity_Phone.AddRange(Activity.list_goAML_Act_Entity_Phone)
                    listActivity_list_goAML_Act_Entity_Relationship.AddRange(Activity.list_goAML_Act_Entity_Relationship)
                    listActivity_list_goAML_Act_Director.AddRange(Activity.list_goAML_Act_Director)
                    listActivity_list_goAML_Act_Director_Address.AddRange(Activity.list_goAML_Act_Director_Address)
                    listActivity_list_goAML_Act_Director_Phone.AddRange(Activity.list_goAML_Act_Director_Phone)

                Next
                For Each Activity In objDataActivity_Old
                    listActivity.Add(Activity.obj_goAML_Act_ReportPartyType)
                    listActivity_list_goAML_Act_Person_Old.AddRange(Activity.list_goAML_Act_Person)
                    listActivity_list_goAML_Act_Person_Address_Old.AddRange(Activity.list_goAML_Act_Person_Address)
                    listActivity_list_goAML_Act_Person_Phone_Old.AddRange(Activity.list_goAML_Act_Person_Phone)
                    listActivity_list_goAML_Act_Person_Relationship_Old.AddRange(Activity.list_goAML_Act_Person_Relationship)
                    listActivity_list_goAML_Act_Entity_Old.AddRange(Activity.list_goAML_Act_Entity)
                    listActivity_list_goAML_Act_Entity_Address_Old.AddRange(Activity.list_goAML_Act_Entity_Address)
                    listActivity_list_goAML_Act_Entity_Phone_Old.AddRange(Activity.list_goAML_Act_Entity_Phone)
                    listActivity_list_goAML_Act_Entity_Relationship_Old.AddRange(Activity.list_goAML_Act_Entity_Relationship)
                    listActivity_list_goAML_Act_Director_Old.AddRange(Activity.list_goAML_Act_Director)
                    listActivity_list_goAML_Act_Director_Address_Old.AddRange(Activity.list_goAML_Act_Director_Address)
                    listActivity_list_goAML_Act_Director_Phone_Old.AddRange(Activity.list_goAML_Act_Director_Phone)

                Next
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity, listActivity_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Person, listActivity_list_goAML_Act_Person_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Person_Address, listActivity_list_goAML_Act_Person_Address_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Person_Phone, listActivity_list_goAML_Act_Person_Phone_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Person_Relationship, listActivity_list_goAML_Act_Person_Relationship_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Entity, listActivity_list_goAML_Act_Entity_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Entity_Address, listActivity_list_goAML_Act_Entity_Address_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Entity_Phone, listActivity_list_goAML_Act_Entity_Phone_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Entity_Relationship, listActivity_list_goAML_Act_Entity_Relationship_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Director, listActivity_list_goAML_Act_Director_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Director_Address, listActivity_list_goAML_Act_Director_Address_Old)
                NawaFramework.CreateAuditTrailDetailXML(lngPKAuditTrail, listActivity_list_goAML_Act_Director_Phone, listActivity_list_goAML_Act_Director_Phone_Old)


            End Using

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report & Activity', 'Save Audit Trail finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "20-Mei-2022 : Save Data To Validate by XML"

    Shared Function validateActivity(objReport As SIPESATGoAML_Report_Class, objActivity As SIPESATGoAML_Activity_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Validate Activity', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of SIPESATGOAML_REPORT)
            Dim listGoAML_Activity As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)

            listGoAML_Report.Add(objReport.obj_goAML_Report)
            listGoAML_Activity.Add(objActivity.obj_goAML_Act_ReportPartyType)

            SaveDataToValidateWithXML(listGoAML_Report)
            SaveDataToValidateWithXML(listGoAML_Activity)

            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person_Phone)
            If objActivity.list_goAML_Act_Person_Relationship IsNot Nothing Then
                If objActivity.list_goAML_Act_Person_Relationship.Count > 0 Then
                    SaveDataToValidateWithXML(objActivity.list_goAML_Act_Person_Relationship)
                End If
            End If


            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Phone)
            If objActivity.list_goAML_Act_Entity_Relationship IsNot Nothing Then
                If objActivity.list_goAML_Act_Entity_Relationship.Count > 0 Then
                    SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Relationship)
                End If
            End If


            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director_Address)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director_Phone)
            SaveDataToValidateWithXML(objActivity.list_goAML_Act_Director_Identification)

            'SaveDataToValidateWithXML(objActivity.list_goAML_Act_Entity_Account)
            'SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Address)
            'SaveDataToValidateWithXML(objActivity.list_goAML_Act_Acc_Entity_Phone)

            SaveDataToValidateWithXML(objActivity.list_goAML_Activity_Person_Identification)

            'Audit Trail Header
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Validate Activity', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_PerActivityID
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Validate Activity', 'Validate Activity started (Activity ID = " & objActivity.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_goAML_Report.PK_REPORT_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@PK_TransactionOrActivity_ID"
            param(1).Value = objActivity.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID
            param(1).DbType = SqlDbType.BigInt

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).DbType = SqlDbType.VarChar

            'param(3) = New SqlParameter
            'param(3).ParameterName = "@TRN_or_ACT"
            'param(3).Value = "ACT"
            'param(3).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGOAML_Report_Validate_PerActivityID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Validate Activity', 'Validate Activity finished (Activity ID = " & objActivity.obj_goAML_Act_ReportPartyType.PK_ACT_REPORTPARTYTYPE_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function validateReportAndIndicator(objReport As SIPESATGoAML_Report_Class) As String
        Try
            Dim strQuery As String

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Data to Validate by XML started')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim listGoAML_Report As New List(Of SIPESATGOAML_REPORT)
            listGoAML_Report.Add(objReport.obj_goAML_Report)
            SaveDataToValidateWithXML(listGoAML_Report)
            'SaveDataToValidateWithXML(objReport.list_goAML_Report_Indicator)
            'If objReport.list_goAML_Transaction IsNot Nothing Then
            '    SaveDataToValidateWithXML(objReport.list_goAML_Transaction)
            'End If
            If objReport.list_goAML_Act_ReportPartyType IsNot Nothing Then
                SaveDataToValidateWithXML(objReport.list_goAML_Act_ReportPartyType)
            End If

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Save Data to Validate by XML finished')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan SP usp_GOAML_Report_Validate_ReportAndIndicator untuk Validasi hanya level Report dan Indicator
            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Validate Report started (Report ID = " & objReport.obj_goAML_Report.PK_REPORT_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = objReport.obj_goAML_Report.PK_REPORT_ID
            param(0).DbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID"
            param(1).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(1).DbType = SqlDbType.VarChar

            Dim strValidationResult As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGOAML_Validate_Report", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Validate Report finished (Report ID = " & objReport.obj_goAML_Report.PK_REPORT_ID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Ambil lagi semua ValidationReport by ReportID
            strValidationResult = getValidationReportByReportID(objReport.obj_goAML_Report.PK_REPORT_ID)
            Return strValidationResult

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Sub SaveDataToValidateWithXML(objData As Object)
        Try
            Dim objtype As Type = objData.GetType
            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
            Dim tablename As String = properties(2).PropertyType.Name

            Dim objdt As New DataTable
            Dim objdataset = New DataSet()
            Dim objXMLData As String = ""
            Dim byteData As Byte() = Nothing

            If objData IsNot Nothing Then
                objdt = NawaBLL.Common.CopyGenericToDataTable(objData)
                objdataset.Tables.Add(objdt)
                objXMLData = objdataset.GetXml()
                byteData = System.Text.Encoding.Default.GetBytes(objXMLData)
            End If

            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@TableName"
            param(0).Value = tablename
            param(0).SqlDbType = SqlDbType.VarChar

            param(1) = New SqlParameter
            param(1).ParameterName = "@XmlDocument"
            param(1).Value = byteData
            param(1).SqlDbType = SqlDbType.VarBinary

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID"
            param(2).Value = NawaBLL.Common.SessionCurrentUser.UserID
            param(2).SqlDbType = SqlDbType.VarChar

            Dim dtPK As DataTable = Nothing
            dtPK = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATgoAML_XML_ToValidate_Save", param)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "20-Mei-2022 : Delete Report"
    Shared Sub DeleteReportByReportID(strReportID As String, strUserMaker As String, strUserChecker As String)
        Try

            Dim strQuery As String = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Delete Report SIPESATGoAML started (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

            'Jalankan semua fungsi Delete, Audit Trail, Update Status dan List of Generated lewat SP
            Dim param(2) As SqlParameter

            param(0) = New SqlParameter
            param(0).ParameterName = "@PK_Report_ID"
            param(0).Value = strReportID
            param(0).SqlDbType = SqlDbType.BigInt

            param(1) = New SqlParameter
            param(1).ParameterName = "@UserID_Maker"
            param(1).Value = strUserMaker
            param(1).SqlDbType = SqlDbType.VarChar

            param(2) = New SqlParameter
            param(2).ParameterName = "@UserID_Checker"
            param(2).Value = strUserChecker
            param(2).SqlDbType = SqlDbType.VarChar

            NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPESATGoAML_DeleteReportByReportID", param)

            strQuery = "INSERT INTO EODLogSP (ProcessDate, Process, Keterangan) VALUES ('" & DateTime.Now & "', 'SIPESATGoAML Report', 'Delete Report SIPESATGoAML finished (Report ID = " & strReportID & ")')"
            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class

#Region "Model"

Public Class goAML_Ref_WIC

    Public Property PK_Customer_ID As Integer

    Public Property INDV_Title As String

    Public Property INDV_First_Name As String

    Public Property INDV_Middle_Name As String

    Public Property INDV_Prefix As String

    Public Property INDV_Last_Name As String

    Public Property INDV_BirthDate As Nullable(Of Date)

    Public Property INDV_Birth_Place As String

    Public Property INDV_Mothers_Name As String

    Public Property INDV_Alias As String

    Public Property INDV_SSN As String

    Public Property INDV_Passport_Number As String

    Public Property INDV_Passport_Country As String

    Public Property INDV_ID_Number As String

    Public Property INDV_Nationality1 As String

    Public Property INDV_Nationality2 As String

    Public Property INDV_Nationality3 As String

    Public Property INDV_Residence As String

    Public Property INDV_Email As String

    Public Property INDV_Occupation As String

    Public Property INDV_Employer_Name As String

    Public Property Corp_Name As String

    Public Property Corp_Commercial_Name As String

    Public Property Corp_Incorporation_Legal_Form As String

    Public Property Corp_Incorporation_Number As String

    Public Property Corp_Business As String

    Public Property Corp_Email As String

    Public Property Corp_Url As String

    Public Property Corp_Incorporation_State As String

    Public Property Corp_Incorporation_Country_Code As String

    Public Property Corp_Director_ID As Nullable(Of Integer)

    Public Property Corp_Role As String

    Public Property Corp_Incorporation_Date As Nullable(Of Date)

    Public Property Corp_Business_Closed As Nullable(Of Boolean)

    Public Property Corp_Date_Business_Closed As Nullable(Of Date)

    Public Property Corp_Tax_Number As String

    Public Property Corp_Tax_Registeration_Number As String

    Public Property Corp_Comments As String

    Public Property FK_Customer_Type_ID As Nullable(Of Integer)

    Public Property isUpdateFromDataSource As Nullable(Of Boolean)

    Public Property INDV_Gender As String

    Public Property INDV_SumberDana As String

    Public Property INDV_Tax_Number As String

    Public Property INDV_Tax_Reg_Number As Nullable(Of Boolean)

    Public Property INDV_Email2 As String

    Public Property INDV_Email3 As String

    Public Property INDV_Email4 As String

    Public Property INDV_Email5 As String

    Public Property INDV_Comment As String

    Public Property INDV_Deceased As Nullable(Of Boolean)

    Public Property INDV_Deceased_Date As Nullable(Of Date)

    Public Property WIC_No As String

    Public Property Active As Nullable(Of Boolean)

    Public Property CreatedBy As String

    Public Property LastUpdateBy As String

    Public Property ApprovedBy As String

    Public Property CreatedDate As Nullable(Of Date)

    Public Property LastUpdateDate As Nullable(Of Date)

    Public Property ApprovedDate As Nullable(Of Date)

    Public Property Alternateby As String

    Public Property Country_Of_Birth As String
    Public Property Full_Name_Frn As String
    Public Property Residence_Since As Nullable(Of Date)
    Public Property Is_Protected As Nullable(Of Boolean)
    Public Property Is_RealWIC As Nullable(Of Boolean)
    Public Property Entity_Status As String
    Public Property Entity_Status_Date As Nullable(Of Date)

End Class

Public Class goAML_Ref_Customer
    Public Property PK_Customer_ID As Integer
    Public Property CIF As String
    Public Property Status_Code As String
    Public Property INDV_Title As String
    Public Property INDV_First_name As String
    Public Property INDV_Middle_name As String
    Public Property INDV_Prefix As String
    Public Property INDV_Last_Name As String
    Public Property INDV_BirthDate As Nullable(Of Date)
    Public Property INDV_Birth_Place As String
    Public Property INDV_Mothers_Name As String
    Public Property INDV_Alias As String
    Public Property INDV_SSN As String
    Public Property INDV_Passport_Number As String
    Public Property INDV_Passport_Country As String
    Public Property INDV_ID_Number As String
    Public Property INDV_Nationality1 As String
    Public Property INDV_Nationality2 As String
    Public Property INDV_Nationality3 As String
    Public Property INDV_Residence As String
    Public Property INDV_Email As String
    Public Property INDV_Occupation As String
    Public Property INDV_Employer_Name As String
    Public Property Corp_Name As String
    Public Property Corp_Commercial_Name As String
    Public Property Corp_Incorporation_Legal_Form As String
    Public Property Corp_Incorporation_Number As String
    Public Property Corp_Business As String
    Public Property Corp_Email As String
    Public Property Corp_Url As String
    Public Property Corp_Incorporation_State As String
    Public Property Corp_Incorporation_Country_Code As String
    Public Property Corp_Director_ID As Nullable(Of Integer)
    Public Property Corp_Role As String
    Public Property Corp_Incorporation_Date As Nullable(Of Date)
    Public Property Corp_Business_Closed As Nullable(Of Boolean)
    Public Property Corp_Date_Business_Closed As Nullable(Of Date)
    Public Property Corp_Tax_Number As String
    Public Property Corp_Tax_Registeration_Number As String
    Public Property Corp_Comments As String
    Public Property FK_Customer_Type_ID As Nullable(Of Integer)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property isDeleted As String
    Public Property StatusApproval As String
    Public Property INDV_Gender As String
    Public Property INDV_Deceased As Nullable(Of Boolean)
    Public Property INDV_Deceased_Date As Nullable(Of Date)
    Public Property INDV_Tax_Number As String
    Public Property INDV_Tax_Reg_Number As Nullable(Of Boolean)
    Public Property INDV_Source_of_Wealth As String
    Public Property INDV_Comments As String
    Public Property INDV_Email2 As String
    Public Property INDV_Email3 As String
    Public Property INDV_Email4 As String
    Public Property INDV_Email5 As String
    Public Property isUpdateFromDataSource As Nullable(Of Boolean)
    Public Property GCN As String
    Public Property isGCNPrimary As Nullable(Of Boolean)
    Public Property Opening_Date As Nullable(Of Date)
    Public Property Opening_Branch_Code As String
    Public Property Status As String
    Public Property Closing_Date As Nullable(Of Date)
    Public Property FK_BU_CODE As String
    Public Property FK_SBU_CODE As String
    Public Property FK_RM_CODE As String

End Class
Public Class SIPESATGOAML_REPORT
    Public Property PK_REPORT_ID As Long
    Public Property RENTITY_ID As Nullable(Of Integer)
    Public Property SUBMISSION_CODE As String
    Public Property REPORT_CODE As String
    Public Property ENTITY_REFERENCE As String
    Public Property SUBMISSION_DATE As Nullable(Of Date)
    Public Property CURRENCY_CODE_LOCAL As String
    Public Property SUBMIT_DATE As Nullable(Of Date)
    Public Property ID_LAPORAN_PPATK_RESULT As String
    Public Property STATUS As Nullable(Of Integer)
    Public Property UNIKREFERENCE As String
    Public Property ISVALID As Nullable(Of Boolean)
    Public Property MONTH_PERIOD As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
    Public Property MarkedAsDelete As Nullable(Of Boolean)

End Class

Public Class SIPESATGOAML_ACT_REPORTPARTYTYPE
    Public Property PK_ACT_REPORTPARTYTYPE_ID As Long
    Public Property FK_Report_ID As Nullable(Of Long)
    Public Property SIGNIFICANCE As Nullable(Of Integer)
    Public Property REASON As String
    Public Property COMMENTS As String
    Public Property SUBNODETYPE As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_PERSON
    Public Property PK_SIPESATGOAML_ACT_PERSON_ID As Long
    Public Property FK_ACT_REPORTPARTY_ID As Nullable(Of Long)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property GENDER As String
    Public Property TITLE As String
    Public Property FIRST_NAME As String
    Public Property MIDDLE_NAME As String
    Public Property PREFIX As String
    Public Property LAST_NAME As String
    Public Property BIRTHDATE As Nullable(Of Date)
    Public Property BITH_PLACE As String
    Public Property MOTHERS_NAME As String
    Public Property [ALIAS] As String
    Public Property SSN As String
    Public Property PASSPORT_NUMBER As String
    Public Property PASSPORT_COUNTRY As String
    Public Property ID_NUMBER As String
    Public Property NATIONALITY1 As String
    Public Property NATIONALITY2 As String
    Public Property NATIONALITY3 As String
    Public Property RESIDENCE As String
    Public Property EMAIL As String
    Public Property OCCUPATION As String
    Public Property EMPLOYER_NAME As String
    Public Property DECEASED As Nullable(Of Boolean)
    Public Property DECEASED_DATE As Nullable(Of Date)
    Public Property TAX_NUMBER As String
    Public Property TAX_REG_NUMBER As Nullable(Of Boolean)
    Public Property SOURCE_OF_WEALTH As String
    Public Property COMMENT As String
    Public Property EMAIL2 As String
    Public Property EMAIL3 As String
    Public Property EMAIL4 As String
    Public Property EMAIL5 As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_PERSON_ADDRESS
    Public Property PK_SIPESATGOAML_ACT_PERSON_ADDRESS_ID As Long
    Public Property FK_ACT_PERSON As Nullable(Of Long)
    Public Property ADDRESS_TYPE As String
    Public Property ADDRESS As String
    Public Property TOWN As String
    Public Property CITY As String
    Public Property ZIP As String
    Public Property COUNTRY_CODE As String
    Public Property STATE As String
    Public Property COMMENTS As String
    Public Property ISEMPLOYER As Nullable(Of Boolean)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_PERSON_PHONE
    Public Property PK_SIPESATGOAML_ACT_PERSON_PHONE_ID As Long
    Public Property FK_ACT_PERSON As Nullable(Of Long)
    Public Property TPH_CONTACT_TYPE As String
    Public Property TPH_COMMUNICATION_TYPE As String
    Public Property TPH_COUNTRY_PREFIX As String
    Public Property TPH_NUMBER As String
    Public Property TPH_EXTENSION As String
    Public Property COMMENTS As String
    Public Property ISEMPLOYER As Nullable(Of Boolean)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_PERSON_RELATIONSHIP
    Public Property PK_SIPESATGOAML_ACT_PERSON_RELATIONSHIP_ID As Long
    Public Property FK_ACT_PERSON_ID As Nullable(Of Long)
    Public Property CLIENT_NUMBER As String
    Public Property VALID_FROM As Nullable(Of Date)
    Public Property IS_APPROX_FROM_DATE As Nullable(Of Boolean)
    Public Property VALID_TO As Nullable(Of Date)
    Public Property IS_APPROX_TO_DATE As Nullable(Of Boolean)
    Public Property COMMENTS As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION
    Public Property PK_ACTIVITY_PERSON_IDENTIFICATION_ID As Long
    Public Property FK_ACT_ACC_SIGN_ID As Nullable(Of Long)
    Public Property FK_ACT_ENTITY_ID As Nullable(Of Long)
    Public Property FK_ACT_PERSON As Nullable(Of Long)
    Public Property TYPE As String
    Public Property NUMBER As String
    Public Property ISSUED_DATE As Nullable(Of Date)
    Public Property EXPIRY_DATE As Nullable(Of Date)
    Public Property ISSUED_BY As String
    Public Property ISSUED_COUNTRY As String
    Public Property IDENTIFICATION_COMMENT As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_ENTITY
    Public Property PK_SIPESATGOAML_ACT_ENTITY_ID As Long
    Public Property FK_ACT_REPORTPARTY_ID As Nullable(Of Long)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property NAME As String
    Public Property COMMERCIAL_NAME As String
    Public Property INCORPORATION_LEGAL_FORM As String
    Public Property INCORPORATION_NUMBER As String
    Public Property BUSINESS As String
    Public Property EMAIL As String
    Public Property URL As String
    Public Property INCORPORATION_STATE As String
    Public Property INCORPORATION_COUNTRY_CODE As String
    Public Property DIRECTOR_ID As Nullable(Of Integer)
    Public Property INCORPORATION_DATE As Nullable(Of Date)
    Public Property BUSINESS_CLOSED As Nullable(Of Boolean)
    Public Property DATE_BUSINESS_CLOSED As Nullable(Of Date)
    Public Property TAX_NUMBER As String
    Public Property TAX_REGISTRATION_NUMBER As String
    Public Property COMMENTS As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_ENTITY_ACCOUNT
    Public Property PK_ACT_ENTITY_ACCOUNT_ID As Long
    Public Property FK_ACTIVITY_REPORTPARTY_ID As Nullable(Of Long)
    Public Property FK_ACT_ACCOUNT_ID As Nullable(Of Long)
    Public Property NAME As String
    Public Property COMMERCIAL_NAME As String
    Public Property INCORPORATION_LEGAL_FORM As String
    Public Property INCORPORATION_NUMBER As String
    Public Property BUSINESS As String
    Public Property EMAIL As String
    Public Property URL As String
    Public Property INCORPORATION_STATE As String
    Public Property INCORPORATION_COUNTRY_CODE As String
    Public Property INCORPORATION_DATE As Nullable(Of Date)
    Public Property BUSINESS_CLOSED As Nullable(Of Boolean)
    Public Property DATE_BUSINESS_CLOSED As Nullable(Of Date)
    Public Property TAX_NUMBER As String
    Public Property COMMENTS As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property CIF As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_ENTITY_ADDRESS
    Public Property PK_SIPESATGOAML_ACT_ENTITY_Address_ID As Long
    Public Property FK_ACT_ENTITY_ID As Nullable(Of Long)
    Public Property ADDRESS_TYPE As String
    Public Property ADDRESS As String
    Public Property TOWN As String
    Public Property CITY As String
    Public Property ZIP As String
    Public Property COUNTRY_CODE As String
    Public Property STATE As String
    Public Property COMMENTS As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_ENTITY_RELATIONSHIP
    Public Property PK_SIPESATGOAML_ACT_ENTITY_RELATIONSHIP_ID As Long
    Public Property FK_ACT_ENTITY_ID As Nullable(Of Long)
    Public Property CLIENT_NUMBER As String
    Public Property VALID_FROM As Nullable(Of Date)
    Public Property IS_APPROX_FROM_DATE As Nullable(Of Boolean)
    Public Property VALID_TO As Nullable(Of Date)
    Public Property IS_APPROX_TO_DATE As Nullable(Of Boolean)
    Public Property COMMENTS As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_ENTITY_PHONE
    Public Property PK_SIPESATGOAML_ACT_ENTITY_PHONE As Long
    Public Property FK_ACT_ENTITY_ID As Nullable(Of Long)
    Public Property TPH_CONTACT_TYPE As String
    Public Property TPH_COMMUNICATION_TYPE As String
    Public Property TPH_COUNTRY_PREFIX As String
    Public Property TPH_NUMBER As String
    Public Property TPH_EXTENSION As String
    Public Property COMMENTS As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_DIRECTOR
    Public Property PK_SIPESATGOAML_ACT_DIRECTOR_ID As Long
    Public Property FK_REPORTPARTY_ID As Nullable(Of Long)
    Public Property FK_ACT_ENTITY_ID As Nullable(Of Long)
    Public Property GENDER As String
    Public Property TITLE As String
    Public Property FIRST_NAME As String
    Public Property MIDDLE_NAME As String
    Public Property PREFIX As String
    Public Property LAST_NAME As String
    Public Property BIRTHDATE As Nullable(Of Date)
    Public Property BIRTH_PLACE As String
    Public Property MOTHERS_NAME As String
    Public Property [ALIAS] As String
    Public Property SSN As String
    Public Property PASSPORT_NUMBER As String
    Public Property PASSPORT_COUNTRY As String
    Public Property ID_NUMBER As String
    Public Property NATIONALITY1 As String
    Public Property NATIONALITY2 As String
    Public Property NATIONALITY3 As String
    Public Property RESIDENCE As String
    Public Property EMAIL As String
    Public Property OCCUPATION As String
    Public Property EMPLOYER_NAME As String
    Public Property DECEASED As Nullable(Of Boolean)
    Public Property DECEASED_DATE As Nullable(Of Date)
    Public Property TAX_NUMBER As String
    Public Property TAX_REG_NUMBER As Nullable(Of Boolean)
    Public Property SOURCE_OF_WEALTH As String
    Public Property COMMENT As String
    Public Property ROLE As String
    Public Property EMAIL2 As String
    Public Property EMAIL3 As String
    Public Property EMAIL4 As String
    Public Property EMAIL5 As String
    Public Property FK_SENDER_INFORMATION As Nullable(Of Integer)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_DIRECTOR_ADDRESS
    Public Property PK_SIPESATGOAML_ACT_DIRECTOR_ADDRESS_ID As Long
    Public Property FK_ACT_DIRECTOR_ID As Nullable(Of Long)
    Public Property ADDRESS_Type As String
    Public Property ADDRESS As String
    Public Property TOWN As String
    Public Property CITY As String
    Public Property ZIP As String
    Public Property COUNTRY_CODE As String
    Public Property STATE As String
    Public Property COMMENTS As String
    Public Property ISEMPLOYER As Nullable(Of Boolean)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_DIRECTOR_PHONE
    Public Property PK_SIPESATGOAML_ACT_DIRECTOR_PHONE As Long
    Public Property FK_ACT_DIRECTOR_ID As Nullable(Of Long)
    Public Property TPH_CONTACT_TYPE As String
    Public Property TPH_COMMUNICATION_TYPE As String
    Public Property TPH_COUNTRY_PREFIX As String
    Public Property TPH_NUMBER As String
    Public Property TPH_EXTENSION As String
    Public Property COMMENTS As String
    Public Property ISEMPLOYER As Nullable(Of Boolean)
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class

Public Class SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION
    Public Property PK_ACT_DIRECTOR_IDENTIFICATION_ID As Long
    Public Property FK_ACT_DIRECTOR_ID As Nullable(Of Long)
    Public Property TYPE As String
    Public Property NUMBER As String
    Public Property ISSUED_DATE As Nullable(Of Date)
    Public Property EXPIRY_DATE As Nullable(Of Date)
    Public Property ISSUED_BY As String
    Public Property ISSUED_COUNTRY As String
    Public Property IDENTIFICATION_COMMENT As String
    Public Property FK_REPORT_ID As Nullable(Of Long)
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String
End Class

Public Class SIPESATGoAML_Note_History
    Public Property PK_SIPESATGoAML_Note_History_ID As Long
    Public Property UnikID As String
    Public Property UserID As Nullable(Of Long)
    Public Property UserName As String
    Public Property RoleID As Nullable(Of Long)
    Public Property Status As String
    Public Property Notes As String
    Public Property Active As Nullable(Of Boolean)
    Public Property CreatedBy As String
    Public Property LastUpdateBy As String
    Public Property ApprovedBy As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property ApprovedDate As Nullable(Of Date)
    Public Property Alternateby As String

End Class


#End Region

Public Class SIPESATGoAML_Report_Class

    ' Report
    Public obj_goAML_Report As New SIPESATGOAML_REPORT

    'Activity
    Public list_goAML_Act_ReportPartyType As New List(Of SIPESATGOAML_ACT_REPORTPARTYTYPE)

End Class

Public Class SIPESATGoAML_Activity_Class
    Public obj_goAML_Act_ReportPartyType As New SIPESATGOAML_ACT_REPORTPARTYTYPE

    'Public list_goAML_Act_Account As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACCOUNT)
    'Public list_goAML_Act_acc_Signatory As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGNATORY)
    'Public list_goAML_Act_Acc_sign_Address As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_ADDRESS)
    'Public list_goAML_Act_Acc_sign_Phone As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_SIGN_PHONE)

    'Public list_goAML_Act_Entity_Account As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ENTITY_ACCOUNT)
    'Public list_goAML_Act_Acc_Entity_Address As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_ADDRESS)
    'Public list_goAML_Act_Acc_Entity_Phone As New List(Of SIPESATGoAMLDAL.SIPESATGOAML_ACT_ACC_ENTITY_PHONE)

    Public list_goAML_Activity_Person_Identification As New List(Of SIPESATGOAML_ACTIVITY_PERSON_IDENTIFICATION)

    'Public list_goAML_Act_Acc_Ent_Director As New List(Of NawaDevDAL.goAML_Act_Acc_Ent_Director)
    'Public list_goAML_Act_Acc_Entity_Director_address As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_address)
    'Public list_goAML_Act_Acc_Entity_Director_Phone As New List(Of NawaDevDAL.goAML_Act_Acc_Entity_Director_Phone)

    Public list_goAML_Act_Person As New List(Of SIPESATGOAML_ACT_PERSON)
    Public list_goAML_Act_Person_Phone As New List(Of SIPESATGOAML_ACT_PERSON_PHONE)
    Public list_goAML_Act_Person_Address As New List(Of SIPESATGOAML_ACT_PERSON_ADDRESS)
    Public list_goAML_Act_Person_Relationship As New List(Of SIPESATGOAML_ACT_PERSON_RELATIONSHIP)

    Public list_goAML_Act_Entity As New List(Of SIPESATGOAML_ACT_ENTITY)
    Public list_goAML_Act_Entity_Account As New List(Of SIPESATGOAML_ACT_ENTITY_ACCOUNT)
    Public list_goAML_Act_Entity_Address As New List(Of SIPESATGOAML_ACT_ENTITY_ADDRESS)
    Public list_goAML_Act_Entity_Relationship As New List(Of SIPESATGOAML_ACT_ENTITY_RELATIONSHIP)
    Public list_goAML_Act_Entity_Phone As New List(Of SIPESATGOAML_ACT_ENTITY_PHONE)

    Public list_goAML_Act_Director As New List(Of SIPESATGOAML_ACT_DIRECTOR)
    Public list_goAML_Act_Director_Address As New List(Of SIPESATGOAML_ACT_DIRECTOR_ADDRESS)
    Public list_goAML_Act_Director_Phone As New List(Of SIPESATGOAML_ACT_DIRECTOR_PHONE)
    Public list_goAML_Act_Director_Identification As New List(Of SIPESATGOAML_ACT_DIRECTOR_IDENTIFICATION)
End Class
