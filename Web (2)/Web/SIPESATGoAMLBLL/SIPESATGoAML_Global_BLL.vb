﻿Public Class SIPESATGoAML_Global_BLL

    Shared Function getDataRowByID(strTable As String, strTableKeyField As String, strKey As String) As DataRow
        Try
            Dim strSQL As String = "SELECT TOP 1 * FROM " & strTable & " WHERE " & strTableKeyField & "='" & strKey & "'"
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return drResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function getGlobalParameterValueByID(strID As String) As String
        Try
            Dim strResult As String = Nothing
            Dim drTemp As DataRow = SIPESATGoAML_Global_BLL.getDataRowByID("SIPESATGOAML_GLOBAL_PARAMETER", "PK_GLOBAL_PARAMETER_ID", strID)
            If drTemp IsNot Nothing Then
                strResult = drTemp("ParameterValue")
            End If

            Return strResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function PullDataAsDataRowSingleString(strQuery As String) As DataRow
        Try
            Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery)
            Return drResult
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Function getDataTableByQuery(strSQL As String) As DataTable
        Try
            Dim dtResult As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strSQL)

            Return dtResult
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function
End Class
