﻿Imports System.Data.SqlClient
Imports System.Web
Imports Ext.Net
Imports SiPendarDAL
Public Class SipendarWatchlistBLL

    Shared Function getWatchlistByID(ID As String) As SIPENDAR_WATCHLIST
        Dim objWatchlist As New SIPENDAR_WATCHLIST
        Using objdb As New SiPendarEntities
            objWatchlist = objdb.SIPENDAR_WATCHLIST.Where(Function(x) x.PK_SIPENDAR_WATCHLIST_ID = ID).FirstOrDefault
        End Using
        Return objWatchlist
    End Function


    Public Shared Sub SettingColor(formPanelOld As FormPanel, formPanelNew As FormPanel, unikkeyold As String, unikkeynew As String)
        Try
            Dim objdisplayOld As DisplayField
            Dim objdisplayNew As DisplayField

            Dim listfieldwatchlist As List(Of String) = New List(Of String)
            listfieldwatchlist.Add("PK_SIPENDAR_WATCHLIST_ID")
            listfieldwatchlist.Add("PERIODE")
            listfieldwatchlist.Add("ID_PPATK")
            listfieldwatchlist.Add("KODE_WATCHLIST")
            listfieldwatchlist.Add("JENIS_PELAKU")
            listfieldwatchlist.Add("NAMA_ASLI")
            listfieldwatchlist.Add("PARAMETER_PENCARIAN_NAMA")
            listfieldwatchlist.Add("TEMPAT_LAHIR")
            listfieldwatchlist.Add("TANGGAL_LAHIR")
            listfieldwatchlist.Add("NPWP")
            listfieldwatchlist.Add("KTP")
            listfieldwatchlist.Add("PAS")
            listfieldwatchlist.Add("KITAS")
            listfieldwatchlist.Add("SUKET")
            listfieldwatchlist.Add("SIM")
            listfieldwatchlist.Add("KITAP")
            listfieldwatchlist.Add("KIMS")
            listfieldwatchlist.Add("Watchlist")
            listfieldwatchlist.Add("CreatedBy")
            listfieldwatchlist.Add("LastUpdateDate")

            For Each item As String In listfieldwatchlist
                objdisplayOld = formPanelOld.FindControl(item & unikkeynew)
                objdisplayNew = formPanelNew.FindControl(item & unikkeyold)
                If Not objdisplayOld Is Nothing And Not objdisplayNew Is Nothing Then

                    If objdisplayOld.Text <> objdisplayNew.Text Then
                        objdisplayOld.FieldStyle = "Color:red"
                        objdisplayNew.FieldStyle = "Color:red"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Shared Sub SettingColorGrid(formPanelOld As GridPanel, formPanelNew As GridPanel)
        Try
            Dim objdisplayOld As Column
            Dim objdisplayNew As Column

            Dim listfieldWatchlist As List(Of String) = New List(Of String)
            listfieldWatchlist.Add("PK_MRole_ID")
            listfieldWatchlist.Add("RoleName")


            For Each item As String In listfieldWatchlist
                objdisplayOld = formPanelOld.FindControl(item)
                objdisplayNew = formPanelNew.FindControl(item)
                If Not objdisplayOld Is Nothing And Not objdisplayNew Is Nothing Then

                    If objdisplayOld.Text <> objdisplayNew.Text Then
                        objdisplayOld.StyleSpec = "Color:red"
                        objdisplayNew.StyleSpec = "Color:red"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Shared Sub SaveAddTanpaApproval(objWatchlist As SIPENDAR_WATCHLIST, objModule As NawaDAL.Module)
        Using objDb As New SiPendarEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try
                    With objWatchlist
                        If String.IsNullOrEmpty(.CreatedBy) Then
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .CreatedDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objWatchlist).State = Entity.EntityState.Added
                    objDb.SaveChanges()


                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objWatchlist)

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveAddWithApproval(objWatchlist As SIPENDAR_WATCHLIST, objModule As NawaDAL.Module)
        Using objDb As New SiPendarEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    Dim xmldata As String = NawaBLL.Common.Serialize(objWatchlist)

                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = 0 '---- Module Key ????' -----
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDb.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDb.SaveChanges()

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                    'AuditTrailDetail
                    NawaFramework.CreateAuditTrailDetailAdd(objDb, header.PK_AuditTrail_ID, objWatchlist)

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveEditTanpaApproval(objWatchlist As SIPENDAR_WATCHLIST, objModule As NawaDAL.Module)
        Dim objWatchlist_Old As SIPENDAR_WATCHLIST = getWatchlistByID(objWatchlist.PK_SIPENDAR_WATCHLIST_ID)

        Using objDb As New SiPendarEntities
            Using objTrans As Entity.DbContextTransaction = objDb.Database.BeginTransaction
                Try

                    With objWatchlist
                        If String.IsNullOrEmpty(.LastUpdateBy) Then
                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            .LastUpdateDate = Now
                        End If

                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        .ApprovedDate = Now
                    End With

                    objDb.Entry(objWatchlist).State = Entity.EntityState.Modified
                    objDb.SaveChanges()

                    'AuditTrail
                    Dim header As AuditTrailHeader = NawaFramework.CreateAuditTrail(objDb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                    NawaFramework.CreateAuditTrailDetailEdit(objDb, header.PK_AuditTrail_ID, objWatchlist, objWatchlist_Old)

                    objDb.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Public Shared Sub SaveEditWithApproval(objWatchlist As SIPENDAR_WATCHLIST, objModule As NawaDAL.Module)
        Dim objWatchlist_Old = getWatchlistByID(objWatchlist.PK_SIPENDAR_WATCHLIST_ID)

        Using objdb As New SiPendarEntities
            Using objtrans As Entity.DbContextTransaction = objdb.Database.BeginTransaction
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Dim xmldata As String = NawaBLL.Common.Serialize(objWatchlist)
                    Dim xmldataOld As String = NawaBLL.Common.Serialize(objWatchlist_Old)

                    Dim objModuleApproval As New ModuleApproval
                    With objModuleApproval
                        .ModuleName = objModule.ModuleName
                        .ModuleKey = objWatchlist.PK_SIPENDAR_WATCHLIST_ID
                        .ModuleField = xmldata
                        .ModuleFieldBefore = xmldataOld
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objdb.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objdb.SaveChanges()

                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function Accept(ID As String, strNotes As String) As Boolean
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim notes As String = strNotes 'HttpContext.Current.Session("Sipendar_Watchlist_Upload_Notes").ToString
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    If objApproval.PK_ModuleAction_ID = 7 Then
                        'Dim getmodulefield = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                        'Dim modulefield As String = getmodulefield.ModuleField

                        'Dim ListUpload As List(Of SiPendarDAL.SIPENDAR_WATCHLIST) = NawaBLL.Common.Deserialize(modulefield, GetType(List(Of SiPendarDAL.SIPENDAR_WATCHLIST)))

                        'For Each item In ListUpload
                        '    Dim cekdata = objdb.SIPENDAR_WATCHLIST.Where(Function(x) x.ID_PPATK = item.ID_PPATK).FirstOrDefault
                        '    If cekdata IsNot Nothing Then
                        '        item.PK_SIPENDAR_WATCHLIST_ID = cekdata.PK_SIPENDAR_WATCHLIST_ID
                        '        SaveEditTanpaApproval(item, objModule)
                        '        CreateApprovalHistory(objApproval, 0, 1, notes, objdb)
                        '    Else
                        '        SaveAddTanpaApproval(item, objModule)
                        '        CreateApprovalHistory(objApproval, 0, 2, notes, objdb)
                        '    End If

                        'Next

                        'Pakai Query saja lebih cepat
                        Dim strQuery As String = ""

                        '14-Des-2021 Penambahan Field Submission_Type
                        'Save objData to Table SIPENDAR_WATCHLIST_Upload
                        strQuery = "INSERT INTO SIPENDAR_WATCHLIST "
                        strQuery += "("
                        strQuery += " PERIODE,"
                        strQuery += " ID_PPATK,"
                        strQuery += " KODE_WATCHLIST,"
                        strQuery += " JENIS_PELAKU,"
                        strQuery += " NAMA_ASLI,"
                        strQuery += " PARAMETER_PENCARIAN_NAMA,"
                        strQuery += " TEMPAT_LAHIR,"
                        strQuery += " TANGGAL_LAHIR,"
                        strQuery += " NPWP,"
                        strQuery += " KTP,"
                        strQuery += " PAS,"
                        strQuery += " KITAS,"
                        strQuery += " SUKET,"
                        strQuery += " SIM,"
                        strQuery += " KITAP,"
                        strQuery += " KIMS,"
                        strQuery += " Watchlist,"
                        strQuery += " Submission_Type,"
                        strQuery += " [Active],"
                        strQuery += " CreatedBy,"
                        strQuery += " CreatedDate"
                        strQuery += ") "
                        strQuery += "SELECT "
                        strQuery += " PERIODE,"
                        strQuery += " ID_PPATK,"
                        strQuery += " KODE_WATCHLIST,"
                        strQuery += " JENIS_PELAKU,"
                        strQuery += " NAMA_ASLI,"
                        strQuery += " PARAMETER_PENCARIAN_NAMA,"
                        strQuery += " TEMPAT_LAHIR,"
                        strQuery += " TANGGAL_LAHIR,"
                        strQuery += " NPWP,"
                        strQuery += " KTP,"
                        strQuery += " PAS,"
                        strQuery += " KITAS,"
                        strQuery += " SUKET,"
                        strQuery += " SIM,"
                        strQuery += " KITAP,"
                        strQuery += " KIMS,"
                        strQuery += " Watchlist,"
                        strQuery += " Submission_Type,"
                        strQuery += " 1,"
                        strQuery += " '" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                        strQuery += " GETDATE()"
                        strQuery += " FROM SIPENDAR_WATCHLIST_Upload_Approval"
                        strQuery += " WHERE FK_ModuleApproval_ID=" & objApproval.PK_ModuleApproval_ID & " "
                        strQuery += " AND nawa_Action='Insert'"
                        strQuery += " ORDER BY ID_PPATK ASC"

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        '14-Des-2021 Penambahan Field Submission_Type
                        strQuery = "UPDATE SIPENDAR_WATCHLIST SET"
                        strQuery += " PERIODE = b.PERIODE,"
                        strQuery += " ID_PPATK = b.ID_PPATK,"
                        strQuery += " KODE_WATCHLIST = b.KODE_WATCHLIST,"
                        strQuery += " JENIS_PELAKU = b.JENIS_PELAKU,"
                        strQuery += " NAMA_ASLI = b.NAMA_ASLI,"
                        strQuery += " PARAMETER_PENCARIAN_NAMA = b.PARAMETER_PENCARIAN_NAMA,"
                        strQuery += " TEMPAT_LAHIR = b.TEMPAT_LAHIR,"
                        strQuery += " TANGGAL_LAHIR = b.TANGGAL_LAHIR,"
                        strQuery += " NPWP = b.NPWP,"
                        strQuery += " KTP = b.KTP,"
                        strQuery += " PAS = b.PAS,"
                        strQuery += " KITAS = b.KITAS,"
                        strQuery += " SUKET = b.SUKET,"
                        strQuery += " SIM = b.SIM,"
                        strQuery += " KITAP = b.KITAP,"
                        strQuery += " KIMS = b.KIMS,"
                        strQuery += " Watchlist = b.Watchlist,"
                        strQuery += " Submission_Type = b.Submission_Type,"
                        strQuery += " LastUpdateBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "',"
                        strQuery += " LastUpdateDate = GETDATE()"
                        strQuery += " FROM SIPENDAR_WATCHLIST a"
                        strQuery += " JOIN SIPENDAR_WATCHLIST_Upload_Approval b"
                        strQuery += " ON a.ID_PPATK = b.ID_PPATK"
                        strQuery += " and a.Submission_Type = b.Submission_Type"
                        strQuery += " WHERE b.FK_ModuleApproval_ID=" & objApproval.PK_ModuleApproval_ID & " "
                        strQuery += " AND b.nawa_Action='Update'"

                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        '21-June-2022 Riyan sync sipendar watchlist to AML Watchlist
                        Dim objParamAPI(0) As SqlParameter
                        objParamAPI(0) = New SqlParameter
                        objParamAPI(0).ParameterName = "@UserID"
                        objParamAPI(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

                        NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_AfterSave", objParamAPI)
                        'End sync sipendar watchlist to AML Watchlist

                        'Delete data dari _Upload_Approval
                        strQuery = "DELETE FROM SIPENDAR_WATCHLIST_Upload_Approval "
                        strQuery += " where FK_ModuleApproval_ID=" & objApproval.PK_ModuleApproval_ID
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                        strQuery = "update SIPENDAR_WATCHLIST_Upload_Header "
                        strQuery += " set Data_Status = 'Data Accepted', "
                        strQuery += " LastUpdateBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "', "
                        strQuery += " LastUpdateDate = GETDATE(), "
                        strQuery += " ApprovedBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "', "
                        strQuery += " ApprovedDate = GETDATE() "
                        strQuery += " where PK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & objApproval.ModuleKey
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                    Else
                        Select Case objApproval.PK_ModuleAction_ID
                            Case NawaBLL.Common.ModuleActionEnum.Insert
                                Dim objData As SIPENDAR_WATCHLIST = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_WATCHLIST))
                                SaveAddTanpaApproval(objData, objModule)
                            Case NawaBLL.Common.ModuleActionEnum.Update
                                Dim objData As SIPENDAR_WATCHLIST = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_WATCHLIST))
                                SaveEditTanpaApproval(objData, objModule)
                        End Select

                        '21-June-2022 Riyan sync sipendar watchlist to AML Watchlist
                        Dim objParamAPI(0) As SqlParameter
                        objParamAPI(0) = New SqlParameter
                        objParamAPI(0).ParameterName = "@UserID"
                        objParamAPI(0).Value = NawaBLL.Common.SessionCurrentUser.UserID

                        NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_WATCHLIST_AfterSave", objParamAPI)
                        'End sync sipendar watchlist to AML Watchlist
                    End If

                    'Create Approval History
                    CreateApprovalHistory(objApproval, 0, objApproval.PK_ModuleAction_ID, notes, objdb)

                    'Hapus data di Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted

                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Function Reject(ID As String, strNotes As String) As Boolean
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim notes As String = strNotes     'HttpContext.Current.Session("Sipendar_Watchlist_Upload_Notes").ToString
                    Dim objApproval As ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As SiPendarDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    '30-Sep-2021 : pindahkan module field dan module field before dari module approval ke approval history as-is
                    CreateApprovalHistory(objApproval, 1, objApproval.PK_ModuleAction_ID, notes, objdb)

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    Dim strquery As String = ""
                    'Delete data dari _Upload_Approval
                    strquery = "DELETE FROM SIPENDAR_WATCHLIST_Upload_Approval "
                    strquery += " where FK_ModuleApproval_ID=" & objApproval.PK_ModuleApproval_ID
                    NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)

                    If objApproval.PK_ModuleAction_ID = 7 Then
                        strquery = "update SIPENDAR_WATCHLIST_Upload_Header "
                        strquery += " set Data_Status = 'Data Rejected', "
                        strquery += " LastUpdateBy = '" & NawaBLL.Common.SessionCurrentUser.UserID & "', "
                        strquery += " LastUpdateDate = GETDATE() "
                        strquery += " where PK_SIPENDAR_WATCHLIST_Upload_Header_ID = " & objApproval.ModuleKey
                        NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strquery, Nothing)
                    End If
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)
        Try
            Dim objWatchlistClass As New SIPENDAR_WATCHLIST
            objWatchlistClass = NawaBLL.Common.Deserialize(objdata, GetType(SIPENDAR_WATCHLIST))
            Dim objWatchlist As New SIPENDAR_WATCHLIST
            objWatchlist = objWatchlistClass
            Using db As SiPendarEntities = New SiPendarEntities
                If Not objWatchlist Is Nothing Then
                    Dim strunik As String = unikkey
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Watchlist ID", "PK_SIPENDAR_WATCHLIST_ID" & strunik, objWatchlist.PK_SIPENDAR_WATCHLIST_ID)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Periode", "PERIODE" & strunik, objWatchlist.PERIODE)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID PPATK", "ID_PPATK" & strunik, objWatchlist.ID_PPATK)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kode Watchllist", "KODE_WATCHLIST" & strunik, objWatchlist.KODE_WATCHLIST)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Jenis Pelaku", "JENIS_PELAKU" & strunik, objWatchlist.JENIS_PELAKU)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Asli", "NAMA_ASLI" & strunik, objWatchlist.NAMA_ASLI)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Parameter Pencarian", "PARAMETER_PENCARIAN_NAMA" & strunik, objWatchlist.PARAMETER_PENCARIAN_NAMA)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "TEMPAT_LAHIR" & strunik, objWatchlist.TEMPAT_LAHIR)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "TANGGAL_LAHIR" & strunik, objWatchlist.TANGGAL_LAHIR)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "NPWP" & strunik, objWatchlist.NPWP)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "KTP", "KTP" & strunik, objWatchlist.KTP)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "PAS", "PAS" & strunik, objWatchlist.PAS)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "KITAS", "KITAS" & strunik, objWatchlist.KITAS)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "SUKET", "SUKET" & strunik, objWatchlist.SUKET)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "SIM", "SIM" & strunik, objWatchlist.SIM)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "KITAP", "KITAP" & strunik, objWatchlist.KITAP)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "KIMS", "KIMS" & strunik, objWatchlist.KIMS)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Watchlist", "Watchlist" & strunik, objWatchlist.Watchlist)
                    '14-Des-2021 Penambahan Field Submission_Type
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Submission Type", "Submission_Type" & strunik, objWatchlist.Submission_Type)
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CreatedBy", "CreatedBy" & strunik, objWatchlist.CreatedBy)
                    'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "LastUpdateDate", "LastUpdateDate" & strunik, objWatchlist.LastUpdateDate)
                    If objWatchlist.LastUpdateDate IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Update Date", "LastUpdateDate" & strunik, objWatchlist.LastUpdateDate)
                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Last Update Date", "LastUpdateDate" & strunik, Nothing)
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New SiPendarEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function

    Shared Sub CreateApprovalHistory(ModuleApproval As ModuleApproval, actionApproval As Integer, actionForm As Integer, notes As String, objdb As SiPendarDAL.SiPendarEntities)
        Dim objHistory As New SiPendarDAL.SIPENDAR_WATCHLIST_APPROVAL_HISTORY

        Dim approvalH As String
        Dim formH As String

        Select Case actionApproval
            Case 0
                approvalH = "Accept"
            Case 1
                approvalH = "Reject"
            Case 2
                approvalH = "Request"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Select Case actionForm
            Case 1
                formH = "Add:<br/> "
            Case 2
                formH = "Edit:<br/> "
            Case 3
                formH = "Delete:<br/> "
            Case 7
                formH = "Import:<br/>"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        objHistory.UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
        objHistory.ModuleField = ModuleApproval.ModuleField
        objHistory.ModuleFieldBefore = ModuleApproval.ModuleFieldBefore
        objHistory.ModuleKey = ModuleApproval.ModuleKey
        objHistory.ModuleName = ModuleApproval.ModuleName
        objHistory.FK_MRole_ID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
        objHistory.PK_ModuleAction_ID = ModuleApproval.PK_ModuleAction_ID
        objHistory.Status = approvalH
        objHistory.Notes = formH & notes
        objHistory.Active = 1
        objHistory.CreatedBy = ModuleApproval.CreatedBy
        objHistory.CreatedDate = ModuleApproval.CreatedDate

        If actionApproval = 0 Then     'Accept
            objHistory.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
            objHistory.ApprovedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
        ElseIf actionApproval = 1 Then    'Reject
            objHistory.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
            objHistory.LastUpdateDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
        End If

        objdb.Entry(objHistory).State = Entity.EntityState.Added
        objdb.SaveChanges()
    End Sub

End Class
