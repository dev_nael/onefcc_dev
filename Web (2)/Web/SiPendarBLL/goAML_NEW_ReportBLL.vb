﻿Imports System.Data.SqlClient
Imports System.Text
Imports NawaBLL
Imports System.Text.RegularExpressions
Imports SiPendarDAL
Imports NawaDAL

Public Class goAML_NEW_ReportBLL
    Enum actionApproval
        Accept = 0
        Reject = 1
        Request = 2
    End Enum

    Enum actionForm
        Add = 0
        Edit = 1
        Delete = 2
        Approval = 3
    End Enum

    Shared Function SendEmailModuleApproval(PkModuleID As Long, UnikID As Long, inttemplateid As Integer) As Boolean
        Try
            Dim objparam(1) As System.Data.SqlClient.SqlParameter

            objparam(0) = New SqlClient.SqlParameter
            objparam(0).ParameterName = "@PkmoduleApprovalid"
            objparam(0).SqlDbType = SqlDbType.BigInt
            objparam(0).Value = PkModuleID

            objparam(1) = New SqlClient.SqlParameter
            objparam(1).ParameterName = "@inttemplateid"
            objparam(1).SqlDbType = SqlDbType.Int
            objparam(1).Value = inttemplateid

            Dim dt As Data.DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_InserEmailSchedulerModuleApproval", objparam)
            For Each item As DataRow In dt.Rows
                Dim dtTablePrimary As Data.DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT eta.NamaTable, eta.QueryData, eta.FieldUnikPrimaryTable  FROM EmailTemplateAdditional eta WHERE eta.FK_EmailTableType_ID=1 AND eta.FK_EmailTemplate_ID=" & inttemplateid, Nothing)

                For Each item1 As DataRow In dtTablePrimary.Rows

                    Dim strqueryx As String = System.Net.WebUtility.HtmlDecode(item1("QueryData"))
                    strqueryx = strqueryx.Replace("@ID", UnikID)
                    Using objDb As NawaDataEntities = New NawaDataEntities

                        Dim objparamtable As New Data.SqlClient.SqlParameter("@tablename", item1("NamaTable"))
                        Dim objparamquerydata As New Data.SqlClient.SqlParameter("@querydata", strqueryx)

                        objDb.Database.ExecuteSqlCommand("exec usp_CreateTableEmailPrimary @tablename,@querydata", objparamtable, objparamquerydata)

                    End Using


                    Dim sql As String
                    sql = "INSERT INTO EmailTemplateSchedulerDetail "
                    sql += " ( "
                    sql += " 	 "
                    sql += " 	FK_EmailTEmplateScheduler_ID, "
                    sql += " 	UnikFieldTablePrimary, "
                    sql += " 	EmailTo, "
                    sql += " 	EmailCC, "
                    sql += " 	EmailBCC, "
                    sql += " 	EmailSubject, "
                    sql += " 	EmailBody, "
                    sql += " 	ProcessDate, "
                    sql += " 	SendEmailDate, "
                    sql += " 	FK_EmailStatus_ID, "
                    sql += " 	ErrorMessage, "
                    sql += " 	retrycount "
                    sql += " ) "
                    sql += " select  "
                    sql += " '" & item("PK_EmailTemplateScheduler_ID") & "' ,"
                    sql += " " & item1("FieldUnikPrimaryTable") & " ,"
                    sql += " '" & item("EmailTo") & "' ,"
                    sql += " '" & item("EmailCC") & "' ,"
                    sql += " '" & item("EmailBCC") & "' ,"
                    sql += " '" & item("EmailSubject") & "' ,"
                    sql += " '" & item("EmailBody") & "' ,"
                    sql += " '" & CDate(item("ProcessDate")).ToString("yyyy-MM-dd HH:mm:ss") & "' ,"
                    sql += " null ,"
                    sql += " '1' ,"
                    sql += " '' ,"
                    sql += " '0' "
                    sql += " from __" & item1("NamaTable")


                    SQLHelper.ExecuteScalar(SQLHelper.strConnectionString, CommandType.Text, sql, Nothing)

                    'buat table detail dulu untuk setiap master

                    Dim dtschedulerdetail As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM EmailTemplateSchedulerDetail WHERE FK_EmailTEmplateScheduler_ID=" & item("PK_EmailTemplateScheduler_ID"), Nothing)

                    Dim dtemaildetail As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT etd.Replacer , SUBSTRING(etd.FieldReplacer,1,1)+'__'+SUBSTRING(etd.FieldReplacer,2,LEN(etd.FieldReplacer)-1) AS FieldReplacer  FROM EmailTemplateDetail etd WHERE etd.FK_EmailTemplate_ID=" & item("PK_EmailTemplate_ID"), Nothing)

                    For Each rowschedulerdetail As DataRow In dtschedulerdetail.Rows

                        Dim dtTableAdditional As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT eta.NamaTable, eta.QueryData, eta.FieldUnikPrimaryTable  FROM EmailTemplateAdditional eta WHERE eta.FK_EmailTableType_ID=2 AND eta.FK_EmailTemplate_ID=" & item("PK_EmailTemplate_ID"), Nothing)

                        For Each item2 As DataRow In dtTableAdditional.Rows


                            'generate table additional detail
                            Using objDb As NawaDataEntities = New NawaDataEntities
                                Dim strquery As String = System.Net.WebUtility.HtmlDecode(item2("QueryData"))
                                strquery = strquery.Replace("@ID", "'" & rowschedulerdetail("UnikFieldTablePrimary") & "'")

                                Dim objparamtable As New Data.SqlClient.SqlParameter("@tablename", item2("NamaTable"))
                                Dim objparamquerydata As New Data.SqlClient.SqlParameter("@querydata", strquery)
                                objDb.Database.ExecuteSqlCommand("exec usp_CreateTableEmailPrimary @tablename,@querydata", objparamtable, objparamquerydata)

                            End Using

                        Next

                        'replace isi tabledetail
                        For Each rowreplacer As DataRow In dtemaildetail.Rows

                            Using objDb As NawaDataEntities = New NawaDataEntities
                                Dim objparampk As New Data.SqlClient.SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", rowschedulerdetail("PK_EmailTemplateSchedulerDetail_ID"))
                                Dim objparamreplacer As New Data.SqlClient.SqlParameter("@Replacer", "'" & rowreplacer("Replacer") & "'")
                                Dim objparamfieldreplacer As New Data.SqlClient.SqlParameter("@FieldReplacer", rowreplacer("FieldReplacer"))
                                Dim objparampkemailtemplate As New Data.SqlClient.SqlParameter("@pk_emailitemplate_id", item("PK_EmailTemplate_ID"))

                                objDb.Database.ExecuteSqlCommand("exec usp_replaceEmailSchedulerDetail @PK_EmailTemplateSchedulerDetail_ID,@Replacer,@FieldReplacer,@pk_emailitemplate_id", objparampk, objparamreplacer, objparamfieldreplacer, objparampkemailtemplate)

                            End Using

                        Next
                        'update status jadi queue untuk EmailTemplateScheduler dan EmailTemplateSchedulerdetail
                    Next
                Next
                Using objDb As NawaDataEntities = New NawaDataEntities
                    Dim objparampk As New Data.SqlClient.SqlParameter("@PK_EmailTemplateScheduler_ID", item("PK_EmailTemplateScheduler_ID"))
                    objDb.Database.ExecuteSqlCommand("exec usp_updateEmailStatusScheduler @PK_EmailTemplateScheduler_ID", objparampk)
                End Using
            Next
        Catch ex As Exception
            Throw
            Return False
        End Try

        Return True
    End Function

    Shared Sub CreateNotes(ModuleID As Integer, ModuleLabel As String, actionApproval As Integer, actionForm As Integer, unikID As String, notes As String)
        Dim objHistory As New SiPendarDAL.goAML_Module_Note_History

        Dim approvalH As String
        Dim formH As String

        Select Case actionApproval
            Case 0
                approvalH = "Accept"
            Case 1
                approvalH = "Reject"
            Case 2
                approvalH = "Request"
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Select Case actionForm
            Case 0
                formH = "Add:<br/> "
            Case 1
                formH = "Edit:<br/> "
            Case 2
                formH = "Delete:<br/> "
            Case 3
                formH = ""
            Case Else
                Throw New ApplicationException("Action not found")
        End Select

        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            objHistory.FK_Module_ID = ModuleID
            objHistory.UnikID = unikID
            objHistory.UserID = NawaBLL.Common.SessionCurrentUser.PK_MUser_ID
            objHistory.UserName = NawaBLL.Common.SessionCurrentUser.UserName
            objHistory.RoleID = NawaBLL.Common.SessionCurrentUser.FK_MRole_ID
            objHistory.Status = approvalH
            objHistory.Notes = formH & notes
            objHistory.Active = 1
            objHistory.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
            objHistory.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")

            objDb.Entry(objHistory).State = Entity.EntityState.Added
            objDb.SaveChanges()
        End Using
    End Sub

    Shared Function get_ListNotesHistory(moduleID As Long, unikID As String) As List(Of SiPendarDAL.goAML_Module_Note_History)
        Dim objHistory As New List(Of SiPendarDAL.goAML_Module_Note_History)

        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            objHistory = objDb.goAML_Module_Note_History.Where(Function(x) x.FK_Module_ID = moduleID And x.UnikID = unikID).OrderByDescending(Function(x) x.PK_goAML_Module_Note_History_ID).ToList()
        End Using

        Return objHistory
    End Function

    Shared Function GetListTrnAccountSignatoryFromRefByAccountNo(strAccountNo As String, intFromTo As Integer) As List(Of SiPendarBLL.SignatoryClass)
        Try
            Dim listSignatoryAccountFromClass As New List(Of SiPendarBLL.SignatoryClass)

            Using objDB As New SiPendarEntities

                Dim objListgoAML_Ref_Account_Signatory = goAML_AccountBLL.GetAccountSignatory(strAccountNo)
                Dim intCountSignatory As Integer = 0
                'start daniel20210719
                Dim intCount As Integer = 0
                'end daniel20210719

                If objListgoAML_Ref_Account_Signatory IsNot Nothing Then
                    For Each item In objListgoAML_Ref_Account_Signatory
                        'start daniel20210719
                        intCountSignatory = intCountSignatory - 1
                        'end daniel20210719

                        If item.isCustomer Then     'Jika Sigantory adalah Nasabah
                            'Counter for List
                            'start daniel20210719
                            'Dim intCount As Integer = 0
                            'end daniel20210719

                            'Signatory Class Objects
                            Dim objSignatoryClass As New SiPendarBLL.SignatoryClass
                            Dim objSignatory As New SIPENDAR_Trn_acc_Signatory
                            Dim listAddressSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
                            Dim listPhoneSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
                            Dim listAddressEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
                            Dim listPhoneEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
                            Dim listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

                            'Get objSignatory
                            Dim objCIFPerson = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = item.FK_CIF_Person_ID).FirstOrDefault
                            Dim objAccount = objDB.goAML_Ref_Account.Where(Function(x) x.Account_No = strAccountNo).FirstOrDefault
                            If objCIFPerson IsNot Nothing Then
                                'start daniel20210719
                                'intCountSignatory = 0
                                'end daniel20210719
                                With objSignatory
                                    'start daniel20210719
                                    .PK_goAML_Trn_acc_Signatory_ID = intCountSignatory
                                    If objAccount IsNot Nothing Then
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                    End If
                                    'end daniel20210719
                                    .FK_From_Or_To = intFromTo

                                    'Load Person CIF Detail
                                    If objCIFPerson IsNot Nothing Then
                                        .Gender = objCIFPerson.INDV_Gender
                                        .Title = objCIFPerson.INDV_Title
                                        .First_Name = objCIFPerson.INDV_First_name
                                        .Middle_Name = objCIFPerson.INDV_Middle_name
                                        .Prefix = objCIFPerson.INDV_Prefix
                                        .Last_Name = objCIFPerson.INDV_Last_Name
                                        .BirthDate = objCIFPerson.INDV_BirthDate
                                        .Birth_Place = objCIFPerson.INDV_Birth_Place
                                        .Mothers_Name = objCIFPerson.INDV_Mothers_Name
                                        .Alias = objCIFPerson.INDV_Alias
                                        .SSN = objCIFPerson.INDV_SSN
                                        .Passport_Number = objCIFPerson.INDV_Passport_Number
                                        .Passport_Country = objCIFPerson.INDV_Passport_Country
                                        .Id_Number = objCIFPerson.INDV_ID_Number
                                        .Nationality1 = objCIFPerson.INDV_Nationality1
                                        .Nationality2 = objCIFPerson.INDV_Nationality2
                                        .Nationality3 = objCIFPerson.INDV_Nationality3
                                        .Residence = objCIFPerson.INDV_Residence
                                        .Email = objCIFPerson.INDV_Email
                                        .email2 = objCIFPerson.INDV_Email2
                                        .email3 = objCIFPerson.INDV_Email3
                                        .email4 = objCIFPerson.INDV_Email4
                                        .email5 = objCIFPerson.INDV_Email5
                                        .Occupation = objCIFPerson.INDV_Occupation
                                        .Employer_Name = objCIFPerson.INDV_Employer_Name
                                        .Deceased = objCIFPerson.INDV_Deceased
                                        .Deceased_Date = objCIFPerson.INDV_Deceased_Date
                                        .Tax_Number = objCIFPerson.INDV_Tax_Number
                                        .Tax_Reg_Number = objCIFPerson.INDV_Tax_Reg_Number
                                        .Source_Of_Wealth = objCIFPerson.INDV_Source_of_Wealth
                                        .Comment = objCIFPerson.INDV_Comments
                                    End If

                                    'Signatory Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddress = SiPendarBLL.goAML_CustomerBLL.GetListgoAML_Ref_AddressByPKID(objCIFPerson.PK_Customer_ID)
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryAddress As New SIPENDAR_Trn_Acc_sign_Address
                                            With objNewSignatoryAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Acc_Entity_Address_ID = intCount
                                                'end daniel20210719
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 0
                                            End With

                                            listAddressSignatory.Add(objNewSignatoryAddress)
                                        Next
                                    End If

                                    'Signatory Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhone = SiPendarBLL.goAML_CustomerBLL.GetListgoAML_Ref_PhoneByPKID(objCIFPerson.PK_Customer_ID)
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryPhone As New SIPENDAR_trn_acc_sign_Phone
                                            With objNewSignatoryPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_acc_sign_Phone = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objPhone.FK_for_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 0
                                            End With

                                            listPhoneSignatory.Add(objNewSignatoryPhone)
                                        Next
                                    End If

                                    'Signatory Employer Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddressEmployer = SiPendarBLL.goAML_CustomerBLL.GetEmployerAddresses(objCIFPerson.PK_Customer_ID)
                                    If objListAddressEmployer IsNot Nothing AndAlso objListAddressEmployer.Count > 0 Then
                                        For Each objAddress In objListAddressEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryAddress As New SIPENDAR_Trn_Acc_sign_Address
                                            With objNewSignatoryAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Acc_Entity_Address_ID = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objAddress.FK_To_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 1
                                            End With

                                            listAddressEmployerSignatory.Add(objNewSignatoryAddress)
                                        Next
                                    End If

                                    'Signatory Employer Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhoneEmployer = SiPendarBLL.goAML_CustomerBLL.GetEmployerPhones(objCIFPerson.PK_Customer_ID)
                                    If objListPhoneEmployer IsNot Nothing AndAlso objListPhoneEmployer.Count > 0 Then
                                        For Each objPhone In objListPhoneEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryPhone As New SIPENDAR_trn_acc_sign_Phone
                                            With objNewSignatoryPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_acc_sign_Phone = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objPhone.FK_for_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 1
                                            End With

                                            listPhoneEmployerSignatory.Add(objNewSignatoryPhone)
                                        Next
                                    End If

                                    'Signatory Identification(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListIdentification = SiPendarBLL.goAML_CustomerBLL.GetCustomerIdentifications(objCIFPerson.PK_Customer_ID)
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification In objListIdentification
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryIdentification As New SIPENDAR_Transaction_Person_Identification
                                            With objNewSignatoryIdentification
                                                'start daniel20210719
                                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                                .FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                'end daniel20210719
                                                .FK_Person_Type = 2     '1 Person/Customer, 2 signatory, 3 conductor, 4 director
                                                .from_or_To_Type = intFromTo
                                                .Type = objIdentification.Type
                                                .Number = objIdentification.Number
                                                .Issue_Date = objIdentification.Issue_Date
                                                .Expiry_Date = objIdentification.Expiry_Date
                                                .Issued_By = objIdentification.Issued_By
                                                .Issued_Country = objIdentification.Issued_Country
                                                .Identification_Comment = objIdentification.Identification_Comment
                                            End With

                                            listIdentification.Add(objNewSignatoryIdentification)
                                        Next
                                    End If

                                    .isPrimary = item.isPrimary
                                    .role = item.Role
                                    .Active = 1
                                End With
                            End If

                            'Populate to Signatory Class
                            objSignatoryClass.objSignatory = objSignatory
                            objSignatoryClass.listAddressSignatory = listAddressSignatory
                            objSignatoryClass.listPhoneSignatory = listPhoneSignatory
                            objSignatoryClass.listAddressEmployerSignatory = listAddressEmployerSignatory
                            objSignatoryClass.listPhoneEmployerSignatory = listPhoneEmployerSignatory
                            objSignatoryClass.listIdentification = listIdentification

                            'Add to List of Signatory
                            listSignatoryAccountFromClass.Add(objSignatoryClass)

                        Else        '====== SIGNATORY NON NASABAH ======

                            'Counter for List
                            'start daniel20210719
                            'Dim intCount As Integer = 0
                            'end daniel20210719

                            'Signatory Class Objects
                            Dim objSignatoryClass As New SiPendarBLL.SignatoryClass
                            Dim objSignatory As New SIPENDAR_Trn_acc_Signatory
                            Dim listAddressSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
                            Dim listPhoneSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
                            Dim listAddressEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
                            Dim listPhoneEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
                            Dim listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

                            'Get objSignatory
                            Dim objWICPerson = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = item.WIC_No).FirstOrDefault
                            Dim objAccount = objDB.goAML_Ref_Account.Where(Function(x) x.Account_No = strAccountNo).FirstOrDefault
                            If objWICPerson IsNot Nothing Then
                                'start daniel20210719
                                'intCountSignatory = 0
                                'end daniel20210719
                                With objSignatory
                                    'start daniel20210719
                                    .PK_goAML_Trn_acc_Signatory_ID = intCountSignatory
                                    If objAccount IsNot Nothing Then
                                        .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                    End If
                                    'end daniel20210719
                                    .FK_From_Or_To = intFromTo

                                    'Load Person WIC Detail
                                    If objWICPerson IsNot Nothing Then
                                        .Gender = objWICPerson.INDV_Gender
                                        .Title = objWICPerson.INDV_Title
                                        .First_Name = objWICPerson.INDV_First_Name
                                        .Middle_Name = objWICPerson.INDV_Middle_Name
                                        .Prefix = objWICPerson.INDV_Prefix
                                        .Last_Name = objWICPerson.INDV_Last_Name
                                        .BirthDate = objWICPerson.INDV_BirthDate
                                        .Birth_Place = objWICPerson.INDV_Birth_Place
                                        .Mothers_Name = objWICPerson.INDV_Mothers_Name
                                        .Alias = objWICPerson.INDV_Alias
                                        .SSN = objWICPerson.INDV_SSN
                                        .Passport_Number = objWICPerson.INDV_Passport_Number
                                        .Passport_Country = objWICPerson.INDV_Passport_Country
                                        .Id_Number = objWICPerson.INDV_ID_Number
                                        .Nationality1 = objWICPerson.INDV_Nationality1
                                        .Nationality2 = objWICPerson.INDV_Nationality2
                                        .Nationality3 = objWICPerson.INDV_Nationality3
                                        .Residence = objWICPerson.INDV_Residence
                                        .Email = objWICPerson.INDV_Email
                                        .email2 = objWICPerson.INDV_Email2
                                        .email3 = objWICPerson.INDV_Email3
                                        .email4 = objWICPerson.INDV_Email4
                                        .email5 = objWICPerson.INDV_Email5
                                        .Occupation = objWICPerson.INDV_Occupation
                                        .Employer_Name = objWICPerson.INDV_Employer_Name
                                        .Deceased = objWICPerson.INDV_Deceased
                                        .Deceased_Date = objWICPerson.INDV_Deceased_Date
                                        .Tax_Number = objWICPerson.INDV_Tax_Number
                                        .Tax_Reg_Number = objWICPerson.INDV_Tax_Reg_Number
                                        .Source_Of_Wealth = objWICPerson.INDV_SumberDana
                                        .Comment = objWICPerson.INDV_Comment
                                    End If

                                    'Signatory Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 3 And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryAddress As New SIPENDAR_Trn_Acc_sign_Address
                                            With objNewSignatoryAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Acc_Entity_Address_ID = intCount
                                                'end daniel20210719
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 0
                                            End With

                                            listAddressSignatory.Add(objNewSignatoryAddress)
                                        Next
                                    End If

                                    'Signatory Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 3 And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryPhone As New SIPENDAR_trn_acc_sign_Phone
                                            With objNewSignatoryPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_acc_sign_Phone = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objPhone.FK_for_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 0
                                            End With

                                            listPhoneSignatory.Add(objNewSignatoryPhone)
                                        Next
                                    End If

                                    'Signatory Employer Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddressEmployer = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 10 And x.FK_To_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                    If objListAddressEmployer IsNot Nothing AndAlso objListAddressEmployer.Count > 0 Then
                                        For Each objAddress In objListAddressEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryAddress As New SIPENDAR_Trn_Acc_sign_Address
                                            With objNewSignatoryAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Acc_Entity_Address_ID = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objAddress.FK_To_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 1
                                            End With

                                            listAddressEmployerSignatory.Add(objNewSignatoryAddress)
                                        Next
                                    End If

                                    'Signatory Employer Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhoneEmployer = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 10 And x.FK_for_Table_ID = objWICPerson.PK_Customer_ID).ToList
                                    If objListPhoneEmployer IsNot Nothing AndAlso objListPhoneEmployer.Count > 0 Then
                                        For Each objPhone In objListPhoneEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryPhone As New SIPENDAR_trn_acc_sign_Phone
                                            With objNewSignatoryPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_acc_sign_Phone = intCount
                                                'end daniel20210719
                                                '.FK_Trn_Acc_Entity = objPhone.FK_for_Table_ID
                                                .FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 1
                                            End With

                                            listPhoneEmployerSignatory.Add(objNewSignatoryPhone)
                                        Next
                                    End If

                                    'Signatory Identification(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 8 And x.FK_Person_ID = objWICPerson.PK_Customer_ID)
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification In objListIdentification
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewSignatoryIdentification As New SIPENDAR_Transaction_Person_Identification
                                            With objNewSignatoryIdentification
                                                'start daniel20210719
                                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                                .FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID
                                                'end daniel20210719
                                                .FK_Person_Type = 2     '1 Person/Customer, 2 signatory, 3 conductor, 4 director
                                                .from_or_To_Type = intFromTo
                                                .Type = objIdentification.Type
                                                .Number = objIdentification.Number
                                                .Issue_Date = objIdentification.Issue_Date
                                                .Expiry_Date = objIdentification.Expiry_Date
                                                .Issued_By = objIdentification.Issued_By
                                                .Issued_Country = objIdentification.Issued_Country
                                                .Identification_Comment = objIdentification.Identification_Comment
                                            End With

                                            listIdentification.Add(objNewSignatoryIdentification)
                                        Next
                                    End If

                                    .isPrimary = item.isPrimary
                                    .role = item.Role
                                    .Active = 1
                                End With
                            End If

                            'Populate to Signatory Class
                            objSignatoryClass.objSignatory = objSignatory
                            objSignatoryClass.listAddressSignatory = listAddressSignatory
                            objSignatoryClass.listPhoneSignatory = listPhoneSignatory
                            objSignatoryClass.listAddressEmployerSignatory = listAddressEmployerSignatory
                            objSignatoryClass.listPhoneEmployerSignatory = listPhoneEmployerSignatory
                            objSignatoryClass.listIdentification = listIdentification

                            'Add to List of Signatory
                            listSignatoryAccountFromClass.Add(objSignatoryClass)

                        End If

                    Next
                End If
            End Using

            Return listSignatoryAccountFromClass
        Catch ex As Exception
            Throw
            Return New List(Of SiPendarBLL.SignatoryClass)
        End Try
    End Function

    Shared Function GetListTrnAddressFromRefByCIF(strCIF As String) As List(Of SIPENDAR_Trn_Acc_Entity_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Acc_Entity_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strCIF).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefCustomer IsNot Nothing Then
                    'end daniel20210719
                    Dim objListAddress = SiPendarBLL.goAML_CustomerBLL.GetListgoAML_Ref_AddressByPKID(objRefCustomer.PK_Customer_ID)
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Acc_Entity_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_Trn_Acc_Entity_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Acc_Entity = objAddress.FK_To_Table_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Acc_Entity_Address)
        End Try
    End Function

    Shared Function GetListTrnPhoneFromRefByCIF(strCIF As String) As List(Of SIPENDAR_Trn_Acc_Entity_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_Trn_Acc_Entity_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCustomer = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strCIF).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefCustomer IsNot Nothing Then
                    'end daniel20210719
                    Dim objListPhone = SiPendarBLL.goAML_CustomerBLL.GetListgoAML_Ref_PhoneByPKID(objRefCustomer.PK_Customer_ID)
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_Trn_Acc_Entity_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_Trn_Acc_Entity_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Acc_Entity = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Acc_Entity_Phone)
        End Try
    End Function

    Shared Function GetListTrnDirectorFromRefByCIF(strCIF As String, intFromTo As Integer, intSenderInformation As Integer) As List(Of SiPendarBLL.DirectorClass)
        Try
            Dim listDirectorAccountFromClass As New List(Of SiPendarBLL.DirectorClass)

            Using objDB As New SiPendarEntities

                Dim objListgoAML_Ref_Customer_Director = SiPendarBLL.goAML_CustomerBLL.GetCustomerDirector(strCIF)
                Dim intCountDirector As Integer = 0
                'start daniel20210719
                Dim intCount As Integer = 0
                'end daniel20210719
                If objListgoAML_Ref_Customer_Director IsNot Nothing Then
                    For Each item In objListgoAML_Ref_Customer_Director

                        'Counter for List
                        'start daniel20210719
                        'Dim intCount As Integer = 0
                        'end daniel20210719

                        'CIF Object
                        Dim objRefCustomer = goAML_CustomerBLL.GetCustomerbyCIF(item.FK_Entity_ID)

                        'Director Class Objects
                        Dim objDirectorClass As New SiPendarBLL.DirectorClass
                        Dim objDirector As New SiPendarDAL.SIPENDAR_Trn_Director
                        Dim listAddressDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                        Dim listPhoneDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                        Dim listAddressEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                        Dim listPhoneEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                        Dim listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

                        'Get objDirector
                        If item IsNot Nothing Then
                            'start daniel20210719
                            intCountDirector = intCountDirector - 1
                            'end daniel20210719
                            With objDirector
                                'start daniel20210719
                                .PK_goAML_Trn_Director_ID = intCountDirector
                                If objRefCustomer IsNot Nothing Then
                                    .FK_Entity_ID = objRefCustomer.PK_Customer_ID
                                End If
                                'end daniel20210719
                                .FK_From_Or_To = intFromTo
                                .FK_Sender_Information = intSenderInformation

                                'Load Person CIF Detail
                                If item IsNot Nothing Then
                                    .Gender = item.Gender
                                    .Title = item.Title
                                    .First_Name = item.First_name
                                    .Middle_Name = item.Middle_Name
                                    .Prefix = item.Prefix
                                    .Last_Name = item.Last_Name
                                    .BirthDate = item.BirthDate
                                    .Birth_Place = item.Birth_Place
                                    .Mothers_Name = item.Mothers_Name
                                    .Alias = item.Alias
                                    .SSN = item.SSN
                                    .Passport_Number = item.Passport_Number
                                    .Passport_Country = item.Passport_Country
                                    .Id_Number = item.ID_Number
                                    .Nationality1 = item.Nationality1
                                    .Nationality2 = item.Nationality2
                                    .Nationality3 = item.Nationality3
                                    .Residence = item.Residence
                                    .Email = item.Email
                                    .email2 = item.Email2
                                    .email3 = item.Email3
                                    .email4 = item.Email4
                                    .email5 = item.Email5
                                    .Occupation = item.Occupation
                                    .Employer_Name = item.Employer_Name
                                    .Deceased = item.Deceased
                                    .Deceased_Date = item.Deceased_Date
                                    .Tax_Number = item.Tax_Number
                                    .Tax_Reg_Number = item.Tax_Reg_Number
                                    .Source_Of_Wealth = item.Source_of_Wealth
                                    .Comment = item.Comments
                                End If

                                'Director Address(es)
                                'start daniel20210719
                                'intCount = 0
                                'end daniel20210719
                                Dim objListAddress = SiPendarBLL.goAML_CustomerBLL.GetDirectorAddresses(item.PK_goAML_Ref_Customer_Entity_Director_ID)
                                If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                    For Each objAddress In objListAddress
                                        'start daniel20210719
                                        intCount = intCount - 1
                                        'end daniel20210719
                                        Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                        With objNewDirectorAddress
                                            'start daniel20210719
                                            .PK_goAML_Trn_Director_Address_ID = intCount
                                            .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                            'end daniel20210719
                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments
                                            .isEmployer = 0
                                        End With

                                        listAddressDirector.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Phone(s)
                                'start daniel20210719
                                'intCount = 0
                                'end daniel20210719
                                Dim objListPhone = SiPendarBLL.goAML_CustomerBLL.GetDirectorPhone(item.PK_goAML_Ref_Customer_Entity_Director_ID)
                                If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                    For Each objPhone In objListPhone
                                        'start daniel20210719
                                        intCount = intCount - 1
                                        'end daniel20210719
                                        Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                        With objNewDirectorPhone
                                            'start daniel20210719
                                            .PK_goAML_trn_Director_Phone = intCount
                                            .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                            'end daniel20210719
                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments
                                            .isEmployer = 0
                                        End With

                                        listPhoneDirector.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Employer Address(es)
                                'start daniel20210719
                                'intCount = 0
                                'end daniel20210719
                                Dim objListAddressEmployer = SiPendarBLL.goAML_CustomerBLL.GetDirectorEmployerAddresses(item.PK_goAML_Ref_Customer_Entity_Director_ID)
                                If objListAddressEmployer IsNot Nothing AndAlso objListAddressEmployer.Count > 0 Then
                                    For Each objAddress In objListAddressEmployer
                                        'start daniel20210719
                                        intCount = intCount - 1
                                        'end daniel20210719
                                        Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                        With objNewDirectorAddress
                                            'start daniel20210719
                                            .PK_goAML_Trn_Director_Address_ID = intCount
                                            .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                            'end daniel20210719
                                            .Address_Type = objAddress.Address_Type
                                            .Address = objAddress.Address
                                            .Town = objAddress.Town
                                            .City = objAddress.City
                                            .Zip = objAddress.Zip
                                            .Country_Code = objAddress.Country_Code
                                            .State = objAddress.State
                                            .Comments = objAddress.Comments
                                            .isEmployer = 1
                                        End With

                                        listAddressEmployerDirector.Add(objNewDirectorAddress)
                                    Next
                                End If

                                'Director Employer Phone(s)
                                'start daniel20210719
                                'intCount = 0
                                'end daniel20210719
                                Dim objListPhoneEmployer = SiPendarBLL.goAML_CustomerBLL.GetDirectorEmployerPhones(item.PK_goAML_Ref_Customer_Entity_Director_ID)
                                If objListPhoneEmployer IsNot Nothing AndAlso objListPhoneEmployer.Count > 0 Then
                                    For Each objPhone In objListPhoneEmployer
                                        'start daniel20210719
                                        intCount = intCount - 1
                                        'end daniel20210719
                                        Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                        With objNewDirectorPhone
                                            'start daniel20210719
                                            .PK_goAML_trn_Director_Phone = intCount
                                            .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                            'end daniel20210719
                                            .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                            .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                            .tph_country_prefix = objPhone.tph_country_prefix
                                            .tph_number = objPhone.tph_number
                                            .tph_extension = objPhone.tph_extension
                                            .comments = objPhone.comments
                                            .isEmployer = 1
                                        End With

                                        listPhoneEmployerDirector.Add(objNewDirectorPhone)
                                    Next
                                End If

                                'Director Identification(s)
                                'start daniel20210719
                                'intCount = 0
                                'end daniel20210719
                                Dim objListIdentification = SiPendarBLL.goAML_CustomerBLL.GetDirectorIdentifications(item.PK_goAML_Ref_Customer_Entity_Director_ID)
                                If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                    For Each objIdentification In objListIdentification
                                        'start daniel20210719
                                        intCount = intCount - 1
                                        'end daniel20210719
                                        Dim objNewDirectorIdentification As New SIPENDAR_Transaction_Person_Identification
                                        With objNewDirectorIdentification
                                            'start daniel20210719
                                            .PK_goAML_Transaction_Person_Identification_ID = intCount
                                            .FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID
                                            'end daniel20210719
                                            .FK_Person_Type = 4
                                            .from_or_To_Type = intFromTo
                                            .Type = objIdentification.Type
                                            .Number = objIdentification.Number
                                            .Issue_Date = objIdentification.Issue_Date
                                            .Expiry_Date = objIdentification.Expiry_Date
                                            .Issued_By = objIdentification.Issued_By
                                            .Issued_Country = objIdentification.Issued_Country
                                            .Identification_Comment = objIdentification.Identification_Comment
                                        End With

                                        listIdentification.Add(objNewDirectorIdentification)
                                    Next
                                End If

                                .role = item.Role
                                .Active = 1
                            End With
                        End If

                        'Populate to Director Class
                        objDirectorClass.objDirector = objDirector
                        objDirectorClass.listAddressDirector = listAddressDirector
                        objDirectorClass.listPhoneDirector = listPhoneDirector
                        objDirectorClass.listAddressEmployerDirector = listAddressEmployerDirector
                        objDirectorClass.listPhoneEmployerDirector = listPhoneEmployerDirector
                        objDirectorClass.listIdentification = listIdentification

                        'Add to List of Director
                        listDirectorAccountFromClass.Add(objDirectorClass)
                    Next
                End If
            End Using

            Return listDirectorAccountFromClass
        Catch ex As Exception
            Throw
            Return New List(Of SiPendarBLL.DirectorClass)
        End Try
    End Function

    '*********************** END of ACCOUNT **************************************************

    'Daniel 22 Des 2020
    '*********************** WIC PERSON (CUSTOMER) **************************************************
    Shared Function GetTrnPersonFromRefByCIFCustomer(strWICNo As String, intFromTo As Integer) As SIPENDAR_Transaction_Person
        Try
            Dim objTrnPerson As New SIPENDAR_Transaction_Person

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault
                With objTrnPerson
                    .FK_From_Or_To = intFromTo
                    .Gender = objRefCIF.INDV_Gender
                    .Title = objRefCIF.INDV_Title
                    .Last_Name = objRefCIF.INDV_Last_Name
                    .BirthDate = objRefCIF.INDV_BirthDate
                    .Birth_Place = objRefCIF.INDV_Birth_Place
                    .Mothers_Name = objRefCIF.INDV_Mothers_Name
                    .Alias = objRefCIF.INDV_Alias
                    .SSN = objRefCIF.INDV_SSN
                    .Passport_Number = objRefCIF.INDV_Passport_Number
                    .Passport_Country = objRefCIF.INDV_Passport_Country
                    .Id_Number = objRefCIF.INDV_ID_Number
                    .Nationality1 = objRefCIF.INDV_Nationality1
                    .Nationality2 = objRefCIF.INDV_Nationality2
                    .Nationality3 = objRefCIF.INDV_Nationality3
                    .Residence = objRefCIF.INDV_Residence
                    .Email = objRefCIF.INDV_Email
                    .email2 = objRefCIF.INDV_Email2
                    .email3 = objRefCIF.INDV_Email3
                    .email4 = objRefCIF.INDV_Email4
                    .email5 = objRefCIF.INDV_Email5
                    .Occupation = objRefCIF.INDV_Occupation
                    .Employer_Name = objRefCIF.INDV_Employer_Name
                    .Deceased = objRefCIF.INDV_Deceased
                    .Deceased_Date = objRefCIF.INDV_Deceased_Date
                    .Tax_Number = objRefCIF.INDV_Tax_Number
                    .Tax_Reg_Number = objRefCIF.INDV_Tax_Reg_Number
                    .Source_Of_Wealth = objRefCIF.INDV_Source_of_Wealth
                    .Comment = objRefCIF.INDV_Comments
                End With

            End Using

            Return objTrnPerson

        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetListTrnAddressFromRefByCIFCustomer(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_Trn_Person_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Person_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                Dim refdetail As Integer = 0
                If intEmployer = 0 Then
                    refdetail = 1
                ElseIf intEmployer = 1 Then
                    refdetail = 5
                End If
                If objRefCIF IsNot Nothing Then
                    'end daniel20210719
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = refdetail And x.FK_To_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Person_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Person_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Person = objRefCIF.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                                .isEmployer = intEmployer
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Person_Address)
        End Try
    End Function

    Shared Function GetListTrnPhoneFromRefByCIFCustomer(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_trn_Person_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_trn_Person_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                Dim refdetail As Integer = 0
                If intEmployer = 0 Then
                    refdetail = 1
                ElseIf intEmployer = 1 Then
                    refdetail = 5
                End If
                If objRefCIF IsNot Nothing Then
                    'end daniel20210719
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = refdetail And x.FK_for_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_trn_Person_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_trn_Person_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Person = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                                .isEmployer = intEmployer
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_trn_Person_Phone)
        End Try
    End Function

    Shared Function GetListTrnIdentificationFromRefByCIFCustomer(strWICNo As String, intPersonType As Integer, intFromTo As Integer) As List(Of SIPENDAR_Transaction_Person_Identification)
        Try
            'PersonType : 1 Person, 2 Signatory, 3 Conductor, 4 Director
            Dim listIdentification As New List(Of SIPENDAR_Transaction_Person_Identification)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                If objRefCIF IsNot Nothing Then
                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intPersonType And x.FK_Person_ID = objRefCIF.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                        For Each objIdentification In objListIdentification
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewIdentification As New SIPENDAR_Transaction_Person_Identification
                            With objNewIdentification
                                'start daniel20210719
                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                'end daniel20210719
                                .FK_Person_ID = objIdentification.FK_Person_ID
                                .FK_Person_Type = intPersonType
                                .from_or_To_Type = intFromTo
                                .Type = objIdentification.Type
                                .Number = objIdentification.Number
                                .Issue_Date = objIdentification.Issue_Date
                                .Issued_By = objIdentification.Issued_By
                                .Issued_Country = objIdentification.Issued_Country
                                .Expiry_Date = objIdentification.Expiry_Date
                                .Identification_Comment = objIdentification.Identification_Comment
                            End With

                            listIdentification.Add(objNewIdentification)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listIdentification

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Transaction_Person_Identification)
        End Try
    End Function
    '*********************** END of WIC PERSON (CUSTOMER) **************************************************
    'End 22 Des 2020

    '*********************** WIC PERSON (NON CUSTOMER) **************************************************
    Shared Function GetTrnPersonFromRefByWICNo(strWICNo As String, intFromTo As Integer) As SIPENDAR_Transaction_Person
        Try
            Dim objTrnPerson As New SIPENDAR_Transaction_Person

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                With objTrnPerson
                    .FK_From_Or_To = intFromTo
                    .Gender = objRefWIC.INDV_Gender
                    .Title = objRefWIC.INDV_Title
                    .Last_Name = objRefWIC.INDV_Last_Name
                    .BirthDate = objRefWIC.INDV_BirthDate
                    .Birth_Place = objRefWIC.INDV_Birth_Place
                    .Mothers_Name = objRefWIC.INDV_Mothers_Name
                    .Alias = objRefWIC.INDV_Alias
                    .SSN = objRefWIC.INDV_SSN
                    .Passport_Number = objRefWIC.INDV_Passport_Number
                    .Passport_Country = objRefWIC.INDV_Passport_Country
                    .Id_Number = objRefWIC.INDV_ID_Number
                    .Nationality1 = objRefWIC.INDV_Nationality1
                    .Nationality2 = objRefWIC.INDV_Nationality2
                    .Nationality3 = objRefWIC.INDV_Nationality3
                    .Residence = objRefWIC.INDV_Residence
                    .Email = objRefWIC.INDV_Email
                    .email2 = objRefWIC.INDV_Email2
                    .email3 = objRefWIC.INDV_Email3
                    .email4 = objRefWIC.INDV_Email4
                    .email5 = objRefWIC.INDV_Email5
                    .Occupation = objRefWIC.INDV_Occupation
                    .Employer_Name = objRefWIC.INDV_Employer_Name
                    .Deceased = objRefWIC.INDV_Deceased
                    .Deceased_Date = objRefWIC.INDV_Deceased_Date
                    .Tax_Number = objRefWIC.INDV_Tax_Number
                    .Tax_Reg_Number = objRefWIC.INDV_Tax_Reg_Number
                    .Source_Of_Wealth = objRefWIC.INDV_SumberDana
                    .Comment = objRefWIC.INDV_Comment
                End With

            End Using

            Return objTrnPerson

        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetListTrnAddressFromRefByWICNo(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_Trn_Person_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Person_Address)

            'Counter for List
            Dim intCount As Integer = 0
            'start daniel20210719
            Dim intdetailof As Integer = 0
            If intEmployer = 0 Then
                intdetailof = 3
            ElseIf intEmployer = 1 Then
                intdetailof = 10
            End If
            'end daniel20210719
            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefWIC IsNot Nothing Then
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = intdetailof And x.FK_To_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Person_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Person_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Person = objRefWIC.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                                .isEmployer = intEmployer
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Person_Address)
        End Try
    End Function

    Shared Function GetListTrnPhoneFromRefByWICNo(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_trn_Person_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_trn_Person_Phone)

            'Counter for List
            Dim intCount As Integer = 0
            'start daniel20210719
            Dim intdetailof As Integer = 0
            If intEmployer = 0 Then
                intdetailof = 3
            ElseIf intEmployer = 1 Then
                intdetailof = 10
            End If
            'end daniel20210719

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                If objRefWIC IsNot Nothing Then
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = intdetailof And x.FK_for_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_trn_Person_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_trn_Person_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Person = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                                .isEmployer = intEmployer
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_trn_Person_Phone)
        End Try
    End Function

    Shared Function GetListTrnIdentificationFromRefByWICNo(strWICNo As String, intPersonType As Integer, intFromTo As Integer) As List(Of SIPENDAR_Transaction_Person_Identification)
        Try
            'PersonType : 1 Person, 2 Signatory, 3 Conductor, 4 Director
            Dim listIdentification As New List(Of SIPENDAR_Transaction_Person_Identification)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefWIC IsNot Nothing Then
                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = intPersonType And x.FK_Person_ID = objRefWIC.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                        For Each objIdentification In objListIdentification
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewIdentification As New SIPENDAR_Transaction_Person_Identification
                            With objNewIdentification
                                'start daniel20210719
                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                'end daniel20210719
                                .FK_Person_ID = objIdentification.FK_Person_ID
                                .FK_Person_Type = intPersonType
                                .from_or_To_Type = intFromTo
                                .Type = objIdentification.Type
                                .Number = objIdentification.Number
                                .Issue_Date = objIdentification.Issue_Date
                                .Issued_By = objIdentification.Issued_By
                                .Issued_Country = objIdentification.Issued_Country
                                .Expiry_Date = objIdentification.Expiry_Date
                                .Identification_Comment = objIdentification.Identification_Comment
                            End With

                            listIdentification.Add(objNewIdentification)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listIdentification

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Transaction_Person_Identification)
        End Try
    End Function

    '*********************** END of WIC PERSON (NON CUSTOMER) **************************************************

    'Daniel 22 Des 2020
    '*********************** WIC ENTITY (CUSTOMER) **************************************************
    Shared Function GetTrnEntityFromRefByCIFCustomer(strWICNo As String, intFromTo As Integer) As SIPENDAR_Transaction_Entity
        Try
            Dim objTrnEntity As New SIPENDAR_Transaction_Entity

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault
                With objTrnEntity
                    .FK_From_Or_To = intFromTo
                    .Name = objRefCIF.Corp_Name
                    .Commercial_Name = objRefCIF.Corp_Commercial_Name
                    .Incorporation_Legal_Form = objRefCIF.Corp_Incorporation_Legal_Form
                    .Incorporation_Number = objRefCIF.Corp_Incorporation_Number
                    .Business = objRefCIF.Corp_Business
                    .Email = objRefCIF.Corp_Email
                    .Url = objRefCIF.Corp_Url
                    .Incorporation_State = objRefCIF.Corp_Incorporation_State
                    .Incorporation_Country_Code = objRefCIF.Corp_Incorporation_Country_Code
                    .Incorporation_Date = objRefCIF.Corp_Incorporation_Date
                    .Business_Closed = objRefCIF.Corp_Business_Closed
                    .Date_Business_Closed = objRefCIF.Corp_Date_Business_Closed
                    .Tax_Number = objRefCIF.Corp_Tax_Number
                    .Tax_Registeration_Number = objRefCIF.Corp_Tax_Registeration_Number
                    .Comments = objRefCIF.Corp_Comments
                End With

            End Using

            Return objTrnEntity

        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetListTrnEntityAddressFromRefByCIFCustomer(strWICNo As String) As List(Of SIPENDAR_Trn_Entity_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Entity_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefCIF IsNot Nothing Then
                    'end daniel20210719
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_To_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Entity_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Entity_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Entity = objRefCIF.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Entity_Address)
        End Try
    End Function

    Shared Function GetListTrnEntityPhoneFromRefByCIFCustomer(strWICNo As String) As List(Of SIPENDAR_Trn_Entity_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_Trn_Entity_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                If objRefCIF IsNot Nothing Then
                    'end daniel20210719
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 1 And x.FK_for_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_Trn_Entity_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_Trn_Entity_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Entity = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Entity_Phone)
        End Try
    End Function

    Shared Function GetListTrnEntityDirectorFromRefByCIFCustomer(strWICNo As String, intFromTo As Integer, intSenderInformation As Integer) As List(Of SiPendarBLL.DirectorClass)
        Try
            Dim listDirectorAccountFromClass As New List(Of SiPendarBLL.DirectorClass)

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault
                If objRefCIF IsNot Nothing Then

                    Dim objListgoAML_Ref_Customer_Director = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = objRefCIF.PK_Customer_ID).ToList
                    Dim intCountDirector As Integer = 0
                    'start daniel20210719
                    Dim intCount As Integer = 0
                    'end daniel20210719

                    If objListgoAML_Ref_Customer_Director IsNot Nothing Then
                        For Each item In objListgoAML_Ref_Customer_Director

                            'Counter for List
                            'start daniel20210719
                            'Dim intCount As Integer = 0
                            'end daniel20210719

                            'Director Class Objects
                            Dim objDirectorClass As New SiPendarBLL.DirectorClass
                            Dim objDirector As New SiPendarDAL.SIPENDAR_Trn_Director
                            Dim listAddressDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                            Dim listPhoneDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                            Dim listAddressEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                            Dim listPhoneEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                            Dim listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

                            'Get objDirector
                            If item IsNot Nothing Then
                                'start daniel20210719
                                intCountDirector = intCountDirector - 1
                                'end daniel20210719
                                With objDirector
                                    'start daniel20210719
                                    .PK_goAML_Trn_Director_ID = intCountDirector
                                    'end daniel20210719
                                    .FK_Entity_ID = objRefCIF.PK_Customer_ID
                                    .FK_From_Or_To = intFromTo
                                    .FK_Sender_Information = intSenderInformation

                                    'Load Person CIF Detail
                                    If item IsNot Nothing Then
                                        .Gender = item.Gender
                                        .Title = item.Title
                                        .First_Name = item.First_name
                                        .Middle_Name = item.Middle_Name
                                        .Prefix = item.Prefix
                                        .Last_Name = item.Last_Name
                                        .BirthDate = item.BirthDate
                                        .Birth_Place = item.Birth_Place
                                        .Mothers_Name = item.Mothers_Name
                                        .Alias = item.Alias
                                        .SSN = item.SSN
                                        .Passport_Number = item.Passport_Number
                                        .Passport_Country = item.Passport_Country
                                        .Id_Number = item.ID_Number
                                        .Nationality1 = item.Nationality1
                                        .Nationality2 = item.Nationality2
                                        .Nationality3 = item.Nationality3
                                        .Residence = item.Residence
                                        .Email = item.Email
                                        .email2 = item.Email2
                                        .email3 = item.Email3
                                        .email4 = item.Email4
                                        .email5 = item.Email5
                                        .Occupation = item.Occupation
                                        .Employer_Name = item.Employer_Name
                                        .Deceased = item.Deceased
                                        .Deceased_Date = item.Deceased_Date
                                        .Tax_Number = item.Tax_Number
                                        .Tax_Reg_Number = item.Tax_Reg_Number
                                        .Source_Of_Wealth = item.Source_of_Wealth
                                        .Comment = item.Comments
                                    End If

                                    'Director Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 6 And x.FK_To_Table_ID = item.NO_ID)
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                            With objNewDirectorAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Director_Address_ID = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 0
                                            End With

                                            listAddressDirector.Add(objNewDirectorAddress)
                                        Next
                                    End If

                                    'Director Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 6 And x.FK_for_Table_ID = item.NO_ID)
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                            With objNewDirectorPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_Director_Phone = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 0
                                            End With

                                            listPhoneDirector.Add(objNewDirectorPhone)
                                        Next
                                    End If

                                    'Director Employer Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddressEmployer = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 7 And x.FK_To_Table_ID = item.NO_ID)
                                    If objListAddressEmployer IsNot Nothing AndAlso objListAddressEmployer.Count > 0 Then
                                        For Each objAddress In objListAddressEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                            With objNewDirectorAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Director_Address_ID = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 1
                                            End With

                                            listAddressEmployerDirector.Add(objNewDirectorAddress)
                                        Next
                                    End If

                                    'Director Employer Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhoneEmployer = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 7 And x.FK_for_Table_ID = item.NO_ID)
                                    If objListPhoneEmployer IsNot Nothing AndAlso objListPhoneEmployer.Count > 0 Then
                                        For Each objPhone In objListPhoneEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                            With objNewDirectorPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_Director_Phone = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 1
                                            End With

                                            listPhoneEmployerDirector.Add(objNewDirectorPhone)
                                        Next
                                    End If

                                    'Director Identification(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 4 And x.FK_Person_ID = item.NO_ID)
                                    'end daniel20210719
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification In objListIdentification
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorIdentification As New SIPENDAR_Transaction_Person_Identification
                                            With objNewDirectorIdentification
                                                'start daniel20210719
                                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                                .FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .FK_Person_Type = 4
                                                .from_or_To_Type = intFromTo
                                                .Type = objIdentification.Type
                                                .Number = objIdentification.Number
                                                .Issue_Date = objIdentification.Issue_Date
                                                .Expiry_Date = objIdentification.Expiry_Date
                                                .Issued_By = objIdentification.Issued_By
                                                .Issued_Country = objIdentification.Issued_Country
                                                .Identification_Comment = objIdentification.Identification_Comment
                                            End With

                                            listIdentification.Add(objNewDirectorIdentification)
                                        Next
                                    End If

                                    .role = item.Role
                                    .Active = 1
                                End With
                            End If

                            'Populate to Director Class
                            objDirectorClass.objDirector = objDirector
                            objDirectorClass.listAddressDirector = listAddressDirector
                            objDirectorClass.listPhoneDirector = listPhoneDirector
                            objDirectorClass.listAddressEmployerDirector = listAddressEmployerDirector
                            objDirectorClass.listPhoneEmployerDirector = listPhoneEmployerDirector
                            objDirectorClass.listIdentification = listIdentification

                            'Add to List of Director
                            listDirectorAccountFromClass.Add(objDirectorClass)
                        Next
                    End If
                End If
            End Using

            Return listDirectorAccountFromClass
        Catch ex As Exception
            Throw
            Return New List(Of SiPendarBLL.DirectorClass)
        End Try
    End Function
    '*********************** END of WIC ENTITY (CUSTOMER) **************************************************
    'End 22 Des 2020

    '*********************** WIC ENTITY (NON CUSTOMER) **************************************************
    Shared Function GetTrnEntityFromRefByWICNo(strWICNo As String, intFromTo As Integer) As SIPENDAR_Transaction_Entity
        Try
            Dim objTrnEntity As New SIPENDAR_Transaction_Entity

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                With objTrnEntity
                    .FK_From_Or_To = intFromTo
                    .Name = objRefWIC.Corp_Name
                    .Commercial_Name = objRefWIC.Corp_Commercial_Name
                    .Incorporation_Legal_Form = objRefWIC.Corp_Incorporation_Legal_Form
                    .Incorporation_Number = objRefWIC.Corp_Incorporation_Number
                    .Business = objRefWIC.Corp_Business
                    .Email = objRefWIC.Corp_Email
                    .Url = objRefWIC.Corp_Url
                    .Incorporation_State = objRefWIC.Corp_Incorporation_State
                    .Incorporation_Country_Code = objRefWIC.Corp_Incorporation_Country_Code
                    .Incorporation_Date = objRefWIC.Corp_Incorporation_Date
                    .Business_Closed = objRefWIC.Corp_Business_Closed
                    .Date_Business_Closed = objRefWIC.Corp_Date_Business_Closed
                    .Tax_Number = objRefWIC.Corp_Tax_Number
                    .Tax_Registeration_Number = objRefWIC.Corp_Tax_Registeration_Number
                    .Comments = objRefWIC.Corp_Comments
                End With

            End Using

            Return objTrnEntity

        Catch ex As Exception
            Throw
            Return Nothing
        End Try
    End Function

    Shared Function GetListTrnEntityAddressFromRefByWICNo(strWICNo As String) As List(Of SIPENDAR_Trn_Entity_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Entity_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                If objRefWIC IsNot Nothing Then
                    'end daniel20210719
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 3 And x.FK_To_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Entity_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Entity_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Entity = objRefWIC.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Entity_Address)
        End Try
    End Function

    Shared Function GetListTrnEntityPhoneFromRefByWICNo(strWICNo As String) As List(Of SIPENDAR_Trn_Entity_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_Trn_Entity_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                If objRefWIC IsNot Nothing Then
                    'end daniel20210719
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 3 And x.FK_for_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_Trn_Entity_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_Trn_Entity_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Entity = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Entity_Phone)
        End Try
    End Function

    Shared Function GetListTrnEntityDirectorFromRefByWICNo(strWICNo As String, intFromTo As Integer, intSenderInformation As Integer) As List(Of SiPendarBLL.DirectorClass)
        Try
            Dim listDirectorAccountFromClass As New List(Of SiPendarBLL.DirectorClass)

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                If objRefWIC IsNot Nothing Then

                    Dim objListgoAML_Ref_Customer_Director = objDB.goAML_Ref_Walk_In_Customer_Director.Where(Function(x) x.FK_Entity_ID = objRefWIC.PK_Customer_ID).ToList
                    Dim intCountDirector As Integer = 0

                    'start daniel20210719
                    Dim intCount As Integer = 0
                    'end daniel20210719
                    If objListgoAML_Ref_Customer_Director IsNot Nothing Then
                        For Each item In objListgoAML_Ref_Customer_Director

                            'Counter for List
                            'start daniel20210719
                            'Dim intCount As Integer = 0
                            'end daniel20210719

                            'Director Class Objects
                            Dim objDirectorClass As New SiPendarBLL.DirectorClass
                            Dim objDirector As New SiPendarDAL.SIPENDAR_Trn_Director
                            Dim listAddressDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                            Dim listPhoneDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                            Dim listAddressEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
                            Dim listPhoneEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
                            Dim listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

                            'Get objDirector
                            If item IsNot Nothing Then
                                'start daniel20210719
                                intCountDirector = intCountDirector - 1
                                'end daniel20210719
                                With objDirector
                                    'start daniel20210719
                                    .PK_goAML_Trn_Director_ID = intCountDirector
                                    'end daniel20210719
                                    .FK_Entity_ID = objRefWIC.PK_Customer_ID
                                    .FK_From_Or_To = intFromTo
                                    .FK_Sender_Information = intSenderInformation

                                    'Load Person CIF Detail
                                    If item IsNot Nothing Then
                                        .Gender = item.Gender
                                        .Title = item.Title
                                        .First_Name = item.First_name
                                        .Middle_Name = item.Middle_Name
                                        .Prefix = item.Prefix
                                        .Last_Name = item.Last_Name
                                        .BirthDate = item.BirthDate
                                        .Birth_Place = item.Birth_Place
                                        .Mothers_Name = item.Mothers_Name
                                        .Alias = item.Alias
                                        .SSN = item.SSN
                                        .Passport_Number = item.Passport_Number
                                        .Passport_Country = item.Passport_Country
                                        .Id_Number = item.ID_Number
                                        .Nationality1 = item.Nationality1
                                        .Nationality2 = item.Nationality2
                                        .Nationality3 = item.Nationality3
                                        .Residence = item.Residence
                                        .Email = item.Email
                                        .email2 = item.Email2
                                        .email3 = item.Email3
                                        .email4 = item.Email4
                                        .email5 = item.Email5
                                        .Occupation = item.Occupation
                                        .Employer_Name = item.Employer_Name
                                        .Deceased = item.Deceased
                                        .Deceased_Date = item.Deceased_Date
                                        .Tax_Number = item.Tax_Number
                                        .Tax_Reg_Number = item.Tax_Reg_Number
                                        .Source_Of_Wealth = item.Source_of_Wealth
                                        .Comment = item.Comments
                                    End If

                                    'Director Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 8 And x.FK_To_Table_ID = item.NO_ID)
                                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                                        For Each objAddress In objListAddress
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                            With objNewDirectorAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Director_Address_ID = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 0
                                            End With

                                            listAddressDirector.Add(objNewDirectorAddress)
                                        Next
                                    End If

                                    'Director Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 8 And x.FK_for_Table_ID = item.NO_ID)
                                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                                        For Each objPhone In objListPhone
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                            With objNewDirectorPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_Director_Phone = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 0
                                            End With

                                            listPhoneDirector.Add(objNewDirectorPhone)
                                        Next
                                    End If

                                    'Director Employer Address(es)
                                    'start daniel20210719
                                    'intCount = 0
                                    Dim objListAddressEmployer = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = 4 And x.FK_To_Table_ID = item.NO_ID)
                                    'end daniel20210719
                                    If objListAddressEmployer IsNot Nothing AndAlso objListAddressEmployer.Count > 0 Then
                                        For Each objAddress In objListAddressEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorAddress As New SIPENDAR_Trn_Director_Address
                                            With objNewDirectorAddress
                                                'start daniel20210719
                                                .PK_goAML_Trn_Director_Address_ID = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Address_Type = objAddress.Address_Type
                                                .Address = objAddress.Address
                                                .Town = objAddress.Town
                                                .City = objAddress.City
                                                .Zip = objAddress.Zip
                                                .Country_Code = objAddress.Country_Code
                                                .State = objAddress.State
                                                .Comments = objAddress.Comments
                                                .isEmployer = 1
                                            End With

                                            listAddressEmployerDirector.Add(objNewDirectorAddress)
                                        Next
                                    End If

                                    'Director Employer Phone(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    Dim objListPhoneEmployer = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = 4 And x.FK_for_Table_ID = item.NO_ID)
                                    'end daniel20210719
                                    If objListPhoneEmployer IsNot Nothing AndAlso objListPhoneEmployer.Count > 0 Then
                                        For Each objPhone In objListPhoneEmployer
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorPhone As New SIPENDAR_trn_Director_Phone
                                            With objNewDirectorPhone
                                                'start daniel20210719
                                                .PK_goAML_trn_Director_Phone = intCount
                                                .FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID
                                                'end daniel20210719
                                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                                .tph_country_prefix = objPhone.tph_country_prefix
                                                .tph_number = objPhone.tph_number
                                                .tph_extension = objPhone.tph_extension
                                                .comments = objPhone.comments
                                                .isEmployer = 1
                                            End With

                                            listPhoneEmployerDirector.Add(objNewDirectorPhone)
                                        Next
                                    End If

                                    'Director Identification(s)
                                    'start daniel20210719
                                    'intCount = 0
                                    'end daniel20210719
                                    Dim objListIdentification = objDB.goAML_Person_Identification.Where(Function(x) x.FK_Person_Type = 7 And x.FK_Person_ID = item.NO_ID)
                                    If objListIdentification IsNot Nothing AndAlso objListIdentification.Count > 0 Then
                                        For Each objIdentification In objListIdentification
                                            'start daniel20210719
                                            intCount = intCount - 1
                                            'end daniel20210719
                                            Dim objNewDirectorIdentification As New SIPENDAR_Transaction_Person_Identification
                                            With objNewDirectorIdentification
                                                'start daniel20210719
                                                .PK_goAML_Transaction_Person_Identification_ID = intCount
                                                .FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID
                                                .FK_Person_Type = 7
                                                'end daniel20210719
                                                .from_or_To_Type = intFromTo
                                                .Type = objIdentification.Type
                                                .Number = objIdentification.Number
                                                .Issue_Date = objIdentification.Issue_Date
                                                .Expiry_Date = objIdentification.Expiry_Date
                                                .Issued_By = objIdentification.Issued_By
                                                .Issued_Country = objIdentification.Issued_Country
                                                .Identification_Comment = objIdentification.Identification_Comment
                                            End With

                                            listIdentification.Add(objNewDirectorIdentification)
                                        Next
                                    End If

                                    .role = item.Role
                                    .Active = 1
                                End With
                            End If

                            'Populate to Director Class
                            objDirectorClass.objDirector = objDirector
                            objDirectorClass.listAddressDirector = listAddressDirector
                            objDirectorClass.listPhoneDirector = listPhoneDirector
                            objDirectorClass.listAddressEmployerDirector = listAddressEmployerDirector
                            objDirectorClass.listPhoneEmployerDirector = listPhoneEmployerDirector
                            objDirectorClass.listIdentification = listIdentification

                            'Add to List of Director
                            listDirectorAccountFromClass.Add(objDirectorClass)
                        Next
                    End If
                End If
            End Using

            Return listDirectorAccountFromClass
        Catch ex As Exception
            Throw
            Return New List(Of SiPendarBLL.DirectorClass)
        End Try
    End Function
    '*********************** END of WIC ENTITY (NON CUSTOMER) **************************************************

    'Daniel 22 Des 2020
    '*********************** CONDUCTOR (CUSTOMER) *****************************************************************
    Shared Function GetListTrnConductorAddressFromRefByCIFCustomer(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_Trn_Conductor_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Conductor_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                Dim refdetail As Integer = 0
                If intEmployer = 0 Then
                    refdetail = 1
                ElseIf intEmployer = 1 Then
                    refdetail = 5
                End If
                If objRefCIF IsNot Nothing Then
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = refdetail And x.FK_To_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Conductor_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Conductor_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Conductor_ID = objRefCIF.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                                .isEmployer = intEmployer
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Conductor_Address)
        End Try
    End Function

    Shared Function GetListTrnConductorPhoneFromRefByCIFCustomer(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_trn_Conductor_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_trn_Conductor_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefCIF = objDB.goAML_Ref_Customer.Where(Function(x) x.CIF = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                Dim refdetail As Integer = 0
                If intEmployer = 0 Then
                    refdetail = 1
                ElseIf intEmployer = 1 Then
                    refdetail = 5
                End If
                If objRefCIF IsNot Nothing Then
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = refdetail And x.FK_for_Table_ID = objRefCIF.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_trn_Conductor_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_trn_Conductor_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Conductor_ID = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                                .isEmployer = intEmployer
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_trn_Conductor_Phone)
        End Try
    End Function
    '*********************** END of CONDUCTOR (CUSTOMER) **********************************************************
    'End 22 Des 2020

    '*********************** CONDUCTOR *****************************************************************
    Shared Function GetListTrnConductorAddressFromRefByWICNo(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_Trn_Conductor_Address)
        Try
            Dim listAddress As New List(Of SIPENDAR_Trn_Conductor_Address)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault
                'start daniel20210719
                'intCount = 0
                Dim intdetail As Integer = 0
                If intEmployer = 0 Then
                    intdetail = 3
                ElseIf intEmployer = 1 Then
                    intdetail = 10
                End If
                If objRefWIC IsNot Nothing Then
                    Dim objListAddress = objDB.goAML_Ref_Address.Where(Function(x) x.FK_Ref_Detail_Of = intdetail And x.FK_To_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListAddress IsNot Nothing AndAlso objListAddress.Count > 0 Then
                        For Each objAddress In objListAddress
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewAddress As New SIPENDAR_Trn_Conductor_Address
                            With objNewAddress
                                'start daniel20210719
                                .PK_goAML_Trn_Conductor_Address_ID = intCount
                                'end daniel20210719
                                .FK_Trn_Conductor_ID = objRefWIC.PK_Customer_ID
                                .Address_Type = objAddress.Address_Type
                                .Address = objAddress.Address
                                .Town = objAddress.Town
                                .City = objAddress.City
                                .Zip = objAddress.Zip
                                .Country_Code = objAddress.Country_Code
                                .State = objAddress.State
                                .Comments = objAddress.Comments
                                .isEmployer = intEmployer
                            End With

                            listAddress.Add(objNewAddress)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listAddress

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_Trn_Conductor_Address)
        End Try
    End Function

    Shared Function GetListTrnConductorPhoneFromRefByWICNo(strWICNo As String, intEmployer As Integer) As List(Of SIPENDAR_trn_Conductor_Phone)
        Try
            Dim listPhone As New List(Of SIPENDAR_trn_Conductor_Phone)

            'Counter for List
            Dim intCount As Integer = 0

            Using objDB As New SiPendarEntities

                Dim objRefWIC = objDB.goAML_Ref_WIC.Where(Function(x) x.WIC_No = strWICNo).FirstOrDefault

                'start daniel20210719
                'intCount = 0
                Dim intdetail As Integer = 0
                If intEmployer = 0 Then
                    intdetail = 3
                ElseIf intEmployer = 1 Then
                    intdetail = 10
                End If
                If objRefWIC IsNot Nothing Then
                    Dim objListPhone = objDB.goAML_Ref_Phone.Where(Function(x) x.FK_Ref_Detail_Of = intdetail And x.FK_for_Table_ID = objRefWIC.PK_Customer_ID).ToList
                    'end daniel20210719
                    If objListPhone IsNot Nothing AndAlso objListPhone.Count > 0 Then
                        For Each objPhone In objListPhone
                            'start daniel20210719
                            intCount = intCount - 1
                            'end daniel20210719
                            Dim objNewPhone As New SIPENDAR_trn_Conductor_Phone
                            With objNewPhone
                                'start daniel20210719
                                .PK_goAML_trn_Conductor_Phone = intCount
                                'end daniel20210719
                                .FK_Trn_Conductor_ID = objPhone.FK_for_Table_ID
                                .Tph_Contact_Type = objPhone.Tph_Contact_Type
                                .Tph_Communication_Type = objPhone.Tph_Communication_Type
                                .tph_country_prefix = objPhone.tph_country_prefix
                                .tph_number = objPhone.tph_number
                                .tph_extension = objPhone.tph_extension
                                .comments = objPhone.comments
                                .isEmployer = intEmployer
                            End With

                            listPhone.Add(objNewPhone)
                        Next
                    End If
                    'start daniel20210719
                End If
                'end daniel20210719
            End Using

            Return listPhone

        Catch ex As Exception
            Throw
            Return New List(Of SIPENDAR_trn_Conductor_Phone)
        End Try
    End Function
    '*********************** END of CONDUCTOR **********************************************************

    '*********************** TITIP SEMENTARA SAVE REPORT TRANSACTIONS AFTER DELETE *********************
    Shared Function saveEditReport(report As Report, objmodule As NawaDAL.Module, OldObjReport As SiPendarDAL.SIPENDAR_Report, IsUseApproval As Boolean)
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    Dim objReport As New SIPENDAR_Report
                    objReport = report.objReport
                    'objReport.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'objReport.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    'objReport.CreatedDate = Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")
                    'objReport.ApprovedDate = Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objReport.LastUpdateDate = Now ' Now.ToString("yyyy-MM-dd HH:mm:ss")
                    objReport.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    ' objReport.Alternateby = alternateby

                    If IsUseApproval Then
                        objReport.Status = 4    'Waiting for Approval
                    Else
                        objReport.Status = 1    'Waiting for Generate
                        objReport.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objReport.ApprovedDate = Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")
                    End If

                    objdb.Entry(objReport).State = Entity.EntityState.Modified
                    objdb.SaveChanges()

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim modulename As String = objmodule.ModuleLabel

                    Dim objaudittrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objReport, OldObjReport)

                    If report.listIndicator.Count > 0 Then
                        For Each item In report.listIndicator
                            With item
                                .FK_Report = objReport.PK_Report_ID
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .Alternateby = alternateby
                                .Active = True
                            End With
                            objdb.Entry(item).State = Entity.EntityState.Added
                            objdb.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, item)
                        Next
                    End If

                    If getTrnORActivity(objReport.Report_Code) = "TRN" Then
                        For Each TransactionClass As SiPendarBLL.Transaction In report.listObjTransaction
                            Dim objtransaction As SIPENDAR_Transaction = TransactionClass.objTransaction
                            With objtransaction
                                .FK_Report_ID = objReport.PK_Report_ID
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .Alternateby = alternateby
                                .Active = True
                            End With
                            objdb.Entry(objtransaction).State = Entity.EntityState.Added
                            objdb.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objtransaction)
                            'Conductor
                            If objtransaction.usedConductor Then
                                Dim conductor As SiPendarDAL.SIPENDAR_Trn_Conductor = TransactionClass.ObjConductor
                                With conductor
                                    .FK_REPORT_ID = objReport.PK_Report_ID
                                    .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                    .Alternateby = alternateby
                                    .Active = True
                                End With
                                objdb.Entry(conductor).State = Entity.EntityState.Added
                                objdb.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, conductor)
                                'Address Conductor
                                If TransactionClass.listObjAddressConductor.Count > 0 Then
                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Conductor_Address In TransactionClass.listObjAddressConductor
                                        With address
                                            .FK_REPORT_ID = objReport.PK_Report_ID
                                            .FK_Trn_Conductor_ID = conductor.PK_goAML_Trn_Conductor_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(address).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                    Next
                                End If
                                'Phone Conductor
                                If TransactionClass.listObjPhoneConductor.Count > 0 Then
                                    For Each Phone As SiPendarDAL.SIPENDAR_trn_Conductor_Phone In TransactionClass.listObjPhoneConductor
                                        With Phone
                                            .FK_REPORT_ID = objReport.PK_Report_ID
                                            .FK_Trn_Conductor_ID = conductor.PK_goAML_Trn_Conductor_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(Phone).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                    Next
                                End If
                                'Address Employer Conductor
                                If TransactionClass.listObjAddressEmployerConductor.Count > 0 Then
                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Conductor_Address In TransactionClass.listObjAddressEmployerConductor
                                        With address
                                            .FK_REPORT_ID = objReport.PK_Report_ID
                                            .FK_Trn_Conductor_ID = conductor.PK_goAML_Trn_Conductor_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(address).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                    Next
                                End If
                                'Phone Employer Conductor
                                If TransactionClass.listObjPhoneEmployerConductor.Count > 0 Then
                                    For Each Phone As SiPendarDAL.SIPENDAR_trn_Conductor_Phone In TransactionClass.listObjPhoneEmployerConductor
                                        With Phone
                                            .FK_REPORT_ID = objReport.PK_Report_ID
                                            .FK_Trn_Conductor_ID = conductor.PK_goAML_Trn_Conductor_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(Phone).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                    Next
                                End If
                                'Identification Conductor
                                If TransactionClass.listObjIdentificationConductor.Count > 0 Then
                                    For Each Identification As SIPENDAR_Transaction_Person_Identification In TransactionClass.listObjIdentificationConductor
                                        With Identification
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .FK_Person_ID = conductor.PK_goAML_Trn_Conductor_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(Identification).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Identification)
                                    Next
                                End If

                            End If
                            '============================== Pengirim =================================
                            'AccountFrom
                            If objtransaction.FK_Transaction_Type = 1 Then
                                If objtransaction.FK_Sender_From_Information = 1 Then
                                    Dim objAccount As SIPENDAR_Transaction_Account = TransactionClass.objAccountFrom
                                    With objAccount
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Report_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objAccount).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccount)
                                    'Signatory
                                    If TransactionClass.LisAccountSignatoryFrom.Count > 0 Then
                                        For Each signatoryClass As SiPendarBLL.SignatoryClass In TransactionClass.LisAccountSignatoryFrom
                                            Dim signatory As SiPendarDAL.SIPENDAR_Trn_acc_Signatory = signatoryClass.objSignatory
                                            With signatory
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(signatory).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, signatory)
                                            'Address Signatory
                                            If signatoryClass.listAddressSignatory.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address In signatoryClass.listAddressSignatory
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Signatory
                                            If signatoryClass.listPhoneSignatory.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_acc_sign_Phone In signatoryClass.listPhoneSignatory
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Address Employer Signatory
                                            If signatoryClass.listAddressEmployerSignatory.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address In signatoryClass.listAddressEmployerSignatory
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Employer Signatory
                                            If signatoryClass.listPhoneEmployerSignatory.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_acc_sign_Phone In signatoryClass.listPhoneEmployerSignatory
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'IdentificationSignatory
                                            If signatoryClass.listIdentification.Count > 0 Then
                                                For Each Identification As SIPENDAR_Transaction_Person_Identification In signatoryClass.listIdentification
                                                    With Identification
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Person_ID = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(Identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Identification)
                                                Next
                                            End If
                                        Next
                                    End If
                                    'Entity Account
                                    If objAccount.IsRekeningKorporasi Then
                                        Dim objEntityAccount As SiPendarDAL.SIPENDAR_Trn_Entity_account = TransactionClass.objAccountEntityFrom
                                        With objEntityAccount
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .FK_Account_ID = objAccount.PK_Account_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(objEntityAccount).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntityAccount)
                                        'Address EntityAccount
                                        If TransactionClass.listObjAddressAccountEntityFrom.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_Entity_Address In TransactionClass.listObjAddressAccountEntityFrom
                                                With address
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Trn_Acc_Entity = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        'Phone EntityAccount
                                        If TransactionClass.listObjPhoneAccountEntityFrom.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Trn_Acc_Entity_Phone In TransactionClass.listObjPhoneAccountEntityFrom
                                                With phone
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Trn_Acc_Entity = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        'Director
                                        If TransactionClass.listDirectorAccountEntityFrom.Count > 0 Then
                                            For Each directorClass As SiPendarBLL.DirectorClass In TransactionClass.listDirectorAccountEntityFrom
                                                Dim director As SiPendarDAL.SIPENDAR_Trn_Director = directorClass.objDirector
                                                With director
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Entity_ID = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(director).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                                'Address Director
                                                If directorClass.listAddressDirector.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressDirector
                                                        With address
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                'Phone Director
                                                If directorClass.listPhoneDirector.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneDirector
                                                        With phone
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                'Address Employer Director
                                                If directorClass.listAddressEmployerDirector.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressEmployerDirector
                                                        With address
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                'Phone Employer Director
                                                If directorClass.listPhoneEmployerDirector.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneEmployerDirector
                                                        With phone
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                'Identification
                                                If directorClass.listIdentification.Count > 0 Then
                                                    For Each identification As SIPENDAR_Transaction_Person_Identification In directorClass.listIdentification
                                                        With identification
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Person_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(identification).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                    Next
                                                End If
                                            Next

                                        End If

                                    End If
                                    'Person
                                ElseIf objtransaction.FK_Sender_From_Information = 2 Then
                                    Dim objPerson As SIPENDAR_Transaction_Person = TransactionClass.objPersonFrom
                                    With objPerson
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objPerson).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objPerson)
                                    'Address Person
                                    If TransactionClass.listObjAddressPersonFrom.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Person_Address In TransactionClass.listObjAddressPersonFrom
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Person
                                    If TransactionClass.listObjPhonePersonFrom.Count > 0 Then
                                        For Each Phone As SiPendarDAL.SIPENDAR_trn_Person_Phone In TransactionClass.listObjPhonePersonFrom
                                            With Phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(Phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                        Next
                                    End If
                                    'Address Employer Person
                                    If TransactionClass.listObjAddressEmployerPersonFrom.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Person_Address In TransactionClass.listObjAddressEmployerPersonFrom
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Employer Person
                                    If TransactionClass.listObjPhoneEmployerPersonFrom.Count > 0 Then
                                        For Each Phone As SiPendarDAL.SIPENDAR_trn_Person_Phone In TransactionClass.listObjPhoneEmployerPersonFrom
                                            With Phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(Phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                        Next
                                    End If
                                    'Identification
                                    If TransactionClass.listObjIdentificationPersonFrom.Count > 0 Then
                                        For Each identification As SIPENDAR_Transaction_Person_Identification In TransactionClass.listObjIdentificationPersonFrom
                                            With identification
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Person_ID = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(identification).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                        Next
                                    End If

                                    'Entity
                                ElseIf objtransaction.FK_Sender_From_Information = 3 Then
                                    Dim objEntity As SIPENDAR_Transaction_Entity = TransactionClass.objEntityFrom
                                    With objEntity
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objEntity).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntity)
                                    'Address Entity
                                    If TransactionClass.ListObjAddressEntityFrom.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Entity_Address In TransactionClass.ListObjAddressEntityFrom
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Entity = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Entity
                                    If TransactionClass.listObjPhoneEntityFrom.Count > 0 Then
                                        For Each phone As SiPendarDAL.SIPENDAR_Trn_Entity_Phone In TransactionClass.listObjPhoneEntityFrom
                                            With phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Entity = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    'Director
                                    If TransactionClass.listDirectorEntityFrom.Count > 0 Then
                                        For Each directorClass As SiPendarBLL.DirectorClass In TransactionClass.listDirectorEntityFrom
                                            Dim director As SiPendarDAL.SIPENDAR_Trn_Director = directorClass.objDirector
                                            With director
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Entity_ID = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(director).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                            'Address Director
                                            If directorClass.listAddressDirector.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressDirector
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Director
                                            If directorClass.listPhoneDirector.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneDirector
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Address Employer Director
                                            If directorClass.listAddressEmployerDirector.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressEmployerDirector
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Employer Director
                                            If directorClass.listPhoneEmployerDirector.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneEmployerDirector
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Identification
                                            If directorClass.listIdentification.Count > 0 Then
                                                For Each identification As SIPENDAR_Transaction_Person_Identification In directorClass.listIdentification
                                                    With identification
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Person_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                Next
                                            End If
                                        Next

                                    End If
                                End If
                                '========================= Penerima =======================================
                                'AccountTo
                                If objtransaction.FK_Sender_To_Information = 1 Then

                                    Dim objAccount As SIPENDAR_Transaction_Account = TransactionClass.objAccountTo
                                    With objAccount
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Report_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objAccount).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccount)
                                    'signatory
                                    If TransactionClass.LisAccountSignatoryTo.Count > 0 Then
                                        For Each signatoryClass As SiPendarBLL.SignatoryClass In TransactionClass.LisAccountSignatoryTo
                                            Dim signatory As SiPendarDAL.SIPENDAR_Trn_acc_Signatory = signatoryClass.objSignatory
                                            With signatory
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Transaction_Account_ID = objAccount.PK_Account_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(signatory).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, signatory)
                                            'Address Signatory
                                            If signatoryClass.listAddressSignatory.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address In signatoryClass.listAddressSignatory
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Signatory
                                            If signatoryClass.listPhoneSignatory.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_acc_sign_Phone In signatoryClass.listPhoneSignatory
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Address Employer Signatory
                                            If signatoryClass.listAddressEmployerSignatory.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address In signatoryClass.listAddressEmployerSignatory
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Employer Signatory
                                            If signatoryClass.listPhoneEmployerSignatory.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_acc_sign_Phone In signatoryClass.listPhoneEmployerSignatory
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Acc_Entity = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'IdentificationSignatory
                                            If signatoryClass.listIdentification.Count > 0 Then
                                                For Each Identification As SIPENDAR_Transaction_Person_Identification In signatoryClass.listIdentification
                                                    With Identification
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Person_ID = signatory.PK_goAML_Trn_acc_Signatory_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(Identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Identification)
                                                Next
                                            End If
                                        Next
                                    End If

                                    If objAccount.IsRekeningKorporasi Then
                                        Dim objEntityAccount As SiPendarDAL.SIPENDAR_Trn_Entity_account = TransactionClass.objAccountEntityTo
                                        With objEntityAccount
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .FK_Account_ID = objAccount.PK_Account_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(objEntityAccount).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntityAccount)
                                        'Address EntityAccount
                                        If TransactionClass.listObjAddressAccountEntityto.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Trn_Acc_Entity_Address In TransactionClass.listObjAddressAccountEntityto
                                                With address
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Trn_Acc_Entity = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        'Phone EntityAccount
                                        If TransactionClass.listObjPhoneAccountEntityto.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Trn_Acc_Entity_Phone In TransactionClass.listObjPhoneAccountEntityto
                                                With phone
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Trn_Acc_Entity = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        'Director
                                        If TransactionClass.listDirectorAccountEntityto.Count > 0 Then
                                            For Each directorClass As SiPendarBLL.DirectorClass In TransactionClass.listDirectorAccountEntityto
                                                Dim director As SiPendarDAL.SIPENDAR_Trn_Director = directorClass.objDirector
                                                With director
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .FK_Entity_ID = objEntityAccount.PK_goAML_Trn_Entity_account
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(director).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                                'Address Director
                                                If directorClass.listAddressDirector.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressDirector
                                                        With address
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                'Phone Director
                                                If directorClass.listPhoneDirector.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneDirector
                                                        With phone
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                'Address Employer Director
                                                If directorClass.listAddressEmployerDirector.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressEmployerDirector
                                                        With address
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                'Phone Employer Director
                                                If directorClass.listPhoneEmployerDirector.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneEmployerDirector
                                                        With phone
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                'Identification
                                                If directorClass.listIdentification.Count > 0 Then
                                                    For Each identification As SIPENDAR_Transaction_Person_Identification In directorClass.listIdentification
                                                        With identification
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .FK_Person_ID = director.PK_goAML_Trn_Director_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(identification).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                    Next
                                                End If
                                            Next

                                        End If

                                    End If
                                    'Person
                                ElseIf objtransaction.FK_Sender_To_Information = 2 Then
                                    Dim objPerson As SIPENDAR_Transaction_Person = TransactionClass.objPersonTo
                                    With objPerson
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objPerson).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objPerson)
                                    'Address Person
                                    If TransactionClass.listObjAddressPersonTo.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Person_Address In TransactionClass.listObjAddressPersonTo
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Person
                                    If TransactionClass.listObjPhonePersonTo.Count > 0 Then
                                        For Each Phone As SiPendarDAL.SIPENDAR_trn_Person_Phone In TransactionClass.listObjPhonePersonTo
                                            With Phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(Phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                        Next
                                    End If
                                    'Address Employer Person
                                    If TransactionClass.listObjAddressEmployerPersonTo.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Person_Address In TransactionClass.listObjAddressEmployerPersonTo
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Employer Person
                                    If TransactionClass.listObjPhoneEmployerPersonTo.Count > 0 Then
                                        For Each Phone As SiPendarDAL.SIPENDAR_trn_Person_Phone In TransactionClass.listObjPhoneEmployerPersonTo
                                            With Phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Person = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(Phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, Phone)
                                        Next
                                    End If
                                    'Identification
                                    If TransactionClass.listObjIdentificationPersonTo.Count > 0 Then
                                        For Each identification As SIPENDAR_Transaction_Person_Identification In TransactionClass.listObjIdentificationPersonTo
                                            With identification
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Person_ID = objPerson.PK_Person_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(identification).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                        Next
                                    End If
                                    'Entity
                                ElseIf objtransaction.FK_Sender_To_Information = 3 Then
                                    Dim objEntity As SIPENDAR_Transaction_Entity = TransactionClass.objEntityTo
                                    With objEntity
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objEntity).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntity)
                                    'Address Entity
                                    If TransactionClass.ListObjAddressEntityto.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Entity_Address In TransactionClass.ListObjAddressEntityto
                                            With address
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Entity = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    'Phone Entity
                                    If TransactionClass.listObjPhoneEntityto.Count > 0 Then
                                        For Each phone As SiPendarDAL.SIPENDAR_Trn_Entity_Phone In TransactionClass.listObjPhoneEntityto
                                            With phone
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Trn_Entity = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    'Director
                                    If TransactionClass.listDirectorEntityto.Count > 0 Then
                                        For Each directorClass As SiPendarBLL.DirectorClass In TransactionClass.listDirectorEntityto
                                            Dim director As SiPendarDAL.SIPENDAR_Trn_Director = directorClass.objDirector
                                            With director
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .FK_Entity_ID = objEntity.PK_Entity_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(director).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                            'Address Director
                                            If directorClass.listAddressDirector.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressDirector
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Director
                                            If directorClass.listPhoneDirector.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneDirector
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Address Employer Director
                                            If directorClass.listAddressEmployerDirector.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Director_Address In directorClass.listAddressEmployerDirector
                                                    With address
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            'Phone Employer Director
                                            If directorClass.listPhoneEmployerDirector.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_Director_Phone In directorClass.listPhoneEmployerDirector
                                                    With phone
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Trn_Director_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            'Identification
                                            If directorClass.listIdentification.Count > 0 Then
                                                For Each identification As SIPENDAR_Transaction_Person_Identification In directorClass.listIdentification
                                                    With identification
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .FK_Person_ID = director.PK_goAML_Trn_Director_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                Next
                                            End If
                                        Next

                                    End If
                                End If
                            ElseIf objtransaction.FK_Transaction_Type = 2 Then
                                Dim objTransactionParty As SIPENDAR_Transaction_Party = TransactionClass.objTransactionParty
                                With objTransactionParty
                                    .FK_Transaction_ID = objtransaction.PK_Transaction_ID
                                    .FK_Report_ID = objReport.PK_Report_ID
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                    .Alternateby = alternateby
                                    .Active = True
                                End With
                                objdb.Entry(objTransactionParty).State = Entity.EntityState.Added
                                objdb.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objTransactionParty)
                                If objTransactionParty.fk_ref_detail_of = 1 Then
                                    Dim objAccountParty As SiPendarDAL.SIPENDAR_Trn_Party_Account = TransactionClass.objAccountParty
                                    With objAccountParty
                                        .FK_Trn_Party_ID = objTransactionParty.PK_Trn_Party_ID
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objAccountParty).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccountParty)
                                    If TransactionClass.listSignatoryAccountParty.Count > 0 Then
                                        For Each signatoryClass As SiPendarBLL.SignatoryAccountPartyClass In TransactionClass.listSignatoryAccountParty
                                            Dim objsignatory As SiPendarDAL.SIPENDAR_Trn_par_acc_Signatory = signatoryClass.objSignatoryAccountParty
                                            With objsignatory
                                                .FK_Trn_Party_Account_ID = objAccountParty.PK_Trn_Party_Account_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(objsignatory).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objsignatory)
                                            If signatoryClass.listAddress.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_par_Acc_sign_Address In signatoryClass.listAddress
                                                    With address
                                                        .FK_Trn_par_acc_Signatory_ID = objsignatory.PK_Trn_par_acc_Signatory_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If signatoryClass.listAddressEmployer.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_par_Acc_sign_Address In signatoryClass.listAddressEmployer
                                                    With address
                                                        .FK_Trn_par_acc_Signatory_ID = objsignatory.PK_Trn_par_acc_Signatory_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If signatoryClass.listPhone.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_par_acc_sign_Phone In signatoryClass.listPhone
                                                    With phone
                                                        .FK_Trn_Par_Acc_Sign = objsignatory.PK_Trn_par_acc_Signatory_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If signatoryClass.listPhoneEmployer.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_trn_par_acc_sign_Phone In signatoryClass.listPhoneEmployer
                                                    With phone
                                                        .FK_Trn_Par_Acc_Sign = objsignatory.PK_Trn_par_acc_Signatory_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If signatoryClass.listIdentification.Count > 0 Then
                                                For Each identification As SIPENDAR_Transaction_Party_Identification In signatoryClass.listIdentification
                                                    With identification
                                                        .FK_Person_ID = objsignatory.PK_Trn_par_acc_Signatory_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                Next
                                            End If
                                        Next

                                    End If
                                    If objAccountParty.isRekeningKorporasi Then
                                        Dim objaccountEntity As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Entity = TransactionClass.objAccountEntityParty
                                        With objaccountEntity
                                            .FK_Trn_Party_Account_ID = objAccountParty.PK_Trn_Party_Account_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(objaccountEntity).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objaccountEntity)
                                        If TransactionClass.listAddresAccountEntityParty.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Trn_par_Acc_Entity_Address In TransactionClass.listAddresAccountEntityParty
                                                With address
                                                    .FK_Trn_par_acc_Entity_ID = objaccountEntity.PK_Trn_Par_acc_Entity_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        If TransactionClass.listPhoneAccountEntityParty.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_trn_par_acc_Entity_Phone In TransactionClass.listPhoneAccountEntityParty
                                                With phone
                                                    .FK_Trn_par_acc_Entity_ID = objaccountEntity.PK_Trn_Par_acc_Entity_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        If TransactionClass.listDirectorEntytAccountparty.Count > 0 Then
                                            For Each directorClass As SiPendarBLL.DirectorEntityAccountPartyClass In TransactionClass.listDirectorEntytAccountparty
                                                Dim director As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director = directorClass.objDirectorEntityAccountParty
                                                With director
                                                    .FK_Trn_Par_Acc_Entity_ID = objaccountEntity.PK_Trn_Par_acc_Entity_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(director).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                                If directorClass.listAddress.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Address In directorClass.listAddress
                                                        With address
                                                            .FK_Trn_par_acc_Entity_Director_ID = director.PK_Trn_Par_Acc_Ent_Director_ID
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                If directorClass.listAddressEmployer.Count > 0 Then
                                                    For Each address As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Address In directorClass.listAddressEmployer
                                                        With address
                                                            .FK_Trn_par_acc_Entity_Director_ID = director.PK_Trn_Par_Acc_Ent_Director_ID
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(address).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                    Next
                                                End If
                                                If directorClass.listPhone.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Phone In directorClass.listPhone
                                                        With phone
                                                            .FK_Trn_par_acc_Entity_Director_ID = director.PK_Trn_Par_Acc_Ent_Director_ID
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                If directorClass.listPhoneEmployer.Count > 0 Then
                                                    For Each phone As SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Phone In directorClass.listPhoneEmployer
                                                        With phone
                                                            .FK_Trn_par_acc_Entity_Director_ID = director.PK_Trn_Par_Acc_Ent_Director_ID
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                    Next
                                                End If
                                                If directorClass.listIdentification.Count > 0 Then
                                                    For Each identification As SIPENDAR_Transaction_Party_Identification In directorClass.listIdentification
                                                        With identification
                                                            .FK_Person_ID = director.PK_Trn_Par_Acc_Ent_Director_ID
                                                            .FK_Report_ID = objReport.PK_Report_ID
                                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .CreatedDate = DateTime.Now
                                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .LastUpdateDate = DateTime.Now
                                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                            .ApprovedDate = DateTime.Now
                                                            .Alternateby = alternateby
                                                            .Active = True
                                                        End With
                                                        objdb.Entry(identification).State = Entity.EntityState.Added
                                                        objdb.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                    Next
                                                End If
                                            Next
                                        End If
                                    End If
                                ElseIf objTransactionParty.fk_ref_detail_of = 2 Then
                                    Dim objpersonParty As SiPendarDAL.SIPENDAR_Trn_Party_Person = TransactionClass.objPersonParty
                                    With objpersonParty
                                        .FK_Transaction_Party_ID = objTransactionParty.PK_Trn_Party_ID
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objpersonParty).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objpersonParty)
                                    If TransactionClass.listAddressPersonParty.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Party_Person_Address In TransactionClass.listAddressPersonParty
                                            With address
                                                .FK_Trn_Party_Person_ID = objpersonParty.PK_Trn_Party_Person_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    If TransactionClass.listAddressEmployerPersonParty.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Trn_Party_Person_Address In TransactionClass.listAddressEmployerPersonParty
                                            With address
                                                .FK_Trn_Party_Person_ID = objpersonParty.PK_Trn_Party_Person_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    If TransactionClass.listPhonePersonParty.Count > 0 Then
                                        For Each phone As SiPendarDAL.SIPENDAR_Trn_Party_Person_Phone In TransactionClass.listPhonePersonParty
                                            With phone
                                                .FK_Trn_Party_Person_ID = objpersonParty.PK_Trn_Party_Person_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    If TransactionClass.listPhoneEmployerPersonParty.Count > 0 Then
                                        For Each phone As SiPendarDAL.SIPENDAR_Trn_Party_Person_Phone In TransactionClass.listPhoneEmployerPersonParty
                                            With phone
                                                .FK_Trn_Party_Person_ID = objpersonParty.PK_Trn_Party_Person_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    If TransactionClass.listIdentificationPersonParty.Count > 0 Then
                                        For Each identification As SIPENDAR_Transaction_Party_Identification In TransactionClass.listIdentificationPersonParty
                                            With identification
                                                .FK_Person_ID = objpersonParty.PK_Trn_Party_Person_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(identification).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                        Next
                                    End If
                                ElseIf objTransactionParty.fk_ref_detail_of = 3 Then
                                    Dim objEntityParty As SiPendarDAL.SIPENDAR_Trn_Party_Entity = TransactionClass.objentityParty
                                    With objEntityParty
                                        .FK_Transaction_Party_ID = objTransactionParty.PK_Trn_Party_ID
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objEntityParty).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntityParty)
                                    If TransactionClass.listAddressEntityparty.Count > 0 Then
                                        For Each address In TransactionClass.listAddressEntityparty
                                            With address
                                                .FK_Trn_Party_Entity_ID = objEntityParty.PK_Trn_Party_Entity_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    If TransactionClass.listPhoneEntityParty.Count > 0 Then
                                        For Each phone In TransactionClass.listPhoneEntityParty
                                            With phone
                                                .FK_Trn_Party_Entity_ID = objEntityParty.PK_Trn_Party_Entity_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    If TransactionClass.listDirectorEntityParty.Count > 0 Then
                                        For Each directorClass As SiPendarBLL.DirectorEntityPartyClass In TransactionClass.listDirectorEntityParty
                                            Dim director As SiPendarDAL.SIPENDAR_Trn_Par_Entity_Director = directorClass.objDirectorEntityParty
                                            With director
                                                .FK_Trn_Party_Entity_ID = objEntityParty.PK_Trn_Party_Entity_ID
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(director).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                            If directorClass.listAddress.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Par_Entity_Director_Address In directorClass.listAddress
                                                    With address
                                                        .FK_Trn_par_Entity_Director_ID = director.PK_Trn_Par_Entity_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If directorClass.listAddressEmployer.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Trn_Par_Entity_Director_Address In directorClass.listAddressEmployer
                                                    With address
                                                        .FK_Trn_par_Entity_Director_ID = director.PK_Trn_Par_Entity_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If directorClass.listPhone.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_Trn_Par_Entity_Director_Phone In directorClass.listPhone
                                                    With phone
                                                        .FK_Trn_par_Entity_Director_ID = director.PK_Trn_Par_Entity_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If directorClass.listPhoneEmployer.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_Trn_Par_Entity_Director_Phone In directorClass.listPhoneEmployer
                                                    With phone
                                                        .FK_Trn_par_Entity_Director_ID = director.PK_Trn_Par_Entity_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If directorClass.listIdentification.Count > 0 Then
                                                For Each identification As SIPENDAR_Transaction_Party_Identification In directorClass.listIdentification
                                                    With identification
                                                        .FK_Person_ID = director.PK_Trn_Par_Entity_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                Next
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        Next
                    ElseIf getTrnORActivity(objReport.Report_Code) = "ACT" Then
                        For Each activityClass As SiPendarBLL.ActivityClass In report.listObjActivity
                            Dim objActivity As SiPendarDAL.SIPENDAR_Act_ReportPartyType = activityClass.objActivity
                            With objActivity
                                .FK_Report_ID = objReport.PK_Report_ID
                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .CreatedDate = DateTime.Now
                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .LastUpdateDate = DateTime.Now
                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                .ApprovedDate = DateTime.Now
                                .Alternateby = alternateby
                                .Active = True
                            End With
                            objdb.Entry(objActivity).State = Entity.EntityState.Added
                            objdb.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objActivity)

                            '======== Activity Account===========
                            If objActivity.SubNodeType = 1 Then
                                Dim objAccountActivity As SiPendarDAL.SIPENDAR_Act_Account = activityClass.objAccountActivity
                                With objAccountActivity
                                    .FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID
                                    .FK_Report_ID = objReport.PK_Report_ID
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                    .Alternateby = alternateby
                                    .Active = True
                                End With
                                objdb.Entry(objAccountActivity).State = Entity.EntityState.Added
                                objdb.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objAccountActivity)

                                If activityClass.listSignatoryAccountActivity.Count > 0 Then
                                    For Each signatoryClass As SiPendarBLL.SignatoryAccountActivityClass In activityClass.listSignatoryAccountActivity
                                        Dim objsignatory As SiPendarDAL.SIPENDAR_Act_acc_Signatory = signatoryClass.objSignatoryAccountActivity
                                        With objsignatory
                                            .FK_Activity_Account_ID = objAccountActivity.PK_goAML_Act_Account_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(objsignatory).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objsignatory)
                                        If signatoryClass.listAddress.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Act_Acc_sign_Address In signatoryClass.listAddress
                                                With address
                                                    .FK_Act_Acc_Entity = objsignatory.PK_goAML_Act_acc_Signatory_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        If signatoryClass.listAddressEmployer.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Act_Acc_sign_Address In signatoryClass.listAddressEmployer
                                                With address
                                                    .FK_Act_Acc_Entity = objsignatory.PK_goAML_Act_acc_Signatory_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        If signatoryClass.listPhone.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Act_Acc_sign_Phone In signatoryClass.listPhone
                                                With phone
                                                    .FK_Act_Acc_Entity_ID = objsignatory.PK_goAML_Act_acc_Signatory_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        If signatoryClass.listPhoneEmployer.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Act_Acc_sign_Phone In signatoryClass.listPhoneEmployer
                                                With phone
                                                    .FK_Act_Acc_Entity_ID = objsignatory.PK_goAML_Act_acc_Signatory_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        If signatoryClass.listIdentification.Count > 0 Then
                                            For Each identification As SiPendarDAL.SIPENDAR_Activity_Person_Identification In signatoryClass.listIdentification
                                                With identification
                                                    .FK_Act_Person_ID = objsignatory.PK_goAML_Act_acc_Signatory_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(identification).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                            Next
                                        End If
                                    Next

                                End If
                                If objAccountActivity.IsRekeningKorporasi Then
                                    Dim objaccountEntity As SiPendarDAL.SIPENDAR_Act_Entity_Account = activityClass.objAccountEntityActivity
                                    With objaccountEntity
                                        .FK_Activity_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID
                                        .FK_Act_Account_ID = objAccountActivity.PK_goAML_Act_Account_ID
                                        .FK_Report_ID = objReport.PK_Report_ID
                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .CreatedDate = DateTime.Now
                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .LastUpdateDate = DateTime.Now
                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                        .ApprovedDate = DateTime.Now
                                        .Alternateby = alternateby
                                        .Active = True
                                    End With
                                    objdb.Entry(objaccountEntity).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objaccountEntity)
                                    If activityClass.listAddresAccountEntityActivity.Count > 0 Then
                                        For Each address As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Address In activityClass.listAddresAccountEntityActivity
                                            With address
                                                .FK_Act_Acc_Entity_ID = objaccountEntity.PK_goAML_Act_Entity_account
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(address).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                        Next
                                    End If
                                    If activityClass.listPhoneAccountEntityActivity.Count > 0 Then
                                        For Each phone As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Phone In activityClass.listPhoneAccountEntityActivity
                                            With phone
                                                .FK_Act_Acc_Entity_ID = objaccountEntity.PK_goAML_Act_Entity_account
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(phone).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                        Next
                                    End If
                                    If activityClass.listDirectorEntytAccountActivity.Count > 0 Then
                                        For Each directorClass As SiPendarBLL.DirectorEntityAccountActivityClass In activityClass.listDirectorEntytAccountActivity
                                            Dim director As SiPendarDAL.SIPENDAR_Act_Acc_Ent_Director = directorClass.objDirectorEntityAccountActivity
                                            With director
                                                .FK_Act_Acc_Entity_ID = objaccountEntity.PK_goAML_Act_Entity_account
                                                .FK_Report_ID = objReport.PK_Report_ID
                                                .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .CreatedDate = DateTime.Now
                                                .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .LastUpdateDate = DateTime.Now
                                                .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                .ApprovedDate = DateTime.Now
                                                .Alternateby = alternateby
                                                .Active = True
                                            End With
                                            objdb.Entry(director).State = Entity.EntityState.Added
                                            objdb.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                            If directorClass.listAddress.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_address In directorClass.listAddress
                                                    With address
                                                        .FK_Act_Entity_Director_ID = director.PK_Act_Acc_Ent_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If directorClass.listAddressEmployer.Count > 0 Then
                                                For Each address As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_address In directorClass.listAddressEmployer
                                                    With address
                                                        .FK_Act_Entity_Director_ID = director.PK_Act_Acc_Ent_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(address).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                                Next
                                            End If
                                            If directorClass.listPhone.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_Phone In directorClass.listPhone
                                                    With phone
                                                        .FK_Act_Entity_Director_ID = director.PK_Act_Acc_Ent_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If directorClass.listPhoneEmployer.Count > 0 Then
                                                For Each phone As SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_Phone In directorClass.listPhoneEmployer
                                                    With phone
                                                        .FK_Act_Entity_Director_ID = director.PK_Act_Acc_Ent_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(phone).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                                Next
                                            End If
                                            If directorClass.listIdentification.Count > 0 Then
                                                For Each identification As SiPendarDAL.SIPENDAR_Activity_Person_Identification In directorClass.listIdentification
                                                    With identification
                                                        .FK_Act_Person_ID = director.PK_Act_Acc_Ent_Director_ID
                                                        .FK_Report_ID = objReport.PK_Report_ID
                                                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .CreatedDate = DateTime.Now
                                                        .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .LastUpdateDate = DateTime.Now
                                                        .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                        .ApprovedDate = DateTime.Now
                                                        .Alternateby = alternateby
                                                        .Active = True
                                                    End With
                                                    objdb.Entry(identification).State = Entity.EntityState.Added
                                                    objdb.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                                Next
                                            End If
                                        Next
                                    End If
                                End If
                                '===========Person Activity================
                            ElseIf objActivity.SubNodeType = 2 Then
                                Dim objpersonActivity As SiPendarDAL.SIPENDAR_Act_Person = activityClass.objPersonActivity
                                With objpersonActivity
                                    .FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID
                                    .FK_Report_ID = objReport.PK_Report_ID
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                    .Alternateby = alternateby
                                    .Active = True
                                End With
                                objdb.Entry(objpersonActivity).State = Entity.EntityState.Added
                                objdb.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objpersonActivity)
                                If activityClass.listAddressPersonActivity.Count > 0 Then
                                    For Each address As SiPendarDAL.SIPENDAR_Act_Person_Address In activityClass.listAddressPersonActivity
                                        With address
                                            .FK_Act_Person = objpersonActivity.PK_goAML_Act_Person_ID
                                            .fk_report_id = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(address).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                    Next
                                End If
                                If activityClass.listAddressEmployerPersonActivity.Count > 0 Then
                                    For Each address As SiPendarDAL.SIPENDAR_Act_Person_Address In activityClass.listAddressEmployerPersonActivity
                                        With address
                                            .FK_Act_Person = objpersonActivity.PK_goAML_Act_Person_ID
                                            .fk_report_id = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(address).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                    Next
                                End If
                                If activityClass.listPhonePersonActivity.Count > 0 Then
                                    For Each phone As SiPendarDAL.SIPENDAR_Act_Person_Phone In activityClass.listPhonePersonActivity
                                        With phone
                                            .FK_Act_Person = objpersonActivity.PK_goAML_Act_Person_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                    Next
                                End If
                                If activityClass.listPhoneEmployerPersonActivity.Count > 0 Then
                                    For Each phone As SiPendarDAL.SIPENDAR_Act_Person_Phone In activityClass.listPhoneEmployerPersonActivity
                                        With phone
                                            .FK_Act_Person = objpersonActivity.PK_goAML_Act_Person_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                    Next
                                End If
                                If activityClass.listIdentificationPersonActivity.Count > 0 Then
                                    For Each identification As SiPendarDAL.SIPENDAR_Activity_Person_Identification In activityClass.listIdentificationPersonActivity
                                        With identification
                                            .FK_Act_Person_ID = objpersonActivity.PK_goAML_Act_Person_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(identification).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                    Next
                                End If
                                '=============== Entity Activity ===================
                            ElseIf objActivity.SubNodeType = 3 Then
                                Dim objEntityActivity As SiPendarDAL.SIPENDAR_Act_Entity = activityClass.objentityActivity
                                With objEntityActivity
                                    .FK_Act_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID
                                    .FK_Report_ID = objReport.PK_Report_ID
                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .CreatedDate = DateTime.Now
                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .LastUpdateDate = DateTime.Now
                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    .ApprovedDate = DateTime.Now
                                    .Alternateby = alternateby
                                    .Active = True
                                End With
                                objdb.Entry(objEntityActivity).State = Entity.EntityState.Added
                                objdb.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, objEntityActivity)
                                If activityClass.listAddressEntityActivity.Count > 0 Then
                                    For Each address In activityClass.listAddressEntityActivity
                                        With address
                                            .FK_Act_Entity_ID = objEntityActivity.PK_goAML_Act_Entity_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(address).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                    Next
                                End If
                                If activityClass.listPhoneEntityActivity.Count > 0 Then
                                    For Each phone In activityClass.listPhoneEntityActivity
                                        With phone
                                            .FK_Act_Entity_ID = objEntityActivity.PK_goAML_Act_Entity_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(phone).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                    Next
                                End If
                                If activityClass.listDirectorEntityActivity.Count > 0 Then
                                    For Each directorClass As SiPendarBLL.DirectorEntityActivityClass In activityClass.listDirectorEntityActivity
                                        Dim director As SiPendarDAL.SIPENDAR_Act_Director = directorClass.objDirectorEntityActivity
                                        With director
                                            .FK_Act_Entity_ID = objEntityActivity.PK_goAML_Act_Entity_ID
                                            .FK_ReportParty_ID = objActivity.PK_goAML_Act_ReportPartyType_ID
                                            .FK_Report_ID = objReport.PK_Report_ID
                                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .CreatedDate = DateTime.Now
                                            .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .LastUpdateDate = DateTime.Now
                                            .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                            .ApprovedDate = DateTime.Now
                                            .Alternateby = alternateby
                                            .Active = True
                                        End With
                                        objdb.Entry(director).State = Entity.EntityState.Added
                                        objdb.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, director)
                                        If directorClass.listAddress.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Act_Director_Address In directorClass.listAddress
                                                With address
                                                    .FK_Act_Director_ID = director.PK_goAML_Act_Director_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        If directorClass.listAddressEmployer.Count > 0 Then
                                            For Each address As SiPendarDAL.SIPENDAR_Act_Director_Address In directorClass.listAddressEmployer
                                                With address
                                                    .FK_Act_Director_ID = director.PK_goAML_Act_Director_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(address).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, address)
                                            Next
                                        End If
                                        If directorClass.listPhone.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Act_Director_Phone In directorClass.listPhone
                                                With phone
                                                    .FK_Act_Director_ID = director.PK_goAML_Act_Director_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        If directorClass.listPhoneEmployer.Count > 0 Then
                                            For Each phone As SiPendarDAL.SIPENDAR_Act_Director_Phone In directorClass.listPhoneEmployer
                                                With phone
                                                    .FK_Act_Director_ID = director.PK_goAML_Act_Director_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(phone).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, phone)
                                            Next
                                        End If
                                        If directorClass.listIdentification.Count > 0 Then
                                            For Each identification As SiPendarDAL.SIPENDAR_Activity_Person_Identification In directorClass.listIdentification
                                                With identification
                                                    .FK_Act_Person_ID = director.PK_goAML_Act_Director_ID
                                                    .FK_Report_ID = objReport.PK_Report_ID
                                                    .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .CreatedDate = DateTime.Now
                                                    .LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .LastUpdateDate = DateTime.Now
                                                    .ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                                    .ApprovedDate = DateTime.Now
                                                    .Alternateby = alternateby
                                                    .Active = True
                                                End With
                                                objdb.Entry(identification).State = Entity.EntityState.Added
                                                objdb.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objaudittrailheader.PK_AuditTrail_ID, identification)
                                            Next
                                        End If
                                    Next
                                End If
                            End If
                        Next
                    End If
                    objdb.SaveChanges()
                    objdb.Database.ExecuteSqlCommand("exec usp_updateEntityReference")
                    objdb.Database.ExecuteSqlCommand("exec usp_UpdategoAML_Generate_XML")
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Function saveDeleteReport(report As Report, objmodule As NawaDAL.Module, OldObjReport As SIPENDAR_Report, IsUseApproval As Boolean)
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    Dim objReport As New SIPENDAR_Report
                    objReport = report.objReport
                    objReport.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID

                    If IsUseApproval Then
                        objReport.Status = 4    'Waiting for Approval
                        objdb.Entry(objReport).State = Entity.EntityState.Modified
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete    'Daniel 11 Des 2020 change to delete
                        Dim modulename As String = objmodule.ModuleLabel

                        Dim objaudittrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        'NawaFramework.CreateAuditTrailDetailEdit(objdb, objaudittrailheader.PK_AuditTrail_ID, objReport, OldObjReport)
                    Else
                        objReport.Status = 1    'Waiting for Generate
                        objReport.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        objReport.ApprovedDate = Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")

                        objdb.Entry(objReport).State = Entity.EntityState.Deleted
                        objdb.SaveChanges()

                        Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                        Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                        Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                        Dim modulename As String = objmodule.ModuleLabel

                        Dim objaudittrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                        'NawaFramework.CreateAuditTrailDetailDelete(objdb, objaudittrailheader.PK_AuditTrail_ID, objReport)

                        'khusus yg pake Generate SAR, kalau tidak boleh di komen function nya
                        GenerateSarBLL.UpdateStatusGenerateSAR(objReport.PK_Report_ID)

                        'Dim paramChangeStatus(0) As SqlParameter
                        'paramChangeStatus(0) = New SqlParameter
                        'paramChangeStatus(0).ParameterName = "@SP_Pemanggil"
                        'paramChangeStatus(0).Value = "usp_SIPENDAR_DeleteReport"
                        'paramChangeStatus(0).DbType = SqlDbType.VarChar
                        'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_UpdateStatus", paramChangeStatus)
                    End If


                    objdb.Database.ExecuteSqlCommand("exec usp_updateEntityReference")
                    objdb.Database.ExecuteSqlCommand("exec usp_UpdategoAML_Generate_XML")
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Function

    Shared Function saveApprovalDeleteReport(report As SIPENDAR_Report, objmodule As NawaDAL.Module)
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim alternateby As String = ""
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    End If
                    Dim objReport As SIPENDAR_Report = report 'As New SiPendarDAL.SIPENDAR_Report
                    'objReport = report

                    objReport.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objReport.ApprovedDate = Now 'Now.ToString("yyyy-MM-dd HH:mm:ss")

                    objdb.Entry(objReport).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objmodule.ModuleLabel

                    Dim objaudittrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, user, act, acts, modulename)
                    'NawaFramework.CreateAuditTrailDetailDelete(objdb, objaudittrailheader.PK_AuditTrail_ID, objReport)
                    'khusus yg pake Generate SAR, kalau tidak boleh di komen function nya
                    GenerateSarBLL.UpdateStatusGenerateSAR(objReport.PK_Report_ID)

                    'Dim paramChangeStatus(0) As SqlParameter
                    'paramChangeStatus(0) = New SqlParameter
                    'paramChangeStatus(0).ParameterName = "@SP_Pemanggil"
                    'paramChangeStatus(0).Value = "usp_SIPENDAR_DeleteReport"
                    'paramChangeStatus(0).DbType = SqlDbType.VarChar
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_UpdateStatus", paramChangeStatus)

                    objdb.Database.ExecuteSqlCommand("exec usp_updateEntityReference")
                    objdb.Database.ExecuteSqlCommand("exec usp_UpdategoAML_Generate_XML")
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Sub DeleteAllSubNodesDataReport(IDReport As String, objModule As NawaDAL.Module)
        Using objDB As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    '------ Report
                    Dim Old_ObjReport = objDB.SIPENDAR_Report.Where(Function(x) x.PK_Report_ID = IDReport).FirstOrDefault

                    '------ Create Audit Trail
                    Dim user As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim act As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim acts As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim modulename As String = objModule.ModuleLabel

                    Dim objaudittrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, user, act, acts, modulename)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_ObjReport)

                    '------ Indicator
                    Dim Old_ListIndicator = objDB.SIPENDAR_Report_Indicator.Where(Function(x) x.FK_Report = IDReport).ToList
                    If Old_ListIndicator IsNot Nothing Then
                        For Each itemx In Old_ListIndicator
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx)
                        Next
                    End If

                    '====================== TRANSAKSI (TRN) ============================================================================
                    If getTrnORActivity(Old_ObjReport.Report_Code) = "TRN" Then
                        Dim Old_listTransaction = objDB.SIPENDAR_Transaction.Where(Function(X) X.FK_Report_ID = Old_ObjReport.PK_Report_ID).ToList
                        If Old_listTransaction IsNot Nothing Then
                            For Each itemx In Old_listTransaction
                                objDB.Entry(itemx).State = Entity.EntityState.Deleted
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx)
                            Next
                        End If

                        For Each item As SIPENDAR_Transaction In Old_listTransaction

                            '=================== BI-PARTY ===================================================
                            If item.FK_Transaction_Type = 1 Then

                                'Conductor
                                If item.usedConductor Then
                                    Dim Old_Conductor = objDB.SIPENDAR_Trn_Conductor.Where(Function(x) x.FK_Transaction_ID = item.PK_Transaction_ID).FirstOrDefault
                                    If Not Old_Conductor Is Nothing Then

                                        Dim Old_listObjPhoneConductor = objDB.SIPENDAR_trn_Conductor_Phone.Where(Function(x) x.FK_Trn_Conductor_ID = Old_Conductor.PK_goAML_Trn_Conductor_ID).ToList
                                        Dim Old_listObjAddressConductor = objDB.SIPENDAR_Trn_Conductor_Address.Where(Function(x) x.FK_Trn_Conductor_ID = Old_Conductor.PK_goAML_Trn_Conductor_ID).ToList
                                        Dim Old_listObjIdentificationConductor = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Old_Conductor.PK_goAML_Trn_Conductor_ID And x.FK_Person_Type = 3 And x.from_or_To_Type = 1).ToList

                                        For Each itemx1 In Old_listObjPhoneConductor
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listObjAddressConductor
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next
                                        For Each itemx3 In Old_listObjIdentificationConductor
                                            objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                        Next

                                        objDB.Entry(Old_Conductor).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_Conductor)
                                    End If
                                End If

                                '------ PENGIRIM (FROM)
                                If item.FK_Sender_From_Information = 1 Then 'Account From
                                    Dim Old_objAccountFrom = objDB.SIPENDAR_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = item.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault

                                    If Not Old_objAccountFrom Is Nothing Then
                                        'Signatory Account From
                                        Dim Old_listSignatory = objDB.SIPENDAR_Trn_acc_Signatory.Where(Function(x) x.FK_Transaction_Account_ID = Old_objAccountFrom.PK_Account_ID And x.FK_From_Or_To = 1).ToList
                                        For Each objSignatory As SIPENDAR_Trn_acc_Signatory In Old_listSignatory
                                            Dim Old_listPhoneSignatory = objDB.SIPENDAR_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID).ToList
                                            Dim Old_listAddressSignatory = objDB.SIPENDAR_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 1).ToList

                                            For Each itemx1 In Old_listPhoneSignatory
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listAddressSignatory
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(objSignatory).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objSignatory)
                                        Next

                                        'Account From Korporasi
                                        If Old_objAccountFrom.IsRekeningKorporasi Then
                                            Dim Old_objAccountEntityFrom = objDB.SIPENDAR_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = Old_objAccountFrom.PK_Account_ID And x.FK_From_Or_To = 1).FirstOrDefault

                                            If Not Old_objAccountEntityFrom Is Nothing Then
                                                'Address and Phone
                                                Dim Old_listObjAddressAccountEntityFrom = objDB.SIPENDAR_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Old_objAccountEntityFrom.PK_goAML_Trn_Entity_account).ToList
                                                Dim Old_listObjPhoneAccountEntityFrom = objDB.SIPENDAR_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Old_objAccountEntityFrom.PK_goAML_Trn_Entity_account).ToList

                                                For Each itemx1 In Old_listObjAddressAccountEntityFrom
                                                    objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                Next
                                                For Each itemx2 In Old_listObjPhoneAccountEntityFrom
                                                    objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                Next

                                                'Director
                                                Dim Old_listDirectorEntityAccount = objDB.SIPENDAR_Trn_Director.Where(Function(x) x.FK_Entity_ID = Old_objAccountEntityFrom.PK_goAML_Trn_Entity_account And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 1).ToList
                                                For Each objDirector As SIPENDAR_Trn_Director In Old_listDirectorEntityAccount
                                                    Dim Old_listPhoneDirector = objDB.SIPENDAR_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                                    Dim Old_listAddressDirector = objDB.SIPENDAR_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                                    Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 1).ToList

                                                    For Each itemx1 In Old_listPhoneDirector
                                                        objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                    Next
                                                    For Each itemx2 In Old_listAddressDirector
                                                        objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                    Next
                                                    For Each itemx3 In Old_listIdentification
                                                        objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                                    Next

                                                    objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)
                                                Next

                                                objDB.Entry(Old_objAccountEntityFrom).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountEntityFrom)
                                            End If

                                            objDB.Entry(Old_objAccountFrom).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountFrom)
                                        End If
                                    End If
                                ElseIf item.FK_Sender_From_Information = 2 Then 'Person From
                                    Dim Old_objPersonFrom = objDB.SIPENDAR_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = item.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault
                                    If Not Old_objPersonFrom Is Nothing Then
                                        Dim Old_listObjPhonePersonFrom = objDB.SIPENDAR_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = Old_objPersonFrom.PK_Person_ID).ToList
                                        Dim Old_listObjAddressPersonFrom = objDB.SIPENDAR_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = Old_objPersonFrom.PK_Person_ID).ToList
                                        Dim Old_listObjIdentificationPersonFrom = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Old_objPersonFrom.PK_Person_ID And x.FK_Person_Type = 1 And x.from_or_To_Type = 1).ToList

                                        For Each itemx1 In Old_listObjPhonePersonFrom
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listObjAddressPersonFrom
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next
                                        For Each itemx3 In Old_listObjIdentificationPersonFrom
                                            objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                        Next

                                        objDB.Entry(Old_objPersonFrom).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objPersonFrom)
                                    End If
                                ElseIf item.FK_Sender_From_Information = 3 Then 'Entity From
                                    Dim Old_objEntityFrom = objDB.SIPENDAR_Transaction_Entity.Where(Function(x) x.FK_Transaction_ID = item.PK_Transaction_ID And x.FK_From_Or_To = 1).FirstOrDefault

                                    If Not Old_objEntityFrom Is Nothing Then
                                        'Address and Phone
                                        Dim Old_ListObjAddressEntityFrom = objDB.SIPENDAR_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = Old_objEntityFrom.PK_Entity_ID).ToList
                                        Dim Old_listObjPhoneEntityFrom = objDB.SIPENDAR_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = Old_objEntityFrom.PK_Entity_ID).ToList

                                        For Each itemx1 In Old_ListObjAddressEntityFrom
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listObjPhoneEntityFrom
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next

                                        'Director
                                        Dim Old_listDirectorEntity = objDB.SIPENDAR_Trn_Director.Where(Function(x) x.FK_Entity_ID = Old_objEntityFrom.PK_Entity_ID And x.FK_From_Or_To = 1 And x.FK_Sender_Information = 3)
                                        For Each objDirector As SIPENDAR_Trn_Director In Old_listDirectorEntity
                                            Dim Old_listPhoneDirector = objDB.SIPENDAR_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                            Dim Old_listAddressDirector = objDB.SIPENDAR_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 1)

                                            For Each itemx1 In Old_listPhoneDirector
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listAddressDirector
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)
                                        Next

                                        objDB.Entry(Old_objEntityFrom).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objEntityFrom)
                                    End If
                                End If

                                '------ PENERIMA (TO)
                                If item.FK_Sender_To_Information = 1 Then 'Account To
                                    Dim Old_objAccountTo = objDB.SIPENDAR_Transaction_Account.Where(Function(x) x.FK_Report_Transaction_ID = item.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault

                                    If Not Old_objAccountTo Is Nothing Then
                                        Dim Old_listSignatoryTo = objDB.SIPENDAR_Trn_acc_Signatory.Where(Function(X) X.FK_Transaction_Account_ID = Old_objAccountTo.PK_Account_ID And X.FK_From_Or_To = 2).ToList
                                        For Each objSignatory As SIPENDAR_Trn_acc_Signatory In Old_listSignatoryTo
                                            Dim Old_listPhoneSignatory = objDB.SIPENDAR_trn_acc_sign_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID).ToList
                                            Dim Old_listAddressSignatory = objDB.SIPENDAR_Trn_Acc_sign_Address.Where(Function(x) x.FK_Trn_Acc_Entity = objSignatory.PK_goAML_Trn_acc_Signatory_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = objSignatory.PK_goAML_Trn_acc_Signatory_ID And x.FK_Person_Type = 2 And x.from_or_To_Type = 2).ToList

                                            For Each itemx1 In Old_listPhoneSignatory
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listAddressSignatory
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(objSignatory).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objSignatory)
                                        Next

                                        'Account To - Rekening Korporasi
                                        If Old_objAccountTo.IsRekeningKorporasi Then
                                            Dim Old_objAccountEntityTo = objDB.SIPENDAR_Trn_Entity_account.Where(Function(x) x.FK_Account_ID = Old_objAccountTo.PK_Account_ID And x.FK_From_Or_To = 2).FirstOrDefault

                                            If Not Old_objAccountEntityTo Is Nothing Then
                                                'Address and Phone
                                                Dim Old_listObjAddressAccountEntityto = objDB.SIPENDAR_Trn_Acc_Entity_Address.Where(Function(x) x.FK_Trn_Acc_Entity = Old_objAccountEntityTo.PK_goAML_Trn_Entity_account).ToList
                                                Dim Old_listObjPhoneAccountEntityto = objDB.SIPENDAR_Trn_Acc_Entity_Phone.Where(Function(x) x.FK_Trn_Acc_Entity = Old_objAccountEntityTo.PK_goAML_Trn_Entity_account).ToList

                                                For Each itemx1 In Old_listObjAddressAccountEntityto
                                                    objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                Next
                                                For Each itemx2 In Old_listObjPhoneAccountEntityto
                                                    objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                Next

                                                'Director
                                                Dim Old_listDirectorEntityAccountTo As List(Of SIPENDAR_Trn_Director) = objDB.SIPENDAR_Trn_Director.Where(Function(X) X.FK_Entity_ID = Old_objAccountEntityTo.PK_goAML_Trn_Entity_account And X.FK_From_Or_To = 2 And X.FK_Sender_Information = 1).ToList
                                                For Each objDirector As SIPENDAR_Trn_Director In Old_listDirectorEntityAccountTo
                                                    Dim Old_listPhoneDirector = objDB.SIPENDAR_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                                    Dim Old_listAddressDirector = objDB.SIPENDAR_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                                    Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 2).ToList

                                                    For Each itemx1 In Old_listPhoneDirector
                                                        objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                    Next
                                                    For Each itemx2 In Old_listAddressDirector
                                                        objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                    Next
                                                    For Each itemx3 In Old_listIdentification
                                                        objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                                    Next

                                                    objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)
                                                Next

                                                objDB.Entry(Old_objAccountEntityTo).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountEntityTo)
                                            End If

                                            objDB.Entry(Old_objAccountTo).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountTo)
                                        End If
                                    End If
                                ElseIf item.FK_Sender_To_Information = 2 Then 'Person To
                                    Dim Old_objPersonTo = objDB.SIPENDAR_Transaction_Person.Where(Function(x) x.FK_Transaction_ID = item.PK_Transaction_ID And x.FK_From_Or_To = 2).FirstOrDefault
                                    If Not Old_objPersonTo Is Nothing Then
                                        Dim Old_listObjPhonePersonTo = objDB.SIPENDAR_trn_Person_Phone.Where(Function(x) x.FK_Trn_Person = Old_objPersonTo.PK_Person_ID).ToList
                                        Dim Old_listObjAddressPersonTo = objDB.SIPENDAR_Trn_Person_Address.Where(Function(x) x.FK_Trn_Person = Old_objPersonTo.PK_Person_ID).ToList
                                        Dim Old_listObjIdentificationPersonTo = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = Old_objPersonTo.PK_Person_ID And x.FK_Person_Type = 1 And x.from_or_To_Type = 2).ToList

                                        For Each itemx1 In Old_listObjPhonePersonTo
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listObjAddressPersonTo
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next
                                        For Each itemx3 In Old_listObjIdentificationPersonTo
                                            objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                        Next

                                        objDB.Entry(Old_objPersonTo).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objPersonTo)
                                    End If
                                ElseIf item.FK_Sender_To_Information = 3 Then 'Entity To
                                    Dim Old_objEntityTo = objDB.SIPENDAR_Transaction_Entity.Where(Function(X) X.FK_Transaction_ID = item.PK_Transaction_ID And X.FK_From_Or_To = 2).FirstOrDefault

                                    If Not Old_objEntityTo Is Nothing Then
                                        'Address and Phone
                                        '20201230 Achmad -- Nambahin = Old_objEntityTo.PK_Entity_ID 
                                        'Dim Old_ListObjAddressEntityto = objDB.sipendar_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity).ToList
                                        Dim Old_ListObjAddressEntityto = objDB.SIPENDAR_Trn_Entity_Address.Where(Function(x) x.FK_Trn_Entity = Old_objEntityTo.PK_Entity_ID).ToList
                                        '20201230 Achmad --
                                        Dim Old_listObjPhoneEntityto = objDB.SIPENDAR_Trn_Entity_Phone.Where(Function(x) x.FK_Trn_Entity = Old_objEntityTo.PK_Entity_ID).ToList

                                        For Each itemx1 In Old_ListObjAddressEntityto
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listObjPhoneEntityto
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next

                                        'Director
                                        Dim Old_listDirectorEntityTo As List(Of SIPENDAR_Trn_Director) = objDB.SIPENDAR_Trn_Director.Where(Function(x) x.FK_Entity_ID = Old_objEntityTo.PK_Entity_ID And x.FK_From_Or_To = 2 And x.FK_Sender_Information = 3).ToList
                                        For Each objDirector As SIPENDAR_Trn_Director In Old_listDirectorEntityTo
                                            Dim Old_listPhoneDirector = objDB.SIPENDAR_trn_Director_Phone.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                            Dim Old_listAddressDirector = objDB.SIPENDAR_Trn_Director_Address.Where(Function(x) x.FK_Trn_Director_ID = objDirector.PK_goAML_Trn_Director_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Person_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_goAML_Trn_Director_ID And x.FK_Person_Type = 4 And x.from_or_To_Type = 2).ToList

                                            For Each itemx1 In Old_listPhoneDirector
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listAddressDirector
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)
                                        Next

                                        objDB.Entry(Old_objEntityTo).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objEntityTo)
                                    End If
                                End If
                            End If
                            '=================== END OF BI-PARTY ===================================================

                            '=================== MULTI-PARTY ===================================================
                            If item.FK_Transaction_Type = 2 Then
                                Dim Old_objTransactionParty = objDB.SIPENDAR_Transaction_Party.Where(Function(x) x.FK_Transaction_ID = item.PK_Transaction_ID).FirstOrDefault
                                If Not Old_objTransactionParty Is Nothing Then
                                    If Old_objTransactionParty.fk_ref_detail_of = 1 Then  'Account
                                        Dim Old_objAccountParty = objDB.SIPENDAR_Trn_Party_Account.Where(Function(x) x.FK_Trn_Party_ID = Old_objTransactionParty.PK_Trn_Party_ID).FirstOrDefault
                                        Dim Old_objAccountEntityParty = objDB.SIPENDAR_Trn_Par_Acc_Entity.Where(Function(x) x.FK_Trn_Party_Account_ID = Old_objAccountParty.PK_Trn_Party_Account_ID).FirstOrDefault

                                        'Account Korporasi
                                        If Not Old_objAccountEntityParty Is Nothing Then
                                            'Address and Phone
                                            Dim Old_listAddresAccountEntityParty = objDB.SIPENDAR_Trn_par_Acc_Entity_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = Old_objAccountEntityParty.PK_Trn_Par_acc_Entity_ID).ToList
                                            Dim Old_listPhoneAccountEntityParty = objDB.SIPENDAR_trn_par_acc_Entity_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_ID = Old_objAccountEntityParty.PK_Trn_Par_acc_Entity_ID).ToList

                                            For Each itemx1 In Old_listAddresAccountEntityParty
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listPhoneAccountEntityParty
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next

                                            'Director
                                            Dim Old_listDirectorAccountEntityParty = objDB.SIPENDAR_Trn_Par_Acc_Ent_Director.Where(Function(x) x.FK_Trn_Par_Acc_Entity_ID = Old_objAccountEntityParty.PK_Trn_Par_acc_Entity_ID).ToList
                                            For Each objDirector In Old_listDirectorAccountEntityParty
                                                Dim Old_listAddress = objDB.SIPENDAR_Trn_Par_Acc_Ent_Director_Address.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                Dim Old_listPhone = objDB.SIPENDAR_Trn_Par_Acc_Ent_Director_Phone.Where(Function(x) x.FK_Trn_par_acc_Entity_Director_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID).ToList
                                                Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_Trn_Par_Acc_Ent_Director_ID And x.FK_Person_Type = 5).ToList

                                                For Each itemx1 In Old_listAddress
                                                    objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                Next
                                                For Each itemx2 In Old_listPhone
                                                    objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                Next
                                                For Each itemx3 In Old_listIdentification
                                                    objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                                Next

                                                objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)
                                            Next

                                            objDB.Entry(Old_objAccountEntityParty).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountEntityParty)

                                        End If

                                        objDB.Entry(Old_objAccountParty).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountParty)

                                    ElseIf Old_objTransactionParty.fk_ref_detail_of = 2 Then  'Person
                                        Dim Old_objPersonParty = objDB.SIPENDAR_Trn_Party_Person.Where(Function(x) x.FK_Transaction_Party_ID = Old_objTransactionParty.PK_Trn_Party_ID).FirstOrDefault
                                        If Not Old_objPersonParty Is Nothing Then
                                            Dim Old_listAddressPersonParty = objDB.SIPENDAR_Trn_Party_Person_Address.Where(Function(x) x.FK_Trn_Party_Person_ID = Old_objPersonParty.PK_Trn_Party_Person_ID).ToList
                                            Dim Old_listPhonePersonParty = objDB.SIPENDAR_Trn_Party_Person_Phone.Where(Function(x) x.FK_Trn_Party_Person_ID = Old_objPersonParty.PK_Trn_Party_Person_ID).ToList
                                            Dim Old_listIdentificationPersonParty = objDB.SIPENDAR_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = Old_objPersonParty.PK_Trn_Party_Person_ID And x.FK_Person_Type = 1).ToList

                                            For Each itemx1 In Old_listAddressPersonParty
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listPhonePersonParty
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentificationPersonParty
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(Old_objPersonParty).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objPersonParty)

                                        End If
                                    ElseIf Old_objTransactionParty.fk_ref_detail_of = 3 Then  'Entity
                                        Dim Old_objentityParty = objDB.SIPENDAR_Trn_Party_Entity.Where(Function(x) x.FK_Transaction_Party_ID = Old_objTransactionParty.PK_Trn_Party_ID).FirstOrDefault
                                        If Not Old_objentityParty Is Nothing Then
                                            'Address and Phone
                                            Dim Old_listAddressEntityparty = objDB.SIPENDAR_Trn_Party_Entity_Address.Where(Function(x) x.FK_Trn_Party_Entity_ID = Old_objentityParty.PK_Trn_Party_Entity_ID).ToList
                                            Dim Old_listPhoneEntityParty = objDB.SIPENDAR_Trn_Party_Entity_Phone.Where(Function(x) x.FK_Trn_Party_Entity_ID = Old_objentityParty.PK_Trn_Party_Entity_ID).ToList

                                            For Each itemx1 In Old_listAddressEntityparty
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listPhoneEntityParty
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next

                                            'Director
                                            Dim Old_listDirectorEntityParty = objDB.SIPENDAR_Trn_Par_Entity_Director.Where(Function(x) x.FK_Trn_Party_Entity_ID = Old_objentityParty.PK_Trn_Party_Entity_ID).ToList
                                            For Each objDirector As SIPENDAR_Trn_Par_Entity_Director In Old_listDirectorEntityParty
                                                Dim Old_listAddress = objDB.SIPENDAR_Trn_Par_Entity_Director_Address.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                Dim Old_listPhone = objDB.SIPENDAR_Trn_Par_Entity_Director_Phone.Where(Function(x) x.FK_Trn_par_Entity_Director_ID = objDirector.PK_Trn_Par_Entity_Director_ID).ToList
                                                Dim Old_listIdentification = objDB.SIPENDAR_Transaction_Party_Identification.Where(Function(x) x.FK_Person_ID = objDirector.PK_Trn_Par_Entity_Director_ID And x.FK_Person_Type = 6).ToList

                                                For Each itemx1 In Old_listAddress
                                                    objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                Next
                                                For Each itemx2 In Old_listPhone
                                                    objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                Next
                                                For Each itemx3 In Old_listIdentification
                                                    objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                                Next

                                                objDB.Entry(objDirector).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, objDirector)

                                            Next

                                            objDB.Entry(Old_objentityParty).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objentityParty)

                                        End If
                                    End If

                                    objDB.Entry(Old_objTransactionParty).State = Entity.EntityState.Deleted
                                    objDB.SaveChanges()
                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objTransactionParty)

                                End If
                            End If
                            '=================== END OF MULTI-PARTY ===================================================
                        Next
                    End If
                    '====================== END OF TRANSAKSI (TRN) ============================================================================

                    '====================== AKTIVITAS (ACT) ============================================================================
                    If getTrnORActivity(Old_ObjReport.Report_Code) = "ACT" Then
                        Dim Old_listActivity = objDB.SIPENDAR_Act_ReportPartyType.Where(Function(x) x.FK_Report_ID = Old_ObjReport.PK_Report_ID).ToList

                        If Old_listActivity.Count > 0 Then
                            For Each Old_ObjActivity In Old_listActivity
                                If Old_ObjActivity.SubNodeType = 1 Then
                                    Dim Old_objAccountActivity = objDB.SIPENDAR_Act_Account.Where(Function(x) x.FK_Act_ReportParty_ID = Old_ObjActivity.PK_goAML_Act_ReportPartyType_ID).FirstOrDefault

                                    If Not Old_objAccountActivity Is Nothing Then
                                        Dim Old_listSignatoryActivity = objDB.SIPENDAR_Act_acc_Signatory.Where(Function(x) x.FK_Activity_Account_ID = Old_objAccountActivity.PK_goAML_Act_Account_ID).ToList

                                        For Each Old_signatory In Old_listSignatoryActivity
                                            Dim Old_listPhone = objDB.SIPENDAR_Act_Acc_sign_Phone.Where(Function(x) x.FK_Act_Acc_Entity_ID = Old_signatory.PK_goAML_Act_acc_Signatory_ID).ToList
                                            Dim Old_listAddress = objDB.SIPENDAR_Act_Acc_sign_Address.Where(Function(x) x.FK_Act_Acc_Entity = Old_signatory.PK_goAML_Act_acc_Signatory_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = Old_signatory.PK_goAML_Act_acc_Signatory_ID And x.FK_Person_Type = 2).ToList

                                            For Each itemx1 In Old_listAddress
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listPhone
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(Old_signatory).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_signatory)

                                        Next

                                        If Old_objAccountActivity.IsRekeningKorporasi Then
                                            Dim Old_objAccountEntityActivity = objDB.SIPENDAR_Act_Entity_Account.Where(Function(x) x.FK_Act_Account_ID = Old_objAccountActivity.PK_goAML_Act_Account_ID).FirstOrDefault

                                            If Not Old_objAccountEntityActivity Is Nothing Then
                                                Dim Old_listAddresAccountEntityActivity = objDB.SIPENDAR_Act_Acc_Entity_Address.Where(Function(x) x.FK_Act_Acc_Entity_ID = Old_objAccountEntityActivity.PK_goAML_Act_Entity_account).ToList
                                                Dim Old_listPhoneAccountEntityActivity = objDB.SIPENDAR_Act_Acc_Entity_Phone.Where(Function(X) X.FK_Act_Acc_Entity_ID = Old_objAccountEntityActivity.PK_goAML_Act_Entity_account).ToList

                                                For Each itemx1 In Old_listAddresAccountEntityActivity
                                                    objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                Next
                                                For Each itemx2 In Old_listPhoneAccountEntityActivity
                                                    objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                Next

                                                Dim Old_listDirectorEntityAccountActivity = objDB.SIPENDAR_Act_Acc_Ent_Director.Where(Function(X) X.FK_Act_Acc_Entity_ID = Old_objAccountEntityActivity.PK_goAML_Act_Entity_account).ToList
                                                For Each Old_directorAccount In Old_listDirectorEntityAccountActivity
                                                    Dim Old_listAddress = objDB.SIPENDAR_Act_Acc_Entity_Director_address.Where(Function(x) x.FK_Act_Entity_Director_ID = Old_directorAccount.PK_Act_Acc_Ent_Director_ID).ToList
                                                    Dim Old_listPhone = objDB.SIPENDAR_Act_Acc_Entity_Director_Phone.Where(Function(x) x.FK_Act_Entity_Director_ID = Old_directorAccount.PK_Act_Acc_Ent_Director_ID).ToList
                                                    Dim Old_listIdentification = objDB.SIPENDAR_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = Old_directorAccount.PK_Act_Acc_Ent_Director_ID And x.FK_Person_Type = 5).ToList

                                                    For Each itemx1 In Old_listAddress
                                                        objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                                    Next
                                                    For Each itemx2 In Old_listPhone
                                                        objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                                    Next
                                                    For Each itemx3 In Old_listIdentification
                                                        objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                        objDB.SaveChanges()
                                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                                    Next

                                                    objDB.Entry(Old_directorAccount).State = Entity.EntityState.Deleted
                                                    objDB.SaveChanges()
                                                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_directorAccount)

                                                Next

                                                objDB.Entry(Old_objAccountEntityActivity).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountEntityActivity)

                                            End If
                                        End If

                                        objDB.Entry(Old_objAccountActivity).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objAccountActivity)

                                    End If
                                ElseIf Old_ObjActivity.SubNodeType = 2 Then
                                    Dim Old_objPersonActivity = objDB.SIPENDAR_Act_Person.Where(Function(x) x.FK_Act_ReportParty_ID = Old_ObjActivity.PK_goAML_Act_ReportPartyType_ID).FirstOrDefault

                                    If Not Old_objPersonActivity Is Nothing Then

                                        Dim Old_listAddress = objDB.SIPENDAR_Act_Person_Address.Where(Function(x) x.FK_Act_Person = Old_objPersonActivity.PK_goAML_Act_Person_ID).ToList
                                        Dim Old_listPhone = objDB.SIPENDAR_Act_Person_Phone.Where(Function(x) x.FK_Act_Person = Old_objPersonActivity.PK_goAML_Act_Person_ID).ToList
                                        Dim Old_listIdentification = objDB.SIPENDAR_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = Old_objPersonActivity.PK_goAML_Act_Person_ID And x.FK_Person_Type = 1).ToList

                                        For Each itemx1 In Old_listAddress
                                            objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                        Next
                                        For Each itemx2 In Old_listPhone
                                            objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                        Next
                                        For Each itemx3 In Old_listIdentification
                                            objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                        Next

                                        objDB.Entry(Old_objPersonActivity).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objPersonActivity)
                                    End If

                                ElseIf Old_ObjActivity.SubNodeType = 3 Then
                                    Dim Old_objentityActivity = objDB.SIPENDAR_Act_Entity.Where(Function(x) x.FK_Act_ReportParty_ID = Old_ObjActivity.PK_goAML_Act_ReportPartyType_ID).FirstOrDefault

                                    If Not Old_objentityActivity Is Nothing Then
                                        Dim Old_listAddressEntityActivity = objDB.SIPENDAR_Act_Entity_Address.Where(Function(x) x.FK_Act_Entity_ID = Old_objentityActivity.PK_goAML_Act_Entity_ID).ToList
                                        Dim Old_listPhoneEntityActivity = objDB.SIPENDAR_Act_Entity_Phone.Where(Function(x) x.FK_Act_Entity_ID = Old_objentityActivity.PK_goAML_Act_Entity_ID).ToList

                                        Dim Old_listDirectorEntityActivity = objDB.SIPENDAR_Act_Director.Where(Function(x) x.FK_Act_Entity_ID = Old_objentityActivity.PK_goAML_Act_Entity_ID).ToList
                                        For Each Old_director In Old_listDirectorEntityActivity
                                            Dim Old_listAddress = objDB.SIPENDAR_Act_Director_Address.Where(Function(x) x.FK_Act_Director_ID = Old_director.PK_goAML_Act_Director_ID).ToList
                                            Dim Old_listPhone = objDB.SIPENDAR_Act_Director_Phone.Where(Function(x) x.FK_Act_Director_ID = Old_director.PK_goAML_Act_Director_ID).ToList
                                            Dim Old_listIdentification = objDB.SIPENDAR_Activity_Person_Identification.Where(Function(x) x.FK_Act_Person_ID = Old_director.PK_goAML_Act_Director_ID And x.FK_Person_Type = 6).ToList

                                            For Each itemx1 In Old_listAddress
                                                objDB.Entry(itemx1).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx1)
                                            Next
                                            For Each itemx2 In Old_listPhone
                                                objDB.Entry(itemx2).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx2)
                                            Next
                                            For Each itemx3 In Old_listIdentification
                                                objDB.Entry(itemx3).State = Entity.EntityState.Deleted
                                                objDB.SaveChanges()
                                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, itemx3)
                                            Next

                                            objDB.Entry(Old_director).State = Entity.EntityState.Deleted
                                            objDB.SaveChanges()
                                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_director)

                                        Next

                                        objDB.Entry(Old_objentityActivity).State = Entity.EntityState.Deleted
                                        objDB.SaveChanges()
                                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_objentityActivity)

                                    End If
                                End If

                                objDB.Entry(Old_ObjActivity).State = Entity.EntityState.Deleted
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailDelete(objDB, objaudittrailheader.PK_AuditTrail_ID, Old_ObjActivity)
                            Next
                        End If
                    End If
                    '====================== END OF AKTIVITAS (ACT) ============================================================================

                    objDB.SaveChanges()
                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function getTrnORActivity(kode As String) As String
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim objects As SiPendarDAL.goAML_odm_ref_Report_TRNorACT = objDb.goAML_odm_ref_Report_TRNorACT.Where(Function(x) x.ReportType = kode).FirstOrDefault
            Return objects.TRNorACT
        End Using
    End Function
    '***************************************************************************************************



End Class