﻿Public Class DirectorClass
    Public objDirector As New SiPendarDAL.SIPENDAR_Trn_Director
    Public listAddressDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
    Public listPhoneDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
    Public listAddressEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_Trn_Director_Address)
    Public listPhoneEmployerDirector As New List(Of SiPendarDAL.SIPENDAR_trn_Director_Phone)
    Public listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)

End Class
