﻿'--------------------------------------------
'Classname      : SIPENDAR_PROFILE
'Description    : Maintain SIPENDAR PROFILE Data
'Created By     : Adi Darmawan
'Created Date   : 24-Jun-2021
'--------------------------------------------

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports SiPendarDAL
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml
Imports System.Numerics

Public Class SiPendarProfile_BLL

    Shared Function GetSiPendarProfileClassByID(ID As Long) As SiPendarBLL.SIPENDAR_PROFILE_CLASS
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim objProfile = objDb.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = ID).FirstOrDefault
            Dim listProfileAccount = objDb.SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileAccountATM = objDb.SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileAddress = objDb.SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfilePhone = objDb.SIPENDAR_PROFILE_PHONE.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileIdentification = objDb.SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList

            Dim objProfileClass = New SIPENDAR_PROFILE_CLASS
            With objProfileClass
                .objSIPENDAR_PROFILE = objProfile
                .objList_SIPENDAR_PROFILE_ACCOUNT = listProfileAccount
                .objList_SIPENDAR_PROFILE_ACCOUNT_ATM = listProfileAccountATM
                .objList_SIPENDAR_PROFILE_ADDRESS = listProfileAddress
                .objList_SIPENDAR_PROFILE_PHONE = listProfilePhone
                .objList_SIPENDAR_PROFILE_IDENTIFICATION = listProfileIdentification
            End With

            Return objProfileClass
        End Using
    End Function

    Shared Function GetCustomerTypeByID(ID As String) As SiPendarDAL.goAML_Ref_Customer_Type
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Customer_Type.Where(Function(x) x.PK_Customer_Type_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisRekeningByCode(Code As String) As SiPendarDAL.goAML_Ref_Jenis_Rekening
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Rekening.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetStatusRekeningByCode(Code As String) As SiPendarDAL.goAML_Ref_Status_Rekening
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetKategoriKontakByCode(Code As String) As SiPendarDAL.goAML_Ref_Kategori_Kontak
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetNegaraByCode(Code As String) As SiPendarDAL.goAML_Ref_Nama_Negara
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisDokumenIdentitasByCode(Code As String) As SiPendarDAL.goAML_Ref_Jenis_Dokumen_Identitas
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisAlatKomunikasiByCode(Code As String) As SiPendarDAL.goAML_Ref_Jenis_Alat_Komunikasi
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Sub SaveEditWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetSiPendarProfileClassByID(objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objData_Old)

                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objData_Old.objSIPENDAR_PROFILE.Status = 4
                    objDB.Entry(objData_Old.objSIPENDAR_PROFILE).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveEditWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetSiPendarProfileClassByID(objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE, objData_Old.objSIPENDAR_PROFILE)


                    '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID < 0).ToList
                        'Ambil dulu list ATM dengan PK_Account_ID sebelum diupdate
                        Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)

                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)

                        'Save ACCOUNT ATM
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            objDB.Entry(item_atm).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    '===================== END OF NEW ADDED DATA


                    '===================== UPDATED DATA (PK untuk masing2 data > 0)
                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)

                        'Save ACCOUNT ATM
                        Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            If item_atm.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID < 0 Then
                                objDB.Entry(item_atm).State = Entity.EntityState.Added
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                            Else
                                Dim item_atm_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = item_atm.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID).FirstOrDefault
                                objDB.Entry(item_atm).State = Entity.EntityState.Modified
                                objDB.SaveChanges()

                                If Not item_atm_old Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm, item_atm_old)
                                End If
                            End If
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item.PK_SIPENDAR_PROFILE_ADDRESS_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item.PK_SIPENDAR_PROFILE_PHONE_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next
                    '===================== END OF UPDATED DATA


                    '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                    'Deleted ACCOUNT
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted ACCOUNT ATM
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Address
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item_old.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted PHONE
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item_old.PK_SIPENDAR_PROFILE_PHONE_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Identity
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item_old.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next
                    '===================== END OF DELETED DATA

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using

        'Jalankan validasi satuan untuk replace IsValid dan Error Message
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter
        param(0).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
        param(0).Value = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
        param(0).DbType = SqlDbType.BigInt
        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)

    End Sub

    Shared Sub SaveAddWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        '.Active = True
                        .CreatedBy = strUserID
                        .CreatedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    '' Add 18-Nov-2021
                    '' Edit 16-Dec-2021 [1 atau 2]-[GCN]-[Tanggal]_[Sumber_ID]+[Submission_Type]
                    Dim newModuleKey As String = ""
                    If objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                        'newModuleKey = objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objData.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd")
                        newModuleKey = objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objData.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_0+" & objData.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                    ElseIf objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 2 Then
                        'newModuleKey = objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objData.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "-" & objData.objSIPENDAR_PROFILE.SUMBER_ID
                        newModuleKey = objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objData.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") & "_" & objData.objSIPENDAR_PROFILE.SUMBER_ID & "+" & objData.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                    End If

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        '.ModuleKey = Nothing '' Edited by Felix 02 Aug 2021
                        '.ModuleKey = objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID & "-" & objData.objSIPENDAR_PROFILE.GCN & "-" & Now.ToString("yyyy-MM-dd") '' Edit 18 Nov 2021
                        .ModuleKey = newModuleKey
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = Nothing
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function SaveAddWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        Dim IDUnik As Long

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        .Active = True

                        If .CreatedBy Is Nothing Then
                            .CreatedBy = strUserID
                            .CreatedDate = Now
                        End If

                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    '25-May-2021
                    IDUnik = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                    '-----------

                    'Audit Trail Header
                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)

                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                        'Ambil dulu list ATM dengan PK_Account_ID sebelum diupdate
                        Dim listATM As New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
                        listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID).ToList

                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)

                        'Save ACCOUNT ATM
                        'Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount).ToList
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            objDB.Entry(item_atm).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()

                    'Return IDUnik
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex

                    Return Nothing
                End Try
            End Using
        End Using

        'Jalankan validasi satuan untuk replace IsValid dan Error Message
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter
        param(0).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
        param(0).Value = IDUnik
        param(0).DbType = SqlDbType.BigInt
        NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)

        '8-Jul-2021 : Generate Report within Date Range
        GenerateTransaction(objData)

        Return IDUnik
    End Function

    Shared Sub SIPENDARProfileSaveDeleteWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = Nothing
                        .ModuleFieldBefore = objXMLData
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objData.objSIPENDAR_PROFILE.Status = 4
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveDeleteWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        Using objDB As New SiPendarDAL.SiPendarEntities
            Dim listobjprofile As New List(Of SiPendarDAL.SIPENDAR_PROFILE)
            Dim objAuditTrailheader As New SiPendarDAL.AuditTrailHeader
            Dim savedfinish As Boolean = False
            Dim jumlahprofile As Integer = 0
            Dim ObjReport As New SiPendarDAL.SIPENDAR_Report
            If objData.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 2 Then
                Dim strqueries As String = ""
                strqueries = " select count(1) as jumlahprofile from SIPENDAR_PROFILE "
                strqueries = strqueries & " where FK_SIPENDAR_TYPE_ID = 2 "
                strqueries = strqueries & " and GCN = '" & objData.objSIPENDAR_PROFILE.GCN & "' "
                strqueries = strqueries & " and SUMBER_ID = '" & objData.objSIPENDAR_PROFILE.SUMBER_ID & "' "
                strqueries = strqueries & " and FK_SIPENDAR_SUBMISSION_TYPE_CODE = '" & objData.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "' "
                strqueries = strqueries & " and DATEDIFF(DAY, CreatedDate, '" & objData.objSIPENDAR_PROFILE.CreatedDate & "') = 0 "
                Dim drResult As DataRow = NawaDAL.SQLHelper.ExecuteRow(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strqueries, Nothing)
                jumlahprofile = drResult("jumlahprofile")
                'jumlahprofile = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strqueries, Nothing)

                '' Update 20-Dec-2021 Report.unikreferece = (profile.[GCN]-profile.[SUMBER_ID]-profile.[FK_SIPENDAR_SUBMISSION_TYPE_CODE])
                Dim unikref As String = objData.objSIPENDAR_PROFILE.GCN & "-" & objData.objSIPENDAR_PROFILE.SUMBER_ID & "-" & objData.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                ObjReport = objDB.SIPENDAR_Report.Where(Function(x) x.UnikReference = unikref AndAlso System.Data.Entity.DbFunctions.DiffDays(x.Submission_Date, objData.objSIPENDAR_PROFILE.CreatedDate) = 0).FirstOrDefault
            End If
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Delete objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()

                    'Audit Trail Header
                    objAuditTrailheader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    listobjprofile.Add(objData.objSIPENDAR_PROFILE)
                    'If listobjprofile IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, listobjprofile)
                    '    objDB.SaveChanges()
                    'End If
                    'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)

                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    'If objData.objList_SIPENDAR_PROFILE_ACCOUNT IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ACCOUNT)
                    '    objDB.SaveChanges()
                    'End If

                    'Save ACCOUNT_ATM
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    'If objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM)
                    '    objDB.SaveChanges()
                    'End If

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    'If objData.objList_SIPENDAR_PROFILE_ADDRESS IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ADDRESS)
                    '    objDB.SaveChanges()
                    'End If

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    'If objData.objList_SIPENDAR_PROFILE_PHONE IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_PHONE)
                    '    objDB.SaveChanges()
                    'End If

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        'NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    'If objData.objList_SIPENDAR_PROFILE_IDENTIFICATION IsNot Nothing Then
                    '    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_IDENTIFICATION)
                    '    objDB.SaveChanges()
                    'End If

                    objTrans.Commit()
                    savedfinish = True
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
            If jumlahprofile = 1 AndAlso ObjReport IsNot Nothing Then
                objDB.Entry(ObjReport).State = Entity.EntityState.Deleted
                objDB.SaveChanges()

                Dim paramDelete(0) As SqlParameter
                paramDelete(0) = New SqlParameter
                paramDelete(0).ParameterName = "@pkreportid"
                paramDelete(0).Value = ObjReport.PK_Report_ID
                paramDelete(0).DbType = SqlDbType.BigInt
                NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_DeleteReport_ByPkReportID", paramDelete)
                Dim objModuleReport As New NawaDAL.Module
                objModuleReport = NawaBLL.ModuleBLL.GetModuleByModuleName("SIPENDAR_Report")
                If objModuleReport IsNot Nothing Then
                    SiPendarBLL.goAML_NEW_ReportBLL.CreateNotes(objModuleReport.PK_Module_ID, objModuleReport.ModuleLabel, SiPendarBLL.goAML_NEW_ReportBLL.actionApproval.Accept, SiPendarBLL.goAML_NEW_ReportBLL.actionForm.Delete, ObjReport.PK_Report_ID, "Data Transaction Auto Delted by Deleting SIPENDAR Profile")
                End If
                If ObjReport.PK_Report_ID <> Nothing Then
                    GenerateSarBLL.UpdateStatusGenerateSAR(ObjReport.PK_Report_ID)
                End If
            End If
            If savedfinish AndAlso objAuditTrailheader IsNot Nothing Then
                If listobjprofile IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, listobjprofile)
                End If
                If objData.objList_SIPENDAR_PROFILE_ACCOUNT IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ACCOUNT)
                End If
                If objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM)
                End If
                If objData.objList_SIPENDAR_PROFILE_ADDRESS IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_ADDRESS)
                End If
                If objData.objList_SIPENDAR_PROFILE_PHONE IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_PHONE)
                End If
                If objData.objList_SIPENDAR_PROFILE_IDENTIFICATION IsNot Nothing Then
                    NawaFramework.CreateAuditTrailDetailXML(objAuditTrailheader.PK_AuditTrail_ID, Nothing, objData.objList_SIPENDAR_PROFILE_IDENTIFICATION)
                End If
            End If
        End Using
    End Sub

    Shared Sub Accept(ID As Long)

        Dim IDUnik As Long
        Dim StrQueryDeleteModuleApproval As String '' Add 15-Nov-2021

        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            '25-May-2021 ganti jadi function agar bisa mengembalikan IDUnik
                            'SaveAddWithoutApproval(objModule, objData)
                            IDUnik = SaveAddWithoutApproval(objModule, objData)

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveEditWithoutApproval(objModule, objData)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveDeleteWithoutApproval(objModule, objData_Old)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData_Old.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID

                    End Select

                    '21-Jul-2021 Adi : Update Screening Result Other Info Status agar tidak bisa dijudgement lagi.
                    Using objdbUpdateStatus As New SiPendarDAL.SiPendarEntities
                        Dim objApprovalUpdateStatus As SiPendarDAL.ModuleApproval = objdbUpdateStatus.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                        If objApprovalUpdateStatus IsNot Nothing AndAlso objApprovalUpdateStatus.PK_ModuleAction_ID = 1 Then
                            Dim objDataUpdateStatus As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApprovalUpdateStatus.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')=0"
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 and status not in ('Accept','Excluded')" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 and status not in ('Accept','Not Match')" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 and status not in ('Accept','Not Match')" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini

                            Dim strQuery As String '' 23-Dec-2021 Tambah IF 
                            If objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                                strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 and status not in ('Accept','Not Match') and FK_SIPENDAR_TYPE_ID = '1'" ''Edit 16-Dec-2021 Felix, wherenya pakai GCN, Sumber_ID, Submission_Type
                            Else
                                strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Accept', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND SUMBER_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "' AND FK_SIPENDAR_SUBMISSION_TYPE_CODE = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 and status not in ('Accept','Not Match') and FK_SIPENDAR_TYPE_ID = '2' " ''Edit 16-Dec-2021 Felix, wherenya pakai GCN, Sumber_ID, Submission_Type
                            End If
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)

                            ''Add 08-Nov-2021 Kalau Proaktif, Update Null lg Sumber_ID nya
                            strQuery = "UPDATE SIPENDAR_PROFILE set [SUMBER_ID] = null WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 AND FK_SIPENDAR_TYPE_ID = 1"
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            ''End 08-Nov-2021
                            '' Add 15-Nov-2021 Hapus di Pending Insert Module Approval untuk kombinasi ProfileType-GCN-Tanggal
                            If objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_TYPE_ID = 1 Then
                                Dim lengthGCN As String = Len(objDataUpdateStatus.objSIPENDAR_PROFILE.GCN)
                                'StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3,len(modulekey)-13) = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "'"
                                'StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3,len(modulekey)-13) = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND substring(modulekey,charindex('_',modulekey)+1,charindex('+',modulekey) - charindex('_',modulekey)-1) ='0' and substring(modulekey,charindex('+',modulekey)+1,10) ='" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "'" '' Edit 16-Dec-2021
                                'StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3," & lengthGCN & ") = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND substring(modulekey,charindex('_',modulekey)+1,charindex('+',modulekey) - charindex('_',modulekey)-1) ='0' and substring(modulekey,charindex('+',modulekey)+1,10) ='" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "'" '' Edit 22-Dec-2021
                                'StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF') And PK_ModuleAction_ID = '1' And ModuleKey = '1-11014070013-2021-12-16_0+INITIAL'" '' Edit 16-Dec-2021
                                StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3," & lengthGCN & ") = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "'" '' Edit 23-Dec-2021 Yg proaktif ttp delete by GCN
                            Else
                                Dim lengthGCN As String = Len(objDataUpdateStatus.objSIPENDAR_PROFILE.GCN)
                                'StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PENGAYAAN') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3," & lengthGCN & ") = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "'"
                                StrQueryDeleteModuleApproval = "Delete ModuleApproval where ModuleName in ('vw_SIPENDAR_PROFILE_PENGAYAAN') And PK_ModuleAction_ID = '1' And SUBSTRING(ModuleKey,3," & lengthGCN & ") = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND substring(modulekey,charindex('_',modulekey)+1,charindex('+',modulekey) - charindex('_',modulekey)-1) ='" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "' and substring(modulekey,charindex('+',modulekey)+1,10) ='" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "'" '' Edit 16-Dec-2021
                            End If
                            ''NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                            '' Execute di bawah tidak error pas frameworknya mau delete ModuleApproval record ini
                            ''End 15-Nov-2021
                            '' Add 15-Nov-2021 Hapus di Pending Insert Module Approval untuk kombinasi ProfileType-GCN-Tanggal
                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryDeleteModuleApproval, Nothing)
                        Else

                            'Delete Module Approval
                            objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                            objdb.SaveChanges()
                        End If

                        ''NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        '' Execute di bawah tidak error pas frameworknya mau delete ModuleApproval record ini
                        ''End 15-Nov-2021

                    End Using


                    objtrans.Commit()

                    ''' Add 15-Nov-2021 Hapus di Pending Insert Module Approval untuk kombinasi ProfileType-GCN-Tanggal
                    'NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, StrQueryDeleteModuleApproval, Nothing)

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Reject(ID As Long)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objModule As SiPendarDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Select Case objApproval.PK_ModuleAction_ID
                    '    Case NawaBLL.Common.ModuleActionEnum.Insert
                    '        Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))
                    '
                    '        'Audit Trail Header
                    '        Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    '        With objAuditTrailheader
                    '            .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    '        End With
                    '        NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)
                    '
                    '        'Save ACCOUNT
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Address
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save PHONE
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Identity
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '    Case NawaBLL.Common.ModuleActionEnum.Update
                    '        Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))
                    '        Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))
                    '
                    '        'Audit Trail Header
                    '        Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    '        With objAuditTrailheader
                    '            .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    '        End With
                    '        NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE, objData_Old.objSIPENDAR_PROFILE)
                    '
                    '        '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                    '        'Save ACCOUNT
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID < 0).ToList
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Address
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID < 0).ToList
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save PHONE
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID < 0).ToList
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Identity
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID < 0).ToList
                    '            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '        '===================== END OF NEW ADDED DATA
                    '
                    '
                    '        '===================== UPDATED DATA (PK untuk masing2 data > 0)
                    '        'Save ACCOUNT
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID > 0).ToList
                    '            Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                    '            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    '        Next
                    '
                    '        'Save Address
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID > 0).ToList
                    '            Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                    '            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    '        Next
                    '
                    '        'Save PHONE
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID > 0).ToList
                    '            Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item.PK_SIPENDAR_PROFILE_PHONE_ID)
                    '            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    '        Next
                    '
                    '        'Save Identity
                    '        For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID > 0).ToList
                    '            Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                    '            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    '        Next
                    '        '===================== END OF UPDATED DATA
                    '
                    '
                    '        '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                    '        'Deleted ACCOUNT
                    '        For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                    '            Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                    '            If objCek Is Nothing Then
                    '                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                    '            End If
                    '        Next
                    '
                    '        'Deleted Address
                    '        For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                    '            Dim objCek = objData.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item_old.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                    '            If objCek Is Nothing Then
                    '                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                    '            End If
                    '        Next
                    '
                    '        'Deleted PHONE
                    '        For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                    '            Dim objCek = objData.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item_old.PK_SIPENDAR_PROFILE_PHONE_ID)
                    '            If objCek Is Nothing Then
                    '                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                    '            End If
                    '        Next
                    '
                    '        'Deleted Identity
                    '        For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                    '            Dim objCek = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item_old.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                    '            If objCek Is Nothing Then
                    '                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                    '            End If
                    '        Next
                    '    '===================== END OF DELETED DATA
                    '
                    '    Case NawaBLL.Common.ModuleActionEnum.Delete
                    '        Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))
                    '
                    '        'Audit Trail Header
                    '        Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    '        With objAuditTrailheader
                    '            .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                    '        End With
                    '        NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.objSIPENDAR_PROFILE)
                    '
                    '        'Save ACCOUNT
                    '        For Each item In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                    '            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Address
                    '        For Each item In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                    '            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save PHONE
                    '        For Each item In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                    '            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    '
                    '        'Save Identity
                    '        For Each item In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                    '            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                    '        Next
                    'End Select

                    '21-Jul-2021 Adi : Update Screening Result Other Info Status agar tidak bisa dijudgement lagi.
                    Using objdbUpdateStatus As New SiPendarDAL.SiPendarEntities
                        Dim objApprovalUpdateStatus As SiPendarDAL.ModuleApproval = objdbUpdateStatus.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                        If objApprovalUpdateStatus IsNot Nothing AndAlso objApprovalUpdateStatus.PK_ModuleAction_ID = 1 Then
                            Dim objDataUpdateStatus As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApprovalUpdateStatus.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')=0"
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 And status Not in ('Accept','Excluded','')" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 And status Not in ('Accept','Not Match','')" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            'Dim strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject' WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND DATEDIFF(DAY, CreatedDate, '" & CDate(objDataUpdateStatus.objSIPENDAR_PROFILE.CreatedDate).ToString("yyyy-MM-dd") & "')>=0 And status Not in ('Accept','Not Match') and SUMBER_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "'" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            'Edit 01-Nov-2021 Felix
                            Dim strQuery As String
                            If objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID IsNot Nothing And objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID > 0 Then
                                'strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' And status Not in ('Accept','Not Match') and SUMBER_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "'" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                                strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' AND SUMBER_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "' AND FK_SIPENDAR_SUBMISSION_TYPE_CODE = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE & "' And status Not in ('Accept','Not Match') and SUMBER_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.SUMBER_ID & "'" ''Edit 16-Dec-2021 Felix, Wherenya tambah Submission_Type, Sumber_ID
                            Else
                                strQuery = "UPDATE SIPENDAR_SCREENING_REQUEST_RESULT_OTHER_INFO set [Status] = 'Reject', [LastUpdateDate] = getdate() WHERE GCN = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.GCN & "' And status Not in ('Accept','Not Match') and FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID = '" & objDataUpdateStatus.objSIPENDAR_PROFILE.FK_SIPENDAR_SCREENING_REQUEST_RESULT_ID & "'" ''Edit 30-Sep-2021 Felix, karena GCN ini sudah dibentuk profile per hari ini
                            End If


                            'End 01-Nov-2021

                            NawaDAL.SQLHelper.ExecuteNonQuery(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQuery, Nothing)
                        End If
                    End Using


                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using

            If objApproval IsNot Nothing AndAlso objApproval.ModuleKey IsNot Nothing AndAlso (objApproval.PK_ModuleAction_ID = 2 OrElse objApproval.PK_ModuleAction_ID = 3) Then
                Dim PK_Profile As Long = 0
                'Dim parsestringtobigint As Boolean = BigInteger.TryParse(objApproval.ModuleKey, PK_Profile)
                If Long.TryParse(objApproval.ModuleKey, PK_Profile) Then
                    Dim param(0) As SqlParameter
                    param(0) = New SqlParameter
                    param(0).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
                    param(0).Value = PK_Profile
                    param(0).DbType = SqlDbType.BigInt
                    NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate_Satuan", param)
                    'Else
                    '    Throw New ApplicationException("")
                End If
            End If
        End Using
    End Sub

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New SiPendarDAL.SiPendarEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function

    '' Added by Felix 02 Aug 2021
    Public Shared Function IsExistsInApprovalProfileType(strModuleName As String, strID As String, PK_ModuleAction_ID As String, ParameterValue As String) As Boolean
        Try
            Dim isExists As Boolean = False

            '' Edit 23-Dec-2021
            'Using objdb As New SiPendarDAL.SiPendarEntities
            '    Dim objCek As ModuleApproval
            '    If ParameterValue = 1 Then
            '        objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID And x.PK_ModuleAction_ID = PK_ModuleAction_ID).FirstOrDefault
            '    Else
            '        objCek = objdb.ModuleApprovals.Where(Function(x) (x.ModuleName = "vw_SIPENDAR_PROFILE_PROAKTIF" Or x.ModuleName = "vw_SIPENDAR_PROFILE_PENGAYAAN") And x.ModuleKey.Contains(strID.Substring(2, strID.Length - 2)) And x.PK_ModuleAction_ID = PK_ModuleAction_ID).FirstOrDefault
            '    End If

            '    If objCek IsNot Nothing Then
            '        isExists = True
            '    End If
            'End Using

            Dim countRowModuleApproal As Int64
            Dim strQueryCheck As String = "select 0"
            If ParameterValue = 1 Then

                If Left(strID, 1) = "1" Then ''Proaktif
                    strQueryCheck = " select count(1) as countRowModuleApproal " &
                                        " FROM moduleapproval " &
                                        " where PK_ModuleAction_ID = '" & PK_ModuleAction_ID & "' " &
                                        " and ModuleName = '" & strModuleName & "' " &
                                        " and substring(ModuleKey,3, CHARINDEX('_',ModuleKey)-3) = substring('" & strID & "',3, CHARINDEX('_','" & strID & "')-3) "
                Else '' Pengayaan
                    strQueryCheck = " select count(1) as countRowModuleApproal " &
                                        " FROM moduleapproval " &
                                        " where PK_ModuleAction_ID = '" & PK_ModuleAction_ID & "' " &
                                        " and ModuleName = '" & strModuleName & "' " &
                                        " and ModuleKey = '" & strID & "'"
                End If

            Else
                strQueryCheck = " select count(1) as countRowModuleApproal " &
                                " FROM moduleapproval " &
                                " where PK_ModuleAction_ID = '" & PK_ModuleAction_ID & "' " &
                                " and ModuleName in ('vw_SIPENDAR_PROFILE_PROAKTIF','vw_SIPENDAR_PROFILE_PENGAYAAN')" &
                                " and substring(ModuleKey,3, CHARINDEX('_',ModuleKey)-3) = substring('" & strID & "',3, CHARINDEX('_','" & strID & "')-3) "
            End If
            countRowModuleApproal = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, strQueryCheck, Nothing)
            If countRowModuleApproal > 0 Then
                isExists = True
            End If

            '' End 23-Dec-21

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function
    '' End of 02 Aug 2021

    Public Shared Function GenerateXML_ProAktif(listSiPendarProfile As List(Of SIPENDAR_PROFILE), strDirPath As String) As String
        Try
            Dim strReportCode = "SIPENDAR-PROAKTIF"
            Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmss")

            Dim strZipFile As String = ""

            Dim lstFile As New List(Of String)
            Dim lstFileZip As New List(Of String)

            Dim intjmlFiledalam1zip As Integer = 1000
            'intjmlFiledalam1zip = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

            Dim intCounter As Integer = 0

            Dim listAttachment As New List(Of String)
            For Each item In listSiPendarProfile

                Dim pkProfileID As Long = item.PK_SIPENDAR_PROFILE_ID

                Dim objParam(2) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@PK"
                objParam(0).Value = pkProfileID

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@session"
                objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                objParam(2) = New SqlParameter
                objParam(2).ParameterName = "@FK_SIPENDAR_Type"
                objParam(2).Value = 1   'ProAktif

                Dim incrementRow As Int32
                incrementRow = 0
                Dim xmlDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GenerateXML", objParam)

                For Each itemXML As DataRow In xmlDataTable.Rows
                    Dim strResult As String = itemXML(0)
                    Dim strFileXML As String

                    If xmlDataTable.Rows.Count > 1 Then
                        incrementRow = incrementRow + 1
                        strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & ".xml"
                    Else
                        strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & ".xml"
                    End If

                    If strResult = "" Then
                        Throw New Exception("Data Report Empty for Profile ID =" & pkProfileID)
                    End If

                    'Delete file kalau sudah pernah ada
                    If IO.File.Exists(strFileXML) Then
                        IO.File.Delete(strFileXML)
                    End If

                    'Load XML from data row and save to temp dir
                    Dim objdoc As New XmlDocument
                    objdoc.LoadXml(strResult)
                    objdoc.PreserveWhitespace = False
                    objdoc.Save(strFileXML)

                    'Tambahkan object XML ke list
                    lstFile.Add(strFileXML)
                Next

                If lstFile.Count = intjmlFiledalam1zip And lstFile.Count > 0 Then
                    Using zip As New ZipFile()
                        intCounter += 1
                        Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                        If IO.File.Exists(strFileNameZip) Then
                            IO.File.Delete(strFileNameZip)
                        End If
                        zip.CompressionLevel = CompressionLevel.Level9
                        zip.AddFiles(lstFile, "")

                        zip.Save(strFileNameZip)
                        lstFileZip.Add(strFileNameZip)
                    End Using

                    'Buang file karena sudah di zip
                    For Each Item1 As String In lstFile
                        IO.File.Delete(Item1)
                    Next

                    lstFile.Clear()
                End If
            Next

            'Kalau masih ada list of file yang belum di zip
            If lstFile.Count > 0 Then

                Using zip As New ZipFile()
                    intCounter += 1
                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If
                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFile, "")

                    zip.Save(strFileNameZip)
                    lstFileZip.Add(strFileNameZip)

                End Using

                'buang karena sudah di zip
                For Each Item1 As String In lstFile
                    IO.File.Delete(Item1)
                Next
                lstFile.Clear()

            End If

            'Kembalikan Zip File
            If lstFileZip.Count > 1 Then
                Using zip As New ZipFile()

                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-All" & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If

                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFileZip, "")
                    zip.Save(strFileNameZip)

                    For Each Item1 As String In lstFileZip
                        IO.File.Delete(Item1)
                    Next

                    strZipFile = strFileNameZip
                End Using

            ElseIf lstFileZip.Count = 1 Then
                strZipFile = lstFileZip.Item(0).ToString
            Else
                Throw New Exception("No reports generated.")
            End If

            Return strZipFile
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function GenerateXML_Bulk(listSiPendarProfile As List(Of SIPENDAR_PROFILE), strDirPath As String, strModuleName As String) As String
        Try
            Dim strReportCode = "SIPENDAR-PROAKTIF"
            Dim intSiPendarType As Integer = 1
            If strModuleName = "vw_SIPENDAR_PROFILE_PROAKTIF" Then
                strReportCode = "SIPENDAR-PROAKTIF"
                intSiPendarType = 1
            Else
                strReportCode = "SIPENDAR-PENGAYAAN"
                intSiPendarType = 2
            End If
            Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmss")

            Dim strZipFile As String = ""

            Dim lstFile As New List(Of String)
            Dim lstFileZip As New List(Of String)

            Dim intjmlFiledalam1zip As Integer = 1000
            'intjmlFiledalam1zip = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

            Dim intCounter As Integer = 0

            Dim listAttachment As New List(Of String)
            For Each item In listSiPendarProfile

                Dim pkProfileID As Long = item.PK_SIPENDAR_PROFILE_ID
                Dim strSumberID As String = ""
                If intSiPendarType = 2 Then
                    strSumberID = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select sumber_id FROM sipendar_profile WHERE PK_SIPENDAR_PROFILE_ID = '" & pkProfileID & "'", Nothing)
                End If
                Dim gcn As String = ""
                If item.GCN IsNot Nothing Then
                    gcn = item.GCN
                    Dim cleans As String = ""
                    cleans = gcn.Replace("-", "")
                    gcn = cleans
                End If

                Dim strSubmissionType As String = "" ' Add 16-Dec-2021 Felix
                strSubmissionType = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "select FK_SIPENDAR_SUBMISSION_TYPE_CODE FROM sipendar_profile WHERE PK_SIPENDAR_PROFILE_ID = '" & pkProfileID & "'", Nothing)

                Dim objParam(2) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@PK"
                objParam(0).Value = pkProfileID

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@session"
                objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                objParam(2) = New SqlParameter
                objParam(2).ParameterName = "@FK_SIPENDAR_Type"
                objParam(2).Value = intSiPendarType   '1=ProAktif, 2=Pengayaan

                Dim incrementRow As Int32
                incrementRow = 0
                Dim xmlDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GenerateXML", objParam)

                For Each itemXML As DataRow In xmlDataTable.Rows
                    Dim strResult As String = itemXML(0)
                    Dim strFileXML As String

                    If xmlDataTable.Rows.Count > 1 Then
                        incrementRow = incrementRow + 1

                        'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & ".xml"
                        If intSiPendarType = 2 Then
                            'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & gcn & ".xml"
                            strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & gcn & "-" & strSubmissionType & ".xml"
                        Else
                            'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & gcn & ".xml"
                            strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & gcn & "-" & strSubmissionType & ".xml"
                        End If

                    Else
                        'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & ".xml"
                        If intSiPendarType = 2 Then
                            'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & gcn & ".xml"
                            strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strSumberID & "-" & strTanggalGenerate & "-" & gcn & "-" & strSubmissionType & ".xml"
                        Else
                            'strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & gcn & ".xml"
                            strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & gcn & "-" & strSubmissionType & ".xml"
                        End If

                    End If

                    If strResult = "" Then
                        Throw New Exception("Data Report Empty for Profile ID =" & pkProfileID)
                    End If

                    'Delete file kalau sudah pernah ada
                    If IO.File.Exists(strFileXML) Then
                        IO.File.Delete(strFileXML)
                    End If

                    'Load XML from data row and save to temp dir
                    Dim objdoc As New XmlDocument
                    objdoc.LoadXml(strResult)
                    objdoc.PreserveWhitespace = False
                    objdoc.Save(strFileXML)

                    'Tambahkan object XML ke list
                    lstFile.Add(strFileXML)
                Next

                If lstFile.Count = intjmlFiledalam1zip And lstFile.Count > 0 Then
                    Using zip As New ZipFile()
                        intCounter += 1
                        Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                        If IO.File.Exists(strFileNameZip) Then
                            IO.File.Delete(strFileNameZip)
                        End If
                        zip.CompressionLevel = CompressionLevel.Level9
                        zip.AddFiles(lstFile, "")

                        zip.Save(strFileNameZip)
                        lstFileZip.Add(strFileNameZip)
                    End Using

                    'Buang file karena sudah di zip
                    For Each Item1 As String In lstFile
                        IO.File.Delete(Item1)
                    Next

                    lstFile.Clear()
                End If
            Next

            'Kalau masih ada list of file yang belum di zip
            If lstFile.Count > 0 Then

                Using zip As New ZipFile()
                    intCounter += 1
                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If
                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFile, "")

                    zip.Save(strFileNameZip)
                    lstFileZip.Add(strFileNameZip)

                End Using

                'buang karena sudah di zip
                For Each Item1 As String In lstFile
                    IO.File.Delete(Item1)
                Next
                lstFile.Clear()

            End If

            'Kembalikan Zip File
            If lstFileZip.Count > 1 Then
                Using zip As New ZipFile()

                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-All" & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If

                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFileZip, "")
                    zip.Save(strFileNameZip)

                    For Each Item1 As String In lstFileZip
                        IO.File.Delete(Item1)
                    Next

                    strZipFile = strFileNameZip
                End Using

            ElseIf lstFileZip.Count = 1 Then
                strZipFile = lstFileZip.Item(0).ToString
            Else
                Throw New Exception("No reports generated.")
            End If

            Return strZipFile
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function checkValidation(objSIPENDAR_CLASS As SIPENDAR_PROFILE_CLASS, objmodule As NawaDAL.Module, Action As Integer)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Object Profile
                    Dim listObjProfile As New List(Of SiPendarDAL.SIPENDAR_PROFILE)
                    Dim listObjProfileAccount As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT)
                    Dim listObjProfileAccountATM As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM)
                    Dim listObjProfileAddress As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ADDRESS)
                    Dim listObjProfilePhone As New List(Of SiPendarDAL.SIPENDAR_PROFILE_PHONE)
                    Dim listObjProfileIdentification As New List(Of SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION)

                    'Populate data to list
                    With objSIPENDAR_CLASS
                        listObjProfile.Add(.objSIPENDAR_PROFILE)

                        For Each item1 In .objList_SIPENDAR_PROFILE_ACCOUNT
                            listObjProfileAccount.Add(item1)
                        Next

                        For Each item2 In .objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                            listObjProfileAccountATM.Add(item2)
                        Next

                        For Each item3 In .objList_SIPENDAR_PROFILE_ADDRESS
                            listObjProfileAddress.Add(item3)
                        Next

                        For Each item4 In .objList_SIPENDAR_PROFILE_PHONE
                            listObjProfilePhone.Add(item4)
                        Next

                        For Each item5 In .objList_SIPENDAR_PROFILE_IDENTIFICATION
                            listObjProfileIdentification.Add(item5)
                        Next

                    End With

                    'Convert to Datatable to throw to SP
                    Dim dtProfile As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfile)
                    Dim dtProfileAccount As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAccount)
                    Dim dtProfileAccountATM As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAccountATM)
                    Dim dtProfileAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAddress)
                    Dim dtProfilePhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfilePhone)
                    Dim dtProfileIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileIdentification)

                    Dim param(6) As SqlParameter

                    param(0) = New SqlParameter
                    param(0).ParameterName = "@userid"
                    param(0).Value = NawaBLL.Common.SessionCurrentUser.UserID
                    param(0).DbType = SqlDbType.VarChar

                    param(1) = New SqlParameter
                    param(1).ParameterName = "@TMP_udt_SIPENDAR_PROFILE"
                    param(1).Value = dtProfile
                    param(1).SqlDbType = SqlDbType.Structured
                    param(1).TypeName = "dbo.udt_SIPENDAR_PROFILE"

                    param(2) = New SqlParameter
                    param(2).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ACCOUNT"
                    param(2).Value = dtProfileAccount
                    param(2).SqlDbType = SqlDbType.Structured
                    param(2).TypeName = "dbo.udt_SIPENDAR_PROFILE_ACCOUNT"

                    param(3) = New SqlParameter
                    param(3).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ACCOUNT_ATM"
                    param(3).Value = dtProfileAccountATM
                    param(3).SqlDbType = SqlDbType.Structured
                    param(3).TypeName = "dbo.udt_SIPENDAR_PROFILE_ACCOUNT_ATM"

                    param(4) = New SqlParameter
                    param(4).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ADDRESS"
                    param(4).Value = dtProfileAddress
                    param(4).SqlDbType = SqlDbType.Structured
                    param(4).TypeName = "dbo.udt_SIPENDAR_PROFILE_ADDRESS"

                    param(5) = New SqlParameter
                    param(5).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_PHONE"
                    param(5).Value = dtProfilePhone
                    param(5).SqlDbType = SqlDbType.Structured
                    param(5).TypeName = "dbo.udt_SIPENDAR_PROFILE_PHONE"

                    param(6) = New SqlParameter
                    param(6).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_IDENTIFICATION"
                    param(6).Value = dtProfileIdentification
                    param(6).SqlDbType = SqlDbType.Structured
                    param(6).TypeName = "dbo.udt_SIPENDAR_PROFILE_IDENTIFICATION"

                    Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate", param)

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function


#Region "Update 8-Jul-2021 : penamabahan Generate Transaction from Profile dengan Range Tanggal"
    'Update 24-Jul-2021 Adi : Default Report Type dan Report Reason ambil dari global Parameter
    Shared Sub GenerateTransaction(objSIPENDAR_PROFILE_CLASS As SIPENDAR_PROFILE_CLASS)
        Try
            If Not objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Transaction_DateFrom.HasValue Then
                Exit Sub
            End If

            '24-Jul-2021 Adi : Default Report Type dan Reason
            'Dim strDefaultReportType As String = "LTKM"
            Dim strDefaultReportType As String = "LTKMT"
            Dim strDefaultReportReason As String = "Pelaporan SIPENDAR"

            Using objDB As New SiPendarDAL.SiPendarEntities
                Dim objDefaultReportType = objDB.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 5).FirstOrDefault
                If objDefaultReportType IsNot Nothing AndAlso objDefaultReportType.ParameterValue IsNot Nothing AndAlso objDefaultReportType.ParameterValue <> "" Then
                    strDefaultReportType = objDefaultReportType.ParameterValue
                End If

                Dim objDefaultReportReason = objDB.SIPENDAR_GLOBAL_PARAMETER.Where(Function(x) x.PK_GlobalReportParameter_ID = 6).FirstOrDefault
                If objDefaultReportReason IsNot Nothing AndAlso objDefaultReportReason.ParameterValue IsNot Nothing AndAlso objDefaultReportReason.ParameterValue <> "" Then
                    strDefaultReportReason = objDefaultReportReason.ParameterValue
                End If
            End Using

            Dim ListSar As New List(Of SIPENDAR_ODM_Generate_Report)
            Dim ListODMTransactionIncomplete As New List(Of goAML_ODM_Transaksi)
            Dim ListTransactionIncomplete As New List(Of SIPENDAR_ODM_Generate_Report_Transaction)

            Dim objSARData As New SiPendarBLL.SIPENDARGenerateData
            Dim objListTransaction = SiPendarBLL.SIPENDARGenerateBLL.getListTransactionBySearch_GCN(objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN, objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Transaction_DateFrom, objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Transaction_DateTo)

            'Save Header
            Dim GenerateSAR As New SIPENDAR_ODM_Generate_Report
            With GenerateSAR
                .CIF_NO = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN '' Edit 15-Dec-2021 Felix
                '.CIF_NO = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN + "-" + objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.SUMBER_ID + "-" + objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.FK_SIPENDAR_SUBMISSION_TYPE_CODE
                .CIF_NO = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.GCN
                .Transaction_Code = strDefaultReportType    '"LTKM"      'Default
                .Date_Report = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.CreatedDate
                .Fiu_Ref_Number = Nothing
                .Reason = strDefaultReportReason    'Nothing
                .total_transaction = objListTransaction.Count
                .Fk_Report_ID = 0
                .status = 1
                .IsSelectedAll = True
                .indicator = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Report_Indicator '' Added by Felix on 14 Sep 2021
                .FK_Sipendar_Profile_ID = objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID '' Add 16-Dec-2021

                objSARData.objGenerateSAR = GenerateSAR
            End With

            'Transaction gak usah dilistkan karena nanti dihandle oleh usp_SIPENDAR_GenerateReport

            Dim objModule = NawaBLL.ModuleBLL.GetModuleByModuleName("vw_SIPENDAR_Generate_Transaction")
            Dim DateFrom As Date = CDate(objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Transaction_DateFrom)
            Dim DateTo As Date = CDate(objSIPENDAR_PROFILE_CLASS.objSIPENDAR_PROFILE.Transaction_DateTo)

            SiPendarBLL.SIPENDARGenerateBLL.SaveGenerateSTRSarTanpaApproval(objSARData, objModule, 1, DateFrom, DateTo)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


End Class


Public Class SIPENDAR_PROFILE_CLASS

    Public objSIPENDAR_PROFILE As New SIPENDAR_PROFILE
    Public objList_SIPENDAR_PROFILE_ACCOUNT As New List(Of SIPENDAR_PROFILE_ACCOUNT)
    Public objList_SIPENDAR_PROFILE_ACCOUNT_ATM As New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
    Public objList_SIPENDAR_PROFILE_ADDRESS As New List(Of SIPENDAR_PROFILE_ADDRESS)
    Public objList_SIPENDAR_PROFILE_PHONE As New List(Of SIPENDAR_PROFILE_PHONE)
    Public objList_SIPENDAR_PROFILE_IDENTIFICATION As New List(Of SIPENDAR_PROFILE_IDENTIFICATION)

End Class
