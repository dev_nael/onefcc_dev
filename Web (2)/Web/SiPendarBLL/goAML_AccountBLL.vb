﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Ext.Net
Imports NawaDAL
Imports SiPendarDAL

Public Class goAML_AccountBLL
    Implements IDisposable
    '' edited on 04 Dec 2020
    Shared Function getDataApproval(id As String, moduleName As String) As SiPendarDAL.ModuleApproval
        Dim objModuleApproval As New SiPendarDAL.ModuleApproval
        Using objdb As New SiPendarDAL.SiPendarEntities
            Dim Approval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.ModuleKey = id And x.ModuleName = moduleName).FirstOrDefault
            'If Approval IsNot Nothing Then

            'End If
            objModuleApproval = Approval
        End Using
        Return objModuleApproval
    End Function

    Shared Function getPKAccount(id As String) As String
        Dim PKAccount = ""
        Using objdb As New SiPendarEntities
            Dim PK As String = objdb.goAML_Ref_Account.Where(Function(x) x.Account_No = id).FirstOrDefault.PK_Account_ID
            If PK IsNot Nothing Then
                PKAccount = PK
            End If
        End Using
        Return PKAccount
    End Function
    '' end of edit on 04 Dec 2020
    Shared Function GetVw_Account_Signatory(ID As String) As List(Of SiPendarDAL.Vw_Account_Signatory)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Account_Signatory Where x.AccountNo = ID Select x).ToList()
        End Using
    End Function
    Shared Function GetAccount_Signatory() As List(Of SiPendarDAL.goAML_Ref_Account_Signatory)
        Using objDb As New SiPendarEntities
            Return (From x In objDb.goAML_Ref_Account_Signatory Select x).ToList()
        End Using
    End Function


    Shared Function GetVw_Customer_Phones(ID As String) As List(Of SiPendarDAL.Vw_Customer_Phones)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_Addresses(ID As String) As List(Of SiPendarDAL.Vw_Customer_Addresses)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function
    'Update: Zikri_12102020
    Shared Function GetVw_WIC_Employer_Phones(id As String) As List(Of SiPendarDAL.Vw_WIC_Employer_Phones)
        Using ObjDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim result As List(Of SiPendarDAL.Vw_WIC_Employer_Phones) = ObjDb.Vw_WIC_Employer_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function GetVw_WIC_Phones(id As String) As List(Of SiPendarDAL.Vw_WIC_Phones)
        Using ObjDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim result As List(Of SiPendarDAL.Vw_WIC_Phones) = ObjDb.Vw_WIC_Phones.Where(Function(x) x.FK_for_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function Getvw_wic_addresses(id As String) As List(Of SiPendarDAL.vw_wic_addresses)
        Using ObjDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim result As List(Of SiPendarDAL.vw_wic_addresses) = ObjDb.vw_wic_addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function Getvw_wic_employer_addresses(id As String) As List(Of SiPendarDAL.vw_wic_employer_addresses)
        Using ObjDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Dim result As List(Of SiPendarDAL.vw_wic_employer_addresses) = ObjDb.vw_wic_employer_addresses.Where(Function(x) x.FK_To_Table_ID = id).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update
    Shared Function GetVw_Customer_AddressesCustomer(ID As String) As List(Of SiPendarDAL.Vw_Customer_Addresses)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_PhonesCustomer(ID As String) As List(Of SiPendarDAL.Vw_Customer_Phones)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1 Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_EmployerPhonesCustomer(ID As String) As List(Of SiPendarDAL.Vw_Employer_Phones)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Employer_Phones Where x.FK_for_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_EmployerAddressCustomer(ID As String) As List(Of SiPendarDAL.Vw_Employer_Addresses)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Employer_Addresses Where x.FK_To_Table_ID = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetVw_Customer_AddressesbyPK(ID As String) As SiPendarDAL.Vw_Customer_Addresses
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Addresses Where x.PK_Customer_Address_ID = ID Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetVw_Customer_PhonebyPK(ID As String) As SiPendarDAL.Vw_Customer_Phones
        Using objDb As New SiPendarDAL.SiPendarEntities
            Return (From x In objDb.Vw_Customer_Phones Where x.PK_goAML_Ref_Phone = ID Select x).FirstOrDefault()
        End Using
    End Function

    Shared Function GetAccountSignatory(ID As String) As List(Of SiPendarDAL.goAML_Ref_Account_Signatory)
        Using objDb As New SiPendarEntities
            Return (From x In objDb.goAML_Ref_Account_Signatory Where x.FK_Account_No = ID Select x).ToList()
        End Using
    End Function

    Shared Function GetCustomer(ID As Integer) As SiPendarDAL.goAML_Ref_Customer
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.PK_Customer_ID = ID).FirstOrDefault
        End Using
    End Function

    'Dedy Added
    Shared Function GetWicByCIF(cif As String) As SiPendarDAL.goAML_Ref_WIC
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = cif).FirstOrDefault
        End Using
    End Function


    Shared Function GetPhonebyPK(ID As Integer) As SiPendarDAL.goAML_Ref_Phone
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.PK_goAML_Ref_Phone = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressbyPK(ID As Integer) As SiPendarDAL.goAML_Ref_Address
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.PK_Customer_Address_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetListgoAML_Ref_PhoneByPKID(ID As Integer) As List(Of SiPendarDAL.goAML_Ref_Phone)
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function

    Shared Function GetListgoAML_Ref_AddressByPKID(ID As Integer) As List(Of SiPendarDAL.goAML_Ref_Address)
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = ID And x.FK_Ref_Detail_Of = 1).ToList
        End Using
    End Function
    Shared Function GetTabelTypeForByID(id As Integer) As SiPendarDAL.goAML_For_Table
        'done:code  GetListEmailTableTypes
        Using objDb As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
            Return objDb.goAML_For_Table.Where(Function(x) x.PK_For_Table_ID = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByID(id As String) As SiPendarDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeByKeterangan(id As String) As SiPendarDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByKeterangan(id As String) As SiPendarDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Keterangan = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeByID(id As String) As SiPendarDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAddressTypeByID(id As String) As SiPendarDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetNamaNegaraByID(id As String) As SiPendarDAL.goAML_Ref_Nama_Negara
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerByCIF(cif As String) As SiPendarDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Dim result As SiPendarDAL.goAML_Ref_Customer = objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
            'Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
        End Using
    End Function

    Shared Function GetAccountSignatorybyID(id As String) As SiPendarDAL.goAML_Ref_Account_Signatory
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Dim result As SiPendarDAL.goAML_Ref_Account_Signatory = objDb.goAML_Ref_Account_Signatory.Where(Function(x) x.PK_Signatory_ID = id).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
            'Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerByCIFandCustomerType(cif As String) As SiPendarDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = cif And x.FK_Customer_Type_ID = 2).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerEmployerAddressByPK(pkCustomer As String) As SiPendarDAL.goAML_Ref_Address
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Address.Where(Function(x) x.FK_To_Table_ID = pkCustomer And x.FK_Ref_Detail_Of = 5).FirstOrDefault
        End Using
    End Function

    Shared Function GetCustomerEmployerPhoneByPK(pkCustomer As String) As SiPendarDAL.goAML_Ref_Phone
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Phone.Where(Function(x) x.FK_for_Table_ID = pkCustomer And x.FK_Ref_Detail_Of = 5).FirstOrDefault
        End Using
    End Function

    Shared Function GetContactTypeDescByCode(kode As String) As SiPendarDAL.goAML_Ref_Kategori_Kontak
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetCommunicationTypeDescByCode(kode As String) As SiPendarDAL.goAML_Ref_Jenis_Alat_Komunikasi
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = kode).FirstOrDefault
        End Using
    End Function

    Shared Function GetNameSignatoryByID(id As String) As SiPendarDAL.goAML_Ref_Customer
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Customer.Where(Function(x) x.CIF = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
        End Using
    End Function

    'Update: Zikri_24092020 Change dropdown to NDFSDropdown
    Shared Function GetCBSignatoryNotPerson(id As String) As SiPendarDAL.goAML_Ref_WIC
        Using objDb As New SiPendarEntities
            Dim result As SiPendarDAL.goAML_Ref_WIC = objDb.goAML_Ref_WIC.Where(Function(x) x.WIC_No = id And x.FK_Customer_Type_ID = 1).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update

    'Update: Zikri_24092020 Change dropdown to NDFSDropdown
    Shared Function GetMappingAccountTypebyCode(kode As String) As SiPendarDAL.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP
        Using objDb As New SiPendarEntities
            Dim result As SiPendarDAL.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP = objDb.goAML_Ref_Mapping_Jenis_Rekening_INDV_CORP.Where(Function(x) x.Kode = kode).FirstOrDefault
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'End Update

    Shared Function GetRoleSignatoryByID(id As String) As SiPendarDAL.goAML_Ref_Peran_orang_dalam_rekening
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_rekening.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListPeranOrangDalamRekening() As List(Of SiPendarDAL.goAML_Ref_Peran_orang_dalam_rekening)
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Peran_orang_dalam_rekening.ToList()
        End Using
    End Function

    Shared Function GetJenisRekeningByID(id As String) As SiPendarDAL.goAML_Ref_Jenis_Rekening
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Rekening.Where(Function(x) x.Kode = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetAccountByAccountNo(id As String) As SiPendarDAL.goAML_Ref_Account
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Account.Where(Function(x) x.Account_No = id).FirstOrDefault
        End Using
    End Function

    Shared Function GetListJenisRekening() As List(Of SiPendarDAL.goAML_Ref_Jenis_Rekening)
        'done:code  GetListEmailTableTypes
        Using objDb As New SiPendarEntities
            Return objDb.goAML_Ref_Jenis_Rekening.ToList()
        End Using
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    Function SaveEditApproval(objData As SiPendarDAL.goAML_Ref_Account, objModule As NawaDAL.Module, objListgoAML_Ref_Account_Signatory As List(Of SiPendarDAL.goAML_Ref_Account_Signatory)) As Boolean
        'done:code  SaveAddApproval
        Using objdb As New NawaDAL.NawaDataEntities
            Using objdbDev As New SiPendarEntities
                Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                    Try
                        Dim objxData As New goAML_AccountDataBLL
                        objxData.objgoAML_Ref_Account = objData
                        objxData.objListgoAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory
                        Dim xmldata As String = NawaBLL.Common.Serialize(objxData)

                        Dim objgoAML_AccountBefore As SiPendarDAL.goAML_Ref_Account = objdbDev.goAML_Ref_Account.Where(Function(x) x.PK_Account_ID = objxData.objgoAML_Ref_Account.PK_Account_ID).FirstOrDefault
                        Dim objListgoAML_Ref_Account_SignatoryBefore As List(Of SiPendarDAL.goAML_Ref_Account_Signatory) = objdbDev.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objxData.objgoAML_Ref_Account.Account_No).ToList

                        Dim objxDatabefore As New goAML_AccountDataBLL
                        objxDatabefore.objgoAML_Ref_Account = objgoAML_AccountBefore
                        objxDatabefore.objListgoAML_Ref_Account_Signatory = objListgoAML_Ref_Account_SignatoryBefore

                        Dim xmlbefore As String = NawaBLL.Common.Serialize(objxDatabefore)

                        Dim objModuleApproval As New NawaDAL.ModuleApproval
                        With objModuleApproval
                            .ModuleName = objModule.ModuleName
                            .ModuleKey = objxData.objgoAML_Ref_Account.PK_Account_ID
                            .ModuleField = xmldata
                            .ModuleFieldBefore = xmlbefore
                            .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            .CreatedDate = Now
                            .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                        End With

                        objdb.Entry(objModuleApproval).State = Entity.EntityState.Added

                        Using objDba As New SiPendarDAL.SiPendarEntities
                            Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                            'AuditTrailDetail
                            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objData)
                            If objListgoAML_Ref_Account_Signatory.Count > 0 Then
                                For Each signatory As SiPendarDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                                    NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                                Next
                            End If
                        End Using

                        objdb.SaveChanges()
                        objtrans.Commit()
                        NawaBLL.Nawa.BLL.NawaFramework.SendEmailModuleApproval(objModuleApproval.PK_ModuleApproval_ID)

                    Catch ex As Exception
                        objtrans.Rollback()
                        Throw
                    End Try
                End Using
            End Using
        End Using
    End Function

    Sub SaveEditTanpaApproval(objData As SiPendarDAL.goAML_Ref_Account, objModule As NawaDAL.Module, objListgoAML_Ref_Account_Signatory As List(Of SiPendarDAL.goAML_Ref_Account_Signatory))
        Using objDB As New SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        objData.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                    Else
                        objData.Alternateby = ""
                    End If
                    objData.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.ApprovedDate = Now
                    objData.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                    objData.LastUpdateDate = Now
                    objDB.Entry(objData).State = Entity.EntityState.Modified


                    For Each itemx As SiPendarDAL.goAML_Ref_Account_Signatory In objDB.goAML_Ref_Account_Signatory.Where(Function(x) x.FK_Account_No = objData.Account_No).ToList()
                        Dim objcek As SiPendarDAL.goAML_Ref_Account_Signatory = objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = itemx.PK_Signatory_ID)
                        If objcek Is Nothing Then
                            objDB.Entry(itemx).State = Entity.EntityState.Deleted
                        End If
                    Next
                    For Each item As SiPendarDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                        Dim obcek As SiPendarDAL.goAML_Ref_Account_Signatory = (From x In objDB.goAML_Ref_Account_Signatory Where x.PK_Signatory_ID = item.PK_Signatory_ID Select x).FirstOrDefault
                        If obcek Is Nothing Then
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            item.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.CreatedDate = Now
                            item.Active = 1
                            objDB.Entry(item).State = Entity.EntityState.Added
                        Else
                            objDB.Entry(obcek).CurrentValues.SetValues(item)
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                obcek.Alternateby = ""
                            End If
                            obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.ApprovedDate = Now
                            obcek.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            obcek.CreatedDate = Now
                            objDB.Entry(obcek).State = Entity.EntityState.Modified
                        End If
                    Next

                    Using objDba As New SiPendarEntities
                        Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                        'AuditTrailDetail
                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objData)
                        If objListgoAML_Ref_Account_Signatory.Count > 0 Then
                            For Each signatory As SiPendarDAL.goAML_Ref_Account_Signatory In objListgoAML_Ref_Account_Signatory
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                            Next
                        End If
                    End Using

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub


    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub


    Shared Sub LoadPanel(objPanel As FormPanel, objdata As String, strModulename As String, unikkey As String)

        'done: loadPanel

        Dim objgoAML_AccountDataBLL As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(goAML_AccountDataBLL))
        Dim objAccount As SiPendarDAL.goAML_Ref_Account = objgoAML_AccountDataBLL.objgoAML_Ref_Account

        'Dim objListEmailTemplatedetail As List(Of NawaDAL.EmailTemplateDetail) = GetListEmailTemplateDetailByPKID(unikkey)
        'Dim objListEmailTemplateAction As List(Of NawaDAL.EmailTemplateAction) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateActionByPKID(unikkey)
        'Dim objListEmailTemplateAttachment As List(Of NawaDAL.EmailTemplateAttachment) = NawaBLL.EmailTemplateBLL.GetListEmailTemplateAttachmentByPKID(unikkey)

        Dim INDV_gender As SiPendarDAL.goAML_Ref_Jenis_Kelamin = Nothing
        Dim INDV_statusCode As SiPendarDAL.goAML_Ref_Status_Rekening = Nothing
        Dim INDV_nationality1 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality2 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_nationality3 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
        Dim INDV_residence As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Incorporation_legal_form As SiPendarDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
        Dim CORP_incorporation_country_code As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
        Dim CORP_Role As SiPendarDAL.goAML_Ref_Party_Role = Nothing

        Using db As New SiPendarEntities
            If Not objAccount Is Nothing Then
                Dim strunik As String = unikkey
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ID", "PK_Account_ID" & strunik, objAccount.PK_Account_ID)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account Name", "Account_Name" & strunik, objAccount.Account_Name)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objAccount.Account_No)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Branch", "Branch" & strunik, objAccount.Branch)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "IBAN", "IBAN" & strunik, objAccount.IBAN)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Clint Number", "client_number" & strunik, objAccount.client_number)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Personal Account Type", "personal_account_type" & strunik, GetJenisRekeningByID(objAccount.personal_account_type).Keterangan)
                'Update: Zikri_16102020
                If Not objAccount.balance Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Balance", "balance" & strunik, objAccount.balance)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Balance", "balance" & strunik, "")
                End If
                If Not objAccount.date_balance Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Balance", "date_balance" & strunik, CDate(objAccount.date_balance).ToString(NawaBLL.SystemParameterBLL.GetDateFormat))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Date Balance", "date_balance" & strunik, "")
                End If
                'End Update
                If goAML_CustomerBLL.GetStatusRekeningbyID(objAccount.status_code) IsNot Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Code", "status_code" & strunik, goAML_CustomerBLL.GetStatusRekeningbyID(objAccount.status_code).Keterangan)
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Code", "status_code" & strunik, "")
                End If
                If objAccount.isUpdateFromDataSource = True Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source", "isUpdateFromDataSource" & strunik, "Ya")
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Update From Data Source", "isUpdateFromDataSource" & strunik, "Tidak")
                End If
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Beneficiary", "beneficiary" & strunik, objAccount.beneficiary)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Beneficiary Comment", "beneficiary_comment" & strunik, objAccount.beneficiary_comment)
                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Comments", "comments" & strunik, objAccount.comments)
                'Update BSIM - 07Oct2020
                If Not objAccount.opened Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, CDate(objAccount.opened).ToString("dd-MMM-yyyy"))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, "")
                End If
                If Not objAccount.closed Is Nothing Then
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, CDate(objAccount.closed).ToString("dd-MMM-yyyy"))
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, "")
                End If
                '*****end update***

                If Not String.IsNullOrEmpty(objAccount.FK_CIF_Entity_ID) Then
                    Dim objentitiy As SiPendarDAL.goAML_Ref_Customer = db.goAML_Ref_Customer.Where(Function(x) x.CIF = objAccount.FK_CIF_Entity_ID).FirstOrDefault()
                    If objentitiy IsNot Nothing Then
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Rekening korporasi", "Rekening_Corp" & strunik, "Ya")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objentitiy.CIF)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Korporasi", "Corp_Name" & strunik, objentitiy.Corp_Name)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Komersial", "Corp_Commercial_Name" & strunik, objentitiy.Corp_Commercial_Name)
                        If Not goAML_CustomerBLL.GetBentukBadanUsahaByID(objentitiy.Corp_Incorporation_Legal_Form) Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bentuk Korporasi", "Corp_Incorporation_Legal_Form" & strunik, goAML_CustomerBLL.GetBentukBadanUsahaByID(objentitiy.Corp_Incorporation_Legal_Form).Keterangan)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bentuk Korporasi", "Corp_Incorporation_Legal_Form" & strunik, "")
                        End If
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nomor Induk Berusaha", "Corp_Incorporation_Number" & strunik, objentitiy.Corp_Incorporation_Number)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bidang Usaha", "Corp_Business" & strunik, objentitiy.Corp_Business)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email Korporasi", "Corp_Email" & strunik, objentitiy.Corp_Email)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Website Korporasi", "Corp_Url" & strunik, objentitiy.Corp_Url)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Provinsi", "Corp_Incorporation_State" & strunik, objentitiy.Corp_Incorporation_State)
                        If Not goAML_CustomerBLL.GetNamaNegaraByID(objentitiy.Corp_Incorporation_Country_Code) Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara", "Corp_Incorporation_Country_Code" & strunik, goAML_CustomerBLL.GetNamaNegaraByID(objentitiy.Corp_Incorporation_Country_Code).Keterangan)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara", "Corp_Incorporation_Country_Code" & strunik, "")
                        End If
                        'update BSIM - 07Oct2020
                        If Not objentitiy.Corp_Incorporation_Date Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pendirian", "Corp_Incorporation_Date" & strunik, objentitiy.Corp_Incorporation_Date)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pendirian", "Corp_Incorporation_Date" & strunik, "")
                        End If
                        If Not objentitiy.Corp_Business_Closed Is Nothing Then
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tutup?", "Corp_Business_Closed" & strunik, objentitiy.Corp_Business_Closed)
                        Else
                            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tutup?", "Corp_Business_Closed" & strunik, False)
                        End If
                        '*****end update***
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Corp_Tax_Number" & strunik, objentitiy.Corp_Tax_Number)
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Corp_Comments" & strunik, objentitiy.Corp_Comments)

                    Else
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Rekening korporasi", "Rekening_Corp" & strunik, "Tidak")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Korporasi", "Corp_Name" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Komersial", "Corp_Commercial_Name" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bentuk Korporasi", "Corp_Incorporation_Legal_Form" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nomor Induk Berusaha", "Corp_Incorporation_Number" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bidang Usaha", "Corp_Business" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email Korporasi", "Corp_Email" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Website Korporasi", "Corp_Url" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Provinsi", "Corp_Incorporation_State" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara", "Corp_Incorporation_Country_Code" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pendirian", "Corp_Incorporation_Date" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tutup?", "Corp_Business_Closed" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Corp_Tax_Number" & strunik, "")
                        NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Corp_Comments" & strunik, "")
                    End If
                Else
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Rekening korporasi", "Rekening_Corp" & strunik, "Tidak")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Korporasi", "Corp_Name" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Komersial", "Corp_Commercial_Name" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bentuk Korporasi", "Corp_Incorporation_Legal_Form" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nomor Induk Berusaha", "Corp_Incorporation_Number" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Bidang Usaha", "Corp_Business" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email Korporasi", "Corp_Email" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Website Korporasi", "Corp_Url" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Provinsi", "Corp_Incorporation_State" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara", "Corp_Incorporation_Country_Code" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pendirian", "Corp_Incorporation_Date" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tutup?", "Corp_Business_Closed" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NPWP", "Corp_Tax_Number" & strunik, "")
                    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "Corp_Comments" & strunik, "")
                End If
                'INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objCustomer.Gender).FirstOrDefault
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objCustomer.Balance)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objCustomer.Date_Balance).ToString("dd-MMM-yyyy"))

                'INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objCustomer.Status_Code).FirstOrDefault
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objCustomer.Beneficiary)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objCustomer.Beneficiary_Comment)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, IIf(String.IsNullOrEmpty(objCustomer.Comments), "", objCustomer.Comments))

                'If objEmailTemplate.FK_Monitoringduration_ID <> "1" Then
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime)
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

                'Else
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
                '    NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

                'End If

                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objEmailTemplate.Active)

                ''' Signatory
                Dim objStore As New Ext.Net.Store
                objStore.ID = strunik & "StoreGrid"
                objStore.ClientIDMode = Web.UI.ClientIDMode.Static

                Dim objModel As New Ext.Net.Model
                Dim objField As Ext.Net.ModelField

                objField = New Ext.Net.ModelField
                objField.Name = "PK_Signatory_ID"
                objField.Type = ModelFieldType.Auto
                objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_Ref_Detail_Of"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_for_Table_ID"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "FK_Account_No"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "Role"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "isPrimary"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)


                objField = New Ext.Net.ModelField
                objField.Name = "FK_CIF_Person_ID"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)

                objField = New Ext.Net.ModelField
                objField.Name = "WIC_No"
                objField.Type = ModelFieldType.String
                objModel.Fields.Add(objField)
                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_country_prefix"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_country_number"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "tph_extension"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "comments"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)


                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_EmailTemplate_ID"
                'objField.Type = ModelFieldType.Auto
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "FK_EmailTableType_ID"
                'objField.Type = ModelFieldType.Int
                'objModel.Fields.Add(objField)

                'objField = New Ext.Net.ModelField
                'objField.Name = "EmailTableType"
                'objField.Type = ModelFieldType.String
                'objModel.Fields.Add(objField)

                objStore.Model.Add(objModel)

                Dim objListcolumn As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumn.Add(objcolumnNo)
                End Using

                Dim objColum As Ext.Net.Column
                'Dim objColumwic As Ext.Net.Column

                Dim objColumname As Ext.Net.Column

                objColumname = New Ext.Net.Column
                objColumname.Text = "Name"
                objColumname.DataIndex = "FK_CIF_Person_ID"
                objColumname.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumname.Flex = 1
                objListcolumn.Add(objColumname)

                'objColumwic = New Ext.Net.Column
                'objColumwic.Text = "Name"
                'objColumwic.DataIndex = "WIC_No"
                'objColumwic.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColumwic.Flex = 1
                'objListcolumn.Add(objColumwic)

                objColum = New Ext.Net.Column
                objColum.Text = "Role"
                objColum.DataIndex = "Role"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)

                objColum = New Ext.Net.Column
                objColum.Text = "isPrimary"
                objColum.DataIndex = "isPrimary"
                objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                objColum.Flex = 1
                objListcolumn.Add(objColum)


                Dim objdtSignatory As Data.DataTable = CopyGenericToDataTable(objgoAML_AccountDataBLL.objListgoAML_Ref_Account_Signatory)


                For Each item As DataRow In objdtSignatory.Rows
                    If Not item.IsNull("FK_CIF_Person_ID") Then
                        item("FK_CIF_Person_ID") = GetNameSignatoryByID(item("FK_CIF_Person_ID")).INDV_Last_Name
                        'objColumname.Hidden = False
                        'objColumwic.Hidden = True
                    Else
                        item("FK_CIF_Person_ID") = GetCBSignatoryNotPerson(item("WIC_No")).INDV_Last_Name
                        'item("WIC_No") = GetCBSignatoryNotPerson(item("WIC_No")).INDV_Last_Name
                        'objColumname.Hidden = True
                        'objColumwic.Hidden = False
                    End If
                Next

                For Each item As DataRow In objdtSignatory.Rows
                    item("Role") = GetRoleSignatoryByID(item("Role")).Keterangan
                Next

                For Each item As DataRow In objdtSignatory.Rows
                    item("isPrimary") = Convert.ToBoolean(IIf(item("isPrimary") = True, 1, 0))
                Next

                NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Signatory", objStore, objListcolumn, objdtSignatory)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Country Prefix"
                'objColum.DataIndex = "tph_country_prefix"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Number"
                'objColum.DataIndex = "tph_number"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Extension"
                'objColum.DataIndex = "tph_extension"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                'objColum = New Ext.Net.Column
                'objColum.Text = "Comments"
                'objColum.DataIndex = "comments"
                'objColum.ClientIDMode = Web.UI.ClientIDMode.Static
                'objColum.Flex = 1
                'objListcolumn.Add(objColum)

                ''' Phone -> tbl Reference Contact Type
                'Dim objdt As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)
                'Dim objcol As New Data.DataColumn
                'objcol.ColumnName = "ContactType"
                'objcol.DataType = GetType(String)
                'objdt.Columns.Add(objcol)

                'For Each item As DataRow In objdt.Rows
                '    Dim objtask As SiPendarDAL.goAML_Ref_Kategori_Kontak = GetContactTypeByID(item("Tph_Contact_Type"))
                '    If Not objtask Is Nothing Then
                '        item("ContactType") = objtask.Keterangan
                '    End If
                'Next

                ''' Phone -> tbl Reference Communication Type
                'Dim objdtCommunicationType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objListgoAML_Ref_Phone)
                'Dim objcolCommunicationType As New Data.DataColumn
                'objcolCommunicationType.ColumnName = "CommunicationType"
                'objcolCommunicationType.DataType = GetType(String)
                'objdtCommunicationType.Columns.Add(objcolCommunicationType)

                'For Each item As DataRow In objdtCommunicationType.Rows
                '    Dim objtask As SiPendarDAL.goAML_Ref_Jenis_Alat_Komunikasi = GetCommunicationTypeByID(item("Tph_Communication_Type"))
                '    If Not objtask Is Nothing Then
                '        item("CommunicationType") = objtask.Keterangan
                '    End If
                'Next


                '' Address
                Dim objStoreAddress As New Ext.Net.Store
                objStoreAddress.ID = strunik & "StoreGridAddress"
                objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                Dim objModelAddress As New Ext.Net.Model
                Dim objFieldAddress As Ext.Net.ModelField

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "PK_Customer_Address_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_Ref_Detail_Of"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "FK_To_Table_ID"
                objFieldAddress.Type = ModelFieldType.Auto
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address_Type"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Address"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Town"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "City"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Zip"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Country_Code"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "State"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objFieldAddress = New Ext.Net.ModelField
                objFieldAddress.Name = "Comments"
                objFieldAddress.Type = ModelFieldType.String
                objModelAddress.Fields.Add(objFieldAddress)

                objStoreAddress.Model.Add(objModelAddress)


                Dim objListcolumnAddress As New List(Of ColumnBase)
                Using objcolumnNo As New Ext.Net.RowNumbererColumn
                    objcolumnNo.Text = "No."
                    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
                    objListcolumnAddress.Add(objcolumnNo)
                End Using


                Dim objColumAddress As Ext.Net.Column

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address Type"
                objColumAddress.DataIndex = "Address_Type"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Address"
                objColumAddress.DataIndex = "Address"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Town"
                objColumAddress.DataIndex = "Town"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "City"
                objColumAddress.DataIndex = "City"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Zip"
                objColumAddress.DataIndex = "Zip"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "CountryCode"
                objColumAddress.DataIndex = "Country_Code"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "State"
                objColumAddress.DataIndex = "State"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                objColumAddress = New Ext.Net.Column
                objColumAddress.Text = "Comment"
                objColumAddress.DataIndex = "Comments"
                objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
                objColumAddress.Flex = 1
                objListcolumnAddress.Add(objColumAddress)

                ''' Address -> tbl Reference Address Type
                'Dim objdtAddressType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)
                'Dim objcolAddressType As New Data.DataColumn
                'objcolAddressType.ColumnName = "AddressType"
                'objcolAddressType.DataType = GetType(String)
                'objdtAddressType.Columns.Add(objcolAddressType)

                'For Each item As DataRow In objdtAddressType.Rows
                '    Dim objtaskAddressType As SiPendarDAL.goAML_Ref_Kategori_Kontak = GetAddressTypeByID(item("Address_type"))
                '    If Not objtaskAddressType Is Nothing Then
                '        item("AddressType") = objtaskAddressType.Keterangan
                '    End If
                'Next

                ''' Address -> tbl Reference Nama Negara
                'Dim objdtNamaNegaraType As Data.DataTable = CopyGenericToDataTable(objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)
                'Dim objcolNamaNegaraType As New Data.DataColumn
                'objcolNamaNegaraType.ColumnName = "NamaNegara"
                'objcolNamaNegaraType.DataType = GetType(String)
                'objdtNamaNegaraType.Columns.Add(objcolNamaNegaraType)

                'For Each item As DataRow In objdtNamaNegaraType.Rows
                '    Dim objtaskNamaNegaraType As SiPendarDAL.goAML_Ref_Nama_Negara = GetNamaNegaraByID(item("country_code"))
                '    If Not objtaskNamaNegaraType Is Nothing Then
                '        item("NamaNegara") = objtaskNamaNegaraType.Keterangan
                '    End If
                'Next


                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objgoAML_CustomerDataBLL.objlistgoaml_ref_Address)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Contact Type", objStore, objListcolumn, objdt)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Communication Type", objStore, objListcolumn, objdtCommunicationType)

                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Tipe Alamat", objStoreAddress, objListcolumnAddress, objdtAddressType)
                'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Negara", objStoreAddress, objListcolumnAddress, objdtNamaNegaraType)

            End If
        End Using

    End Sub
    Shared Sub SettingColor(objPanelOld As FormPanel, objPanelNew As FormPanel, objdata As String, objdatabefore As String, unikkeyold As String, unikkeynew As String)

        If objdata.Length > 0 And objdatabefore.Length > 0 Then
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("PK_Customer_ID", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("CIF", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("AccountNo", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Opened", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Closed", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("Gender", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("date_balance", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("status_code", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("beneficiary_comment", objPanelOld, objPanelNew, unikkeyold, unikkeynew)
            NawaBLL.Nawa.BLL.NawaFramework.SetColor("comments", objPanelOld, objPanelNew, unikkeyold, unikkeynew)

        End If
    End Sub

    Shared Function Accept(ID As String) As Boolean
        'done: accept


        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As SiPendarDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))
                            Dim objCustomer As SiPendarDAL.goAML_Ref_Customer = objModuledata.objgoAML_Ref_Customer
                            Dim objPhone As List(Of SiPendarDAL.goAML_Ref_Phone) = objModuledata.objListgoAML_Ref_Phone
                            Dim objAddress As List(Of SiPendarDAL.goAML_Ref_Address) = objModuledata.objlistgoaml_Ref_Address

                            objCustomer.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objCustomer.ApprovedDate = Now

                            objdb.Entry(objCustomer).State = Entity.EntityState.Added

                            'audittrail
                            Dim objaudittrailheader As New SiPendarDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            For Each item As SiPendarDAL.goAML_Ref_Phone In objPhone
                                item.FK_for_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next


                            For Each item As SiPendarDAL.goAML_Ref_Address In objAddress
                                item.FK_To_Table_ID = objCustomer.PK_Customer_ID
                                objdb.Entry(item).State = Entity.EntityState.Added
                            Next

                            Dim objtype As Type = objCustomer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objCustomer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objCustomer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objPhone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objAddress
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objModuledata As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_AccountDataBLL))
                            Dim objModuledataOld As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_AccountDataBLL))

                            Dim objAccount As SiPendarDAL.goAML_Ref_Account = objModuledata.objgoAML_Ref_Account

                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                objAccount.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                objAccount.Alternateby = ""
                            End If
                            objAccount.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objAccount.ApprovedDate = Now
                            objAccount.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objAccount.LastUpdateDate = Now
                            objdb.goAML_Ref_Account.Attach(objAccount)
                            objdb.Entry(objAccount).State = Entity.EntityState.Modified


                            For Each itemx As SiPendarDAL.goAML_Ref_Account_Signatory In (From x In objdb.goAML_Ref_Account_Signatory Where x.FK_Account_No = objModuledata.objgoAML_Ref_Account.Account_No Select x).ToList
                                Dim objcek As SiPendarDAL.goAML_Ref_Account_Signatory = objModuledata.objListgoAML_Ref_Account_Signatory.Find(Function(x) x.PK_Signatory_ID = itemx.PK_Signatory_ID)
                                If objcek Is Nothing Then
                                    objdb.Entry(itemx).State = Entity.EntityState.Deleted
                                End If
                            Next
                            For Each item As SiPendarDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                Dim obcek As SiPendarDAL.goAML_Ref_Account_Signatory = (From x In objdb.goAML_Ref_Account_Signatory Where x.PK_Signatory_ID = item.PK_Signatory_ID Select x).FirstOrDefault
                                If obcek Is Nothing Then
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    item.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.LastUpdateDate = Now
                                    item.Active = 1
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                Else
                                    objdb.Entry(obcek).CurrentValues.SetValues(item)
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        obcek.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        obcek.Alternateby = ""
                                    End If
                                    obcek.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.ApprovedDate = Now
                                    obcek.LastUpdateBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    obcek.LastUpdateDate = Now
                                    objdb.Entry(obcek).State = Entity.EntityState.Modified
                                End If
                            Next

                            Using objDba As New SiPendarEntities
                                Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Update, objModule.ModuleLabel)
                                'AuditTrailDetail
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, objAccount)
                                If objModuledata.objListgoAML_Ref_Account_Signatory.Count > 0 Then
                                    For Each signatory As SiPendarDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, signatory)
                                    Next
                                End If
                            End Using


                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Deleted

                            For Each item As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            For Each item As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objdb.Entry(item).State = Entity.EntityState.Deleted
                            Next

                            'For Each item As EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'For Each item As EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objdb.Entry(item).State = Entity.EntityState.Deleted
                            'Next

                            'audittrail
                            Dim objaudittrailheader As New SiPendarDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next
                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next


                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next



                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            objdb.Entry(objModuledata.objgoAML_Ref_Customer).State = Entity.EntityState.Modified

                            'audittrail
                            Dim objaudittrailheader As New SiPendarDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New SiPendarDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    'Shared Sub LoadPanelActivation(objPanel As FormPanel, objmodulename As String, unikkey As String)
    '    'done: code anelActivation


    '    'Dim objEmailTemplateDataBLL As NawaBLL.EmailTemplateDataBLL = NawaBLL.Common.Deserialize(objdata, GetType(NawaBLL.EmailTemplateDataBLL))
    '    Dim objgoAML_Customer As SiPendarDAL.goAML_Ref_Customer = GetCustomer(unikkey)
    '    Dim objListgoAML_Ref_Phone As List(Of SiPendarDAL.goAML_Ref_Phone) = GetListgoAML_Ref_PhoneByPKID(unikkey)
    '    Dim objListgoAML_Ref_Address As List(Of SiPendarDAL.goAML_Ref_Address) = GetListgoAML_Ref_AddressByPKID(unikkey)

    '    Dim INDV_gender As SiPendarDAL.goAML_Ref_Jenis_Kelamin = Nothing
    '    Dim INDV_statusCode As SiPendarDAL.goAML_Ref_Status_Rekening = Nothing
    '    Dim INDV_nationality1 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality2 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_nationality3 As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim INDV_residence As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Incorporation_legal_form As SiPendarDAL.goAML_Ref_Bentuk_Badan_Usaha = Nothing
    '    Dim CORP_incorporation_country_code As SiPendarDAL.goAML_Ref_Nama_Negara = Nothing
    '    Dim CORP_Role As SiPendarDAL.goAML_Ref_Party_Role = Nothing

    '    Using db As SiPendarDAL.SiPendarEntities = New SiPendarDAL.SiPendarEntities
    '        If Not objgoAML_Customer Is Nothing Then
    '            Dim strunik As String = unikkey
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "CIF", "CIF" & strunik, objgoAML_Customer.CIF)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Account No", "AccountNo" & strunik, objgoAML_Customer.AccountNo)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Pembukaan Rekening", "Opened" & strunik, CDate(objgoAML_Customer.Opened).ToString("dd-MMM-yyyy"))
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Penutupan Rekening", "Closed" & strunik, CDate(objgoAML_Customer.Closed).ToString("dd-MMM-yyyy"))

    '            INDV_gender = db.goAML_Ref_Jenis_Kelamin.Where(Function(x) x.Kode = objgoAML_Customer.Gender).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Gender", "Gender" & strunik, INDV_gender.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Saldo Akhir", "balance" & strunik, objgoAML_Customer.Balance)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Saldo", "date_balance" & strunik, CDate(objgoAML_Customer.Date_Balance).ToString("dd-MMM-yyyy"))

    '            INDV_statusCode = db.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = objgoAML_Customer.Status_Code).FirstOrDefault
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Status Rekening", "status_code" & strunik, INDV_statusCode.Keterangan)

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Penerima Manfaat Utama", "beneficiary" & strunik, objgoAML_Customer.Beneficiary)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan Terkait Penerima Manfaat Utama", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Catatan", "comments" & strunik, objgoAML_Customer.Comments)

    '            If objgoAML_Customer.FK_Customer_Type_ID = "1" Then
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Title", "beneficiary_comment" & strunik, objgoAML_Customer.Beneficiary_Comment)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Lengkap", "INDV_last_name" & strunik, objgoAML_Customer.INDV_Last_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tanggal Lahir", "INDV_Birthdate" & strunik, CDate(objgoAML_Customer.INDV_BirthDate).ToString("dd-MMM-yyyy"))
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Lahir", "INDV_Birth_place" & strunik, objgoAML_Customer.INDV_Birth_Place)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Ibu Kandung", "INDV_Mothers_name" & strunik, objgoAML_Customer.INDV_Mothers_Name)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Nama Alias", "INDV_Alias" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "NIK", "INDV_SSN" & strunik, objgoAML_Customer.INDV_Alias)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Passport", "INDV_Passport_number" & strunik, objgoAML_Customer.INDV_Passport_Number)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Penerbit Passport", "INDV_Passport_country" & strunik, objgoAML_Customer.INDV_Passport_Country)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "No. Identitas Lain", "INDV_ID_Number" & strunik, objgoAML_Customer.INDV_ID_Number)

    '                INDV_nationality1 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality1).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 1", "INDV_Nationality1" & strunik, INDV_nationality1.Keterangan)

    '                INDV_nationality2 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality2).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 2", "INDV_Nationality2" & strunik, INDV_nationality2.Keterangan)

    '                INDV_nationality3 = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Nationality3).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Kewarganegaraan 3", "INDV_Nationality3" & strunik, INDV_nationality3.Keterangan)

    '                INDV_residence = db.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = objgoAML_Customer.INDV_Residence).FirstOrDefault
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Negara Domisili", "INDV_residence" & strunik, INDV_residence.Keterangan)

    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Email", "INDV_Email" & strunik, objgoAML_Customer.INDV_Email)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Pekerjaan", "INDV_Occupation" & strunik, objgoAML_Customer.INDV_Occupation)
    '                NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Tempat Bekerja", "INDV_employer_name" & strunik, objgoAML_Customer.INDV_Occupation)

    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, CDate(objEmailTemplate.StartDate).ToString("dd-MMM-yyyy"))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, objEmailTemplate.StartTime))
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, objEmailTemplate.ExcludeHoliday.GetValueOrDefault(False).ToString)

    '            Else
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartDate", "StartDate" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "StartTime", "StartTime" & strunik, "")
    '                'NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "ExcludeHoliday", "ExcludeHoliday" & strunik, "")

    '            End If

    '            NawaBLL.Nawa.BLL.NawaFramework.ExtDisplayField(objPanel, "Active", "Active" & strunik, objgoAML_Customer.Active.ToString & " -->" & (Not objgoAML_Customer.Active).ToString)

    '            Dim objStore As New Ext.Net.Store
    '            objStore.ID = strunik & "StoreGrid"
    '            objStore.ClientIDMode = Web.UI.ClientIDMode.Static

    '            Dim objModel As New Ext.Net.Model
    '            Dim objField As Ext.Net.ModelField

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "PK_goAML_ref_phone"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_Ref_detail_of"
    '            objField.Type = ModelFieldType.Auto
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "FK_for_Table_ID"
    '            objField.Type = ModelFieldType.Int
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_contact_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_communication_type"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_country_prefix"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_number"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "tph_extension"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objField = New Ext.Net.ModelField
    '            objField.Name = "comments"
    '            objField.Type = ModelFieldType.String
    '            objModel.Fields.Add(objField)

    '            objStore.Model.Add(objModel)



    '            Dim objListcolumn As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumn.Add(objcolumnNo)
    '            End Using


    '            Dim objColum As Ext.Net.Column


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Detail Dari"
    '            objColum.DataIndex = "FK_Ref_detail_of"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)



    '            objColum = New Ext.Net.Column
    '            objColum.Text = "PK"
    '            objColum.DataIndex = "FK_for_Table_ID"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)


    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kategori Kontak"
    '            objColum.DataIndex = "tph_contact_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Jenis Alat Komunikasi"
    '            objColum.DataIndex = "tph_communication_type"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Kode Area Telp"
    '            objColum.DataIndex = "tph_country_prefix"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Telepon"
    '            objColum.DataIndex = "tph_number"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Nomor Extensi"
    '            objColum.DataIndex = "tph_extension"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            objColum = New Ext.Net.Column
    '            objColum.Text = "Catatan"
    '            objColum.DataIndex = "comments"
    '            objColum.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColum.Flex = 1
    '            objListcolumn.Add(objColum)

    '            Dim objdt As Data.DataTable = CopyGenericToDataTable(objListgoAML_Ref_Phone)
    '            Dim objcol As New Data.DataColumn
    '            objcol.ColumnName = "Detail of"
    '            objcol.DataType = GetType(String)
    '            objdt.Columns.Add(objcol)

    '            For Each item As DataRow In objdt.Rows
    '                Dim objtask As SiPendarDAL.goAML_For_Table = GetTabelTypeForByID(item("FK_Ref_Detail_Of"))
    '                If Not objtask Is Nothing Then
    '                    item("FK_Ref_Detail_Of") = objtask.Description
    '                End If
    '            Next

    '            '' ini
    '            Dim objStoreAddress As New Ext.Net.Store
    '            objStoreAddress.ID = strunik & "StoreGridReplacer"
    '            objStoreAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            Dim objModelAddress As New Ext.Net.Model
    '            Dim objFieldAddress As Ext.Net.ModelField




    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "PK_Customer_Address_ID"
    '            objFieldAddress.Type = ModelFieldType.Auto
    '            objModelAddress.Fields.Add(objFieldAddress)


    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_Ref_Detail_Of"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "FK_To_Table_ID"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address_Type"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Address"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Town"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "City"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Zip"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Country_Code"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "State"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objFieldAddress = New Ext.Net.ModelField
    '            objFieldAddress.Name = "Comments"
    '            objFieldAddress.Type = ModelFieldType.String
    '            objModelAddress.Fields.Add(objFieldAddress)

    '            objStoreAddress.Model.Add(objModelAddress)


    '            Dim objListcolumnAddress As New List(Of ColumnBase)
    '            Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '                objcolumnNo.Text = "No."
    '                objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '                objListcolumnAddress.Add(objcolumnNo)
    '            End Using

    '            '' Address
    '            Dim objColumAddress As Ext.Net.Column

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Detail dari"
    '            objColumAddress.DataIndex = "FK_Ref_Detail_Of"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "PK"
    '            objColumAddress.DataIndex = "FK_To_Table_ID"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address Type"
    '            objColumAddress.DataIndex = "Address_Type"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Address"
    '            objColumAddress.DataIndex = "Address"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Town"
    '            objColumAddress.DataIndex = "Town"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "City"
    '            objColumAddress.DataIndex = "City"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Zip"
    '            objColumAddress.DataIndex = "Zip"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Country Code"
    '            objColumAddress.DataIndex = "Country_Code"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "State"
    '            objColumAddress.DataIndex = "State"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)

    '            objColumAddress = New Ext.Net.Column
    '            objColumAddress.Text = "Comments"
    '            objColumAddress.DataIndex = "Comments"
    '            objColumAddress.ClientIDMode = Web.UI.ClientIDMode.Static
    '            objColumAddress.Flex = 1
    '            objListcolumnAddress.Add(objColumAddress)
    '            '' Address


    '            'Dim objStoreAction As New Store
    '            'objStoreAction.ID = strunik & "StoreGridAction"
    '            'objStoreAction.ClientIDMode = Web.UI.ClientIDMode.Static
    '            'Dim ObjModelAction As New Ext.Net.Model

    '            'ObjModelAction.Fields.Add(New ModelField("EmailActionType", ModelFieldType.String))
    '            'ObjModelAction.Fields.Add(New ModelField("TSQLtoExecute", ModelFieldType.String))

    '            'objStoreAction.Model.Add(ObjModelAction)

    '            'Dim objListcolumnAction As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAction.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "EmailActionType", .DataIndex = "EmailActionType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAction.Add(New Ext.Net.Column() With {.Text = "TSQLtoExecute", .DataIndex = "TSQLtoExecute", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})


    '            'Dim objdtaction As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAction)
    '            'Dim objcolaction As New Data.DataColumn
    '            'objcolaction.ColumnName = "EmailActionType"
    '            'objcolaction.DataType = GetType(String)
    '            'objdtaction.Columns.Add(objcolaction)


    '            'For Each item As DataRow In objdtaction.Rows
    '            '    Dim objtask As NawaDAL.EmailActionType = NawaBLL.EmailTemplateBLL.GetEmailActionTypeByID(item("FK_EmailActionType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailActionType") = objtask.EmailActionTypeName
    '            '    End If

    '            'Next

    '            'Dim objStoreAttachment As New Store With {.ID = "StoreGridAttachment", .ClientIDMode = Web.UI.ClientIDMode.Static}
    '            'Dim ObjModelAttachment As New Ext.Net.Model

    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailAttachmentType", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("ParameterReport", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("NamaFile", ModelFieldType.String))
    '            'ObjModelAttachment.Fields.Add(New ModelField("EmailRenderAsName", ModelFieldType.String))
    '            'objStoreAttachment.Model.Add(ObjModelAttachment)



    '            'Dim objListcolumnAttachment As New List(Of ColumnBase)
    '            'Using objcolumnNo As New Ext.Net.RowNumbererColumn
    '            '    objcolumnNo.Text = "No."
    '            '    objcolumnNo.ClientIDMode = Web.UI.ClientIDMode.Static
    '            '    objListcolumnAttachment.Add(objcolumnNo)
    '            'End Using

    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Email Attachment Type", .DataIndex = "EmailAttachmentType", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Name", .DataIndex = "NamaReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report Parameter", .DataIndex = "ParameterReport", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Report File Name", .DataIndex = "NamaFile", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})
    '            'objListcolumnAttachment.Add(New Ext.Net.Column() With {.Text = "Render As", .DataIndex = "EmailRenderAsName", .ClientIDMode = Web.UI.ClientIDMode.Static, .Flex = 1})



    '            'Dim objdtattachment As Data.DataTable = NawaBLL.Common.CopyGenericToDataTable(objListEmailTemplateAttachment)

    '            'Dim objcolAttachment As New Data.DataColumn
    '            'objcolAttachment.ColumnName = "EmailAttachmentType"
    '            'objcolAttachment.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment)

    '            'Dim objcolAttachment1 As New Data.DataColumn
    '            'objcolAttachment1.ColumnName = "EmailRenderAsName"
    '            'objcolAttachment1.DataType = GetType(String)
    '            'objdtattachment.Columns.Add(objcolAttachment1)

    '            'For Each item As DataRow In objdtattachment.Rows
    '            '    Dim objtask As NawaDAL.EmailAttachmentType = NawaBLL.EmailTemplateBLL.GetEMailAttachmentTypebypk(item("FK_EmailAttachmentType_ID"))
    '            '    If Not objtask Is Nothing Then
    '            '        item("EmailAttachmentType") = objtask.EmailAttachmentType1
    '            '    End If
    '            '    If item("FK_EmailRenderAs_Id").ToString <> "" Then
    '            '        Dim objtask1 As NawaDAL.EmailRenderA = NawaBLL.EmailTemplateBLL.GetEmailRenderAsbypk(item("FK_EmailRenderAs_Id"))
    '            '        If Not objtask1 Is Nothing Then
    '            '            item("EmailRenderAsName") = objtask1.EmailRenderAsName
    '            '        End If
    '            '    End If

    '            'Next


    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Phone", objStore, objListcolumn, objdt)
    '            NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Address", objStoreAddress, objListcolumnAddress, objListgoAML_Ref_Address)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Action", objStoreAction, objListcolumnAction, objdtaction)
    '            'NawaBLL.Nawa.BLL.NawaFramework.ExtGridPanel(objPanel, "Attachment", objStoreAttachment, objListcolumnAttachment, objdtattachment)

    '        End If
    '    End Using

    'End Sub

    Shared Function Reject(ID As String) As Boolean
        'done:reject
        Using objdb As New NawaDAL.NawaDataEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As NawaDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next




                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Update

                            Dim objModuledata As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_AccountDataBLL))
                            Dim objModuledataOld As goAML_AccountDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(goAML_AccountDataBLL))

                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Update
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledataOld.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objATDetail As New SiPendarDAL.AuditTrailDetail
                                objATDetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objATDetail.FieldName = item.Name
                                objATDetail.OldValue = ""
                                If Not item.GetValue(objModuledataOld, Nothing) Is Nothing Then
                                    objATDetail.NewValue = item.GetValue(objModuledataOld, Nothing)
                                Else
                                    objATDetail.NewValue = ""
                                End If
                                objdb.Entry(objATDetail).State = Entity.EntityState.Added
                            Next

                            For Each itemheader As SiPendarDAL.goAML_Ref_Account_Signatory In objModuledata.objListgoAML_Ref_Account_Signatory
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                        'For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_ref_Address
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next
                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))

                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Delete
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next

                        'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                        '    objtype = itemheader.GetType
                        '    properties = objtype.GetProperties
                        '    For Each item As System.Reflection.PropertyInfo In properties
                        '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                        '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                        '        objaudittraildetail.FieldName = item.Name
                        '        objaudittraildetail.OldValue = ""
                        '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                        '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                        '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                        '            Else
                        '                objaudittraildetail.NewValue = ""
                        '            End If
                        '        Else
                        '            objaudittraildetail.NewValue = ""
                        '        End If
                        '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                        '    Next
                        'Next

                        Case NawaBLL.Common.ModuleActionEnum.Activation
                            Dim objModuledata As goAML_CustomerDataBLL = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(goAML_CustomerDataBLL))


                            'audittrail
                            Dim objaudittrailheader As New NawaDAL.AuditTrailHeader
                            objaudittrailheader.ApproveBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            objaudittrailheader.CreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                            objaudittrailheader.FK_AuditTrailStatus_ID = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                            objaudittrailheader.FK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Activation
                            objaudittrailheader.ModuleLabel = objModule.ModuleLabel
                            objdb.Entry(objaudittrailheader).State = Entity.EntityState.Added
                            objdb.SaveChanges()

                            Dim objtype As Type = objModuledata.objgoAML_Ref_Customer.GetType
                            Dim properties() As System.Reflection.PropertyInfo = objtype.GetProperties
                            For Each item As System.Reflection.PropertyInfo In properties
                                Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                objaudittraildetail.FieldName = item.Name
                                objaudittraildetail.OldValue = ""
                                If Not item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing) Is Nothing Then
                                    objaudittraildetail.NewValue = item.GetValue(objModuledata.objgoAML_Ref_Customer, Nothing)
                                Else
                                    objaudittraildetail.NewValue = ""
                                End If
                                objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            Next


                            For Each itemheader As SiPendarDAL.goAML_Ref_Phone In objModuledata.objListgoAML_Ref_Phone
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next



                            For Each itemheader As SiPendarDAL.goAML_Ref_Address In objModuledata.objlistgoaml_Ref_Address
                                objtype = itemheader.GetType
                                properties = objtype.GetProperties
                                For Each item As System.Reflection.PropertyInfo In properties
                                    Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                                    objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                                    objaudittraildetail.FieldName = item.Name
                                    objaudittraildetail.OldValue = ""
                                    If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                                        If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                                            objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                                        Else
                                            objaudittraildetail.NewValue = ""
                                        End If
                                    Else
                                        objaudittraildetail.NewValue = ""
                                    End If
                                    objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                                Next
                            Next
                            'For Each itemheader As NawaDAL.EmailTemplateAction In objModuledata.objListEmailTemplateAction
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next

                            'For Each itemheader As NawaDAL.EmailTemplateAttachment In objModuledata.objListEmailTemplateAttachment
                            '    objtype = itemheader.GetType
                            '    properties = objtype.GetProperties
                            '    For Each item As System.Reflection.PropertyInfo In properties
                            '        Dim objaudittraildetail As New NawaDAL.AuditTrailDetail
                            '        objaudittraildetail.FK_AuditTrailHeader_ID = objaudittrailheader.PK_AuditTrail_ID
                            '        objaudittraildetail.FieldName = item.Name
                            '        objaudittraildetail.OldValue = ""
                            '        If Not item.GetValue(itemheader, Nothing) Is Nothing Then
                            '            If item.GetValue(itemheader, Nothing).GetType.ToString <> "System.Byte[]" Then
                            '                objaudittraildetail.NewValue = item.GetValue(itemheader, Nothing)
                            '            Else
                            '                objaudittraildetail.NewValue = ""
                            '            End If
                            '        Else
                            '            objaudittraildetail.NewValue = ""
                            '        End If
                            '        objdb.Entry(objaudittraildetail).State = Entity.EntityState.Added
                            '    Next
                            'Next
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function
#End Region
End Class
