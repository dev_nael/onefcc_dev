﻿Public Class SignatoryClass
    Public objSignatory As New SiPendarDAL.SIPENDAR_Trn_acc_Signatory
    Public listAddressSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
    Public listPhoneSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
    Public listAddressEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_Trn_Acc_sign_Address)
    Public listPhoneEmployerSignatory As New List(Of SiPendarDAL.SIPENDAR_trn_acc_sign_Phone)
    Public listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Person_Identification)
End Class
