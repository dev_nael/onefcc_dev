﻿Public Class DirectorEntityAccountActivityClass
    Public objDirectorEntityAccountActivity As New SiPendarDAL.SIPENDAR_Act_Acc_Ent_Director
    Public listAddress As New List(Of SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_address)
    Public listPhone As New List(Of SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_Phone)
    Public listAddressEmployer As New List(Of SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_address)
    Public listPhoneEmployer As New List(Of SiPendarDAL.SIPENDAR_Act_Acc_Entity_Director_Phone)
    Public listIdentification As New List(Of SiPendarDAL.SIPENDAR_Activity_Person_Identification)
End Class
