﻿Public Class DirectorEntityAccountPartyClass
    Public objDirectorEntityAccountParty As New SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director
    Public listAddress As New List(Of SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Address)
    Public listPhone As New List(Of SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Phone)
    Public listAddressEmployer As New List(Of SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Address)
    Public listPhoneEmployer As New List(Of SiPendarDAL.SIPENDAR_Trn_Par_Acc_Ent_Director_Phone)
    Public listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Party_Identification)

End Class
