﻿<Serializable()>
Public Class goAML_CustomerDataBLL
    Public objgoAML_Ref_Customer As New SiPendarDAL.goAML_Ref_Customer
    Public objListgoAML_Ref_Director As New List(Of SiPendarDAL.goAML_Ref_Customer_Entity_Director)
    Public objListgoAML_Ref_Phone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Address As New List(Of SiPendarDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Identification As New List(Of SiPendarDAL.goAML_Person_Identification)
    Public objListgoAML_Ref_Identification_Director As New List(Of SiPendarDAL.goAML_Person_Identification)
    Public objListgoAML_Ref_Employer_Phone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Employer_Address As New List(Of SiPendarDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Director_Address As New List(Of SiPendarDAL.goAML_Ref_Address)
    Public objListgoAML_Ref_Director_Phone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    Public objListgoAML_Ref_Director_Employer_Phone As New List(Of SiPendarDAL.goAML_Ref_Phone)
    Public objlistgoaml_Ref_Director_Employer_Address As New List(Of SiPendarDAL.goAML_Ref_Address)

End Class
