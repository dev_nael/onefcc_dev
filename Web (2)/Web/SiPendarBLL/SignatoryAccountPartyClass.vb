﻿Public Class SignatoryAccountPartyClass
    Public objSignatoryAccountParty As New SiPendarDAL.SIPENDAR_Trn_par_acc_Signatory
    Public listAddress As New List(Of SiPendarDAL.SIPENDAR_Trn_par_Acc_sign_Address)
    Public listPhone As New List(Of SiPendarDAL.SIPENDAR_trn_par_acc_sign_Phone)
    Public listAddressEmployer As New List(Of SiPendarDAL.SIPENDAR_Trn_par_Acc_sign_Address)
    Public listPhoneEmployer As New List(Of SiPendarDAL.SIPENDAR_trn_par_acc_sign_Phone)
    Public listIdentification As New List(Of SiPendarDAL.SIPENDAR_Transaction_Party_Identification)
End Class
