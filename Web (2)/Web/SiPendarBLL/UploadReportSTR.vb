﻿Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Web.UI
Imports Ext.Net
Imports Microsoft.VisualBasic.CompilerServices
Imports NawaBLL
Imports NawaBLL.Nawa
Imports NawaDAL
Imports SiPendarDAL
Imports SiPendarBLL
Imports System.IO
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml


Public Class UploadReportSTR
    Shared Function GetIDIndikatorLaporan(ID As String) As SiPendarDAL.goAML_Ref_Indikator_Laporan
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Indikator_Laporan Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function GetJenisLaporan(ID As String) As SiPendarDAL.goAML_Ref_Jenis_Laporan
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Jenis_Laporan Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function getReportSTR() As List(Of SiPendarDAL.SIPENDAR_ReportSTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_ReportSTR).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistJenisLaporan() As List(Of SiPendarDAL.goAML_Ref_Jenis_Laporan)
        Using objDb As New SiPendarDAL.SiPendarEntities
            'Dim result = (From x In objDb.goAML_Ref_Jenis_Laporan Where x.ReportType = "LTKM/STR").ToList()
            Dim dt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_SIPENDAR_Ref_Jenis_Laporan", Nothing)
            Dim result As New List(Of goAML_Ref_Jenis_Laporan)
            If dt.Rows.Count > 0 Then
                result = NawaFramework.ConvertDataTable(Of goAML_Ref_Jenis_Laporan)(dt)
            End If
            'Dim result = (From x In objDb.goAML_Ref_Jenis_Laporan Where {"LTKM", "LTKMT"}.Contains(x.Kode) And x.Active = 1 Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistSenderFromTo() As List(Of SiPendarDAL.goAML_Ref_Sender_To_Information)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Sender_To_Information).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistJenisTransaction() As List(Of SiPendarDAL.goAML_Ref_Jenis_Transaksi)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Jenis_Transaksi).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistSourceData() As List(Of SiPendarDAL.goAML_REF_Source_Data)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_REF_Source_Data).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistpartyrole() As List(Of SiPendarDAL.goAML_Ref_Party_Role)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Party_Role).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistinstrumentransaksi() As List(Of SiPendarDAL.goAML_Ref_Instrumen_Transaksi)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Instrumen_Transaksi).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function


    Shared Function getlistcountry() As List(Of SiPendarDAL.goAML_Ref_Nama_Negara)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Nama_Negara).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistMappingmatauang() As List(Of goAML_ODM_Ref_Mata_Uang)
        Using objDb As New SiPendarEntities
            Dim result = (From x In objDb.goAML_ODM_Ref_Mata_Uang).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getlistindikatorlaporan() As List(Of SiPendarDAL.goAML_Ref_Indikator_Laporan)
        Using objDb As New SiPendarDAL.SiPendarEntities
            'Dim result = (From x In objDb.goAML_Ref_Indikator_Laporan).ToList()
            Dim result = objDb.goAML_Ref_Indikator_Laporan.Where(Function(x) x.Kode.Contains("TEROR")).ToList
            'Dim dt As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM vw_SIPENDAR_ref_indicator_laporan", Nothing)
            'Dim result As New List(Of goAML_Ref_Indikator_Laporan)
            'If dt.Rows.Count > 0 Then
            '    result = NawaFramework.ConvertDataTable(Of goAML_Ref_Indikator_Laporan)(dt)
            'End If
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function






    Shared Function getActivitySTR() As List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_ActivitySTR).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getIndikatorSTR() As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Indikator_LaporanSTR).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getBipartySTR() As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_BiPartySTR).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getmultipartySTR() As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_MultiPartySTR).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function GetModuleAction(ID As Integer) As SiPendarDAL.ModuleAction
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.ModuleActions Where x.PK_ModuleAction_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function GetFKReportByCaseID(ID As String) As SiPendarDAL.SIPENDAR_ReportSTR
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_ReportSTR Where x.Case_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getDetailTransactionBiparty(ID As String) As SiPendarDAL.SIPENDAR_Transaction_BiPartySTR_Upload
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_BiPartySTR_Upload Where x.PK_Transaction_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getsendermyclient(ID As String) As SiPendarDAL.goAML_Ref_Customer
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Customer Where x.CIF = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    'vicky himawan added 19 Mar 2021 (get country code & if null return to "-" (UNKNOWN))
    Shared Function getCountryCode(ID As String) As SiPendarDAL.goAML_Ref_Nama_Negara
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Nama_Negara Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return (From x In objDb.goAML_Ref_Nama_Negara Where x.Kode = "-" Select x).FirstOrDefault()
            End If
        End Using
    End Function
    Shared Function getMataUangCode(ID As String) As SiPendarDAL.goAML_ODM_Ref_Mata_Uang
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_ODM_Ref_Mata_Uang Where x.Kode_Internal = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getTransmodecode(ID As String) As SiPendarDAL.goAML_Ref_Jenis_Transaksi
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Jenis_Transaksi Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function getFundsCode(ID As String) As SiPendarDAL.goAML_Ref_Instrumen_Transaksi
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Instrumen_Transaksi Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function getNamaNegara(ID As String) As SiPendarDAL.goAML_Ref_Nama_Negara
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim result = (From x In objDb.goAML_Ref_Nama_Negara Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getSourceData(ID As Integer) As SiPendarDAL.goAML_REF_Source_Data
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim result = (From x In objDb.goAML_REF_Source_Data Where x.PK_Source_Data_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function



    Shared Function getMataUang(ID As String) As SiPendarDAL.goAML_Ref_Mata_Uang
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Mata_Uang Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function getsendernotmyclient(ID As String) As SiPendarDAL.goAML_Ref_WIC
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.goAML_Ref_WIC Where x.WIC_No = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function getApprovalDetail(ID As String) As SiPendarDAL.ModuleApproval
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID)).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function getDetailTransactionMultiparty(ID As String) As SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR_Upload
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.SIPENDAR_Transaction_MultiPartySTR_Upload Where x.PK_Transaction_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function getDetailTransactionMultipartySTR(ID As String) As SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_MultiPartySTR Where x.PK_Transaction_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function getDetailTransactionBipartySTR(ID As String) As SiPendarDAL.SIPENDAR_Transaction_BiPartySTR
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_BiPartySTR Where x.PK_Transaction_ID = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function GetListReport_Upload() As List(Of SiPendarDAL.SIPENDAR_ReportSTR_Upload)
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim result = (From x In objDb.SIPENDAR_ReportSTR_Upload Where x.nawa_userid = NawaBLL.Common.SessionCurrentUser.UserID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If


        End Using
    End Function
    Shared Function GetListIndikatorLaporan_Upload() As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR_Upload)
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.SIPENDAR_Indikator_LaporanSTR_Upload Where x.nawa_userid = NawaBLL.Common.SessionCurrentUser.UserID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function
    Shared Function GetListTransactionBiParty_Upload() As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR_Upload)
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.SIPENDAR_Transaction_BiPartySTR_Upload Where x.nawa_userid = NawaBLL.Common.SessionCurrentUser.UserID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function
    Shared Function GetListTransactionMultiParty_Upload() As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR_Upload)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Transaction_MultiPartySTR_Upload Where x.nawa_userid = NawaBLL.Common.SessionCurrentUser.UserID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function
    Shared Function GetListActivity_Upload() As List(Of SiPendarDAL.SIPENDAR_ActivitySTR_Upload)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_ActivitySTR_Upload Where x.nawa_userid = NawaBLL.Common.SessionCurrentUser.UserID Select x).ToList()

            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    'perubahan 20201109  Dim result = (From x In objDb.SIPENDAR_ActivitySTR Where x.FK_Report_ID = ID Select x).ToList()
    Shared Function GetListActivity(ID As Int64) As List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_ActivitySTR Where x.FK_Report_ID = ID Select x).ToList()

            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function getReportSTR(ID As Integer) As SIPENDAR_ReportSTR
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim result = (From x In objDb.SIPENDAR_ReportSTR Where x.PK_ID = ID Select x).FirstOrDefault()

            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    'perubahan 20201109 
    Shared Function GetListIndikatorLaporan(ID As Int64) As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.SIPENDAR_Indikator_LaporanSTR Where x.FK_Report_ID = ID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function getroleParty(ID As String) As SiPendarDAL.goAML_Ref_Party_Role
        Using objDb As New SiPendarDAL.SiPendarEntities
            Dim result = (From x In objDb.goAML_Ref_Party_Role Where x.Kode = ID Select x).FirstOrDefault()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function

    Shared Function GetListTransactionBiparty(ID As Integer) As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim result = (From x In objDb.SIPENDAR_Transaction_BiPartySTR Where x.FK_Report_ID = ID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function GetListTransactionMultiparty(ID As Integer) As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
        Using objDb As New SiPendarDAL.SiPendarEntities


            Dim result = (From x In objDb.SIPENDAR_Transaction_MultiPartySTR Where x.FK_Report_ID = ID Select x).ToList()
            If result IsNot Nothing Then
                Return result
            Else
                Return Nothing
            End If

        End Using
    End Function


    Shared Function SaveAddUploadReportSTR(ListReportSTR_Upload As List(Of SiPendarDAL.SIPENDAR_ReportSTR_Upload), ListIndikatorLaporan_Upload As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR_Upload), ListTransactionBiparty_Upload As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR_Upload), ListTransactionMultiparty_Upload As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR_Upload), ListActivity_Upload As List(Of SiPendarDAL.SIPENDAR_ActivitySTR_Upload), objmodule As NawaDAL.Module)
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Delete From SIPENDAR_ReportSTR_Upload where nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Delete From SIPENDAR_Indikator_LaporanSTR_Upload where nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Delete From SIPENDAR_Transaction_BiPartySTR_Upload where nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Delete From SIPENDAR_Transaction_MultiPartySTR_Upload where nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)
                    SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Delete From SIPENDAR_ActivitySTR_Upload where nawa_userid = '" & NawaBLL.Common.SessionCurrentUser.UserID & "'", Nothing)


                    Dim row = 0
                    If ListReportSTR_Upload.Count > 0 Then

                        For Each item As SIPENDAR_ReportSTR_Upload In ListReportSTR_Upload
                            row += 1
                            Dim SQLstring As String = "Insert into SIPENDAR_ReportSTR_Upload Values( 
                       '" & NawaBLL.Common.SessionCurrentUser.UserID & "', " & row & ", '', 'Insert', '" & item.PK_ID & "', '" & item.Case_ID & "',
                       '" & item.Tanggal_Laporan & "', '" & item.Jenis_laporan & "', '" & item.Ref_Num & "',
                       '" & item.Alasan & "',
                       '" & item.Tindakan_Pelapor & "', '" & item.Status & "', '" & item.isValid & "','" & item.Comments & "', '','','" & item.fiu_ref_number & "' , NULL      
                    )"
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, SQLstring, Nothing)
                        Next

                    End If

                    row = 0
                    If ListIndikatorLaporan_Upload.Count > 0 Then
                        For Each item As SIPENDAR_Indikator_LaporanSTR_Upload In ListIndikatorLaporan_Upload
                            row += 1
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Insert into SIPENDAR_Indikator_LaporanSTR_Upload Values( 
                            '" & NawaBLL.Common.SessionCurrentUser.UserID & "', " & row & ", '', 'Insert', '" & item.PK_ID & "', '" & item.Case_ID & "', '" & item.FK_Indikator_Laporan & "','',  NULL,NULL 
                            )", Nothing)

                        Next

                    End If

                    row = 0
                    If ListTransactionBiparty_Upload.Count > 0 Then
                        For Each item As SIPENDAR_Transaction_BiPartySTR_Upload In ListTransactionBiparty_Upload
                            row += 1
                            Dim SQLBiParty As String = ""

                            SQLBiParty = "Insert into SIPENDAR_Transaction_BiPartySTR_Upload Values( 
                            '" & NawaBLL.Common.SessionCurrentUser.UserID & "', " & row & ", '', 'Insert',
                            '" & item.PK_Transaction_ID & "',
                            '" & item.FK_Report_ID & "',
                            '" & item.FK_Jenis_Laporan_ID & "',
                            '" & item.isValid & "',
                            '" & item.TransactionNumber & "',
                            '" & item.Internal_Ref_Number & "',
                            '" & item.Transaction_Location & "',
                            '" & item.Transaction_Remark & "',
                            '" & item.Date_Transaction & "',
                            '" & item.Teller & "',
                            '" & item.Authorized & "',
                            '" & item.Late_Deposit & "',
                            '" & item.Date_Posting & "',
                            '" & item.Value_Date & "',
                            '" & item.Transmode_Code & "',
                            '" & item.Transmode_Comment & "',
                            '" & item.Amount_Local & "',
                            '" & item.FK_Transaction_Type & "',
                            '" & item.isMyClient_FROM & "',
                            '" & item.From_Funds_Code & "',
                            '" & item.From_Funds_Comment & "',
                            '" & item.From_Foreign_Currency_Code & "',
                            '" & item.From_Foreign_Currency_Amount & "',
                            '" & item.From_Foreign_Currency_Exchange_Rate & "',
                            '" & item.FK_Person_ID & "',
                            '" & item.FK_Sender_From_Information & "',
                            '" & item.From_Country & "',
                            '" & item.isMyClient_TO & "',
                            '" & item.To_Funds_Code & "',
                            '" & item.To_Funds_Comment & "',
                            '" & item.To_Foreign_Currency_Code & "',
                            '" & item.To_Foreign_Currency_Amount & "',
                            '" & item.To_Foreign_Currency_Exchange_Rate & "',
                            '" & item.FK_Sender_To_Information & "',
                            '" & item.To_Country & "',
                            '" & item.Comments & "',
                            '" & item.Generate_Date & "',
                            '" & item.isSwift & "',
                            '" & item.FK_Source_Data_ID & "',
                            '" & item.FK_Report_Type_ID & "',
                            '" & item.Value_Report & "',
                            '" & item.Jenis_Transaksi & "',
                            '" & item.FROM_Account_No & "',
                            '" & item.FROM_CIFNO & "',
                            '" & item.From_WIC_NO & "',
                            '" & item.TO_ACCOUNT_NO & "',
                            '" & item.TO_CIF_NO & "',
                            '" & item.TO_WIC_NO & "',
                            '" & item.IsUsedConductor & "',
                            '" & item.Condutor_ID & "',
                            '" & item.Case_ID & "',
                            '', 
                            '" & item.Swift_Code_Lawan & "',
                            '" & item.Kode_PJK_Lawan & "',
                            '" & item.Nama_PJK_Lawan & "',
                            NULL
                            )"
                            'vicky add PJK_Lawan (Menambahkan PJK Lawan)
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, SQLBiParty, Nothing) 'HSBC 20201117 Agar mudah untuk debug

                        Next
                    End If

                    row = 0
                    If ListTransactionMultiparty_Upload.Count > 0 Then
                        For Each item As SIPENDAR_Transaction_MultiPartySTR_Upload In ListTransactionMultiparty_Upload
                            row += 1
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Insert into SIPENDAR_Transaction_MultiPartySTR_Upload Values(
                            '" & NawaBLL.Common.SessionCurrentUser.UserID & "', " & row & ", '', 'Insert',
                            '" & item.PK_Transaction_ID & "',
                            '" & item.FK_Report_ID & "',
                            '" & item.FK_Jenis_Laporan_ID & "',
                            '" & item.isValid & "',
                            '" & item.TransactionNumber & "',
                            '" & item.Internal_Ref_Number & "',
                            '" & item.Transaction_Location & "',
                            '" & item.Transaction_Remark & "',
                            '" & item.Date_Transaction & "',
                            '" & item.Teller & "',
                            '" & item.Authorized & "',
                            '" & item.Late_Deposit & "',
                            '" & item.Date_Posting & "',
                            '" & item.Value_Date & "',
                            '" & item.Transmode_Code & "',
                            '" & item.Transmode_Comment & "',
                            '" & item.Amount_Local & "',
                            '" & item.isMyClient & "',
                            '" & item.Funds_Code & "',
                            '" & item.Funds_Comment & "',
                            '" & item.Foreign_Currency_Code & "',
                            '" & item.Foreign_Currency_Amount & "',
                            '" & item.Foreign_Currency_Exchange_Rate & "',
                            '" & item.FK_Person_ID & "',
                            '" & item.FK_Sender_Information & "',
                            '" & item.Country & "',
                            '" & item.Comments & "',
                            '" & item.Generate_Date & "',
                            '" & item.isSwift & "',
                            '" & item.FK_Source_Data_ID & "',
                            '" & item.FK_Report_Type_ID & "',
                            '" & item.Value_Report & "',
                            '" & item.Jenis_Transaksi & "',
                            '" & item.Peran & "',
                            '" & item.Signifikasi & "',
                            '" & item.Account_No & "',
                            '" & item.CIFNO & "',
                            '" & item.WIC_NO & "',
                            '" & item.IsUsedConductor & "',
                            '" & item.Condutor_ID & "',
                            '" & item.Case_ID & "',
                            '', 

                            NULL

                            )", Nothing)

                        Next
                    End If

                    row = 0
                    If ListActivity_Upload.Count > 0 Then
                        For Each item As SIPENDAR_ActivitySTR_Upload In ListActivity_Upload
                            row += 1
                            SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, "Insert into SIPENDAR_ActivitySTR_Upload Values(
                            '" & NawaBLL.Common.SessionCurrentUser.UserID & "', " & row & ", '', 'Insert',
                            '" & item.PK_ID & "',
                            '" & item.Case_ID & "',
                            '" & item.Significance & "',
                            '" & item.Reason & "',
                            '" & item.Comments & "',
                            '" & item.Account_No & "',
                            '" & item.WIC_No & "',
                            '" & item.NamaPJK & "',
                            '" & item.KodePJK & "',
                            '" & item.KodeSwift & "',
                            '" & item.Last_Name & "',
                            '" & item.SSN & "',
                            '" & item.Identity_No & "',
                            '" & item.Passport_No & "',
                            '" & item.SubNodeType & "',
                            '" & item.isMyClient & "',
                            '" & item.Name & "',
                            '" & item.Bussiness & "',
                            '', 

                            NULL,
                            NULL

                            )", Nothing)

                        Next
                    End If


                    objtrans.Commit()


                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function

    Shared Function CheckValidationSheetSTR()
        Using objdb As New SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try

                    Dim parameters As SqlParameter() = {
                        New SqlParameter() With {.SqlDbType = SqlDbType.VarChar, .Size = 50, .ParameterName = "userid", .Value = NawaBLL.Common.SessionCurrentUser.UserID}
                    }
                    NawaDAL.SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_ValidateUploadReportSTR", parameters)

                    objtrans.Commit()



                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function


    Shared Function GenerateReportSTR(UserID As Integer)
        Using objdb2 As New SiPendarEntities
            Using objtrans2 As System.Data.Entity.DbContextTransaction = objdb2.Database.BeginTransaction()
                Try

                    Dim parameters As SqlParameter() = {
                        New SqlParameter() With {.SqlDbType = SqlDbType.VarChar, .Size = 50, .ParameterName = "PK_EODTaskDetailLog_ID", .Value = UserID}
                    }
                    NawaDAL.SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_InsertTableReportFromUpload", parameters)
                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_ReportSTR_Upload", Nothing)
                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_Transaction_BiPartySTR_Upload", Nothing)
                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_Transaction_MultiPartySTR_Upload", Nothing)
                    'NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_ActivitySTR", Nothing)
                    'NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_Indikator_LaporanSTR", Nothing)
                    '' Fix 10-Jun-2022 Felix
                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_ActivitySTR_Upload", Nothing)
                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "Truncate Table SIPENDAR_Indikator_LaporanSTR_Upload", Nothing)
                    '' End 10-Jun-2022
                    objtrans2.Commit()



                Catch ex As Exception
                    objtrans2.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function


    Public Sub SaveUploadReportSTRTanpaApproval(ListReportSTR As List(Of SiPendarDAL.SIPENDAR_ReportSTR), ListIndikatorLaporan As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR), ListTransactionBiparty As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR), ListTransactionMultiparty As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR), ListActivity As List(Of SiPendarDAL.SIPENDAR_ActivitySTR), objmodule As NawaDAL.Module)
        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    If ListReportSTR.Count > 0 Then
                        For Each item As SIPENDAR_ReportSTR In ListReportSTR
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                        Next
                    End If

                    If ListIndikatorLaporan.Count > 0 Then
                        For Each item As SIPENDAR_Indikator_LaporanSTR In ListIndikatorLaporan
                            If Not item.Case_ID Is Nothing Then
                                If ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                    item.FK_Report_ID = ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                Else
                                    item.FK_Report_ID = Nothing
                                End If
                            End If
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                        Next
                    End If

                    If ListActivity.Count > 0 Then
                        For Each item As SIPENDAR_ActivitySTR In ListActivity
                            If Not item.Case_ID Is Nothing Then
                                If ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                    item.FK_Report_ID = ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                Else
                                    item.FK_Report_ID = Nothing
                                End If
                            End If
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                        Next
                    End If

                    If ListTransactionBiparty.Count > 0 Then
                        For Each item As SIPENDAR_Transaction_BiPartySTR In ListTransactionBiparty
                            If Not item.Case_ID Is Nothing Then
                                If ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                    item.FK_Report_ID = ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                Else
                                    item.FK_Report_ID = Nothing
                                    item.isValid = False
                                End If
                            End If
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                        Next
                    End If

                    If ListTransactionMultiparty.Count > 0 Then
                        For Each item As SIPENDAR_Transaction_MultiPartySTR In ListTransactionMultiparty
                            If Not item.Case_ID Is Nothing Then
                                If ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                    item.FK_Report_ID = ListReportSTR.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                Else
                                    item.FK_Report_ID = Nothing
                                    item.isValid = False
                                End If
                            End If
                            If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                            Else
                                item.Alternateby = ""
                            End If
                            item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                            item.ApprovedDate = Now
                            objDB.Entry(item).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                        Next
                    End If


#Region "AuditTrail"
                    Using objDba As New SiPendarEntities
                        Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objmodule.ModuleLabel)

                        'AuditTrailDetail
                        If ListReportSTR.Count > 0 Then
                            For Each Report As SiPendarDAL.SIPENDAR_ReportSTR In ListReportSTR
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Report)
                            Next
                        End If

                        If ListIndikatorLaporan.Count > 0 Then
                            For Each IndikatorLaporan As SiPendarDAL.SIPENDAR_Indikator_LaporanSTR In ListIndikatorLaporan
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, IndikatorLaporan)
                            Next
                        End If

                        '    If ListTransactionBiparty.Count > 0 Then
                        '        For Each Transactionbiparty As SiPendarDAL.SIPENDAR_Transaction_BiPartySTR In ListTransactionBiparty
                        '            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Transactionbiparty)
                        '        Next
                        '    End If

                        '    If ListTransactionMultiparty.Count > 0 Then 
                        '        For Each TransactionMultiParty As SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR In ListTransactionMultiparty
                        '            NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, TransactionMultiParty)
                        '        Next
                        '    End If

                        If ListActivity.Count > 0 Then
                            For Each Activity As SiPendarDAL.SIPENDAR_ActivitySTR In ListActivity
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Activity)
                            Next
                        End If

                    End Using
                    'AuditTrail
#End Region

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
        GenerateReportSTR(NawaBLL.Common.SessionCurrentUser.PK_MUser_ID)
    End Sub

    Sub SaveUploadReportSTRApproval(ListReportSTR As List(Of SiPendarDAL.SIPENDAR_ReportSTR), ListIndikatorLaporan As List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR), ListTransactionBiparty As List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR), ListTransactionMultiparty As List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR), ListActivity As List(Of SiPendarDAL.SIPENDAR_ActivitySTR), objmodule As NawaDAL.Module)
        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try
                    Dim objxData As New SiPendarBLL.UploadReportSTRData
                    objxData.objlistReport = ListReportSTR
                    objxData.objlistIndikatorLaporan = ListIndikatorLaporan
                    objxData.objlistTransactionBiparty = ListTransactionBiparty
                    objxData.objlistTransactionMultiparty = ListTransactionMultiparty
                    objxData.objlistActivity = ListActivity
                    Dim xmldata As String = NawaBLL.Common.Serialize(objxData)

                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleName = objmodule.ModuleName
                        .ModuleKey = 0
                        .ModuleField = xmldata
                        .PK_ModuleAction_ID = NawaBLL.Common.ModuleActionEnum.Insert
                        .CreatedDate = Now
                        .CreatedBy = NawaBLL.Common.SessionCurrentUser.UserID
                    End With

                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added

                    Using objDba As New SiPendarEntities
                        Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDba, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval, NawaBLL.Common.ModuleActionEnum.Insert, objmodule.ModuleLabel)

                        'AuditTrailDetail
                        If ListReportSTR.Count > 0 Then
                            For Each Report As SiPendarDAL.SIPENDAR_ReportSTR In ListReportSTR
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Report)
                            Next
                        End If

                        If ListIndikatorLaporan.Count > 0 Then
                            For Each IndikatorLaporan As SiPendarDAL.SIPENDAR_Indikator_LaporanSTR In ListIndikatorLaporan
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, IndikatorLaporan)
                            Next
                        End If

                        'If ListTransactionBiparty.Count > 0 Then
                        '    For Each Transactionbiparty As SiPendarDAL.SIPENDAR_Transaction_BiPartySTR In ListTransactionBiparty
                        '        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Transactionbiparty)
                        '    Next
                        'End If

                        'If ListTransactionMultiparty.Count > 0 Then
                        '    For Each TransactionMultiParty As SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR In ListTransactionMultiparty
                        '        NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, TransactionMultiParty)
                        '    Next
                        'End If

                        If ListActivity.Count > 0 Then
                            For Each Activity As SiPendarDAL.SIPENDAR_ActivitySTR In ListActivity
                                NawaFramework.CreateAuditTrailDetailAdd(objDba, header.PK_AuditTrail_ID, Activity)
                            Next
                        End If

                    End Using
                    'AuditTrail

                    objDB.SaveChanges()
                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub


    Shared Function Accept(ID As String, Remark As String) As Boolean
        Using objdb As New SiPendarDAL.SiPendarEntities
            Dim Createdby = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
            Dim UserID = objdb.MUsers.Where(Function(x) x.UserID = Createdby.CreatedBy).FirstOrDefault()
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As SiPendarDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As UploadReportSTRData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(UploadReportSTRData))

                            If objModuledata.objlistReport.Count > 0 Then
                                For Each item As SIPENDAR_ReportSTR In objModuledata.objlistReport
                                    If Remark IsNot Nothing Then
                                        item.Remark = Remark
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistIndikatorLaporan.Count > 0 Then
                                For Each item As SIPENDAR_Indikator_LaporanSTR In objModuledata.objlistIndikatorLaporan
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistActivity.Count > 0 Then
                                For Each item As SIPENDAR_ActivitySTR In objModuledata.objlistActivity
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistTransactionBiparty.Count > 0 Then
                                For Each item As SIPENDAR_Transaction_BiPartySTR In objModuledata.objlistTransactionBiparty
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                            item.isValid = False
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistTransactionMultiparty.Count > 0 Then
                                For Each item As SIPENDAR_Transaction_MultiPartySTR In objModuledata.objlistTransactionMultiparty
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                            item.isValid = False
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                            'AuditTrailDetail
                            If objModuledata.objlistReport.Count > 0 Then
                                For Each Reportstr As SIPENDAR_ReportSTR In objModuledata.objlistReport
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, Reportstr)
                                Next
                            End If

                            If objModuledata.objlistIndikatorLaporan.Count > 0 Then
                                For Each IndikatorLaporan As SIPENDAR_Indikator_LaporanSTR In objModuledata.objlistIndikatorLaporan
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, IndikatorLaporan)
                                Next
                            End If

                            'If objModuledata.objlistTransactionBiparty.Count > 0 Then
                            '    For Each TransactionBiparty As SIPENDAR_Transaction_BiPartySTR In objModuledata.objlistTransactionBiparty
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, TransactionBiparty)
                            '    Next
                            'End If

                            'If objModuledata.objlistTransactionMultiparty.Count > 0 Then
                            '    For Each Transactionmultiparty As SIPENDAR_Transaction_MultiPartySTR In objModuledata.objlistTransactionMultiparty
                            '        NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, Transactionmultiparty)
                            '    Next
                            'End If

                            If objModuledata.objlistActivity.Count > 0 Then
                                For Each activity As SIPENDAR_ActivitySTR In objModuledata.objlistActivity
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, activity)
                                Next
                            End If
                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
            GenerateReportSTR(UserID.PK_MUser_ID)
        End Using
    End Function

    Shared Function Reject(ID As String, remark As String) As Boolean
        'done:reject
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As SiPendarDAL.Module
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If
                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert

                            Dim objModuledata As UploadReportSTRData = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(UploadReportSTRData))

                            If objModuledata.objlistReport.Count > 0 Then
                                For Each item As SIPENDAR_ReportSTR In objModuledata.objlistReport
                                    If remark IsNot Nothing Then
                                        item.Remark = remark
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.Status = 3
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistIndikatorLaporan.Count > 0 Then
                                For Each item As SIPENDAR_Indikator_LaporanSTR In objModuledata.objlistIndikatorLaporan
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistActivity.Count > 0 Then
                                For Each item As SIPENDAR_ActivitySTR In objModuledata.objlistActivity
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistTransactionBiparty.Count > 0 Then
                                For Each item As SIPENDAR_Transaction_BiPartySTR In objModuledata.objlistTransactionBiparty
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                            item.isValid = False
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            If objModuledata.objlistTransactionMultiparty.Count > 0 Then
                                For Each item As SIPENDAR_Transaction_MultiPartySTR In objModuledata.objlistTransactionMultiparty
                                    If Not item.Case_ID Is Nothing Then
                                        If objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault() IsNot Nothing Then
                                            item.FK_Report_ID = objModuledata.objlistReport.Where(Function(x) x.Case_ID = item.Case_ID).FirstOrDefault().PK_ID
                                        Else
                                            item.FK_Report_ID = Nothing
                                            item.isValid = False
                                        End If
                                    End If
                                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                                        item.Alternateby = NawaBLL.Common.SessionAlternateUser.UserID
                                    Else
                                        item.Alternateby = ""
                                    End If
                                    item.ApprovedBy = NawaBLL.Common.SessionCurrentUser.UserID
                                    item.ApprovedDate = Now
                                    objdb.Entry(item).State = Entity.EntityState.Added
                                    objdb.SaveChanges()
                                Next
                            End If

                            Dim header As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, NawaBLL.Common.SessionCurrentUser.UserID, NawaBLL.Common.AuditTrailStatusEnum.Rejected, NawaBLL.Common.ModuleActionEnum.Insert, objModule.ModuleLabel)

                            'AuditTrailDetail
                            If objModuledata.objlistReport.Count > 0 Then
                                For Each Reportstr As SIPENDAR_ReportSTR In objModuledata.objlistReport
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, Reportstr)
                                Next
                            End If

                            If objModuledata.objlistIndikatorLaporan.Count > 0 Then
                                For Each IndikatorLaporan As SIPENDAR_Indikator_LaporanSTR In objModuledata.objlistIndikatorLaporan
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, IndikatorLaporan)
                                Next
                            End If

                            If objModuledata.objlistTransactionBiparty.Count > 0 Then
                                For Each TransactionBiparty As SIPENDAR_Transaction_BiPartySTR In objModuledata.objlistTransactionBiparty
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, TransactionBiparty)
                                Next
                            End If

                            If objModuledata.objlistTransactionMultiparty.Count > 0 Then
                                For Each Transactionmultiparty As SIPENDAR_Transaction_MultiPartySTR In objModuledata.objlistTransactionMultiparty
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, Transactionmultiparty)
                                Next
                            End If

                            If objModuledata.objlistActivity.Count > 0 Then
                                For Each activity As SIPENDAR_ActivitySTR In objModuledata.objlistActivity
                                    NawaFramework.CreateAuditTrailDetailAdd(objdb, header.PK_AuditTrail_ID, activity)
                                Next
                            End If


                    End Select
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()
                    objtrans.Commit()
                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function



End Class
