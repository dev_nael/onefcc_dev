﻿Imports SiPendarDAL

Public Class Signatory
    Public objSignatoryFrom As SIPENDAR_Trn_acc_Signatory
    Public listSignatoryFromPhone As List(Of SIPENDAR_trn_acc_sign_Phone)
    Public listSignatoryFromAddress As List(Of SIPENDAR_Trn_Acc_sign_Address)
    Public listSignatoryEmployerFromPhone As List(Of SIPENDAR_trn_acc_sign_Phone)
    Public listSignatoryEmployerFromAddress As List(Of SIPENDAR_Trn_Acc_sign_Address)
    Public listSignatoryIdentificationFrom As List(Of SIPENDAR_Transaction_Person_Identification)

    Public objSignatoryTo As SIPENDAR_Trn_acc_Signatory
    Public listSignatoryToPhone As List(Of SIPENDAR_trn_acc_sign_Phone)
    Public listSignatoryToAddress As List(Of SIPENDAR_Trn_Acc_sign_Address)
    Public listSignatoryEmployerToPhone As List(Of SIPENDAR_trn_acc_sign_Phone)
    Public listSignatoryEmployerToAddress As List(Of SIPENDAR_Trn_Acc_sign_Address)
    Public listSignatoryIdentificationTo As List(Of SIPENDAR_Transaction_Person_Identification)
End Class
