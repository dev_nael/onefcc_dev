﻿Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Public Class SipendarValidateXSDBLL


    Public Shared Function ValidateSchema(pksipendarreportid As Long, sessionuserid As String) As String

        Dim sqlparameter(0) As SqlClient.SqlParameter
        sqlparameter(0) = New SqlClient.SqlParameter
        With sqlparameter(0)
            .ParameterName = "@PK_SIPENDAR_PROFILE_ID"
            .Value = pksipendarreportid
            .SqlDbType = SqlDbType.BigInt
        End With




        Dim dtsipendarprofile As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT sp.PK_SIPENDAR_PROFILE_ID, sp.FK_SIPENDAR_TYPE_ID, sp.Fk_goAML_Ref_Customer_Type_id, sp.GCN, sp.FK_SIPENDAR_JENIS_WATCHLIST_CODE, sp.FK_SIPENDAR_TINDAK_PIDANA_CODE, sp.FK_SIPENDAR_SUMBER_INFORMASI_KHUSUS_CODE, sp.FK_SIPENDAR_ORGANISASI_CODE, sp.Keterangan, sp.INDV_NAME, sp.INDV_FK_goAML_Ref_Nama_Negara_CODE, sp.INDV_ADDRESS, sp.INDV_PLACEOFBIRTH, sp.INDV_DOB, sp.CORP_NAME, sp.CORP_FK_goAML_Ref_Nama_Negara_CODE, sp.CORP_NPWP, sp.CORP_NO_IZIN_USAHA, sp.AGING, sp.IsHaveReportTransaction, sp.IsAlreadyGeneratedXML, sp.LastGenerateXMLDate, sp.LastGenerateXMLBy, sp.XMLDataSIPENDAR, sp.IsValid, sp.ErrorMessage, sp.FK_Report_ID, sp.Similarity, sp.[Status], sp.[Active], sp.CreatedBy, sp.LastUpdateBy, sp.ApprovedBy, sp.CreatedDate, sp.LastUpdateDate, sp.ApprovedDate, sp.Alternateby FROM SIPENDAR_PROFILE AS sp WHERE sp.PK_SIPENDAR_PROFILE_ID=@PK_SIPENDAR_PROFILE_ID", sqlparameter)


        If Not dtsipendarprofile Is Nothing Then
            If dtsipendarprofile.Rows.Count > 0 Then
                Dim intFK_SIPENDAR_TYPE_ID As Integer = dtsipendarprofile.Rows(0)("FK_SIPENDAR_TYPE_ID")

                Dim strXSd As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT XsdXMLData FROM SIPENDAR_Template_Schema_Xsd WHERE FK_SIPENDAR_TYPE_ID= " & intFK_SIPENDAR_TYPE_ID, Nothing)


                Dim sqlparameterxml(2) As SqlClient.SqlParameter
                sqlparameterxml(0) = New SqlClient.SqlParameter
                sqlparameterxml(1) = New SqlClient.SqlParameter
                sqlparameterxml(2) = New SqlClient.SqlParameter

                With sqlparameterxml(0)
                    .Value = pksipendarreportid
                    .ParameterName = "@PK"
                    .SqlDbType = SqlDbType.Int
                End With

                With sqlparameterxml(1)
                    .Value = sessionuserid
                    .ParameterName = "@session"
                    .SqlDbType = SqlDbType.VarChar
                End With

                With sqlparameterxml(2)
                    .Value = intFK_SIPENDAR_TYPE_ID
                    .ParameterName = "@FK_SIPENDAR_Type"
                    .SqlDbType = SqlDbType.Int
                End With

                Dim strxmlsipendar As String = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GenerateXML", sqlparameterxml)

                Dim objListTableError As New DataTable
                objListTableError.Columns.Add(New DataColumn("PK_SIPENDAR_PROFILE_ID", GetType(Long)))
                objListTableError.Columns.Add(New DataColumn("ErrorMessage", GetType(String)))

                LoadValidatedXmlDocumentfromstring(strxmlsipendar, strXSd, objListTableError, pksipendarreportid)




                If objListTableError.Rows.Count >= 0 Then
                    Dim objParam(1) As SqlClient.SqlParameter


                    objParam(0) = New SqlClient.SqlParameter
                    objParam(0).ParameterName = "@SIPENDARtblValidationXSD"
                    objParam(0).Value = objListTableError

                    objParam(1) = New SqlClient.SqlParameter
                    objParam(1).ParameterName = "@PK_SIPENDAR_PROFILE_ID"
                    objParam(1).Value = pksipendarreportid

                    NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_ValidateXSD", objParam)


                End If
            End If
        End If


    End Function
    Public Shared Function LoadValidatedXmlDocumentfromstring(stringxml As String, stringxsd As String, ByRef ListofError As Data.DataTable, pksipendarreportid As Long) As XmlDocument
        Dim doc As New XmlDocument()
        Dim objxsdreader As XmlReader = XmlReader.Create(New StringReader(stringxsd))
        '' Added by Felix 02 Aug 2021
        If IsDBNull(stringxml) Or String.IsNullOrEmpty(stringxml) Then
            Throw New ApplicationException("XML tidak terbentuk. Mohon check query generate XML.")
        End If
        '' End of 02 Aug 2021
        doc.LoadXml(stringxml)
        doc.Schemas.Add(Nothing, objxsdreader)

        Dim errorBuilder As New SiPendarXmlValidationErrorBuilder()
        doc.Validate(New ValidationEventHandler(AddressOf errorBuilder.ValidationEventHandler))
        Dim listerror As List(Of String) = errorBuilder.GetErrors()

        If listerror.Count > 0 Then
            For Each item As String In listerror
                Dim objnewError As Data.DataRow = ListofError.NewRow
                With objnewError

                    objnewError.Item("PK_SIPENDAR_PROFILE_ID") = pksipendarreportid
                    objnewError.Item("ErrorMessage") = item
                End With
                ListofError.Rows.Add(objnewError)
            Next

        End If
        Return doc
    End Function
End Class
