﻿'--------------------------------------------
'Classname      : SIPENDAR_PROFILE
'Description    : Maintain SIPENDAR PROFILE Data
'Created By     : Adi Darmawan
'Created Date   : 24-Jun-2021
'--------------------------------------------

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports SiPendarDAL
Imports NawaDevDAL
Imports Ionic.Zip
Imports Ionic.Zlib
Imports System.Xml

Public Class SiPendarProfile_BLL

    Shared Function GetSiPendarProfileClassByID(ID As Long) As SiPendarBLL.SIPENDAR_PROFILE_CLASS
        Using objDb As New SiPendarDAL.SiPendarEntities

            Dim objProfile = objDb.SIPENDAR_PROFILE.Where(Function(x) x.PK_SIPENDAR_PROFILE_ID = ID).FirstOrDefault
            Dim listProfileAccount = objDb.SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileAccountATM = objDb.SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileAddress = objDb.SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfilePhone = objDb.SIPENDAR_PROFILE_PHONE.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList
            Dim listProfileIdentification = objDb.SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.FK_SIPENDAR_PROFILE_ID = ID).ToList

            Dim objProfileClass = New SIPENDAR_PROFILE_CLASS
            With objProfileClass
                .objSIPENDAR_PROFILE = objProfile
                .objList_SIPENDAR_PROFILE_ACCOUNT = listProfileAccount
                .objList_SIPENDAR_PROFILE_ACCOUNT_ATM = listProfileAccountATM
                .objList_SIPENDAR_PROFILE_ADDRESS = listProfileAddress
                .objList_SIPENDAR_PROFILE_PHONE = listProfilePhone
                .objList_SIPENDAR_PROFILE_IDENTIFICATION = listProfileIdentification
            End With

            Return objProfileClass
        End Using
    End Function

    Shared Function GetCustomerTypeByID(ID As String) As NawaDevDAL.goAML_Ref_Customer_Type
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Customer_Type.Where(Function(x) x.PK_Customer_Type_ID = ID).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisRekeningByCode(Code As String) As NawaDevDAL.goAML_Ref_Jenis_Rekening
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Rekening.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetStatusRekeningByCode(Code As String) As NawaDevDAL.goAML_Ref_Status_Rekening
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Status_Rekening.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetKategoriKontakByCode(Code As String) As NawaDevDAL.goAML_Ref_Kategori_Kontak
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Kategori_Kontak.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetNegaraByCode(Code As String) As NawaDevDAL.goAML_Ref_Nama_Negara
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Nama_Negara.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisDokumenIdentitasByCode(Code As String) As NawaDevDAL.goAML_Ref_Jenis_Dokumen_Identitas
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Dokumen_Identitas.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Function GetJenisAlatKomunikasiByCode(Code As String) As NawaDevDAL.goAML_Ref_Jenis_Alat_Komunikasi
        Using objDb As NawaDevDAL.NawaDatadevEntities = New NawaDevDAL.NawaDatadevEntities
            Return objDb.goAML_Ref_Jenis_Alat_Komunikasi.Where(Function(x) x.Kode = Code).FirstOrDefault
        End Using
    End Function

    Shared Sub SaveEditWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetSiPendarProfileClassByID(objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objXMLData_Old As String = NawaBLL.Common.Serialize(objData_Old)

                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = objXMLData_Old
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveEditWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        'Get Old Data for Audit Trail
        Dim objData_Old = GetSiPendarProfileClassByID(objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Update
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        .LastUpdateBy = strUserID
                        .LastUpdateDate = Now
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Modified
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE, objData_Old.objSIPENDAR_PROFILE)


                    '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID < 0).ToList
                        'Ambil dulu list ATM dengan PK_Account_ID sebelum diupdate
                        Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)

                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)

                        'Save ACCOUNT ATM
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            objDB.Entry(item_atm).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID < 0).ToList
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next
                    '===================== END OF NEW ADDED DATA


                    '===================== UPDATED DATA (PK untuk masing2 data > 0)
                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)

                        'Save ACCOUNT ATM
                        Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            If item_atm.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID < 0 Then
                                objDB.Entry(item_atm).State = Entity.EntityState.Added
                                objDB.SaveChanges()
                                NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                            Else
                                Dim item_atm_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = item_atm.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID).FirstOrDefault
                                objDB.Entry(item_atm).State = Entity.EntityState.Modified
                                objDB.SaveChanges()

                                If Not item_atm_old Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm, item_atm_old)
                                End If
                            End If
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item.PK_SIPENDAR_PROFILE_ADDRESS_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item.PK_SIPENDAR_PROFILE_PHONE_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID > 0).ToList
                        Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)

                        With item
                            '.LastUpdateBy = strUserID
                            '.LastUpdateDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Modified
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailEdit(objDB, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                    Next
                    '===================== END OF UPDATED DATA


                    '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                    'Deleted ACCOUNT
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted ACCOUNT ATM
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ATM_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Address
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item_old.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted PHONE
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item_old.PK_SIPENDAR_PROFILE_PHONE_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next

                    'Deleted Identity
                    For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        Dim objCek = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item_old.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                        If objCek Is Nothing Then
                            objDB.Entry(item_old).State = Entity.EntityState.Deleted
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                        End If
                    Next
                    '===================== END OF DELETED DATA

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveAddWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        '.Active = True
                        .CreatedBy = strUserID
                        .CreatedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New SiPendarDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = Nothing
                        .ModuleName = objModule.ModuleName
                        .ModuleField = objXMLData
                        .ModuleFieldBefore = Nothing
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Function SaveAddWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        Dim IDUnik As Long

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Insert
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Additional Information for Header
                    With objData.objSIPENDAR_PROFILE
                        '.Active = True
                        .CreatedBy = strUserID
                        .CreatedDate = Now
                        .ApprovedBy = strUserID
                        .ApprovedDate = Now
                        .Alternateby = strAlternateID
                    End With

                    'Save objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    '25-May-2021
                    IDUnik = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                    '-----------

                    'Audit Trail Header
                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)

                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                        'Ambil dulu list ATM dengan PK_Account_ID sebelum diupdate
                        Dim listATM As New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
                        listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID).ToList

                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)

                        'Save ACCOUNT ATM
                        'Dim listATM = objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM.Where(Function(x) x.FK_SIPENDAR_PROFILE_ACCOUNT_ID = IDAccount).ToList
                        For Each item_atm In listATM
                            With item_atm
                                .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                                .FK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID
                            End With

                            objDB.Entry(item_atm).State = Entity.EntityState.Added
                            objDB.SaveChanges()
                            NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item_atm)
                        Next
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        With item
                            .FK_SIPENDAR_PROFILE_ID = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                            '.Active = True
                            '.CreatedBy = strUserID
                            '.CreatedDate = Now
                            '.ApprovedBy = strUserID
                            '.ApprovedDate = Now
                            '.Alternateby = strAlternateID
                        End With

                        objDB.Entry(item).State = Entity.EntityState.Added
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailAdd(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()

                    Return IDUnik
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex

                    Return Nothing
                End Try
            End Using
        End Using
    End Function

    Shared Sub SaveDeleteWithApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)

        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.WaitingToApproval
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Save objData to Module Approval
                    Dim objXMLData As String = NawaBLL.Common.Serialize(objData)
                    Dim objModuleApproval As New NawaDAL.ModuleApproval
                    With objModuleApproval
                        .ModuleKey = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID
                        .ModuleName = objModule.ModuleName
                        .ModuleField = Nothing
                        .ModuleFieldBefore = objXMLData
                        .PK_ModuleAction_ID = intModuleAction
                        .CreatedDate = Now
                        .CreatedBy = strUserID
                    End With

                    'Save objModuleApproval
                    objDB.Entry(objModuleApproval).State = Entity.EntityState.Added
                    objDB.SaveChanges()

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub SaveDeleteWithoutApproval(objModule As NawaDAL.Module, objData As SIPENDAR_PROFILE_CLASS)
        Using objDB As New SiPendarDAL.SiPendarEntities
            Using objTrans As System.Data.Entity.DbContextTransaction = objDB.Database.BeginTransaction()
                Try

                    'Define local variables
                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.AffectedToDatabase
                    Dim intModuleAction As Integer = NawaBLL.Common.ModuleActionEnum.Delete
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    'Delete objData
                    objDB.Entry(objData.objSIPENDAR_PROFILE).State = Entity.EntityState.Deleted
                    objDB.SaveChanges()

                    'Audit Trail Header
                    Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objDB, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                    NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)

                    'Save ACCOUNT
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save ACCOUNT_ATM
                    For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Address
                    For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save PHONE
                    For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    'Save Identity
                    For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                        objDB.Entry(item).State = Entity.EntityState.Deleted
                        objDB.SaveChanges()
                        NawaFramework.CreateAuditTrailDetailDelete(objDB, objAuditTrailheader.PK_AuditTrail_ID, item)
                    Next

                    objTrans.Commit()
                Catch ex As Exception
                    objTrans.Rollback()
                    Throw ex
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Accept(ID As Long)

        Dim IDUnik As Long

        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As NawaDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = NawaBLL.ModuleBLL.GetModuleByModuleName(objApproval.ModuleName)
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            '25-May-2021 ganti jadi function agar bisa mengembalikan IDUnik
                            'SaveAddWithoutApproval(objModule, objData)
                            IDUnik = SaveAddWithoutApproval(objModule, objData)

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveEditWithoutApproval(objModule, objData)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))

                            'Save data dan Audit Trail sama dengan Without Approval
                            SaveDeleteWithoutApproval(objModule, objData_Old)

                            '25-May-2021 IDUnik ambil dati objData
                            IDUnik = objData_Old.objSIPENDAR_PROFILE.PK_SIPENDAR_PROFILE_ID

                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Shared Sub Reject(ID As Long)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Define local variables
                    Dim objApproval As SiPendarDAL.ModuleApproval = objdb.ModuleApprovals.Where(Function(x) x.PK_ModuleApproval_ID = ID).FirstOrDefault()
                    Dim objModule As SiPendarDAL.Module = Nothing
                    If Not objApproval Is Nothing Then
                        objModule = objdb.Modules.Where(Function(x) x.ModuleName = objApproval.ModuleName).FirstOrDefault
                    End If

                    Dim strUserID As String = NawaBLL.Common.SessionCurrentUser.UserID
                    Dim strAlternateID As String = Nothing
                    Dim intAuditTrailStatus As Integer = NawaBLL.Common.AuditTrailStatusEnum.Rejected
                    Dim intModuleAction As Integer = objApproval.PK_ModuleAction_ID
                    Dim strModuleName As String = objModule.ModuleLabel

                    If NawaBLL.Common.SessionAlternateUser IsNot Nothing Then
                        strAlternateID = NawaBLL.Common.SessionAlternateUser.UserID
                    End If

                    Select Case objApproval.PK_ModuleAction_ID
                        Case NawaBLL.Common.ModuleActionEnum.Insert
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE)

                            'Save ACCOUNT
                            For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save PHONE
                            For Each item In objData.objList_SIPENDAR_PROFILE_PHONE
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                        Case NawaBLL.Common.ModuleActionEnum.Update
                            Dim objData As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleField, GetType(SIPENDAR_PROFILE_CLASS))
                            Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData.objSIPENDAR_PROFILE, objData_Old.objSIPENDAR_PROFILE)

                            '===================== NEW ADDED DATA (PK untuk masing2 data < 0)
                            'Save ACCOUNT
                            For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save PHONE
                            For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID < 0).ToList
                                NawaFramework.CreateAuditTrailDetailAdd(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next
                            '===================== END OF NEW ADDED DATA


                            '===================== UPDATED DATA (PK untuk masing2 data > 0)
                            'Save ACCOUNT
                            For Each item In objData.objList_SIPENDAR_PROFILE_ACCOUNT.Where(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID > 0).ToList
                                Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next

                            'Save Address
                            For Each item In objData.objList_SIPENDAR_PROFILE_ADDRESS.Where(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID > 0).ToList
                                Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next

                            'Save PHONE
                            For Each item In objData.objList_SIPENDAR_PROFILE_PHONE.Where(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID > 0).ToList
                                Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item.PK_SIPENDAR_PROFILE_PHONE_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next

                            'Save Identity
                            For Each item In objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Where(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID > 0).ToList
                                Dim item_old = objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                                NawaFramework.CreateAuditTrailDetailEdit(objdb, objAuditTrailheader.PK_AuditTrail_ID, item, item_old)
                            Next
                            '===================== END OF UPDATED DATA


                            '===================== DELETED DATA (Cari PK di Data Lama yang tidak ada di Data Baru)
                            'Deleted ACCOUNT
                            For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                                Dim objCek = objData.objList_SIPENDAR_PROFILE_ACCOUNT.Find(Function(x) x.PK_SIPENDAR_PROFILE_ACCOUNT_ID = item_old.PK_SIPENDAR_PROFILE_ACCOUNT_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next

                            'Deleted Address
                            For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                                Dim objCek = objData.objList_SIPENDAR_PROFILE_ADDRESS.Find(Function(x) x.PK_SIPENDAR_PROFILE_ADDRESS_ID = item_old.PK_SIPENDAR_PROFILE_ADDRESS_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next

                            'Deleted PHONE
                            For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                                Dim objCek = objData.objList_SIPENDAR_PROFILE_PHONE.Find(Function(x) x.PK_SIPENDAR_PROFILE_PHONE_ID = item_old.PK_SIPENDAR_PROFILE_PHONE_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next

                            'Deleted Identity
                            For Each item_old In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                                Dim objCek = objData.objList_SIPENDAR_PROFILE_IDENTIFICATION.Find(Function(x) x.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID = item_old.PK_SIPENDAR_PROFILE_IDENTIFICATION_ID)
                                If objCek Is Nothing Then
                                    NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item_old)
                                End If
                            Next
                        '===================== END OF DELETED DATA

                        Case NawaBLL.Common.ModuleActionEnum.Delete
                            Dim objData_Old As SIPENDAR_PROFILE_CLASS = NawaBLL.Common.Deserialize(objApproval.ModuleFieldBefore, GetType(SIPENDAR_PROFILE_CLASS))

                            'Audit Trail Header
                            Dim objAuditTrailheader As SiPendarDAL.AuditTrailHeader = NawaFramework.CreateAuditTrail(objdb, strUserID, intAuditTrailStatus, intModuleAction, strModuleName)
                            With objAuditTrailheader
                                .PK_ModuleApproval_ID = objApproval.PK_ModuleApproval_ID
                            End With
                            NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, objData_Old.objSIPENDAR_PROFILE)

                            'Save ACCOUNT
                            For Each item In objData_Old.objList_SIPENDAR_PROFILE_ACCOUNT
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Address
                            For Each item In objData_Old.objList_SIPENDAR_PROFILE_ADDRESS
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save PHONE
                            For Each item In objData_Old.objList_SIPENDAR_PROFILE_PHONE
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next

                            'Save Identity
                            For Each item In objData_Old.objList_SIPENDAR_PROFILE_IDENTIFICATION
                                NawaFramework.CreateAuditTrailDetailDelete(objdb, objAuditTrailheader.PK_AuditTrail_ID, item)
                            Next
                    End Select

                    'Delete Module Approval
                    objdb.Entry(objApproval).State = Entity.EntityState.Deleted
                    objdb.SaveChanges()

                    objtrans.Commit()

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Sub

    Public Shared Function CopyGenericToDataTable(Of T)(list As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In list
            For i As Integer = 0 To values.Length - 1
                values(i) = If(props(i).GetValue(item), DBNull.Value)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

    Public Shared Function IsExistsInApproval(strModuleName As String, strID As String) As Boolean
        Try
            Dim isExists As Boolean = False

            Using objdb As New SiPendarDAL.SiPendarEntities
                Dim objCek = objdb.ModuleApprovals.Where(Function(x) x.ModuleName = strModuleName And x.ModuleKey = strID).FirstOrDefault
                If objCek IsNot Nothing Then
                    isExists = True
                End If
            End Using

            Return isExists

        Catch ex As Exception
            Throw ex
            Return True
        End Try
    End Function

    Public Shared Function GenerateXML_ProAktif(listSiPendarProfile As List(Of SIPENDAR_PROFILE), strDirPath As String) As String
        Try
            Dim strReportCode = "SIPENDAR-PROAKTIF"
            Dim strTanggalGenerate As String = Convert.ToDateTime(Now()).ToString("yyyyMMddHHmmss")

            Dim strZipFile As String = ""

            Dim lstFile As New List(Of String)
            Dim lstFileZip As New List(Of String)

            Dim intjmlFiledalam1zip As Integer = 1000
            'intjmlFiledalam1zip = NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.Text, "SELECT garrgp.ParameterValue FROM goAML_Ref_ReportGlobalParameter AS garrgp WHERE garrgp.PK_GlobalReportParameter_ID=11", Nothing)

            Dim intCounter As Integer = 0

            Dim listAttachment As New List(Of String)
            For Each item In listSiPendarProfile

                Dim pkProfileID As Long = item.PK_SIPENDAR_PROFILE_ID

                Dim objParam(2) As SqlParameter
                objParam(0) = New SqlParameter
                objParam(0).ParameterName = "@PK"
                objParam(0).Value = pkProfileID

                objParam(1) = New SqlParameter
                objParam(1).ParameterName = "@session"
                objParam(1).Value = NawaBLL.Common.SessionCurrentUser.UserID

                objParam(2) = New SqlParameter
                objParam(2).ParameterName = "@FK_SIPENDAR_Type"
                objParam(2).Value = 1   'ProAktif

                Dim incrementRow As Int32
                incrementRow = 0
                Dim xmlDataTable As DataTable = NawaDAL.SQLHelper.ExecuteTable(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_GenerateXML", objParam)

                For Each itemXML As DataRow In xmlDataTable.Rows
                    Dim strResult As String = itemXML(0)
                    Dim strFileXML As String

                    If xmlDataTable.Rows.Count > 1 Then
                        incrementRow = incrementRow + 1
                        strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & "-" & incrementRow & "-" & ".xml"
                    Else
                        strFileXML = strDirPath & strReportCode & "-" & pkProfileID & "-" & strTanggalGenerate & ".xml"
                    End If

                    If strResult = "" Then
                        Throw New Exception("Data Report Empty for Profile ID =" & pkProfileID)
                    End If

                    'Delete file kalau sudah pernah ada
                    If IO.File.Exists(strFileXML) Then
                        IO.File.Delete(strFileXML)
                    End If

                    'Load XML from data row and save to temp dir
                    Dim objdoc As New XmlDocument
                    objdoc.LoadXml(strResult)
                    objdoc.PreserveWhitespace = False
                    objdoc.Save(strFileXML)

                    'Tambahkan object XML ke list
                    lstFile.Add(strFileXML)
                Next

                If lstFile.Count = intjmlFiledalam1zip And lstFile.Count > 0 Then
                    Using zip As New ZipFile()
                        intCounter += 1
                        Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                        If IO.File.Exists(strFileNameZip) Then
                            IO.File.Delete(strFileNameZip)
                        End If
                        zip.CompressionLevel = CompressionLevel.Level9
                        zip.AddFiles(lstFile, "")

                        zip.Save(strFileNameZip)
                        lstFileZip.Add(strFileNameZip)
                    End Using

                    'Buang file karena sudah di zip
                    For Each Item1 As String In lstFile
                        IO.File.Delete(Item1)
                    Next

                    lstFile.Clear()
                End If
            Next

            'Kalau masih ada list of file yang belum di zip
            If lstFile.Count > 0 Then

                Using zip As New ZipFile()
                    intCounter += 1
                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-" & Right("000" & intCounter, 3) & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If
                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFile, "")

                    zip.Save(strFileNameZip)
                    lstFileZip.Add(strFileNameZip)

                End Using

                'buang karena sudah di zip
                For Each Item1 As String In lstFile
                    IO.File.Delete(Item1)
                Next
                lstFile.Clear()

            End If

            'Kembalikan Zip File
            If lstFileZip.Count > 1 Then
                Using zip As New ZipFile()

                    Dim strFileNameZip As String = strDirPath & strReportCode & "-" & strTanggalGenerate & "-All" & ".zip"

                    If IO.File.Exists(strFileNameZip) Then
                        IO.File.Delete(strFileNameZip)
                    End If

                    zip.CompressionLevel = CompressionLevel.Level9
                    zip.AddFiles(lstFileZip, "")
                    zip.Save(strFileNameZip)

                    For Each Item1 As String In lstFileZip
                        IO.File.Delete(Item1)
                    Next

                    strZipFile = strFileNameZip
                End Using

            ElseIf lstFileZip.Count = 1 Then
                strZipFile = lstFileZip.Item(0).ToString
            Else
                Throw New Exception("No reports generated.")
            End If

            Return strZipFile
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    Shared Function checkValidation(objSIPENDAR_CLASS As SIPENDAR_PROFILE_CLASS, objmodule As NawaDAL.Module, Action As Integer)
        Using objdb As New SiPendarDAL.SiPendarEntities
            Using objtrans As System.Data.Entity.DbContextTransaction = objdb.Database.BeginTransaction()
                Try
                    'Object Profile
                    Dim listObjProfile As New List(Of SiPendarDAL.SIPENDAR_PROFILE)
                    Dim listObjProfileAccount As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT)
                    Dim listObjProfileAccountATM As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ACCOUNT_ATM)
                    Dim listObjProfileAddress As New List(Of SiPendarDAL.SIPENDAR_PROFILE_ADDRESS)
                    Dim listObjProfilePhone As New List(Of SiPendarDAL.SIPENDAR_PROFILE_PHONE)
                    Dim listObjProfileIdentification As New List(Of SiPendarDAL.SIPENDAR_PROFILE_IDENTIFICATION)

                    'Populate data to list
                    With objSIPENDAR_CLASS
                        listObjProfile.Add(.objSIPENDAR_PROFILE)

                        For Each item1 In .objList_SIPENDAR_PROFILE_ACCOUNT
                            listObjProfileAccount.Add(item1)
                        Next

                        For Each item2 In .objList_SIPENDAR_PROFILE_ACCOUNT_ATM
                            listObjProfileAccountATM.Add(item2)
                        Next

                        For Each item3 In .objList_SIPENDAR_PROFILE_ADDRESS
                            listObjProfileAddress.Add(item3)
                        Next

                        For Each item4 In .objList_SIPENDAR_PROFILE_PHONE
                            listObjProfilePhone.Add(item4)
                        Next

                        For Each item5 In .objList_SIPENDAR_PROFILE_IDENTIFICATION
                            listObjProfileIdentification.Add(item5)
                        Next

                    End With

                    'Convert to Datatable to throw to SP
                    Dim dtProfile As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfile)
                    Dim dtProfileAccount As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAccount)
                    Dim dtProfileAccountATM As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAccountATM)
                    Dim dtProfileAddress As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileAddress)
                    Dim dtProfilePhone As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfilePhone)
                    Dim dtProfileIdentification As DataTable = NawaBLL.Common.CopyGenericToDataTable(listObjProfileIdentification)

                    Dim param(6) As SqlParameter

                    param(0) = New SqlParameter
                    param(0).ParameterName = "@userid"
                    param(0).Value = NawaBLL.Common.SessionCurrentUser.UserID
                    param(0).DbType = SqlDbType.VarChar

                    param(1) = New SqlParameter
                    param(1).ParameterName = "@TMP_udt_SIPENDAR_PROFILE"
                    param(1).Value = dtProfile
                    param(1).SqlDbType = SqlDbType.Structured
                    param(1).TypeName = "dbo.udt_SIPENDAR_PROFILE"

                    param(2) = New SqlParameter
                    param(2).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ACCOUNT"
                    param(2).Value = dtProfileAccount
                    param(2).SqlDbType = SqlDbType.Structured
                    param(2).TypeName = "dbo.udt_SIPENDAR_PROFILE_ACCOUNT"

                    param(3) = New SqlParameter
                    param(3).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ACCOUNT_ATM"
                    param(3).Value = dtProfileAccountATM
                    param(3).SqlDbType = SqlDbType.Structured
                    param(3).TypeName = "dbo.udt_SIPENDAR_PROFILE_ACCOUNT_ATM"

                    param(4) = New SqlParameter
                    param(4).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_ADDRESS"
                    param(4).Value = dtProfileAddress
                    param(4).SqlDbType = SqlDbType.Structured
                    param(4).TypeName = "dbo.udt_SIPENDAR_PROFILE_ADDRESS"

                    param(5) = New SqlParameter
                    param(5).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_PHONE"
                    param(5).Value = dtProfilePhone
                    param(5).SqlDbType = SqlDbType.Structured
                    param(5).TypeName = "dbo.udt_SIPENDAR_PROFILE_PHONE"

                    param(6) = New SqlParameter
                    param(6).ParameterName = "@TMP_udt_SIPENDAR_PROFILE_IDENTIFICATION"
                    param(6).Value = dtProfileIdentification
                    param(6).SqlDbType = SqlDbType.Structured
                    param(6).TypeName = "dbo.udt_SIPENDAR_PROFILE_IDENTIFICATION"

                    Return NawaDAL.SQLHelper.ExecuteScalar(NawaDAL.SQLHelper.strConnectionString, CommandType.StoredProcedure, "usp_SIPENDAR_PROFILE_Validate", param)

                Catch ex As Exception
                    objtrans.Rollback()
                    Throw
                End Try
            End Using
        End Using
    End Function


End Class


Public Class SIPENDAR_PROFILE_CLASS

    Public objSIPENDAR_PROFILE As New SIPENDAR_PROFILE
    Public objList_SIPENDAR_PROFILE_ACCOUNT As New List(Of SIPENDAR_PROFILE_ACCOUNT)
    Public objList_SIPENDAR_PROFILE_ACCOUNT_ATM As New List(Of SIPENDAR_PROFILE_ACCOUNT_ATM)
    Public objList_SIPENDAR_PROFILE_ADDRESS As New List(Of SIPENDAR_PROFILE_ADDRESS)
    Public objList_SIPENDAR_PROFILE_PHONE As New List(Of SIPENDAR_PROFILE_PHONE)
    Public objList_SIPENDAR_PROFILE_IDENTIFICATION As New List(Of SIPENDAR_PROFILE_IDENTIFICATION)

End Class
