﻿<Serializable()>
Public Class UploadReportSTRData
    Public objlistReport As New List(Of SiPendarDAL.SIPENDAR_ReportSTR)
    Public objlistIndikatorLaporan As New List(Of SiPendarDAL.SIPENDAR_Indikator_LaporanSTR)
    Public objlistTransactionBiparty As New List(Of SiPendarDAL.SIPENDAR_Transaction_BiPartySTR)
    Public objlistTransactionMultiparty As New List(Of SiPendarDAL.SIPENDAR_Transaction_MultiPartySTR)
    Public objlistActivity As New List(Of SiPendarDAL.SIPENDAR_ActivitySTR)
End Class
